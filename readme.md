# JamJar.com
## Version 1.0.0

**Developer:** Michael Burton <joe.hubbard@impression.co.uk>
**Designer:** Alexandra Lofthouse <alexandra@fifteen.co.uk>

**‼️ THIS IS A RELEASE CANDIDATE ‼️**

## Pre-Installation Information

**[ALL API KEYS ARE IN BASECAMP.](https://3.basecamp.com/3469230/buckets/6795693/vaults/949034351)**

Jamjar is a Laravel application running on Laravel 5.5 as it is LTS. Upgrades **must not** be attempted to Laravel unless absolutely necessary. Stability is paramount.

## Regular installation

Installation is much the same as any other Laravel repository.

- Clone the repository - `git clone git@bitbucket.org:fifteendesignteam/8985-jamjar.git .`
- Set up the githooks - `git config core.hooksPath githooks`
- Checkout the `staging` branch, this is where all development should be done and will usually be ahead of `master` - `git checkout staging`
- Run `cp .env.example .env`
- Run `composer install`
- Run `npm install`
- Run `php artisan key:generate` if the post-composer-install scripts don't run
- Set your database configuration up in `.env`
- **Important** Set up the API credentials from [Basecamp](https://3.basecamp.com/3469230/buckets/6795693/vaults/949034351) in `.env`. At a minimum these must be the `CAPAPI_USERNAME` and `CAPAPI_PASSWORD` variables as other API's can be disabled via boolean.

## Docker installation

You can also setup this application using Docker Compose. Check the docker/README.md file for instructions.

### If you're wanting to use a fresh local database (Not Recommended):

- Run `php artisan migrate`
- Open `database/seeds/UserSeed.php` and change joe.hubbard@impression.co.uk to your email address
- Run `php artisan db:seed`. This seeds basic information and should display on screen what is being done.
- Run `php artisan db:seed --class=CapSeeder`. This will contact CAP and get every Vehicle Model and Derivative ever. This may take some time.

### If you're wanting to use a clone of the live database (Recommended):

- Download a copy of the live database from jamjar.com and set it up locally
- Point your .env file's DB variables to your local copy of the jamjar.com database
- Ensure the jobs table is emptied out `TRUNCATE TABLE jobs;` before running any queue workers to avoid double notifications to customers.

## Unit Tests

- Run `phpunit` to run the (admittedly limited) testing suite. At the time of writing every test is passing.
- For more detailed coverage reports, run `php -d xdebug.profiler_enable=On vendor/phpunit/phpunit/phpunit --coverage-html=./public/report` - This will output a report accessible from `https://jamjar.test/report`

## Basic Technical Specification

* [Google Sheet](https://docs.google.com/spreadsheets/d/1LSG7IeCQa1hOdRVtOHzi9DwAt1f1Jg0ywNnhGBVWFpA/edit)
* [Laravel 5.5.x](https://www.laravel.com/docs/5.5)
* [VueJS 2.x](https://vuejs.org/v2/guide/)
* [WordPress](https://codex.wordpress.org)




# AWS Infrastructure

There are two environments, development and production. Please note that there are Elastic Beanstalk related scripts in .ebextensions directory

#### Development environment is build of two Elastic Beanstalks:

- <b>jamjar-staging</b> - a web application reachable at https://staging.comparison.jamjar.com/ (access is restricted to a set of whitelisted IP addresses only at the security group level)
- <b>jamjar-demo</b> - a instance processing jobs in the queue

Development database (mysql) is an RDS instance named <b>aanmkxfufvvu0n</b> created within jamjar-staging Elastic Beanstalk

#### Production environment is build of two Elastic Beanstalks:

- <b>jamjar-prod-v-2</b> - a web application reachable at http://comparison.jamjar.com/
- <b>jamjar-queue</b> - a instance processing jobs in the queue

Production database (mysql) is an RDS instance named <b>jamjar-prod-v3</b> not created within jamjar-staging Elastic Beanstalk, but maintained separately.

## DNS

DNS records are hosted in Route 53 hosted zone in AWS account.

## CICD

Deployments are conducted automatically, via GitLab actions. Branch named `development` is deployed into development environment, branch named `master` is deployed into production environment.

## Remote access

Access to database is available via BastionHost ssh tunnel only. Bastion host is a separate, t2.nano instance which allows access only from a set of whitelisted IP addresses.

Direct access to EC2 instances command line is very convenient, simply please go into EC2 console, click "Connect", choose "Session manager" tab, hit "Connect" bucket an you are in the console directly.

## Additional info

Whole AWS setup was created and is maintained manually in console. No Infrastructure as Code tool like CloudFormation and Terraform was ever used.
