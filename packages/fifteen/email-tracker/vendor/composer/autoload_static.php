<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit20603c32e63f39ce6bffedd261fb5bb2
{
    public static $prefixLengthsPsr4 = array (
        'j' => 
        array (
            'jdavidbakr\\MailTracker\\' => 23,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'jdavidbakr\\MailTracker\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
            1 => __DIR__ . '/../..' . '/tests',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit20603c32e63f39ce6bffedd261fb5bb2::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit20603c32e63f39ce6bffedd261fb5bb2::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
