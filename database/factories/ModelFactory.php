<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(JamJar\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'verified' => 1,
        'matrix_partner' => 0,
        'role_id' => factory('JamJar\Role')->create()->id
    ];
});

$factory->define(JamJar\Profile::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'user_id' => 1,
        'company_id' => 1,
        'address_line_one' => $faker->streetName,
        'address_line_two' => $faker->streetAddress,
        'town' => $faker->city,
        'city' => $faker->city,
        'county' => $faker->state,
        'postcode' => 'TE5T1NG',
        'telephone_number' => '0115 932 5151',
        'mobile_number' => '07777777777'
    ];
});


$factory->define(JamJar\Company::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'user_id' => 1,
        'telephone' => '0115 932 5151',
        'vat_number'    => '1234 56 789',
        'address_line_one'    => $faker->streetName,
        'address_line_two'    => $faker->streetAddress,
        'town'    => $faker->city,
        'city'    => $faker->city,
        'county'    => $faker->state,
        'postcode'    => $faker->postcode,
    ];
});

$factory->define(JamJar\Role::class, function (Faker\Generator $faker) {
    return [
        'name'  => $faker->name
    ];
});

$factory->define(JamJar\PartnerWebsite::class, function (Faker\Generator $faker) {
    $user = factory('JamJar\User')->create();
    $company = factory('JamJar\Company')->create(['user_id' => $user->id]);
    return [
        'company_id'    => $company->id,
        'user_id'       => $user->id,
        'slug'          => str_slug($company->name),
        'logo_image'    => 'partner-websites/images/'.str_slug($company->name).'/logo.jpg',
        'hero_image'    => 'partner-websites/images/'.str_slug($company->name).'/hero.jpg',
        'brand_color'   => '000000',
        'bg_color'  => 'FFFFFF',
    ];
});

$factory->define(JamJar\PartnerPage::class, function (Faker\Generator $faker) {
    $website = factory('JamJar\PartnerWebsite')->create();
    $title = $faker->sentence;
    return [
        'website_id' => $website->id,
        'title' => $title,
        'content' => $faker->paragraph,
        'slug'  => str_slug($title)
    ];
});

$factory->define(JamJar\PartnerLocation::class, function (Faker\Generator $faker) {
    $user = factory('JamJar\User')->create();
    $company = factory('JamJar\Company')->create(['user_id' => $user->id]);
    $city = $faker->city;

    return [
        'user_id' => $user->id,
        'company_id' => $company->id,
        'name'  => 'Nottingham',
        'telephone' => '0115 932 5151',
        'address_line_one' => $faker->streetName,
        'address_line_two' => $faker->streetAddress,
        'town' => $city,
        'city' =>$city,
        'county' => $faker->state,
        'postcode' => 'TE5T1NG',
        'collection_fee' => 0,
        'allow_collection' => 0,
        'dropoff_fee' => 0,
        'allow_dropoff' => 0,
        'primary' => 0
    ];
});


$factory->define(JamJar\Product::class, function (Faker\Generator $faker) {
    return [
        'name'  => $faker->name,
        'sku' => $faker->uuid,
        'description' => $faker->sentence,
        'price' => '56.66',
        'active' => 1,
        'credit_amount' => 0
    ];
});


$factory->define(JamJar\Order::class, function (Faker\Generator $faker) {
    return [
        'product_name' => $faker->name,
        'product_price' => '10000',
        'pp_payment_id' => $faker->uuid,
        'pp_payer_id' => $faker->uuid,
        'user_id' => factory('JamJar\User')->create()->id,
        'status' => 1,
    ];
});

$factory->define(JamJar\Offer::class, function (Faker\Generator $faker) {
    return [
        'name'  => 'Lexus',
        'slug' => $faker->uuid,
        'base_value' => '92',
        'minimum_mileage' => '1',
        'maximum_mileage' => '100000',
        'minimum_value' => '0',
        'maximum_value' => '25000',
        'minimum_service_history' => 'FullFranchise',
        'minimum_mot' => 'SixPlus',
        'maximum_previous_owners' => '1',
        'accept_write_offs' => 0,
        'acceptable_colors' => '1,2,4',
        'acceptable_defects' => '1,2,3',
        'great_condition_percentage_modifier' => '100',
        'good_condition_percentage_modifier' => '80',
        'avg_condition_percentage_modifier' => '60',
        'poor_condition_percentage_modifier' => '40',
        'manufacturer_id' => '1',
        'model_ids' => '1,2,3',
        'derivative_ids' => '1,2,3',
        'active' => 1,
        'fuel_type' => 'Petrol'
    ];
});

$factory->define(JamJar\Valuation::class, function (Faker\Generator $faker) {
    return [];
});
