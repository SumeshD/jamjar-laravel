<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class addLabelForUserStatements extends Migration
{
    public function up()
    {
        Schema::table('user_statements', function (Blueprint $table) {
            $table->string('transaction_label')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::table('user_statements', function (Blueprint $table) {
            $table->dropColumn('transaction_label');
        });
    }
}
