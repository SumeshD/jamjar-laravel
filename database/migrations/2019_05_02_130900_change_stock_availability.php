<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStockAvailability extends Migration
{
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('stock_availability_in_days');
        });

        Schema::table('vehicles', function (Blueprint $table) {
            $table->date('stock_availability_at')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->boolean('stock_availability_in_days')->nullable()->default(null);
        });

        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('stock_availability_at');
        });
    }
}
