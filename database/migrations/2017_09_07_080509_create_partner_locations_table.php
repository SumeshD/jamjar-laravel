<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->string('name');
            $table->string('telephone');
            $table->string('address_line_one');
            $table->string('address_line_two')->nullable();
            $table->string('town')->nullable();
            $table->string('city');
            $table->string('county');
            $table->string('postcode');
            $table->string('collection_fee')->nullable();
            $table->tinyInteger('allow_collection');
            $table->string('dropoff_fee')->nullable();
            $table->tinyInteger('allow_dropoff');
            $table->tinyInteger('primary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_locations');
    }
}
