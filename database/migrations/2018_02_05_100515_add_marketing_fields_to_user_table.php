<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarketingFieldsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('marketing_email')->after('remember_token')->default(0);
            $table->tinyInteger('marketing_telephone')->after('marketing_email')->default(0);
            $table->tinyInteger('marketing_sms')->after('marketing_telephone')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['marketing_email', 'marketing_telephone', 'marketing_sms']);
        });
    }
}
