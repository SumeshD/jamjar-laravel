<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CapValuesForVehicle extends Migration
{
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->integer('cap_value_in_pence')->nullable()->default(null);
            $table->integer('cap_value_below_in_pence')->nullable()->default(null);
            $table->integer('cap_value_clean_in_pence')->nullable()->default(null);
            $table->integer('cap_value_retail_in_pence')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('cap_value_in_pence');
            $table->dropColumn('cap_value_below_in_pence');
            $table->dropColumn('cap_value_clean_in_pence');
            $table->dropColumn('cap_value_retail_in_pence');
        });
    }
}
