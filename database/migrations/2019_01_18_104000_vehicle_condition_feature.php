<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VehicleConditionFeature extends Migration
{
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->integer('vehicle_condition_default_percentage_good')->nullable()->default(null);
            $table->integer('vehicle_condition_default_percentage_fair')->nullable()->default(null);
            $table->integer('vehicle_condition_default_percentage_poor')->nullable()->default(null);
            $table->boolean('valuation_vehicle_condition_show')->default(true);
        });

        $sql = '
            select users.id as id
            from users
            where users.matrix_partner = true;';

        $rows = DB::select($sql);

        foreach ($rows as $row) {
            $profile = \JamJar\Profile::where('user_id', '=', $row->id)->first();

            if (!$profile) {
                continue;
            }

            $profile->vehicle_condition_default_percentage_good = 80;
            $profile->vehicle_condition_default_percentage_fair = 60;
            $profile->vehicle_condition_default_percentage_poor = 40;

            $profile->save();
        }

        Schema::table('valuations', function (Blueprint $table) {
            $table->integer('vehicle_condition_value_good')->nullable()->default(null);
            $table->integer('vehicle_condition_value_fair')->nullable()->default(null);
            $table->integer('vehicle_condition_value_poor')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn('vehicle_condition_default_percentage_good');
            $table->dropColumn('vehicle_condition_default_percentage_fair');
            $table->dropColumn('vehicle_condition_default_percentage_poor');
            $table->dropColumn('valuation_vehicle_condition_show');
        });

        Schema::table('valuations', function (Blueprint $table) {
            $table->dropColumn('vehicle_condition_value_good');
            $table->dropColumn('vehicle_condition_value_fair');
            $table->dropColumn('vehicle_condition_value_poor');
        });
    }
}
