<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('valuation_id')->index()->unsigned();
            $table->integer('company_id')->index()->unsigned()->comment('Buyer');
            $table->integer('user_id')->index()->unsigned()->comment('Seller');
            $table->integer('vehicle_id')->index()->unsigned();
            $table->string('price')->nullable();
            $table->enum('status', ['pending', 'complete', 'rejected']);
            $table->enum('delivery_method', ['collection', 'dropoff']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
