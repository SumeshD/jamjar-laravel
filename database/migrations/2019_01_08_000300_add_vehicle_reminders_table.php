<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVehicleRemindersTable extends Migration
{
    public function up()
    {
        Schema::create('vehicle_reminders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('vehicle_id');
            $table->timestamps();
            $table->timestamp('start_date_of_sending_reminders')->nullable();
        });

        Schema::table('vehicles', function (Blueprint $table) {
            $table->timestamp('start_sending_reminders_at')->nullable();
        });

        DB::update('update vehicles set start_sending_reminders_at = created_at');
    }

    public function down()
    {
        Schema::dropIfExists('vehicle_reminders');

        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('start_sending_reminders_at');
        });
    }
}
