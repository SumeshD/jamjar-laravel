<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesForSessionTable extends Migration
{
    public function up()
    {
        Schema::table('sessions', function(Blueprint $table) {
            $table->index('last_activity');
        });

        Schema::table('vehicle_reminders', function(Blueprint $table) {
            $table->index('vehicle_id');
            $table->index('type');
            $table->index('start_date_of_sending_reminders');
        });

        Schema::table('vehicles', function(Blueprint $table) {
            $table->index('start_sending_reminders_at');
        });
    }

    public function down()
    {
        Schema::table('sessions', function(Blueprint $table) {
            $table->dropIndex('sessions_last_activity_index');
        });

        Schema::table('vehicle_reminders', function(Blueprint $table) {
            $table->dropIndex('vehicle_reminders_vehicle_id_index');
            $table->dropIndex('vehicle_reminders_type_index');
            $table->dropIndex('vehicle_reminders_start_date_of_sending_reminders_index');
        });

        Schema::table('vehicles', function(Blueprint $table) {
            $table->dropIndex('vehicles_start_sending_reminders_at_index');
        });
    }
}
