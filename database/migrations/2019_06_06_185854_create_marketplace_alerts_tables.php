<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketplaceAlertsTables extends Migration
{
    public function up()
    {
        Schema::create('marketplace_alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->boolean('is_enabled')->nullable();
            $table->string('name')->nullable();
            $table->string('frequency')->nullable();
            $table->string('min_plate_year')->nullable();
            $table->integer('number_of_owners')->nullable();
            $table->integer('min_mileage')->nullable();
            $table->integer('max_mileage')->nullable();
            $table->integer('min_value')->nullable();
            $table->integer('max_value')->nullable();
            $table->string('fuel_type')->nullable();
            $table->string('car_condition')->nullable();
            $table->string('seller_type')->nullable();
            $table->timestamps();
        });

        Schema::create('marketplace_alerts_filters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marketplace_alert_id')->nullable();
            $table->timestamps();
        });

        Schema::create('marketplace_alerts_filters_manufacturers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marketplace_alerts_filter_id')->nullable();
            $table->integer('manufacturer_id')->nullable();
            $table->timestamps();
        });

        Schema::create('marketplace_alerts_filters_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marketplace_alerts_filter_id')->nullable();
            $table->integer('model_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('marketplace_alerts');
        Schema::dropIfExists('marketplace_alerts_filters');
        Schema::dropIfExists('marketplace_alerts_filters_manufacturers');
        Schema::dropIfExists('marketplace_alerts_filters_models');
    }
}
