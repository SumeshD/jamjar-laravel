<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_websites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->string('slug');
            $table->string('logo_image')->default('partner-websites/default/jamjar_partner.jpg');
            $table->string('hero_image')->default('partner-websites/default/hero.jpg');
            $table->string('brand_color')->default('FFFFFF');
            $table->string('bg_color')->default('000000');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_websites');
    }
}
