<?php

use Illuminate\Database\Migrations\Migration;

class UpdateCompletedSales extends Migration
{
    public function up()
    {
        $valuations = \JamJar\Valuation::whereNotNull('accepted_at')->get();

        foreach ($valuations as $valuation) {
            $sale = $valuation->sale;
            if ($sale && $sale->status != 'complete') {
                $sale->status = 'complete';
                $sale->save();
            }
        }
    }

    public function down()
    {
    }
}
