<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('products');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::create('products', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name');
        //     $table->string('sku');
        //     $table->text('description');
        //     $table->integer('price');
        //     $table->integer('credit_amount')->unsigned()->default(0);
        //     $table->tinyinteger('active');
        //     $table->timestamps();
        // });
    }
}
