<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class addIndexToVehicleModelManufacturerColumns extends Migration
{
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->index('manufacturer_id');
            $table->index('model_id');
            $table->index('derivative_id');
        });
    }

    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropIndex('manufacturer_id');
            $table->dropIndex('model_id');
            $table->dropIndex('derivative_id');
        });
    }
}
