<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReminderFieldsToValuationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('valuations', function (Blueprint $table) {
            $table->tinyInteger('24h_sms_reminder_sent')->default(0);
            $table->tinyInteger('96h_sms_reminder_sent')->default(0);
            $table->tinyInteger('24h_email_reminder_sent')->default(0);
            $table->tinyInteger('96h_email_reminder_sent')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('valuations', function (Blueprint $table) {
            $table->dropColumn(['24h_sms_reminder_sent', '96h_sms_reminder_sent', '24h_email_reminder_sent', '96h_email_reminder_sent']);
        });
    }
}
