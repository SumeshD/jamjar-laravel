<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createSubscribersTagsChangelogTables extends Migration
{
    public function up()
    {
        Schema::create('subscribers_tags_changelogs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('subscribers_tags_changelog_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('changelog_id');
            $table->string('tag_type');
            $table->string('email');
            $table->string('member_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('subscribers_tags_changelog');
        Schema::dropIfExists('subscribers_tags_changelog_record');
    }
}
