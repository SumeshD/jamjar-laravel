<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalVehicleInformation extends Migration
{
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->integer('asking_price_in_pence')->nullable()->default(null);
            $table->integer('keys_count')->nullable()->default(null);
            $table->string('service_history_information')->nullable()->default(null);
            $table->string('additional_specification_information')->nullable()->default(null);
            $table->boolean('is_log_book')->nullable()->default(null);
            $table->boolean('is_in_stock')->nullable()->default(null);
            $table->boolean('stock_availability_in_days')->nullable()->default(null);
            $table->dateTime('added_to_marketplace_at')->nullable()->default(null);
            $table->integer('seller_fee_in_pence')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('asking_price_in_pence');
            $table->dropColumn('keys_count');
            $table->dropColumn('service_history_information');
            $table->dropColumn('additional_specification_information');
            $table->dropColumn('is_log_book');
            $table->dropColumn('is_in_stock');
            $table->dropColumn('stock_availability_in_days');
            $table->dropColumn('added_to_marketplace_at');
            $table->dropColumn('seller_fee_in_pence');
        });
    }
}
