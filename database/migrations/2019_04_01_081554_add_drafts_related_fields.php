<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDraftsRelatedFields extends Migration
{
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('is_created_by_lookup');
            $table->boolean('is_visible_on_marketplace')->default(true);
        });
    }

    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->boolean('is_created_by_lookup')->default(false);
            $table->dropColumn('is_visible_on_marketplace');
        });
    }
}
