<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDashboardAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboard_alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->text('text');
            $table->enum('status', [0,1,2]);
            $table->index(['status']);
            $table->timestamps();
        });

        Schema::create('accepted_user_dashboard_alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('dashboard_alert_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard_alerts');
        Schema::dropIfExists('accepted_user_dashboard_alerts');
    }
}
