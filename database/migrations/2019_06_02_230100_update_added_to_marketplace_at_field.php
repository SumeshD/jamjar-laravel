<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class UpdateAddedToMarketplaceAtField extends Migration
{
    public function up()
    {
        $vehicles = \JamJar\Vehicle::where('created_at', '>=', Carbon::now()->subMonth())->get();

        foreach ($vehicles as $vehicle) {
            if ($vehicle->is_visible_on_marketplace && !$vehicle->added_to_marketplace_at) {
                $vehicle->added_to_marketplace_at = $vehicle->getCreatedAt();
                $vehicle->save();
            }
        }
    }

    public function down()
    {
    }
}
