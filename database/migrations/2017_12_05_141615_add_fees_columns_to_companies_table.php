<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeesColumnsToCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('valuation_validity')->nullable();
            $table->string('bank_transfer_fee')->nullable();
            $table->string('admin_fee')->nullable();
            $table->string('postcode_area')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn(['valuation_validity', 'bank_transfer_fee', 'admin_fee', 'postcode_area']);
        });
    }
}
