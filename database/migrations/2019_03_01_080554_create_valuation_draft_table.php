<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValuationDraftTable extends Migration
{
    public function up()
    {
        Schema::create('valuation_drafts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_id')->unsigned();
            $table->integer('buyer_company_id')->unsigned();
            $table->integer('price_in_pence');
            $table->string('acceptance_uri')->nullable();
            $table->string('temp_ref')->nullable();
            $table->string('valuation_id')->nullable();
            $table->string('external_partner_request')->nullable();
            $table->string('external_partner_response')->nullable();
            $table->string('valuation_source');
            $table->string('payment_method');
            $table->integer('distance_to_seller_in_miles')->nullable();
            $table->integer('buyer_location_id')->unsigned();
            $table->integer('valuation_validity_in_days');
            $table->timestamps();

            $table->foreign('vehicle_id')->references('id')->on('vehicles');
            $table->foreign('buyer_company_id')->references('id')->on('companies');
            $table->foreign('buyer_location_id')->references('id')->on('partner_locations');
        });

        Schema::create('valuation_draft_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('valuation_draft_id')->unsigned();
            $table->integer('price_in_pence');
            $table->string('vehicle_condition');
            $table->timestamps();

            $table->foreign('valuation_draft_id')->references('id')->on('valuation_drafts');
        });

        Schema::create('valuation_draft_delivery_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('valuation_draft_id')->unsigned();
            $table->string('delivery_method');
            $table->timestamps();

            $table->foreign('valuation_draft_id')->references('id')->on('valuation_drafts');
        });

        Schema::create('valuation_draft_delivery_method_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_method_id')->unsigned();
            $table->string('fee_type');
            $table->integer('fee_amount_in_pence');
            $table->timestamps();

            $table->foreign('delivery_method_id')->references('id')->on('valuation_draft_delivery_methods');
        });

        Schema::table('vehicles', function (Blueprint $table) {
            $table->boolean('is_created_by_lookup')->default(false);
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->boolean('buy_vehicles_from_other_partners')->default(true);
        });
    }

    public function down()
    {
        Schema::dropIfExists('valuation_draft_delivery_method_fees');
        Schema::dropIfExists('valuation_draft_delivery_methods');
        Schema::dropIfExists('valuation_drafts');

        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('is_created_by_lookup');
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('buy_vehicles_from_other_partners');
        });
    }
}
