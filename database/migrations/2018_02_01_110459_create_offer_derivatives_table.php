<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferDerivativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_derivatives', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id')->unsigned()->index();
            $table->integer('cap_model_id')->unsigned()->index();
            $table->integer('cap_derivative_id')->unsigned()->index();
            $table->integer('base_value')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_derivatives');
    }
}
