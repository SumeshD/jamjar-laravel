<?php

use Illuminate\Database\Migrations\Migration;

class UpdateWwacTempRef extends Migration
{
    public function up()
    {
        $valuations = \JamJar\Valuation::get();

        foreach ($valuations as $valuation) {
            if ($valuation->temp_ref != null) {
                continue;
            }

            if (
                strpos($valuation->acceptance_uri, 'wewantanycar') === false &&
                strpos($valuation->acceptance_uri, 'wwac') === false) {
                continue;
            }

            $parts = parse_url($valuation->acceptance_uri);
            parse_str($parts['query'], $query);

            $valuation->temp_ref = $query['id'];
            $valuation->save();
        }
    }

    public function down()
    {
    }
}
