<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVehicleTypeToMarketplaceAlertsTable extends Migration
{
    public function up()
    {
        Schema::table('marketplace_alerts', function (Blueprint $table) {
            $table->string('vehicle_type')->nullable();
        });
    }

    public function down()
    {
        Schema::table('marketplace_alerts', function (Blueprint $table) {
            $table->dropColumn('vehicle_type');
        });
    }
}
