<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToSpeedUpGoodNewsLeads extends Migration
{
    public function up()
    {
        Schema::table('vehicle_owners', function (Blueprint $table) {
            $table->index('vehicle_id');
        });

        Schema::table('vehicle_meta', function (Blueprint $table) {
            $table->index('vehicle_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_owners', function (Blueprint $table) {
            $table->dropIndex('vehicle_id');
        });

        Schema::table('vehicle_meta', function (Blueprint $table) {
            $table->dropIndex('vehicle_id');
        });
    }
}
