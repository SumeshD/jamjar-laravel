<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadedImagesTable extends Migration
{
    public function up()
    {
        Schema::create('uploaded_images', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('original_name', 1024);
            $table->string('status', 128);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('uploaded_images');
    }
}
