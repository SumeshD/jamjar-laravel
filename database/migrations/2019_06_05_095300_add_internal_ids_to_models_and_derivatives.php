<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class AddInternalIdsToModelsAndDerivatives extends Migration
{
    /**
     * there may be more than one the same cap_model_id or cap_vehicle_derivative_id so we have to store data a little bit different
     */
    public function up()
    {
        Schema::table('vehicle_models', function (Blueprint $table) {
            $table->integer('internal_manufacturer_id')->nullable();
        });

        Schema::table('vehicle_derivatives', function (Blueprint $table) {
            $table->integer('internal_model_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('vehicle_models', function (Blueprint $table) {
            $table->dropColumn('internal_manufacturer_id');
        });

        Schema::table('vehicle_derivatives', function (Blueprint $table) {
            $table->dropColumn('internal_model_id');
        });
    }
}
