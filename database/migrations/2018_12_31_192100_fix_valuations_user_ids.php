<?php

use Illuminate\Database\Migrations\Migration;

class FixValuationsUserIds extends Migration
{
    public function up()
    {
        $sql = '
            select valuations.id as valuation_id, vehicle_owners.user_id as correct_user_id, valuations.user_id as current_user_id, profiles.name
            from valuations
               join users on valuations.user_id = users.id
               join profiles on profiles.user_id = users.id
               join vehicles on vehicles.id = valuations.vehicle_id
               join vehicle_owners on vehicle_owners.vehicle_id = vehicles.id
            where profiles.company_id is not null and vehicle_owners.user_id != valuations.user_id;';

        $rows = DB::select($sql);

        foreach ($rows as $row) {
            $valuation = \JamJar\Valuation::where('id', '=', $row->valuation_id)->first();

            $valuation->user_id = $row->correct_user_id;
            $valuation->save();
        }
    }

    public function down()
    {
    }
}
