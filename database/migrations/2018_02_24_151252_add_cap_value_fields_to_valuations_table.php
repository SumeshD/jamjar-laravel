<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCapValueFieldsToValuationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('valuations', function (Blueprint $table) {
            $table->string('cap_value_retail')->after('cap_value')->nullable();
            $table->string('cap_value_clean')->after('cap_value')->nullable();
            $table->string('cap_value_below')->after('cap_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('valuations', function (Blueprint $table) {
            $table->dropColumn(['cap_value_below', 'cap_value_clean', 'cap_value_retail']);
        });
    }
}
