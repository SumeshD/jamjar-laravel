<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreMetaFieldsToVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_meta', function (Blueprint $table) {
            $table->string('body_type')->nullable();
            $table->integer('doors')->nullable();
            $table->string('engine_capacity')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_meta', function (Blueprint $table) {
            $table->dropColumn(['body_type', 'doors', 'engine_capacity']);
        });
    }
}
