<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestedValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggested_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('percentage');
            $table->integer('previous_percentage')->nullable();
            $table->integer('cap_model_id')->index();
            $table->integer('cap_derivative_id')->index();
            // $table->integer('vehicle_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggested_values');
    }
}
