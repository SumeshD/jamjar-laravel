<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedByAutomatedBidsField extends Migration
{
    public function up()
    {
        Schema::table('valuations', function (Blueprint $table) {
            $table->boolean('is_created_by_automated_bids')->default(false);
        });
    }

    public function down()
    {
        Schema::table('valuations', function (Blueprint $table) {
            $table->dropColumn('is_created_by_automated_bids');
        });
    }
}
