<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class changeResponseAndRequestFieldType extends Migration
{
    public function up()
    {
        Schema::table('valuation_drafts', function (Blueprint $table) {
            $table->text('external_partner_request')->change();
            $table->text('external_partner_response')->change();
        });
    }

    public function down()
    {
        Schema::table('valuation_drafts', function (Blueprint $table) {
            $table->string('external_partner_request')->change();
            $table->string('external_partner_response')->change();
        });
    }
}
