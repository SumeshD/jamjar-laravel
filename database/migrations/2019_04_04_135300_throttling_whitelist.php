<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThrottlingWhitelist extends Migration
{
    public function up()
    {
        Schema::create('throttling_whitelist_ips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('throttling_whitelist_ips');
    }
}
