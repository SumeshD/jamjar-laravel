<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('name');
            $table->uuid('slug')->unique()->index();
            $table->integer('base_value');
            $table->integer('minimum_mileage');
            $table->integer('maximum_mileage');
            $table->integer('minimum_value');
            $table->integer('maximum_value');
            $table->string('minimum_service_history');
            $table->string('minimum_mot');
            $table->integer('maximum_previous_owners');
            $table->boolean('accept_write_offs')->default(0);
            $table->longtext('acceptable_colors')->comment('Comma Separated Values of ids from colours table');
            $table->longtext('acceptable_defects')->comment('Comma Separated Values of ids from defects table');
            $table->integer('great_condition_percentage_modifier');
            $table->integer('good_condition_percentage_modifier');
            $table->integer('avg_condition_percentage_modifier');
            $table->integer('poor_condition_percentage_modifier');
            $table->integer('manufacturer_id')->nullable()->index();
            $table->longtext('model_ids')->nullable()->comment('Comma Separated Values of ids from vehicle_models table');
            $table->longtext('derivative_ids')->nullable()->comment('Comma Separated Values of ids from vehicle_models table');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
