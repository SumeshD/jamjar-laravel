<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ValuationIsClickedField extends Migration
{
    public function up()
    {
        Schema::table('valuations', function (Blueprint $table) {
            $table->dateTime('accept_button_clicked_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('valuations', function (Blueprint $table) {
            $table->dropColumn('accept_button_clicked_at');
        });
    }
}
