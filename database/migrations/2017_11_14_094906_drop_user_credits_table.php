<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUserCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_credits');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::create('user_credits', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('user_id')->unsigned()->index();
        //     $table->integer('available_credits')->unsigned()->index();
        //     $table->integer('spent_credits')->unsigned();
        //     $table->timestamps();
        // });
    }
}
