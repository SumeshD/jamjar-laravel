<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_id')->nullable();
            $table->string('manufacturer')->nullable();
            $table->string('model')->nullable();
            $table->string('derivative')->nullable();
            $table->integer('color')->nullable();
            $table->string('transmission')->nullable();
            $table->string('plate_year')->nullable();
            $table->string('fuel_type')->nullable();
            $table->string('registration_date')->nullable();
            $table->string('mileage')->nullable();
            $table->integer('number_of_owners')->nullable();
            $table->string('service_history')->nullable();
            $table->string('mot')->nullable();
            $table->boolean('write_off')->default(0);
            $table->boolean('non_runner')->default(0);
            $table->longtext('non_runner_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_meta');
    }
}
