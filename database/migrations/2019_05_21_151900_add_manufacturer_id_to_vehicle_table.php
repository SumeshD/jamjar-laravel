<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class AddManufacturerIdToVehicleTable extends Migration
{
    /**
     * @note there are string fields manufacturer, model and derivative in the vehicle_meta table
     * these fields should be removed later
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->integer('manufacturer_id')->nullable();
            $table->integer('model_id')->nullable();
            $table->integer('derivative_id')->nullable();
        });

        $vehicles = \JamJar\Vehicle::where('created_at', '>', Carbon::now()->subMonth())->get();

        foreach ($vehicles as $vehicle) {
            $capModelId = $vehicle->meta->cap_model_id;
            $vehicleModel = \JamJar\VehicleModel::where('cap_model_id', '=', $vehicle->meta->cap_model_id)->first();

            if (!$vehicleModel) {
                continue;
            }

            $vehicleManufacturer = $vehicleModel->manufacturer;
            $vehicleDerivative = \JamJar\VehicleDerivative::where([
                ['model_id', '=', $capModelId],
                ['name', '=', $vehicle->meta->derivative]
            ])->first();

            if ($vehicleManufacturer) {
                $vehicle->manufacturer_id = $vehicleManufacturer->id;
            }

            if ($vehicleModel) {
                $vehicle->model_id = $vehicleModel->id;
            }

            if ($vehicleDerivative) {
                $vehicle->derivative_id = $vehicleDerivative->id;
            }

            $vehicle->save();
        }
    }

    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('manufacturer_id');
            $table->dropColumn('model_id');
            $table->dropColumn('derivative_id');
        });
    }
}
