<?php

use Illuminate\Database\Seeder;
use JamJar\FundingAmount;

class FundingAmountSeeder extends Seeder
{
    protected $toInsert = [
        [
            'price' => '250',
            'active' => 1
        ],
        [
            'price' => '350',
            'active' => 1
        ],
        [
            'price' => '500',
            'active' => 1
        ],
        [
            'price' => '750',
            'active' => 1
        ],
        [
            'price' => '1000',
            'active' => 1
        ],
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->toInsert as $toInsert) {
            FundingAmount::create($toInsert);
        }
    }
}
