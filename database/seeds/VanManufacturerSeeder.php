<?php

use JamJar\Api\CapApi;
use Illuminate\Database\Seeder;
use JamJar\VehicleManufacturer;

class VanManufacturerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $api = new CapApi;

        $manufacturers = $api->getManufacturers('LCV');

        foreach ($manufacturers as $manufacturer) {
            VehicleManufacturer::updateOrCreate($manufacturer->toArray());
        }
    }
}
