<?php

use Illuminate\Database\Seeder;
use JamJar\PlateYear;

class PlateYearSeed extends Seeder
{
    public $toInsert = [
        '1983 A',
        '1984 B',
        '1985 C',
        '1986 D',
        '1987 E',
        '1988 F',
        '1989 G',
        '1990 H',
        '1991 J',
        '1992 K',
        '1993 L',
        '1994 M',
        '1995 N',
        '1996 P',
        '1997 R',
        '1998 S',
        '1999 T',
        '1999 V',
        '2000 W',
        '2000 X',
        '2001 Y',
        '2001 01',
        '2001 51',
        '2002 51',
        '2002 02',
        '2002 52',
        '2003 52',
        '2003 03',
        '2003 53',
        '2004 53',
        '2004 04',
        '2004 54',
        '2005 54',
        '2005 05',
        '2005 55',
        '2006 55',
        '2006 06',
        '2006 56',
        '2007 56',
        '2007 07',
        '2007 57',
        '2008 57',
        '2008 08',
        '2008 58',
        '2009 58',
        '2009 09',
        '2009 59',
        '2010 59',
        '2010 10',
        '2010 60',
        '2011 60',
        '2011 11',
        '2011 61',
        '2012 61',
        '2012 12',
        '2012 62',
        '2013 62',
        '2013 13',
        '2013 63',
        '2014 63',
        '2014 14',
        '2014 64',
        '2015 64',
        '2015 15',
        '2015 65',
        '2016 65',
        '2016 16',
        '2016 66',
        '2017 66',
        '2017 17',
        '2017 67',
        '2018 67',
        '2018 18',
        '2018 68',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->toInsert as $toInsert) {
            $parts = explode(' ', $toInsert);

            $year = PlateYear::where('plate_year', '=', $parts[0])->where('plate_number', '=', $parts[1]);
            if ($year->exists()) {
                continue;
            }

            PlateYear::create([
                'plate_year'    => $parts[0],
                'plate_number'  => $parts[1]
            ]);
        }
    }
}
