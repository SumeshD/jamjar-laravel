<?php

use Illuminate\Database\Seeder;
use JamJar\Defect;

class DefectSeeder extends Seeder
{
    public $toInsert = [
        'Alternator',
        'Axle',
        'Battery',
        'Brakes',
        'Cam Belt',
        'Clutch',
        'Corrosion/Excessive Rust',
        'Driveshaft',
        'Electrical',
        'Engine Seized',
        'Gearbox',
        'Head Gasket',
        'Power Steering',
        'Steering',
        'Suspension',
        'Timing Chain',
        'Turbo',
        'Tyres',
        'Water/Flood Damage',
        'Water Pump',
        'No Tax',
        'No Valid Insurance',
        'SORN'
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->toInsert as $toInsert) {
            $defect = Defect::where('title', '=', $toInsert);
            if ($defect->exists()) {
                continue;
            }

            Defect::create([
                'title'    =>    $toInsert
            ]);
        }
    }
}
