<?php

use JamJar\Api\CapApi;
use JamJar\VehicleManufacturer;
use Illuminate\Database\Seeder;

class VehicleManufacturerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $api = new CapApi;

        $manufacturers = $api->getManufacturers();

        foreach ($manufacturers as $manufacturer) {
            VehicleManufacturer::updateOrCreate($manufacturer->toArray());
        }
    }
}
