<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use JamJar\User;

class UserSeeder extends Seeder
{

    public $toInsert = [
        [
            'email' =>  'alex.lee+test@impression.co.uk',
            'name'  =>  'Joe Hubbard',
            'role_id'   => 1,
            'verified'  => 1,
            'credits'   => 0,
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->toInsert as $toInsert) {
            $user = User::where('email', '=', $toInsert['email']);

            if ($user->exists()) {
                continue;
            }

            $password = str_random(8);

            Mail::raw(

                '
                Please log in to '.env('APP_URL').' with your email address: '.$toInsert['email'].'
                Your temporary password is: ' . $password,

                function ($message) use ($toInsert) {
                    $message->to($toInsert['email'])
                    ->subject('jamjar.com Installed');
                }
            );

            User::create([
                'email' =>  $toInsert['email'],
                'name' =>  $toInsert['name'],
                'password' =>  Hash::make($password),
                'role_id' => $toInsert['role_id'],
                'verified' => $toInsert['verified'],
            ]);
        }
    }
}
