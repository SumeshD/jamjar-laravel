<?php

use JamJar\User;
use Illuminate\Database\Seeder;

class WBCTSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = User::where('email', 'joe.hubbard@impression.co.uk')->first();

        $wbct = $user->company()->create([
			'name'  => 'WeBuyCarsToday',
			'telephone' => '0207 183 9263',
			'vat_number' => '000 0000 00',
			'address_line_one' => 'WBOC LTD t/a WeBuyCarsToday',
			'address_line_two' => '36, 88-90 Hatton Garden',
			'town' => 'London',
			'city' => 'London',
			'county' => 'London',
			'postcode' => 'EC1N 8PG',
			'company_type' => 'api'
        ]);

        $wbct->location()->create([
            'name' => 'Various',
            'telephone' => '0207 183 9263',
            'address_line_one' => 'WBOC LTD t/a WeBuyCarsToday',
			'address_line_two' => '36, 88-90 Hatton Garden',
            'city' => 'London',
            'county' => 'London',
            'postcode' => 'EC1N 8PG',
            'allow_collection' => false,
            'allow_dropoff' => true,
            'primary' => true,
            'user_id' => $user->id,
        ]);
    }
}
