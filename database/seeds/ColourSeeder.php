<?php

use Illuminate\Database\Seeder;
use JamJar\Colour;

class ColourSeeder extends Seeder
{
    public $toInsert = [
        'White',
        'Black',
        'Grey',
        'Blue',
        'Red',
        'Silver',
        'Green',
        'Orange',
        'Brown',
        'Yellow',
        'Gold',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->toInsert as $toInsert) {
            $color = Colour::where('title', '=', $toInsert);
            if ($color->exists()) {
                continue;
            }
            
            Colour::create([
                'title'    =>    $toInsert
            ]);
        }
    }
}
