<?php

use Illuminate\Database\Seeder;
use JamJar\User;

class M4YMSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'joe.hubbard@impression.co.uk')->first();

        $m4ym = $user->company()->create([
			'name'  => 'Money4yourMotors',
			'telephone' => '01516669980',
			'vat_number' => '000 0000 00',
			'address_line_one' => 'Money4yourMotors.com Ltd ',
			'address_line_two' => 'Urban Quay, 60 Hamilton Square',
			'town' => 'Birkenhead',
			'city' => 'Birkenhead',
			'county' => 'Merseyside',
			'postcode' => 'CH41 5AT',
			'company_type' => 'api'
        ]);

        $m4ym->location()->create([
            'name' => 'Various',
            'telephone' => '01516669980',
            'address_line_one' => 'Urban Quay, 60 Hamilton Square',
            'city' => 'Birkenhead',
            'county' => 'Birkenhead',
            'postcode' => 'Merseyside',
            'allow_collection' => true,
            'allow_dropoff' => true,
            'primary' => true,
            'user_id' => $user->id,
        ]);
    }
}
