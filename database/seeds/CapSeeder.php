<?php

use Illuminate\Database\Seeder;
use JamJar\Api\CapApi;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;
use JamJar\VehicleDerivative;
use Carbon\Carbon;

class CapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $api = new CapApi;

        // ALL
        $manufacturers = VehicleManufacturer::get();
        foreach ($manufacturers as $manufacturer) {
            try {
                $models = $api->getModels($manufacturer->cap_manufacturer_id, $manufacturer->vehicle_type);
            } catch (Exception $e) {
                echo "problem with -> ". $manufacturer->cap_manufacturer_id . ' => ' . $manufacturer->vehicle_type ."\r\n";
                continue;
            }

            foreach ($models as $model) {
                $fuelType = str_contains($model->get('name'), 'DIESEL') ? 'Diesel' : 'Petrol';

                $introduced = $model->get('introduced');
                $discontinued = $model->get('discontinued') == 0 ? date('Y') : $model->get('discontinued');
                $plateYears = range($introduced, $discontinued);

                $vehicleModel = VehicleModel::where([
                    ['cap_model_id', '=', $model->get('cap_model_id')],
                    ['internal_manufacturer_id', '=', $manufacturer->id],
                ])->first();

                if (!$vehicleModel) {
                    $vehicleModel = VehicleModel::where([
                            ['cap_model_id', '=', $model->get('cap_model_id')],
                            ['manufacturer_id', '=', (int) $manufacturer->cap_manufacturer_id],
                            ['internal_manufacturer_id', '=', null]
                        ])
                        ->first();
                }

                $vehicleModelParameters = [
                    'cap_model_id' => $model->get('cap_model_id'),
                    'internal_manufacturer_id' => $manufacturer->id,
                    'name' => $model->get('name'),
                    'manufacturer_id' => (int) $manufacturer->cap_manufacturer_id,
                    'introduced_date' => $model->get('introduced'),
                    'discontinued_date' => $model->get('discontinued'),
                    'fuel_type' => $fuelType,
                    'plate_years' => implode(',', $plateYears),
                    'updated_at' => \Carbon\Carbon::now()
                ];

                if ($vehicleModel) {
                    echo "updating model!\r\n";
                    $vehicleModel->update($vehicleModelParameters);
                } else {
                    echo "creating new model!\r\n";
                    $vehicleModel = new VehicleModel();
                    $vehicleModel->fill($vehicleModelParameters)->save();
                }

                // if any derivative of this model has been updated this month -> just skip
                $lastUpdatedDerivative = VehicleDerivative::where([
                        ['internal_model_id', '=', $vehicleModel->getId()],
                        ['updated_at', '>=', Carbon::now()->format('Y-m-01 00:00:00')],
                    ])
                    ->first();

                if ($lastUpdatedDerivative) {
                    echo "skip all derivatives for model id -> ". $model->get('cap_model_id') ." they have been updated already this month \r\n";
                    continue;
                }

                try {
                    $derivs = $api->getDerivatives($model->get('cap_model_id'), $manufacturer->vehicle_type);
                } catch (Exception $e) {
                    echo "problem with CAP API for derivatives, skip!\r\n";
                    continue;
                }

                foreach ($derivs as $derivative) {
                    $vehicleDerivative = VehicleDerivative::where([
                        ['cap_derivative_id', '=', $derivative->get('cap_derivative_id')],
                        ['internal_model_id', '=', $vehicleModel->getId()],
                    ])->first();

                    if (!$vehicleDerivative) {
                        $vehicleDerivative = VehicleDerivative::where([
                                ['cap_derivative_id', '=', $derivative->get('cap_derivative_id')],
                                ['model_id', '=', $vehicleModel->getCapModelId()],
                                ['internal_model_id', '=', null],
                            ])
                            ->first();
                    }

                    $vehicleDerivativeParameters = [
                        'cap_derivative_id' => $derivative->get('cap_derivative_id'),
                        'internal_model_id' => $vehicleModel->getId(),
                        'name' => $derivative->get('name'),
                        'model_id' => $model->get('cap_model_id'),
                        'updated_at' => \Carbon\Carbon::now(),
                    ];

                    if ($vehicleDerivative) {
                        echo "updating derivative!\r\n";
                        $vehicleDerivative->update($vehicleDerivativeParameters);
                    } else {
                        echo "creating new derivative!\r\n";
                        (new VehicleDerivative())->fill($vehicleDerivativeParameters)->save();
                    }

                    echo "{$manufacturer->name} - {$model->get('name')} - {$derivative->get('name')} added.\r\n";
                }
                echo "Sleeping for 200ms\r\n";
                usleep(200000);
            }

            echo "Sleeping for 200s\r\n";
            usleep(200000);
        }
    }
}
