<?php

use Illuminate\Database\Seeder;
use JamJar\Setting;

class SettingSeeder extends Seeder
{
    protected $defaultItems = [
        'site_name' => 'jamjar.com',
        'site_version' => '3.0.0a',
        'cap_api_username' => '',
        'cap_api_password' => '',
        'copart_api_username' => '',
        'copart_api_password' => '',
        'cartakeback_api_username' => '',
        'cartakeback_api_password' => '',
        'matrix_partner_admin_name' => '',
        'matrix_partner_admin_email' => '',
        'jamjar_address_line_one' => '',
        'jamjar_address_line_two' => '',
        'jamjar_address_town'  => '',
        'jamjar_address_city' => '',
        'jamjar_address_county' => '',
        'jamjar_address_postcode' => ''
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->defaultItems as $key => $value) {
            Setting::create([
                    'key'       => $key,
                    'value'     => $value,
            ]);
        }
    }
}
