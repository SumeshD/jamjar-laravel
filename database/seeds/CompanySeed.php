<?php

use Illuminate\Database\Seeder;
use JamJar\User;

class CompanySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'joe.hubbard@impression.co.uk')->first();

        $ctb = $user->company()->create([
            'name'  => 'CarTakeBack',
            'telephone' => '0330 066 95 85',
            'vat_number' => '855 6670 87',
            'address_line_one' => 'CarTakeBack.com Limited',
            'address_line_two' => 'Bankfield House, Regent Road',
            'town' => 'Liverpool',
            'city' => 'Liverpool',
            'county' => 'Liverpool',
            'postcode' => 'L20 8RQ',
            'company_type' => 'api'
        ]);

        $ctb->location()->create([
            'name' => 'Various',
            'telephone' => '0330 066 95 85',
            'address_line_one' => 'Nationwide',
            'city' => 'Nationwide',
            'county' => 'Nationwide',
            'postcode' => 'Nationwide',
            'allow_collection' => true,
            'allow_dropoff' => true,
            'primary' => true,
            'user_id' => $user->id,
        ]);
    }
}
