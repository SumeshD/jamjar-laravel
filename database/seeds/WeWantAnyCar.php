<?php

use JamJar\User;
use Illuminate\Database\Seeder;

class WeWantAnyCar extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'joe.hubbard@impression.co.uk')->first();

        $wwac = $user->company()->create([
            'name'  => 'WeWantAnyCar',
            'telephone' => '08436450000',
            'vat_number' => '000 0000 00',
            'address_line_one' => 'We Want Any Car, Airport House Business Centre',
            'address_line_two' => 'Purley Way',
            'town' => 'Croydon',
            'city' => 'London',
            'county' => 'London',
            'postcode' => 'CR00XZ',
            'company_type' => 'api'
        ]);

        $wwac->location()->create([
            'name' => 'Various',
            'telephone' => '08436450000',
            'address_line_one' => 'WeWantAnyCar, Airport House Business Centre',
            'address_line_two' => 'Purley Way',
            'city' => 'London',
            'county' => 'London',
            'postcode' => 'CR00XZ',
            'allow_collection' => false,
            'allow_dropoff' => true,
            'primary' => true,
            'user_id' => $user->id,
        ]);
    }
}
