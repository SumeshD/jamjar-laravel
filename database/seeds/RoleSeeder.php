<?php

use Illuminate\Database\Seeder;
use JamJar\Role;

class RoleSeeder extends Seeder
{
    public $toInsert = [
        'Administrator',
        'Staff',
        'Partner',
        'Client'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->toInsert as $toInsert) {
            $role = Role::where('name', '=', $toInsert);
            if ($role->exists()) {
                continue;
            }

            Role::create([
                'name'    =>    $toInsert
            ]);
        }
    }
}
