<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->command->info('Creating Basic Immutable Data');
        $this->call(DefectSeeder::class);
        $this->call(ColourSeeder::class);
        $this->call(SettingSeeder::class);
    	$this->command->info('Creating Users and Roles');
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(FundingAmountSeeder::class);
    	$this->command->info('Creating API Company Records');
        $this->call(CompanySeed::class);
        $this->call(M4YMSeed::class);
        $this->call(WBCTSeed::class);
    	$this->command->info('Creating Default Plate Years');
        $this->call(PlateYearSeed::class);
    	$this->command->info('Creating All Postcodes');
        $this->call(PostcodesTableSeeder::class);
    	$this->command->info('Contacting CAP to get all Vehicle Manufacturers');
        $this->call(VehicleManufacturerSeeder::class);
    	$this->command->info('Done! Next up please run php artisan db:seed --class=CapSeeder to get all models and derivatives from CAP.');
    	$this->command->info('This may take a while to run, and may stop based on how busy CAP\'s API is.');

    }
}
