<?php

use Illuminate\Database\Seeder;

class PostcodesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('postcodes')->delete();
        
        \DB::table('postcodes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'postcode' => 'AB',
                'county' => 'Aberdeen',
                'country' => 'Scotland',
                'lat' => '57.301',
                'lng' => '-2.30789',
            ),
            1 => 
            array (
                'id' => 2,
                'postcode' => 'AL',
                'county' => 'St Albans',
                'country' => 'England',
                'lat' => '51.7755',
                'lng' => '-0.284023',
            ),
            2 => 
            array (
                'id' => 3,
                'postcode' => 'B',
                'county' => 'Birmingham',
                'country' => 'England',
                'lat' => '52.4652',
                'lng' => '-1.88888',
            ),
            3 => 
            array (
                'id' => 4,
                'postcode' => 'BA',
                'county' => 'Bath',
                'country' => 'England',
                'lat' => '51.2295',
                'lng' => '-2.41725',
            ),
            4 => 
            array (
                'id' => 5,
                'postcode' => 'BB',
                'county' => 'Blackburn',
                'country' => 'England',
                'lat' => '53.7871',
                'lng' => '-2.33373',
            ),
            5 => 
            array (
                'id' => 6,
                'postcode' => 'BD',
                'county' => 'Bradford',
                'country' => 'England',
                'lat' => '53.8286',
                'lng' => '-1.82852',
            ),
            6 => 
            array (
                'id' => 7,
                'postcode' => 'BH',
                'county' => 'Bournemouth',
                'country' => 'England',
                'lat' => '50.7504',
                'lng' => '-1.89081',
            ),
            7 => 
            array (
                'id' => 8,
                'postcode' => 'BL',
                'county' => 'Bolton',
                'country' => 'England',
                'lat' => '53.5868',
                'lng' => '-2.40784',
            ),
            8 => 
            array (
                'id' => 9,
                'postcode' => 'BN',
                'county' => 'Brighton',
                'country' => 'England',
                'lat' => '50.8288',
                'lng' => '-0.119797',
            ),
            9 => 
            array (
                'id' => 10,
                'postcode' => 'BR',
                'county' => 'Bromley',
                'country' => 'England',
                'lat' => '51.3945',
                'lng' => '0.0424774',
            ),
            10 => 
            array (
                'id' => 11,
                'postcode' => 'BS',
                'county' => 'Bristol',
                'country' => 'England',
                'lat' => '51.4488',
                'lng' => '-2.62257',
            ),
            11 => 
            array (
                'id' => 12,
                'postcode' => 'BT',
                'county' => 'Belfast',
                'country' => 'Northern Ireland',
                'lat' => '54.6035',
                'lng' => '-6.38083',
            ),
            12 => 
            array (
                'id' => 13,
                'postcode' => 'CA',
                'county' => 'Carlisle',
                'country' => 'England',
                'lat' => '54.7317',
                'lng' => '-3.10889',
            ),
            13 => 
            array (
                'id' => 14,
                'postcode' => 'CB',
                'county' => 'Cambridge',
                'country' => 'England',
                'lat' => '52.2066',
                'lng' => '0.189093',
            ),
            14 => 
            array (
                'id' => 15,
                'postcode' => 'CF',
                'county' => 'Cardiff',
                'country' => 'Wales',
                'lat' => '51.5344',
                'lng' => '-3.29351',
            ),
            15 => 
            array (
                'id' => 16,
                'postcode' => 'CH',
                'county' => 'Chester',
                'country' => 'England',
                'lat' => '53.2905',
                'lng' => '-3.02022',
            ),
            16 => 
            array (
                'id' => 17,
                'postcode' => 'CM',
                'county' => 'Chelmsford',
                'country' => 'England',
                'lat' => '51.7578',
                'lng' => '0.404361',
            ),
            17 => 
            array (
                'id' => 18,
                'postcode' => 'CO',
                'county' => 'Colchester',
                'country' => 'England',
                'lat' => '51.9031',
                'lng' => '0.924431',
            ),
            18 => 
            array (
                'id' => 19,
                'postcode' => 'CR',
                'county' => 'Croydon',
                'country' => 'England',
                'lat' => '51.356',
                'lng' => '-0.103134',
            ),
            19 => 
            array (
                'id' => 20,
                'postcode' => 'CT',
                'county' => 'Canterbury',
                'country' => 'England',
                'lat' => '51.2558',
                'lng' => '1.23163',
            ),
            20 => 
            array (
                'id' => 21,
                'postcode' => 'CV',
                'county' => 'Coventry',
                'country' => 'England',
                'lat' => '52.3678',
                'lng' => '-1.50138',
            ),
            21 => 
            array (
                'id' => 22,
                'postcode' => 'CW',
                'county' => 'Crewe',
                'country' => 'England',
                'lat' => '53.1551',
                'lng' => '-2.45013',
            ),
            22 => 
            array (
                'id' => 23,
                'postcode' => 'DA',
                'county' => 'Dartford',
                'country' => 'England',
                'lat' => '51.4422',
                'lng' => '0.220124',
            ),
            23 => 
            array (
                'id' => 24,
                'postcode' => 'DD',
                'county' => 'Dundee',
                'country' => 'Scotland',
                'lat' => '56.5369',
                'lng' => '-2.86448',
            ),
            24 => 
            array (
                'id' => 25,
                'postcode' => 'DE',
                'county' => 'Derby',
                'country' => 'England',
                'lat' => '52.9333',
                'lng' => '-1.49094',
            ),
            25 => 
            array (
                'id' => 26,
                'postcode' => 'DG',
                'county' => 'Dumfries / Dumfries and Galloway',
                'country' => 'Scotland',
                'lat' => '55.0226',
                'lng' => '-3.88091',
            ),
            26 => 
            array (
                'id' => 27,
                'postcode' => 'DH',
                'county' => 'Durham',
                'country' => 'England',
                'lat' => '54.825',
                'lng' => '-1.61406',
            ),
            27 => 
            array (
                'id' => 28,
                'postcode' => 'DL',
                'county' => 'Darlington',
                'country' => 'England',
                'lat' => '54.533',
                'lng' => '-1.6601',
            ),
            28 => 
            array (
                'id' => 29,
                'postcode' => 'DN',
                'county' => 'Doncaster',
                'country' => 'England',
                'lat' => '53.5412',
                'lng' => '-0.719872',
            ),
            29 => 
            array (
                'id' => 30,
                'postcode' => 'DT',
                'county' => 'Dorchester',
                'country' => 'England',
                'lat' => '50.7548',
                'lng' => '-2.48307',
            ),
            30 => 
            array (
                'id' => 31,
                'postcode' => 'DY',
                'county' => 'Dudley',
                'country' => 'England',
                'lat' => '52.4536',
                'lng' => '-2.17686',
            ),
            31 => 
            array (
                'id' => 32,
                'postcode' => 'E',
                'county' => 'London E',
                'country' => 'England',
                'lat' => '51.5435',
                'lng' => '-0.0157977',
            ),
            32 => 
            array (
                'id' => 33,
                'postcode' => 'EC',
                'county' => 'London EC',
                'country' => 'England',
                'lat' => '51.5187',
                'lng' => '-0.0967189',
            ),
            33 => 
            array (
                'id' => 34,
                'postcode' => 'EH',
                'county' => 'Edinburgh',
                'country' => 'Scotland',
                'lat' => '55.9266',
                'lng' => '-3.22798',
            ),
            34 => 
            array (
                'id' => 35,
                'postcode' => 'EN',
                'county' => 'Enfield',
                'country' => 'England',
                'lat' => '51.6817',
                'lng' => '-0.0862893',
            ),
            35 => 
            array (
                'id' => 36,
                'postcode' => 'EX',
                'county' => 'Exeter',
                'country' => 'England',
                'lat' => '50.8267',
                'lng' => '-3.65584',
            ),
            36 => 
            array (
                'id' => 37,
                'postcode' => 'FK',
                'county' => 'Falkirk and Stirling',
                'country' => 'Scotland',
                'lat' => '56.0772',
                'lng' => '-3.85932',
            ),
            37 => 
            array (
                'id' => 38,
                'postcode' => 'FY',
                'county' => 'Blackpool / Fylde',
                'country' => 'England',
                'lat' => '53.825',
                'lng' => '-3.02281',
            ),
            38 => 
            array (
                'id' => 39,
                'postcode' => 'G',
                'county' => 'Glasgow',
                'country' => 'Scotland',
                'lat' => '55.8688',
                'lng' => '-4.26848',
            ),
            39 => 
            array (
                'id' => 40,
                'postcode' => 'GL',
                'county' => 'Gloucester',
                'country' => 'England',
                'lat' => '51.8368',
                'lng' => '-2.17637',
            ),
            40 => 
            array (
                'id' => 41,
                'postcode' => 'GU',
                'county' => 'Guildford',
                'country' => 'England',
                'lat' => '51.2318',
                'lng' => '-0.717089',
            ),
            41 => 
            array (
                'id' => 42,
                'postcode' => 'GY',
                'county' => 'Guernsey',
                'country' => 'Channel Islands',
                'lat' => '',
                'lng' => '',
            ),
            42 => 
            array (
                'id' => 43,
                'postcode' => 'HA',
                'county' => 'Harrow',
                'country' => 'England',
                'lat' => '51.5853',
                'lng' => '-0.336115',
            ),
            43 => 
            array (
                'id' => 44,
                'postcode' => 'HD',
                'county' => 'Huddersfield',
                'country' => 'England',
                'lat' => '53.6302',
                'lng' => '-1.78273',
            ),
            44 => 
            array (
                'id' => 45,
                'postcode' => 'HG',
                'county' => 'Harrogate',
                'country' => 'England',
                'lat' => '54.0373',
                'lng' => '-1.54634',
            ),
            45 => 
            array (
                'id' => 46,
                'postcode' => 'HP',
                'county' => 'Hemel Hempstead',
                'country' => 'England',
                'lat' => '51.7241',
                'lng' => '-0.691033',
            ),
            46 => 
            array (
                'id' => 47,
                'postcode' => 'HR',
                'county' => 'Hereford',
                'country' => 'England',
                'lat' => '52.0657',
                'lng' => '-2.71933',
            ),
            47 => 
            array (
                'id' => 48,
                'postcode' => 'HS',
                'county' => 'Outer Hebrides',
                'country' => 'Scotland',
                'lat' => '57.9787',
                'lng' => '-6.69421',
            ),
            48 => 
            array (
                'id' => 49,
                'postcode' => 'HU',
                'county' => 'Hull',
                'country' => 'England',
                'lat' => '53.768',
                'lng' => '-0.35321',
            ),
            49 => 
            array (
                'id' => 50,
                'postcode' => 'HX',
                'county' => 'Halifax',
                'country' => 'England',
                'lat' => '53.7203',
                'lng' => '-1.8928',
            ),
            50 => 
            array (
                'id' => 51,
                'postcode' => 'IG',
                'county' => 'Ilford',
                'country' => 'England',
                'lat' => '51.5841',
                'lng' => '0.07166',
            ),
            51 => 
            array (
                'id' => 52,
                'postcode' => 'IM',
                'county' => 'Isle of Man',
                'country' => 'Isle of Man',
                'lat' => '',
                'lng' => '',
            ),
            52 => 
            array (
                'id' => 53,
                'postcode' => 'IP',
                'county' => 'Ipswich',
                'country' => 'England',
                'lat' => '52.1945',
                'lng' => '1.0844',
            ),
            53 => 
            array (
                'id' => 54,
                'postcode' => 'IV',
                'county' => 'Inverness',
                'country' => 'Scotland',
                'lat' => '57.5869',
                'lng' => '-4.11876',
            ),
            54 => 
            array (
                'id' => 55,
                'postcode' => 'JE',
                'county' => 'Jersey',
                'country' => 'Channel Islands',
                'lat' => '',
                'lng' => '',
            ),
            55 => 
            array (
                'id' => 56,
                'postcode' => 'KA',
                'county' => 'Kilmarnock',
                'country' => 'Scotland',
                'lat' => '55.5637',
                'lng' => '-4.60633',
            ),
            56 => 
            array (
                'id' => 57,
                'postcode' => 'KT',
                'county' => 'Kingston upon Thames',
                'country' => 'England',
                'lat' => '51.36',
                'lng' => '-0.342981',
            ),
            57 => 
            array (
                'id' => 58,
                'postcode' => 'KW',
                'county' => 'Kirkwall',
                'country' => 'Scotland',
                'lat' => '58.6543',
                'lng' => '-3.28255',
            ),
            58 => 
            array (
                'id' => 59,
                'postcode' => 'KY',
                'county' => 'Kirkcaldy',
                'country' => 'Scotland',
                'lat' => '56.1624',
                'lng' => '-3.21238',
            ),
            59 => 
            array (
                'id' => 60,
                'postcode' => 'L',
                'county' => 'Liverpool',
                'country' => 'England',
                'lat' => '53.4156',
                'lng' => '-2.96339',
            ),
            60 => 
            array (
                'id' => 61,
                'postcode' => 'LA',
                'county' => 'Lancaster',
                'country' => 'England',
                'lat' => '54.1712',
                'lng' => '-2.90479',
            ),
            61 => 
            array (
                'id' => 62,
                'postcode' => 'LD',
                'county' => 'Llandrindod Wells',
                'country' => 'Wales',
                'lat' => '52.1291',
                'lng' => '-3.341',
            ),
            62 => 
            array (
                'id' => 63,
                'postcode' => 'LE',
                'county' => 'Leicester',
                'country' => 'England',
                'lat' => '52.6443',
                'lng' => '-1.1483',
            ),
            63 => 
            array (
                'id' => 64,
                'postcode' => 'LL',
                'county' => 'Llandudno',
                'country' => 'Wales',
                'lat' => '53.1332',
                'lng' => '-3.71613',
            ),
            64 => 
            array (
                'id' => 65,
                'postcode' => 'LN',
                'county' => 'Lincoln',
                'country' => 'England',
                'lat' => '53.2565',
                'lng' => '-0.35623',
            ),
            65 => 
            array (
                'id' => 66,
                'postcode' => 'LS',
                'county' => 'Leeds',
                'country' => 'England',
                'lat' => '53.8227',
                'lng' => '-1.55356',
            ),
            66 => 
            array (
                'id' => 67,
                'postcode' => 'LU',
                'county' => 'Luton',
                'country' => 'England',
                'lat' => '51.8965',
                'lng' => '-0.509475',
            ),
            67 => 
            array (
                'id' => 68,
                'postcode' => 'M',
                'county' => 'Manchester',
                'country' => 'England',
                'lat' => '53.4787',
                'lng' => '-2.27175',
            ),
            68 => 
            array (
                'id' => 69,
                'postcode' => 'ME',
                'county' => 'Rochester / Medway / Maidstone',
                'country' => 'England',
                'lat' => '51.3339',
                'lng' => '0.58452',
            ),
            69 => 
            array (
                'id' => 70,
                'postcode' => 'MK',
                'county' => 'Milton Keynes',
                'country' => 'England',
                'lat' => '52.0624',
                'lng' => '-0.666183',
            ),
            70 => 
            array (
                'id' => 71,
                'postcode' => 'ML',
                'county' => 'Motherwell',
                'country' => 'Scotland',
                'lat' => '55.7766',
                'lng' => '-3.94397',
            ),
            71 => 
            array (
                'id' => 72,
                'postcode' => 'N',
                'county' => 'London N',
                'country' => 'England',
                'lat' => '51.5853',
                'lng' => '-0.114036',
            ),
            72 => 
            array (
                'id' => 73,
                'postcode' => 'NE',
                'county' => 'Newcastle upon Tyne',
                'country' => 'England',
                'lat' => '55.0359',
                'lng' => '-1.64222',
            ),
            73 => 
            array (
                'id' => 74,
                'postcode' => 'NG',
                'county' => 'Nottingham',
                'country' => 'England',
                'lat' => '53.0054',
                'lng' => '-1.07252',
            ),
            74 => 
            array (
                'id' => 75,
                'postcode' => 'NN',
                'county' => 'Northampton',
                'country' => 'England',
                'lat' => '52.2888',
                'lng' => '-0.841918',
            ),
            75 => 
            array (
                'id' => 76,
                'postcode' => 'NP',
                'county' => 'Newport',
                'country' => 'Wales',
                'lat' => '51.673',
                'lng' => '-3.01631',
            ),
            76 => 
            array (
                'id' => 77,
                'postcode' => 'NR',
                'county' => 'Norwich',
                'country' => 'England',
                'lat' => '52.646',
                'lng' => '1.34086',
            ),
            77 => 
            array (
                'id' => 78,
                'postcode' => 'NW',
                'county' => 'London NW',
                'country' => 'England',
                'lat' => '51.5547',
                'lng' => '-0.196782',
            ),
            78 => 
            array (
                'id' => 79,
                'postcode' => 'OL',
                'county' => 'Oldham',
                'country' => 'England',
                'lat' => '53.5817',
                'lng' => '-2.12594',
            ),
            79 => 
            array (
                'id' => 80,
                'postcode' => 'OX',
                'county' => 'Oxford',
                'country' => 'England',
                'lat' => '51.796',
                'lng' => '-1.29054',
            ),
            80 => 
            array (
                'id' => 81,
                'postcode' => 'PA',
                'county' => 'Paisley',
                'country' => 'Scotland',
                'lat' => '56.0715',
                'lng' => '-4.9136',
            ),
            81 => 
            array (
                'id' => 82,
                'postcode' => 'PE',
                'county' => 'Peterborough',
                'country' => 'England',
                'lat' => '52.6275',
                'lng' => '-0.0446836',
            ),
            82 => 
            array (
                'id' => 83,
                'postcode' => 'PH',
                'county' => 'Perth',
                'country' => 'Scotland',
                'lat' => '56.5628',
                'lng' => '-3.73469',
            ),
            83 => 
            array (
                'id' => 84,
                'postcode' => 'PL',
                'county' => 'Plymouth',
                'country' => 'England',
                'lat' => '50.4277',
                'lng' => '-4.32541',
            ),
            84 => 
            array (
                'id' => 85,
                'postcode' => 'PO',
                'county' => 'Portsmouth',
                'country' => 'England',
                'lat' => '50.8019',
                'lng' => '-1.03536',
            ),
            85 => 
            array (
                'id' => 86,
                'postcode' => 'PR',
                'county' => 'Preston',
                'country' => 'England',
                'lat' => '53.722',
                'lng' => '-2.75212',
            ),
            86 => 
            array (
                'id' => 87,
                'postcode' => 'RG',
                'county' => 'Reading',
                'country' => 'England',
                'lat' => '51.4046',
                'lng' => '-1.02852',
            ),
            87 => 
            array (
                'id' => 88,
                'postcode' => 'RH',
                'county' => 'Redhill',
                'country' => 'England',
                'lat' => '51.1104',
                'lng' => '-0.208604',
            ),
            88 => 
            array (
                'id' => 89,
                'postcode' => 'RM',
                'county' => 'Romford',
                'country' => 'England',
                'lat' => '51.547',
                'lng' => '0.217223',
            ),
            89 => 
            array (
                'id' => 90,
                'postcode' => 'S',
                'county' => 'Sheffield',
                'country' => 'England',
                'lat' => '53.3941',
                'lng' => '-1.4123',
            ),
            90 => 
            array (
                'id' => 91,
                'postcode' => 'SA',
                'county' => 'Swansea',
                'country' => 'Wales',
                'lat' => '51.7607',
                'lng' => '-4.20679',
            ),
            91 => 
            array (
                'id' => 92,
                'postcode' => 'SE',
                'county' => 'London SE',
                'country' => 'England',
                'lat' => '51.4669',
                'lng' => '-0.0359111',
            ),
            92 => 
            array (
                'id' => 93,
                'postcode' => 'SG',
                'county' => 'Stevenage',
                'country' => 'England',
                'lat' => '51.9465',
                'lng' => '-0.159465',
            ),
            93 => 
            array (
                'id' => 94,
                'postcode' => 'SK',
                'county' => 'Stockport',
                'country' => 'England',
                'lat' => '53.3715',
                'lng' => '-2.09801',
            ),
            94 => 
            array (
                'id' => 95,
                'postcode' => 'SL',
                'county' => 'Slough',
                'country' => 'England',
                'lat' => '51.5153',
                'lng' => '-0.642218',
            ),
            95 => 
            array (
                'id' => 96,
                'postcode' => 'SM',
                'county' => 'Sutton',
                'country' => 'England',
                'lat' => '51.3628',
                'lng' => '-0.184654',
            ),
            96 => 
            array (
                'id' => 97,
                'postcode' => 'SN',
                'county' => 'Swindon',
                'country' => 'England',
                'lat' => '51.5056',
                'lng' => '-1.89452',
            ),
            97 => 
            array (
                'id' => 98,
                'postcode' => 'SO',
                'county' => 'Southampton',
                'country' => 'England',
                'lat' => '50.9293',
                'lng' => '-1.39225',
            ),
            98 => 
            array (
                'id' => 99,
                'postcode' => 'SP',
                'county' => 'Salisbury / Salisbury Plain',
                'country' => 'England',
                'lat' => '51.1082',
                'lng' => '-1.77978',
            ),
            99 => 
            array (
                'id' => 100,
                'postcode' => 'SR',
                'county' => 'Sunderland',
                'country' => 'England',
                'lat' => '54.8765',
                'lng' => '-1.38939',
            ),
            100 => 
            array (
                'id' => 101,
                'postcode' => 'SS',
                'county' => 'Southend-on-Sea',
                'country' => 'England',
                'lat' => '51.5621',
                'lng' => '0.599953',
            ),
            101 => 
            array (
                'id' => 102,
                'postcode' => 'ST',
                'county' => 'Stoke-on-Trent',
                'country' => 'England',
                'lat' => '52.9779',
                'lng' => '-2.14618',
            ),
            102 => 
            array (
                'id' => 103,
                'postcode' => 'SW',
                'county' => 'London SW',
                'country' => 'England',
                'lat' => '51.4642',
                'lng' => '-0.167498',
            ),
            103 => 
            array (
                'id' => 104,
                'postcode' => 'SY',
                'county' => 'Shrewsbury',
                'country' => 'England',
                'lat' => '52.6639',
                'lng' => '-3.04862',
            ),
            104 => 
            array (
                'id' => 105,
                'postcode' => 'TA',
                'county' => 'Taunton',
                'country' => 'England',
                'lat' => '51.0558',
                'lng' => '-3.06819',
            ),
            105 => 
            array (
                'id' => 106,
                'postcode' => 'TD',
                'county' => 'Galashiels / Tweeddale',
                'country' => 'Scotland',
                'lat' => '55.614',
                'lng' => '-2.52564',
            ),
            106 => 
            array (
                'id' => 107,
                'postcode' => 'TF',
                'county' => 'Telford',
                'country' => 'England',
                'lat' => '52.7133',
                'lng' => '-2.46197',
            ),
            107 => 
            array (
                'id' => 108,
                'postcode' => 'TN',
                'county' => 'Tonbridge',
                'country' => 'England',
                'lat' => '51.0852',
                'lng' => '0.441469',
            ),
            108 => 
            array (
                'id' => 109,
                'postcode' => 'TQ',
                'county' => 'Torquay',
                'country' => 'England',
                'lat' => '50.4589',
                'lng' => '-3.61605',
            ),
            109 => 
            array (
                'id' => 110,
                'postcode' => 'TR',
                'county' => 'Truro',
                'country' => 'England',
                'lat' => '50.2102',
                'lng' => '-5.24464',
            ),
            110 => 
            array (
                'id' => 111,
                'postcode' => 'TS',
                'county' => 'Cleveland / Teesside',
                'country' => 'England',
                'lat' => '54.585',
                'lng' => '-1.22362',
            ),
            111 => 
            array (
                'id' => 112,
                'postcode' => 'TW',
                'county' => 'Twickenham',
                'country' => 'England',
                'lat' => '51.4486',
                'lng' => '-0.390179',
            ),
            112 => 
            array (
                'id' => 113,
                'postcode' => 'UB',
                'county' => 'Southall / Uxbridge',
                'country' => 'England',
                'lat' => '51.5278',
                'lng' => '-0.417424',
            ),
            113 => 
            array (
                'id' => 114,
                'postcode' => 'W',
                'county' => 'London W',
                'country' => 'England',
                'lat' => '51.5115',
                'lng' => '-0.19226',
            ),
            114 => 
            array (
                'id' => 115,
                'postcode' => 'WA',
                'county' => 'Warrington',
                'country' => 'England',
                'lat' => '53.3905',
                'lng' => '-2.59133',
            ),
            115 => 
            array (
                'id' => 116,
                'postcode' => 'WC',
                'county' => 'London WC',
                'country' => 'England',
                'lat' => '51.5176',
                'lng' => '-0.122637',
            ),
            116 => 
            array (
                'id' => 117,
                'postcode' => 'WD',
                'county' => 'Watford',
                'country' => 'England',
                'lat' => '51.6587',
                'lng' => '-0.393849',
            ),
            117 => 
            array (
                'id' => 118,
                'postcode' => 'WF',
                'county' => 'Wakefield',
                'country' => 'England',
                'lat' => '53.6855',
                'lng' => '-1.4973',
            ),
            118 => 
            array (
                'id' => 119,
                'postcode' => 'WN',
                'county' => 'Wigan',
                'country' => 'England',
                'lat' => '53.5344',
                'lng' => '-2.63754',
            ),
            119 => 
            array (
                'id' => 120,
                'postcode' => 'WR',
                'county' => 'Worcester',
                'country' => 'England',
                'lat' => '52.1631',
                'lng' => '-2.1689',
            ),
            120 => 
            array (
                'id' => 121,
                'postcode' => 'WS',
                'county' => 'Walsall',
                'country' => 'England',
                'lat' => '52.649',
                'lng' => '-1.95765',
            ),
            121 => 
            array (
                'id' => 122,
                'postcode' => 'WV',
                'county' => 'Wolverhampton',
                'country' => 'England',
                'lat' => '52.5807',
                'lng' => '-2.16213',
            ),
            122 => 
            array (
                'id' => 123,
                'postcode' => 'YO',
                'county' => 'York',
                'country' => 'England',
                'lat' => '54.0449',
                'lng' => '-0.887389',
            ),
            123 => 
            array (
                'id' => 124,
                'postcode' => 'ZE',
                'county' => 'Lerwick / Zetland',
                'country' => 'Scotland',
                'lat' => '60.2214',
                'lng' => '-1.19817',
            ),
        ));
        
        
    }
}