<?php
/*
|--------------------------------------------------------------------------
| JamJar.com was developed by Michael Burton.
| Designed by Alexandra Lofthouse.
| In memory of Tony Austin.
|--------------------------------------------------------------------------
*/

/**
 * User Centric Routes
 */

use JamJar\Jobs\SendNotEnoughFundsEmail;
use JamJar\Vehicle;

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

require(base_path() . '/routes/leads.php');
require(base_path() . '/routes/matrix/valuationDrafts.php');
require(base_path() . '/routes/imagesUpload.php');

// Route::group(['middleware' => ['redirects'] ], function () {
//         Route::any('/email/verify/{token}', 'Auth\RegisterController@verify')->name; 
// });


Route::get('/email/verify/{token}', 'Auth\RegisterController@verify')
    ->name('verifyEmail');

Route::get('/valuations/saved', 'ValuationsController@index')
        ->name('dashboard');

Route::get('/valuations/saved/{uuid}/{valuationId?}', 'ValuationsController@show')
    ->name('showSavedValuation');

Route::post('/valuations/saved/{uuid}', 'ValuationsController@cancelValuation')
    ->name('cancelValuation');

Route::patch('/account', 'UserController@update')
        ->name('update_account');

//Route::get('/account/delete', 'UserController@delete')
        //->name('delete_account');

Route::get('/account/unsubscribe', 'UserController@notificationUnsubscribe')->name('notificationUnsubscribe');

Route::delete('/account/{user}', 'UserController@destroy')->name('destroy_account');
Route::post('/account/remove-current-user', 'UserController@destroyCurrentUser')->name('destroy_current_account');

Route::get('/account/{id?}', 'UserController@edit')
		->name('account');
/**
 * VRM Search Engine Routes
 * VRM Search Engine Routes
 */

Route::get('/', 'VehicleController@index')
	->name('/');

Route::post('get-offers-from-source', 'OffersProviderController@saveValuationDraftFromSource')->name('saveValuationDraftFromSource');
Route::post('get-valuations-from-source', 'OffersProviderController@saveValuationDraftAndValuationFromSource')->name('saveValuationDraftAndValuationFromSource');

Route::get('your-car/{vehicle}', 'VehicleController@showVehicleInformation')->name('showVehicleInformation');
Route::post('your-car/{vehicle}', 'VehicleController@storeVehicleMeta')->name('storeVehicleMeta');

Route::get('your-offers/{vehicle}', 'VehicleController@showValuationDraftsForClient')->name('showValuationDraftsForClient');

Route::get('your-details/{vehicle}/{valuationDraftId?}/{showcase?}', 'VehicleController@showVehicleDetails')->name('showVehicleDetails');
Route::patch('your-details/{vehicle}', 'VehicleController@storeVehicleUserDetails')->name('storeVehicleUserDetails');
Route::patch('your-details-login/{vehicle}', 'VehicleController@storeVehicleUserDetailsByLogin')->name('storeVehicleUserDetailsLogin');

Route::get('vehicle/edit/{vehicleId}', 'VehicleController@vehicleEditForm')->name('vehicleEditForm')->middleware('auth');
Route::post('vehicle/edit/{vehicleId}', 'VehicleController@vehicleEdit')->name('vehicleEdit')->middleware('auth');

Route::get('valuations/{vehicle}/{valuationDraftId?}', 'VehicleController@showValuations')->name('showValuations');
Route::get('valuations-create/{vehicle}', 'VehicleController@showValuationsAjax')->name('showValuationsAjax');
Route::get('vehicle/{vehicle}/additional-information', 'VehicleController@showAdditionalInformationForm')->name('showAdditionalInformationForm');
Route::get('vehicle/{vehicle}/showcase', 'VehicleController@showShowcaseForm')->name('showShowcaseForm');
Route::post('vehicle/{vehicle}/additional-information', 'VehicleController@processAdditionalInformationForm')->name('processAdditionalInformationForm');
Route::post('vehicle/{vehicle}/showcase', 'VehicleController@processShowcaseForm')->name('processShowcaseForm');
Route::get('vehicle/{vehicle}/remove-from-marketplace', 'VehicleController@removeFromMarketplace')->name('removeFromMarketplaceCustomer');

Route::get('valuations/{vehicle}/{id}', 'VehicleController@showValuations')
		->name('showValuationsWithId');

Route::get('enter-your-car/{vehicle}', 'VehicleController@noVehicleFound')
		->name('noVehicleFound');

Route::post('valuations/{vehicle}', 'ValuationsController@store')
		->name('storeValuation');

Route::post('valuations/{vrm}/{company}', 'ValuationsController@update')
        ->name('acceptValuation');

Route::get('valuations/accept/{valuation}', 'ValuationsController@acceptGoodNewsLead')
        ->name('acceptValuationGoodNewsLead');

Route::get('tools/calculator', 'Utilities\\ToolsController@showCalculator')->name('standaloneCalculator');

Route::get('autologin/{token}', ['as' => 'autologin', 'uses' => '\Watson\Autologin\AutologinController@autologin']);

Route::get('force-redirect-to-associate-apply-form', 'UserController@forceRedirectToAssociateApplyForm')
    ->name('forceRedirectToAssociateApplyForm');


/**
 * Matrix Partner Routes
 */
Route::namespace('Matrix')->group(function() {
    Route::prefix('associates')->group(function() {
        // Partner Approve / Deny email links
        Route::get('/approve/{user}', 'Controller@approve')
             ->name('approvePartner');

        Route::get('/deny/{user}', 'Controller@deny')
             ->name('denyPartner');


        Route::get('/additional-vehicle-information/unsubscribe', 'SettingsController@additionalVehicleInformationUnsubscribe')->name('additionalVehicleInformationUnsubscribe');

        // Partner Dashboard
        Route::get('/', 'DashboardController@index')->name('partnerDashboard');
        Route::get('/agreed-purchases', 'SalesController@agreedPurchasesList')->name('matrixAgreedPurchasesIndex');
        Route::get('/agreed-sales', 'SalesController@agreedSalesList')->name('matrixAgreedSalesIndex');

        // Partner Dashboard
        Route::get('', 'DashboardController@index')
                ->name('partnerDashboard');

        Route::get('/sales/{sale}', 'SalesController@show')
                ->name('partnerShowSale');

        Route::patch('/sales/{sale}', 'SalesController@update')
                ->name('partnerUpdateSale');

        Route::post('/sales/reject/{sale}', 'SalesController@rejectSale')
            ->name('partnerRejectSale');

        // Partner Account Settings
        Route::get('/account', 'UsersController@edit')
                ->name('partnerAccount');

        Route::patch('/account', 'UsersController@update')
                ->name('partnerAccount');

        Route::get('/settings', 'SettingsController@edit')
            ->name('partnerSettingsForm');

        Route::patch('/settings', 'SettingsController@update')
            ->name('partnerSettingsUpdate');

        Route::get('/apply', 'UsersController@create')
         ->name('becomeAPartner');

        Route::post('/apply', 'UsersController@store')
             ->name('savePartner');

        // Partner Billing
        Route::get('/funds', 'BillingController@index')
            ->name('partnerCredits');

        Route::get('/statements', 'ActivityController@index')
            ->name('partnerStatements');

        Route::any('/checkout', 'BillingController@show')
            ->name('partnerCheckout');

        Route::post('/checkout/complete', 'BillingController@update')
            ->name('partnerCompletePayment');

        Route::get('/checkout/cancel', 'BillingController@destroy')
            ->name('partnerCancelPayment');

        Route::post('/payments', 'BillingController@process')
            ->name('partnerPayments');

        Route::get('/invoices', 'InvoiceController@index')
            ->name('partnerInvoices');

        Route::get('/invoices/{order}', 'InvoiceController@show')
            ->name('partnerInvoice');

        // Partner Valuations
        Route::get('/valuations', 'ValuationsController@index')
            ->name('partnerValuations');

        Route::get('/valuations/{valuation}', 'ValuationsController@show')
            ->name('partnerShowValuation');

        Route::post('/valuations/update-value/{valuation}', 'ValuationsController@submitHigherValueForValuation')
            ->name('partnersubmitHigherValueForValuation');

        Route::post('/valuations/save-note/{valuation}', 'ValuationsController@saveNoteForValuation')
            ->name('saveNoteForValuation');

        // Partner Offers
        Route::get('/automated-bids', 'OffersController@index')
            ->name('partnerOffers');

        Route::get('/automated-bids/create', 'OffersController@create')
            ->name('partnerOfferCreate');

        Route::post('/automated-bids/create', 'OffersController@store')
            ->name('partnerOfferSave');

        Route::get('/automated-bids/{offer}/edit', 'OffersController@edit')
            ->name('partnerOfferEdit');

        Route::patch('/automated-bids/{offer}/edit', 'OffersController@update')
            ->name('partnerOfferUpdate');

	    Route::get('/automated-bids/{offer}/vehicles', 'OffersController@editVehicles')
            ->name('partnerOfferEditVehicles');

        Route::patch('/automated-bids/{offer}/vehicles', 'OffersController@updateVehicles')
            ->name('partnerOfferUpdateVehicles');

        Route::patch('/automated-bids/{offer}/update-value-for-all', 'OffersController@updateOfferValuesForAllVehicles')
            ->name('partnerOfferUpdateValuesForAllVehicles');

        Route::post('/automated-bids/update-value-for-all-rules', 'OffersController@updateOfferValuesForAllRules')
            ->name('partnerOfferValuesForAllRules');

        Route::get('/automated-bids/{offer}/suggested', 'SuggestedValueController@show')
            ->name('partnerOfferSuggestedValues');

        Route::patch('/automated-bids/{offer}/suggested', 'SuggestedValueController@update')
            ->name('partnerOfferUpdateSuggestedValues');

        Route::delete('/automated-bids/{offer}', 'OffersController@destroy')
            ->name('partnerOfferDelete');

        Route::get('/automated-bids/{offer}', 'OffersController@show')
            ->name('partnerOfferShow');

        Route::get('/automated-bids/{offer}/complete', 'OffersController@complete')
            ->name('partnerOfferComplete');

        Route::get('/automated-bids/{offer}/pause', 'OffersController@pause')
            ->name('partnerOfferPause');

        // Partner Locations
        Route::get('/locations', 'LocationsController@index')
            ->name('partnerLocations');

        Route::get('/locations/create', 'LocationsController@create')
            ->name('partnerLocationsCreate');

        Route::post('/locations', 'LocationsController@store')
            ->name('partnerLocationsSave');

        Route::get('/locations/{location}/edit', 'LocationsController@edit')
            ->name('partnerLocationsEdit');

        Route::post('/locations/{location}/markAsPrimary', 'LocationsController@markLocationAsPrimary')
            ->name('partnerLocationsMakePrimary');

        Route::patch('/locations/{location}', 'LocationsController@update')
            ->name('partnerLocationsUpdate');

        Route::delete('/locations/{location}', 'LocationsController@destroy')
            ->name('partnerLocationsDelete');

        // Partner Theme
        Route::get('/theme', 'PartnerWebsiteController@create')
            ->name('partnerWebsiteTheme');

        Route::post('/theme', 'PartnerWebsiteController@update')
            ->name('partnerWebsiteSave');


        // Suggested Values
        Route::get('/suggested-values/{offerSlug}', 'SuggestedValuesController@index')
            ->name('suggestedValues');

        // Update Suggested Values for whole offer/model
        Route::get('/update-suggested-values-model/{offerSlug}', 'SuggestedValuesController@updateSuggestedValuesModel')
            ->name('updateSuggestedValuesModel');

        // Update All Suggested Values for a user
        Route::get('/update-all-suggested-values', 'SuggestedValuesController@updateAllSuggestedValues')
            ->name('updateAllSuggestedValues');

        // Update Suggested Value
        Route::get('/update-suggested-value/{offer}/{derivativeId}/{value}', 'SuggestedValuesController@updateSuggestedValue')
            ->name('updateSuggestedValue');

        // Update Suggested Value Manual
        Route::post('/update-suggested-value-manual/{offer}/{derivativeId}', 'SuggestedValuesController@updateSuggestedValueManual')
            ->name('updateSuggestedValueManual');

        Route::get('/get-alert', 'DashboardController@getAlertForUser')
            ->name('getAlertForUser');

        Route::get('{alert}/accept-alert', 'DashboardController@acceptAlert')
            ->name('acceptAlert');
    });


    Route::get('/partner-sites/{website}', 'PartnerWebsiteController@index')
        ->name('partnerWebsiteIndex');

    Route::get('/partner-sites/{website}/{page}', 'PartnerPageController@show')
        ->name('partnerWebsitePage');

    Route::patch('/partner-sites/{website}/{page}', 'PartnerPageController@show')
        ->name('partnerWebsitePage');

    Route::post('/partner-sites/{website}/{page}', 'PartnerPageController@update')
        ->name('partnerWebsitePageSave');
});



/**
 * Admin Control Panel
 */
Route::namespace('Admin')->middleware('admin')->prefix('admin')->group(function() {
	// Admin Dashboard
	Route::get('/index', 'ActivityController@index')
            ->name('adminDashboard');

    Route::get('/', 'ActivityController@index')
            ->name('adminDashboard');

	// Settings
    Route::get('settings', 'SettingsController@index')
            ->name('adminSettings');

    Route::patch('settings', 'SettingsController@update')
            ->name('adminSettingsUpdate');

    // Activity
    Route::get('activity', 'ActivityController@index')
            ->name('adminActivity');

    // API Partners
    Route::get('valuations/api-partners', 'ValuationsController@apiPartnerIndex')
            ->name('adminApiValuations');

	// Valuations
	Route::get('valuations', 'ValuationsController@index')
			->name('adminValuations');

    Route::any('valuations/search/{vrm?}', 'ValuationsController@search')
            ->name('adminValuationsSearch');


    Route::get('completed-sales', 'SalesController@completedList')
        ->name('adminCompletedSales');

    Route::get('look-ups', 'ValuationDraftsController@index')
        ->name('adminValuationDrafts');

    //Customer Drafts
    Route::get('customer-drafts', 'CustomerDraftsController@index')
        ->name('customerDrafts');

	// Users
	Route::get('users', 'UsersController@index')
			->name('adminUsers');

	Route::get('users/create', 'UsersController@create')
			->name('adminUserAdd');
    Route::post('users/create', 'UsersController@store')
            ->name('adminUserAdd');

    Route::get('users/{user}', 'UsersController@show')
            ->name('adminUserShow');
    Route::get('users/edit/{user}', 'UsersController@edit')
            ->name('adminUserEdit');
    Route::patch('users/edit/{user}', 'UsersController@update')
            ->name('adminUserEdit');
	Route::delete('users/delete/{user}', 'UsersController@destroy')
			->name('adminUserDelete');

    // Partners
	Route::get('partners', 'PartnersController@index')
			->name('adminPartners');

    Route::get('partners/create', 'PartnersController@create')
            ->name('adminPartnerAdd');
    Route::post('partners/create', 'PartnersController@store')
            ->name('adminPartnerAdd');

    Route::get('partners/edit/{user}', 'PartnersController@edit')
            ->name('adminPartnerEdit');
    Route::patch('partners/edit/{user}', 'PartnersController@update')
            ->name('adminPartnerEdit');

    Route::delete('partners/delete/{user}', 'PartnersController@destroy')
            ->name('adminPartnerDelete');

    Route::get('partners/login/{user}', 'PartnersController@loginAsUser')
            ->name('adminPartnerLogin');

    Route::get('users/login/{user}', 'UsersController@loginAsUser')
        ->name('adminUserLogin');

    Route::get('subscribers-tags-changelog', 'SubscribersTagsChangelogController@index')->name('subscribersTagsChangelog');
    Route::get('subscribers-tags-changelog/{changelogId}', 'SubscribersTagsChangelogController@showDetails')->name('subscribersTagsChangelogDetails');

    // Automatic Partner Wizard
    Route::prefix('auto-partner')->group(function() {
        // Creation
        Route::get('', 'AutoPartnerController@index')
            ->name('autoPartnerIndex');
        Route::get('create', 'AutoPartnerController@create')
            ->name('autoPartnerCreate');
        Route::post('', 'AutoPartnerController@store')
            ->name('autoPartnerStore');
        Route::delete('{partner}', 'AutoPartnerController@destroy')
            ->name('autoPartnerDelete');
        Route::get('edit/{partner}', 'AutoPartnerController@edit')
            ->name('autoPartnerEdit');
        Route::patch('edit/{partner}', 'AutoPartnerController@update')
            ->name('autoPartnerUpdate');

        // Pricing
        Route::get('{partner}/pricing', 'AutoPartnerPricingController@create')
            ->name('autoPartnerPricing');
        Route::post('{partner}', 'AutoPartnerPricingController@store')
            ->name('autoPartnerPricingStore');
        Route::delete('{partner}/{tier}', 'AutoPartnerPricingController@destroy')
            ->name('autoPartnerPricingDelete');

        // Branding & Website
        Route::get('{partner}/website', 'AutoPartnerWebsiteController@create')
            ->name('autoPartnerWebsite');

        Route::post('{partner}/website', 'AutoPartnerWebsiteController@store')
            ->name('autoPartnerWebsiteStore');

    });

    // API Partners
    Route::get('api-partners', 'ApiPartnerController@index')
            ->name('adminApiPartnerIndex');

    Route::get('api-partners/{company}/toggle', 'ApiPartnerController@toggle')
            ->name('adminApiPartnerToggle');

    Route::get('api-partners/edit/{company}', 'ApiPartnerController@edit')
            ->name('adminApiPartnerEdit');
    Route::patch('api-partners/edit/{company}', 'ApiPartnerController@update')
            ->name('adminApiPartnerEdit');

    // Funds & Billing
    Route::get('billing', 'BillingController@index')
            ->name('adminFundsIndex');

    // Pricing Tiers
    Route::get('billing/tiers', 'PricingTiersController@index')
            ->name('adminTiersIndex');

    Route::get('billing/tiers/create', 'PricingTiersController@create')
            ->name('adminTiersCreate');

    Route::post('billing/tiers/', 'PricingTiersController@store')
            ->name('adminTiersStore');

    Route::get('billing/tiers/{tier}/edit', 'PricingTiersController@edit')
            ->name('adminTiersEdit');

    Route::patch('billing/tiers/{tier}', 'PricingTiersController@update')
            ->name('adminTiersUpdate');

    Route::delete('billing/tiers/{tier}/delete', 'PricingTiersController@destroy')
            ->name('adminTiersDelete');

    Route::get('billing/tiers/{tier}/pause', 'PricingTiersController@pause')
            ->name('adminTiersPause');

    Route::get('billing/tiers/{tier}/resume', 'PricingTiersController@resume')
            ->name('adminTiersResume');

    // Position Costing
    Route::get('billing/positions', 'PricingPositionsController@index')
            ->name('adminPricingPositionsIndex');

    // Products
    Route::get('billing/funding', 'FundingAmountsController@index')
            ->name('adminFundingAmounts');

    Route::get('billing/funding/create', 'FundingAmountsController@create')
            ->name('adminFundingAmountAdd');
    Route::post('billing/funding/create', 'FundingAmountsController@store')
            ->name('adminFundingAmountStore');

    Route::get('billing/funding/edit/{amount}', 'FundingAmountsController@edit')
            ->name('adminFundingAmountEdit');
    Route::patch('billing/funding/edit/{amount}', 'FundingAmountsController@update')
            ->name('adminFundingAmountUpdate');

    Route::delete('billing/funding/delete/{amount}', 'FundingAmountsController@destroy')
            ->name('adminFundingAmountDelete');

    Route::get('billing/funding/{amount}/pause', 'FundingAmountsController@pause')
            ->name('adminFundingAmountPause');

    Route::get('billing/funding/{amount}/resume', 'FundingAmountsController@resume')
            ->name('adminFundingAmountResume');

    // CAP
    Route::get('tools/cap', 'ToolsController@showCap')
            ->name('showCapValuations');
    Route::post('tools/cap', 'ToolsController@processCap')
            ->name('getCapValuations');

    Route::get('tools/cap/manufacturers', 'ToolsController@showManufacturers')
            ->name('getCapManufacturers');

    Route::get('tools/cap/models/{manufacturer}', 'ToolsController@showModels')
            ->name('getCapModels');

    Route::get('tools/cap/derivatives/{model}', 'ToolsController@showDerivatives')
            ->name('getCapDerivatives');

    Route::get('tools/cap/save/{manufacturer}', 'ToolsController@saveCapData')
            ->name('saveCapData');

    //Email KPI
    Route::get('kpi-email', 'EmailKpiController@index')->name('emailKpi');

    //KPI Data
    Route::get('kpi-data', 'KpiDataController@index')->name('kpiEvaluation');
    Route::post('kpi-data', 'KpiDataController@saveClickthrough')->name('saveClickthrough');\

    Route::get('reminders-tracker', 'VehicleReminderController@index')->name('vehicleRemindersTracker');
    Route::get('emails-debugger', 'EmailDebuggerController@index')->name('AdminEmailDebugger');
    Route::get('emails-debugger/single/{type}', 'EmailDebuggerController@showSingleType')->name('AdminEmailDebuggerSingleType');
    Route::post('emails-debugger/single/{type}', 'EmailDebuggerController@processSingleType')->name('AdminEmailDebuggerSingleTypeSubmit');

    Route::get('throttling-whitelist-ip', 'ThrottlingWhitelistIpsController@index')->name('adminThrottlingWhitelistIpsList');
    Route::post('throttling-whitelist-ip/add', 'ThrottlingWhitelistIpsController@add')->name('adminThrottlingWhitelistIpAdd');
    Route::post('throttling-whitelist-ip/delete/{ipId}', 'ThrottlingWhitelistIpsController@delete')->name('adminThrottlingWhitelistIpDelete');

    //Dashboard Alerts
    Route::get('alerts', 'DashboardController@showAlerts')->name('showAlerts');
    Route::get('alerts/create', 'DashboardController@dashboardAlertCreate')->name('dashboardAlertCreate');
    Route::post('alerts/create', 'DashboardController@dashboardAlertStore')->name('dashboardAlertStore');
    Route::get('alerts/{alert}/activate', 'DashboardController@dashboardAlertActivate')->name('dashboardAlertActivate');
    Route::get('alerts/{alert}/deactivate', 'DashboardController@dashboardAlertDeactivate')->name('dashboardAlertDeactivate');
    Route::delete('alerts/{alert}/delete', 'DashboardController@dashboardAlertDelete')->name('dashboardAlertDelete');
});

// Route::get('sitemap', function() {
//     \Spatie\Sitemap\SitemapGenerator::create(config('app.url'))->writeToFile(public_path('sitemap.xml'));
//     return redirect()->to(config('app.url').'/sitemap.xml');
// });

Route::middleware('admin')->get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('account/mine', 'Admin\PartnersController@returnToOriginalUser')->name('returnToOriginalUser');

// Webhooks (for API's to postback Sale Confirmation)
Route::namespace('Webhooks')->middleware('api')->prefix('webhooks')->group(function() {
    Route::any('/', 'WebhookController@store')->name('webhookStore');

    Route::any('wbct', 'WeBuyCarsTodayController@store')->name('weBuyCarsTodayWebhook');
	Route::any('ctb', 'CarTakeBackController@store')->name('carTakeBackWebhook');
	Route::post('ctb/update-customer-data', 'CarTakeBackController@updateCustomerData')->name('carTakeBackUpdateCustomerDataWebhook');
    Route::post('m4ym/accept', 'Money4YourMotorsController@show')->name('money4YourMotorsWebhook');
    Route::post('m4ym/send', 'Money4YourMotorsController@store')->name('money4YourMotorsSendLead');
    Route::post('m4ymAcceptance', 'Money4YourMotorsController@acceptValuation')->name('money4YourMotorsAcceptValuation');
    Route::post('wbctAcceptance', 'WeBuyCarsTodayController@acceptValuation')->name('weBuyCarsTodayAcceptValuation');
    Route::post('accept-valuation', 'WebhookController@acceptValuation')->name('acceptValuationWebhook');
    Route::post('mw/create-new-sale', 'MotorwiseController@createNewSale')->name('MotorwiseCreateNewSale');


});

// API Routes (for Vue)
Route::namespace('Api')->prefix('api')->group(function() {

    Route::prefix('billing')->group(function() {
        Route::patch('position', 'MatrixController@updatePosition');
        Route::post('calculator', 'MatrixController@calculatePositionCost')->name('billingCalculator');
    });

    Route::prefix('vehicles')->group(function() {
        Route::any('manufacturers/{type}', 'VehicleController@getManufacturers');
        Route::any('models/{manufacturerId}/{fuelType?}', 'VehicleController@getModels');
        Route::post('derivatives', 'VehicleController@getDerivatives');
        Route::post('derivative', 'VehicleController@getDerivative');
        Route::get('derivativeById', 'VehicleController@getDerivativeById');
        Route::get('getSuggestedValue/{derivative}', 'VehicleController@getSuggestedValue');

        Route::post('/', 'VehicleController@store');
        Route::any('/getModelsAndDerivatives', 'VehicleController@getModelsAndDerivatives');
        Route::any('/getModelsAndDerivativesCount', 'VehicleController@getModelsAndDerivativesCount');
        Route::get('derivatives/{modelId}/{offerId}', 'VehicleController@getDerivatives');

        Route::get('/vrm/{vrm?}', 'VehicleController@populateSessionFromExternal')->name('apiVrmInit');
    });

    Route::prefix('amazon')->group(function() {

        Route::post('instance-change', 'AmazonNotifyController@instanceChange')->name('instanceChange');
    });

    Route::prefix('automated-bids')->group(function() {
        Route::post('/', 'OfferController@store');
        Route::patch('/{offer}', 'OfferController@store');
        Route::patch('/{offer}/update', 'OfferController@update');
        Route::patch('/{offer}/complete', 'OfferController@complete');
        Route::get('/{offer}/vehicles', 'OfferController@showVehicles');
        Route::get('/get-data', 'OfferController@getData');
        Route::get('/get-vehicles', 'OfferController@getVehicles');
    });

    Route::prefix('suggested-values')->group(function() {

        Route::get('/get-data', 'SuggestedValuesController@getSuggestedValues');
        Route::get('/get-data-for-model/{offerId}/{capModelId}', 'SuggestedValuesController@getSuggestedValuesForOfferAndParticularModel')->name('suggested-values.get-data-for-model');
    });

    Route::prefix('paid-leads')->group(function() {

        Route::get('/get-data', 'ValuationsController@getData');
    });


    Route::delete('websites/{website}/deleteLogo', 'WebsiteController@deleteLogo');
    Route::delete('websites/{website}/deleteHero', 'WebsiteController@deleteHero');

    Route::get('backlog-valuations', function () {
        Artisan::call('jamjar:backlogged-accepted-valuations');
    });

    Route::get('accepted-valuations', function () {
        Artisan::call('jamjar:accepted-valuations');
    });

});

Route::get('api/user', function(Request $request) {
    return auth()->check() ? ['user' => auth()->user()] : ['user' => false];
});

Route::get('error', function() {
    throw new \Exception('We are in Debug Mode.');
});

Route::get('error', 'Utilities\\SomeController@someMethod');

// Development Routes
if ((Request::ip() === "::1") || (Request::ip() === '84.19.40.74') || (Request::ip() === '127.0.0.1')) {
    require(base_path() . '/routes/development.php');
}

// Route::any('testing-vue', 'Utilities\\ToolsController@vuePlayground');
