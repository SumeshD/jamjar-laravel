<?php

Route::group(['prefix' => '/api/images', 'namespace' => 'Api\Images'], function () {
    Route::get('/uploaded-images', 'UploadedImagesController@list')->name('uploadedImagesList');
    Route::post('/uploaded-images', 'UploadedImagesController@create')->name('uploadedImagesCreate');
    Route::put('/uploaded-images/{uploaded_image_id}', 'UploadedImagesController@edit')->name('uploadedImagesEdit');
});


Route::get('/image/{uploaded_image_id}/{file_name}', 'Api\Images\ImagesController@image')->name('image');
Route::get('/thumbnail/{uploaded_image_id}/{file_name}', 'Api\Images\ImagesController@thumbnail')->name('thumbnail');

Route::post('/api/images/vehicle-image/{vehicleId}', 'Matrix\VehicleImagesController@addVehicleImage')->name('addVehicleImage');
Route::post('/api/images/vehicle-image/remove/{imageId}', 'Matrix\VehicleImagesController@removeVehicleImage')->name('removeVehicleImage');
