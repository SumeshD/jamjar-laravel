<?php
Route::namespace('Matrix')->group(function() {
    Route::prefix('associates')->group(function() {
        Route::get('/marketplace', 'LeadsController@index')->name('partnerGoodNewsLeads');

        Route::get('/marketplace/fee', 'LeadsController@getFeeByValue')->name('leadFeeByValue');
        Route::get('/marketplace/fee/{vehicleId}', 'ValuationDraftsController@getFeeForSeller')->name('matrixCalculateFeeForSeller');
        Route::get('/marketplace/{lead}', 'LeadsController@show')->name('partnerShowLead');

        Route::get('/marketplace/add-new/{vehicleId}', 'LeadsController@addNewValuation')->name('partnerAddNewLeadForm');
        Route::post('/marketplace/{lead}', 'LeadsController@update')->name('partnerUpdateLead');
        Route::post('/marketplace/add-new/{vehicleId}', 'LeadsController@createLeadByVehicle')->name('partnerAddNewLead');

        Route::get('/marketplace-alerts', 'MarketplaceAlertsController@showEnabledList')->name('marketplaceEnabledAlertsList');
        Route::get('/marketplace-alerts/paused', 'MarketplaceAlertsController@showDisabledList')->name('marketplaceDisabledAlertsList');
        Route::get('/marketplace-alerts/create', 'MarketplaceAlertsController@showCreateForm')->name('marketplaceAlertsShowCreateForm');
        Route::post('/marketplace-alerts/create', 'MarketplaceAlertsController@processCreateForm')->name('marketplaceAlertsProcessCreateForm');
        Route::get('/marketplace-alerts/{alert}/edit', 'MarketplaceAlertsController@showEditForm')->name('marketplaceAlertsShowEditForm');
        Route::post('/marketplace-alerts/{alert}/edit', 'MarketplaceAlertsController@processEditForm')->name('marketplaceAlertsProcessEditForm');

        Route::post('/marketplace-alerts/disable/{alert}', 'MarketplaceAlertsController@disableAlert')->name('marketplaceAlertsDisable');
        Route::post('/marketplace-alerts/enable/{alert}', 'MarketplaceAlertsController@enableAlert')->name('marketplaceAlertsEnable');
        Route::post('/marketplace-alerts/delete/{alert}', 'MarketplaceAlertsController@deleteAlert')->name('marketplaceAlertsDelete');
    });
});
