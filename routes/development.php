<?php

use JamJar\PricingTier;

Route::prefix('developer')->group(function() {
    // Valuation Tester
    Route::get('valuation-tester/{valuation}', function($valuation) {
        $tiers = [];
        foreach (PricingTier::all() as $tier) {
            $tiers[] = [
                'from' => $tier->valuation_from,
                'to' => $tier->valuation_to,
                'fee' => $tier->fee
            ];
        }

        /// pricing tiers to calculate from

        /// the value of the car
        $valuation = (int)$valuation;

        $break = false;

        /// workout which band the valuation fits in
        $matchedband = null;
        foreach($tiers as $key => $tier) {
            if($valuation >= $tier['from']) {
                $matchedband = $key;
            } else {
                continue;
            }
        }
        
        // / work out how far up the tier it is
        $balance = $valuation - $tiers[$matchedband]['from'];

        if ($balance == 0) {
            /** @todo fix */
            $valuation = $valuation + 1;
            $balance = $valuation - $tiers[$matchedband]['from'];
        }

        /// work out the range we are working with for percentage calculations
        $range = $tiers[$matchedband]['to'] - $tiers[$matchedband]['from'];

        /// work out how far up the tier they are as a percentage
        $percentage = (100 / ($range / $balance));

        /// work out the previous fee so we have the two points for calculation
        $previousfee = isset($tiers[$matchedband - 1]['fee']) ? $tiers[$matchedband - 1]['fee'] : 0;

        /// work out how much we are adding
        $modifier = ( ( ($tiers[$matchedband]['fee'] - $previousfee) / 100 ) * $percentage);

        $fee = $previousfee + $modifier;
        $fee = round($fee, 2);
        $fee = number_format($fee, 2);

        exit($fee);
    });

    Route::prefix('wwac')->group(function() {
        Route::get('vrm', function() {
            $vehicle = \JamJar\Vehicle::with(['meta','owner'])->find(1);
            $base_uri = env('WWAC_ENV') == 'sandbox' ? 'https://www.wewantanycar.com/apiv2-test/GuidePriceAPILookupTest/api' : 'https://www.wewantanycar.com/apiv2/GuidePriceAPILookup/api';

            $client = new \GuzzleHttp\Client([
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . base64_encode(env('WWAC_API_KEY')),
                ]
            ]);

            $ValuationRequest = [
                'ColumnMappings' => [
                    [
                        'Id' => 'VRM',
                        'Value' => 'YR10YFV',
                    ],
                    [
                        'Id' => 'PartnerIdentifier',
                        'Value' => env('WWAC_PARTNER_IDENTIFIER')
                    ]
                ]
            ];

            $response = $client->post("{$base_uri}/Vehicle/VRMLookup", [
                'body' => json_encode($ValuationRequest),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . base64_encode(env('WWAC_API_KEY')),
                ]
            ]);
            $body = $response->getBody()->getContents();
            $body = json_decode($body, true);

            dd($body);
        });

    	Route::get('valuations', function() {
            $vehicle = \JamJar\Vehicle::with(['meta','owner'])->find(1);
    		$base_uri = env('WWAC_ENV') == 'sandbox' ? 'https://www.wewantanycar.com/apiv2-test/GuidePriceAPILookupTest/api' : 'https://www.wewantanycar.com/apiv2/GuidePriceAPILookup/api';

            $client = new \GuzzleHttp\Client([
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . base64_encode(env('WWAC_API_KEY')),
                ]
            ]);


            $ValuationRequest = [
            	'ColumnMappings' => [
            		[
            			'Id' => 'VRM',
            			'Value' => $vehicle->numberplate,
            		],
            		[
            			'Id' => 'Mileage',
            			'Value' => $vehicle->meta->getOriginal('mileage'),
            		],
            		[
            			'Id' => 'DateFirstReg',
            			'Value' => $vehicle->meta->registration_date,
            		],
            		[
            			'Id' => 'CustomerPostcode',
            			'Value' => $vehicle->owner->postcode,
            		],
            		[
            			'Id' => 'PartnerIdentifier',
            			'Value' =>  env('WWAC_PARTNER_IDENTIFIER'),
            		],
            		[
            			'Id' => 'ServiceHistory',
            			'Value' => $vehicle->meta->service_history,
            		],
            		[
            			'Id' => 'VehicleCondition',
            			'Value' => 'Average',
            		],
            		[
            			'Id' => 'WriteOff',
            			'Value' => $vehicle->meta->write_off ? 'True' : 'False',
            		],
            		[
            			'Id' => 'PreviousOwners',
            			'Value' => (string)$vehicle->meta->number_of_owners,
            		],
            		[
            			'Id' => 'Transmission',
            			'Value' => $vehicle->meta->transmission,
            		],
            		[
            			'Id' => 'Mot',
            			'Value' => str_replace('+', '', $vehicle->meta->mot),
            		],
            		[
            			'Id' => 'ExteriorColour',
            			'Value' => $vehicle->meta->colour->title,
            		]
            	]
            ];

            $response = $client->post("{$base_uri}/Vehicle/GetValuation", [
                'body' => json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . base64_encode(env('WWAC_API_KEY')),
                ]
            ]);
            $body = $response->getBody()->getContents();
            $body = json_decode($body, true);
            dd($body);
    	});
    });

    Route::prefix('m4ym')->group(function() {
        Route::get('valuations', function() {
            $vehicle = \JamJar\Vehicle::with(['meta','owner'])->find(1);

            // Build our client
            $client = new \GuzzleHttp\Client([
                'base_uri' => 'http://uat.m4ym.com/',
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'ClientAPIKey' => '3Nza2dx0H4f1ZZfZqxL8dlrGf6NP4Raf'
                ]
            ]);

            // Get a Valuation Reference
            $response = $client->post('/', [
                'body' => json_encode(['RegistrationPlate' => $vehicle->numberplate])
            ]);
            // Get the body
            $body = $response->getBody()->getContents();
            // Parse it
            $body = json_decode($body, true);
            // Store our Valuation Ref
            $valuationRef = $body['ValuationReference'];

            // Make a PUT request to 'update the valuation record'
            $data = [
            	'VehicleMilage' => (int)$vehicle->meta->getOriginal('mileage'),
            	'VehiclePrevOwners' => $vehicle->meta->number_of_owners, 
            	'VehicleOwnerFirstname' => $vehicle->owner->firstname, 
            	'VehicleOwnerSurname' => $vehicle->owner->lastname, 
            	'VehicleOwnerEmailAddress' => $vehicle->owner->email, 
            	'VehicleOnwerPostcode' => $vehicle->owner->postcode, 
            	'VehicleOwnerTelphoneNumber' => $vehicle->owner->telephone
            ];

            $response = $client->put("/{$valuationRef}", [
                'body' => json_encode($data),
            ]);
            
            // If this returns 200 ok, continue
            if ($response->getStatusCode() === 200) {
                // Get the valuation from M4YM.
                $response = $client->get("/{$valuationRef}");
                //
                $body = $response->getBody()->getContents();
                
                dd($body);
            }
        });
    });

    Route::prefix('wbct')->group(function() {
        Route::get('valuations', function() {
            $vehicle = \JamJar\Vehicle::with(['meta','owner'])->find(1);

            // Build our client
            $client = new \GuzzleHttp\Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ]);

            $client->post('http://www.webuycarstoday.co.uk/wbct-api/linear_services/set_webhook', [
            	'form_params' => [
					'key' => env('WBCT_KEY', '$1232323hjkkljkll$'),
					'username' => env('WBCT_USERNAME', 'JamJar.com'),
					'password' => env('WBCT_PASSWORD', 'jamjar1234'),
					'hook' => 'http://www.webuycarstoday.co.uk/wbct-api/welcome/testHook'
            	]
            ]);

            // Get a Valuation Reference
            $response = $client->post('http://www.webuycarstoday.co.uk/wbct-api/linear_services/get_quote', [
            	'form_params' => [
					'key' => env('WBCT_KEY', '$1232323hjkkljkll$'),
					'username' => env('WBCT_USERNAME', 'JamJar.com'),
					'password' => env('WBCT_PASSWORD', 'jamjar1234'),
					'car_reg' => $vehicle->numberplate, 
					'miles' => $vehicle->meta->getOriginal('mileage'), 
					'postcode' => $vehicle->owner->postcode
    			]
            ]);
            // Get the body
            $body = $response->getBody()->getContents();
            // Parse it
            $body = json_decode($body, true);
            dd($body);
		});
    });

    // Postcode Lookup
    Route::prefix('postcode')->group(function() {

        Route::get('autocomplete/{postcode}', function($postcode) {
            $data = Postcode::autocomplete($postcode);
            dd($data);
        });

        Route::get('outcode/{postcode}', function($postcode) {
            $data = Postcode::outcodeLookup($postcode);
            dd($data);
        });

        Route::get('nearest/{postcode}', function($postcode) {
            $data = Postcode::nearest($postcode);
            dd($data);
        });

        Route::get('{postcode}', function($postcode) {
            $data = Postcode::postcodeLookup($postcode);
            dd($data);
        });
    });

    // SMS Tester
    Route::prefix('sms')->group(function() {
        // test text marketer
        Route::get('test', function() {
            $smsService = new JamJar\Api\TextMarketer;
            $test = $smsService->sendSms('07812093025', 'jamjar.com', 'Welcome to Jamjar.com');
            dd($test);
        });
    });

    // Mailchimp Tester
    Route::prefix('mailchimp')->group(function() {
        Route::get('/', function() {
            return '
                <li>/subscribe/YOU@EXAMPLE.COM</li>
                <li>/unsubscribe/YOU@EXAMPLE.COM</li>
            '; 
        });

        Route::get('/subscribe/{email}', function($email) {
            \Newsletter::subscribe($email);
        });
        Route::get('/unsubscribe/{email}', function($email) {
            \Newsletter::unsubscribe($email);
        });
    });

    // CAP Debug
    Route::prefix('cap/debug')->group(function() {
        Route::get('/', function() {
            return '
                <h2>Get Vehicle Details from the CAP-HPI API</h2>
                <h3>by VRM:</h3>
                <li>https://jamjar.dev/developer/cap/debug/vrm/SA10SZN</li>
                <h3>by Manufacturer ID</h3>
                <li>https://jamjar.dev/developer/cap/debug/models/5261</li>
                <h3>by Derivative ID</h3>
                <li>https://jamjar.dev/developer/cap/debug/derivatives/45513</li>
                <h2>All Manufacturers</h2>
                <li>https://jamjar.dev/developer/cap/debug/manufacturers</li>
            ';
        });

        Route::get('vrm/{vrm}', function($vrm) {
            $api = new \JamJar\Api\CapApi;
            dd($api->getVehicle($vrm));
        });

        Route::get('manufacturers', function() {
            $api = new \JamJar\Api\CapApi;
            dd($api->getManufacturers());
        });

        Route::get('models/{manufacturerId}', function($manufacturerId) {
            $api = new \JamJar\Api\CapApi;
            return $api->getModels($manufacturerId);
        });

        Route::get('derivatives/{modelId}', function($modelId) {
            $api = new \JamJar\Api\CapApi;
            dd($api->getDerivatives($modelId));
        });

        Route::get('model/{capId}', function($capId) {
            $api = new \JamJar\Api\CapApi;
            dd($api->getModelIdForCapId($capId));
        });

        Route::get('seed', function() {
            $api = new \JamJar\Api\CapApi;
            $manufacturers = $api->getManufacturers();
            foreach ($manufacturers as $manufacturer) {
                JamJar\VehicleManufacturer::updateOrCreate($manufacturer->toArray());
                $models = $api->getModels($manufacturer->get('cap_manufacturer_id'));
                foreach ($models as $model) {
                    JamJar\VehicleModel::updateOrCreate([
                        'cap_model_id' => $model->get('cap_model_id'),
                        'name' => $model->get('name'),
                        'manufacturer_id' => (int) $manufacturer->get('cap_manufacturer_id'),
                        'introduced_date' => $model->get('introduced'),
                        'discontinued_date' => $model->get('discontinued'),
                    ]);
                    $derivs = $api->getDerivatives($model->get('cap_model_id'));
                    foreach ($derivs as $derivative) {
                        JamJar\VehicleDerivative::updateOrCreate([
                            'cap_derivative_id' => $derivative->get('cap_derivative_id'),
                            'name' => $derivative->get('name'),
                            'model_id' => $model->get('cap_model_id'),
                        ]);
                        echo $manufacturer->get('name') . ' - ' . $model->get('name')  . ' ' . $derivative->get('name') . ' added.<br>';
                    }
                }
            }
        });
    });
});


// Fix orphaned users to re-add their profiles.
Route::get('/tools/fix-orphaned-users', function() {
    $users = \JamJar\User::get()->reject(function($user) {
        return $user->profile != null;
    });

    $users->each(function($user) {
        $parts = explode('@', $user->email);
        $email = '{$parts[0]}+'.time().'@{$parts[1]}';
        \JamJar\Profile::firstOrCreate([
            'name' => $user->name,
            'email' => $email,
            'user_id' => $user->id
        ]);
        echo $user->name . ' fixed';
    });
    dd('done');
});

