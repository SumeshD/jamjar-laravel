<?php
Route::namespace('Matrix')->group(function() {
    Route::prefix('associates')->group(function() {
        Route::get('/partner-lookups', 'ValuationDraftsController@index')->name('matrixValuationDraftsIndex');
        Route::get('/partner-lookups/listed-vehicles', 'ValuationDraftsController@listedVehiclesList')->name('matrixValuationDraftsListedVehicles');
        Route::get('/partner-lookups/offers-received', 'ValuationDraftsController@offersReceived')->name('matrixValuationDraftsOffersReceived');
        Route::post('/partner-lookups/create-vehicle', 'ValuationDraftsController@createVehicle')->name('matrixValuationDraftsCreateVehicle');
        Route::get('/partner-lookups/create-vehicle', 'ValuationDraftsController@createVehicle')->name('matrixValuationDraftsCreateVehicleWrapper');
        Route::get('/partner-lookups/vehicle-information/{vehicleId}', 'ValuationDraftsController@showVehicleInformationForm')->name('matrixValuationDraftsShowVehicleInformationForm');
        Route::post('/partner-lookups/vehicle-information-save/{vehicleId}', 'ValuationDraftsController@saveVehicleInformation')->name('matrixValuationDraftsSaveVehicleInformation');
        Route::get('/partner-lookups/show-drafts/{vehicleId}', 'ValuationDraftsController@showDrafts')->name('matrixValuationDraftsShow');
        Route::get('/partner-lookups/accept-draft/{draftId}', 'ValuationDraftsController@showDraftAcceptForm')->name('matrixValuationDraftsShowDraftAcceptForm');
        Route::post('/partner-lookups/accept-draft/{draftId}', 'ValuationDraftsController@submitDraftAcceptForm')->name('matrixValuationDraftsSubmitDraftAcceptForm');

        Route::get('/partner-lookups/accept-valuation/{valuationId}', 'ValuationDraftsController@showValuationAcceptForm')->name('matrixValuationDraftsShowValuationAcceptForm');
        Route::post('/partner-lookups/accept-valuation/{valuationId}', 'ValuationDraftsController@submitValuationAcceptForm')->name('matrixValuationDraftsSubmitValuationAcceptForm');

        Route::get('/partner-lookups/edit-vehicle-drafts/{vehicleId}', 'ValuationDraftsController@showVehicleEditForm')->name('matrixValuationDraftsShowEditVehicleForm');
        Route::post('/partner-lookups/edit-vehicle-drafts/{vehicleId}', 'ValuationDraftsController@editVehicle')->name('matrixValuationDraftsEditVehicle');

        Route::get('/partner-lookups/add-to-marketplace/{vehicleId}', 'ValuationDraftsController@showAddToMarketplaceForm')->name('matrixValuationDraftsShowAddToMarketplaceForm');
        Route::post('/partner-lookups/add-to-marketplace/{vehicleId}', 'ValuationDraftsController@addToMarketplace')->name('matrixValuationDraftsAddToMarketplace');
        Route::get('/partner-lookups/remove-from-marketplace/{vehicleId}', 'ValuationDraftsController@removeFromMarketplace')->name('removeFromMarketplace');
//        Route::get('/partner-lookups/sellittrade', 'ValuationDraftsController@sellItTrade')->name('matrixSellItTradeTest');
//        Route::get('/partner-lookups/sellittrade/login', 'ValuationDraftsController@sellItTradeLoginDealer')->name('matrixSellItTradeTestLoginDealer');
//        Route::get('/partner-lookups/sellittrade/create', 'ValuationDraftsController@sellItTradeCreateDealer')->name('matrixSellItTradeTestCreateDealer');
//        Route::post('/partner-lookups/sellittrade/get-valuation', 'ValuationDraftsController@sellItTradeGetValuation')->name('matrixSellItTradeGetValuation');
//        Route::post('/partner-lookups/sellittrade/add-trade-valuation', 'ValuationDraftsController@sellItTradeAddTradeValuation')->name('sellItTradeAddTradeValuation');
    });
});

Route::namespace('Api')->group(function() {
    Route::prefix('api/vehicle-filters')->group(function() {
        Route::get('/manufacturers-group', 'VehicleFiltersController@getManufacturersGroupsList')->name('VehicleFiltersManufacturers');
        Route::get('/models-group', 'VehicleFiltersController@getModelsGroupsList')->name('VehicleFiltersModels');
        Route::get('/vehicles-summary', 'VehicleFiltersController@getVehiclesSummary')->name('VehicleFiltersVehiclesSummary');
    });

    Route::prefix('api/marketplace-alerts')->group(function() {

        Route::post('', 'MarketplaceAlertsController@create')->name('MarketplaceAlertsCreate');
        Route::get('/manufacturers-group', 'VehicleFiltersController@getManufacturersGroupsList')->name('VehicleFiltersManufacturers');
        Route::get('/models-group', 'VehicleFiltersController@getModelsGroupsList')->name('VehicleFiltersModels');
        Route::get('/vehicles-summary', 'VehicleFiltersController@getVehiclesSummary')->name('VehicleFiltersVehiclesSummary');
        Route::post('/alert-configuration', 'MarketplaceAlertsController@getAlertConfiguration')->name('VehicleFiltersVehiclesAlertConfiguration');
    });
});
