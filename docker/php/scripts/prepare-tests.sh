#!/usr/bin/env bash

sh /scripts/setup.sh

cd /var/www/php/ && php artisan migrate --force
cd /var/www/php/ && php artisan key:generate
