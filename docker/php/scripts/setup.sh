#!/usr/bin/env bash

touch /var/www/php/storage/logs/laravel.log
chmod 777 /var/www/php/storage/logs/laravel.log

chmod 777 -R /var/www/php/storage/framework

cd /var/www/php/ && composer self-update 1.10.16 && composer install