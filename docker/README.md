# JamJar Docker


## Installation

### Requirements
* [docker-compose (1.7 or higher)](https://docs.docker.com/compose/). (installation - https://docs.docker.com/compose/install/)

### Preparation

* Copy .env.dist file and change variables according to your Laravel setup
```
$ cp .env.dist .env
```
* Change your Laravel .env file in the root directory
```
$ cd ../
$ nano .env
```

* It should looks like
```
(...)
RDS_HOSTNAME=db
RDS_PORT=3306
RDS_DB_NAME=jamjar_local
RDS_USERNAME=jamjar_local
RDS_PASSWORD=jamjar_local_password
(...)
```

* Add new host to your /etc/hosts file
```bash
$ sudo nano /etc/hosts
```

```bash
127.0.0.1	jamjar.local
```

* Make sure that your local mysql and web server are turned off.
Docker compose expose ports 443 and 3306
```bash
$ sudo service nginx stop
$ sudo service mysql stop
```

### Run docker containers and install dependencies

```
$ cd docker
$ docker-compose build
$ docker-compose up -d
$ docker-compose exec php /scripts/setup.sh
```

## Usage

### Containers status

* Now you can check the status of containers
```bash
$ docker-compose ps
```
You should see something like this:
```bash
docker_db_1      docker-entrypoint.sh mysqld   Up      0.0.0.0:3306->3306/tcp      
docker_nginx_1   /scripts/setup.sh             Up      0.0.0.0:443->443/tcp, 80/tcp
docker_php_1     /scripts/setup.sh             Up      9000/tcp
```
### Website

From now you will be able to open the local website in your favorite web browser [https://jamjar.local](https://jamjar.local)

You should also add an exception for this "unsecure" ssl connection

### Getting into container

```bash
$ docker-compose exec php bash
```

### Running tests

```bash
$ docker-compose exec php vendor/bin/phpunit
```

Now you can run migrations or other artisan command. Check the readme.md file in the root directory.

## Deployment

You can deploy application on several AWS environments 
In order to to that you must provide some additional variables

```bash
TESTING_AWS_ACCESS_KEY_ID=youraccesskeyid
TESTING_AWS_SECRET_ACCESS_KEY=youraccesskeysecret
```

Check the .gitlab-ci.yml file in the root directory of the project

## Getting into AWS EB server

Follow the instructions from deployment section but without last step (we don't want to deploy the application in this case)

Instead of that simply run:

```bash
$ docker-compose exec aws-cli sh /scripts/testing-remote-connection.sh
```

## Database backups

You can create a dump of staging or production database locally (mysql dump)
NOTE: Provide the credentials to .env first!

```bash
$ docker-compose exec db sh /scripts/export-staging-database.sh
```

You can find the dump in db/dumps directory

## Docker Compose Manual

[https://docs.docker.com/compose/](https://docs.docker.com/compose/)

## TODO

* install nodejs + npm and compile vue application