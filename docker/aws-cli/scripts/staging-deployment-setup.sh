#!/usr/bin/env bash

# change elasticbeanstalk to staging (deployment)
cp /data/.elasticbeanstalk/staging-config.yml.dist /data/.elasticbeanstalk/config.yml

# set aws config for staging (ssh connection)
cp /root/.aws/config.dist /root/.aws/config
sed -i -e s,{{AWS_ACCESS_KEY_ID}},${STAGING_AWS_ACCESS_KEY_ID},g /root/.aws/config
sed -i -e s,{{AWS_SECRET_ACCESS_KEY}},${STAGING_AWS_SECRET_ACCESS_KEY},g /root/.aws/config
