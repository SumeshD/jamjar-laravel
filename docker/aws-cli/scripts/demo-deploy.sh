#!/usr/bin/env bash

export AWS_ACCESS_KEY_ID=${STAGING_AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${STAGING_AWS_SECRET_ACCESS_KEY}

export PATH=$PATH:/root/.ebcli-virtual-env/bin
cd /data && eb deploy jamjar-demo --region=eu-west-2
