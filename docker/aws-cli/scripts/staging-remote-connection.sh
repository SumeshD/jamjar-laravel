#!/usr/bin/env sh

export AWS_ACCESS_KEY_ID=${STAGING_AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${STAGING_AWS_SECRET_ACCESS_KEY}

eb ssh jamjar-staging --region=eu-west-2 --custom="ssh -i /root/.ssh/jamjar-staging.pem -o StrictHostKeyChecking=no"
