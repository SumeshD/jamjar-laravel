#!/usr/bin/env bash

export AWS_ACCESS_KEY_ID=${PRODUCTION_AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${PRODUCTION_AWS_SECRET_ACCESS_KEY}

export PATH=$PATH:/root/.ebcli-virtual-env/bin
cd /data && eb deploy Jamjar-prod-v-2 --region=eu-west-2
