#!/usr/bin/env bash

# change elasticbeanstalk to production (deployment)
cp /data/.elasticbeanstalk/production-config.yml.dist /data/.elasticbeanstalk/config.yml

# set aws config for production (ssh connection)
cp /root/.aws/config.dist /root/.aws/config
sed -i -e s,{{AWS_ACCESS_KEY_ID}},${PRODUCTION_AWS_ACCESS_KEY_ID},g /root/.aws/config
sed -i -e s,{{AWS_SECRET_ACCESS_KEY}},${PRODUCTION_AWS_SECRET_ACCESS_KEY},g /root/.aws/config
