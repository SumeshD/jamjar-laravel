#!/usr/bin/env sh

export AWS_ACCESS_KEY_ID=${PRODUCTION_AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${PRODUCTION_AWS_ACCESS_KEY_ID}

eb ssh jamjar-queue --region=eu-west-2 --custom="ssh -i /root/.ssh/jamjar-staging.pem -o StrictHostKeyChecking=no"
