#!/bin/bash

if [  $# -le 1 ]
then
        echo "Usage example:\n"
        echo "ssh_connection_via_bastion.sh 172.31.17.207 jamjar-production-2020.pem"
        echo ""
        echo "Where:\n"
        echo "172.31.17.207 is an private IP of instance"
        echo "jamjar-production-2020.pem is an SSH key of instance"
        exit 1
fi

ssh -f -N -L 2345:$1:22 ec2-user@3.11.18.72 -i jamjar-bastion-host.pem
ssh ec2-user@localhost -p 2345 -i $2 -o StrictHostKeyChecking=no
