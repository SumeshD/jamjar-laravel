#!/usr/bin/env bash

mysqldump --databases ${MYSQL_PRODUCTION_DATABASE_NAME} -u ${MYSQL_PRODUCTION_USER} -p${MYSQL_PRODUCTION_PASSWORD} -h ${MYSQL_PRODUCTION_HOST} > /dumps/production_dump.sql