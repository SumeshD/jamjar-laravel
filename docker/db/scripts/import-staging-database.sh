#!/usr/bin/env bash

sed -i -e 's/ebdb/jamjar_local/g' /dumps/staging_dump.sql
mysql -u root -p${MYSQL_ROOT_PASSWORD} ${MYSQL_DATABASE} < /dumps/staging_dump.sql
