#!/usr/bin/env bash

mysqldump --databases ${MYSQL_STAGING_DATABASE_NAME} -u ${MYSQL_STAGING_USER} -p${MYSQL_STAGING_PASSWORD} -h ${MYSQL_STAGING_HOST} > /dumps/staging_dump.sql