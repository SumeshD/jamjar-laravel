<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'jamjar.com',

    'title_postfix' => '',

    'title_prefix' => 'Admin Control Panel &mdash; ',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>Jam</b>jar',

    'logo_mini' => '<b>J</b>j',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'black',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'admin',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'MAIN NAVIGATION',
        [
            'text' => 'Home',
            'url'  => 'admin/index',
            'icon'  => 'home'
        ],

        [
            'text' => 'CAP Search',
            'url'  => 'admin/tools/cap',
            'icon'  => 'search'
        ],

        [
            'text'  => 'Activity Log',
            'url'   => 'admin/activity',
            'icon'  => 'tachometer',
        ],
        

        [
        	'text'	=> 'Settings',
        	'url'	=> 'admin/settings',
        	'icon'	=> 'cogs',
        ],
        

        [
            'text' => 'Users',
            'icon'  => 'user',
            'submenu' => [
                [
                    'text' => 'Users',
                    'url'  => 'admin/users',
                    'icon'  => 'users'
                ],
                [
                    'text' => 'Associates',
                    'url'  => 'admin/partners',
                    'icon'  => 'user-secret'
                ],
                [
                    'text' => 'API Partners',
                    'url'  => 'admin/api-partners',
                    'icon'  => 'user-secret'
                ],
            ]
        ],

        [
            'text' => 'Customer Drafts',
            'url'  => 'admin/customer-drafts',
            'icon'  => 'pencil-square-o'
        ],

        [
            'text' => 'Valuations',
            'url'  => 'admin/valuations',
            'icon'  => 'dollar'
        ],

        [
            'text' => 'Look-Ups',
            'url'  => 'admin/look-ups',
            'icon'  => 'eye'
        ],

        [
            'text' => 'Completed Sales',
            'url'  => 'admin/completed-sales',
            'icon'  => 'dollar'
        ],

        [
            'text' => 'API Associate Valuations',
            'url'  => 'admin/valuations/api-partners',
            'icon'  => 'gbp'
        ],

        [
            'text' => 'Auto Associate Setup',
            'url'  => 'admin/auto-partner',
            'icon'  => 'magic'
        ],

        [
            'text' => 'Funds &amp; Billing',
            'icon'  => 'money',
            'submenu' => [
                [
                    'text' => 'Pricing Tiers',
                    'url'  => 'admin/billing/tiers',
                    'icon'  => 'money'
                ],
                [
                    'text' => 'Funding Amounts',
                    'url'  => 'admin/billing/funding',
                    'icon'  => 'gbp'
                ],
                [
                    'text' => 'Position Costings',
                    'url'  => 'admin/billing/positions',
                    'icon'  => 'credit-card'
                ],
            ]
        ],
        [
        	'text'	=> 'Email Tracker',
        	'url'	=> 'admin/email-tracker',
	        'icon'  => 'envelope'
        ],
        [
            'text'	=> 'Reminders Tracker',
            'url'	=> 'admin/reminders-tracker',
            'icon'  => 'envelope'
        ],
        [
            'text'	=> 'Emails debugger tool',
            'url'	=> 'admin/emails-debugger',
            'icon'  => 'envelope'
        ],
        [
            'text'	=> 'Email KPIs',
            'url'	=> 'admin/kpi-email',
            'icon'  => 'envelope'
        ],
        [
        	'text'	=> 'KPI Data',
        	'url'	=> 'admin/kpi-data',
	        'icon'  => 'pie-chart'
        ],
        [
            'text'	=> 'Whitelist IPs',
            'url'	=> 'admin/throttling-whitelist-ip',
            'icon'  => 'lock'
        ],
        [
            'text'	=> 'Mailchimp Tags Changelog',
            'url'	=> 'admin/subscribers-tags-changelog',
            'icon'  => 'user'
        ],
        [
            'text' => 'Dashboard alerts',
            'url' => 'admin/alerts',
            'icon' => 'exclamation'
        ]
        // 'ACCOUNT SETTINGS',
        // [
        //     'text' => 'Change Password',
        //     'url'  => 'admin/me',
        //     'icon' => 'lock',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true
    ],
];
