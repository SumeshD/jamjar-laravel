<?php


return  [
    'marketplace-alert-daily-email',
    'marketplace-alert-instant-email',
    'initial-reminder',
    'continuous_reminder',
    'final-reminder',
    'reset-password',
    'improved-offer',
    'new-sale-created-seller-confirmation',
    'sale-received',
    'vehicle-additional-information',
    'matrix-admin-new-partner',
    'matrix-partner-welcome',
    'sale-has-been-removed',
    'update-suggested-value',
    'matrix-partner-approved',
    'matrix-invoice',
    'order-received',
];