<?php

return [

        /*
         * The api key of a MailChimp account. You can find yours here:
         * https://us10.admin.mailchimp.com/account/api-key-popup/
         */
        'apiKey' => env('MAILCHIMP_APIKEY') ?? 'f65ed86d699a38cba736d41577862df0-us17',

        'defaultListName' => 'customer_subscribers',

        'lists' => [
            'customer_subscribers' => [
                'id' => env('MAILCHIMP_CUSTOMERS_LIST_ID') ?? '869394a42f',
            ],
            'matrix_subscribers' => [
                'id' => env('MAILCHIMP_MATRIX_LIST_ID') ?? '869394a42f',
            ],
        ],

        /*
         * If you're having trouble with https connections, set this to false.
         */
        'ssl' => true,
];
