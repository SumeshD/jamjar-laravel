Feature: Edit Vehicle
  In order to update information about my vehicle
  As a Client
  I want to be able to use vehicle edit form

  Background:
    Given Now is "1st of January"
      And I added a vehicle "mazda"

  Scenario: Changing vehicle details
    Given API partner "WWAC" added a Valuation "1001" with collection value "£1000" for Vehicle "mazda"
     When I change vehicle "mazda" to "non-runner" and save the form
     Then Valuation "1001" should be removed
      And API partner "WWAC" should check the vehicle "mazda" again and re-evaluate

  Scenario: Changing vehicle details after place a lead by Matrix
    Given Matrix "Collector" added a valuation "1001" with collection value "£1500" for Vehicle "mazda"
      And Matrix "Collector" paid fee "£5.50" for valuation "1001"
     When I change vehicle "mazda" to "non-runner" and save the form
     Then Valuation "1001" should be removed
      And Matrix "Collector" should get a refund "£5.50"

  Scenario: Changing vehicle details with an "pending" sale
    Given Matrix "Collector" added a valuation "1001" with collection value "£1500" for Vehicle "mazda"
      And I created a sale with id "2001" for valuation "1001" with status "pending"
     When I change vehicle "mazda" to "non-runner" and save the form
     Then Valuation "1001" should be removed
      And Sale with id "2002" should be removed
      And Matrix "Collector" should get a refund "£5.50"
      And Matrix "Collector" should receive email with proper information

  Scenario: Changing vehicle details with rejected sale
    Given Matrix "Collector" added a valuation "1001" with collection value "£1500" for Vehicle "mazda"
      And Sale "2001" with status "rejected" has been added to vehicle "mazda"
     When I change vehicle "mazda" to "non-runner" and save the form
     Then Sale "2001" should be removed
      And Valuation "1001" should be removed

  Scenario: Changing vehicle details with an "complete" sale
    Given Matrix "Collector" added a valuation "1001" with collection value "£1500" for Vehicle "mazda"
      And I created a sale with id "2001" for valuation "1001" with status "complete"
     Then I should not be able to edit vehicle "mazda"