Feature: Good News Leads
  In order to re-sell used cars to my clients
  As a Matrix Partner
  I want to find and contact JamJar customers and buy vehicles from them

  Background:
    Given Client "Smith" added a Vehicle "bmw" with id "1001" at "1st of January"
      And Matrix "Test1 Associate" added a valuation with id "2001" with collection value "£1000" for Vehicle "bmw"
      And Matrix "Test2 Associate" added a valuation with id "2002" with collection value "£1500" for Vehicle "bmw"
      And Client "Smith" added a Vehicle "mazda" with id "1002" at "2st of January"
      And I am a flat_fee payer with value "£2"
      And I have founds "£100"

  Scenario: Use create lead by vehicle form
     When I visit "/associates/leads/add-new/1002"
      And I send the form with value "£150" and fee "0"
     Then New valuation for vehicle "mazda" should be added
      And And I should have "£98" founds left

  Scenario: Use create lead by latest valuation form
    When I visit "/associates/leads/2002"
     And I send the form with value "£1550" and fee "0"
    Then New valuation for vehicle "bmw" should be added
     And And I should have "£98" founds left

  Scenario: Use create lead by previous valuation form
    When I visit "/associates/leads/2001"
    Then I should see minimum price "£1500" for vehicle "bmw"

  Scenario: Use create lead by previous valuation form, adding new valuation
    When I visit "/associates/leads/2001"
     And I send the form with value "£1100" and fee "0"
    Then I should see error message "the minimum value must be greater than 1500"

  Scenario: Use create lead by vehicle form for vehicle with valuations
    When I visit "/associates/leads/add-new/1001"
    Then I should see minimum price "£1500"

  Scenario: Use create lead by vehicle form for vehicle with valuations, adding new valuation
    When I visit "/associates/leads/add-new/1001"
     And I send the form with value "£400" and fee "0"
    Then I should see error message "the minimum value must be greater than 1500"

  Scenario: Matrix can not add valuation for expired vehicles
   Given Now is "4st of February"
    When I visit "/associates/leads/add-new/1001"
    Then I should be redirected to page "/associates/leads"

  Scenario: Matrix can not add valuation for expired vehicles by lead form
    Given Now is "4st of February"
     When I visit "/associates/leads/2002"
     Then I should be redirected to page "/associates/leads"

  Scenario: Matrix can not send form if he has insufficient funds
    Given I have founds "£0"
     When I visit "/associates/leads/2002"
      And I send the form with value "£1550" and fee "0"
     Then I should be redirected to page "/associates/funds"

  Scenario: Matrix can not see vehicles with pending or completed Sales
    Given Client "Smith" created a Sale for Valuation "2002" with status "pending"
     When I visit "/associates/leads/add-new/1001"
     Then I should be redirected to page "/associates/leads"
     NOTE the same behavior for endpoint "/associates/leads/2002" and for Sale status "completed" (so this should be split into 4 scenarios)

  Scenario: Matrix can see vehicles with rejected sale
    Given Client "Smith" created a Sale with id "3001" for Valuation "2002" with status "pending"
      And Matrix "Test1 Associate" rejected a sale with id "3001"
     When I visit "/associates/leads/2002"
     Then I should be able to place a Valuation
     NOTE the same behavior for endpoint "/associates/leads/2002" and for the good news list

  Scenario: Matrix can not see vehicles with pending or completed Sales
    Given I added a valuation for vehicle "bmw"
     When I visit "/associates/leads/add-new/1001"
     Then I should be redirected to page "/associates/leads"
     NOTE the same behavior for endpoint "/associates/leads/2002"
