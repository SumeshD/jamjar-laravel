Feature: Good News Leads
  In order to re-sell used cars to my clients
  As a Matrix Partner
  I want to find and contact JamJar customers and buy vehicles from them

  Background:
    Given Client "Smith" added a Vehicle "bmw" at "1st of January"
      And API partner added a Valuation with collection value "£1000" for Vehicle "bmw" at  "2nd of January"
      And Client "Johnson" added a Vehicle "mazda" at "3th of January"
      And API partner added a Valuation with collection value "£1000" for Vehicle "mazda" at "4th of January"
      And Now is "5th of January"

  Scenario: Good News Leads Ordering
     When I visit the Good News Leads
     Then I should see 2 Leads with order (from top to bottom) "mazda", "bmw"
      And I should be able to place value "£1001" for Vehicle "bmw"

  Scenario: Good News Leads Ordering when another Matrix place a bid
    Given Matrix "Rival Collector" added a valuation with collection value "£1500" for Vehicle "bmw"
      And Matrix "Annoying Collector" added a valuation with collection value "£1501" for Vehicle "bmw"
     When I visit the Good News Leads
     Then I should see 2 Leads with order (from top to bottom) "mazda", "bmw"

  Scenario: Good News Leads Ordering after place better value
     When I place collection value "£2000" for Vehicle "bmw"
      And I visit the Good News Leads
     Then I should see 1 Lead with order (from top to bottom) "mazda"

  Scenario: Another matrix has made a valuation with value higher than mine
    Given I place collection value "£2000" for Vehicle "bmw"
      And Matrix "Rival Collector" added a valuation with collection value "£3000" for Vehicle "bmw"
     When I visit the Good News Leads
     Then I should see 1 Lead with order (from top to bottom) "mazda"

  @NOT_SURE
  Scenario: Another matrix from mine company has placed a valuation
    Given Matrix "Friend Collector" added a valuation with collection value "£3000" for Vehicle "bmw"
     When I visit the Good News Leads
     Then I should see 1 Leads with order (from top to bottom) "mazda"

  @NOT_SURE
  Scenario: Another matrix from mine company has placed a valuation and then lost the lead
    Given Matrix "Friend Collector" added a valuation with collection value "£3000" for Vehicle "bmw"
      And Matrix "Rival Collector" added a valuation with collection value "£4000" for Vehicle "bmw"
     When I visit the Good News Leads
     Then I should see 1 Leads with order (from top to bottom) "mazda"

  Scenario: Client accepted valuation of another Matrix
    Given Matrix "Rival Collector" added a valuation with collection value "£3000" for Vehicle "bmw"
      And Client "Smith" accepted valuation of "Rival Collector"
     When I visit the Good News Leads
     Then I should see 1 Leads with order (from top to bottom) "mazda"

  Scenario: Client accepted valuation of another Matrix but it has been rejected
    Given Matrix "Rival Collector" added a valuation with collection value "£3000" for Vehicle "bmw"
      And Client "Smith" accepted valuation of "Rival Collector"
      And Matrix "Rival Collector" rejected sale of Client "Smith"
     When I visit the Good News Leads
     Then I should see 2 Leads with order (from top to bottom) "bmw", "mazda"

  Scenario: Client does not accepted valuation after 7 days
    Given Now is "10th of January"
     When I visit the Good News Leads
     Then I should see 2 Leads with order (from top to bottom) "mazda", "bmw"
      And The Top Value of Vehicle "bmw" should be "£1000"
      And The Top Value of Vehicle "bmw" should be marked as expired
      And The Top Value of Vehicle "mazda" should be "£1000"
      And I should not be able to place value "£100" for Vehicle "bmw"
      And I should be able to place value "£1001" for Vehicle "bmw"

  Scenario: Client does not accepted valuation after 30 days
    Given Now is "2nd of February"
    When I visit the Good News Leads
    Then I should see 1 Leads with order (from top to bottom) "mazda"
     And The Top Value of Vehicle "mazda" should be "£0"
     And Vehicle "mazda" should be marked as "highest offer expired"
     And I should be able to place value "£100" for Vehicle "mazda"

  Scenario: Client added a vehicle manually and there is no valuation from API
    Given Client "Williams" added a Vehicle "volkswagen" at "5st of January"
      And Client "Williams" did not received any valuations from API for Vehicle "volkswagen"
     When I visit the Good News Leads
     Then I should see 3 Leads with order (from top to bottom) "volkswagen", "mazda", "bmw"
      And The Top Value of Vehicle "volkswagen" should be "£0"
      And Vehicle "volkswagen" should be marked as "no current offers"

  Scenario: Flat payment after accepted lead
    Given My payment is set to "flat" with amount "2.00"
      And My credits are set to "100.00"
     When I place collection value "£2000" for Vehicle "bmw"
     Then My credits should be set to "98.00"

  Scenario: Non-flat payment after place better value for Vehicle
    Given My payment is set to "non-flat"
      And My credits are set to "100.00"
     When I place collection value "£2000" for Vehicle "bmw"
     Then My credits should be set to "100.00"

  Scenario: Non-flat payment after placenew value for Vehicle
    Given Client "Williams" added a Vehicle "volkswagen" at "5st of January"
      And Client "Williams" did not received any valuations from API for Vehicle "volkswagen"
      And My payment is set to "non-flat"
      And My credits are set to "100.00"
     When I place collection value "£1000" for Vehicle "volkswagen"
     Then My credits should be set to "100.00"
