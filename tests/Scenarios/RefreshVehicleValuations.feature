Feature: Refresh Vehicle Valuations
  In order to try sell vehicle again
  As a Client
  I want to be able to use "get fresh results" button

  Background:
    Given Now is "1st of January"
      And I added a vehicle "mazda"

  Scenario: Basic refresh
    Given API partner "WWAC" added a Valuation "1001" with collection value "£1000" for Vehicle "mazda"
      And Now is "8st of January"
     When I get fresh results
     Then Valuation "1001" should be removed
      And API partner "WWAC" should check the vehicle "mazda" again and re-evaluate

  Scenario: Refresh with active valuations
    Given API partner "WWAC" added a Valuation "1001" with collection value "£1000" for Vehicle "mazda"
     When I go to saved valuations for vehicle "mazda"
     Then I should not be able to use get fresh results button

  Scenario: Refresh with expired matrix valuations
    Given Matrix "Collector" added a valuation "1002" with collection value "£1500" for Vehicle "mazda"
      And Now is "8st of January"
     When I get fresh results
     Then valuation "1002" should be removed

  Scenario: Refresh with active matrix valuations
    Given API partner "WWAC" added a Valuation "1001" with collection value "£1000" for Vehicle "mazda"
      And Now is "4th of January"
      And Matrix "Collector" added a valuation "1002" with collection value "£1500" for Vehicle "mazda"
      And Matrix "Collector" paid fee "£5.50" for valuation "1001"
      And Now is "8st of January"
     When I get fresh results
     Then Valuation "1001" should be removed
      And API partner "WWAC" should check the vehicle "mazda" again and re-evaluate
      And Valuation "1002" should be removed
      And Matrix "Collector" should get a refund "£5.50"
      And Matrix "Collector" should receive email with proper information
