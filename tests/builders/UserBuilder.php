<?php
namespace Tests\builders;

use JamJar\User;
use JamJar\UserFund;

class UserBuilder
{
    /** @var int */
    protected static $seed = 1;

    /** @var bool */
    protected $isMatrixPartner = false;

    /** @var bool */
    protected $isScrapDealer = false;

    /** @var string */
    protected $postCode = 'M1+1AE';

    /** @var bool */
    protected $isAdmin = false;

    /** @var int */
    protected $funds = 10000;

    /** @var string */
    private $email;

    public function matrixPartner() : UserBuilder
    {
        $this->isMatrixPartner = true;

        return $this;
    }

    public function associate() : UserBuilder
    {
        $this->isMatrixPartner = false;

        return $this;
    }

    public function scrapDealer() : UserBuilder
    {
        $this->isScrapDealer = true;

        return $this;
    }

    public function nonScrapDealer() : UserBuilder
    {
        $this->isScrapDealer = false;

        return $this;
    }

    public function withPostCode(string $postCode) : UserBuilder
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function isAdmin() : UserBuilder
    {
        $this->isAdmin = true;

        return $this;
    }

    public function withFunds(int $fundsInPounds): UserBuilder
    {
        $this->funds = $fundsInPounds * 100;

        return $this;
    }

    public function withEmail(string $email): UserBuilder
    {
        $this->email = $email;

        return $this;
    }

    public function build() : User
    {
        $user = factory('JamJar\User')->create([
            'matrix_partner' => $this->isMatrixPartner,
        ]);



        $profile = $user->profile;

        $profile->is_scrap_dealer = $this->isScrapDealer;
        $profile->auto_partner = !$this->isMatrixPartner;
        $profile->postcode = $this->postCode;
        $profile->use_flat_fee = true;
        $profile->flat_fee = 2;
        $profile->save();

        if ($this->isAdmin) {
            $user->role_id = 1;
            $user->save();
        }

        if ($this->email) {
            $user->email = $this->email;
            $user->save();
        }

        $founds = new UserFund();
        $founds->user_id = $user->id;
        $founds->funds = $this->funds;
        $founds->save();

        return $user;
    }

}
