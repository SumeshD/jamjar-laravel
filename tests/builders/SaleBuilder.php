<?php
namespace Tests\builders;

use JamJar\Sale;
use JamJar\Valuation;

class SaleBuilder
{
    /** @var Valuation */
    protected $valuation;

    /** @var string */
    protected $status = 'pending';

    public function ofValuation(Valuation $valuation) : SaleBuilder
    {
        $this->valuation = $valuation;

        return $this;
    }

    public function rejected(): SaleBuilder
    {
        $this->status = 'rejected';

        return $this;
    }

    public function completed(): SaleBuilder
    {
        $this->status = 'complete';

        return $this;
    }

    public function build() : Sale
    {

        $sale = new Sale();
        $sale->valuation_id = $this->valuation->id;
        $sale->company_id = $this->valuation->company_id;
        $sale->user_id = $this->valuation->user_id;
        $sale->vehicle_id = $this->valuation->vehicle_id;
        $sale->status = $this->status;

        $sale->delivery_method = 'collection';
        $sale->vehicle_condition = 'great';
        $sale->price = 300;

        $sale->save();

        return $sale;
    }

}
