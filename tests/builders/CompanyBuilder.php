<?php
namespace Tests\builders;

use JamJar\Company;
use JamJar\PartnerLocation;
use JamJar\PartnerWebsite;
use JamJar\User;

class CompanyBuilder
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $city = 'Example City';

    /** @var string */
    protected $companyType = '';

    /** @var bool */
    protected $createLocation = true;

    /** @var string */
    protected $slug = 'test';

    /** @var null|int */
    protected $valuationValidity = null;

    /** @var null|string */
    protected $name;

    public function withUser(User $user) : CompanyBuilder
    {
        $this->user = $user;

        return $this;
    }

    public function withName(string $name) : CompanyBuilder
    {
        $this->name = $name;

        return $this;
    }

    public function withCity(string $city) : CompanyBuilder
    {
        $this->city = $city;

        return $this;
    }

    public function isApiCompany() : CompanyBuilder
    {
        $this->withCompanyType('api');

        return $this;
    }

    public function withCompanyType(string $companyType) : CompanyBuilder
    {
        $this->companyType = $companyType;

        return $this;
    }

    public function withoutLocation() : CompanyBuilder
    {
        $this->createLocation = false;

        return $this;
    }

    public function withSlug(string $slug) : CompanyBuilder
    {
        $this->slug = $slug;

        return $this;
    }

    public function withValuationValidity(int $valuationValidity) : CompanyBuilder
    {
        $this->valuationValidity = $valuationValidity;

        return $this;
    }

    public function build() : Company
    {
        $data = [
            'user_id' => $this->user ? $this->user->id : '',
            'company_type' => $this->companyType,
            'valuation_validity' => $this->valuationValidity,
            'name' => $this->name,
        ];

        $company = factory('JamJar\Company')->create($data);

        if ($this->createLocation) {
            (new PartnerLocation)->fill(['company_id' => $company->id, 'city' => $this->city])->save();
        }

        (new PartnerWebsite)->fill([
            'company_id' => $company->id,
            'slug' => $this->slug,
        ])->save();

        return $company;
    }

}
