<?php
namespace Tests\builders;

use JamJar\Colour;
use JamJar\Company;
use JamJar\Sale;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleMeta;
use JamJar\VehicleModel;
use JamJar\VehicleOwner;
use Ramsey\Uuid\UuidInterface;

class ValuationBuilder
{
    /** @var User */
    protected $user;

    /** @var UuidInterface */
    protected $uuid;

    /** @var Company */
    protected $company;

    /** @var int */
    protected $dropOffValue = 0;

    /** @var int */
    protected $collectionValue = 0;

    /** @var int */
    protected $dropOffDistance = null;

    /** @var Vehicle|null */
    protected $vehicle = null;

    /** @var bool */
    protected $isAccepted = false;

    /** @var bool */
    protected $isSold = false;

    /** @var bool */
    protected $isDeclined = false;

    /** @var int */
    protected $fee = 0;

    /** @var string */
    protected $tempRef;

    /** @var bool */
    protected $legacyOneHourReminderHasBeenSent = false;

    /** @var int */
    protected $goodValue;

    /** @var VehicleManufacturer */
    private $manufacturer;

    /** @var VehicleModel */
    private $model;

    /** @var VehicleDerivative */
    private $derivative;

    public function ofUser(User $user) : ValuationBuilder
    {
        $this->user = $user;

        return $this;
    }

    public function withUuid(UuidInterface $uuid) : ValuationBuilder
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function ofCompany(Company $company) : ValuationBuilder
    {
        $this->company = $company;

        return $this;
    }

    public function ofVehicle(Vehicle $vehicle) : ValuationBuilder
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function withDropOffValue(int $dropOffValue) : ValuationBuilder
    {
        $this->dropOffValue = $dropOffValue;

        return $this;
    }

    public function withGoodValue(int $goodValue) : ValuationBuilder
    {
        $this->goodValue = $goodValue;

        return $this;
    }

    public function withDropOffDistance(int $dropOffDistance) : ValuationBuilder
    {
        $this->dropOffDistance = $dropOffDistance;

        return $this;
    }

    public function withCollectionValue(int $collectionValue) : ValuationBuilder
    {
        $this->collectionValue = $collectionValue;

        return $this;
    }

    public function legacyOneHourReminderHasBeenSent(): ValuationBuilder
    {
        $this->legacyOneHourReminderHasBeenSent = true;

        return $this;
    }

    public function withVehicle(Vehicle $vehicle) : ValuationBuilder
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function isAccepted() : ValuationBuilder
    {
        $this->isAccepted = true;
        $this->isDeclined = false;

        return $this;
    }

    public function isDeclined(): ValuationBuilder
    {
        $this->isDeclined = true;
        $this->isAccepted = false;

        return $this;
    }

    public function isSold()
    {
        $this->isAccepted = true;
        $this->isSold = true;

        return $this;
    }

    public function withFee(int $feeInPounds): self
    {
        $this->fee = $feeInPounds * 100;

        return $this;
    }

    public function withTempRef(string $tempRef): self
    {
        $this->tempRef = $tempRef;

        return $this;
    }

    public function ofManufacturer(VehicleManufacturer $manufaturer): self
    {
        $this->manufacturer = $manufaturer;

        return $this;
    }

    public function ofModel(VehicleModel $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function ofDerivative(VehicleDerivative $derivative): self
    {
        $this->derivative = $derivative;

        return $this;
    }

    public function build() : Valuation
    {
        if ($this->vehicle == null) {
            ($colour = new Colour())->fill(['title' => 'red'])->save();
            ($vehicle = new Vehicle())->save();
            (new VehicleMeta())->fill(['vehicle_id' => $vehicle->id, 'color' => $colour->id])->save();
            (new VehicleOwner())->fill(['vehicle_id' => $vehicle->id])->save();

            $this->vehicle = $vehicle;
        }

        $data = [
            'user_id' => $this->user ? $this->user->id : '',
            'uuid' => $this->uuid ? $this->uuid : '',
            'company_id' => $this->company ? $this->company->id : '',
            'vehicle_id' => $this->vehicle->id,
            'dropoff_value' => $this->dropOffValue,
            'collection_value' => $this->collectionValue,
            'distance' => $this->dropOffDistance,
            'fee' => $this->fee,
            'temp_ref' => $this->tempRef,
            'vehicle_condition_value_good' => $this->goodValue,
            '1h_email_reminder_sent' => $this->legacyOneHourReminderHasBeenSent,
            'cap_value' => $this->vehicle ? $this->vehicle->getCapValueInPence() / 100 : null,
            'manufacturer_id' => $this->manufacturer ? $this->manufacturer->cap_manufacturer_id : 100,
            'model_id' => $this->model ? $this->model->cap_model_id : 100,
            'derivative_id' => $this->derivative ? $this->derivative->cap_derivative_id : 100,
        ];

        /** @var Valuation $valuation */
        $valuation = factory('JamJar\Valuation')->create($data);

        if ($this->isAccepted || $this->isDeclined) {
            $sale = new Sale();
            $sale->valuation_id = $valuation->id;
            $sale->company_id = $this->company ? $this->company->id : '';
            $sale->user_id = $this->user ? $this->user->id : '';
            $sale->vehicle_id = $this->vehicle->id;

            if ($this->isSold) {
                $sale->status = 'complete';
            } else if ($this->isDeclined) {
                $sale->status = 'rejected';
            } else {
                $sale->status = 'pending';
            }

            $sale->delivery_method = $this->collectionValue >= $this->dropOffValue ? 'collection' : 'dropoff';
            $sale->vehicle_condition = 'great';
            $sale->price = $this->collectionValue >= $this->dropOffValue ? $this->collectionValue : $this->dropOffValue;

            $sale->save();
        }

        return $valuation;
    }

}
