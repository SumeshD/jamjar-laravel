<?php
namespace Tests\builders;

use JamJar\PartnerLocation;
use JamJar\User;

class LocationBuilder
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $postcode = 'NG15LL';

    public function withUser(User $user): LocationBuilder
    {
        $this->user = $user;

        return $this;
    }

    public function withPostcode(string $postcode): LocationBuilder
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function build() : PartnerLocation
    {
        $location = new PartnerLocation();

        $location->user_id = $this->user->id;
        $location->company_id = $this->user->company->id;
        $location->save();

        return $location;
    }

}
