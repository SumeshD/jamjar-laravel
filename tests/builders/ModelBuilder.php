<?php
namespace Tests\builders;

use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;

class ModelBuilder
{
    private static $capModelIdSeed = 2001;

    /** @var string */
    private $name;

    /** @var VehicleManufacturer */
    private $manufacturer;

    public function withName(string $name): ModelBuilder
    {
        $this->name = $name;

        return $this;
    }

    public function withManufacturer(VehicleManufacturer $manufacturer): ModelBuilder
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function build(): VehicleModel
    {
        $model = new VehicleModel();
        $model->name = $this->name;
        $model->cap_model_id = self::$capModelIdSeed++;
        $model->introduced_date = 2015;
        $model->discontinued_date = 2017;
        $model->manufacturer_id = $this->manufacturer->cap_manufacturer_id;
        $model->fuel_type = 'Petrol';
        $model->plate_years = '2015,2016,2017';
        $model->internal_manufacturer_id = $this->manufacturer->id;
        $model->save();

        return $model;
    }
}
