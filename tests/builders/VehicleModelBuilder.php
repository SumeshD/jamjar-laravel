<?php
namespace Tests\builders;

use JamJar\Role;
use JamJar\User;
use JamJar\VehicleModel;

class VehicleModelBuilder
{

    /** @var int */
    protected $manufacturerId;

    protected $capModelId;

    protected $name;

    protected $introducedDate;

    protected $discontinuedDate;

    protected $fuelType;

    protected $plateYears;

    protected $internalManufacturerId;

    public function withManufacturerId(int $manufacturerId) : self
    {
        $this->manufacturerId = $manufacturerId;

        return $this;
    }

    public function withCapModelId(int $capModelId): self
    {
        $this->capModelId = $capModelId;

        return $this;
    }

    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withIntroducedDate(string $introducedDate): self
    {
        $this->introducedDate = $introducedDate;

        return $this;
    }

    public function withDiscontinuedDate(string $discountinueDate): self
    {
        $this->discontinuedDate = $discountinueDate;

        return $this;
    }

    public function withFuelType(string $fuelType): self
    {
        $this->fuelType = $fuelType;

        return $this;
    }

    public function withPlateYears(string $plateYears): self
    {
        $this->plateYears = $plateYears;

        return $this;
    }

    public function withInternalManufacturerId(int $internalManufacturerId): self
    {
        $this->internalManufacturerId = $internalManufacturerId;

        return $this;
    }

    /**
     * @return VehicleModel
     * @throws \Exception
     */
    public function build() : VehicleModel
    {
        $vehicleModel = new VehicleModel();

        $vehicleModel->manufacturer_id = $this->manufacturerId;
        $vehicleModel->cap_model_id = $this->capModelId;
        $vehicleModel->name = $this->name;
        $vehicleModel->introduced_date = $this->introducedDate;
        $vehicleModel->discontinued_date = $this->discontinuedDate;
        $vehicleModel->fuel_type = $this->fuelType;
        $vehicleModel->plate_years = $this->plateYears;
        $vehicleModel->internal_manufacturer_id = $this->internalManufacturerId;
        $vehicleModel->save();

        return $vehicleModel;
    }

}
