<?php
namespace Tests\builders;

use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;

class DerivativeBuilder
{
    private static $capDerivativeIdSeed = 3001;

    /** @var string */
    private $name;

    /** @var VehicleModel */
    private $model;

    public function withName(string $name): DerivativeBuilder
    {
        $this->name = $name;

        return $this;
    }

    public function withModel(VehicleModel $model): DerivativeBuilder
    {
        $this->model = $model;

        return $this;
    }

    public function build(): VehicleDerivative
    {
        $derivative = new VehicleDerivative();
        $derivative->name = $this->name;
        $derivative->cap_derivative_id = self::$capDerivativeIdSeed++;
        $derivative->model_id = $this->model->cap_model_id;
        $derivative->internal_model_id = $this->model->id;
        $derivative->save();

        return $derivative;
    }
}
