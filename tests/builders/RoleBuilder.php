<?php
namespace Tests\builders;

use JamJar\Role;
use JamJar\User;

class RoleBuilder
{
    /** @var int */
    protected $id;

    /** @var string */
    protected $name;

    public function withId(int $id) : RoleBuilder
    {
        $this->id = $id;

        return $this;
    }

    public function withName(string $name) : RoleBuilder
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Role
     * @throws \Exception
     */
    public function build() : Role
    {
        $role = new Role();

        if (!$this->id || !$this->name) {
            throw new \Exception('You must provide both id and name');
        }

        $role->id = $this->id;
        $role->name = $this->name;
        $role->save();

        return $role;
    }

}
