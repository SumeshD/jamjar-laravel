<?php
namespace Tests\builders;

use JamJar\VehicleManufacturer;

class ManufacturerBuilder
{
    private static $capManufacturerIdSeed = 1001;

    private $name;

    private $vehicleType = 'CAR';

    public function withName(string $name): ManufacturerBuilder
    {
        $this->name = $name;

        return $this;
    }

    public function lightCommercialVehicle(): ManufacturerBuilder
    {
        $this->vehicleType = 'LCV';

        return $this;
    }

    public function car(): ManufacturerBuilder
    {
        $this->vehicleType = 'CAR';

        return $this;
    }

    public function build(): VehicleManufacturer
    {
        $manufacturer = new VehicleManufacturer();
        $manufacturer->name = $this->name;
        $manufacturer->cap_manufacturer_id = self::$capManufacturerIdSeed++;
        $manufacturer->vehicle_type = $this->vehicleType;
        $manufacturer->save();

        return $manufacturer;
    }

}
