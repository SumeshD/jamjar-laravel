<?php
namespace Tests\builders;

use JamJar\Offer;
use JamJar\OfferDerivative;
use JamJar\OfferModel;
use JamJar\User;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;
use Ramsey\Uuid\Uuid;

class OfferBuilder
{
    /** @var User */
    private $user;

    /** @var string */
    private $name = 'Example Offer Name';

    /** @var VehicleDerivative */
    private $derivative;

    /** @var int */
    private $percentage = 100;

    public function ofUser(User $user): OfferBuilder
    {
        $this->user = $user;

        return $this;
    }

    public function setName(string $name): OfferBuilder
    {
        $this->name = $name;

        return $this;
    }

    public function withDerivative(VehicleDerivative $derivative): OfferBuilder
    {
        $this->derivative = $derivative;

        return $this;
    }

    public function withPercentageValue(int $percentage): OfferBuilder
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function build(): Offer
    {
        $offer = new Offer();
        $offer->user_id = $this->user->id;
        $offer->name = $this->name;
        $offer->slug = Uuid::uuid4()->toString();
        $offer->base_value = 100;
        $offer->minimum_mileage = 0;
        $offer->maximum_mileage = 200000;
        $offer->minimum_value = 0;
        $offer->maximum_value = 200000;
        $offer->minimum_service_history = 'None';
        $offer->minimum_mot = 'Expired';
        $offer->maximum_previous_owners = 6;
        $offer->accept_write_offs = 0;
        $offer->acceptable_colors = 'a:0:{}';
        $offer->acceptable_defects = 'a:0:{}';
        $offer->great_condition_percentage_modifier = 100;
        $offer->good_condition_percentage_modifier = 80;
        $offer->avg_condition_percentage_modifier = 60;
        $offer->poor_condition_percentage_modifier = 40;
        $offer->manufacturer_id = $this->derivative->model->manufacturer->cap_manufacturer_id;
        $offer->active = true;
        $offer->minimum_age = '1983-A';
        $offer->maximum_age = '2019-68';
        $offer->maximum_write_off_category = 'none';
        $offer->fuel_type = 'Petrol';
        $offer->vehicle_type = 'CAR';
        $offer->finished = true;

        $offer->save();

        $offerModel = new OfferModel();
        $offerModel->offer_id = $offer->id;
        $offerModel->cap_model_id = $this->derivative->model->cap_model_id;
        $offerModel->base_value = $this->percentage;

        $offerModel->save();

        $offerDerivative = new OfferDerivative();

        $offerDerivative->offer_id = $offer->id;
        $offerDerivative->cap_model_id = $this->derivative->model->cap_model_id;
        $offerDerivative->cap_derivative_id = $this->derivative->cap_derivative_id;
        $offerDerivative->base_value = $this->percentage;

        $offerDerivative->save();

        return $offer;
    }
}
