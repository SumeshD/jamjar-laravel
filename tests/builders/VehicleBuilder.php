<?php
namespace Tests\builders;

use Carbon\Carbon;
use JamJar\Colour;
use JamJar\User;
use JamJar\Vehicle;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleMeta;
use JamJar\VehicleModel;
use JamJar\VehicleOwner;

class VehicleBuilder
{
    /** @var int */
    private static $seed = 1;

    /** @var string */
    private $numberPlate = 'TEST001';

    /** @var User */
    private $owner;

    /** @var int */
    private $mileage = 10000;

    /** @var string */
    private $modelName = 'BMW';

    /** @var VehicleManufacturer */
    private $manufacturer;

    /** @var VehicleModel */
    private $model;

    /** @var VehicleDerivative */
    private $derivative;

    /** @var int */
    private $capValueInPence;

    /** @var bool */
    private $addedToMarketplace;

    public function withNumberPlate(string $numberPlate) : VehicleBuilder
    {
        $this->numberPlate = $numberPlate;

        return $this;
    }

    public function withOwner(User $owner) : VehicleBuilder
    {
        $this->owner = $owner;

        return $this;
    }

    public function withModelName(string $modelName): VehicleBuilder
    {
        $this->modelName = $modelName;

        return $this;
    }

    public function ofModel(VehicleModel $model) : VehicleBuilder
    {
        $this->model = $model;

        return $this;
    }

    public function ofDerivative(VehicleDerivative $derivative) : VehicleBuilder
    {
        $this->derivative = $derivative;

        return $this;
    }

    public function withMileage(int $mileage) : VehicleBuilder
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function ofManufacturer(VehicleManufacturer $manufacturer): VehicleBuilder
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function withCapValueInPence(int $value): VehicleBuilder
    {
        $this->capValueInPence = $value;

        return $this;
    }

    public function addedToMarketplace(): VehicleBuilder
    {
        $this->addedToMarketplace = true;

        return $this;
    }

    public function addCapIds(): VehicleBuilder
    {
        return $this;
    }

    public function build() : Vehicle
    {
        ($colour = new Colour)->fill(['title' => 'red'])->save();
        ($vehicle = new Vehicle)->fill([
            'numberplate' => $this->numberPlate,
            'cap_value_in_pence' => $this->capValueInPence ?? null,
        ])->save();

        if ($this->addedToMarketplace) {
            $vehicle->is_visible_on_marketplace = true;
            $vehicle->added_to_marketplace_at = Carbon::now();
            $vehicle->save();
        }

        if (!$this->manufacturer) {
            $manufacturer = new VehicleManufacturer();
            $manufacturer->save();
            $this->manufacturer = $manufacturer;
        }

        if (!$this->model) {
            $model = new VehicleModel();
            $model->name = $this->modelName ? $this->modelName : 'Generic model';
            $model->save();
            $this->model = $model;
        }

        if (!$this->derivative) {
            $derivative = new VehicleDerivative();
            $derivative->save();
            $this->derivative = $derivative;
        }

        $vehicle->manufacturer_id = $this->manufacturer ? $this->manufacturer->getId() : null;
        $vehicle->model_id = $this->manufacturer ? $this->model->getId() : null;
        $vehicle->derivative_id = $this->derivative ? $this->derivative->getId() : null;
        $vehicle->is_visible_on_marketplace = true;
        $vehicle->save();

        (new VehicleMeta)->fill([
            'vehicle_id' => $vehicle->id,
            'color' => $colour->id,
            'mileage' => $this->mileage,
            'plate_year' => '2016 15',
            'manufacturer' => $this->manufacturer ? $this->manufacturer->name : '',
            'model' => $this->model ? $this->model->name : $this->modelName,
            'derivative' => $this->derivative ? $this->derivative->name : '',
            'transmission' => 'Manual',
            'fuel_type' => 'Petrol',
            'registration_date' => '20161513',
            'number_of_owners' => 2,
            'service_history' => 'FullFranchise',
            'mot' => 'OneToThree',
            'write_off' => 0,
            'write_off_category' => 'none',
            'non_runner' => 0,
            'non_runner_reason' => 'a:0:{}',
            'vin_number' => 'WF0AXXGCDA4A56461',
            'body_type' => '5 Door Hatchback',
            'doors' => null,
            'engine_capacity' => 1596,
            'cap_model_id' => $this->model ? $this->model->cap_model_id : null,
        ])->save();

        (new VehicleOwner)->fill([
            'vehicle_id' => $vehicle->id,
            'user_id' => $this->owner ? $this->owner->id : null,
            'postcode' => 'NG15LL',
            'email' => $this->owner ? $this->owner->email : sprintf('test%d@example.com', self::$seed++),
        ])->save();

        return $vehicle;
    }
}