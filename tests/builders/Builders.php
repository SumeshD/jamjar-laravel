<?php
namespace Tests\builders;

use JamJar\Offer;

class Builders {
    public static function aUser() : UserBuilder
    {
        return new UserBuilder();
    }

    public static function aCompany() : CompanyBuilder
    {
        return new CompanyBuilder();
    }

    public static function aValuation() : ValuationBuilder
    {
        return new ValuationBuilder();
    }

    public static function aVehicle() : VehicleBuilder
    {
        return new VehicleBuilder();
    }

    public static function aRole() : RoleBuilder
    {
        return new RoleBuilder();
    }

    public static function aSale() : SaleBuilder
    {
        return new SaleBuilder();
    }

    public static function aLocation() : LocationBuilder
    {
        return new LocationBuilder();
    }

    public static function aManufacturer() : ManufacturerBuilder
    {
        return new ManufacturerBuilder();
    }

    public static function aModel() : ModelBuilder
    {
        return new ModelBuilder();
    }

    public static function aDerivative() : DerivativeBuilder
    {
        return new DerivativeBuilder();
    }

    public static function anOffer() : OfferBuilder
    {
        return new OfferBuilder();
    }

    public static function aVehicleModel() : VehicleModelBuilder
    {
        return new VehicleModelBuilder();
    }



}
