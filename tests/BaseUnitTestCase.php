<?php
namespace Tests;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use JamJar\Model\UploadedImage;
use Jamjar\Services\EloquentUploadedImageManager;
use Jamjar\Services\ImagesManagement\ImagesManagerInterface;

abstract class BaseUnitTestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp()
    {
        parent::setUp();

        $this->disableExceptionHandling();
    }

    // Hat tip, @adamwathan.
    protected function disableExceptionHandling()
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);
        $this->app->instance(ExceptionHandler::class, new TestHandler);
    }

    protected function createUploadedImage(string $seedString): UploadedImage
    {
        return $this->getUploadedImageManager()->create(
            'original_name_' . $seedString . '.png',
            'active'
        );
    }

    /** @return EloquentUploadedImageManager */
    protected function getUploadedImageManager()
    {
        return $this->app->get('Jamjar\Services\EloquentUploadedImageManager');
    }

    /** @return ImagesManagerInterface */
    protected function getImagesManager()
    {
        return $this->app->get('Jamjar\Services\ImagesManagement\ImagesManagerInterface');
    }
}
