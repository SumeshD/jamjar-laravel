<?php
namespace Tests;

use JamJar\Exceptions\Handler;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use JamJar\Model\UploadedImage;
use Tests\builders\Builders;
use JamJar\Services\EloquentUploadedImageManager;
use JamJar\Services\ImagesManagement\ImagesManagerInterface;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp()
    {
        parent::setUp();

        $_SERVER['HTTP_HOST'] = 'localhost';

        Builders::aRole()->withId('1')->withName('Administrator')->build();

        $this->disableExceptionHandling();
    }

    public function signIn($user = null)
    {
        $user = $user ?: factory('JamJar\User')->create();

        $this->actingAs($user);

        return $this;
    }

    // Hat tip, @adamwathan.
    protected function disableExceptionHandling()
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);
        $this->app->instance(ExceptionHandler::class, new TestHandler);
    }

    protected function withExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, $this->oldExceptionHandler);

        return $this;
    }

    protected function seeIsSoftDeletedInDatabase($table, array $data, $connection = null)
    {
        $database = $this->app->make('db');

        $connection = $connection ?: $database->getDefaultConnection();

        $count = $database->connection($connection)
        ->table($table)
        ->where($data)
        ->whereNotNull('deleted_at')
        ->count();

        $this->assertGreaterThan(0, $count, sprintf(
            'Found unexpected records in database table [%s] that matched attributes [%s].', $table, json_encode($data)
        ));

        return $this;
    }

    protected function createUploadedImage(string $seedString): UploadedImage
    {
        return $this->getUploadedImageManager()->create(
            'original_name_' . $seedString . '.png',
            'active'
        );
    }

    /** @return EloquentUploadedImageManager */
    protected function getUploadedImageManager()
    {
        return $this->app->get(EloquentUploadedImageManager::class);
    }

    /** @return ImagesManagerInterface */
    protected function getImagesManager()
    {
        return $this->app->get(ImagesManagerInterface::class);
    }
}

class TestHandler extends Handler
{
    public function __construct()
    {
    }
    public function report(\Exception $e)
    {
    }
    public function render($request, \Exception $e)
    {
        throw $e;
    }
}
