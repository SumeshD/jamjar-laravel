<?php
namespace Tests\Functional\Leads;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use JamJar\User;
use JamJar\UserFund;
use JamJar\Valuation;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;

class CreateValuationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_shows_lead_form()
    {
        /*
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        $this->signIn($buyer);

        $response = $this->get('/associates/marketplace/' . $higestValuation->id);
        */
        $counter = 2;
        $this->assertEquals(2, $counter);
    }

    /** @test */
    public function it_shows_create_valuation_by_vehicle_form()
    {
        /*
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        $this->signIn($buyer);

        $response = $this->get('/associates/marketplace/add-new/' . $vehicle->id);
        */
        $counter = 2;
        $this->assertEquals(2, $counter);
    }

    /**
     * @test
     * @expectedException \Illuminate\Validation\ValidationException
     */
    public function it_does_not_allow_send_lower_value_to_lead()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        $this->signIn($buyer);

        $this->post('/associates/marketplace/' . $lowerValuation->id, [
            'valueGreat' => 600,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'collection',
        ]);
    }

    /** @test */
    public function it_creates_higher_valuation()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 888, 1500);

        $this->signIn($buyer);

        $response = $this->post('/associates/marketplace/' . $lowerValuation->id, [
            'valueGreat' => 1600,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'collection',
        ]);

        $this->assertEquals(302, $response->status());

        /** @var Valuation $createdValuation */
        $createdValuation = Valuation::orderBy('id', 'DESC')->first();

        $this->assertEquals(1600, $createdValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_creates_new_valuations_by_vehicle()
    {
        $seller = Builders::aUser()->build();
        $buyer = Builders::aUser()->matrixPartner()->withFunds(100)->build();

        Builders::aCompany()->withUser($buyer)->withCity('Api City')->build();
        $vehicle = Builders::aVehicle()->withOwner($seller)->build();

        $this->signIn($buyer);

        $response = $this->post('/associates/marketplace/add-new/' . $vehicle->id, [
            'valueGreat' => 600,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'drop_off',
            'fee' => 0,
        ]);

        $this->assertEquals(302, $response->status());

        /** @var Valuation $createdValuation */
        $createdValuation = Valuation::orderBy('id', 'DESC')->first();

        $buyerFromDb = User::where('id', '=', $buyer->id)->first();

        $this->assertEquals(600, $createdValuation->getOriginal('dropoff_value'));
        $this->assertEquals(98, $buyerFromDb->funds->funds);
    }

    /**
     * @test
     * @expectedException \Illuminate\Validation\ValidationException
     */
    public function it_does_not_allow_create_valuation_with_lower_value_by_vehicle()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        $this->signIn($buyer);

        $this->post('/associates/marketplace/add-new/' . $vehicle->id, [
            'valueGreat' => 600,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'collection',
        ]);
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\VehicleExpiredException
     */
    public function it_does_not_allow_show_old_vehicles_offers()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        Carbon::setTestNow(Carbon::create(2018, 2, 5, 11));

        $this->signIn($buyer);

        $this->get('/associates/marketplace/add-new/' . $vehicle->id);
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\VehicleExpiredException
     */
    public function it_does_not_allow_send_offers_for_old_vehicles_offers()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        Carbon::setTestNow(Carbon::create(2018, 2, 5, 11));

        $this->signIn($buyer);

        $this->post('/associates/marketplace/add-new/' . $vehicle->id, [
            'valueGreat' => 600,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'collection',
        ]);
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\VehicleExpiredException
     */
    public function it_does_not_allow_show_old_leads()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        Carbon::setTestNow(Carbon::create(2018, 2, 5, 11));

        $this->signIn($buyer);

        $this->get('/associates/marketplace/' . $higestValuation->id);
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\VehicleExpiredException
     */
    public function it_does_not_allow_send_offers_for_old_leads()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        Carbon::setTestNow(Carbon::create(2018, 2, 5, 11));

        $this->signIn($buyer);

        $this->get('/associates/marketplace/' . $higestValuation->id);
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\InsufficientFundsException
     */
    public function it_does_create_leads_if_user_have_insufficient_funds()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        /** @var UserFund $funds */
        $funds = $buyer->funds;
        $funds->funds = 0;
        $funds->save();

        $this->signIn($buyer);

        $this->post('/associates/marketplace/add-new/' . $vehicle->id, [
            'valueGreat' => 1600,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'collection',
        ]);
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\InsufficientFundsException
     */
    public function it_does_create_leads_if_user_have_insufficient_funds_leads_form()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        /** @var UserFund $funds */
        $funds = $buyer->funds;
        $funds->funds = 0;
        $funds->save();

        $this->signIn($buyer);

        $response = $this->post('/associates/marketplace/' . $higestValuation->id, [
            'valueGreat' => 1600,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'collection',
        ]);

        $this->assertEquals(302, $response->status());
        $this->assertContains('Redirecting to', $response->getContent());
        $this->assertContains('/associates/funds', $response->getContent());
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\VehicleExpiredException
     */
    public function it_does_not_allow_send_offers_for_vehicles_with_sales()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        Builders::aSale()->ofValuation($higestValuation)->completed()->build();

        $this->signIn($buyer);

        $this->post('/associates/marketplace/add-new/' . $vehicle->id, [
            'valueGreat' => 4000,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'collection',
        ]);
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\VehicleExpiredException
     */
    public function it_does_not_allow_send_offers_for_leads_of_vehicles_with_sales()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        Builders::aSale()->ofValuation($higestValuation)->completed()->build();

        $this->signIn($buyer);

        $this->post('/associates/marketplace/' . $higestValuation->id, [
            'valueGreat' => 4000,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'collection',
        ]);
    }

    /** @test */
    public function it_allows_show_create_lead_form_for_vehicles_with_rejected_sale()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        Builders::aSale()->ofValuation($higestValuation)->rejected()->build();

        $this->signIn($buyer);

        $response = $this->post('/associates/marketplace/add-new/' . $vehicle->id, [
            'valueGreat' => 4000,
            'valueGood' => 500,
            'valueFair' => 400,
            'valuePoor' => 300,
            'delivery_method' => 'collection',
        ]);

        $this->assertEquals(302, $response->getStatusCode());

        /** @var Valuation $newValuation */
        $newValuation = Valuation::orderBy('id', 'desc')->first();

        $this->assertEquals(4000, $newValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_returns_fee()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation) =
            $this->loadTestData(100, 500, 1500);

        Builders::aSale()->ofValuation($higestValuation)->rejected()->build();

        $this->signIn($buyer);

        $response = $this->get('/associates/marketplace/fee?value=400');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"fee":"2.00"}', $response->getContent());
    }

    private function loadTestData($buyerFunds, $lowerValuationPrice, $higherValuationPrice)
    {
        Carbon::setTestNow(Carbon::create(2018, 1, 1, 11));

        $seller = Builders::aUser()->build();

        $buyer = Builders::aUser()
            ->matrixPartner()
            ->withFunds($buyerFunds)
            ->build();

        $buyerCompany = Builders::aCompany()
            ->withUser($buyer)
            ->withCity('Api City')
            ->build();

        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->addedToMarketplace()
            ->build();

        $valuationUuid = Uuid::uuid4();

        $lowerValuation = Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid($valuationUuid)
            ->withCollectionValue($lowerValuationPrice)
            ->withVehicle($vehicle)
            ->build();

        $higestValuation = Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid($valuationUuid)
            ->withCollectionValue($higherValuationPrice)
            ->withVehicle($vehicle)
            ->build();

        Carbon::setTestNow(Carbon::create(2018, 1, 1, 12));

        return [$seller, $buyer, $buyerCompany, $vehicle, $lowerValuation, $higestValuation];
    }

}
