<?php
namespace Tests\Functional;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;

class PartnerWebsiteTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_allows_create_offer_on_partner_scrap_dealer_website()
    {
        $companySlug = 'scrap-company';
        $plateNumber = 'EXMPL11';

        $valuationUuid = Uuid::uuid4();
        $user = Builders::aUser()->build();

        $matrixScrapDealerUser = Builders::aUser()->matrixPartner()->scrapDealer()->build();
        $matrixScrapDealerCompany = Builders::aCompany()
            ->withSlug($companySlug)
            ->withUser($matrixScrapDealerUser)
            ->build();

        $vehicle = Builders::aVehicle()
            ->withNumberPlate($plateNumber)
            ->withOwner($user)
            ->build();

        $valuation = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($matrixScrapDealerCompany)
            ->withUuid($valuationUuid)
            ->withDropOffValue(600)
            ->withVehicle($vehicle)
            ->build();

        $this->signIn($user);

        $this->get('/valuations/saved/' . $valuationUuid);

        $this->patch('/partner-sites/'. $companySlug .'/your-offer', [
            'company_id' => $matrixScrapDealerCompany->id,
            'uuid' => $valuationUuid->toString(),
            'vid' => $valuation->id,
            'vrm' => $vehicle->numberplate,
        ], ['X-Requested-With' => 'XMLHttpRequest']);

        $response = $this->get('/partner-sites/'. $companySlug .'/your-offer');

        $this->assertEquals(200, $response->status());
        $responseContent = $response->content();

        $this->assertNotContains(
            'Adjust valuation based on your vehicles condition.',
            $responseContent,
            'something went wrong with this assertion'
        );
    }

    /** @test */
    public function it_allows_create_offer_on_partner_non_scrap_dealer_website()
    {
        $companySlug = 'quality-company';
        $plateNumber = 'EXMPL12';

        $valuationUuid = Uuid::uuid4();
        $user = Builders::aUser()->build();

        $matrixScrapDealerUser = Builders::aUser()->matrixPartner()->build();
        $matrixScrapDealerCompany = Builders::aCompany()
            ->withSlug($companySlug)
            ->withUser($matrixScrapDealerUser)
            ->build();

        $vehicle = Builders::aVehicle()
            ->withNumberPlate($plateNumber)
            ->withOwner($user)
            ->build();

        $valuation = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($matrixScrapDealerCompany)
            ->withUuid($valuationUuid)
            ->withDropOffValue(600)
            ->withGoodValue(400)
            ->withVehicle($vehicle)
            ->build();

        $this->signIn($user);

        $this->get('/valuations/saved/' . $valuationUuid);

        $this->patch('/partner-sites/'. $companySlug .'/your-offer', [
            'company_id' => $matrixScrapDealerCompany->id,
            'uuid' => $valuationUuid->toString(),
            'vid' => $valuation->id,
            'vrm' => $vehicle->numberplate,
        ], ['X-Requested-With' => 'XMLHttpRequest']);

        $response = $this->get('/partner-sites/'. $companySlug .'/your-offer');

        $this->assertEquals(200, $response->status());
        $responseContent = $response->content();

        $this->assertContains(
            'Additional details',
            $responseContent,
            'something went wrong with this assertion'
        );
    }

    /** @test */
    public function it_sends_emails_to_seller_and_buyer_after_complete_transaction_non_scrap_dealer_drop_off()
    {
        $companySlug = 'quality-company-buy';
        $plateNumber = 'EXMPL13';
        $postCode = 'M1+1AE';

        $valuationUuid = Uuid::uuid4();
        $user = Builders::aUser()->withPostCode($postCode)->build();

        $matrixNonScrapDealerUser = Builders::aUser()->matrixPartner()->build();
        $matrixNonScrapDealerCompany = Builders::aCompany()
            ->withSlug($companySlug)
            ->withUser($matrixNonScrapDealerUser)
            ->build();

        $vehicle = Builders::aVehicle()
            ->withNumberPlate($plateNumber)
            ->withOwner($user)
            ->build();

        $valuation = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($matrixNonScrapDealerCompany)
            ->withUuid($valuationUuid)
            ->withDropOffValue(600)
            ->withVehicle($vehicle)
            ->build();

        $this->signIn($user);

        $this->get('/valuations/saved/' . $valuationUuid);
        $this->patch('/partner-sites/'. $companySlug .'/your-offer', [
            'company_id' => $matrixNonScrapDealerCompany->id,
            'uuid' => $valuationUuid->toString(),
            'vid' => $valuation->id,
            'vrm' => $vehicle->numberplate,
        ], ['X-Requested-With' => 'XMLHttpRequest']);
        $this->get('/partner-sites/'. $companySlug .'/your-offer');

        $response = $this->post(sprintf('/valuations/%s/%s', $plateNumber, $matrixNonScrapDealerCompany->id), [
            'address_line_one' => 'Example one',
            'address_line_two' => 'Example two',
            'condition' => 'great',
            'deliveryMethod' => 'dropoff',
            'email' => 'example_customer@example.com',
            'firstname' => 'Example First',
            'lastname' => 'Example Last',
            'postcode' => $postCode,
            'price' => '600',
            'telephone_number' => '+44+7107+881234',
            'town' => 'Example Town',
            'vid' => encrypt($valuation->id),
            'keys-count' =>2,
            '_token' => csrf_token(),
        ]);

        $this->assertEquals(302, $response->status());
        $responseContent = $response->content();

        $this->assertContains(
            sprintf('/partner-sites/%s/offer-complete', $companySlug),
            $responseContent,
            'something went wrong with this assertion'
        );
    }

    /** @test */
    public function it_sends_emails_to_seller_and_buyer_after_complete_transaction_scrap_dealer_collection()
    {
        $companySlug = 'scrap-company-buy';
        $plateNumber = 'EXMPL13';
        $postCode = 'M1+1AE';

        $valuationUuid = Uuid::uuid4();
        $user = Builders::aUser()->withPostCode($postCode)->build();

        $matrixScrapDealerUser = Builders::aUser()->matrixPartner()->scrapDealer()->build();
        $matrixScrapDealerCompany = Builders::aCompany()
            ->withSlug($companySlug)
            ->withUser($matrixScrapDealerUser)
            ->build();

        $vehicle = Builders::aVehicle()
            ->withNumberPlate($plateNumber)
            ->withOwner($user)
            ->build();

        $valuation = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($matrixScrapDealerCompany)
            ->withUuid($valuationUuid)
            ->withCollectionValue(700)
            ->withVehicle($vehicle)
            ->build();

        $this->signIn($user);

        $this->get('/valuations/saved/' . $valuationUuid);
        $this->patch('/partner-sites/'. $companySlug .'/your-offer', [
            'company_id' => $matrixScrapDealerCompany->id,
            'uuid' => $valuationUuid->toString(),
            'vid' => $valuation->id,
            'vrm' => $vehicle->numberplate,
        ], ['X-Requested-With' => 'XMLHttpRequest']);
        $this->get('/partner-sites/'. $companySlug .'/your-offer');

        $response = $this->post(sprintf('/valuations/%s/%s', $plateNumber, $matrixScrapDealerCompany->id), [
            'address_line_one' => 'Example one',
            'address_line_two' => 'Example two',
            'deliveryMethod' => 'collection',
            'condition' => 'great',
            'email' => 'example_customer@example.com',
            'firstname' => 'Example First',
            'lastname' => 'Example Last',
            'postcode' => $postCode,
            'price' => '600',
            'telephone_number' => '+44+7107+881234',
            'town' => 'Example Town',
            'vid' => encrypt($valuation->id),
            'keys-count' =>2,
            '_token' => csrf_token(),
        ]);

        $this->assertEquals(302, $response->status());
        $responseContent = $response->content();

        $this->assertContains(
            sprintf('/partner-sites/%s/offer-complete', $companySlug),
            $responseContent,
            'something went wrong with this assertion'
        );
    }

}
