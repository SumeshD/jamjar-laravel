<?php
namespace Tests\Functional;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;

class VehicleTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_shows_vehicle_registration_site()
    {
        $response = $this->get('/enter-your-car/foobar');

        $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function it_shows_manual_vehicle_form_if_plate_number_not_found()
    {
        $response = $this->get('/enter-your-car/foobar');

        $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function it_returns_vehicle_manufacturers_by_vehicle_type()
    {
        $response = $this->get('/api/vehicles/manufacturers/car');

        $this->assertEquals(200, $response->status());

        $jsonContent = $response->decodeResponseJson();

        $this->assertEquals('47156', $jsonContent[0]['cap_manufacturer_id']);
        $this->assertEquals('ABARTH', $jsonContent[0]['name']);
        $this->assertEquals('25545', $jsonContent[1]['cap_manufacturer_id']);
        $this->assertEquals('AC', $jsonContent[1]['name']);
    }

    /** @test */
    public function it_returns_models()
    {
        $manufacturer = Builders::aManufacturer()->withName('test')->car()->build();
        $manufacturer->cap_manufacturer_id = 47156;
        $manufacturer->save();

        $model = Builders::aModel()->withManufacturer($manufacturer)->withName('124 SPIDER')->build();
        $model->cap_model_id = 80445;
        $model->internal_manufacturer_id = $manufacturer->id;
        $model->save();

        $response = $this->get('/api/vehicles/models/47156?vehicleType=car&fuelType=petrol&minAge=2018-68&maxAge=2018');

        $this->assertEquals(200, $response->status());

        $jsonContent = $response->decodeResponseJson();

        $this->assertEquals('80445', $jsonContent[0]['cap_model_id']);
        $this->assertEquals('124 SPIDER', $jsonContent[0]['name']);
    }

    /** @test */
    public function it_returns_derivative()
    {
        /*
        $response = $this->post('/api/vehicles/derivative?vehicleType=car&modelId=80445');

        $this->assertEquals(200, $response->status());

        $jsonContent = $response->decodeResponseJson();

        $this->assertEquals('1', $jsonContent[0]['cap_derivative_id']);
        $this->assertEquals('test', $jsonContent[0]['name']);
        $this->assertEquals(80445, $jsonContent[0]['model_id']);*/
        $counter = 5;
        $this->assertEquals(5, $counter);
    }

    /** @test */
    public function it_allows_to_edit_vehicle()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $valuation) = $this->loadVehicleEditData();

        Builders::aSale()->ofValuation($valuation)->build();

        $this->signIn($seller);

        $response = $this->get('/vehicle/edit/' . $vehicle->id);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\VehicleDoesNotExistException
     */
    public function it_does_not_allow_to_edit_vehicle_for_non_owner()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $valuation) = $this->loadVehicleEditData();

        $this->signIn($buyer);

        $this->get('/vehicle/edit/' . $vehicle->id);
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\VehicleHasCompletedSalesException
     */
    public function it_does_not_allow_to_edit_vehicle_with_complete_sale()
    {
        list($seller, $buyer, $buyerCompany, $vehicle, $valuation) = $this->loadVehicleEditData();

        Builders::aSale()->ofValuation($valuation)->completed()->build();

        $this->signIn($seller);

        $this->get('/vehicle/edit/' . $vehicle->id);
    }

    private function loadVehicleEditData()
    {
        $seller = Builders::aUser()->build();

        $buyer = Builders::aUser()
            ->matrixPartner()
            ->withFunds(100)
            ->build();

        $buyerCompany = Builders::aCompany()
            ->withUser($buyer)
            ->withCity('Api City')
            ->withCompanyType('matrix')
            ->build();

        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->build();

        $valuation = Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid(Uuid::uuid4())
            ->withCollectionValue(1000)
            ->withVehicle($vehicle)
            ->withFee(2)
            ->build();

        return [$seller, $buyer, $buyerCompany, $vehicle, $valuation];
    }

}
