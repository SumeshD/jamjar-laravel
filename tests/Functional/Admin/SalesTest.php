<?php
namespace Tests\Functional;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;

class SalesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_return_matrix_sales_page()
    {
        $admin = Builders::aUser()->isAdmin()->build();

        $seller = Builders::aUser()->build();

        $buyer = Builders::aUser()->matrixPartner()->build();
        $buyerCompany = Builders::aCompany()
            ->withUser($buyer)
            ->withCity('Api City')
            ->build();

        $valuationUuid = Uuid::uuid4();

        Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid($valuationUuid)
            ->withCollectionValue(888)
            ->build();

        Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid($valuationUuid)
            ->withCollectionValue(1500)
            ->isSold()
            ->build();

        $this->signIn($admin);

        $response = $this->get('/admin/completed-sales');

        $this->assertEquals(200, $response->status());
        $responseContent = $response->content();

        $this->assertContains(
            '1,500',
            $responseContent,
            'something went wrong with this assertion'
        );

        $this->assertNotContains(
            '888',
            $responseContent,
            'something went wrong with this assertion'
        );
    }

}
