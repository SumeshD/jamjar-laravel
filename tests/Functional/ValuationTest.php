<?php
namespace Tests\Functional;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;

class ValuationTest extends TestCase
{
    use RefreshDatabase;

//    /** @test */
//    public function it_should_be_collected_from_any_location()
//    {
//        $user = Builders::aUser()->build();
//        $company = Builders::aCompany()->isApiCompany()->withCity('Api City')->build();
//
//        $valuationUuid = Uuid::uuid4();
//
//        $vehicle = Builders::aVehicle()->withOwner($user)->build();
//
//        Builders::aValuation()
//            ->ofUser($user)
//            ->ofCompany($company)
//            ->withUuid($valuationUuid)
//            ->withCollectionValue(500)
//            ->withVehicle($vehicle)
//            ->build();
//
//        $this->signIn($user);
//
//        $response = $this->get('/valuations/saved/' . $valuationUuid);
//
//        $this->assertEquals(200, $response->status());
//        $responseContent = $response->content();
//
//        $this->assertContains(
//            'Collection available at',
//            $responseContent,
//            'something went wrong with this assertion'
//        );
//    }

//    /** @test */
//    public function it_should_be_dropped_at_distance()
//    {
//        $user = Builders::aUser()->build();
//        $company = Builders::aCompany()->isApiCompany()->build();
//
//        $valuationUuid = Uuid::uuid4();
//
//        $vehicle = Builders::aVehicle()->withOwner($user)->build();
//
//        Builders::aValuation()
//            ->ofUser($user)
//            ->ofCompany($company)
//            ->withUuid($valuationUuid)
//            ->withDropOffValue(500)
//            ->withDropOffDistance(60)
//            ->withVehicle($vehicle)
//            ->build();
//
//        $this->signIn($user);
//
//        $response = $this->get('/valuations/saved/' . $valuationUuid);
//
//        $this->assertEquals(200, $response->status());
//        $responseContent = $response->content();
//
//        $this->assertContains(
//            'Distance: 60 miles',
//            $responseContent,
//            'something went wrong with this assertion'
//        );
//    }

//    /** @test */
//    public function it_should_be_dropped_at_location()
//    {
//        $valuationUuid = Uuid::uuid4();
//        $user = Builders::aUser()->build();
//
//        $company = Builders::aCompany()->withCity('Example City')->build();
//
//        $vehicle = Builders::aVehicle()->withOwner($user)->build();
//
//        Builders::aValuation()
//            ->ofUser($user)
//            ->ofCompany($company)
//            ->withUuid($valuationUuid)
//            ->withDropOffValue(600)
//            ->withVehicle($vehicle)
//            ->build();
//
//        $this->signIn($user);
//
//        $response = $this->get('/valuations/saved/' . $valuationUuid);
//
//        $this->assertEquals(200, $response->status());
//        $responseContent = $response->content();
//
//        $this->assertContains(
//            'Example City',
//            $responseContent,
//            'something went wrong with this assertion'
//        );
//    }

    /** @test */
    public function it_checks_if_valuation_is_expired()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-10-20 10:10:10'));
        $valuation = Builders::aValuation()->build();

        $this->assertFalse($valuation->isExpired());

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-10-27 10:20:10'));

        $this->assertTrue($valuation->isExpired());
    }

}
