<?php

namespace Tests\Functional\Images;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;

class ImagesTest extends TestCase
{
    use RefreshDatabase;

    private $imagePath = 'tests/example-image.png';

    /** @test */
    public function it_returns_image()
    {
        $uploadedImage = $this->createUploadedImage('image1');

        $this->getImagesManager()->uploadImage(base64_encode(file_get_contents($this->imagePath)), $uploadedImage);

        $response = $this->get('/image/' . $uploadedImage->id()->toString() . '/image.png');

        $response->assertStatus(200);
    }

    /** @test */
    public function it_returns_thumbnail()
    {
        $uploadedImage = $this->createUploadedImage('image1');
        $this->getImagesManager()->uploadImage(base64_encode(file_get_contents($this->imagePath)), $uploadedImage);

        $response = $this->get('/thumbnail/' . $uploadedImage->id()->toString() . '/image.png');

        $response->assertStatus(200);
    }
}
