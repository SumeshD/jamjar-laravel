<?php

namespace Tests\Integration\Images;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;

class UploadedImagesServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_creates_uploaded_image()
    {
        Carbon::setTestNow(Carbon::create(2017, 3, 18, 04, 30));
        $uploadedImage = $this->getUploadedImageManager()->create('stuff.png', 'active');

        $this->assertTrue(Uuid::isValid($uploadedImage->id()->toString()));
        $this->assertEquals($uploadedImage->originalName(), 'stuff.png');
        $this->assertEquals($uploadedImage->status(), 'active');
        $this->assertEquals($uploadedImage->createdAt()->toDateTimeString(), '2017-03-18 04:30:00');
        $this->assertEquals($uploadedImage->updatedAt()->toDateTimeString(), '2017-03-18 04:30:00');
    }

    /** @test */
    public function it_edits_uploaded_image()
    {
        Carbon::setTestNow(Carbon::create(2017, 3, 18, 04, 30));
        $uploadedImage = $this->createUploadedImage('test');
        $imageId = $uploadedImage->id();

        Carbon::setTestNow(Carbon::create(2017, 3, 18, 05, 30));
        $this->getUploadedImageManager()->edit(
            $uploadedImage,
            'changed_original_file.png',
            'deleted'
        );

        $editedUploadedImage = $this->getUploadedImageManager()->getById($imageId);

        $this->assertEquals($editedUploadedImage->originalName(), 'changed_original_file.png');
        $this->assertEquals($editedUploadedImage->status(), 'deleted');
        $this->assertEquals($editedUploadedImage->createdAt()->toDateTimeString(), '2017-03-18 04:30:00');
        $this->assertEquals($editedUploadedImage->updatedAt()->toDateTimeString(), '2017-03-18 05:30:00');
    }
}
