<?php
namespace Tests\Integration\Entities;

use Carbon\Carbon;
use JamJar\Company;
use JamJar\Sale;
use JamJar\Services\VehicleFilter\VehicleFilters;
use JamJar\User;
use JamJar\Vehicle;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VehicleTest extends TestCase
{
    use RefreshDatabase;

    /** @var User */
    private $matrix;

    /** @var Company */
    private $matrixCompany;

    /** @var User */
    private $anotherMatrix;

    /** @var Company */
    private $anotherMatrixCompany;

    /** @var Company */
    private $apiCompany;

    /** @var Vehicle */
    private $bmw;

    /** @var Vehicle */
    private $mazda;

    public function setUp()
    {
        parent::setUp();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-01 00:00:01'));

        $vehicleOwner = Builders::aUser()->build();

        $this->matrix = Builders::aUser()->matrixPartner()->build();
        $this->matrixCompany = Builders::aCompany()->withUser($this->matrix)->build();

        $this->apiCompany = Builders::aCompany()->isApiCompany()->build();

        $this->anotherMatrix = Builders::aUser()->matrixPartner()->build();
        $this->anotherMatrixCompany = Builders::aCompany()->withUser($this->anotherMatrix)->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-01 01:00:01'));
        $this->bmw = Builders::aVehicle()->withNumberPlate('BMW')->withOwner($vehicleOwner)->addedToMarketplace()->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-02 01:00:01'));
        Builders::aValuation()
            ->ofVehicle($this->bmw)
            ->ofCompany($this->apiCompany)
            ->withCollectionValue(1000)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-03 01:00:01'));
        $this->mazda = Builders::aVehicle()->withNumberPlate('mazda')->withOwner($vehicleOwner)->addedToMarketplace()->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-04 01:00:01'));
        Builders::aValuation()
            ->ofVehicle($this->mazda)
            ->ofCompany($this->apiCompany)
            ->withCollectionValue(1000)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-05 01:00:01'));
    }

    /** @test */
    public function it_should_return_ordered_leads()
    {
        $leads = Vehicle::getLeadsForMatrix($this->matrix, new VehicleFilters());

        $this->assertEquals(2, count($leads));

        $this->assertEquals('mazda', $leads[0]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[0]->highestValuation->getOriginal('collection_value'));

        $this->assertEquals('BMW', $leads[1]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[1]->highestValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_should_return_ordered_lead_after_another_matrix_offer_placement()
    {
        Builders::aValuation()
            ->ofVehicle($this->bmw)
            ->withCollectionValue(1500)
            ->ofUser($this->anotherMatrix)
            ->ofCompany($this->anotherMatrixCompany)
            ->build();

        $leads = Vehicle::getLeadsForMatrix($this->matrix, new VehicleFilters());

        $this->assertEquals(2, count($leads));

        $this->assertEquals('mazda', $leads[0]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[0]->highestValuation->getOriginal('collection_value'));

        $this->assertEquals('BMW', $leads[1]->vehicle->getNumberPlate());
        $this->assertEquals(1500, $leads[1]->highestValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_does_not_show_vehicles_after_my_bid()
    {
        Builders::aValuation()
            ->ofVehicle($this->bmw)
            ->withCollectionValue(2000)
            ->ofUser($this->matrix)
            ->ofCompany($this->matrixCompany)
            ->build();

        $leads = Vehicle::getLeadsForMatrix($this->matrix, new VehicleFilters());

        $this->assertEquals(1, count($leads));

        $this->assertEquals('mazda', $leads[0]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[0]->highestValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_does_not_show_vehicles_after_other_matrix_better_valuation_placed()
    {
        Builders::aValuation()
            ->ofVehicle($this->bmw)
            ->withCollectionValue(2000)
            ->ofUser($this->matrix)
            ->ofCompany($this->matrixCompany)
            ->build();

        Builders::aValuation()
            ->ofVehicle($this->bmw)
            ->withCollectionValue(3000)
            ->ofUser($this->anotherMatrix)
            ->ofCompany($this->anotherMatrixCompany)
            ->build();

        $leads = Vehicle::getLeadsForMatrix($this->matrix, new VehicleFilters());

        $this->assertEquals(1, count($leads));

        $this->assertEquals('mazda', $leads[0]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[0]->highestValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_does_not_shows_lead_when_client_accepted_another_matrix_valuation()
    {
        Builders::aValuation()
            ->ofVehicle($this->bmw)
            ->withCollectionValue(2000)
            ->ofUser($this->anotherMatrix)
            ->ofCompany($this->anotherMatrixCompany)
            ->isAccepted()
            ->build();

        $leads = Vehicle::getLeadsForMatrix($this->matrix, new VehicleFilters());

        $this->assertEquals(1, count($leads));

        $this->assertEquals('mazda', $leads[0]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[0]->highestValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_shows_lead_when_client_accepted_another_matrix_valuation_and_then_it_is_rejected()
    {
        Builders::aValuation()
            ->ofVehicle($this->bmw)
            ->withCollectionValue(2000)
            ->ofUser($this->anotherMatrix)
            ->ofCompany($this->anotherMatrixCompany)
            ->isDeclined()
            ->build();

        $leads = Vehicle::getLeadsForMatrix($this->matrix, new VehicleFilters());

        $this->assertEquals(2, count($leads));

        $this->assertEquals('mazda', $leads[0]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[0]->highestValuation->getOriginal('collection_value'));

        $this->assertEquals('BMW', $leads[1]->vehicle->getNumberPlate());
        $this->assertEquals(2000, $leads[1]->highestValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_hides_expired_valuations()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-07 02:10:10'));

        $leads = Vehicle::getLeadsForMatrix($this->matrix, new VehicleFilters());

        $this->assertEquals(2, count($leads));

        $this->assertEquals('mazda', $leads[0]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[0]->highestValuation->getOriginal('collection_value'));
        $this->assertEquals(null, $leads[0]->highestExpiredValuation);

        $this->assertEquals('BMW', $leads[1]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[1]->highestExpiredValuation->getOriginal('collection_value'));
        $this->assertEquals(1000, $leads[1]->highestExpiredValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_hides_sales_after_30_days()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-02-02 02:10:10'));

        $leads = Vehicle::getLeadsForMatrix($this->matrix, new VehicleFilters());

        $this->assertEquals(1, count($leads));

        $this->assertEquals('mazda', $leads[0]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[0]->highestExpiredValuation->getOriginal('collection_value'));
        $this->assertEquals(1000, $leads[0]->highestExpiredValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_shows_vehicles_without_valuations()
    {
        $vehicleOwner = Builders::aUser()->build();
        Builders::aVehicle()->withNumberPlate('volkswagen')->withOwner($vehicleOwner)->addedToMarketplace()->build();

        $leads = Vehicle::getLeadsForMatrix($this->matrix, new VehicleFilters());

        $this->assertEquals(3, count($leads));

        $this->assertEquals('volkswagen', $leads[0]->vehicle->getNumberPlate());
        $this->assertEquals(null, $leads[0]->highestValuation);
        $this->assertEquals(null, $leads[0]->highestExpiredValuation);

        $this->assertEquals('mazda', $leads[1]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[1]->highestValuation->getOriginal('collection_value'));

        $this->assertEquals('BMW', $leads[2]->vehicle->getNumberPlate());
        $this->assertEquals(1000, $leads[2]->highestValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_returns_good_news_lead_by_vehicle()
    {
        $vehicle = Builders::aVehicle()->withNumberPlate('volkswagen')->addedToMarketplace()->build();

        $matrix1 = Builders::aUser()->matrixPartner()->build();
        $company1 = Builders::aCompany()->withUser($matrix1)->withValuationValidity(2)->build();

        $matrix2 = Builders::aUser()->matrixPartner()->build();
        $company2 = Builders::aCompany()->withUser($matrix2)->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-01 00:00:01'));

        $valuation1 = Builders::aValuation()->withVehicle($vehicle)->withCollectionValue(2000)->ofCompany($company1)->build();
        $valuation2 = Builders::aValuation()->withVehicle($vehicle)->withCollectionValue(3000)->ofCompany($company1)->build();
        $valuation3 = Builders::aValuation()->withVehicle($vehicle)->withCollectionValue(4000)->ofCompany($company2)->build();
        $valuation4 = Builders::aValuation()->withVehicle($vehicle)->withCollectionValue(5000)->ofCompany($company2)->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-04 00:00:01'));

        $goodNewsLead = Vehicle::getGoodNewsLeadByVehicle($vehicle);

        $this->assertEquals(3000, $goodNewsLead->highestExpiredValuation->getOriginal('collection_value'));
        $this->assertEquals(5000, $goodNewsLead->highestValuation->getOriginal('collection_value'));
    }

    /** @test */
    public function it_returns_leads_only_if_matrix_does_not_created_valuation_yet()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-01 00:00:01'));

        $matrix = Builders::aUser()->matrixPartner()->build();
        $company = Builders::aCompany()->withUser($matrix)->withValuationValidity(2)->build();

        $vehicle = Builders::aVehicle()->withNumberPlate('volkswagen')->build();
        $valuation = Builders::aValuation()->withVehicle($vehicle)->withCollectionValue(2000)->ofCompany($company)->build();

        list($count, $vehicles) = Vehicle::getRawLeadsVehiclesForMatrix($matrix, new VehicleFilters());
        $leads = Vehicle::getLeadsByRawVehicles($vehicles);

        $this->assertEquals(2, count($leads));
    }

    /** @test */
    public function it_returns_offers_received()
    {
        $seller = Builders::aUser()->matrixPartner()->build();
        $vehicleWithoutValuations = Builders::aVehicle()->withOwner($seller)->addedToMarketplace()->build();
        $vehicleWithoutSale = Builders::aVehicle()->withOwner($seller)->addedToMarketplace()->build();
        $vehicleWithSale = Builders::aVehicle()->withOwner($seller)->addedToMarketplace()->build();

        $buyerWithoutSale = Builders::aUser()->matrixPartner()->build();
        $valuationWithoutSale = Builders::aValuation()->ofVehicle($vehicleWithoutSale)->ofUser($buyerWithoutSale)->build();

        $buyerWithSale = Builders::aUser()->matrixPartner()->build();
        $valuationWithSale = Builders::aValuation()->ofVehicle($vehicleWithSale)->ofUser($buyerWithSale)->build();
        Builders::aSale()->ofValuation($valuationWithSale)->rejected()->build();

        /** @var Vehicle[] $results */
        $results = Vehicle::getOffersReceivedForUserQuery($seller)->get();

        $this->assertEquals(1, count($results));
        $this->assertEquals($vehicleWithoutSale->getId(), $results[0]->getId());
    }

    public function it_returns_good_news_lead_in_acceptable_amount_of_time()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-01 00:00:01'));

        $matrix1 = Builders::aUser()->matrixPartner()->build();
        $company1 = Builders::aCompany()->withUser($matrix1)->withValuationValidity(2)->build();

        $matrix2 = Builders::aUser()->matrixPartner()->build();
        $company2 = Builders::aCompany()->withUser($matrix2)->build();

        $matrix3 = Builders::aUser()->matrixPartner()->build();
        $company3 = Builders::aCompany()->withUser($matrix3)->build();

        for ($i = 0; $i < 1000; $i++) {
            $vehicle = Builders::aVehicle()->withNumberPlate('volkswagen')->build();
            $valuation1 = Builders::aValuation()->withVehicle($vehicle)->withCollectionValue(2000)->ofCompany($company1)->build();
            $valuation2 = Builders::aValuation()->withVehicle($vehicle)->withCollectionValue(3000)->ofCompany($company1)->build();
            $valuation3 = Builders::aValuation()->withVehicle($vehicle)->withCollectionValue(4000)->ofCompany($company2)->build();
            $valuation4 = Builders::aValuation()->withVehicle($vehicle)->withCollectionValue(5000)->ofCompany($company2)->build();
        }

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-04 00:00:01'));

        $start = microtime(true);
        $vehicles = Vehicle::getRawLeadsVehiclesForMatrix($matrix3, new VehicleFilters());
        $leads = Vehicle::getLeadsByRawVehicles($vehicles);
        $stop = microtime(true);

        $this->assertEquals(15, count($leads));
        $this->assertLessThan(0.10, $stop-$start);
    }

}
