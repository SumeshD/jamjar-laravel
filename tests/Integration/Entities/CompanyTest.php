<?php
namespace Tests\Integration\Entities;

use Tests\builders\Builders;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_show_vehicle_condition_for_matrix_non_scrap_dealer_only()
    {
        $matrixScrapDealerUser = Builders::aUser()->matrixPartner()->scrapDealer()->build();
        $matrixScrapDealerCompany = Builders::aCompany()->withUser($matrixScrapDealerUser)->build();

        $matrixNonScrapDealerUser = Builders::aUser()->matrixPartner()->nonScrapDealer()->build();
        $matrixNonScrapDealerCompany = Builders::aCompany()->withUser($matrixNonScrapDealerUser)->build();

        $associateUser = Builders::aUser()->associate()->build();
        $associateCompany = Builders::aCompany()->withUser($associateUser)->build();

        $this->assertFalse($matrixScrapDealerCompany->isVehicleConditionAvailable());
        $this->assertTrue($matrixNonScrapDealerCompany->isVehicleConditionAvailable());
        $this->assertFalse($associateCompany->isVehicleConditionAvailable());
    }

}
