<?php
namespace Tests\Integration\Entities;

use Carbon\Carbon;
use JamJar\Valuation;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ValuationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_be_collected_from_any_location()
    {
        $user = Builders::aUser()->build();
        $company = Builders::aCompany()->isApiCompany()->build();

        $valuationUuid = Uuid::uuid4();

        $valuation = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($company)
            ->withUuid($valuationUuid)
            ->withCollectionValue(500)
            ->build();

        $this->assertTrue($valuation->shouldBeCollectedFromAnyLocation());
        $this->assertFalse($valuation->shouldBeCollectedFromClientLocation());
        $this->assertFalse($valuation->shouldBeDroppedAtDistance());
        $this->assertFalse($valuation->shouldBeDroppedAtLocation());
    }

    /** @test */
    public function it_should_be_collected_from_client_location()
    {
        $user = Builders::aUser()->build();
        $company = Builders::aCompany()->build();

        $valuationUuid = Uuid::uuid4();

        $valuation = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($company)
            ->withUuid($valuationUuid)
            ->withCollectionValue(500)
            ->build();

        $this->assertFalse($valuation->shouldBeCollectedFromAnyLocation());
        $this->assertTrue($valuation->shouldBeCollectedFromClientLocation());
        $this->assertFalse($valuation->shouldBeDroppedAtDistance());
        $this->assertFalse($valuation->shouldBeDroppedAtLocation());
    }

    /** @test */
    public function it_should_be_dropped_at_distance()
    {
        $user = Builders::aUser()->build();
        $company1 = Builders::aCompany()->isApiCompany()->build();

        $valuationUuid = Uuid::uuid4();

        $valuation1 = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($company1)
            ->withUuid($valuationUuid)
            ->withDropOffValue(500)
            ->withDropOffDistance(500)
            ->build();

        $company2 = Builders::aCompany()->isApiCompany()->build();

        $valuation2 = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($company2)
            ->withUuid($valuationUuid)
            ->withDropOffValue(500)
            ->withCollectionValue(700)
            ->withDropOffDistance(500)
            ->build();

        $this->assertFalse($valuation1->shouldBeCollectedFromAnyLocation());
        $this->assertFalse($valuation1->shouldBeCollectedFromClientLocation());
        $this->assertTrue($valuation1->shouldBeDroppedAtDistance());
        $this->assertFalse($valuation1->shouldBeDroppedAtLocation());

        $this->assertFalse($valuation2->shouldBeCollectedFromAnyLocation());
        $this->assertFalse($valuation2->shouldBeCollectedFromClientLocation());
        $this->assertTrue($valuation2->shouldBeDroppedAtDistance());
        $this->assertFalse($valuation2->shouldBeDroppedAtLocation());
    }

    /** @test */
    public function it_should_be_dropped_at_location()
    {
        $valuationUuid = Uuid::uuid4();
        $user = Builders::aUser()->build();
        $company1 = Builders::aCompany()->isApiCompany()->build();

        $valuation1 = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($company1)
            ->withUuid($valuationUuid)
            ->withDropOffValue(500)
            ->build();

        $company2 = Builders::aCompany()->isApiCompany()->build();

        $valuation2 = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($company2)
            ->withUuid($valuationUuid)
            ->withDropOffValue(500)
            ->withCollectionValue(1500)
            ->build();

        $company3 = Builders::aCompany()->build();

        $valuation3 = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($company3)
            ->withUuid($valuationUuid)
            ->withDropOffValue(600)
            ->build();

        $company4 = Builders::aCompany()->build();

        $valuation4 = Builders::aValuation()
            ->ofUser($user)
            ->ofCompany($company4)
            ->withUuid($valuationUuid)
            ->withDropOffValue(700)
            ->withCollectionValue(1700)
            ->build();

        $this->assertTrue($valuation1->shouldBeDroppedAtLocation());
        $this->assertTrue($valuation2->shouldBeDroppedAtLocation());
        $this->assertTrue($valuation3->shouldBeDroppedAtLocation());
        $this->assertTrue($valuation4->shouldBeDroppedAtLocation());
    }

    /** @test */
    public function it_returns_m4ym_valuation_by_numberplate()
    {
        Carbon::setTestNow(Carbon::create(2018, 2, 5, 11));

        $user = Builders::aUser()->build();

        $numberplate = 'XYZ1234';

        $companyM4YM = Builders::aCompany()->withName('Money4yourMotors')->build();
        $otherCompany = Builders::aCompany()->withName('Other company')->build();

        $vehicle1 = Builders::aVehicle()->withNumberPlate($numberplate)->withOwner($user)->build();

        $valuation1 = Builders::aValuation()
            ->ofUser($user)
            ->withDropOffValue(600)
            ->ofCompany($companyM4YM)
            ->withVehicle($vehicle1)
            ->build();

        Carbon::setTestNow(Carbon::create(2018, 2, 6, 11));

        $vehicle2 = Builders::aVehicle()->withNumberPlate($numberplate)->withOwner($user)->build();

        $valuation2 = Builders::aValuation()
            ->ofUser($user)
            ->withDropOffValue(600)
            ->withVehicle($vehicle2)
            ->ofCompany($otherCompany)
            ->build();

        Carbon::setTestNow(Carbon::create(2018, 2, 6, 12));

        $vehicle3 = Builders::aVehicle()->withNumberPlate($numberplate)->withOwner($user)->build();

        $valuation3 = Builders::aValuation()
            ->ofUser($user)
            ->withDropOffValue(600)
            ->withVehicle($vehicle3)
            ->ofCompany($companyM4YM)
            ->build();

        Carbon::setTestNow(Carbon::create(2018, 2, 6, 13));

        $vehicle4 = Builders::aVehicle()->withNumberPlate($numberplate)->withOwner($user)->build();

        $valuation4 = Builders::aValuation()
            ->ofUser($user)
            ->withDropOffValue(600)
            ->withVehicle($vehicle4)
            ->ofCompany($otherCompany)
            ->build();

        Carbon::setTestNow(Carbon::create(2018, 2, 7, 11));

        $vehicle5 = Builders::aVehicle()->withNumberPlate($numberplate)->withOwner($user)->build();

        $valuation5 = Builders::aValuation()
            ->ofUser($user)
            ->withDropOffValue(600)
            ->withVehicle($vehicle5)
            ->ofCompany($companyM4YM)
            ->build();

        $dbValuation = Valuation::getByNumberplateForM4YM($numberplate, '2018-02-06 13:30:00');
        $this->assertEquals($valuation3->id, $dbValuation->id);

        $dbValuation = Valuation::getByNumberplateForM4YM($numberplate, '2018-02-06 11:30:00');
        $this->assertEquals($valuation1->id, $dbValuation->id);

        $dbValuation = Valuation::getByNumberplateForM4YM($numberplate, '2018-02-04 11:30:00');
        $this->assertEquals(null, $dbValuation);
    }

}
