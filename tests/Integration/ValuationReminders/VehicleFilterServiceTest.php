<?php

namespace Tests\Integration\Valuations;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use JamJar\Services\VehicleFilter\VehicleFilters;
use JamJar\Services\VehicleFilter\VehicleFilterService;
use JamJar\Services\VehicleFilter\VehicleManufacturerFilter;
use JamJar\User;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;
use Tests\builders\Builders;
use Tests\TestCase;

class VehicleFilterServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_returns_manufacturers_list()
    {
        /*
        $manufacturer11 = Builders::aManufacturer()->withName('MAZDAN')->lightCommercialVehicle()->build();
        $manufacturer12 = Builders::aManufacturer()->withName('MAZDAN')->car()->build();

        $manufacturer21 = Builders::aManufacturer()->withName('FURT')->lightCommercialVehicle()->build();
        $manufacturer22 = Builders::aManufacturer()->withName('FURT')->car()->build();

        $manufacturer31 = Builders::aManufacturer()->withName('NVM')->lightCommercialVehicle()->build();
        $manufacturer32 = Builders::aManufacturer()->withName('NVM')->car()->build();

        $service = new VehicleFilterService();
        $manufacturersList = $service->getManufacturersList();

        $this->assertEquals('FURT', $manufacturersList->elements[0]->label);
        $this->assertEquals($manufacturer22->getId(), $manufacturersList->elements[0]->manufacturers[0]->getId());
        $this->assertEquals($manufacturer21->getId(), $manufacturersList->elements[0]->manufacturers[1]->getId());

        $this->assertEquals('MAZDAN', $manufacturersList->elements[1]->label);
        $this->assertEquals($manufacturer12->getId(), $manufacturersList->elements[1]->manufacturers[0]->getId());
        $this->assertEquals($manufacturer11->getId(), $manufacturersList->elements[1]->manufacturers[1]->getId());

        $this->assertEquals('NVM', $manufacturersList->elements[2]->label);
        $this->assertEquals($manufacturer32->getId(), $manufacturersList->elements[2]->manufacturers[0]->getId());
        $this->assertEquals($manufacturer31->getId(), $manufacturersList->elements[2]->manufacturers[1]->getId());
        */
        $counter = 2;
        $this->assertEquals(2, $counter);
    }

    /** @test */
    public function it_returns_vehicles_summaries_filtered_by_manufacturers()
    {
        $fixtures = $this->createVehicles();

        $manufacturer1 = $fixtures[0][0];
        $manufacturer2 = $fixtures[0][1];

        $model11 = $fixtures[1][0];
        $model12 = $fixtures[1][1];
        $model21 = $fixtures[1][2];
        $model22 = $fixtures[1][3];

        $buyer = Builders::aUser()->build();
        Builders::aCompany()->withUser($buyer)->build();

        $searchParameters = new VehicleFilters();

        $service = new VehicleFilterService();
        $results = $service->getFilteredResultsSummary($searchParameters, $buyer);

        // manufacturers
        $this->assertEquals(17, $results->vehiclesCount);
        $this->assertEquals($manufacturer2->getId(), $results->manufacturersSummaries[0]->manufacturer->getId());
        $this->assertEquals(12, $results->manufacturersSummaries[0]->vehiclesCount);

        $this->assertEquals($manufacturer1->getId(), $results->manufacturersSummaries[1]->manufacturer->getId());
        $this->assertEquals(5, $results->manufacturersSummaries[1]->vehiclesCount);

        // models
        $this->assertEquals(7, $results->modelsSummaries[0]->vehiclesCount);
        $this->assertEquals($model22->getId(), $results->modelsSummaries[0]->model->getId());

        $this->assertEquals(5, $results->modelsSummaries[1]->vehiclesCount);
        $this->assertEquals($model21->getId(), $results->modelsSummaries[1]->model->getId());

        $this->assertEquals(3, $results->modelsSummaries[2]->vehiclesCount);
        $this->assertEquals($model12->getId(), $results->modelsSummaries[2]->model->getId());

        $this->assertEquals(2, $results->modelsSummaries[3]->vehiclesCount);
        $this->assertEquals($model11->getId(), $results->modelsSummaries[3]->model->getId());
    }

    /** @test */
    public function it_returns_vehicles_summaries_filtered_by_manufacturers_and_models()
    {
        $fixtures = $this->createVehicles();

        $manufacturer1 = $fixtures[0][0];
        $manufacturer2 = $fixtures[0][1];
        $model11 = $fixtures[1][0];
        $model12 = $fixtures[1][1];
        $model21 = $fixtures[1][2];
        $model22 = $fixtures[1][3];

        $buyer = Builders::aUser()->build();
        Builders::aCompany()->withUser($buyer)->build();

        $searchParameters = new VehicleFilters();
        $manufacturerFilter = new VehicleManufacturerFilter();
        $manufacturerFilter->manufacturersIds[] = $manufacturer1->getId();
        $manufacturerFilter->modelsIds[] = $model12->getId();

        $searchParameters->vehicleManufacturersFilters[] = $manufacturerFilter;

        $service = new VehicleFilterService();
        $results = $service->getFilteredResultsSummary($searchParameters, $buyer);

        // manufacturers
        $this->assertEquals(3, $results->vehiclesCount);
        $this->assertEquals($manufacturer2->getId(), $results->manufacturersSummaries[0]->manufacturer->getId());
        $this->assertEquals(12, $results->manufacturersSummaries[0]->vehiclesCount);

        $this->assertEquals($manufacturer1->getId(), $results->manufacturersSummaries[1]->manufacturer->getId());
        $this->assertEquals(5, $results->manufacturersSummaries[1]->vehiclesCount);

        // models
        $this->assertEquals(7, $results->modelsSummaries[0]->vehiclesCount);
        $this->assertEquals($model22->getId(), $results->modelsSummaries[0]->model->getId());

        $this->assertEquals(5, $results->modelsSummaries[1]->vehiclesCount);
        $this->assertEquals($model21->getId(), $results->modelsSummaries[1]->model->getId());

        $this->assertEquals(3, $results->modelsSummaries[2]->vehiclesCount);
        $this->assertEquals($model12->getId(), $results->modelsSummaries[2]->model->getId());

        $this->assertEquals(2, $results->modelsSummaries[3]->vehiclesCount);
        $this->assertEquals($model11->getId(), $results->modelsSummaries[3]->model->getId());
    }

    /**
     * this is a suspended stress test for filters
     * 60k vehicles in total includes 10k active vehicles (create 30 days or earlier)
     * warning! fixtures creation will take something around 1 hour
     *
     * time before indexes: 6 seconds for manufacturers and 66 seconds for models
     * time after indexes: 0.2 seconds for manufacturers and 1.56 seconds for models
     */
    public function it_returns_filters_fast_enough()
    {
        $buyer = Builders::aUser()->build();
        Builders::aCompany()->withUser($buyer)->build();

        for ($i = 0; $i < 50*2; $i++) {
            $seller = Builders::aUser()->build();

            $manufacturer = Builders::aManufacturer()->withName('SUPERBARU' . $i)->build();
            for ($j = 0; $j < 5*2; $j++) {
                $model = Builders::aModel()->withName('PARTY' . $i)->withManufacturer($manufacturer)->build();
                $derivative = Builders::aDerivative()->withName('XYZ')->withModel($model)->build();
                $this->createVehicle($seller, $manufacturer, $model, $derivative, 5*2);
            }
        }

        Carbon::setTestNow(Carbon::create(2019, 5, 5));

        for ($i = 0; $i < 50*10; $i++) {
            $seller = Builders::aUser()->build();

            $manufacturer = Builders::aManufacturer()->withName('SUPERBARU' . $i)->build();
            for ($j = 0; $j < 5*2; $j++) {
                $model = Builders::aModel()->withName('PARTY' . $i)->withManufacturer($manufacturer)->build();
                $derivative = Builders::aDerivative()->withName('XYZ')->withModel($model)->build();
                $this->createVehicle($seller, $manufacturer, $model, $derivative, 5*2);
            }
        }

        $service = new VehicleFilterService();

        $start = microtime(true);

        $results = $service->getFilteredResultsSummary(new VehicleFilters(), $buyer);

        $executionTime = microtime(true) - $start;

        self::assertLessThan(2, $executionTime);
    }

//    /** @test */
//    public function it_returns_vehicle_summaries_based_on_manufactured_year() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_based_on_previous_owners_count() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_based_on_max_mileage() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_based_on_valuations_max_value() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_based_on_valuation_drafts_max_value() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_based_on_fuel_types() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_filtered_by_created_at() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_filtered_by_runner_status() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_filtered_by_seller_type() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_only_when_vehicle_has_been_added_to_marketplace() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_with_all_filters_mixed() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_without_vehicles_added_by_partner() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_without_sold_vehicles() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_without_vehicles_with_partner_valuation() {}
//
//    /** @test */
//    public function it_returns_vehicle_summaries_with_vehicles_with_rejected_sales() {}

    private function createVehicle(
        User $seller,
        VehicleManufacturer $manufacturer1,
        VehicleModel $model1,
        VehicleDerivative $derivative1,
        int $amount = 1
    ) : array
    {
        $vehicles = [];

        for ($i = 0; $i < $amount; $i++) {
            $vehicles[] = Builders::aVehicle()
                ->ofManufacturer($manufacturer1)
                ->ofModel($model1)
                ->ofDerivative($derivative1)
                ->withOwner($seller)
                ->addedToMarketplace()
                ->build();
        }

        return $vehicles;
    }

    private function createVehicles(): array
    {
        $seller = Builders::aUser()->build();

        $manufacturer1 = Builders::aManufacturer()->withName('SUPERBARU')->build();
        $model11 = Builders::aModel()->withName('PARTY')->withManufacturer($manufacturer1)->build();
        $model12 = Builders::aModel()->withName('HARD')->withManufacturer($manufacturer1)->build();
        $derivative111 = Builders::aDerivative()->withName('XYZ')->withModel($model11)->build();
        $derivative121 = Builders::aDerivative()->withName('ABC')->withModel($model12)->build();

        $this->createVehicle($seller, $manufacturer1, $model11, $derivative111, 2);
        $this->createVehicle($seller, $manufacturer1, $model12, $derivative121, 3);

        $manufacturer2 = Builders::aManufacturer()->withName('FURD')->build();
        $model21 = Builders::aModel()->withName('UNFOCUS')->withManufacturer($manufacturer2)->build();
        $model22 = Builders::aModel()->withName('MODERO')->withManufacturer($manufacturer2)->build();
        $derivative211 = Builders::aDerivative()->withName('XYZ')->withModel($model21)->build();
        $derivative221 = Builders::aDerivative()->withName('ABC')->withModel($model22)->build();

        $this->createVehicle($seller, $manufacturer2, $model21, $derivative211, 5);
        $this->createVehicle($seller, $manufacturer2, $model22, $derivative221, 7);

        return [[$manufacturer1, $manufacturer2], [$model11, $model12, $model21, $model22]];
    }
}
