<?php
namespace Tests\Integration\ValuationReminders;

use Carbon\Carbon;
use JamJar\Services\ValuationsReminders\InitialEmailReminderService;
use jdavidbakr\MailTracker\Model\SentEmail;
use Tests\builders\Builders;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InitialEmailReminderServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_sends_reminders_after_one_hour()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        $companyUser = Builders::aUser()->build();
        $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

        $user = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()->withOwner($user)->withModelName('Test Mazda')->addedToMarketplace()->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withCollectionValue(2841)
            ->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withDropOffValue(3162)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:04:00'));
        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(0, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:06:00'));
        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));

        $email = $emails[0];

        $this->assertContains('Test Mazda', $email->content);
        $this->assertContains('£3,162', $email->content);
    }

    /** @test */
    public function it_does_not_send_reminders_twice()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        $companyUser = Builders::aUser()->build();
        $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

        $user = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()->withOwner($user)->addedToMarketplace()->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withCollectionValue(700)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:05:00'));

        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:15:00'));

        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 17:45:00'));

        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));
    }

    /** @test */
    public function it_does_not_send_email_if_vehicle_has_a_sale()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        $companyUser = Builders::aUser()->build();
        $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

        $user = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()->withOwner($user)->build();

        $valuation = Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withCollectionValue(700)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:10:00'));

        Builders::aSale()->ofValuation($valuation)->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:15:00'));

        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(0, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 17:45:00'));

        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(0, count($emails));
    }

    /** @test */
    public function it_does_not_send_email_if_vehicle_has_a_good_news_lead_valuation()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        $companyUser = Builders::aUser()->build();
        $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

        $user = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()->withOwner($user)->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withCollectionValue(700)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:10:00'));

        $matrix = Builders::aUser()->matrixPartner()->build();
        $matrixCompany = Builders::aCompany()->withUser($matrix)->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($matrixCompany)
            ->withCollectionValue(1000)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:15:00'));

        $service->sendReminders();
        $emailsAfterAnotherCall = SentEmail::get();
        $this->assertEquals(0, count($emailsAfterAnotherCall));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 17:45:00'));

        $service->sendReminders();
        $emailsAfterAnotherCall = SentEmail::get();
        $this->assertEquals(0, count($emailsAfterAnotherCall));
    }

    /** @test */
    public function it_does_not_send_reminder_about_expired_valuation()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        $companyUser = Builders::aUser()->build();
        $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

        $expiredCompanyUser = Builders::aUser()->build();
        $expiredCompany = Builders::aCompany()->withUser($expiredCompanyUser)->isApiCompany()->withValuationValidity(0)->build();

        $user = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()->withOwner($user)->withModelName('Test Mazda')->addedToMarketplace()->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withCollectionValue(2841)
            ->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withDropOffValue(3262)
            ->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($expiredCompany)
            ->withDropOffValue(4262)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:04:00'));
        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(0, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:05:00'));
        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));

        $email = $emails[0];

        $this->assertContains('Test Mazda', $email->content);
        $this->assertContains('£3,262', $email->content);
    }

    /** @test */
    public function it_does_not_send_reminders_after_twenty_four_hours()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        $companyUser = Builders::aUser()->build();
        $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

        $user = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()->withOwner($user)->withModelName('Test Mazda')->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withCollectionValue(2841)
            ->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withDropOffValue(3162)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-02 12:15:00'));
        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(0, count($emails));
    }

    /** @test */
    public function it_does_not_send_reminders_if_legacy_reminder_has_been_sent()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        $companyUser = Builders::aUser()->build();
        $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

        $user = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()->withOwner($user)->withModelName('Test Mazda')->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withCollectionValue(2841)
            ->legacyOneHourReminderHasBeenSent()
            ->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withDropOffValue(3162)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:15:00'));
        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(0, count($emails));
    }

    /** @test */
    public function it_sends_emails_again_if_vehicle_has_been_modified()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        $companyUser = Builders::aUser()->build();
        $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

        $user = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()->withOwner($user)->withModelName('Test Mazda')->addedToMarketplace()->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withCollectionValue(2841)
            ->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withDropOffValue(3162)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:15:00'));
        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:30:00'));

        $vehicle->start_sending_reminders_at = Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:30:00');
        $vehicle->save();

        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 14:40:00'));

        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(2, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 14:50:00'));

        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(2, count($emails));
    }

    /** @test */
    public function it_does_not_send_reminders_if_there_is_no_valuation()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        $user = Builders::aUser()->build();
        Builders::aVehicle()->withOwner($user)->withModelName('Test Mazda')->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:05:00'));
        $service->sendReminders();
        $emails = SentEmail::get();
        $this->assertEquals(0, count($emails));
    }

    public function it_sends_many_reminders_in_acceptable_time()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $service = new InitialEmailReminderService();

        for ($i = 0; $i < 1000; $i++) {
            $companyUser = Builders::aUser()->build();
            $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

            $user = Builders::aUser()->build();
            $vehicle = Builders::aVehicle()->withOwner($user)->withModelName('Test Mazda')->build();

            Builders::aValuation()
                ->ofVehicle($vehicle)
                ->ofUser($user)
                ->ofCompany($company)
                ->withCollectionValue(2841)
                ->build();

            Builders::aValuation()
                ->ofVehicle($vehicle)
                ->ofUser($user)
                ->ofCompany($company)
                ->withDropOffValue(3162)
                ->build();
        }

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:05:00'));

        $start = microtime(true);

        $service->sendReminders();

        $stop = microtime(true);

        $emails = SentEmail::get();
        $this->assertEquals(1000, count($emails));
        $this->assertLessThan(15, $stop - $start);
    }

}
