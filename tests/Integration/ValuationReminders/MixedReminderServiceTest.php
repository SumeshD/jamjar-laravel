<?php
namespace Tests\Integration\ValuationReminders;

use Carbon\Carbon;
use JamJar\Services\ValuationsReminders\ContinuousEmailReminderService;
use JamJar\Services\ValuationsReminders\FinalEmailReminderService;
use JamJar\Services\ValuationsReminders\InitialEmailReminderService;
use jdavidbakr\MailTracker\Model\SentEmail;
use Tests\builders\Builders;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MixedReminderServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @var InitialEmailReminderService */
    private $initialReminderService;

    /** @var ContinuousEmailReminderService */
    private $continuousReminderService;

    /** @var FinalEmailReminderService */
    private $finalReminderService;

    public function __construct()
    {
        parent::__construct();

        $this->initialReminderService = new InitialEmailReminderService();
        $this->continuousReminderService = new ContinuousEmailReminderService();
        $this->finalReminderService = new FinalEmailReminderService();
    }

    /** @test */
    public function it_sends_reminders()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:00:00'));

        $companyUser = Builders::aUser()->build();
        $company = Builders::aCompany()->withUser($companyUser)->isApiCompany()->build();

        $expiredCompanyUser = Builders::aUser()->build();
        $expiredCompany = Builders::aCompany()->withUser($expiredCompanyUser)->isApiCompany()->withValuationValidity(3)->build();

        $user = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()->withOwner($user)->withModelName('Test Mazda')->addedToMarketplace()->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($company)
            ->withCollectionValue(2841)
            ->build();

        Builders::aValuation()
            ->ofVehicle($vehicle)
            ->ofUser($user)
            ->ofCompany($expiredCompany)
            ->withDropOffValue(3162)
            ->build();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 12:04:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(0, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-01 13:05:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));

        $initialEmail = $emails[0];

        $this->assertContains('Test Mazda', $initialEmail->content);
        $this->assertContains('£3,162', $initialEmail->content);

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-02 10:05:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-02 10:05:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(1, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-02 12:05:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(2, count($emails));

        //$this->assertContains('Your best valuation for your...', $emails[1]->content);
        //$this->assertContains('Your best valuation for your...', $emails[0]->content);

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-03 12:05:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(2, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-04 12:05:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(2, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-05 12:05:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(3, count($emails));

        $finalEmail = $emails[2];

        $this->assertContains('Test Mazda', $finalEmail->content);
        $this->assertContains('£2,841', $finalEmail->content);

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-05 12:05:00'));

        $vehicle->start_sending_reminders_at = Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-05 13:00:00');
        $vehicle->save();

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-05 13:35:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(4, count($emails));

        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2019-01-05 14:15:00'));
        $this->sendAllReminders();
        $emails = SentEmail::get();
        $this->assertEquals(4, count($emails));
    }

    private function sendAllReminders() {
        $this->initialReminderService->sendReminders();
        $this->continuousReminderService->sendReminders();
        $this->finalReminderService->sendReminders();
    }

}
