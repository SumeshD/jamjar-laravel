<?php

namespace Tests\Integration\Valuations;

use Illuminate\Foundation\Testing\RefreshDatabase;
use JamJar\Services\Valuations\ExternalDataProviders\CTBDataProvider;
use JamJar\Services\Valuations\ExternalDataProviders\FakeDataProvider;
use JamJar\Services\Valuations\VehicleValuationsCollector;
use JamJar\Services\Valuations\VehicleValuationsCreator;
use JamJar\Services\Valuations\VehicleValuers\FakeVehicleValuer;
use JamJar\Services\Valuations\VehicleValuers\VehicleValuer;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;

class VehicleValuationsCollectorTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_creates_valuation_drafts()
    {
        $buyer = Builders::aUser()->matrixPartner()->build();
        $buyerCompany = Builders::aCompany()->withUser($buyer)->build();
        $location = Builders::aLocation()->withUser($buyer)->withPostcode('NG15LL')->build();

        $seller = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->withNumberPlate('XYZ1234')
            ->build();

        $valuer = new VehicleValuationsCollector([], [new FakeVehicleValuer($buyerCompany, $location)]);

        $valuationDrafts = $valuer->createValuationDrafts($vehicle);

        $this->assertEquals(1, count($valuationDrafts));
    }

    /** @test */
    public function it_creates_valuation_drafts_from_external_source()
    {
        $buyer = Builders::aUser()->matrixPartner()->build();
        $buyerCompany = Builders::aCompany()->withUser($buyer)->build();
        Builders::aLocation()->withUser($buyer)->withPostcode('NG15LL')->build();

        $seller = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->withNumberPlate('XYZ1234')
            ->build();

        $valuer = new VehicleValuationsCollector([], [new VehicleValuer(new FakeDataProvider($buyerCompany))]);

        $valuationDrafts = $valuer->createValuationDrafts($vehicle);

        $this->assertEquals(1, count($valuationDrafts));
    }

    public function it_creates_ctb_drafts()
    {
        $buyer = Builders::aUser()->matrixPartner()->build();
        $buyerCompany = Builders::aCompany()->withUser($buyer)->withName('CarTakeBack')->build();
        Builders::aLocation()->withUser($buyer)->withPostcode('NG15LL')->build();

        $seller = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->withNumberPlate('NV08TXO')
            ->build();

        $collector = new VehicleValuationsCollector([], [new VehicleValuer(new CTBDataProvider())]);

        $valuationDrafts = $collector->createValuationDrafts($vehicle);

        $this->assertEquals(1, count($valuationDrafts));
    }

    public function it_creates_valuations_based_on_draft()
    {
        $buyer = Builders::aUser()->matrixPartner()->build();
        $buyerCompany = Builders::aCompany()->withUser($buyer)->withName('CarTakeBack')->build();
        Builders::aLocation()->withUser($buyer)->withPostcode('NG15LL')->build();

        $seller = Builders::aUser()->build();
        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->withNumberPlate('NV08TXO')
            ->build();

        $collector = new VehicleValuationsCollector([], [new VehicleValuer(new CTBDataProvider())]);

        $valuationDrafts = $collector->createValuationDrafts($vehicle);

        $this->assertEquals(1, count($valuationDrafts));

        $creator = new VehicleValuationsCreator();

        $uuid = Uuid::uuid4();

        foreach ($valuationDrafts as $draft) {
            $valuation = $creator->createValuationFromDraft($draft, $uuid);
        }

        $this->assertGreaterThan(0, $valuation->getOriginal('collection_value'));
    }
}
