<?php
namespace Tests\Integration\Entities;

use Carbon\Carbon;
use JamJar\Api\FakeWeWantAnyCar;
use JamJar\Api\FakeWeWantAnyCarValuation;
use JamJar\Sale;
use JamJar\Services\APIValuationsService;
use JamJar\Valuation;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APIValuationsServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_accepts_wwac_valuations()
    {
        $seller = Builders::aUser()->build();
        $buyer = Builders::aUser()->build();
        $tempRef = Uuid::uuid4();

        $buyerCompany = Builders::aCompany()
            ->withUser($buyer)
            ->withCity('wwac city')
            ->withCompanyType('api')
            ->build();

        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->withMileage(50020)
            ->build();

        $valuation = Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid(Uuid::uuid4())
            ->withCollectionValue(1000)
            ->withVehicle($vehicle)
            ->withTempRef($tempRef->toString())
            ->build();

        $valuationService = new APIValuationsService();

        $fakeWWACService = new FakeWeWantAnyCar([
            new FakeWeWantAnyCarValuation($tempRef, Carbon::now(), Carbon::now()->subDay(1), 1500)
        ]);

        $acceptedValuations = $fakeWWACService->getAcceptedValuations(
            Carbon::now()->subHour(1),
            Carbon::now()->addHour(1)
        );

        $valuationService->acceptWWACValuations($acceptedValuations);

        $valuationFromDatabase = Valuation::where('id', '=', $valuation->id)->first();
        $this->assertEquals(Carbon::now()->format('Y-m-d'), $valuationFromDatabase->accepted_at);

        $sale = Sale::where('valuation_id', '=', $valuationFromDatabase->id)->first();
        $this->assertNotNull($sale);
        $this->assertEquals(1500, $sale->final_price);
        $this->assertEquals('complete', $sale->status);
    }

    /** @test */
    public function it_does_not_accept_wwac_valuations_twice()
    {
        $seller = Builders::aUser()->build();
        $buyer = Builders::aUser()->build();
        $tempRef = Uuid::uuid4();

        $buyerCompany = Builders::aCompany()
            ->withUser($buyer)
            ->withCity('wwac city')
            ->withCompanyType('api')
            ->build();

        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->withMileage(50020)
            ->build();

        $valuation = Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid(Uuid::uuid4())
            ->withCollectionValue(1000)
            ->withVehicle($vehicle)
            ->withTempRef($tempRef->toString())
            ->build();

        $valuationService = new APIValuationsService();

        $fakeWWACService = new FakeWeWantAnyCar([
            new FakeWeWantAnyCarValuation($tempRef, Carbon::now(), Carbon::now()->subDay(1), 1500)
        ]);

        $acceptedValuations = $fakeWWACService->getAcceptedValuations(
            Carbon::now()->subHour(1),
            Carbon::now()->addHour(1)
        );

        $valuationService->acceptWWACValuations($acceptedValuations);
        $valuationService->acceptWWACValuations($acceptedValuations);
        $valuationService->acceptWWACValuations($acceptedValuations);

        $sales = Sale::where('valuation_id', '=', $valuation->id)->get();

        $this->assertEquals(1, count($sales));
    }
}
