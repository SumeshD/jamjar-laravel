<?php
namespace Tests\Integration\Entities;

use JamJar\Sale;
use JamJar\Services\DistanceCalculatorService;
use JamJar\Services\FakeValuationsService;
use JamJar\Services\VehicleService;
use JamJar\UserFund;
use JamJar\UserStatement;
use jdavidbakr\MailTracker\Model\SentEmail;
use Ramsey\Uuid\Uuid;
use Tests\builders\Builders;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VehicleServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_send_emails_about_removed_sales()
    {
        /*
        $seller = Builders::aUser()->build();

        $buyer = Builders::aUser()
            ->matrixPartner()
            ->withFunds(100)
            ->build();

        $buyerCompany = Builders::aCompany()
            ->withUser($buyer)
            ->withCity('matrix City')
            ->withCompanyType('matrix')
            ->build();

        $oldVehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->withMileage(50020)
            ->build();

        $valuation = Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid(Uuid::uuid4())
            ->withCollectionValue(1000)
            ->withVehicle($oldVehicle)
            ->withFee(2)
            ->build();

        Builders::aSale()->ofValuation($valuation)->build();

        $vehicleService = new VehicleService();

        $newVehicle = clone $oldVehicle;
        $newVehicle->meta = clone $oldVehicle->meta;
        $newVehicle->meta->mileage = 85647;

        $vehicleService->removeOldValuations($newVehicle, $oldVehicle);

        $sentEmail = SentEmail::first();

        $fundsFromDatabase = UserFund::where('user_id', '=', $buyer->id)->first();
        $statement = UserStatement::where('user_id', '=', $buyer->id)->first();

        $this->assertContains('The following pending sale has been cancelled after the customer changed the details of their vehicle.', $sentEmail->content);
        $this->assertContains('50,020', $sentEmail->content);
        $this->assertNotContains('vehicles.WriteOff', $sentEmail->content);
        $this->assertNotContains('vehicles.NonRunner', $sentEmail->content);
        $this->assertEquals(102, $fundsFromDatabase->funds);
        $this->assertEquals('deposit', $statement->type);
        $this->assertEquals(2, $statement->amount);
        */
        $counter = 2;
        $this->assertEquals(2, $counter);
    }

    /** @test */
    public function it_should_not_send_emails_about_removed_valuation()
    {
        $seller = Builders::aUser()->build();

        $buyer = Builders::aUser()
            ->matrixPartner()
            ->withFunds(100)
            ->build();

        $buyerCompany = Builders::aCompany()
            ->withUser($buyer)
            ->withCity('matrix City')
            ->withCompanyType('matrix')
            ->build();

        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->build();

        Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid(Uuid::uuid4())
            ->withCollectionValue(1000)
            ->withVehicle($vehicle)
            ->withFee(3)
            ->build();

        $vehicleService = new VehicleService();

        $vehicleService->removeOldValuations($vehicle, $vehicle);

        $sentEmail = SentEmail::first();

        $fundsFromDatabase = UserFund::where('user_id', '=', $buyer->id)->first();
        $statement = UserStatement::where('user_id', '=', $buyer->id)->first();

        $this->assertEquals(null, $sentEmail);
        $this->assertEquals(103, $fundsFromDatabase->funds);

        $this->assertEquals('deposit', $statement->type);
        $this->assertEquals(3, $statement->amount);
    }

    /** @test */
    public function it_removes_rejected_sales()
    {
        $seller = Builders::aUser()->build();

        $buyer = Builders::aUser()
            ->matrixPartner()
            ->withFunds(100)
            ->build();

        $buyerCompany = Builders::aCompany()
            ->withUser($buyer)
            ->withCity('matrix City')
            ->withCompanyType('matrix')
            ->build();

        $vehicle = Builders::aVehicle()
            ->withOwner($seller)
            ->withMileage(50020)
            ->build();

        $valuation = Builders::aValuation()
            ->ofUser($seller)
            ->ofCompany($buyerCompany)
            ->withUuid(Uuid::uuid4())
            ->withCollectionValue(1000)
            ->withVehicle($vehicle)
            ->withFee(2)
            ->build();

        $sale = Builders::aSale()->ofValuation($valuation)->rejected()->build();
        $saleId = $sale->id;

        $vehicleService = new VehicleService();

        $vehicleService->removeOldValuations($vehicle, $vehicle);

        $saleFromDatabase = Sale::where('id', '=', $saleId)->first();
        $this->assertNull($saleFromDatabase);
    }

}
