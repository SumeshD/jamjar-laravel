<?php
namespace Tests\Integration\ValuationReminders;

use JamJar\Api\CapApi;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CapApiTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_receives_cap_vehicle_data_if_there_is_no_error()
    {
        $capService = new CapApi();

        $httpClient = $capService->getHttpClient();
        $httpClient->failAttempts = 0;

        $data = $capService->getVehicle('NV08TXO');

        $this->assertNotEmpty($data);
    }

    /** @test */
    public function it_receives_cap_vehicle_data_if_there_is_temporary_problem_with_cap_api()
    {
        $capService = new CapApi();

        $httpClient = $capService->getHttpClient();
        $httpClient->failAttempts = 2;

        $data = $capService->getVehicle('NV08TXO');

        $this->assertNotEmpty($data);
    }

    /**
     * @test
     * @expectedException JamJar\Exceptions\ApiResponseException
     */
    public function it_throws_exception_if_attempts_limit_exceeded()
    {
        $capService = new CapApi();

        $httpClient = $capService->getHttpClient();
        $httpClient->failAttempts = 4;

        $capService->getVehicle('NV08TXO');
    }
}
