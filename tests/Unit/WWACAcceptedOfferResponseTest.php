<?php

namespace Tests\Unit;

use JamJar\Api\Responses\WWACAcceptedOfferResponse;
use Tests\BaseUnitTestCase;

class WWACAcceptedOfferResponseTest extends BaseUnitTestCase
{
    /** @test */
    public function it_creates_responses_from_request()
    {
        $response = $this->getResponse();

        $results = WWACAcceptedOfferResponse::createFromJsonResponse($response);

        $this->assertEquals(2, count($results));
        $this->assertEquals('ab82d53e-3705-4504-ab37-cc979cfbdc0c', $results[0]->valuationIdentifier->toString());
        $this->assertEquals('2018-11-04', $results[0]->datePurchase->format('Y-m-d'));
    }

    private function getResponse(): string
    {
        return '{
            "ColumnMappings": [
                {
                    "Id": "ValuationID",
                    "Description": "2487663",
                    "Value": "2487663",
                    "Options": [
                        {
                            "Id": "BidReference",
                            "Value": "d902c2d0-b68a-4837-88a2-b6a437980e0c"
                        },
                        {
                            "Id": "ValuationIdentifier",
                            "Value": "ab82d53e-3705-4504-ab37-cc979cfbdc0c"
                        },
                        {
                            "Id": "ValuationOffer",
                            "Value": "635"
                        },
                        {
                            "Id": "DateValued",
                            "Value": "02/11/2018 15:08:00"
                        },
                        {
                            "Id": "DatePurchased",
                            "Value": "04/11/2018 15:08:00"
                        },
                        {
                            "Id": "RecommendedBy",
                            "Value": "JamJar-Comparison"
                        },
                        {
                            "Id": "ValuationStatus",
                            "Value": "Valuation Accepted"
                        },
                        {
                            "Id": "VRM",
                            "Value": "DA04ZHF"
                        },
                        {
                            "Id": "Make",
                            "Value": "AUDI"
                        },
                        {
                            "Id": "Model",
                            "Value": "A3 1.6 SE 3dr"
                        },
                        {
                            "Id": "Mileage",
                            "Value": "115000"
                        },
                        {
                            "Id": "Year",
                            "Value": "2004"
                        },
                        {
                            "Id": "CustomerName",
                            "Value": "Tristan Reilly"
                        },
                        {
                            "Id": "CustomerEmail",
                            "Value": "tristanreilly@hotmail.co.uk"
                        },
                        {
                            "Id": "CustomerPhone",
                            "Value": "07751062672"
                        },
                        {
                            "Id": "CustomerPostcode",
                            "Value": "SN25      1WR"
                        }
                    ]
                },
                {
                    "Id": "ValuationID",
                    "Description": "2486167",
                    "Value": "2486167",
                    "Options": [
                        {
                            "Id": "BidReference",
                            "Value": "ab82d53e-3705-4504-ab37-cc979cfbdc01"
                        },
                        {
                            "Id": "ValuationIdentifier",
                            "Value": "c02f3b9f-8fd7-4b84-b1d4-00374d6e5d46"
                        },
                        {
                            "Id": "ValuationOffer",
                            "Value": "873"
                        },
                        {
                            "Id": "DateValued",
                            "Value": "01/11/2018 16:37:38"
                        },
                        {
                            "Id": "DatePurchased",
                            "Value": "03/11/2018 16:37:38"
                        },
                        {
                            "Id": "RecommendedBy",
                            "Value": "JamJar-Comparison"
                        },
                        {
                            "Id": "ValuationStatus",
                            "Value": "Valuation Accepted"
                        },
                        {
                            "Id": "VRM",
                            "Value": "MV06FHR"
                        },
                        {
                            "Id": "Make",
                            "Value": "VAUXHALL"
                        },
                        {
                            "Id": "Model",
                            "Value": "ASTRA 1.6 16V 2dr"
                        },
                        {
                            "Id": "Mileage",
                            "Value": "82258"
                        },
                        {
                            "Id": "Year",
                            "Value": "2006"
                        },
                        {
                            "Id": "CustomerName",
                            "Value": "Carajade Arthur"
                        },
                        {
                            "Id": "CustomerEmail",
                            "Value": "ceejaifeeney@gmail.com"
                        },
                        {
                            "Id": "CustomerPhone",
                            "Value": "47902996211"
                        },
                        {
                            "Id": "CustomerPostcode",
                            "Value": "L15       2HU"
                        }
                    ]
                }
            ],
            "ValuationResponseStatuses": [
                {
                    "Code": "100A",
                    "Description": "Success"
                },
                {
                    "Code": "100B",
                    "Description": "0.13"
                }
            ]
        }';
    }
}

