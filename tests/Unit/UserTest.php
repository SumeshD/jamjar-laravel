<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    protected $user, $admin, $staff, $partner;
    public function setUp()
    {
        parent::setUp();
        $this->user = factory('JamJar\User')->create(['role_id' => 4]);
        $this->admin = factory('JamJar\Role')->create(['name'  => 'Administrator']);
        $this->staff = factory('JamJar\Role')->create(['name'  => 'Staff']);
        $this->partner = factory('JamJar\Role')->create(['name'  => 'Partner']);

        factory('JamJar\Role')->create(['name'  => 'Client']);
    }

    public function test_it_has_a_profile()
    {
        $this->assertInstanceOf('JamJar\Profile', $this->user->profile);
    }
    
    public function test_it_has_a_role()
    {
        $role = factory('JamJar\Role')->create();
        $user = factory('JamJar\User')->create(['role_id' => $role->id]);
        $this->assertInstanceOf('JamJar\Role', $user->role);
    }

    public function test_it_knows_if_it_is_a_partner()
    {
        // given we  have a user
        // who is marked as a matrix partner
        $partner = factory('JamJar\User')->create(['matrix_partner' => 1, 'role_id' => $this->partner->id]);
        // who has a company
        factory('JamJar\Company')->create(['user_id'  => $partner->id]);
        // they should pass the isPartner() check
        $this->assertTrue($partner->isPartner());
    }

    public function test_it_knows_if_it_is_a_staff_member()
    {
        $staff = factory('JamJar\User')->create(['role_id' => $this->staff->id]);

        $this->assertTrue($staff->isStaff());
    }

    public function test_it_knows_if_it_is_an_administrator()
    {
        $admin = factory('JamJar\User')->create(['role_id' => $this->admin->id]);

        $this->assertTrue($admin->isAdmin());
    }

    public function test_it_can_have_a_company()
    {
        $company = factory('JamJar\Company')->create(['user_id' => $this->user->id]);

        $this->assertInstanceOf('JamJar\Company', $this->user->company); 
    }

}
