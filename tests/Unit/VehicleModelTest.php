<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use JamJar\VehicleModel;
use JamJar\VehicleOwner;
use Tests\builders\Builders;
use Tests\TestCase;

class VehicleModelTest extends TestCase
{
    use RefreshDatabase;

    protected $vehicleModel;


    /** @test */
    public function it_returns_vehicle_model_based_on_production_time_frame()
    {
        $manufacturer = Builders::aManufacturer()->car()->withName('Test Manufacturer')->build();
        $vehicleModel = Builders::aVehicleModel()->withCapModelId(1)->withManufacturerId(1)->withFuelType('Petrol')->withName('Fancy Car')->withIntroducedDate("2004")->withDiscontinuedDate("2010")->withPlateYears("2012,2013")->withInternalManufacturerId($manufacturer->id)->build();
        $vehicleModel = Builders::aVehicleModel()->withCapModelId(2)->withManufacturerId(1)->withFuelType('Petrol')->withName('Fancy Car 2')->withIntroducedDate("2009")->withDiscontinuedDate("2012")->withPlateYears("2012,2013")->withInternalManufacturerId($manufacturer->id)->build();
        $vehicleModel = Builders::aVehicleModel()->withCapModelId(3)->withManufacturerId(1)->withFuelType('Petrol')->withName('Fancy Car 3')->withIntroducedDate("2010")->withDiscontinuedDate("2011")->withPlateYears("2012,2013")->withInternalManufacturerId($manufacturer->id)->build();
        $vehicleModel = Builders::aVehicleModel()->withCapModelId(4)->withManufacturerId(1)->withFuelType('Petrol')->withName('Fancy Car 4')->withIntroducedDate("2011")->withDiscontinuedDate("2018")->withPlateYears("2012,2013")->withInternalManufacturerId($manufacturer->id)->build();
        $vehicleModel = Builders::aVehicleModel()->withCapModelId(5)->withManufacturerId(1)->withFuelType('Petrol')->withName('Fancy Car 5')->withIntroducedDate("2005")->withDiscontinuedDate("2020")->withPlateYears("2012,2013")->withInternalManufacturerId($manufacturer->id)->build();
        $vehicleModel = Builders::aVehicleModel()->withCapModelId(6)->withManufacturerId(1)->withFuelType('Petrol')->withName('Fancy Car 6')->withIntroducedDate("2005")->withDiscontinuedDate("2007")->withPlateYears("2012,2013")->withInternalManufacturerId($manufacturer->id)->build();
        $vehicleModel = Builders::aVehicleModel()->withCapModelId(7)->withManufacturerId(1)->withFuelType('Petrol')->withName('Fancy Car 7')->withIntroducedDate("2016")->withDiscontinuedDate("2018")->withPlateYears("2012,2013")->withInternalManufacturerId($manufacturer->id)->build();

        $models = VehicleModel::getModelByManufacturerAndTimeFrame(1,2009,2012,'CAR','Petrol');
        $this->assertEquals(5, count($models));
    }


    /** @test */
    public function it_returns_phone_number_in_ctb_format()
    {
        /** @var VehicleOwner $owner */
        $owner1 = (new VehicleOwner())->setTelephone('+44 7107 881234');
        $this->assertEquals('07107 881234', $owner1->getPhoneForCarTakeBack());

        /** @var VehicleOwner $owner */
        $owner2 = (new VehicleOwner())->setTelephone('07107 881234');
        $this->assertEquals('07107 881234', $owner2->getPhoneForCarTakeBack());

        /** @var VehicleOwner $owner */
        $owner3 = (new VehicleOwner())->setTelephone('7107 881234');
        $this->assertEquals('07107 881234', $owner3->getPhoneForCarTakeBack());

        /** @var VehicleOwner $owner */
        $owner4 = (new VehicleOwner())->setTelephone('+4 7107 881234');
        $this->assertEquals('07107 881234', $owner4->getPhoneForCarTakeBack());
    }
}

