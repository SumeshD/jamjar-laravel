<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PartnerWebsiteTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_can_be_loaded_by_its_slug()
    {
        $website = factory('JamJar\PartnerWebsite')->create();

        $this->json('GET', '/partner-sites/'.$website->slug.'/home')->assertStatus(200);
    }

    public function test_it_consists_of_pages()
    {
        $website = factory('JamJar\PartnerWebsite')->create();
        $count = $website->pages->count();

        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Collection', $website->pages
        );

        $this->assertCount($count, $website->pages);
    }

}
