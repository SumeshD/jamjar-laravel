<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use JamJar\VehicleOwner;
use Tests\TestCase;

class VehicleOwnerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_returns_area_by_postcode()
    {
        $owner1 = (new VehicleOwner())->setPostcode('TE57 1NG');
        $this->assertEquals('TE57', $owner1->getAreaByPostcode());

        $owner2 = (new VehicleOwner())->setPostcode('NG1 2AS');
        $this->assertEquals('NG1', $owner2->getAreaByPostcode());

        $owner3 = (new VehicleOwner())->setPostcode('NG12 3NX');
        $this->assertEquals('NG12', $owner3->getAreaByPostcode());

        $owner4 = (new VehicleOwner())->setPostcode('NG11LL');
        $this->assertEquals('NG1', $owner4->getAreaByPostcode());

        $owner5 = (new VehicleOwner())->setPostcode('DE78PW');
        $this->assertEquals('DE7', $owner5->getAreaByPostcode());

        $owner6 = (new VehicleOwner())->setPostcode('LE8 9JY');
        $this->assertEquals('LE8', $owner6->getAreaByPostcode());

        $owner7 = (new VehicleOwner())->setPostcode('DN551PT');
        $this->assertEquals('DN55', $owner7->getAreaByPostcode());

        $owner8 = (new VehicleOwner())->setPostcode('W1A0AX');
        $this->assertEquals('W1', $owner8->getAreaByPostcode());

        $owner9 = (new VehicleOwner())->setPostcode('EC1A1BB');
        $this->assertEquals('EC1', $owner9->getAreaByPostcode());

        $owner9 = (new VehicleOwner())->setPostcode('M1 1AE');
        $this->assertEquals('M1', $owner9->getAreaByPostcode());

        $owner10 = (new VehicleOwner())->setPostcode(' ng92rs');
        $this->assertEquals('NG9', $owner10->getAreaByPostcode());

    }

    /** @test */
    public function it_returns_empty_string_if_there_is_no_postcode()
    {
        $owner = (new VehicleOwner())->setPostcode('');
        $this->assertEquals('', $owner->getAreaByPostcode());
    }

    /** @test */
    public function it_returns_original_if_postcode_is_invalid()
    {
        $owner = (new VehicleOwner())->setPostcode('12345');
        $this->assertEquals('12345', $owner->getAreaByPostcode());
    }

    /** @test */
    public function it_returns_phone_number_in_ctb_format()
    {
        /** @var VehicleOwner $owner */
        $owner1 = (new VehicleOwner())->setTelephone('+44 7107 881234');
        $this->assertEquals('07107 881234', $owner1->getPhoneForCarTakeBack());

        /** @var VehicleOwner $owner */
        $owner2 = (new VehicleOwner())->setTelephone('07107 881234');
        $this->assertEquals('07107 881234', $owner2->getPhoneForCarTakeBack());

        /** @var VehicleOwner $owner */
        $owner3 = (new VehicleOwner())->setTelephone('7107 881234');
        $this->assertEquals('07107 881234', $owner3->getPhoneForCarTakeBack());

        /** @var VehicleOwner $owner */
        $owner4 = (new VehicleOwner())->setTelephone('+4 7107 881234');
        $this->assertEquals('07107 881234', $owner4->getPhoneForCarTakeBack());
    }
}

