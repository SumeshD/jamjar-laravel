<?php
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_can_successfully_log_in()
    {
        $this->withExceptionHandling();

        $user = factory('JamJar\User')->create();
        $credentials = [
            'email'    => $user->email,
            'password'    => $user->password
        ];
        $response = $this->post('/login', $credentials);
        $response->assertRedirect('/')->assertStatus(302);
    }

    public function test_a_user_receives_errors_for_wrong_login_credentials()
    {
        $this->withExceptionHandling();

        $user = factory('JamJar\User')->create();

        $invalidCredentials = [
            'email'    => $user->email,
            'password'    => 'badpass'
        ];

        $response = $this->post('/login', $invalidCredentials);
        // default auth never populates an $errors array for password.
        $response->assertSessionHasErrors('email');
    }

    public function test_a_user_is_redirected_to_dashboard_if_logged_in_and_tries_to_access_login_page()
    {
        $user = factory('JamJar\User')->create();
        $roles = factory('JamJar\Role', 4)->create();
        $company = factory('JamJar\Company')->create(['user_id' => $user->id]);
        
        $this->signIn($user);

        $response = $this->get('/login');

        $response->assertRedirect('/valuations/saved')->assertStatus(302);
    }

    public function test_a_user_is_redirected_to_login_page_if_not_logged_in()
    {
        $this->withExceptionHandling();
        $response = $this->get('/valuations/saved');
        $response->assertRedirect('/login')->assertStatus(302);
    }
}
