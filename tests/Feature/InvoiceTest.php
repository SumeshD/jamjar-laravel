<?php

namespace Tests\Feature;

use Tests\builders\Builders;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvoiceTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_can_view_an_invoice()
    {
        $user = Builders::aUser()->matrixPartner()->build();
        $order = factory('JamJar\Order')->create();
        $order->user_id = $user->id;
        $order->save();

        $this->signIn($user);

        $response = $this->get("/associates/invoices/{$order->pp_payment_id}");
        $response->assertStatus(200);
    }

    /**
     * @test
     * @expectedException \JamJar\Exceptions\NotAllowedException
     */
    public function another_user_cannot_check_invoice()
    {
        $user = Builders::aUser()->matrixPartner()->build();
        $order = factory('JamJar\Order')->create();

        $this->signIn($user);
        $this->get("/associates/invoices/{$order->pp_payment_id}");
    }

    /** @test */
    public function admin_can_check_invoice()
    {
        $admin = Builders::aUser()->isAdmin()->build();
        $order = factory('JamJar\Order')->create();

        $this->signIn($admin);
        $response = $this->get("/associates/invoices/{$order->pp_payment_id}");
        $response->assertStatus(200);
    }
}
