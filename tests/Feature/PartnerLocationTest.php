<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PartnerLocationTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_partner_can_add_a_new_location()
    {
        // given i am a matrix partner
        $user = factory('JamJar\User')->create(['matrix_partner' => 1]);
        // who has a company
        $company = factory('JamJar\Company')->create(['user_id' => $user->id]);
        // who is signed in
        $this->signIn($user);
        // and I want to create a new location
        $location = factory('JamJar\PartnerLocation')->make(['user_id' => $user->id, 'company_id' => $company->id, 'primary' => 1]);
        $response = $this->json('POST', '/associates/locations', $location->toArray()); 
        // it should be saved in the database
        $this->assertDatabaseHas('partner_locations', $location->toArray());
        // and associated with my account
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $user->company->location);
        // and i should be redirected away
        $response->assertStatus(302);
    }

    public function test_a_partner_can_edit_a_location()
    {
        //given i am a matrix partner
        $user = factory('JamJar\User')->create(['matrix_partner' => 1]);
        //who has a company
        $company = factory('JamJar\Company')->create(['user_id' => $user->id]);
        //who is signed in
        $this->signIn($user);
        //and i have a location
        $location = factory('JamJar\PartnerLocation')->create(['user_id' => $user->id, 'company_id' => $company->id]);
        //and i want to update my location
        $updatedLocation = factory('JamJar\PartnerLocation')->make(['user_id' => $user->id, 'company_id' => $company->id]);
        // 
        $response = $this->json('PATCH', '/associates/locations/'.$location->id, $updatedLocation->toArray());
        //it should be updated in the database
        $this->assertDatabaseHas('partner_locations', $updatedLocation->toArray());
        //and i should be redirected to the locations page
        $response->assertStatus(302);
    }

    public function test_a_partner_can_delete_a_location()
    {
        //given i am a matrix partner
        $user = factory('JamJar\User')->create(['matrix_partner' => 1]);
        //who has a company
        $company = factory('JamJar\Company')->create(['user_id' => $user->id]);
        //who is signed in
        $this->signIn($user);
        //and i have a location
        factory('JamJar\PartnerLocation')->create(['user_id' => $user->id, 'company_id' => $company->id]);
        $location = factory('JamJar\PartnerLocation')->create(['user_id' => $user->id, 'company_id' => $company->id]);
        // and i submit a delete request
        $response = $this->json('DELETE', '/associates/locations/'.$location->id);
        // it should be deleted
        $this->assertEquals(false, $location->is_active);
        //and i should be redirected to the locations page
        $response->assertStatus(302);
    }
}
