<?php
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    public function test_unauthenticated_users_cannot_change_account_details()
    {
        $this->withExceptionHandling();

        $response = $this->get('/account');

        $response->assertRedirect('/login')->assertStatus(302);
    }

    public function test_authenticated_users_can_change_their_name()
    {
        // Given I have a user
        $user = factory('JamJar\User')->create(['name' => 'Jane Doe']);
        // Who is signed in
        $this->signIn($user);
        // Who has a profile
        $this->assertInstanceOf('JamJar\Profile', $user->profile);
        // When I submit a patch request to change my name
        $profile = factory('JamJar\Profile')->make(['name' => 'Jane Jackson']);
        $response = $this->patch('/account', $profile->toArray());
        // Then my name should be changed
        $this->assertEquals($user->profile->fresh()->name, 'Jane Jackson');
    }
    
    public function test_authenticated_users_can_change_their_email()
    {
        // given i have a user
        $user = factory('JamJar\User')->create(['email' => 'jane.doe@example.com']);
        // who is signed in
        $this->signIn($user);
        // when i submit a patch request to change my email
        $profile = factory('JamJar\Profile')->make(['email' => 'jane.jackson@example.com']);
        $response = $this->patch('/account', $profile->toArray());
        // then my email should be changed
        $this->assertEquals($user->profile->fresh()->email, 'jane.jackson@example.com');    
    }

    public function test_authenticated_users_can_change_their_telephone_number()
    {
        // given i have a user
        $user = factory('JamJar\User')->create();
        // who is signed in
        $this->signIn($user);
        // when i submit a patch request to change my telephone number
        $profile = factory('JamJar\Profile')->make(['telephone' => '911']);
        $response = $this->patch('/account', $profile->toArray());
        // then my telephone number should be changed
        $this->assertEquals($user->profile->fresh()->telephone_number, '911');
    }

    public function test_authenticated_users_can_change_their_address()
    {
        // given i have a user
        $user = factory('JamJar\User')->create();
        // who is signed in
        $this->signIn($user);
        // when i submit a patch request to change my address
        $profile = factory('JamJar\Profile')->make(['address_line_one' => 'Testing Street']);
        $response = $this->patch('/account', $profile->toArray());
        // then my address should be changed
        $this->assertEquals($user->profile->fresh()->address_line_one, 'Testing Street');
    }


}
