<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateOfferTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_non_partner_cannot_create_an_offer()
    {
        $this->withExceptionHandling();
        // given i am not signed in
        // when i try to create a new offer
        $response = $this->get('/associates/automated-bids/create');
        // i should be redirected to the login screen
        $response->assertRedirect('/login');
        $response->assertStatus(302);
    }

    // public function test_a_partner_can_create_an_offer()
    // {
    //     // given i am a matrix partner
    //     $user = factory('JamJar\User')->create(['matrix_partner' => 1]);
    //     $company = factory('JamJar\Company')->create(['user_id' => $user->id]);
    //     $this->signIn($user);
    //     // when i go to create an offer
    //     $response = $this->get('/associates/offers/create');
    //     // i should be shown the create offers page
    //     $response->assertStatus(200);
    // }
}
