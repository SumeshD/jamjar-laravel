<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class PartnerWebsiteTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_partner_can_have_a_website()
    {
        $partner = factory('JamJar\User')->create(['matrix_partner' => 1]);
        $website = factory('JamJar\PartnerWebsite')->create(['user_id' => $partner->id]);
        
        $response = $this->json('GET', '/partner-sites/'.$website->slug.'/home');
        $response->assertStatus(200);
    }

    public function test_a_valid_logo_image_must_be_provided()
    {
        $this->withExceptionHandling()->signIn();

       $logo = $this->json('POST', '/associates/theme', [
            'logo_image'  => 'not-an-image'
        ]); 
        
        $logo->assertStatus(422);
    }

    public function test_a_valid_hero_image_must_be_provided()
    {
        $this->withExceptionHandling()->signIn();
        
        $hero = $this->json('POST', '/associates/theme', [
            'hero_image'  => 'not-an-image'
        ]);
        
        $hero->assertStatus(422);
    }

    public function test_a_partner_can_add_a_logo_to_their_website()
    {
        $partner = factory('JamJar\User')->create(['matrix_partner' => 1]);

        $website = factory('JamJar\PartnerWebsite')->create(['user_id' => $partner->id]);

        $this->signIn($partner);

        Storage::fake('s3');

        $logo = UploadedFile::fake()->image('logo.jpg');

        $this->json('POST', '/associates/theme', [
            'logo_image'  => $logo
        ]);
        $filePath = 'partner-websites/images/'.$website->slug.'/logo.jpg';
        $fileUrl = Storage::disk('s3')->url('partner-websites/images/'.$website->slug.'/logo.jpg');

        Storage::disk('s3')->assertExists($filePath);

        $this->assertEquals($fileUrl, $partner->website->logo_image);
    }

    public function test_a_partner_can_add_a_hero_image_to_their_website()
    {
        $partner = factory('JamJar\User')->create(['matrix_partner' => 1]);

        $website = factory('JamJar\PartnerWebsite')->create(['user_id' => $partner->id]);

        $this->signIn($partner);

        Storage::fake('s3');

        $hero = UploadedFile::fake()->image('hero.jpg');

        $this->json('POST', '/associates/theme', [
            'hero_image'  => $hero
        ]);
        $filePath = 'partner-websites/images/'.$website->slug.'/hero.jpg';
        $fileUrl = Storage::disk('s3')->url('partner-websites/images/'.$website->slug.'/hero.jpg');

        Storage::disk('s3')->assertExists($filePath);

        $this->assertEquals($fileUrl, $partner->website->hero_image);
    }

    public function test_a_partner_can_add_a_brand_color_to_their_website()
    {
        $partner = factory('JamJar\User')->create(['matrix_partner' => 1]);

        $website = factory('JamJar\PartnerWebsite')->create(['user_id' => $partner->id]);

        $this->signIn($partner);

        $this->json('POST', '/associates/theme', [
            'brand_color'   => 'AAAAAA'
        ]);

        $this->assertEquals('AAAAAA', $partner->website->brand_color);
    }

    public function test_a_partner_can_add_a_background_color_to_their_website()
    {
        $partner = factory('JamJar\User')->create(['matrix_partner' => 1]);

        $website = factory('JamJar\PartnerWebsite')->create(['user_id' => $partner->id]);

        $this->signIn($partner);

        $this->json('POST', '/associates/theme', [
            'bg_color'   => 'FFFFFF'
        ]);

        $this->assertEquals('FFFFFF', $partner->website->bg_color);  
    }
}
