<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PartnerPageTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_partner_can_update_their_home_page_content()
    {
        // given I am a partner
        $partner = factory('JamJar\User')->create(['matrix_partner' => 1]);
        // and I have a website
        $website = factory('JamJar\PartnerWebsite')->create(['user_id' => $partner->id]);
        // which has a home page
        $page = $website->pages->first();
        // and I am signed in
        $this->signIn($partner);
        // when I submit a change to my homepage
        $response = $this->json('POST', '/partner-sites/'.$website->slug.'/'.$page->slug, [
            'title' => 'Home Page',
            'content' => 'Hello World, This is my Home Page.'
        ]); 
        // it should be changed in the database
        $this->assertEquals('Home Page', $page->fresh()->title);
        $this->assertEquals('Hello World, This is my Home Page.', $page->fresh()->content);
    }
}
