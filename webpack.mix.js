let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	//.sass('resources/assets/sass/style.scss', 'public/css')
	.js('resources/assets/js/app.js', 'public/js')
	.styles([
		'node_modules/sweetalert/dist/sweetalert.css',
        'node_modules/trumbowyg/dist/ui/trumbowyg.min.css',
        'node_modules/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.css',
	], 'public/css/vendor.css')
	.babel([
			'node_modules/jquery/dist/jquery.js',
			'node_modules/sweetalert/dist/sweetalert.min.js',
            'node_modules/trumbowyg/dist/trumbowyg.min.js',
            'node_modules/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js',
			// 'resources/assets/js/vendor/webfont.min.js',
			'resources/assets/js/vendor/icheck.min.js',
			'resources/assets/js/vendor/jquery.matchHeight-min.js',
            'resources/assets/js/vendor/wow.js',
            'resources/assets/js/vendor/jscolor.min.js',
            'resources/assets/js/vendor/multiple-select.js',
            'resources/assets/js/vendor/ion.rangeSlider.js',
			'resources/assets/js/vendor/scripts.js',
		], 'public/js/vendor.js')
	.sourceMaps()
	.options({
		processCssUrls: false
	}).copy('resources/assets/img', 'public/images');

