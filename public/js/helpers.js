function confirmAction(callback, message = '', buttons = null) {
    swal({
        title: "Are you sure?",
        icon: "warning",
        text: message ? message : "Once deleted, you will not be able to recover this.",
        buttons: buttons ? buttons : true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                callback();
            }
        });
}

$(function() {
    var redirectClassPrefix = 'redirect:';
    $('body').on('click', '[class*="' + redirectClassPrefix + '"]', function() {
        var classList = $(this).attr('class').split(/\s+/);
        for (var i = 0; i < classList.length; i++) {
            var className = classList[i];
            if (className.indexOf(redirectClassPrefix) !== -1) {
                var url = className.substr(redirectClassPrefix.length);
                window.location.href = url;
            }
        }
    });

    $('body').on('click', '.redirect-to-associates-apply-form', function() {
        window.location.href = '/associates/apply';
    });

    //hero__form count
    $( ".numberplate-matrix-dashboard .numberplate__input, .js-numberplate-form .numberplate__input" ).keydown(function() {

        var count = this.value.length;

        if(count >= 3){
            $(this).parent().find('button').addClass('numberplate__button__active');
        }else{
            $(this).parent().find('button').removeClass('numberplate__button__active');
        }

    });

    $('.numberplate-matrix-dashboard .numberplate__input, .js-numberplate-form .numberplate__input').on('input',function(e){
        var count = this.value.length;

        if(count >= 3){
            $(this).parent().find('button').addClass('numberplate__button__active');
        }else{
            $(this).parent().find('button').removeClass('numberplate__button__active');
        }

    });

    var hiddenLogoTimeouts = [];
    $('.offered-value-hidden-company-logo').on('mouseenter', function() {
        var $box = $(this);
        clearTimeout(hiddenLogoTimeouts[$box.attr('id')]);
        $('.lock-information', $box).fadeIn();
    });
    $('.offered-value-hidden-company-logo').on('mouseleave', function() {
        var $box = $(this);
        clearTimeout(hiddenLogoTimeouts[$box.attr('id')]);
        hiddenLogoTimeouts[$box.attr('id')] = setTimeout(function() {
            $('.lock-information', $box).fadeOut();
        }, 300);
    });
});

$(function() {
    var $dropArea = $('#drop-area');
    var addVehicleImageUrl = $dropArea.attr('data-add-vehicle-image-url');
    var $imagesHolder = $('#images-holder');

    if ($dropArea.length === 0) {
        return;
    }

    $dropArea.dmUploader({
        url: addVehicleImageUrl,
        onUploadSuccess: function(id, response) {
            $imagesHolder.append(response['data']['imageBoxHtml'])
        },
        onUploadError: function(id, response) {
            var jsonResponse = response.responseJSON;

            if (jsonResponse['message']) {
                swal(jsonResponse['message']);
            } else {
                swal('Error. Try to refresh the page and add image again. Sorry for inconvenience.')
            }
            $('#uploaded-image-preloader').hide();
        },
        onUploadProgress: function(id, percent) {
            $('.upload-images-info').hide();
            $('.thumbnails-holder').removeClass('mobile-hide');
            $('#images-upload-progress').css('width', percent + '%');
        },
        onUploadComplete: function(id) {
            $('#images-upload-progress').hide();
            $('#uploaded-image-preloader').hide();
        },
        onBeforeUpload: function(id) {
            $('#images-upload-progress').css('width','0').show();
            $('#uploaded-image-preloader').show();
        }
    });

    $(document).on('click', '.remove-vehicle-image-button', function() {
        var removeTime = 600;
        var $element = $(this);
        var $imageContainer = $element.parent();
        var imageId = $element.attr('data-vehicle-image-id');
        var $form = $('#remove-vehicle-image-' + imageId);
        var removeUrl = $form.attr('action');
        var token = $('[name=_token]', $form).val();

        $.ajax({
            'type': 'POST',
            'url': removeUrl,
            'data': {
                '_token': token
            },
            'success': function(data) {
                $imageContainer.fadeOut(removeTime);
                setTimeout(function() {
                    $imageContainer.remove();
                    if ($('#images-holder .single-vehicle-image').length === 0) {
                        $('.upload-images-info').fadeIn();
                        $('.thumbnails-holder').addClass('mobile-hide');
                    }
                }, removeTime);
            }
        })
    });
});

$(document).ready(function() {
    $('.filters-container .filters-header .toggle-filters-area').on('click', function() {
        $('.filters-container').toggleClass('expanded');
    });
});

function slug(title, separator) {
    if(typeof separator == 'undefined') separator = '-';

    // Convert all dashes/underscores into separator
    var flip = separator == '-' ? '_' : '-';
    title = title.replace(flip, separator);

    // Remove all characters that are not the separator, letters, numbers, or whitespace.
    title = title.toLowerCase()
        .replace(new RegExp('[^a-z0-9' + separator + '\\s]', 'g'), '');

    // Replace all separator characters and whitespace by a single separator
    title = title.replace(new RegExp('[' + separator + '\\s]+', 'g'), separator);

    return title.replace(new RegExp('^[' + separator + '\\s]+|[' + separator + '\\s]+$', 'g'),'');
}


var vehicleFilters, manufacturersResponse, modelsResponse, summaryResponse;

function initFilters() {
    $.ajax('/api/vehicle-filters/manufacturers-group', {
        success: function(data) {
            manufacturersResponse = data;
        }
    });

    $.ajax('/api/vehicle-filters/models-group', {
        success: function(data) {
            modelsResponse = data;
        }
    });

    $.ajax('/api/vehicle-filters/vehicles-summary', {
        success: function(data) {
            summaryResponse = data;
        }
    });

    var allAttempts = 0;
    var vehicleFiltersInitializationInterval = setInterval(function() {
        if (allAttempts++ > 40) {
            clearInterval(vehicleFiltersInitializationInterval);
            console.log('filters problem');
        }
        if (!manufacturersResponse || !modelsResponse || !summaryResponse) {
            return;
        }

        clearInterval(vehicleFiltersInitializationInterval);

        vehicleFilters = new VehicleFilters({
            vehicleManufacturers: manufacturersResponse.manufacturers,
            vehicleModels: modelsResponse.models,
            summary: summaryResponse.summary,
            container: $('.search-manufacturers'),
            selectedVehiclesSummaryTextContainer: $('.selected-manufacturers'),
            modelsAndManufacturersHolder: $('.models-and-manufacturers-holder')
        });

        vehicleFilters.initialize();
    }, 300);
}

// marketplace alerts
$(function() {
    var mouseEnterEvents = {};
    var mouseExitEvents = {};

    $('.close').on('click', function () {
        $('.blackout').removeClass('active');
    });


    $(document).click(function(event) {

        if(!$(event.target).hasClass('selected-manufacturers')) {

            if($('#myModal').is(':visible')){

                if ($(event.target).closest("#myModal").length===0 && ($(event.target).closest(".fa-arrow-circle-up").length===0 && $(event.target).closest(".fa-arrow-circle-down").length===0)) {


                    $('.blackout').removeClass('active');
                    $('#myModal').hide();

                }
            }

        }


    });


    $('.manufacturers-summary .modal-trigger-area').on('mouseenter', function() {
        var $element = $(this);
        var $modal = $('.models-and-manufacturers-modal', $element.parent());
        if (mouseExitEvents[$modal.attr('id')]) {
            clearTimeout(mouseExitEvents[$modal.attr('id')]);
        }

        mouseEnterEvents[$modal.attr('id')] = setTimeout(function() {
            $modal.fadeIn();
        }, 300);
    });

    $('.manufacturers-summary .modal-trigger-area').on('mouseleave', function() {
        var $element = $(this);
        var $modal = $('.models-and-manufacturers-modal', $element.parent());

        if (mouseEnterEvents[$modal.attr('id')]) {
            clearTimeout(mouseEnterEvents[$modal.attr('id')]);
        }

        mouseExitEvents[$modal.attr('id')] = setTimeout(function() {
            $modal.fadeOut();
        }, 300);
    });


    $('.save-alert-button').on('click', function(event) {
        event.preventDefault();

        $.ajax('/api/marketplace-alerts/alert-configuration', {
            'method': 'POST',
            'data': getFormData($("#filters-form")),
            'success': function(data) {
                $('.alert-name-error-box').text('');

                var $alertNameField = $('[name=alert-name]');
                $alertNameField.removeAttr('placeholder');
                $alertNameField.attr('placeholder', data.nextFreeDefaultName);

                var $instantHolder = $('.frequency-field-instant-holder');
                $('.frequency-field-daily-holder .iCheck-helper').click();
                if ($instantHolder.hasClass("tooltipstered")) {
                    $instantHolder.tooltipster('destroy');
                }

                if (data.isInstantAlertPossible) {
                    $('.frequency-field-instant').removeAttr('disabled');
                    $instantHolder.removeClass('not-allowed');
                    $('.frequency-field-instant-holder .iCheck-helper').css('cursor', 'pointer');
                    $instantHolder.attr('title', '');
                } else {
                    $('.frequency-field-instant').attr('disabled', 'disabled');
                    $instantHolder.addClass('not-allowed');
                    $('.frequency-field-instant-holder .iCheck-helper').css('cursor', 'not-allowed');
                    $instantHolder.attr('title', 'Your selected filters are too broad for instant alerts.\n' +
                        'For real time alerts on individual vehicles, please select more specific filter options.')
                    $instantHolder.tooltipster({
                        theme: 'tooltipster-borderless'
                    });
                }
                $('.loader').show();
            },
            'error': function(){
                swal('Sorry, this service is temporary unavailable, please try again later. If the problem appear regularly - contact us');
            }
        });
    });

    $('.create-alert').on('click', function() {
        var $element = $(this);
        var $errorBox = $('.alert-name-error-box');
        var $alertName = $('[name=alert-name]');
        var isErrorVisible = $errorBox.text().length > 0;

        if ($alertName.val() && $alertName.val().length < 3) {
            $errorBox.text('Name is too short (minimum 3 characters)');
            if (isErrorVisible) {
                $errorBox.fadeOut(200).fadeIn(200);
            }

            return;
        }

        $errorBox.text('');

        $('.create-alert-loader').show();
        $element.hide();

        $.ajax('/api/marketplace-alerts', {
            'method': 'POST',
            'data': getFormData($("#filters-form")),
            'success': function() {
                $('.loader').hide();
                swal({
                    title: "Success!",
                    text: "Now that your alert has been set, we'll notify you of any new vehicles that match your filters.",
                    icon: "success",
                });
            },
            'complete': function() {
                $('.create-alert-loader').hide();
                $element.show();
            },
            'error': function(data){
                console.log(data.responseJSON);

                var errorText = 'Error! ';

                if (data.responseJSON.errors) {
                    for (var i in data.responseJSON.errors) {
                        var error = data.responseJSON.errors[i];
                        errorText += error + ' ';
                    }
                }

                swal(errorText);
            }
        });
    });

    $('.loader').on('click', function(event) {
        var $explicitTarget = $(event.originalEvent.explicitOriginalTarget);
        if ($explicitTarget.hasClass('loader')) {
            $('.loader').hide();
        }
    });

    $('.close-button').on('click', function() {
        $('.loader').hide();
    });
});



function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        if (n['name'].indexOf('[]') !== -1) {
            if (!indexed_array[n['name']]) {
                indexed_array[n['name']] = [];
            }
            indexed_array[n['name']].push(n['value']);
        } else {
            indexed_array[n['name']] = n['value'];
        }
    });

    return indexed_array;
}

var VehicleFilters = function(data) {
    var filters = this;

    filters.vehicleManufacturers = data.vehicleManufacturers;
    filters.vehicleModels = data.vehicleModels;
    filters.summary = data.summary;
    filters.container = data.container;
    filters.manufacturersListContainer = $('.manufacturers-list', filters.container);
    filters.searchInput = $('.vehicle-manufacturers-and-models-search', filters.container);
    filters.selectedVehiclesSummaryTextContainer = data.selectedVehiclesSummaryTextContainer;
    filters.modelsAndManufacturersHolder = data.modelsAndManufacturersHolder;
    filters.selectedFilters = {};

    filters.initialize = function() {
        filters.initializeManufacturers();
        //filters.loadLocalStorage();
        filters.applyPreviousFilters();
        filters.showManufacturersByFilters();
        filters.bindExpandButtons();
        filters.bindSelectManufacturerButton();
        filters.bindSelectModelButton();
        filters.updateFiltersSummaryText();
        filters.bindShowHolderButton();
        filters.bindSearch();
        filters.bindShowAllManufacturersButton();
        filters.bindShowAllModelsButton();
        filters.bindChangeVehicleTypeButton();
        filters.unselectAll();
        filters.saveFilters();

        $('.single-manufacturer').each(function(){

            if($(this).hasClass('active')){
                $(this).addClass('selected');
            }
        });

        //filters.startExpand();
    };

    filters.showedVehicleTypes = 'ALL';

    filters.bindChangeVehicleTypeButton = function() {
        $('.iCheck-helper').click(function(){
            var type = $('.vehicle-section .checked input').val();
            if(type == 'car') {
                filters.showedVehicleTypes = 'CAR';
                filters.searchInput.val('');
                filters.updateSearchResults('');
                $('.LCV:not(.CAR)').removeClass('active');
                $('.CAR').addClass('active');
                $('.search-section').hide();
                $('.show-all-manufacturers-button').hide();
            }
            if(type == 'van') {
                filters.showedVehicleTypes = 'LCV';
                filters.searchInput.val('');
                filters.updateSearchResults('');
                $('.CAR:not(.LCV)').removeClass('active');
                $('.LCV').addClass('active');
                $('.search-section').hide();
                $('.show-all-manufacturers-button').hide();
            }
            if(type == 'all') {
                filters.showedVehicleTypes = 'ALL';
                filters.searchInput.val('');
                filters.updateSearchResults('');
                $('.CAR').addClass('active');
                $('.LCV').addClass('active');
                $('.search-section').show();
                $('.show-all-manufacturers-button').show();
            }
        });
    };

    filters.bindShowAllModelsButton = function() {
        $(filters.container).on('click', '.show-all-models-button', function(event) {
            event.preventDefault();
            var $element = $(this);
            var $elementContainer = $element.parent();

            if (!$element.hasClass('active')) {
                return;
            }

            var $models = $element.parent().parent();
            var $manufacturer = $element.parent().parent().parent().parent();
            var manufacturerLabel = $manufacturer.attr('data-label');

            $('.single-model', $models).remove();

            if (!$element.hasClass('expanded')) {
                $element.text('SHOW FEWER MODELS');
                $element.addClass('expanded');
                filters.showAllManufacturerModels(manufacturerLabel, $manufacturer, $models);

                var $manufacturersAndModelsInput = $('.manufacturers-and-models-input', $manufacturer);
                if ($manufacturersAndModelsInput.length > 0) {
                    var manufacturersAndModels = $manufacturersAndModelsInput.val();
                    var selectedModelsIds = manufacturersAndModels.split(';')[1];
                    selectedModelsIds = !selectedModelsIds ? [] : selectedModelsIds.split(',');
                    for(var i=0; i<selectedModelsIds.length;i++) selectedModelsIds[i] = +selectedModelsIds[i];

                    if (selectedModelsIds.length === 0) {
                        $('.single-model', $manufacturer).addClass('selected');
                    } else {
                        var modelsLabelsToCheck = filters.getModelsLabelsByModelsIds(manufacturerLabel, selectedModelsIds);

                        for (var modelKey in modelsLabelsToCheck) {
                            var modelLabel = modelsLabelsToCheck[modelKey];
                            $('.single-model[data-label="'+ modelLabel +'"]').addClass('selected');
                        }
                    }
                }
            } else {

                $element.parent().parent().empty();
                $element.parent().parent().parent().parent().parent().scrollTop(0);
                $element.text('SHOW ALL MODELS');
                $element.removeClass('expanded');
                filters.showModelsWithVehicles(manufacturerLabel, $manufacturer, $models);
                var $manufacturersAndModelsInput = $('.manufacturers-and-models-input', $manufacturer);
                if ($manufacturersAndModelsInput.length > 0) {
                    var manufacturersAndModels = $manufacturersAndModelsInput.val();
                    var selectedModelsIds = manufacturersAndModels.split(';')[1];
                    selectedModelsIds = !selectedModelsIds ? [] : selectedModelsIds.split(',');
                    for(var i=0; i<selectedModelsIds.length;i++) selectedModelsIds[i] = +selectedModelsIds[i];
                    var modelsLabelsToShow = filters.getModelsLabelsByModelsIds(manufacturerLabel, selectedModelsIds);

                    for (var modelLabelKey in modelsLabelsToShow) {
                        var modelLabel = modelsLabelsToShow[modelLabelKey];
                        var $addedModel = $('.single-model[data-label="' + modelLabel + '"]', $models);

                        if ($addedModel.length == 0) {
                            $addedModel = filters.addModelToManufacturer({
                                label: modelLabel,
                                vehiclesCount: filters.getVehicleCountByModelLabel(modelLabel)
                            }, $models);
                        }

                        $addedModel.addClass('selected');
                    }
                }
            }

            $models.append($elementContainer);
        });
    };

    filters.getVehicleCountByModelLabel = function(modelLabel) {
        var vehiclesCount = 0;

        for (var modelSummaryKey in filters.summary.modelsSummaries) {
            var summary = filters.summary.modelsSummaries[modelSummaryKey];
            if (summary.label == modelLabel) {
                vehiclesCount += summary.vehiclesCount;
            }
        }

        return vehiclesCount;
    };

    filters.getModelsLabelsByModelsIds = function(manufacturerLabel, selectedModelsIds) {
        var modelsLabelsToShow = [];

        for (var groupKey in filters.vehicleModels[manufacturerLabel].modelsGroups) {
            var modelsGroups = filters.vehicleModels[manufacturerLabel].modelsGroups[groupKey];
            var modelLabel = modelsGroups.label;
            for (var modelGroupKey in modelsGroups.modelsIds) {
                var modelId = modelsGroups.modelsIds[modelGroupKey];

                if (selectedModelsIds.indexOf(parseInt(modelId)) !== -1 && modelsLabelsToShow.indexOf(modelLabel) == -1) {
                    modelsLabelsToShow.push(modelLabel);
                }
            }
        }

        return modelsLabelsToShow;
    };

    filters.bindShowAllManufacturersButton = function() {
        $('.show-all-manufacturers-button', filters.container).on('click', function(event) {
            event.preventDefault();
            var $clickedElement = $(this);

            if (!$clickedElement.hasClass('active')) {
                return;
            }

            if ($clickedElement.hasClass('expanded')) {
                $clickedElement.text('SHOW ALL MANUFACTURERS');
                $clickedElement.removeClass('expanded');
                var $manufacturers = $('.single-manufacturer', filters.manufacturersListContainer);

                $manufacturers.each(function() {
                    var $manufacturer = $(this);

                    if ((parseInt($('.vehicles-count', $manufacturer).text()) == 0 && !$manufacturer.hasClass('selected') ) || ($('.vehicles-count', $manufacturer).text()=='SELECT ALL' && !$manufacturer.hasClass('selected') )) {
                        $manufacturer.removeClass('active');
                    }
                });
            } else {
                $clickedElement.text('SHOW FEWER MANUFACTURERS');
                $clickedElement.addClass('expanded');
                $('.single-manufacturer', filters.manufacturersListContainer).addClass('active');
            }

            $('.vehicles-count').each(function(){

                if($(this).text()=='0'){
                    $(this).html('<div class="model-label">SELECT ALL</div>');
                }

            });

            //filters.sortManufacturers();
        });
    };

    filters.applyPreviousFilters = function() {
        var previousFilters = JSON.parse($('.previousVehicleFilters').attr('data-previous-filters'));

        var selectedManufacturers = 0;

        for (var manufacturerFilterKey in previousFilters.vehicleManufacturersFilters) {
            var vehicleManufacturerFilter = previousFilters.vehicleManufacturersFilters[manufacturerFilterKey];
            var manufacturersIds = vehicleManufacturerFilter.manufacturersIds.join(',');
            var $singleManufacturer = $('.single-manufacturer[data-manufacturers-ids="' + manufacturersIds + '"]', filters.manufacturersListContainer);
            var manufacturerName = $singleManufacturer.attr('data-label');

            var modelsIds = vehicleManufacturerFilter.modelsIds;

            var manufacturerModelsGroups = filters.vehicleModels[manufacturerName].modelsGroups;
            var selectedModelsCount = 0;

            if (modelsIds.length > 0) {
                for (var manufacturerModelGroupKey in manufacturerModelsGroups) {
                    var manufacturerModelsGroup = manufacturerModelsGroups[manufacturerModelGroupKey];
                    for (var modelIdKey in modelsIds) {
                        var modelId = modelsIds[modelIdKey];

                        if (manufacturerModelsGroup.modelsIds.indexOf(modelId) !== -1) {
                            selectedModelsCount++;
                            break;
                        }
                    }
                }
            }

            $singleManufacturer.append('<input ' +
                'class="manufacturers-and-models-input" ' +
                'type="hidden" name="manufacturers-and-models[]" ' +
                'data-manufacturer-label="' + $singleManufacturer.attr('data-label') + '" ' +
                'data-selected-models-count="' + (modelsIds.length === 0 ? 'all' : selectedModelsCount) +'" ' +
                'value="' + manufacturersIds + ';'+modelsIds.join(',')+'">'
            );

            filters.selectManufacturer($singleManufacturer);
            if (modelsIds.length > 0) {
                filters.markManufacturerAsPartiallySelected($singleManufacturer);
            } else {
                filters.markManufacturerAsFullySelected($singleManufacturer);
            }

            filters.updateFiltersSummaryText();
            selectedManufacturers++;
        }
    };

    filters.bindShowHolderButton = function() {
        filters.selectedVehiclesSummaryTextContainer.on('click', function() {
            if (filters.modelsAndManufacturersHolder.is(':visible')) {
                filters.modelsAndManufacturersHolder.hide();
            } else {
                filters.modelsAndManufacturersHolder.show();
            }
        });
    };

    filters.updateFiltersSummaryText = function() {
        var $manufacturersAndModelsInput = $('.manufacturers-and-models-input', filters.manufacturersListContainer);

        if ($manufacturersAndModelsInput.length === 0) {

            filters.selectedVehiclesSummaryTextContainer.text('Manufacturers (all)');
            filters.selectedVehiclesSummaryTextContainer.attr('title', 'Manufacturers (all)');
            filters.selectedVehiclesSummaryTextContainer.tooltipster('destroy');
            filters.selectedVehiclesSummaryTextContainer.tooltipster({
                theme: 'tooltipster-borderless'
            });
            return;
        }

        var showedTextParts = [];

        $manufacturersAndModelsInput.each(function() {
            var $input = $(this);

            var manufacturerLabel = $input.attr('data-manufacturer-label');
            var selectedModelsCount = $input.attr('data-selected-models-count');
            var showedText = '';

            showedText += manufacturerLabel + ' ';

            if (selectedModelsCount == 'all') {
                showedText += ' (all)';
            } else {
                showedText += '(' + selectedModelsCount + ' '+ (selectedModelsCount == 1 ? 'model' : 'models') +')';
            }

            showedTextParts.push(showedText);
        });

        var stringText = showedTextParts.join(',');

        filters.selectedVehiclesSummaryTextContainer.text(stringText);
        filters.selectedVehiclesSummaryTextContainer.attr('title', stringText);
        filters.selectedVehiclesSummaryTextContainer.tooltipster('destroy');
        filters.selectedVehiclesSummaryTextContainer.tooltipster({
            theme: 'tooltipster-borderless'
        });
    };

    filters.addManufacturerToList = function(manufacturersGroup, show = 'block') {

        var slugName = slug(manufacturersGroup.label);

        if(manufacturersGroup.label=='RENAULT TRUCKS UK') {

            slugName = 'renault';
        }

        if(manufacturersGroup.label=='MG MOTOR UK') {

            slugName = 'mg';
        }

        if(manufacturersGroup.label=='MITSUBISHI CV' || manufacturersGroup.label=='MITSUBISHI FUSO') {

            slugName = 'mitsubishi';
        }

        if(manufacturersGroup.label=='ISUZU TRUCKS') {

            slugName = 'isuzu';
        }

        var $manufacturersGroupElement = $('' +
            '<div class="single-manufacturer row nopadding '+manufacturersGroup.type+'" data-manufacturers-ids="' +  manufacturersGroup.manufacturersIds.join(',') + '" data-label="' + manufacturersGroup.label + '">' +
            '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 model-left-side nopadding"><img class="manufacturer-logo" src="/images/manufacturers/'+slugName+'.png"/></div>'+
            '<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 model-right-side nopadding">'+
            '<div class="flexer">'+
            '<span class="select-manufacturer">' +
            '<span class="manufacturer-label lead-form__slider__info">' + manufacturersGroup.label + '  </span>' +
            '<span class="vehicles-count">SELECT ALL</span>' +
            '</span>' +
            '<span class="expand-models active modal-filter-btn button--orange-bg">SHOW MODELS<i class="fa fa-arrow-circle-down"></i></span>' +
            '</div>'+
            '<div class="models-list" style="display:'+show+'"></div>' +
            '</div>' +
            '</div>');

        filters.manufacturersListContainer.append($manufacturersGroupElement);


    };

    filters.initializeManufacturers = function() {
        for (var manufacturerGroupKey in filters.vehicleManufacturers) {
            var manufacturersGroup = filters.vehicleManufacturers[manufacturerGroupKey];
            filters.addManufacturerToList(manufacturersGroup, 'none');
        }
    };

    filters.showManufacturersByFilters = function() {
        $('.single-manufacturer', filters.manufacturersListContainer).each(function() {
            var $element = $(this);
            $element.removeClass('active');
            $('.vehicles-count', $element).text(0);
            //$('.vehicles-count', $element).html('<div class="model-label">SELECT ALL</div>');
        });

        for (var summaryKey in filters.summary.manufacturersSummaries) {
            var manufacturerSummary = filters.summary.manufacturersSummaries[summaryKey];
            var $singleManufacturer = $('.single-manufacturer[data-label="'+ manufacturerSummary.label +'"]', filters.manufacturersListContainer);

            var currentVehicleCount = parseInt($('.vehicles-count', $singleManufacturer).text());
            var newVehiclesCount = currentVehicleCount + manufacturerSummary.vehiclesCount;

            if (newVehiclesCount) {
                $singleManufacturer.addClass('active');
            }


            $('.vehicles-count', $singleManufacturer).html('<div class="model-label">SELECT ALL</div>');
        }

        $('.single-manufacturer.selected', filters.manufacturersListContainer).addClass('active');

        $('.vehicles-count').each(function(){

            if($(this).text()=='0'){
                $(this).html('<div class="model-label">SELECT ALL</div>');
            }

        });

        //filters.sortManufacturers();
    };

    filters.sortManufacturers = function() {
        var $visibleManufacturers = $('.single-manufacturer', filters.manufacturersListContainer);

        $visibleManufacturers.sort(function(elementA, elementB) {
            var vehiclesCountA = parseInt($('.vehicles-count', elementA).text());
            var vehiclesCountB = parseInt($('.vehicles-count', elementB).text());

            return vehiclesCountB - vehiclesCountA;
        }).appendTo(filters.manufacturersListContainer);
    };

    filters.showModelsWithVehicles = function(manufacturerLabel, $singleManufacturer, $models) {

        for (var modelsSummaryKey in filters.summary.modelsSummaries) {
            var modelSummary = filters.summary.modelsSummaries[modelsSummaryKey];


            if (modelSummary.manufacturerLabel !== manufacturerLabel) {

                continue;
            }

            var $previousModelsGroup = $('.single-model[data-label="'+ modelSummary.label +'"]', $singleManufacturer);

            if ($previousModelsGroup.length === 0) {
                filters.addModelToManufacturer(modelSummary, $models);

                continue;
            }

            var previousVehiclesCount = parseInt($('.model-vehicle-count', $previousModelsGroup).text());
            $('.model-vehicle-count', $previousModelsGroup).html('<i class="fa fa-car" style="margin-right:5px"></i>' + (previousVehiclesCount + modelSummary.vehiclesCount));
        }
    };

    filters.showAllManufacturerModels = function(manufacturerLabel, $singleManufacturer, $models) {

        for (var modelGroupKey in filters.vehicleModels[manufacturerLabel].modelsGroups) {
            var modelGroup = filters.vehicleModels[manufacturerLabel].modelsGroups[modelGroupKey];
            filters.addModelToManufacturer({type:modelGroup.type, label: modelGroup.label, vehiclesCount: filters.getVehicleCountByModelLabel(modelGroup.label)}, $models);
        }
    };

    filters.addModelToManufacturer = function(modelSummary, $models) {

        var type = $('.vehicle-section .checked input').val();
        if(type == 'car') {
            type = 'CAR';
        }
        if(type == 'van') {
            type = 'LCV';
        }

        var $newElement = $('' +
            '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-bottom:7px">'+
            '<div class="single-model" data-label="' + modelSummary.label + '">' +
            '<span class="model-label">' + modelSummary.label + '</span>' +
            '<span class="model-vehicle-count"><i class="fa fa-car" style="margin-right:5px"></i>' + modelSummary.vehiclesCount + '</span>' +
            '</div>'+
            '</div>');

        if(type==modelSummary.type || type=='all') {

            $models.append($newElement);
            return $newElement;
        }
        /*
        var $newElement = $('' +
            '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-bottom:7px">'+
            '<div class="single-model" data-label="' + modelSummary.label + '">' +
            '<span class="model-label">' + modelSummary.label + '</span>' +
            '<span class="model-vehicle-count"><i class="fa fa-car" style="margin-right:5px"></i>' + modelSummary.vehiclesCount + '</span>' +
            '</div>'+
            '</div>');

        $models.append($newElement);
        return $newElement;

         */

    };

    filters.addShowAllModelsButton = function($models) {

        $models.append('<div class="col-md-12" style="text-align: center;float:left;margin-top:7px;margin-bottom:10px"><a style="display:block;width:100%" class="button--narrow button--orange show-all-models-button active" href="#">SHOW ALL MODELS</a></div>');
    };

    filters.expandModels = function($clickedElement, $singleManufacturer, $models) {
        var isExpanded = $singleManufacturer.hasClass('expanded');
        $clickedElement.parent().next().empty();

        if (isExpanded) {
            $singleManufacturer.removeClass('expanded');
            $('.single-model', $models).remove();
            $clickedElement.html('SHOW MODELS<i class="fa fa-arrow-circle-down"></i>');
            $('.show-all-models-button', $models).remove();

            return;
        }

        var manufacturerLabel = $singleManufacturer.attr('data-label');
        $singleManufacturer.addClass('expanded');

        $clickedElement.html('HIDE MODELS<i class="fa fa-arrow-circle-up"></i>');

        var $manufacturersAndModelsInput = $('.manufacturers-and-models-input', $singleManufacturer);

        if ($manufacturersAndModelsInput.length === 0) {
            if (parseInt($('.vehicles-count', $singleManufacturer).text()) > 0) {
                if (isSearchModeEnabled) {
                    filters.showAllManufacturerModels(manufacturerLabel, $singleManufacturer, $models);
                } else {
                    filters.showModelsWithVehicles(manufacturerLabel, $singleManufacturer, $models);
                    filters.addShowAllModelsButton($models);
                }
            } else {
                filters.showAllManufacturerModels(manufacturerLabel, $singleManufacturer, $models);

            }

            return;
        }

        var manufacturersAndModels = $manufacturersAndModelsInput.val();

        var selectedModelsIds = manufacturersAndModels.split(';')[1];
        selectedModelsIds = !selectedModelsIds ? [] : selectedModelsIds.split(',');
        for(var i=0; i<selectedModelsIds.length;i++) selectedModelsIds[i] = +selectedModelsIds[i];

        if (selectedModelsIds.length === 0) {
            if (parseInt($('.vehicles-count', $singleManufacturer).text()) > 0) {
                if (isSearchModeEnabled) {
                    filters.showAllManufacturerModels(manufacturerLabel, $singleManufacturer, $models);
                } else {
                    filters.showModelsWithVehicles(manufacturerLabel, $singleManufacturer, $models);
                    filters.addShowAllModelsButton($models);
                }
            } else {
                filters.showAllManufacturerModels(manufacturerLabel, $singleManufacturer, $models);
            }

            $('.single-model', $models).addClass('selected');

            return;
        }

        var modelsLabelsToShow = [];

        for (var groupKey in filters.vehicleModels[manufacturerLabel].modelsGroups) {
            var modelsGroups = filters.vehicleModels[manufacturerLabel].modelsGroups[groupKey];
            var modelLabel = modelsGroups.label;
            for (var modelGroupKey in modelsGroups.modelsIds) {
                var modelId = modelsGroups.modelsIds[modelGroupKey];

                if (selectedModelsIds.indexOf(parseInt(modelId)) !== -1 && modelsLabelsToShow.indexOf(modelLabel) == -1) {
                    modelsLabelsToShow.push(modelLabel);
                }
            }
        }

        filters.showModelsWithVehicles(manufacturerLabel, $singleManufacturer, $models);

        for (var modelLabelKey in modelsLabelsToShow) {
            var modelLabel = modelsLabelsToShow[modelLabelKey];
            var $addedModel = $('.single-model[data-label="'+modelLabel+'"]', $models);

            if ($addedModel.length == 0) {
                $addedModel = filters.addModelToManufacturer({label: modelLabel, vehiclesCount: 0}, $models);
            }

            $addedModel.addClass('selected');
        }

        filters.addShowAllModelsButton($models);
    };

    filters.startExpand = function() {

        $(".single-manufacturer").each(function() {
            if($(this).hasClass('active')){

                $(this).find('.expand-models').trigger('click');
            }
        });

    };

    filters.bindExpandButtons = function() {
        $(filters.manufacturersListContainer).on('click', '.single-manufacturer .expand-models.active', function() {
            var $clickedElement = $(this);
            var $singleManufacturer = $clickedElement.parent().parent().parent();
            var $models = $('.models-list', $singleManufacturer);

            filters.expandModels($clickedElement, $singleManufacturer, $models);

            if($models.is(":visible")) {

                $models.hide();
            }else{
                $models.show();
            }
        });
    };

    filters.unselectAll = function() {

        $('.clear-filters').on('click', function(e){

            e.preventDefault();
            $('.single-manufacturer').removeClass('selected');
            $('.single-manufacturer').removeClass('expanded');
            $('.models-list .single-model').removeClass('selected');
            $('.vehicle-manufacturers-and-models-search').val('');
            $('.manufacturers-and-models-input').remove();
            filters.updateFiltersSummaryText();
            filters.disableSearchMode();

            filters.showManufacturersByFilters();

            $('.models-list').hide();
        });

    };

    filters.loadLocalStorage = function() {

        var data = JSON.parse(localStorage.getItem('filters'));

        console.log(data);
    };

    filters.saveFilters = function() {

        $('.save-filters').on('click', function(e){

            e.preventDefault();
            $('.blackout').removeClass('active');
            $('#myModal').hide();

            var filters = [];
            $('.single-manufacturer.selected').each(function(){

                filters.push({label:$(this).find('input').attr('data-manufacturer-label'),value:$(this).find('input').val()});


            });
        });
    };

    filters.unselectManufacturer = function($singleManufacturer) {
        $singleManufacturer.removeClass('selected');
    };

    filters.unselectAllManufacturerModels = function($singleManufacturer) {
        $('.models-list .single-model', $singleManufacturer).removeClass('selected');
    };

    filters.removeManufacturerFromFilters = function($singleManufacturer) {
        $('.manufacturers-and-models-input', $singleManufacturer).remove();
        filters.updateFiltersSummaryText();
    };

    filters.selectManufacturer = function($singleManufacturer) {
        $singleManufacturer.addClass('selected');
    };

    filters.selectAllManufacturerModels = function($singleManufacturer) {
        $('.single-model', $singleManufacturer).addClass('selected');
    };

    filters.addManufacturerToFilters = function($singleManufacturer) {
        var manufacturersIds = $singleManufacturer.attr('data-manufacturers-ids');

        $singleManufacturer.append('<input ' +
            'class="manufacturers-and-models-input" ' +
            'type="hidden" name="manufacturers-and-models[]" ' +
            'data-manufacturer-label="' + $singleManufacturer.attr('data-label') + '" ' +
            'data-selected-models-count="all" ' +
            'value="' + manufacturersIds + ';">'
        );

        filters.markManufacturerAsFullySelected($singleManufacturer);
        filters.updateFiltersSummaryText();
    };

    filters.selectManufacturerModel = function($clickedElement) {
        $($clickedElement).addClass('selected');
    };

    filters.unselectManufacturerModel = function($clickedElement) {
        $($clickedElement).removeClass('selected');
    };

    filters.getModelsIdsByManufacturerModelLabel = function(manufacturerLabel, modelLabel) {
        var modelsGroups = filters.vehicleModels[manufacturerLabel]['modelsGroups'];

        for (var modelsGroupsKey in modelsGroups) {
            var modelGroup = modelsGroups[modelsGroupsKey];

            if (modelLabel == modelGroup.label) {
                return modelGroup.modelsIds;
            }
        }

        return [];
    };

    filters.markManufacturerAsPartiallySelected = function ($singleManufacturer) {
        $singleManufacturer.addClass('partially-selected');
    };

    filters.markManufacturerAsFullySelected = function ($singleManufacturer) {
        $singleManufacturer.removeClass('partially-selected');
    };

    filters.addModelToFilters = function($clickedElement, $singleManufacturer) {
        var manufacturersIds = $singleManufacturer.attr('data-manufacturers-ids');
        var manufacturerLabel = $singleManufacturer.attr('data-label');
        var modelLabel = $clickedElement.attr('data-label');

        var modelsIds = filters.getModelsIdsByManufacturerModelLabel(manufacturerLabel, modelLabel);
        var $manufacturersAndModelsInput = $('.manufacturers-and-models-input', $singleManufacturer);

        if ($manufacturersAndModelsInput.length) {
            var oldValue = $manufacturersAndModelsInput.val();
            $manufacturersAndModelsInput.val(oldValue + ',' + modelsIds.join(','));
            var selectedModelsCount = parseInt($manufacturersAndModelsInput.attr('data-selected-models-count'));

            selectedModelsCount++;
            filters.markManufacturerAsPartiallySelected($singleManufacturer);

            $manufacturersAndModelsInput.attr('data-selected-models-count', selectedModelsCount);
        } else {
            var selectedModelsCount = 1;

            filters.markManufacturerAsPartiallySelected($singleManufacturer);

            $singleManufacturer.append('<input ' +
                'class="manufacturers-and-models-input" ' +
                'type="hidden" name="manufacturers-and-models[]" ' +
                'data-manufacturer-label="' + $singleManufacturer.attr('data-label') + '" ' +
                'data-selected-models-count="' + selectedModelsCount + '" ' +
                'value="' + manufacturersIds + ';' + modelsIds.join(',') + '">'
            )
        }

        filters.updateFiltersSummaryText();
    };

    filters.removeModelFromFilters = function($clickedElement, $singleManufacturer) {
        var manufacturersIds = $singleManufacturer.attr('data-manufacturers-ids');
        var manufacturerLabel = $singleManufacturer.attr('data-label');
        var modelLabel = $clickedElement.attr('data-label');

        var modelsIdsToRemove = filters.getModelsIdsByManufacturerModelLabel(manufacturerLabel, modelLabel);
        var $manufacturersAndModelsInput = $('.manufacturers-and-models-input', $singleManufacturer);

        var oldValue = $manufacturersAndModelsInput.val();
        var oldModels = oldValue.split(';')[1];
        oldModels = oldModels ? oldModels.split(',') : [];

        // no models selected so we assume that entire manufacturer has been selected
        if (oldModels.length === 0) {
            var selectedModelsCount = 0;
            $('.single-model.selected', $singleManufacturer).each(function() {
                var $selectedModel = $(this);
                var selectedModelLabel = $selectedModel.attr('data-label');
                var modelsIds = filters.getModelsIdsByManufacturerModelLabel(manufacturerLabel, selectedModelLabel);

                for (var modelIdKey in modelsIds) {
                    var modelId = modelsIds[modelIdKey];
                    oldModels.push(modelId);
                }
                selectedModelsCount++;
                filters.markManufacturerAsPartiallySelected($singleManufacturer);
            });

            $manufacturersAndModelsInput.attr('data-selected-models-count', selectedModelsCount);
        }

        var newModelsIds = oldModels.filter( function( el ) {
            return modelsIdsToRemove.indexOf( parseInt(el) ) < 0;
        } );

        if (newModelsIds.length == 0) {
            $manufacturersAndModelsInput.remove();
            filters.removeManufacturerFromFilters($singleManufacturer);
            filters.unselectManufacturer($singleManufacturer);
            filters.updateFiltersSummaryText();

            return;
        }

        $manufacturersAndModelsInput.attr('data-selected-models-count', $('.single-model.selected', $singleManufacturer).length);
        filters.markManufacturerAsPartiallySelected($singleManufacturer);
        $manufacturersAndModelsInput.val(manufacturersIds + ';' + newModelsIds.join(','));
        filters.updateFiltersSummaryText();
    };

    filters.bindSelectManufacturerButton = function() {
        $(filters.manufacturersListContainer).on('click', '.single-manufacturer .select-manufacturer', function() {
            var $clickedElement = $(this);

            var $singleManufacturer = $clickedElement.parent().parent().parent();

            var isSelected = $singleManufacturer.hasClass('selected');

            if (isSelected) {
                filters.unselectManufacturer($singleManufacturer);
                filters.unselectAllManufacturerModels($singleManufacturer);
                filters.removeManufacturerFromFilters($singleManufacturer);
                $clickedElement.find('input').prop('checked',false);
                $clickedElement.find('.vehicles-count').removeClass('selected');

            } else {
                filters.selectManufacturer($singleManufacturer);
                filters.selectAllManufacturerModels($singleManufacturer);
                filters.addManufacturerToFilters($singleManufacturer);
                $clickedElement.find('input').prop('checked',true);
                $clickedElement.find('.vehicles-count').addClass('selected');

            }
        });
    };

    filters.bindSelectModelButton = function() {
        $(filters.manufacturersListContainer).on('click', '.single-model', function() {
            var $clickedElement = $(this);
            var $singleManufacturer = $clickedElement.parent().parent().parent().parent();



            var isManufacturerSelected = $singleManufacturer.hasClass('selected');
            var isModelSelected = $clickedElement.hasClass('selected');

            if (!isManufacturerSelected) {
                filters.selectManufacturer($singleManufacturer);
                filters.selectManufacturerModel($clickedElement);
                filters.addModelToFilters($clickedElement, $singleManufacturer);

                return;
            }

            if (!isModelSelected) {
                filters.selectManufacturerModel($clickedElement);
                filters.addModelToFilters($clickedElement, $singleManufacturer);

                return;
            }

            filters.unselectManufacturerModel($clickedElement);
            filters.removeModelFromFilters($clickedElement, $singleManufacturer);

        });
    };

    var searchTimeout;

    filters.updateSearchResults = function(searchText) {
        var searchPhrases = [];
        var rawSearchPhrases = searchText.split(' ');
        for (var rawSearchPhrasesIndex in rawSearchPhrases) {
            var rawSearchPhrase = rawSearchPhrases[rawSearchPhrasesIndex];

            if (rawSearchPhrase.length !== 0) {
                searchPhrases.push(rawSearchPhrase);
            }
        }



        var manufacturersAndModelsToDisplay = {};

        for (var manufacturerLabel in filters.vehicleModels) {
            var modelsGroups = filters.vehicleModels[manufacturerLabel]['modelsGroups'];
            var foundModels = [];

            for (var modelGroupIndex in modelsGroups) {
                var modelGroup = modelsGroups[modelGroupIndex];

                if (isEverySearchPhraseMatch(searchPhrases, manufacturerLabel + ' ' + modelGroup.label)) {
                    foundModels.push(modelGroup.label);
                }
            }

            if (foundModels.length > 0) {
                manufacturersAndModelsToDisplay[manufacturerLabel] = foundModels;
            }
        }


        filters.hideAllManufacturers();

        if (searchText.length < 1) {

            filters.disableSearchMode();
            //filters.applyPreviousFilters();
            filters.showManufacturersByFilters();
            //filters.enableSearchMode();
            //filters.showManufacturersBySearchResults(manufacturersAndModelsToDisplay);
            //$('.show-all-manufacturers-button').trigger('click');
            // $('.show-all-manufacturers-button').trigger('click');
            // $('.expand-models.active').trigger('click');
            //  $('.expand-models.active').trigger('click');
            $('.models-list').hide();

        }else{


            filters.enableSearchMode();
            filters.showManufacturersBySearchResults(manufacturersAndModelsToDisplay);

            $('.models-list').each(function(){

                if($(this).parent().parent().hasClass('expanded')) {

                    $(this).show();
                }

            });
        }


        $('.models-list div').each(function(){

            if($(this).children().length==0) {

                $(this).remove();
            }

        });
    };

    filters.bindSearch = function() {
        filters.searchInput.on('keyup', function() {

            if (searchTimeout) {
                clearTimeout(searchTimeout);
            }

            var searchText = $(this).val().toUpperCase();
            searchTimeout = setTimeout(function() {
                filters.updateSearchResults(searchText);
            }, 500);
        });
    };

    var isEverySearchPhraseMatch = function(searchPhrases, text) {
        var matchCount = 0;
        for (var searchPhraseIndex in searchPhrases) {
            var singleSearchPhrase = searchPhrases[searchPhraseIndex];

            if (text.indexOf(singleSearchPhrase) !== -1) {
                matchCount++;
            }
        }

        return (matchCount >= searchPhrases.length)
    };

    var isSearchModeEnabled = false;

    filters.enableSearchMode = function() {
        isSearchModeEnabled = true;
        $('.expand-models', filters.container).each(function() {
            var $button = $(this);
            if ($('.models-list .single-model', $button.parent()).length > 0) {
                $button.removeClass('active');
            } else {
                $button.addClass('active');
            }
        });
        $('.show-all-manufacturers-button', filters.container).removeClass('active');
        $('.show-all-models-button', filters.container).hide();
    };

    filters.disableSearchMode = function() {
        isSearchModeEnabled = false;
        $('.expand-models', filters.container).addClass('active');
        $('.show-all-manufacturers-button', filters.container).addClass('active');
        $('.show-all-models-button', filters.container).show();
    };

    filters.hideAllManufacturers = function() {
        $('.single-manufacturer', filters.manufacturersListContainer).each(function() {
            var $element = $(this);
            $element.removeClass('active');
            $('.single-model', $element).remove();
            $element.removeClass('expanded');
            var $expandedElement = $('.expand-models', $element);
            $expandedElement.html('SHOW MODELS<i class="fa fa-arrow-circle-down"></i>');
        });
    };

    var addResultsToSearch = function(manufacturersAndModelsToDisplay) {
        for (var manufacturerLabel in manufacturersAndModelsToDisplay) {

            var modelsToDisplay = manufacturersAndModelsToDisplay[manufacturerLabel];
            var $singleManufacturer = $('.single-manufacturer[data-label="'+ manufacturerLabel +'"]', filters.manufacturersListContainer);

            $singleManufacturer.addClass('active');
            var $expandedElement = $('.expand-models', $singleManufacturer);
            var $models = $('.models-list', $singleManufacturer);

            var selectedModels = filters.getSelectedModelsLabelsForManufacturer(manufacturerLabel, $singleManufacturer);

            if (modelsToDisplay.length > 0) {
                $singleManufacturer.addClass('expanded');
                $expandedElement.html('SHOW MODELS<i class="fa fa-arrow-circle-down"></i>');

                for (var modelIndex in modelsToDisplay) {
                    var modelLabel = modelsToDisplay[modelIndex];

                    var vehiclesCount = 0;

                    for (var modelSummaryKey in filters.summary.modelsSummaries) {
                        var summary = filters.summary.modelsSummaries[modelSummaryKey];
                        if (summary.label == modelLabel) {
                            vehiclesCount += summary.vehiclesCount;
                        }
                    }

                    var $newModel = filters.addModelToManufacturer({label: modelLabel, vehiclesCount: vehiclesCount}, $models);

                    if (($('.manufacturers-and-models-input', $singleManufacturer).length > 0 && selectedModels.length == 0) || selectedModels.indexOf(modelLabel) != -1) {
                        $newModel.addClass('selected');
                    }
                }
            } else {
                $singleManufacturer.removeClass('expanded');
                $('.single-model', $models).remove();
                $expandedElement.html('SHOW MODELS<i class="fa fa-arrow-circle-down"></i>');
            }
        }
    };

    filters.showManufacturersBySearchResults = function(manufacturersAndModelsToDisplay) {
        addResultsToSearch(manufacturersAndModelsToDisplay);
    };

    filters.getSelectedModelsLabelsForManufacturer = function(manufacturerLabel, $singleManufacturer) {
        var $manufacturersAndModelsInput = $('.manufacturers-and-models-input', $singleManufacturer);

        if ($manufacturersAndModelsInput.length == 0) {
            return [];
        }

        var manufacturersAndModels = $manufacturersAndModelsInput.val();

        var selectedModelsIds = manufacturersAndModels.split(';')[1];
        selectedModelsIds = !selectedModelsIds ? [] : selectedModelsIds.split(',');
        for(var i=0; i<selectedModelsIds.length;i++) selectedModelsIds[i] = +selectedModelsIds[i];

        if (selectedModelsIds.length === 0) {
            return [];
        }

        var modelsLabelsToShow = [];

        for (var groupKey in filters.vehicleModels[manufacturerLabel].modelsGroups) {
            var modelsGroups = filters.vehicleModels[manufacturerLabel].modelsGroups[groupKey];
            var modelLabel = modelsGroups.label;
            for (var modelGroupKey in modelsGroups.modelsIds) {
                var modelId = modelsGroups.modelsIds[modelGroupKey];

                if (selectedModelsIds.indexOf(parseInt(modelId)) !== -1 && modelsLabelsToShow.indexOf(modelLabel) == -1) {
                    modelsLabelsToShow.push(modelLabel);
                }
            }
        }

        return modelsLabelsToShow;
    };

    return filters;
};


$(document).ready(function() {
    if ($('.search-manufacturers').length > 0) {
        initFilters();
    }
});