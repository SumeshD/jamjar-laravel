// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"Bc6M":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.enableBodyScroll = exports.clearAllBodyScrollLocks = exports.disableBodyScroll = void 0;

function _toConsumableArray(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  } else {
    return Array.from(arr);
  }
} // Older browsers don't support event options, feature detect it.
// Adopted and modified solution from Bohdan Didukh (2017)
// https://stackoverflow.com/questions/41594997/ios-10-safari-prevent-scrolling-behind-a-fixed-overlay-and-maintain-scroll-posi


var hasPassiveEvents = false;

if (typeof window !== 'undefined') {
  var passiveTestOptions = {
    get passive() {
      hasPassiveEvents = true;
      return undefined;
    }

  };
  window.addEventListener('testPassive', null, passiveTestOptions);
  window.removeEventListener('testPassive', null, passiveTestOptions);
}

var isIosDevice = typeof window !== 'undefined' && window.navigator && window.navigator.platform && (/iP(ad|hone|od)/.test(window.navigator.platform) || window.navigator.platform === 'MacIntel' && window.navigator.maxTouchPoints > 1);
var locks = [];
var documentListenerAdded = false;
var initialClientY = -1;
var previousBodyOverflowSetting = void 0;
var previousBodyPaddingRight = void 0; // returns true if `el` should be allowed to receive touchmove events.

var allowTouchMove = function allowTouchMove(el) {
  return locks.some(function (lock) {
    if (lock.options.allowTouchMove && lock.options.allowTouchMove(el)) {
      return true;
    }

    return false;
  });
};

var preventDefault = function preventDefault(rawEvent) {
  var e = rawEvent || window.event; // For the case whereby consumers adds a touchmove event listener to document.
  // Recall that we do document.addEventListener('touchmove', preventDefault, { passive: false })
  // in disableBodyScroll - so if we provide this opportunity to allowTouchMove, then
  // the touchmove event on document will break.

  if (allowTouchMove(e.target)) {
    return true;
  } // Do not prevent if the event has more than one touch (usually meaning this is a multi touch gesture like pinch to zoom).


  if (e.touches.length > 1) return true;
  if (e.preventDefault) e.preventDefault();
  return false;
};

var setOverflowHidden = function setOverflowHidden(options) {
  // If previousBodyPaddingRight is already set, don't set it again.
  if (previousBodyPaddingRight === undefined) {
    var _reserveScrollBarGap = !!options && options.reserveScrollBarGap === true;

    var scrollBarGap = window.innerWidth - document.documentElement.clientWidth;

    if (_reserveScrollBarGap && scrollBarGap > 0) {
      previousBodyPaddingRight = document.body.style.paddingRight;
      document.body.style.paddingRight = scrollBarGap + 'px';
    }
  } // If previousBodyOverflowSetting is already set, don't set it again.


  if (previousBodyOverflowSetting === undefined) {
    previousBodyOverflowSetting = document.body.style.overflow;
    document.body.style.overflow = 'hidden';
  }
};

var restoreOverflowSetting = function restoreOverflowSetting() {
  if (previousBodyPaddingRight !== undefined) {
    document.body.style.paddingRight = previousBodyPaddingRight; // Restore previousBodyPaddingRight to undefined so setOverflowHidden knows it
    // can be set again.

    previousBodyPaddingRight = undefined;
  }

  if (previousBodyOverflowSetting !== undefined) {
    document.body.style.overflow = previousBodyOverflowSetting; // Restore previousBodyOverflowSetting to undefined
    // so setOverflowHidden knows it can be set again.

    previousBodyOverflowSetting = undefined;
  }
}; // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions


var isTargetElementTotallyScrolled = function isTargetElementTotallyScrolled(targetElement) {
  return targetElement ? targetElement.scrollHeight - targetElement.scrollTop <= targetElement.clientHeight : false;
};

var handleScroll = function handleScroll(event, targetElement) {
  var clientY = event.targetTouches[0].clientY - initialClientY;

  if (allowTouchMove(event.target)) {
    return false;
  }

  if (targetElement && targetElement.scrollTop === 0 && clientY > 0) {
    // element is at the top of its scroll.
    return preventDefault(event);
  }

  if (isTargetElementTotallyScrolled(targetElement) && clientY < 0) {
    // element is at the bottom of its scroll.
    return preventDefault(event);
  }

  event.stopPropagation();
  return true;
};

var disableBodyScroll = function disableBodyScroll(targetElement, options) {
  // targetElement must be provided
  if (!targetElement) {
    // eslint-disable-next-line no-console
    console.error('disableBodyScroll unsuccessful - targetElement must be provided when calling disableBodyScroll on IOS devices.');
    return;
  } // disableBodyScroll must not have been called on this targetElement before


  if (locks.some(function (lock) {
    return lock.targetElement === targetElement;
  })) {
    return;
  }

  var lock = {
    targetElement: targetElement,
    options: options || {}
  };
  locks = [].concat(_toConsumableArray(locks), [lock]);

  if (isIosDevice) {
    targetElement.ontouchstart = function (event) {
      if (event.targetTouches.length === 1) {
        // detect single touch.
        initialClientY = event.targetTouches[0].clientY;
      }
    };

    targetElement.ontouchmove = function (event) {
      if (event.targetTouches.length === 1) {
        // detect single touch.
        handleScroll(event, targetElement);
      }
    };

    if (!documentListenerAdded) {
      document.addEventListener('touchmove', preventDefault, hasPassiveEvents ? {
        passive: false
      } : undefined);
      documentListenerAdded = true;
    }
  } else {
    setOverflowHidden(options);
  }
};

exports.disableBodyScroll = disableBodyScroll;

var clearAllBodyScrollLocks = function clearAllBodyScrollLocks() {
  if (isIosDevice) {
    // Clear all locks ontouchstart/ontouchmove handlers, and the references.
    locks.forEach(function (lock) {
      lock.targetElement.ontouchstart = null;
      lock.targetElement.ontouchmove = null;
    });

    if (documentListenerAdded) {
      document.removeEventListener('touchmove', preventDefault, hasPassiveEvents ? {
        passive: false
      } : undefined);
      documentListenerAdded = false;
    } // Reset initial clientY.


    initialClientY = -1;
  } else {
    restoreOverflowSetting();
  }

  locks = [];
};

exports.clearAllBodyScrollLocks = clearAllBodyScrollLocks;

var enableBodyScroll = function enableBodyScroll(targetElement) {
  if (!targetElement) {
    // eslint-disable-next-line no-console
    console.error('enableBodyScroll unsuccessful - targetElement must be provided when calling enableBodyScroll on IOS devices.');
    return;
  }

  locks = locks.filter(function (lock) {
    return lock.targetElement !== targetElement;
  });

  if (isIosDevice) {
    targetElement.ontouchstart = null;
    targetElement.ontouchmove = null;

    if (documentListenerAdded && locks.length === 0) {
      document.removeEventListener('touchmove', preventDefault, hasPassiveEvents ? {
        passive: false
      } : undefined);
      documentListenerAdded = false;
    }
  } else if (!locks.length) {
    restoreOverflowSetting();
  }
};

exports.enableBodyScroll = enableBodyScroll;
},{}],"A2T1":[function(require,module,exports) {
"use strict";

var _bodyScrollLock = require("body-scroll-lock");

// [JAMJ] Prepped
// app.js
// All Javascript for the home page.
//  
// @Author: Kubo HQ (Fabio Pinto da Costa)
// @Date: 2020-09-17T15:48
// @Email: developers@kubokhq.com
// const { capture } = require('@pcs/loqate')("JF17-BY24-HU58-RZ33");
(function () {
  // A polyfill for the forEach function in the NodeList object
  if ("NodeList" in window && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;

      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  } // Detect IE 11


  var isIE11 = !!(navigator.userAgent.match(/Trident/) && navigator.userAgent.match(/rv[ :]11/));

  if (isIE11) {
    document.querySelectorAll(".photo-booth label, .services-grid article").forEach(function (element, _) {
      element.classList.add("ie-fix");
    });
  }

  var CAROUSELS_INTERVAL = 5000; // Carousel for the home page customer feedback section

  var CUSTOMER_FEEDBACK_PAGES = 3;
  var _feedbackActiveCounter = 0;

  var handleCarousel = function handleCarousel() {
    _feedbackActiveCounter = (_feedbackActiveCounter + 1) % CUSTOMER_FEEDBACK_PAGES;
    document.querySelectorAll("#homeCustomerFeedback .page-section__content__columns__carousel-tape").forEach(function (element, _) {
      // However, if this is the new page, change it to its active state
      element.style.marginLeft = "-".concat(_feedbackActiveCounter * 100, "%");
    });
    document.querySelectorAll("#homeCustomerFeedbackControls li").forEach(function (element, index) {
      element.classList.remove("active");

      if (index === _feedbackActiveCounter) {
        element.classList.add("active");
      }
    });
  };

  var homeFeedbackCarouselInterval = setInterval(handleCarousel, CAROUSELS_INTERVAL);
  document.querySelectorAll("#homeCustomerFeedbackControls li").forEach(function (element, index) {
    element.onclick = function (e) {
      _feedbackActiveCounter = index;
      clearInterval(homeFeedbackCarouselInterval);
      document.querySelectorAll("#homeCustomerFeedback .page-section__content__columns__carousel-tape").forEach(function (el, _) {
        // However, if this is the new page, change it to its active state
        el.style.marginLeft = "-".concat(_feedbackActiveCounter * 100, "%");
      });
      document.querySelectorAll("#homeCustomerFeedbackControls li").forEach(function (el, _) {
        el.classList.remove("active");
      });
      element.classList.add("active");
      homeFeedbackCarouselInterval = setInterval(handleCarousel, CAROUSELS_INTERVAL);
    };
  }); // Media logos

  var MEDIA_LOGOS_PAGES = 4;
  var _mediaLogosActiveCounter = 0;

  var handleMediaLogosCarousel = function handleMediaLogosCarousel() {
    _mediaLogosActiveCounter = (_mediaLogosActiveCounter + 1) % MEDIA_LOGOS_PAGES;
    document.querySelectorAll(".page-section__content__media-logos__carousel-tape").forEach(function (element, _) {
      // However, if this is the new page, change it to its active state
      element.style.marginLeft = "-".concat(_mediaLogosActiveCounter * 100, "%");
    });
  };

  setInterval(handleMediaLogosCarousel, CAROUSELS_INTERVAL); // Auto logos

  var AUTO_LOGOS_PAGES = 5;
  var _autoLogosActiveCounter = 0;

  var handleAutoLogosCarousel = function handleAutoLogosCarousel() {
    _autoLogosActiveCounter = (_autoLogosActiveCounter + 1) % AUTO_LOGOS_PAGES;
    document.querySelectorAll(".page-section__content__auto-logos__carousel-tape").forEach(function (element, _) {
      // However, if this is the new page, change it to its active state
      element.style.marginLeft = "-".concat(_autoLogosActiveCounter * 100, "%");
    });
  };

  setInterval(handleAutoLogosCarousel, CAROUSELS_INTERVAL); // Open and close mobile menu

  var _mobileMenuOpen = false;

  document.getElementById("mobileMenuButton").onclick = function (e) {
    e.preventDefault();

    var _mobileMenu = document.getElementById("mobileMenu");

    if (_mobileMenuOpen) {
      _mobileMenuOpen = false;

      _mobileMenu.classList.remove("active"); // Clear all body scrolls


      (0, _bodyScrollLock.clearAllBodyScrollLocks)();
    } else {
      _mobileMenuOpen = true;

      _mobileMenu.classList.add("active"); // Lock body scrolling


      (0, _bodyScrollLock.disableBodyScroll)(_mobileMenu);
    }
  };

  window.onresize = function (e) {
    // On resizing, make sure we inactivate the mobile menu once we're
    // passed that threshold
    if (window.innerWidth > 768) {
      _mobileMenuOpen = false;
      document.getElementById("mobileMenu").classList.remove("active"); // Clear all body scrolls

      (0, _bodyScrollLock.clearAllBodyScrollLocks)();
    }
  }; // Active state for the header folder on mobile


  document.querySelectorAll(".header__content__nav__link--folder").forEach(function (element, _) {
    element.onclick = function (e) {
      if (!e.target.classList.contains("header__content__nav__sub-link")) {
        e.preventDefault();

        if (element.classList.contains("active")) {
          element.classList.remove("active");
        } else {
          element.classList.add("active");
        }
      }
    }; // Remove the active class on mouseleave as well


    element.onmouseleave = function (e) {
      element.classList.remove("active");
    };
  }); // Image upload elements

  document.querySelectorAll(".photo-booth input").forEach(function (element, _) {
    // Let's work with the label too, which is this element's parent
    var _label = element.parentElement; // On changing the input element, set its uploaded file's url as a 
    // background image

    element.onchange = function (e) {
      var _imageUrl = URL.createObjectURL(e.target.files[0]);

      if (_imageUrl) {
        _label.style.backgroundImage = "url(".concat(_imageUrl, ")");

        _label.classList.add("active");
      } else {
        _label.style.backgroundImage = "";

        _label.classList.remove("active");
      }
    }; // On clicking on the input as active, remove the image and the 
    // background


    _label.onclick = function (e) {
      if (_label.classList.contains("active")) {
        e.preventDefault();
        element.value = "";
        _label.style.backgroundImage = "";

        _label.classList.remove("active");
      }
    };
  }); // Video to display poster when it stops

  document.querySelectorAll(".page-section__content__video-hero video, .masthead__content__video-hero video").forEach(function (video, _) {
    var _videoContainer = video.parentElement;

    _videoContainer.onclick = function (e) {
      if (e.target != video) {
        video.click();
      }
    };

    video.addEventListener("ended", function (e) {
      video.load(); // _videoContainer.classList.add("paused");
    }, false);
    video.addEventListener("pause", function (e) {
      _videoContainer.classList.add("paused");
    }, false);
    video.addEventListener("play", function (e) {
      _videoContainer.classList.remove("paused");
    }, false);
  }); // Show hidden address fields for manual address input purposes when
  // hitting manual address option link

  var manualAddressPrompt = document.getElementById("manualAddressPrompt");

  if (manualAddressPrompt) {
    manualAddressPrompt.onclick = function (e) {
      e.preventDefault(); // Change the label

      document.querySelector("label[for=address_line_one] span").innerHTML = "Address 1 *"; // Show the fields

      document.querySelectorAll("label[for=address_line_two], label[for=city], label[for=county], label[for=postcode]").forEach(function (element, _) {
        element.classList.remove("hidden-address-field");
      }); // Hide prompt

      manualAddressPrompt.style.display = "none";
    };
  } // Loqate integration


  if (typeof pca !== "undefined") {
    pca.on("options", function (type, id, options) {
      if (type == "capture+") {
        options.prompt = false;
      }
    });
    pca.on("load", function (type, id, control) {
      var addressLineOneInput = document.getElementById("address_line_one");
      addressLineOneInput.setAttribute("autocomplete", "nope");


      addressLineOneInput.onfocus = function (e) {
        if (type === "capture+") {
          control.prompt = false;
          control.search(addressLineOneInput.getAttribute("placeholder"));
        }
      };

      control.listen("populate", function (address) {
        addressLineOneInput.value = address.Line1;
        document.getElementById("address_line_two").value = address.Line2;
        document.getElementById("city").value = address.City;
        document.getElementById("county").value = address.Province;
        document.getElementById("postcode").value = address.PostalCode;
        manualAddressPrompt.click();
        location.href = "#city";
        var emailAddressInput = document.getElementById("fullname");
        emailAddressInput.focus();
      });

      addressLineOneInput.focus();
    });
  }

})();
},{"body-scroll-lock":"Bc6M"}]},{},["A2T1"], null)
//# sourceMappingURL=/app.ce5cbd10.js.map