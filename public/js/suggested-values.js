var suggestedValues = {
    loadMore: true,
    modelIterator : 0,
    loadMoreAction: function(){
        if($(window).scrollTop() >= $('.main-offer').offset().top + $('.main-offer').outerHeight() - window.innerHeight && this.loadMore == true) {
            this.loadMore = false;
            if(this.modelIterator < this.models.length){
                $.ajax({url: suggestedValues.models[this.modelIterator].url, success: (result) => {
                        $(".main-offer").append(result);
                        this.modelIterator++;
                        this.loadMore = true;
                        if($(window).scrollTop() >= $('.main-offer').offset().top + $('.main-offer').outerHeight() - window.innerHeight) {
                            this.loadMoreAction();
                        }
                    }});
            }
        }
    },
    models: {},

}

$(document).ready(function() {
    var models = $('.offer__title').attr('data-models');
    suggestedValues.models = JSON.parse(models);
    suggestedValues.loadMoreAction();
    $(window).on('scroll', function() {
        suggestedValues.loadMoreAction();
    });
});