$(function() {
    $('.datepicker').datepicker({ dateFormat: 'dd/mm/yy' });
});

$(function() {
    $('.completed-sales-ordering').on('click', function(){
        var $clickedElement = $(this);
        var orderColumn = $clickedElement.attr('data-order-column');
        var orderDirection = $clickedElement.attr('data-order-direction');

        $form = $('#completed-sales-filtering');

        $('[name=orderColumn]', $form).val(orderColumn);
        $('[name=orderDirection]', $form).val(orderDirection);

        $form.submit();
    });
});

function confirmAction(callback) {
    swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            callback();
        }
    });
}