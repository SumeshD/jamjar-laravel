<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use JamJar\VehicleModel;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class VehicleManufacturer extends Model
{
    use LogsActivity;
    protected $fillable = ['cap_manufacturer_id', 'name', 'vehicle_type'];

    /**
     * A vehicle manufacturer has many models
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function models()
    {
        return $this->hasMany(VehicleModel::class, 'manufacturer_id', 'cap_manufacturer_id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
}
