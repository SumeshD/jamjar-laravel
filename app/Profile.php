<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use JamJar\Primitives\Money;
use Spatie\Activitylog\Traits\LogsActivity;

class Profile extends Model
{
    use LogsActivity;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['name', 'email', 'company_name', 'telephone_number', 'mobile_number', 'vat_number', 'user_id', 'address_line_one', 'address_line_two', 'town', 'city', 'county', 'postcode', 'use_flat_fee', 'flat_fee', 'is_scrap_dealer', 'auto_partner'];

    /**
     * Log these attributes on model change
     *
     * @var array
     */
    protected static $logAttributes = ['name', 'email', 'company_name', 'telephone_number', 'vat_number', 'user_id'];

    /**
     * A profile belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
    return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * A profile (can) have a company
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class, 'user_id', 'user_id');
    }

    /**
     * Get and format the address attribute
     *
     * @return string
     */
    public function getAddressAttribute()
    {
        $address = '';
        $address .= $this->address_line_one;
        if ($this->address_line_two) {
            $address .= ', '.$this->address_line_two;
        }
        if ($this->city) {
            $address .= ', '.$this->city;
        }
        if ($this->county) {
            $address .= ', '.$this->county;
        }
        if ($this->postcode) {
            $address .= ', '.$this->postcode;
        }

        return $address;
    }

    /**
     * Get and format the address attribute
     *
     * @return string
     */
    public function getFormattedAddressAttribute()
    {
        $address = '';
        $address .= $this->address_line_one;
        if ($this->address_line_two) {
            $address .= '<br>' . $this->address_line_two;
        }
        if ($this->city) {
            $address .= '<br>' . $this->city;
        }
        if ($this->county) {
            $address .= '<br>' . $this->county;
        }
        if ($this->postcode) {
            $address .= '<br>' . $this->postcode;
        }

        return $address;
    }

    public function getAddressLineOne(): ?string
    {
        return $this->address_line_one;
    }

    public function getAddressLineTwo(): ?string
    {
        return $this->address_line_two;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function getMobileNumber(): ?string
    {
        return $this->mobile_number;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set Flat Fee Attribute
     *
     * @param float $value
     */
    public function setFlatFeeAttribute($value)
    {
        $value = str_replace('£', '', $value);

        if (!is_null($value) && $value) {
            $amount = Money::fromPounds($value);
            $this->attributes['flat_fee'] = $amount->inPence();
        } else {
            $this->attributes['flat_fee'] = null;
        }
    }

    /**
     * Get the Flat Fee Attribute
     *
     * @param  int $flatFee
     * @return float
     */
    public function getFlatFeeAttribute($flatFee)
    {
        if ($flatFee) {
            $money = Money::fromPence($flatFee);
            return $money->inPoundsAndPence();
        }
    }

    public function getVehicleConditionDefaultPercentageGood(): ?int
    {
        return $this->vehicle_condition_default_percentage_good;
    }

    public function getVehicleConditionDefaultPercentageFair(): ?int
    {
        return $this->vehicle_condition_default_percentage_fair;
    }

    public function getVehicleConditionDefaultPercentagePoor(): ?int
    {
        return $this->vehicle_condition_default_percentage_poor;
    }
}
