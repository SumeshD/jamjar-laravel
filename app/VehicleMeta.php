<?php

namespace JamJar;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use JamJar\VehicleDerivative;
use Spatie\Activitylog\Traits\LogsActivity;

class VehicleMeta extends Model
{
    use LogsActivity;

    public $guarded = [];
    public $table = 'vehicle_meta';

    /**
     * Log these attributes on model change
     *
     * @var array
     */
    protected static $logAttributes = ['manufacturer', 'model', 'derivative', 'color', 'transmission', 'plate_year', 'mileage', 'registration_date', 'fuel_type', 'number_of_owners', 'service_history', 'mot', 'write_off', 'non_runner', 'non_runner_reason', 'write_off_category'];

    /** @return array|string[] */
    public static function getAvailableServicesHistories(): array
    {
        return ['FullFranchise', 'PartFranchise', 'PartHistory', 'MinimalHistory', 'None'];
    }

    /** @return array|string[] */
    public static function getAvailableMots(): array
    {
        return ['SixPlus', 'ThreetoSix', 'OneToThree', 'LessThanOne', 'Expired'];
    }

    /** @return array|string[] */
    public static function getAvailableWriteOffCategories(): array
    {
        return ['none', 'A', 'B', 'C', 'D', 'N', 'S'];
    }

    /**
     * Meta belongs to a Vehicle
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    /**
     * Vehicle Meta has a Colour
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function colour()
    {
        return $this->hasOne(Colour::class, 'id', 'color');
    }

    /**
     * Normalize the non_runner_reason attribute
     *
     * @param  array $value
     * @return JamJar\Defect
     */
    public function getNonRunnerReasonAttribute($value)
    {
        $defects = unserialize($value);
        if (!$defects) {
            return false;
        }

        $defectIds = [];

        foreach ($defects as $defect) {
            $defectIds[] = $defect['id'];
        }

        $defects = Defect::whereIn('id', $defectIds)->get();

        return $defects;
    }

    public function getDefectIdsAttribute($value) 
    {
        $defects = unserialize($this->getOriginal('non_runner_reason'));

        $defectIds = collect();
        foreach ($defects as $defect) {
            $defectIds->push($defect['id']);
        }

        return implode(',', $defectIds->toArray());
    }

    /**
     * Normalize the mileage attribute
     *
     * @return string
     */
    public function getMileageAttribute($mileage)
    {
        return number_format((int)$mileage);
    }

    /**
     * Normalize the service_history attribute (uses translations)
     *
     * @return string
     */
    public function getServiceHistoryAttribute($serviceHistory)
    {
        return __('vehicles.'. $serviceHistory);
    }

    /**
     * Normalize the mot attribute (uses translations)
     *
     * @return string
     */
    public function getMotAttribute($mot)
    {
        return __('vehicles.'. $mot);
    }

    public function getFormattedNameAttribute($formatted_name)
    {
        return strtoupper($this->manufacturer . ' ' . $this->model . ' ' . $this->derivative);
    }

    public function getAgeAttribute()
    {
        return date('Y') - explode(' ', $this->plate_year)[0];
    }

    public function getPlateNumberAttribute()
    {
        return explode(' ', $this->plate_year)[1];
    }

    public function getDerivativeIdAttribute()
    {
        return VehicleDerivative::select('cap_derivative_id')->where('name', $this->derivative)->where('model_id', $this->cap_model_id)->first()->cap_derivative_id;
    }

    public function getRegistrationDateAttribute($registrationDate)
    {
        if ($registrationDate) {
            return Carbon::createFromFormat('Ymd', $registrationDate)->format('d/m/Y');
        } else {
            return $registrationDate;
        }
    }

    public function getMileage(): ?int
    {
        return $this->getOriginal('mileage');
    }

    public function getNumberOfOwners(): ?int
    {
        return $this->number_of_owners;
    }

    public function getServiceHistory(): ?string
    {
        return $this->service_history;
    }

    public function getMot(): ?string
    {
        return $this->mot;
    }

    public function isWriteOff(): bool
    {
        return (bool) $this->write_off;
    }

    public function isNonRunner(): bool
    {
        return (bool) $this->non_runner;
    }
}
