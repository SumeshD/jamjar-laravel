<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Setting extends Model
{
    use LogsActivity;

    public $timestamps = false;
    public $fillable = ['key', 'value'];
    protected static $logAttributes = ['key', 'value'];

    public static function get($key, $default=false)
    {
        $setting = static::where('key', $key)->first();

        if (! $setting) {
            return $default;
        }

        return $setting->value;
    }

    public static function set($key, $value)
    {
        $setting = self::where('key', '=', $key)->first();
        if ($setting) {
            $setting->key = $key;
            $setting->value = $value;
            $setting->update();
        } else {
            $setting = new static;
            $setting->key = $key;
            $setting->value = $value;
            $setting->save();
        }
    }
}
