<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;

class SuggestedValue extends Model
{
    protected $fillable = ['percentage', 'previous_percentage', 'cap_model_id', 'cap_derivative_id', 'vehicle_id'];
}
