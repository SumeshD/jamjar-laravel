<?php

namespace JamJar\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use JamJar\Mail\NotEnoughFunds;
use JamJar\User;
use JamJar\Vehicle;

class SendNotEnoughFundsEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $vehicle;
    protected $fee;
    protected $currentFunds;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Vehicle $vehicle, $fee, $currentFunds)
    {
        $this->user = $user;
        $this->vehicle = $vehicle;
        $this->fee = $fee;
        $this->currentFunds = $currentFunds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       $email = new NotEnoughFunds($this->user, $this->vehicle, $this->fee, $this->currentFunds);

       \Mail::to($this->user->email)->send($email);
    }
}
