<?php

namespace JamJar\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use JamJar\Offer;
use JamJar\OfferDerivative;
use JamJar\VehicleModel;

class CacheAutomatedBids implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $offer;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Offer $offer)
    {
        $this->offer = $offer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $minAge = (int)explode('-', $this->offer->minimum_age)[0];
        $maxAge = (int)explode('-', $this->offer->maximum_age)[0];

        $vehicles = VehicleModel::with(['manufacturer', 'derivatives'])
            ->distinct('cap_model_id')
            ->where('manufacturer_id', $this->offer->manufacturer_id)
            ->where('fuel_type', $this->offer->fuel_type)
            ->whereRaw('( `introduced_date` between '.$minAge.' and '.$maxAge.' or discontinued_date between '.$minAge.' and '.$maxAge.' )')
            ->get()
            ->unique('cap_model_id');


        $selectedModels = $this->offer->model_ids_flipped_array;
        $selectedDerivatives = $this->offer->derivative_ids_flipped_array;

        $derivativeBaseValues = OfferDerivative::whereIn('cap_derivative_id', array_keys($selectedDerivatives))->get()->keyBy('cap_derivative_id')->toArray();


        // CACHE ID - vehicles-slug-manufacturer_id-fuel_type-minAge-maxAge
        $cacheKey = 'vehicles-'.$this->offer->slug.'-'.$this->offer->manufacturer_id.'-'.$this->offer->fuel_type.'-'.$minAge.'-'.$maxAge;

        $data = [
            'offer' => $this->offer,
            'vehicles' => $vehicles,
            'derivativeBaseValues' => $derivativeBaseValues,
            'selectedDerivatives' => $selectedDerivatives,
            'selectedModels' => $selectedModels
        ];

        Cache::forever($cacheKey,$data);

    }
}
