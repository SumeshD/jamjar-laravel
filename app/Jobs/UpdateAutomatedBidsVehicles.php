<?php

namespace JamJar\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use JamJar\OfferDerivative;
use JamJar\OfferModel;

class UpdateAutomatedBidsVehicles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $offer;
    protected $request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($offer, $request)
    {
        $this->offer = $offer;
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Collect the currently selected models and derivatives
        $selectedModels = $this->offer->model_ids_flipped_array;
        $selectedDerivatives = $this->offer->derivative_ids_flipped_array;

        // build some collections of models and derivatives to delete, we'll come to these later.
        $modelsToDelete = collect();
        $derivativesToDelete = collect();


        // loop through submitted models
        foreach ($this->request['models'] as $key => $model) {

            if (!isset($model['model_id'])) {
                // push previously selected models which are now unselected to our deletion queue
                if (isset($selectedModels[$key])) {
                    $modelsToDelete->push($key);
                }
                // skip as they are unselected
                continue;
            }

            var_dump($model['model_id']);
            // save the model submitted
            $this->offer->models()->updateOrCreate(
                [
                    'cap_model_id' => $model['model_id']
                ],
                [
                    'cap_model_id' => $model['model_id'],
                    'base_value' => $this->offer->base_value
                ]
            );


        }

        die();

        foreach($this->request['models'] as $model) {

            $found = false;
            foreach($selectedModels as $key=>$model2) {

                if($model['model_id']==$key) {

                    $found = true;
                }
            }

            if($found) {

                unset($selectedModels[$model['model_id']]);
            }
        }

        $modelsToDelete = array_keys($selectedModels);



        // loop through submitted derivatives
        foreach ($this->request['derivatives'] as $key => $derivative) {
            // if they don't have a derivative ID
            if (!isset($derivative['derivative_id'])) {
                // push previously selected derivatives which are now unselected to our deletion queue
                if (isset($selectedDerivatives[$key])) {
                    $derivativesToDelete->push($key);
                }
                // skip as they are unselected
                continue;
            }

            $parts = explode('-', $derivative['derivative_id']);
            $modelId = (int)$parts[0];
            $derivativeId = (int)$parts[1];
            $base_value = (int)$derivative['base_value'];

            cache()->forget('derivatives-for-model-'.$modelId.'-and-user-'.auth()->id().'-and-offer-'.$this->offer->id);


            $this->offer->derivatives()->updateOrCreate(
                [
                    'cap_model_id' => $modelId,
                    'cap_derivative_id' => $derivativeId
                ],
                [
                    'cap_model_id' => $modelId,
                    'cap_derivative_id' => $derivativeId,
                    'base_value' => $base_value
                ]
            );
        }


        // Delete models which have been unselected
        OfferModel::whereIn('cap_model_id', $modelsToDelete)->where('offer_id', $this->offer->id)->delete();
        // Delete derivatives which have been unselected
        OfferDerivative::whereIn('cap_derivative_id', $derivativesToDelete->toArray())->where('offer_id', $this->offer->id)->delete();
        // send the user on their way

        //CacheAutomatedBids::dispatch($offer);
    }
}
