<?php

namespace JamJar\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use JamJar\Mail\AutomatedBiddingRuleMatched;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;

class SendAutomaticBiddingRuleMatchedEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $valuation;
    protected $vehicle;
    protected $seller;
    protected $position;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Valuation $valuation, Vehicle $vehicle, $position = 1)
    {
        $this->user = $user;
        $this->valuation = $valuation;
        $this->vehicle = $vehicle;
        $this->position = $position;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new AutomatedBiddingRuleMatched($this->valuation, $this->vehicle, $this->position);

        \Mail::to($this->user->email)->send($email);
    }
}
