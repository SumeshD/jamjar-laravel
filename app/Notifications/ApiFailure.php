<?php

namespace JamJar\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ApiFailure extends Notification
{
    use Queueable;

    protected $api;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($api)
    {
        $this->api = $api;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->priority(1)
                    ->greeting(' ')
                    ->subject('API Failure on jamjar.com - '. $this->api)
                    ->line('An API on jamjar.com has failed:')
                    ->line($this->api);
    }
}
