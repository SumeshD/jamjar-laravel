<?php

namespace JamJar\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class QueueServerFailed extends Notification
{
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->priority(1)
                    ->greeting(' ')
                    ->subject('Failure on jamjar.com queue server')
                    ->line('There is a problem with queue script, please check the queue server. Until that moment queue server will be triggered once per 15 minutes.');
    }
}
