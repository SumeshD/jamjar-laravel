<?php

namespace JamJar;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JamJar\Api\CapApi;
use JamJar\Colour;
use JamJar\Exceptions\VehicleExpiredException;
use JamJar\Http\Controllers\ValuationsController;
use JamJar\Http\Controllers\VehicleController;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlert;
use JamJar\Model\ValuationDraft;
use JamJar\Model\VehicleImage;
use JamJar\Services\VehicleFilter\VehicleFilters;
use JamJar\ValueObjects\GoodNewsLead;
use JamJar\ValueObjects\TopVehicleOffer;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleMeta;
use JamJar\VehicleModel;
use Spatie\Activitylog\Traits\LogsActivity;
use \GuzzleHttp\Client as HttpClient;

/** @mixin \Eloquent */
class Vehicle extends Model
{
    use LogsActivity;

    const LVC_TYPE_FULL_NAME = 'Light Commercial Vehicle';
    const CAR_TYPE_FULL_NAME = 'Car';
    const LVT_TYPE_CAP = 'LCV';
    const CAR_TYPE_CAP= 'CAR';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['cap_id','capcode','numberplate','type', 'cap_value_in_pence'];

    /**
     * Log these attributes on model change
     *
     * @var array
     */
    protected static $logAttributes = ['cap_id','capcode','numberplate','type'];

    protected $dates = ['start_sending_reminders_at', 'added_to_marketplace_at'];

    public function getId(): int
    {
        return $this->id;
    }

    public static function getActiveSaleForVehicle(Vehicle $vehicle)
    {
        return Sale::where([['vehicle_id', '=', $vehicle->id], ['status', '<>', 'rejected']])->first();
    }

    public static function getLeadsForMatrix(User $matrix, VehicleFilters $filters)
    {
        list($count, $vehicles) = self::getRawLeadsVehiclesForMatrix($matrix, $filters);

        return self::getLeadsByRawVehicles($vehicles);
    }

    public static function getFirstVehicleOfUserByNumberplate(User $user, string $vrm): ?Vehicle
    {
        return Vehicle::select('vehicles.*')
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id', 'right')
            ->where([
                ['vehicle_owners.user_id', '=', $user->id],
                ['vehicles.numberplate', '=', $vrm],
            ])
            ->orderBy('vehicles.created_at', 'DESC')
            ->first();
    }

    public static function getFinishedVehicle(User $user, string $numberplate): ?Vehicle
    {
        return self::getVehicleWithFinishedFlag(true, $user, $numberplate);
    }

    public static function getUnfinishedVehicle(User $user, string $numberplate): ?Vehicle
    {
        return self::getVehicleWithFinishedFlag(false, $user, $numberplate);
    }

    protected static function getVehicleWithFinishedFlag(bool $isFinished, User $user, string $numberplate): ?Vehicle
    {
        return Vehicle::select('vehicles.*')
            ->where([
                ['vehicle_owners.user_id', '=', $user->id],
                ['vehicles.numberplate', '=', $numberplate],
                ['vehicles.is_finished', '=', $isFinished],
            ])
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->orderBy('vehicles.created_at', 'DESC')
            ->first();
    }

    public static function getOffersReceivedForUserQuery(User $user, string $offersType = 'open'): Builder
    {
        $whereColumns = [
            ['vehicle_owners.user_id', '=', $user->id],
            ['vehicles.is_finished', '=', true],
            ['vehicles.is_visible_on_marketplace', '=', true],
            ['vehicles.added_to_marketplace_at', '>=', Vehicle::getGoodNewsLeadVehicleDisappearDate()],
            ['sales.id', '=', null],
        ];

        /** @var Vehicle[] $vehicles */
        $vehicles = Vehicle::select('vehicles.*')
            ->where($whereColumns)
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->rightJoin('valuations', 'valuations.vehicle_id', '=', 'vehicles.id')
            ->leftJoin('sales', 'sales.valuation_id', '=', 'valuations.id')
            ->get();

        $vehiclesIdsToDisplay = [];

        foreach ($vehicles as $vehicle) {
            if ($offersType == 'open' && !$vehicle->allValuationsAreExpired()) {
                $vehiclesIdsToDisplay[] = $vehicle->getId();
            }

            if ($offersType == 'expired' && $vehicle->allValuationsAreExpired()) {
                $vehiclesIdsToDisplay[] = $vehicle->getId();
            }
        }

        return Vehicle::whereIn('id', $vehiclesIdsToDisplay)
            ->orderBy('vehicles.created_at', 'DESC');
    }

    public static function getListedVehiclesForUserQuery(User $user): Builder
    {
        $whereColumns = [
            ['vehicle_owners.user_id', '=', auth()->user()->id],
            ['vehicles.is_finished', '=', true],
            ['vehicles.is_visible_on_marketplace', '=', true],
            ['vehicles.added_to_marketplace_at', '>=', Vehicle::getGoodNewsLeadVehicleDisappearDate()],
        ];

        return Vehicle::select('vehicles.*')
            ->where($whereColumns)
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->orderBy('vehicles.created_at', 'DESC');
    }

    /**
     * Boot the Vehicle Model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->start_sending_reminders_at = Carbon::now();
        });

        // Cascade delete
        static::deleting(function($vehicle) {
            $vehicle->meta->delete();
            $vehicle->owner->delete();
        });
    }

    public function createVehicleFromApiData(string $numberplate): ?Vehicle
    {
        $numberplate = preg_replace('/\s+/', '', trim($numberplate));
        $data = $this->fetchFromApi($numberplate);
        $vehicle = $this->saveFromApi($data);

        return $vehicle ? $vehicle : null;
    }
    
    /**
     * Get Vehicle Information
     *
     * @param  string $vrm The Vehicle Numberplate
     * @return \JamJar\Vehicle
     */
    public function getVehicle($vrm)
    {
        $vrm = preg_replace('/\s+/', '', trim($vrm));

        $data = $this->fetchFromApi($vrm);

        if ($data instanceof \SimpleXMLElement) {
            $this->saveFromApi($data);
        }

        $vehicle = $this->where('numberplate', '=', $vrm)->first();
        return $vehicle;
    }

    /**
     * Save Vehicle Information from API
     *
     * @param  \SimpleXMLElement $data
     * @return \JamJar\Vehicle
     */
    private function saveFromApi($data)
    {
        if ($data->MATCHLEVEL->CAP == 0) {
            return false;
        }

        list($manufacturer, $model, $derivative) = $this->getVehicleCapData($data, $this->cleanApiResponse($data->DATA->CAP->VEHICLETYPE));

        $vehicle = new static;
        $vehicle->cap_id = $this->cleanApiResponse($data->DATA->CAP->CAPID);
        $vehicle->capcode = $data->DATA->CAP->CAPCODE;
        $vehicle->numberplate = $data->DATA->DVLA->VRM ?? strtoupper(request()->vrm);
        $vehicle->type = $this->cleanApiResponse($data->DATA->CAP->VEHICLETYPE);

        $vehicle->manufacturer_id = $manufacturer ? $manufacturer->getId() : null;
        $vehicle->model_id = $model ? $model->getId() : null;
        $vehicle->derivative_id = $derivative ? $derivative->getId() : null;

        $vehicle->save();

        $this->saveMetaFromApi($vehicle, $data);

        return $vehicle;
    }

    private function getVehicleCapData($data, $vehicleType): array
    {
        $manufacturerCapId = $this->cleanApiResponse($data->DATA->CAP->MANUFACTURER_CODE);
        $modelCapId = $this->cleanApiResponse($data->DATA->CAP->MODEL_CODE);
        $derivativeName = $this->cleanApiResponse($data->DATA->CAP->DERIVATIVE);

        $manufacturer = null;
        $model = null;
        $derivative = null;

        $manufacturer = VehicleManufacturer::where([
            ['cap_manufacturer_id', '=', $manufacturerCapId],
            ['vehicle_type', '=', strtoupper($vehicleType) == 'CAR' ? 'CAR' : 'LCV'],
        ])->first();

        if ($manufacturer) {
            $model = VehicleModel::where([
                ['cap_model_id', '=', $modelCapId],
                ['internal_manufacturer_id', '=', $manufacturer->getId()],
            ])->first();

            if ($model) {
                $derivative = VehicleDerivative::where([
                    ['name', '=', $derivativeName],
                    ['internal_model_id', '=', $model->getId()],
                ])->first();
            }
        }

        return [$manufacturer, $model, $derivative];
    }

    /**
     * Save Vehicle Meta Information from API
     *
     * @param  \JamJar\Vehicle   $vehicle
     * @param  \SimpleXMLElement $data
     * @return \JamJar\Vehicle
     */
    private function saveMetaFromApi(Vehicle $vehicle, $data)
    {
        $colour = Colour::where('title', '=', $this->cleanApiResponse($data->DATA->DVLA->COLOUR))->first();

        $vinNumber = $data->MATCHLEVEL->DVLA > 0 ? $data->DATA->DVLA->VIN : null;
        $previousOwners = $data->MATCHLEVEL->DVLAKEEPER > 0 ? $data->DATA->DVLA->PREVIOUSKEEPERS : null;

        $doors = (int)$this->cleanApiResponse($data->DATA->CAP->DOORS) ?? 0;

        $bodyType = $data->MATCHLEVEL->DVLA > 0 ? $data->DATA->DVLA->BODYTYPE : null;
        $engineCapacity = $data->MATCHLEVEL->DVLA > 0 ? $data->DATA->DVLA->ENGINECAPACITY : null;

        $vehicleType = $data->CAP->VEHICLETYPE;

        $meta = new VehicleMeta(
            [
            'manufacturer' => $this->cleanApiResponse($data->DATA->CAP->MANUFACTURER),
            'model' => $this->cleanApiResponse($data->DATA->CAP->MODEL),
            'derivative' => $this->cleanApiResponse($data->DATA->CAP->DERIVATIVE),
            'color' => empty($colour->id) ? 0 : $colour->id ,
            'transmission' => $this->cleanApiResponse($data->DATA->CAP->TRANSMISSION),
            'plate_year' => $this->cleanApiResponse($data->DATA->CAP->PLATE),
            'fuel_type' => $this->cleanApiResponse($data->DATA->CAP->FUELTYPE),
            'registration_date' => $this->cleanApiResponse($data->DATA->DVLA->REGISTRATIONDATE),
            'vin_number' => $vinNumber,
            'number_of_owners' => $previousOwners + 1,
            'body_type' => $this->cleanApiResponse($bodyType),
            'doors' => $doors,
            'engine_capacity' => $this->cleanApiResponse($engineCapacity),
            'cap_model_id' => $this->cleanApiResponse($data->DATA->CAP->MODEL_CODE)
            ]
        );

        $vehicle->meta()->save($meta);

        // dev only
        // $this->saveModelsFromApi($vehicle);

        return $vehicle;
    }

    /**
     * Get Vehicle Information from CAP API.
     *
     * @param  string $vrm
     * @return \SimpleXMLElement
     */
    private function fetchFromApi($vrm)
    {
        $capApi = new CapApi;
        return $capApi->getVehicle($vrm);
    }

    /**
     * Clean the response from the API into a nicer format
     *
     * @param  mixed $data
     * @return mixed
     */
    private function cleanApiResponse($data)
    {
        if (!is_numeric($data)) {
            return trim(ucwords(strtolower($data)));
        }
    }

    public function isVisibleOnMarketplace(): bool
    {
        return (bool) $this->is_visible_on_marketplace;
    }

    public function setConditionInformation(?string $conditionInformation): self
    {
        $this->condition_information = $conditionInformation;

        return $this;
    }

    public function setVehicleManufacturer(VehicleManufacturer $manufacturer)
    {
        $this->manufacturer_id = $manufacturer->getId();
    }

    public function setVehicleModel(VehicleModel $model)
    {
        $this->model_id = $model->getId();
    }

    public function setVehicleDerivative(VehicleDerivative $derivative)
    {
        $this->derivative_id = $derivative->getId();
    }

    public function getHighestNonExpiredValuation(): ?Valuation
    {
        $highestValuationStatement = DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC, valuations.created_at', 'DESC');

        $valuations = Valuation::where('valuations.vehicle_id', '=', $this->id)
            ->orderBy($highestValuationStatement)
            ->get();

        /** @var Valuation $highestNonExpiredValuation */
        $highestNonExpiredValuation = null;

        foreach ($valuations as $valuation) {
            $expirationDate = $valuation->created_at->addDays((int) $valuation->company->valuation_validity);
            if ($expirationDate < Carbon::now()) {
                continue;
            }

            if (!$highestNonExpiredValuation) {
                $highestNonExpiredValuation = $valuation;
            } else {
                if ($highestNonExpiredValuation->getPriceInPence() < $valuation->getPriceInPence()) {
                    $highestNonExpiredValuation = $valuation;
                }
            }
        }

        return $highestNonExpiredValuation;
    }

    /**
     * Fetch and Save Models from API
     *
     * @param  int $id manufacturer id
     * @return array
     */
    protected function saveModelsFromApi($vehicle)
    {
        $manufacturer = VehicleManufacturer::select('cap_manufacturer_id')
                                                ->where('id', $vehicle->manufacturer_id)
                                                ->first();
        $api = new CapApi;
        $models = $api->getModels($manufacturer->cap_manufacturer_id, $vehicle->type);

        foreach ($models as $model) {
            $fuelType = str_contains($model->get('name'), 'DIESEL') ? 'Diesel' : 'Petrol';

            $introduced = $model->get('introduced');
            $discontinued = $model->get('discontinued') == 0 ? date('Y') : $model->get('discontinued');
            $plateYears = range($introduced, $discontinued);

            $_model = VehicleModel::updateOrCreate(
                [
                'cap_model_id' => $model->get('cap_model_id'),
                ],
                [
                'cap_model_id' => $model->get('cap_model_id'),
                'name' => $model->get('name'),
                'manufacturer_id' => (int) $manufacturer->cap_manufacturer_id,
                'introduced_date' => $model->get('introduced'),
                'discontinued_date' => $model->get('discontinued'),
                'fuel_type' => $fuelType,
                'plate_years' => implode(',', $plateYears)
                ]
            );
            $this->saveDerivsFromApi($_model, $vehicle);
        }

        return $models;
    }

    /**
     * Fetch Derivatives from API
     *
     * @param  int $id
     * @return array
     */
    protected function saveDerivsFromApi($model, $vehicle)
    {
        $api = new CapApi;
        $derivatives = $api->getDerivatives($model->cap_model_id, $vehicle->type);
        $collection = [];

        foreach ($derivatives as $derivative) {
            $deriv = VehicleDerivative::updateOrCreate(
                [
                'cap_derivative_id' => $derivative->get('cap_derivative_id'),
                ],
                [
                'cap_derivative_id' => $derivative->get('cap_derivative_id'),
                'name' => $derivative->get('name'),
                'model_id' => $model->cap_model_id,
                ]
            );
            $collection = collect()->merge($deriv->load('model'));
        }

        return $collection;
    }

    /**
     * A vehicle has meta information
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function meta()
    {
        return $this->hasOne(VehicleMeta::class);
    }

    /**
     * A vehicle has owner information
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function owner()
    {
        return $this->hasOne(VehicleOwner::class);
    }

    /**
     * A vehicle has owner information
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getLatestOwnerAttribute()
    {
        $owner = VehicleOwner::where('vehicle_id', $this->id)->latest()->first();
        return $owner;
    }

    /**
     * A vehicle has owner information
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function valuations()
    {
        return $this->hasMany(Valuation::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasOne */
    public function images()
    {
        return $this->hasMany(VehicleImage::class);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'numberplate';
    }

    /** @return array|VehicleImage[] */
    public function getImages()
    {
        return $this->images;
    }

    public static function addSearchSelectors($vehicles, User $buyer, Carbon $dateLimit, $addGroupBy = true)
    {
        $vehicles = $vehicles
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->join('users', 'vehicle_owners.user_id', '=', 'users.id')
            ->join('vehicle_meta', 'vehicle_meta.vehicle_id', '=', 'vehicles.id')
            ->leftJoin('valuations', 'valuations.vehicle_id', '=', 'vehicles.id')
            ->where([
                ['vehicles.added_to_marketplace_at', '>', $dateLimit],
                ['vehicles.is_visible_on_marketplace', '=', true],
                ['vehicles.manufacturer_id', '<>', null],
                ['vehicle_owners.user_id', '<>', $buyer->id],
                [DB::raw(sprintf('(select sales.id from sales where sales.vehicle_id = vehicles.id and sales.status IN ("pending", "complete") limit 1)', $buyer->id)), '=', null],
                [DB::raw(sprintf('(select valuations.id from valuations where valuations.vehicle_id = vehicles.id and valuations.company_id = %s limit 1)', $buyer->company->id)), '=', null]
            ]);

        if ($addGroupBy) {
            $vehicles = $vehicles->groupBy('vehicles.id');
        }

        return $vehicles;
    }

    public static function getRawLeadsVehiclesForMatrix(User $matrix, VehicleFilters $filters, int $perPage = 15, int $pageNum = 1): array
    {
        $rawVehiclesQuery = self::getVehiclesQueryForMatrixByFilters($matrix, $filters);

        $resultsCount = count($rawVehiclesQuery->get());

        $rawVehicles = $rawVehiclesQuery
            ->limit($perPage)
            ->offset(($pageNum-1) * $perPage)
            ->orderBy('vehicles.added_to_marketplace_at', 'DESC')
            ->get();

        return [$resultsCount, $rawVehicles];
    }

    public static function getVehiclesQueryForMatrixByFilters(User $matrix, VehicleFilters $filters)
    {
        $disappearDate = self::getGoodNewsLeadVehicleDisappearDate();

        $rawVehiclesQuery = DB::table('vehicles')
            ->select(
                'vehicles.*',
                DB::raw('GREATEST(COALESCE((select GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) from valuations where vehicle_id = vehicles.id order by GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC limit 1), 0), FLOOR(COALESCE((select price_in_pence from valuation_drafts where vehicle_id = vehicles.id order by price_in_pence DESC limit 1), 0) / 100)) as max_value')
            );

        $rawVehiclesQuery = self::addSearchSelectors($rawVehiclesQuery, $matrix, $disappearDate);

        if (count($filters->vehicleManufacturersFilters) > 0) {
            $whereClausesArray = [];
            foreach ($filters->vehicleManufacturersFilters as $vehicleManufacturerFilter) {
                if (count($vehicleManufacturerFilter->modelsIds) == 0) {
                    $whereClausesArray[] = '(vehicles.manufacturer_id in ('. implode(',', $vehicleManufacturerFilter->manufacturersIds) .'))';
                } else {
                    $whereClausesArray[] = '(vehicles.manufacturer_id in ('. implode(',', $vehicleManufacturerFilter->manufacturersIds) .') and vehicles.model_id in (' . implode(',', $vehicleManufacturerFilter->modelsIds) . '))';
                }
            }

            $rawVehiclesQuery = $rawVehiclesQuery->whereRaw(' ( ' . implode(' OR ', $whereClausesArray) . ' ) ');
        }

        if ($filters->numberOfOwners) {
            if($filters->numberOfOwners==6) {

                $rawVehiclesQuery->where('vehicle_meta.number_of_owners', '>=', $filters->numberOfOwners);
            }else {

                $rawVehiclesQuery->where('vehicle_meta.number_of_owners', '<=', $filters->numberOfOwners);
            }
        }

        if ($filters->minMileage) {
            $rawVehiclesQuery->where('vehicle_meta.mileage', '>=', $filters->minMileage);
        }

        if ($filters->maxMileage) {
            $rawVehiclesQuery->where('vehicle_meta.mileage', '<=', $filters->maxMileage);
        }

        if ($filters->minValue) {
            $rawVehiclesQuery->having('max_value', '>=', $filters->minValue);
        }

        if ($filters->maxValue) {
            $rawVehiclesQuery->having('max_value', '<=', $filters->maxValue);
        }

        if ($filters->fuelType) {
            $rawVehiclesQuery->where('vehicle_meta.fuel_type', '=', $filters->fuelType);
        }

        if ($filters->minPlateYear) {
            $rawVehiclesQuery->where('vehicle_meta.plate_year', '>=', $filters->minPlateYear);
        }

        if ($filters->addedToMarketplaceDateFrom) {
            $rawVehiclesQuery->where('vehicles.added_to_marketplace_at', '>=', $filters->addedToMarketplaceDateFrom->format('Y-m-d 00:00:00'));
        }

        if ($filters->addedToMarketplaceDateTo) {
            $rawVehiclesQuery->where('vehicles.added_to_marketplace_at', '<=', $filters->addedToMarketplaceDateTo->format('Y-m-d 23:59:59'));
        }

        if ($filters->carCondition) {
            if ($filters->carCondition == 'non-runners') {
                $rawVehiclesQuery->whereRaw('(vehicle_meta.write_off = true or vehicle_meta.non_runner = true)');
            } elseif ($filters->carCondition == 'runners') {
                $rawVehiclesQuery->whereRaw('(vehicle_meta.write_off = false and vehicle_meta.non_runner = false)');
            }
        }

        if ($filters->sellerType) {
            if ($filters->sellerType == 'customers-only') {
                $rawVehiclesQuery->where('users.matrix_partner', '=', false);
            } elseif ($filters->sellerType == 'partners-only') {
                $rawVehiclesQuery->where('users.matrix_partner', '=', true);
            }
        }

        if ($filters->vehicleType) {
            if ($filters->vehicleType == MarketplaceAlert::VEHICLE_TYPE_CAR) {
                $rawVehiclesQuery->where('vehicles.type', '=', 'Car');
            } elseif ($filters->vehicleType == MarketplaceAlert::VEHICLE_TYPE_VAN) {
                $rawVehiclesQuery->where('vehicles.type', '=', 'Light Commercial Vehicle');
            }
        }

        if ($filters->vehicleLocation) {
            if ($filters->vehicleLocation == 'postcode') {

                $postcode_areas = $matrix->company->postcode_area;
                if($postcode_areas) {

                    $postcode_areas = explode(',', $postcode_areas);
                    $rawVehiclesQuery->whereIn(DB::raw('SUBSTRING(vehicle_owners.postcode,1,2)'), $postcode_areas);
                }


            }
        }

        return $rawVehiclesQuery;
    }

    /**
     * @param $rawVehicles
     * @return array|GoodNewsLead[]
     */
    public static function getLeadsByRawVehicles($rawVehicles)
    {
        $leads = [];

        foreach ($rawVehicles as $rawVehicle) {
            $leads[] = self::getGoodNewsLeadByVehicle(Vehicle::where('id', '=', $rawVehicle->id)->first());
        }

        return $leads;
    }

    /**
     * @param Vehicle $vehicle
     * @return GoodNewsLead
     */
    public static function getGoodNewsLeadByVehicle(Vehicle $vehicle): GoodNewsLead
    {
        $disappearDate = self::getGoodNewsLeadVehicleDisappearDate();

        if ($vehicle->getAddedToMarketplaceAt() < $disappearDate) {
            return new GoodNewsLead($vehicle);
        }

        $highestValuationStatement = DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC, valuations.created_at', 'DESC');

        $highestValuation = Valuation::where('valuations.vehicle_id', '=', $vehicle->id)
            ->orderBy($highestValuationStatement)
            ->first();

        $vehicleValuations = Valuation::where([['valuations.vehicle_id', '=', $vehicle->id],])
            ->orderBy($highestValuationStatement)
            ->get();

        $highestExpiredValuation = null;
        $highestNonExpiredValuation = null;

        foreach ($vehicleValuations as $valuation) {
            if (!$valuation->company) {
                continue;
            }
            $expirationDate = $valuation->created_at->addDays((int) $valuation->company->valuation_validity);

            if (!$highestExpiredValuation && $expirationDate < Carbon::now()) {
                $highestExpiredValuation = $valuation;
            }

            if (!$highestNonExpiredValuation && $expirationDate >= Carbon::now()) {
                $highestNonExpiredValuation = $valuation;
            }
        }

        return new GoodNewsLead($vehicle, $highestValuation, $highestExpiredValuation, $highestNonExpiredValuation);
    }

    public static function getGoodNewsLeadVehicleDisappearDate(): Carbon
    {
        return Carbon::now()->subMonth(1);
    }

    public function isOfferExpired(): bool
    {
        if (!$this->added_to_marketplace_at) {
            return false;
        }

        return Carbon::now() > $this->added_to_marketplace_at->addMonth(1);
    }

    /**
     * @param int $clientId
     *
     * @return array|TopVehicleOffer[]
     */
    public static function getTopVehicleOffersForClient(int $clientId) : array
    {
        $rawVehicles = DB::table('vehicles')
            ->select('vehicles.*')
            ->addSelect(DB::raw('(select valuations.id from valuations where vehicle_id = vehicles.id order by GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC, valuations.created_at DESC limit 1) as the_highest_valuation_id'))
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', 'vehicles.id')
            ->where('vehicle_owners.user_id', '=', (int) $clientId)
            ->orderBy('created_at', 'DESC')
            ->get();
        ;

        $offers = [];

        foreach ($rawVehicles as $rawVehicle) {
            $vehicle = Vehicle::where('id', $rawVehicle->id)->first();
            $highestValuation = Valuation::with(['sale', 'vehicle'])
                ->where('valuations.id', '=', $rawVehicle->the_highest_valuation_id)
                ->first();

            $offers[] = new TopVehicleOffer($vehicle, $highestValuation);
        }

        return $offers;
    }

    public function getNumberPlate() : string
    {
        return $this->numberplate;
    }

    public static function cloneVehicle(Vehicle $vehicle)
    {
        // Clone the vehicle
        $clone = $vehicle->replicate();
        $clone->push();

        // If we dont have meta
        if (!$vehicle->meta) {
            // Find all instances of this vehicle with this VRM
            // And delete
            $vehicles = Vehicle::where('numberplate', $vehicle->numberplate)->delete();
            // return a boolean so we can kick the user back to the enter their car page
            return false;
        }

        // get the original's meta
        $meta = $vehicle->meta->getOriginal();
        // create clone emta
        $cloneMeta = [];
        foreach ($meta as $key => $value) {
            // exclude model specific rows
            if (in_array($key, ['id', 'vehicle_id', 'created_at', 'updated_at'])) {
                continue;
            }
            $cloneMeta[$key] = $value;
        }
        // store the cloned meta on the clone
        $clone->meta()->create($cloneMeta);
        //
        return $clone;
    }

    public function hasCompleteSale(): bool
    {
        return count(Sale::where([['vehicle_id', '=', $this->id], ['status', '=', 'complete']])->get()) > 0;
    }

    public function getCompleteSale(): ?Sale
    {
        return Sale::where([['vehicle_id', '=', $this->id], ['status', '=', 'complete']])->first();
    }

    public function hasSale(): bool
    {
        return count(Sale::where('vehicle_id', '=', $this->id)->get()) > 0;
    }

    public function hasPendingSale(): bool
    {
        return count(Sale::where([['vehicle_id', '=', $this->id], ['status', '=', 'pending']])->get()) > 0;
    }

    public function hasRejectedSale(): bool
    {
        return count(Sale::where([['vehicle_id', '=', $this->id], ['status', '=', 'rejected']])->get()) > 0;
    }

    public function getPendingSale(): ?Sale
    {
        return Sale::where([['vehicle_id', '=', $this->id], ['status', '=', 'pending']])->first();
    }

    public function hasActiveValuations(): bool
    {
        $valuations =  Valuation::where([
            ['valuations.vehicle_id', '=', $this->id],
            ['sales.id', '=', null],
        ])
            ->where('valuations.created_at', '>=', Carbon::now()->subMonth())
            ->leftJoin('sales', 'sales.valuation_id', '=', 'valuations.id')
            ->get();

        return count($valuations) > 0;
    }

    public function hasGoodNewsLeadValuation(): bool
    {
        $valuations = Valuation::where('vehicle_id', '=', $this->id)->get();

        /** @var Carbon|null $minTime */
        $minTime = null;
        /** @var Carbon|null $maxTime */
        $maxTime = null;

        foreach ($valuations as $valuation) {
            if (!$minTime || $minTime > $valuation->created_at) {
                $minTime = $valuation->created_at;
            }

            if (!$maxTime || $maxTime < $valuation->created_at) {
                $maxTime = $valuation->created_at;
            }
        }

        if (!$minTime || !$maxTime) {
            return false;
        }

        // if one of valuations has been added 2 minutes after API valuations is a sign that Matrix user added valuation through Good News Leads
        return $maxTime->diff($minTime)->i > 2;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getFormattedName(): string
    {
        return $this->meta->formatted_name;
    }

    public function getCurrentValueInPence(): int
    {
        $draft = ValuationDraft::where('vehicle_id', '=', $this->id)->orderBy('price_in_pence', 'DESC')->first();

        return $draft ? $draft->price_in_pence : 0;
    }

    public function hasExpiredOffer(): bool
    {
        foreach (ValuationDraft::where('vehicle_id', '=', $this->id)->get() as $draft) {
            /** @var ValuationDraft $draft */
            if ($draft->isExpired()) {
                return true;
            }
        }

        foreach (Valuation::where('vehicle_id', '=', $this->id)->get() as $valuation) {
            /** @var Valuation $valuation */
            if ($valuation->isExpired()) {
                return true;
            }
        }

        return false;
    }

    public function allValuationsAreExpired(): bool
    {
        $valuations = Valuation::where('vehicle_id', '=', $this->id)->get();
        $valuationsCount = count($valuations);
        $expiredValuations = 0;

        foreach ($valuations as $valuation) {
            /** @var Valuation $valuation */
            if ($valuation->isExpired()) {
                $expiredValuations++;
            }
        }

        return $valuationsCount == $expiredValuations;
    }

    public function hasExpiredDraft(): bool
    {
        foreach (ValuationDraft::where('vehicle_id', '=', $this->id)->get() as $draft) {
            /** @var ValuationDraft $draft */
            if ($draft->isExpired()) {
                return true;
            }
        }

        return false;
    }

    public function isNonRunner(): bool
    {
        return $this->meta->non_runner || $this->meta->write_off;
    }

    public function getSaleStatus()
    {
        if ($this->hasPendingSale()) {
            return 'Pending sale';
        }

        if ($this->hasCompleteSale()) {
            return 'Complete sale';
        }

        if ($this->hasRejectedSale()) {
            return 'Rejected sale';
        }

        if (count($this->getDrafts()) > 0) {
            if (count($this->getValuations()) > 0) {
                return 'No sale yet';
            }

            return 'Only drafts';
        }

        if (count($this->getValuations()) > 0) {
            return 'Legacy Vehicle';
        }

        return 'No offers';
    }

    /** @return Collection|ValuationDraft[] */
    public function getDrafts()
    {
        return ValuationDraft::where('vehicle_id', '=', $this->id)->get();
    }

    /** @return Valuation[] */
    public function getValuations()
    {
        return Valuation::where('vehicle_id', '=', $this->id)->get();
    }

    public function setAskingPriceInPence(int $askingPriceInPence): self
    {
        $this->asking_price_in_pence = $askingPriceInPence;

        return $this;
    }

    public function getAskingPriceInPence(): ?int
    {
        return $this->asking_price_in_pence;
    }

    public function setKeysCount(int $keysCount): self
    {
        $this->keys_count = $keysCount;

        return $this;
    }

    public function getKeysCount(): ?int
    {
        return $this->keys_count;
    }

    public function setServiceHistoryInformation(?string $serviceHistoryInformation): self
    {
        $this->service_history_information = $serviceHistoryInformation;

        return $this;
    }

    public function getServiceHistoryInformation(): ?string
    {
        return $this->service_history_information;
    }

    public function setAdditionalSpecificationInformation(?string $additionalSpecificationInformation): self
    {
        $this->additional_specification_information = $additionalSpecificationInformation;

        return $this;
    }

    public function getAdditionalSpecificationInformation(): ?string
    {
        return $this->additional_specification_information;
    }

    public function setIsLogBook(bool $isLogBook): self
    {
        $this->is_log_book = $isLogBook;

        return $this;
    }

    public function getIsLogBook(): ?bool
    {
        return $this->is_log_book;
    }

    public function setIsInStock(bool $isInStock): self
    {
        $this->is_in_stock = $isInStock;

        return $this;
    }

    public function getIsInStock(): ?bool
    {
        return $this->is_in_stock;
    }

    public function setStockAvailabilityAt(?Carbon $stockAvailabilityAt): self
    {
        $this->stock_availability_at = $stockAvailabilityAt;

        return $this;
    }

    public function isAvailable(): bool
    {
        if ($this->is_in_stock) {
            return true;
        }

        if (!$this->getStockAvailabilityAt()) {
            return true;
        }

        return $this->getStockAvailabilityAt()->format('Y-m-d') <= Carbon::now()->format('Y-m-d');
    }

    public function additionalInformationHasBeenProvided(): bool
    {
        return $this->getServiceHistoryInformation() ||
            $this->getAdditionalSpecificationInformation() ||
            $this->getKeysCount();
    }

    public function getStockAvailabilityAt(): ?Carbon
    {
        return $this->stock_availability_at ? Carbon::createFromFormat('Y-m-d', $this->stock_availability_at) : null;
    }

    public function setAddedToMarketplaceAt(Carbon $addedToMarketplaceAt): self
    {
        $this->added_to_marketplace_at = $addedToMarketplaceAt;

        return $this;
    }

    public function setSellerFeeInPence(int $sellerFeeInPence): self
    {
        $this->seller_fee_in_pence = $sellerFeeInPence;

        return $this;
    }

    public function getSellerFeeInPence(): ?int
    {
        return $this->seller_fee_in_pence;
    }

    public function setIsVisibleOnMarketplace(bool $isVisibleOnMarketplace): self
    {
        $this->is_visible_on_marketplace = $isVisibleOnMarketplace;

        return $this;
    }

    public function getAddedToMarketplaceAt(): ?Carbon
    {
        return $this->added_to_marketplace_at ?
            Carbon::createFromFormat('Y-m-d H:i:s', $this->added_to_marketplace_at) :
            null;
    }

    public function getMeta(): ?VehicleMeta
    {
        return $this->meta;
    }

    public function setCapValues(array $capValues): self
    {
        $this->cap_value_retail_in_pence = key_exists('retail', $capValues) ? (int) $capValues['retail'] * 100 : 0;
        $this->cap_value_below_in_pence = key_exists('below', $capValues) ? (int) $capValues['below'] * 100 : 0;
        $this->cap_value_clean_in_pence = key_exists('clean', $capValues) ? (int) $capValues['clean'] * 100 : 0;
        $this->cap_value_in_pence = key_exists('average', $capValues) ? (int) $capValues['average'] * 100 : 0;

        return $this;
    }

    public function getCapValueRetailInPence(): ?int
    {
        return $this->cap_value_retail_in_pence;
    }

    public function getCapValueBelowInPence(): ?int
    {
        return $this->cap_value_below_in_pence;
    }

    public function getCapValueCleanInPence(): ?int
    {
        return $this->cap_value_clean_in_pence;
    }

    public function getCapValueInPence(): ?int
    {
        return $this->cap_value_in_pence;
    }

    /**
     * returns highest value from both regular valuations and valuations drafts in pounds
     *
     * @return int|null
     */
    public function getHighestValue(): ?int
    {
        $value = Vehicle::selectRaw(
            DB::raw('GREATEST(COALESCE((select GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) from valuations where vehicle_id = vehicles.id order by GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC limit 1), 0), FLOOR(COALESCE((select price_in_pence from valuation_drafts where vehicle_id = vehicles.id order by price_in_pence DESC limit 1), 0) / 100)) as max_value')
        )->where('vehicles.id', '=', $this->id)->first();

        if ($value && $value->max_value) {
            return $value->max_value;
        }

        return null;
    }

    public function getHighestValueInPence(): ?int
    {
        $highestValueInPounds = $this->getHighestValue();

        return $highestValueInPounds ? $highestValueInPounds * 100 : null;
    }

    public function getHighestValuationValueInPence(): ?int
    {
        $value = Vehicle::selectRaw(
            DB::raw('(select GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) from valuations where vehicle_id = vehicles.id order by GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC limit 1) as max_value')
        )->where('vehicles.id', '=', $this->id)->first();


        if ($value && $value->max_value) {
            return $value->max_value * 100;
        }

        return null;
    }

    public function isAdditionalInfoAdded(): bool
    {
        return (bool) $this->getAskingPriceInPence() ||
            (bool) $this->getAskingPriceInPence();
    }

    public function getVehicleTypeCapName(): string
    {
        switch ($this->type) {
            case self::CAR_TYPE_FULL_NAME :
                return self::CAR_TYPE_CAP;
            case self::LVC_TYPE_FULL_NAME :
                return self::LVT_TYPE_CAP;
        }
    }
}
