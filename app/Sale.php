<?php

namespace JamJar;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use \Exception;
use Illuminate\Support\Facades\DB;

class Sale extends Model
{
    protected $fillable = ['status', 'dealer_notes', 'vehicle_condition', 'final_price'];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function valuation()
    {
        return $this->belongsTo(Valuation::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public static function getSearchColumnsByReqest(Request $request): array
    {
        $whereSearches = [['sales.status', '=', 'complete']];

        if ($request->dateCompletedFrom) {
            try {
                $whereSearches[] = ['sales.updated_at', '>=', Carbon::createFromFormat('d/m/Y H:i:s', $request->dateCompletedFrom . ' 00:00:00')];
            } catch (Exception $e) {
            }
        }

        if ($request->dateCompletedTo) {
            try {
                $whereSearches[] = ['sales.updated_at', '<=', Carbon::createFromFormat('d/m/Y H:i:s', $request->dateCompletedTo . ' 23:59:59')];
            } catch (Exception $e) {
            }
        }

        $companyType = null;
        if ($request->companyType && in_array($request->companyType, ['api', 'matrix'])) {
            $whereSearches[] = ['companies.company_type', '=', $request->companyType];
        }

        if ($request->companyName) {
            $whereSearches[] = ['companies.name', 'LIKE', '%' . $request->companyName . '%'];
        }

        if ($request->finalPriceRange) {
            $finalPrices = json_decode($request->finalPriceRange);
            $whereSearches[] = ['sales.final_price', '>=', (int) $finalPrices[0]];
            $whereSearches[] = ['sales.final_price', '<=', (int) $finalPrices[1]];
        }

        if ($request->valuationValueRange) {
            $finalValues = json_decode($request->valuationValueRange);
            $whereSearches[] = [DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0))'), '>=', (int) $finalValues[0]];
            $whereSearches[] = [DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0))'), '<=', (int) $finalValues[1]];
        }

        if ($request->numberPlate) {
            $whereSearches[] = [
                DB::raw('LOWER(REPLACE(vehicles.numberplate, \' \', \'\'))'),
                '=',
                str_replace(' ', '', strtolower($request->numberPlate))
            ];
        }

        return $whereSearches;
    }

    public static function getOrderingByRequest(Request $request): array
    {
        $availableOrderColumns = ['updatedAt', 'companyType', 'valuationValue', 'finalPrice', 'buyerFee'];
        $availableOrderDirections = ['asc', 'desc'];

        $orderColumn = 'sales.updated_at';
        $orderDirection = 'desc';

        if ($request->orderColumn && in_array($request->orderColumn, $availableOrderColumns)) {
            switch ($request->orderColumn) {
                case 'companyType';
                    $orderColumn = 'companies.company_type';
                    break;
                case 'valuationValue':
                    $orderColumn = DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0))');
                    break;
                case 'finalPrice':
                    $orderColumn = DB::raw('sales.final_price+0'); // zero at the end because we cast string to int in sorting
                    break;
                case 'buyerFee';
                    $orderColumn = 'valuations.fee';
                    break;
                case 'updatedAt':
                default:
                    $orderColumn = 'sales.updated_at';
                    break;
            }
        }

        if ($request->orderDirection && in_array(strtolower($request->orderDirection), $availableOrderDirections)) {
            $orderDirection = $request->orderDirection;
        }

        return [$orderColumn, $orderDirection];
    }

    /** @return array|string[] */
    public static function getAvailablePartnerNames(): array
    {
        $availablePartnersNames = [];

        foreach (Sale::where('status', '=', 'complete')->get() as $sale) {
            if (isset($availablePartnersNames[$sale->company->name])) {
                continue;
            }

            $availablePartnersNames[$sale->company->name] = $sale->company->name;
        }

        return $availablePartnersNames;
    }

    public function createSale(Vehicle $vehicle, Valuation $valuation, Company $company, $args = [])
    {
        $sale = new static;
        $sale->vehicle_id = $vehicle->id;
        $sale->valuation_id = $valuation->id;
        $sale->user_id = auth()->check() ? auth()->user()->id : $vehicle->owner->id;
        $sale->company_id = $company->id;
        $sale->status = 'pending';
        $sale->vehicle_condition = isset($args['condition']) ? $args['condition'] : 'great';
        $sale->delivery_method = isset($args['deliveryMethod']) ? $args['deliveryMethod'] : null;
        $sale->price = isset($args['value']) ? $args['value'] : null;
        $sale->dealer_notes = isset($args['condition-information']) ? $args['condition-information'] : null;
        $sale->save();

        return $sale;
    }

    public function getVehicle(): Vehicle
    {
        return $this->vehicle;
    }

    public function getPriceInPence(): int
    {
        return $this->price * 100;
    }
}
