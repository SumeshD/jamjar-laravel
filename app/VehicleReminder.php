<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;

/** @mixin \Eloquent */
class VehicleReminder extends Model
{
    protected $fillable = ['type', 'start_date_of_sending_reminders', 'vehicle_id'];

    protected $dates = ['start_date_of_sending_reminders'];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }
}
