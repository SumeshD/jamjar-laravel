<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PartnerPage extends Model
{
    use LogsActivity;

    protected $fillable = ['website_id', 'title', 'slug', 'content', 'meta', 'active'];

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
