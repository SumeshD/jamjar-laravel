<?php

namespace JamJar\Http\Controllers\Auth;

use Newsletter;
use JamJar\User;
use JamJar\Profile;
use Illuminate\Http\Request;
use JamJar\Http\Requests\StoreUser;
use Illuminate\Support\Facades\Auth;
use JamJar\Jobs\SendVerificationEmail;
use Illuminate\Auth\Events\Registered;
use JamJar\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/valuations/saved';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('verify');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \JamJar\User
     */
    protected function create(array $data)
    {
        $user = new User;
        $user = $user->registerUser($data['name'], $data['email'], $data['password']);
        $this->handleMarketingPreferences($data, $user);

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  JamJar\Http\Requests\StoreUser $request
     * @return \Illuminate\Http\Response
     */
    public function register(StoreUser $request)
    {
        event(new Registered($user = $this->create($request->all())));

        alert()
            ->info("Thanks for registering with JamJar. We've just sent you an email with a link which you need to click in order to activate your account.")
            ->persistent();

        return redirect(route('/'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  $token
     * @return \Illuminate\Http\Response
     */
    public function verify($token)
    {
        $user = User::where('email_token', $token)->first();
        if ($user) {
            $user->verified = 1;
            $user->email_token = null;

            if ($user->save()) {
                Auth::login($user, true);
                alert()
                    ->success("Thanks for confirming your email address. You are now logged in.")
                    ->persistent();

                return redirect(route('/'));
            }
        } 
        else {
            return redirect(route('/'));
        }
    }


    /**
     * Handle the given marketing preferences (or lack thereof)
     *
     * @param  array $request
     * @return void
     */
    protected function handleMarketingPreferences($request, $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }
        
        if (!isset($request['marketing']) || $request['marketing'] == false) {
            return false;
        }

        // These are our marketing options
        $marketingOptions = [
            'email' => true,
            'telephone' => true,
            'sms' => true
        ];
        
        // Loop through again to update our user
        foreach ($marketingOptions as $key => $value) {
            // because we default to a falsy value, any checkboxes
            // not checked will convert to a 0, thus disabled.
            $user->update(
                [
                "marketing_{$key}" => (int)$value
                ]
            );
        }

        $name = $this->formatName($request['name']);

        $mergeFields = [
            'FNAME' => $name['firstname'],
            'LNAME' => $name['lastname'],
            'MMERGE4' => $request['email'],
        ];

        Newsletter::subscribe($request['email'], $mergeFields, 'customer_subscribers');
    }

    /**
     * Format the given name.
     *
     * @param  string $name
     * @return array
     */
    private function formatName($name)
    {
        $fullname = explode(' ', $name);

        if (array_key_exists(1, $fullname)) {
            $firstname = $fullname[0];
            $lastname = $fullname[1];
        } else {
            $firstname = $name;
            $lastname = ' ';
        }

        return compact('firstname', 'lastname');
    }
}
