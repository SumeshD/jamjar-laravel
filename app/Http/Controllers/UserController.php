<?php

namespace JamJar\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JamJar\Profile;
use JamJar\Rules\UKPostcode;
use JamJar\User;
use JamJar\VehicleOwner;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for editing the specified resource.§
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        if(auth()->user()->isPartner()) {
            return redirect(route('partnerAccount'));
        }

        $user = ($id == null) ? Auth::user() : User::find($id);
        return view('users.edit', compact('user'));
    }

    public function forceRedirectToAssociateApplyForm()
    {
        auth()->logout();

        return redirect(route('becomeAPartner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $user = auth()->user();

        // Find users with requested email
        $users = User::where('email', request()->input('email'))->get();

        // Check if email is more than 1
        if(sizeof($users) > 0 && request()->input('email') != $user->email){
            alert()->error('Email address already taken.');
            return back();
        }

        if($user->isPartner()){
            $validationArgs = [
            'name' => 'required',
            'email' => 'required|email',
            'address_line_one' => 'required',
            'town' => 'required',
            'city' => 'required',
            'postcode' => ['required', new UKPostcode],
            'mobile_number' => 'required|max:20|phone:GB,mobile,fixed_line'
            ];
        } else {
            $validationArgs = [
            'name' => 'required',
            'city' => 'required',
            'email' => 'required|email',
            'postcode' => ['required', new UKPostcode],
            'mobile_number' => 'required|max:20|phone:GB,mobile,fixed_line'
            ];
        }

        $this->validate(
            request(),
            $validationArgs
        );

        $data = [
            'name' => request()->input('name'),
            'email' => request()->input('email'),
            'address_line_one' => request()->input('address_line_one'),
            'address_line_two' => request()->input('address_line_two'),
            'town' => request()->input('town'),
            'city' => request()->input('city'),
            'county' => request()->input('county'),
            'postcode' => request()->input('postcode'),
            'telephone_number' => request()->input('telephone'),
            'mobile_number' => request()->input('mobile_number'),
        ];

        if ($user->profile != null) {
            $user->profile()->update($data);
        } else {
            $user->profile->save($data);
        }

        $user->email = request()->input('email');
        $user->send_additional_emails = (bool) request()->input('send_additional_emails');
        $user->save();

        $vehicleOwners = VehicleOwner::where('user_id', '=', $user->id)->get();

        foreach ($vehicleOwners as $vehicleOwner) {
            $vehicleOwner->email = request()->input('email');
            $vehicleOwner->save();
        }

        alert()->success('Account Updated!');

        return back();
    }

    public function delete()
    {
        return view('users.delete');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->forceDelete();
        auth()->logout();
        alert()->info('Your account has been removed from jamjar.com')->persistent();
        return redirect()->route('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCurrentUser()
    {
        $user = auth()->user();

        if (!$user) {
            return redirect()->route('/');
        }

        $user->forceDelete();
        auth()->logout();
        alert()->info('Your account has been removed from jamjar.com')->persistent();
        return redirect()->route('/');
    }

    public function notificationUnsubscribe()
    {
        $user = auth()->user();

        $user->send_additional_emails = false;
        $user->save();

        alert()->info('You have successfully been unsubscribed from emails informing you of new offers or reminding you of existing offers.
        You will not receive any future emails unless you request new valuations.')->persistent();

        return response()->redirectTo(route('/'));
    }
}
