<?php

namespace JamJar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JamJar\Http\Controllers\Controller;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlert;
use JamJar\Services\VehicleFilter\MarketplaceAlertsService;
use JamJar\Services\VehicleFilter\VehicleFilters;
use JamJar\Services\VehicleFilter\VehicleFilterService;
use JamJar\Vehicle;
use Symfony\Component\HttpFoundation\JsonResponse;

class MarketplaceAlertsController extends Controller
{
    const DEFAULT_ALERT_NAME_BASE = 'Marketplace Alert #';

    /** @var VehicleFilterService */
    private $filterService;

    /** @var MarketplaceAlertsService */
    private $marketplaceAlertsService;

    public function __construct(VehicleFilterService $filterService, MarketplaceAlertsService $marketplaceAlertsService)
    {
        $this->filterService = $filterService;

        $this->marketplaceAlertsService = $marketplaceAlertsService;
    }

    public static function getAlertValidation(VehicleFilters $filters, string $name = null, string $alertFrequency = null, MarketplaceAlert $alert = null): \Illuminate\Validation\Validator
    {
        $validator = Validator::make([
            'alert-name' => $name,
            'alert-frequency' => $alertFrequency,
        ], [
            'alert-name' => 'required|min:3',
            'alert-frequency' => 'required|in:' . MarketplaceAlert::FREQUENCY_DAILY . ',' . MarketplaceAlert::FREQUENCY_INSTANT,
        ]);

        $validator->after(function ($validator) use ($name, $alertFrequency, $filters, $alert) {
            $alertWithSelectedName = MarketplaceAlert::where([['user_id', '=', auth()->user()->id], ['name', '=', $name]]);
            if ($alert) {
                $alertWithSelectedName->where('id', '<>', $alert->getId());
            }

            if ((bool) $alertWithSelectedName->first()) {
                $validator->errors()->add('alert-name', 'Name must be unique');
            }

            if ($alertFrequency == MarketplaceAlert::FREQUENCY_INSTANT && !self::itIsPossibleToCreateInstantAlertBasedOnFilters($filters)){
                $validator->errors()->add('alert-frequency', 'Your selected filters are too broad for instant alerts. For real time alerts on individual vehicles, please select more specific filter options.');
            }
        });

        return $validator;
    }

    public function create(Request $request)
    {
        $filters = $this->filterService->createFiltersFromRequest($request);
        $name = $request->get('alert-name') ? $request->get('alert-name') : self::getNextFreeDefaultName();
        $alertFrequency = $request->get('alert-frequency');

        $validator = self::getAlertValidation($filters, $name, $alertFrequency);

        if ($validator->fails()) {
            return new JsonResponse(['errors' => $validator->errors()], 400);
        }

        $alert = $this->marketplaceAlertsService->createAlert(auth()->user(), $filters, $name, $alertFrequency);

        return new JsonResponse(['marketplace_alert_id' => $alert->getId()]);
    }

    public function getAlertConfiguration(Request $request)
    {
        $filters = $this->filterService->createFiltersFromRequest($request);
        $isInstantAlertPossible = $this->itIsPossibleToCreateInstantAlertBasedOnFilters($filters);
        $nextFreeDefaultName = self::getNextFreeDefaultName();

        return new JsonResponse([
            'isInstantAlertPossible' => $isInstantAlertPossible,
            'nextFreeDefaultName' => $nextFreeDefaultName,
        ]);
    }

    private static function itIsPossibleToCreateInstantAlertBasedOnFilters(VehicleFilters $filters): bool
    {
        $emptyFilters = new VehicleFilters();

        list($totalVehiclesCount,) = Vehicle::getRawLeadsVehiclesForMatrix(auth()->user(), $emptyFilters);
        list($filteredResultsCount,) = Vehicle::getRawLeadsVehiclesForMatrix(auth()->user(), $filters);

        if ($totalVehiclesCount == 0) {
            return true;
        }

        return $filteredResultsCount / $totalVehiclesCount < config('app.marketplace_alerts_instant_percentage_factor');
    }

    public static function getNextFreeDefaultName(): string
    {
        $alertsWithDefaultNames = MarketplaceAlert::where('user_id', '=', auth()->user()->id)
            ->whereRaw('name LIKE \''. self::DEFAULT_ALERT_NAME_BASE .'%\'')
            ->get();

        if (count($alertsWithDefaultNames) == 0) {
            return self::DEFAULT_ALERT_NAME_BASE . '1';
        }

        $maxNumber = null;

        foreach ($alertsWithDefaultNames as $alert) {
            $defaultNameNumber = substr($alert->getName(), strlen(self::DEFAULT_ALERT_NAME_BASE));

            if ((int) $defaultNameNumber != $defaultNameNumber) {
                continue;
            }

            if ($maxNumber < (int) $defaultNameNumber) {
                $maxNumber = $defaultNameNumber;
            }
        }

        if (!$maxNumber) {
            return self::DEFAULT_ALERT_NAME_BASE . '1';
        }

        return self::DEFAULT_ALERT_NAME_BASE . ($maxNumber + 1);
    }

}
