<?php

namespace JamJar\Http\Controllers\Api;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\PartnerPosition;
use JamJar\UserStatement;
use JamJar\Valuation;

class ValuationsController extends Controller
{


    public function getData(Request $request) {


        $limit = $request->input('length');
        $start = $request->input('start');

        $leadSource = $request->get('lead_source');
        $outbid = $request->get('outbid');
        $offerStatus = $request->get('offer_status');
        $vrm = $request->get('vrm');



        $valuations = Valuation::select('valuations.*')->where('company_id', auth()->user()->company->id);
        $totalData = $valuations->count();
        $totalFiltered = $totalData;

        if (isset($leadSource) && $leadSource == 'automated_bids') {
            $valuations->where('is_created_by_automated_bids', '=', true);
        }


        if (isset($leadSource) && $leadSource == 'marketplace') {
            $valuations->where('is_created_by_automated_bids', '=', false);
            $valuations->where('position', '=', 1);
        }

        if (isset($leadSource) && $leadSource == 'all') {

            $position = PartnerPosition::where('company_id','=',auth()->user()->company->id)->where('user_id','=',auth()->user()->id)->first();

            if($position && $position->position) {
                $companyPosition = $position->position;
                $valuations->where('position', '<=', $companyPosition);
            }

        }

        if (isset($vrm) && $vrm != '') {
            $valuations->leftJoin('vehicles', 'valuations.vehicle_id', 'vehicles.id');
            $valuations->where('vehicles.numberplate', '=', $vrm);
        }



        $valuations = $valuations->latest()->get();

        $paidLeads = Valuation::getPaidLeadsByValuations($valuations, $offerStatus, isset($leadSource) ? $leadSource : false);

        if (isset($outbid) && $outbid) {

            foreach ($paidLeads as $key => $paidLead) {
                $valuation = $paidLead->partnerValuation;

                if($valuation && $paidLead->highestValuation) {

                    if ($paidLead->highestValuation->id == $valuation->id) {
                        unset($paidLeads[$key]);
                    }
                }


            }
        }

        $totalFiltered = count($paidLeads);
        $paidLeads = array_slice($paidLeads,$start,$limit);

        $data = [];

        if(!empty($paidLeads)){

            foreach($paidLeads as $paidLead) {

                $valuation = $paidLead->partnerValuation;

                $fee = number_format($valuation->fee / 100, 2);

                if(auth()->user()->profile->use_flat_fee==1) {

                    $statement = UserStatement::where('meta','like','%"id":'.$valuation->id.'%')->where('user_id',auth()->user()->id)->first();
                    if($statement) {
                        $fee = number_format($statement->amount,2);
                    } else {
                        $fee = 0;
                    }

                }

                $fee = $fee < 0 ? 0 : $fee;

                $newData['created_at'] = $valuation->created_at->format('d/m/Y')."<br />". $valuation->created_at->format('H:i:s');
                $newData['name'] = $valuation->vehicle->meta->formatted_name;
                $newData['postcode'] = strtoupper($valuation->vehicle->owner->getAreaByPostcode());
                $newData['num_owners'] = trans('vehicles.owners_count_short.owners_'.($valuation->vehicle->meta->number_of_owners <= 5 ? $valuation->vehicle->meta->number_of_owners : 6));
                $newData['write_off'] = $valuation->vehicle->meta->write_off ? 'Yes' : 'No';
                $newData['non_runner'] = trans('vehicles.NonRunner' . $valuation->vehicle->meta->non_runner);
                $newData['plate_year'] = str_replace_first(' ', '/', $valuation->vehicle->meta->plate_year);
                $newData['mileage'] = $valuation->vehicle->meta->mileage;
                $newData['position'] = $valuation->getRealPosition();
                $newData['outbid'] = $newData['position'] != 1 ? '<div class="outbid-info-container">
                                                    <span class="outbid-info"><b>! OUTBID</b></span>
                                                </div>
                                                <div class="outbid-text">
                                                    <b style="color: red; text-decoration: line-through;">'.$valuation->value.'
                                                    </b>
                                                </div>' : ' <div>'.$valuation->value.'</div>';

                $newData['fee'] = '<strong>&pound;'.$fee.'</strong>';
                $newData['details'] = '<a class="button button--orange-bg" href="'.route('partnerShowValuation', $valuation).'">view details</a>';
                $newData['highest_valuation_id'] = $paidLead->highestValuation->id;
                $newData['valuation_id'] = $valuation->id;

                $data[] = $newData;
            }
        }



        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

    }

}