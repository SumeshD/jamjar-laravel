<?php

namespace JamJar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use JamJar\Http\Controllers\Controller;

class AmazonNotifyController extends Controller
{

    public function instanceChange(Request $request) {

        $data = $request->json()->all();

        \Log::info('TYPE: '.$data['Type'].' MESSAGE: '.$data['Message']. ' TIMESTAMP: '.$data['Timestamp']);
        Artisan::call('queue:work');

        return response(['success' => true], 200);
    }

}

