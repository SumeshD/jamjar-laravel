<?php

namespace JamJar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use JamJar\Events\OfferUpdated;
use JamJar\Http\Controllers\Controller;
use JamJar\Jobs\CacheAutomatedBids;
use JamJar\Offer;
use JamJar\OfferDerivative;
use JamJar\OfferModel;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;
use Ramsey\Uuid\Uuid;

class OfferController extends Controller
{

    public function getData() {

        $offers = Offer::with(['manufacturer','models'])
            ->where('user_id', auth()->id())
            ->where('finished', 1)
            ->orderBy('name', 'asc')
            ->get();

        foreach ($offers as $offer) {

            $vehicleManufacturer = $offer->getVehicleManufacturer();
            $offer->manufacturer_name = $vehicleManufacturer ? $vehicleManufacturer->name : 'Unknown manufacturer';
            $offer->offerCount = $offer->getSuggestedValuesCountForOffer()[0]->derivative_count;

        }

        $response = [
            'data' => $offers,
            'scrap_dealer' => auth()->user()->profile->is_scrap_dealer
        ];
        return response()->json($response);
    }

    public function getVehicles(Request $request) {

        cache()->forget('offers-for-'.auth()->id());

        $offer = Offer::with(['manufacturer','models', 'derivatives', 'derivatives.derivative'])
            ->where('user_id', auth()->id())
            ->where('finished', 1)
            ->where('slug','=',$request->slug)
            ->first();

        $vehicleManufacturer = $offer->getVehicleManufacturer();
        $offer->manufacturer_name = $vehicleManufacturer ? $vehicleManufacturer->name : 'Unknown manufacturer';
        $offer->trans_minimum_mot = trans('vehicles.'.$offer->minimum_mot);
        $offer->trans_accept_write_offs = trans('vehicles.AcceptWriteOffs' . $offer->accept_write_offs);
        $offer->max_write_off = ucfirst($offer->maximum_write_off_category);
        $offer->min_service_history = trans('vehicles.'.$offer->minimum_service_history);
        $offer->max_owners = trans('vehicles.owners_count_external.owners_'.($offer->maximum_previous_owners <= 5 ? $offer->maximum_previous_owners : 6));
        $offer->is_opened = false;

        foreach($offer->models as $model) {

            cache()->forget('derivatives-for-model-'.$model->cap_model_id.'-and-user-'.$offer->user_id.'-and-offer-'.$offer->id);

            $model->vehicle_model = $model->getVehicleModel();
            $model->derivatives_count = $model->derivatives->count();

            if($model->derivatives_count>0) {
                $derivatives = $model->derivatives;

                foreach($derivatives as $derivative) {

                    if (($derivative->base_value != $offer->base_value) ||  ($derivative->suggested)) {

                        $vehicleDerivative = $derivative->getVehicleDerivative();
                        $derivative->vehicle_derivative = $vehicleDerivative ? $vehicleDerivative->name : 'Unknown derivative';
                    }

                }

                $model->derivatives_custom = $derivatives;


            }

        }

        $response = [
            'data' => $offer,
            'scrap_dealer' => auth()->user()->profile->is_scrap_dealer
        ];
        return response()->json($response);

    }

    /**
     * Store an offer
     *
     * @param  Illuminate\Http\Request $request
     * 
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
            'base_value' => 'required|max:200|numeric',
            'minimum_mileage' => 'required|numeric|min:0',
            'maximum_mileage' => 'required|numeric|min:0',
            'minimum_value' => 'required|numeric|min:0',
            'maximum_value' => 'required|numeric|min:0',
            'minimum_service_history' => 'required',
            'minimum_mot' => 'required',
            'maximum_previous_owners' => 'required',
            ]
        );

        $offer = Offer::create(
            [
            'name'  => $request->get('name') ?? ' ',
            'user_id' => auth()->id(),
            'slug' => $request->get('slug') ?? Uuid::uuid4()->toString(),
            'base_value' => $request->get('base_value'),
            'fuel_type' => $request->get('fuel_type'),
            'vehicle_type' => $request->get('vehicle_type') ? strtoupper($request->get('vehicle_type')) : 'CAR',
            'minimum_mileage' => trim(str_replace(',', '', $request->get('minimum_mileage'))),
            'maximum_mileage' => trim(str_replace(',', '', $request->get('maximum_mileage'))),
            'minimum_value' => trim(str_replace(',', '', $request->get('minimum_value'))),
            'maximum_value' => trim(str_replace(',', '', $request->get('maximum_value'))),
            'minimum_service_history' => $request->get('minimum_service_history'),
            'minimum_mot' => $request->get('minimum_mot'),
            'maximum_previous_owners' => $request->get('maximum_previous_owners'),
            'accept_write_offs' => (bool)$request->get('accept_write_offs'),
            'acceptable_colors' => $request->get('acceptable_colors'),
            'acceptable_defects' => $request->get('accept_write_offs') ? $request->get('acceptable_defects') : [],
            'great_condition_percentage_modifier' => $request->get('great_condition_percentage_modifier') ?? 100,
            'good_condition_percentage_modifier' => $request->get('good_condition_percentage_modifier') ?? 80,
            'avg_condition_percentage_modifier' => $request->get('avg_condition_percentage_modifier') ?? 60,
            'poor_condition_percentage_modifier' => $request->get('poor_condition_percentage_modifier') ?? 40,
            'minimum_age' => $request->get('minimum_age'),
            'maximum_age' => $request->get('maximum_age'),
            'maximum_write_off_category' => $request->get('maximum_write_off_category'),
            'manufacturer_id' => $request->get('manufacturer_id') ?? null,
            'model_ids' => $request->get('model_ids') ?? null,
            'derivative_ids' => $request->get('derivative_ids') ?? null,
            'active' => $request->get('active') ?? 0,
            ]
        );

        $manufacturer = VehicleManufacturer::where([
            ['cap_manufacturer_id', '=', $offer->manufacturer_id],
            ['vehicle_type', '=', $offer->vehicle_type],
        ])->first();


        if ($offer) {

            //CacheAutomatedBids::dispatch($offer);

            return response(
                [
                    'success' => true,
                    'uuid' => $offer->slug,
                    'offer' => $offer->toArray(),
                    'manufacturer' => $manufacturer
                ],
                201
            );
        }
    }

    /**
     * Update an offer
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\Offer            $offer
     * 
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        /*
            If there are more than 1000 derivatives posted to this API then
            we should take care to not exceed PHP's max_execution_timeout
            and as such, send all of our data to an event which will
            process the information we need to in a timely manner.
         */

        if (count($request->selectedDerivatives) > 1000) {
            // build up a collection for derivatives and models
            $models = collect();
            $derivatives = collect();
            // loop through models
            foreach ($request->get('models') as $model) {
                if (in_array($model['cap_model_id'], $request->selectedModels)) {
                    // if the model is selected, push it to the collection
                    $models->push(
                        [
                        'cap_model_id' => $model['cap_model_id'],
                        'base_value' => $model['base_value'] ?? $request->get('base_value')
                        ]
                    );
                    // loop through the derivatives
                    foreach ($model['derivatives'] as $derivative) {
                        $needle = $model['cap_model_id'] .'-'.$derivative['cap_derivative_id'];
                        if (in_array($needle, $request->selectedDerivatives)) {
                            // if the derivative is selected, push it to the collection
                            $derivatives->push(
                                [
                                'cap_model_id' => $model['cap_model_id'],
                                'cap_derivative_id' => $derivative['cap_derivative_id'],
                                'base_value' => $derivative['base_value'] ?? $request->get('base_value')
                                ]
                            );
                        }
                    }
                }
            }
            // Fire an event which handles all of these models and derivatives behind the scenes
            event(new OfferUpdated($offer, $models, $derivatives));
        } else {
            /*
                If we have less than 1000 derivatives submitted, the servers should be
                able to handle this, so we will do the database operations
                on the fly...
             */
            
            // loop through the models submitted
            foreach ($request->get('models') as $model) {
                if (in_array($model['cap_model_id'], $request->selectedModels)) {
                    // if they are selected, create or update an offer model record
                    $offer->models()->updateOrCreate(
                        [
                            'cap_model_id' => $model['cap_model_id'],
                        ],
                        [
                            'cap_model_id' => $model['cap_model_id'],
                            'base_value' => $model['base_value'] ?? $request->get('base_value')
                        ]
                    );
                    
                    // loop through the derivatives submitted
                    foreach ($model['derivatives'] as $derivative) {
                        $needle = $model['cap_model_id'] .'-'.$derivative['cap_derivative_id'];
                        if (in_array($needle, $request->selectedDerivatives)) {
                            // if they are selected, create or update an offer derivative record
                            $offer->derivatives()->updateOrCreate(
                                [
                                    'cap_model_id' => $model['cap_model_id'],
                                    'cap_derivative_id' => $derivative['cap_derivative_id']
                                ],
                                [
                                    'cap_model_id' => $model['cap_model_id'],
                                    'cap_derivative_id' => $derivative['cap_derivative_id'],
                                    'base_value' => $derivative['base_value'] ?? $request->get('base_value')
                                ]
                            );
                        }
                    }
                }
            }
        }

        //CacheAutomatedBids::dispatch($offer);
        
        return response(['success' => true], 200);
    }

    /**
     * Complete an offer
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\Offer            $offer
     * 
     * @return Illuminate\Http\Response
     */
    public function complete(Request $request, Offer $offer)
    {
        cache()->forget('offers-for-'.auth()->id());

        $offer->update(
            [
            'active' => 1,
            'finished' => 1
            ]
        );

        return response(['success' => true, 'redirect_uri' => route('partnerOffers')], 200);
    }


    public function showVehicles(Request $request, Offer $offer)
    {

        $minAge = (int)explode('-', $offer->minimum_age)[0];
        $maxAge = (int)explode('-', $offer->maximum_age)[0];


        $vehicles = VehicleModel::with(['manufacturer', 'derivatives'])
            ->distinct('cap_model_id')
            ->where('manufacturer_id', $offer->manufacturer_id)
            ->where('fuel_type', $offer->fuel_type)
            ->whereRaw('
                    (vehicle_models.manufacturer_id = ' . $offer->manufacturer_id . ') and 
                    (
                        (`introduced_date` <= '.$minAge.' and `discontinued_date` between '.$minAge.' and '.$maxAge.') or
                        (`introduced_date` between '.$minAge.' and '.$maxAge.' and `discontinued_date` >= '.$maxAge.') or
                        (`introduced_date` < '.$minAge.' and `discontinued_date` > '.$maxAge.') or
                        (`introduced_date` >= '.$minAge.' and `discontinued_date` <= '.$maxAge.')
                    )
            ')
            ->orderBy('name','asc')
            ->orderBy('introduced_date','asc')
            ->get()
            ->unique('cap_model_id');

        $selectedDerivatives = $offer->derivative_ids_flipped_array;
        $selectedModels = $offer->model_ids_flipped_array;

        $derivativeBaseValues = OfferDerivative::where('offer_id', '=', $offer->id)
            ->whereIn('cap_derivative_id', array_keys($selectedDerivatives))
            ->get()
            ->keyBy('cap_derivative_id')
            ->toArray();

        $data = [
            'offer' => $offer,
            'vehicles' => $vehicles,
            'derivativeBaseValues' => $derivativeBaseValues,
            'selectedDerivatives' => $selectedDerivatives,
            'selectedModels' => $selectedModels
        ];

        return response(['success' => true, 'data' => $data], 200);
    }
}
