<?php

namespace JamJar\Http\Controllers\Api;

use JamJar\Exceptions\NotAllowedException;
use JamJar\Http\Controllers\Controller;
use JamJar\Offer;
use Illuminate\Http\Request;
use JamJar\OfferDerivative;
use JamJar\OfferModel;
use JamJar\User;
use JamJar\VehicleModel;

class SuggestedValuesController extends Controller
{

    public function getSuggestedValuesForOfferAndParticularModel($offerId, $capModelId, Request $request)
    {
        /** @var OfferModel $offerModel */
        $offerModel = OfferModel::where('offer_id', $offerId)->first();
        /** @var Offer $offer */
        $offer = Offer::where('id',$offerId)->first();
        $this->guardAgainstOwnership($offer);
        $chosenDerivativeCount = OfferDerivative::where('offer_id', $offerId)
            ->where('cap_model_id',$capModelId)
            ->count();
        $manufacturerName = $offer->manufacturer()->first()->name;
        $capManufacturerId = $offer->manufacturer()->first()->cap_manufacturer_id;
        $suggestedValuesForModel = $offerModel->getSuggestedValuesForOfferAndModel($capModelId);
        $vehicleModel = VehicleModel::where('cap_model_id',$capModelId)
            ->where('manufacturer_id', $capManufacturerId)
            ->first();
        return view('matrix.suggested-values.suggested-values-table', [
            'manufacturerName' => $manufacturerName,
            'chosenDerivativesCount' => $chosenDerivativeCount,
            'vehicleModel' => $vehicleModel,
            'suggestedValues' => $suggestedValuesForModel,
            'offerSlug' => $offer->slug
        ]);
    }

    public function getSuggestedValues(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $perPage = 50;
        $pageNumber = (int) $request->get('pageNumber') ? (int) $request->get('pageNumber') : 1;
        $totalResults = $user->getSuggestedValuesCount();

        $suggestedValues = $user->getSuggestedValuesWithModelsAndDerivatives($pageNumber, $perPage);
        $offersGroupedByManufacturer = $this->groupSuggestedValuesByManufacturer($suggestedValues);

        $pagination = [
            'current_page' => $pageNumber,
            'last_page' => ceil($totalResults / $perPage)
        ];

        return response([
            'success' => true,
            'data' => $offersGroupedByManufacturer,
            'pagination' => $pagination,
            'count' => $totalResults
        ], 200);
    }

    private function groupSuggestedValuesByManufacturer(array $suggestedValues): array
    {
        $offersGroupedByManufacturer = [];

        foreach ($suggestedValues as $suggestedValue) {
            $id = $suggestedValue->manufacturer_id.'_'.$suggestedValue->fuel_type;

            if(!isset($offersGroupedByManufacturer[$id])){
                $offersGroupedByManufacturer[$id] = (array) $suggestedValue;
            }

            if (!isset($offersGroupedByManufacturer[$id]['offers_by_model'])) {
                $offersGroupedByManufacturer[$id]['offers_by_model'] = [];
            }

            if (!isset($offersGroupedByManufacturer[$id]['offers_by_model'][$suggestedValue->cap_model_id])) {
                $offersGroupedByManufacturer[$id]['offers_by_model'][$suggestedValue->cap_model_id] = [];
            }

            $returnedValue = (array) $suggestedValue;
            $returnedValue['vehicle_derivatives'] = [
                'name' => $suggestedValue->derivative_name,
                'model' => [
                    'name' => $suggestedValue->model_name,
                    'introduced_date' => $suggestedValue->introduced_date,
                    'discontinued_date' => $suggestedValue->discontinued_date,
                ],
            ];

            $offersGroupedByManufacturer[$id]['offers_by_model'][$suggestedValue->cap_model_id][] = $returnedValue;
        }

        return $offersGroupedByManufacturer;
    }

    private function guardAgainstOwnership(Offer $offer)
    {
        if (auth()->user()->id != $offer->user->id) {
            alert()->error('Page not found')->persistent();
            throw new NotAllowedException();
        }
    }

}
