<?php

namespace JamJar\Http\Controllers\Api\Images;

use JamJar\Http\Controllers\Controller;
use JamJar\Services\EloquentUploadedImageManager;
use JamJar\Services\ImagesManagement\ImageNotFoundException;
use JamJar\Services\ImagesManagement\ImagesManagerInterface;
use JamJar\Services\UploadedImageNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Ramsey\Uuid\Uuid;

class ImagesController extends Controller
{
    /** @var EloquentUploadedImageManager */
    private $uploadedImageManager;
    /** @var ImagesManagerInterface */
    private $imagesManager;

    public function __construct(
        EloquentUploadedImageManager $uploadedImageManager,
        ImagesManagerInterface $imagesManager
    ) {
        $this->uploadedImageManager = $uploadedImageManager;
        $this->imagesManager = $imagesManager;
    }

    /**
     * @SWG\Get(
     *   path="/image/{uploaded_image_id}/{fileName}",
     *   summary="Returns original image",
     *   @SWG\Response(
     *     response="200",
     *     description="Successfully returns an original content of an image"
     *   )
     * )
     */
    public function image(Request $request, string $uploadedImageId, string $fileName)
    {
        if (!Uuid::isValid($uploadedImageId)) {
            return new Response('invalid-id', 404);
        }
        try {
            $uploadedImage = $this->uploadedImageManager->getById(Uuid::fromString($uploadedImageId));
        } catch (UploadedImageNotFoundException $e) {
            return new Response('image-not-found', 404);
        }
        try {
            $image = $this->imagesManager->getImage($uploadedImage);
        } catch (ImageNotFoundException $e) {
            return new Response('no-image-content', 404);
        }

        $response = new Response($image);
        $response->header('Content-Type', $this->headerContentTypeByFileName($fileName));

        return $response;
    }

    /**
     * @SWG\Get(
     *   path="/thumbnail/{uploaded_image_id}/{fileName}",
     *   summary="Returns thumbnail of image",
     *   @SWG\Response(
     *     response="200",
     *     description="Successfully returns a thumbnail of an image"
     *   )
     * )
     */
    public function thumbnail(Request $request, string $uploadedImageId, string $fileName)
    {
        if (!Uuid::isValid($uploadedImageId)) {
            return new Response('', 404);
        }

        try {
            $uploadedImage = $this->uploadedImageManager->getById(Uuid::fromString($uploadedImageId));
        } catch (UploadedImageNotFoundException $e) {
            return new Response('', 404);
        }

        try {
            $image = $this->imagesManager->getThumbnail($uploadedImage);
        } catch (ImageNotFoundException $e) {
            return new Response('', 404);
        }

        $response = new Response($image);
        $response->header('Content-Type', $this->headerContentTypeByFileName($fileName));

        return $response;
    }

    private function headerContentTypeByFileName(string $fileName)
    {
        if (substr($fileName, -3) == 'jpg' || substr($fileName, -4) == 'jpeg') {
            return 'image/jpeg';
        }

        if (substr($fileName, -3) == 'png') {
            return 'image/png';
        }

        if (substr($fileName, -3) == 'gif') {
            return 'image/gif';
        }

        return 'image/jpeg';
    }
}
