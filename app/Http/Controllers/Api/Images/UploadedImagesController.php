<?php

namespace JamJar\Http\Controllers\Api\Images;

use JamJar\Http\Controllers\Controller;
use JamJar\Model\UploadedImage;
use JamJar\Services\EloquentUploadedImageManager;
use JamJar\Services\ImagesManagement\ImagesManagerInterface;
use JamJar\Services\UploadedImageNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class UploadedImagesController extends Controller
{
    /** @var EloquentUploadedImageManager */
    private $uploadedImageManager;
    /** @var ImagesManagerInterface */
    private $imagesManager;

    public function __construct(
        EloquentUploadedImageManager $uploadedImageManager,
        ImagesManagerInterface $imagesManager
    ) {
        $this->uploadedImageManager = $uploadedImageManager;
        $this->imagesManager = $imagesManager;
    }

    /**
     * @SWG\Get(
     *   path="/api/v1/uploaded-images",
     *   summary="Returns all uploaded images",
     *   parameters={
     *     @SWG\Parameter(name="status", in="query", required=false, type="string", description="available values: active, deleted. Default: all"),
     *     @SWG\Parameter(name="order_by", in="query", required=false, type="string", description="available values: created_at, original_name. Default: created_at"),
     *     @SWG\Parameter(name="order_direction", in="query", required=false, type="string", description="available values: asc, desc. Default: desc"),
     *     @SWG\Parameter(name="per_page", in="query", required=false, type="string", description="how many entities per page. Default: 20"),
     *     @SWG\Parameter(name="page_num", in="query", required=false, type="string", description="number of page. Default: 1"),
     *   },
     *   @SWG\Response(
     *     response="200",
     *     description="Successfully returns all uploaded images                                                                                                               ",
     *     @SWG\Schema(
     *       type="object",
     *       example="{'data': {'uploaded_images':[{'id':'7a9697d1-7e54-4a13-b349-db6103a3624a','status':'active',...},{...}]}}",
     *       properties={
     *         @SWG\Property(
     *           property="data.uploaded_images",
     *           type="array",
     *           description="array of uploaded images",
     *           properties={
     *             @SWG\Property(property="id", type="uuid"),
     *             @SWG\Property(property="status", type="uuid", description="available values: active, deleted"),
     *             @SWG\Property(property="relative_path_to_original_image", type="string"),
     *             @SWG\Property(property="relative_path_to_thumbnail", type="string"),
     *             @SWG\Property(property="original_name", type="string"),
     *             @SWG\Property(property="created_at", type="date"),
     *             @SWG\Property(property="updated_at", type="date"),
     *           }
     *         ),
     *       }
     *     ),
     *   )
     * )
     */
    public function list(Request $request)
    {
        $status = $request->input('status') ?: null;
        $orderBy = $request->input('order_by') ?: null;
        $orderDirection = $request->input('order_direction') ?: null;
        $perPage = (int) $request->input('per_page') ?: 20;
        $pageNum = (int) $request->input('page_num') ?: 1;

        $status = $status == 'active' || $status == 'deleted' ? $status : null;
        $orderBy = $orderBy == 'created_at' || $orderBy == 'original_name' ? $orderBy : null;
        $orderDirection = $orderDirection == 'DESC' || $orderDirection == 'ASC' ? $orderDirection : 'DESC';

        $uploadedImages = $this->uploadedImageManager->getAll($status, $orderBy, $orderDirection, $perPage, $pageNum);

        $uploadedImagesResponse = [];

        foreach ($uploadedImages as $uploadedImage) {
            $uploadedImagesResponse[] = $this->convertUploadedImageToResponseJson($uploadedImage);
        }

        return $this->jsonResponse([
            'data' => [
                'uploaded_images' => $uploadedImagesResponse
            ]
        ]);
    }


    /**
     * @SWG\Post(
     *   path="/api/v1/uploaded-images",
     *   summary="Create new uploaded image",
     *   parameters={
     *     @SWG\Parameter(name="original_name", in="formData", required=true, type="string"),
     *     @SWG\Parameter(name="image", in="formData", required=true, type="string", description="image encoded in base64"),
     *   },
     *   @SWG\Response(
     *     response="200",
     *     description="New uploaded image has been created",
     *     @SWG\Schema(
     *       type="object",
     *       properties={
     *         @SWG\Property(
     *           property="data.uploaded_image",
     *           type="object",
     *           description="newly created uploaded image",
     *           properties={
     *             @SWG\Property(property="id", type="uuid"),
     *             @SWG\Property(property="status", type="string", description="active"),
     *             @SWG\Property(property="relative_path_to_original_image", type="string"),
     *             @SWG\Property(property="relative_path_to_thumbnail", type="string"),
     *             @SWG\Property(property="original_name", type="string"),
     *             @SWG\Property(property="created_at", type="date"),
     *             @SWG\Property(property="updated_at", type="date"),
     *           }
     *         ),
     *       }
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="400",
     *     description="Invalid data",
     *     @SWG\Schema(
     *       type="object",
     *       example="{'errors':['image':['Field image is required'], 'original_name':['Field original_name is too long']]",
     *       properties={
     *         @SWG\Property(property="errors", type="object", description="array with errors"),
     *       }
     *     ),
     *   )
     * )
     */
    public function create(Request $request)
    {
        $file = $request->file('file');
        $fileContent = file_get_contents($file->getRealPath());

        $data = [
            'original_name' => $file->getClientOriginalName(),
            'image' => $fileContent,
        ];

        $validation = [
            'original_name' => 'required|min:5|max:1024',
            'image' => 'required',
        ];

        $validator = Validator::make($data, $validation);

        if ($validator->fails()) {
            return $this->jsonResponse(['errors' => $validator->messages()->messages()], 400);
        }

        $uploadedImage = $this->uploadedImageManager->create(
            $data['original_name'],
            'active'
        );

        $this->imagesManager->uploadImage($data['image'], $uploadedImage);

        return $this->jsonResponse([
            'data' => [
                'uploaded_image' => $this->convertUploadedImageToResponseJson($uploadedImage)
            ]
        ]);
    }

    /**
     * @SWG\Put(
     *   path="/api/v1/uploaded-images/{uploaded_image_id}",
     *   summary="Edit uploaded image",
     *   parameters={
     *     @SWG\Parameter(name="original_name", in="formData", required=true, type="string"),
     *     @SWG\Parameter(name="status", in="formData", required=true, type="string", description="available: active, deleted"),
     *   },
     *   @SWG\Response(
     *     response="200",
     *     description="New uploaded image has been changed",
     *     @SWG\Schema(
     *       type="object",
     *       properties={
     *         @SWG\Property(
     *           property="data.uploaded_image",
     *           type="object",
     *           description="edited uploaded image",
     *           properties={
     *             @SWG\Property(property="id", type="uuid"),
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="relative_path_to_original_image", type="string"),
     *             @SWG\Property(property="relative_path_to_thumbnail", type="string"),
     *             @SWG\Property(property="original_name", type="string"),
     *             @SWG\Property(property="created_at", type="date"),
     *             @SWG\Property(property="updated_at", type="date"),
     *           }
     *         ),
     *       }
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="400",
     *     description="Invalid data",
     *     @SWG\Schema(
     *       type="object",
     *       example="{'errors':['image':['Field image is required'], 'original_name':['Field original_name is too long']]",
     *       properties={
     *         @SWG\Property(property="errors", type="object", description="array with errors"),
     *       }
     *     ),
     *   )
     * )
     */
    public function edit(Request $request, string $uploadedImageId)
    {
        if (!Uuid::isValid($uploadedImageId)) {
            return $this->jsonResponse(['errors' => ['uploaded_image_id' => ['Invalid uuid.']]], 404);
        }

        try {
            $uploadedImage = $this->uploadedImageManager->getById(Uuid::fromString($uploadedImageId));
        } catch (UploadedImageNotFoundException $e) {
            return $this->jsonResponse(['errors' => ['uploaded_image_id' => ['Uploaded image does not exist']]], 404);
        }

        $data = [
            'original_name' => $request->input('original_name'),
            'status' => $request->input('status'),
        ];

        $validation = [
            'original_name' => 'required|min:5|max:1024',
            'status' => 'required',
        ];

        $validator = Validator::make($data, $validation);
        $this->addCustomStatusValidation($validator, $request);

        if ($validator->fails()) {
            return $this->jsonResponse(['errors' => $validator->messages()->messages()], 400);
        }

        $editedUploadedImage = $this->uploadedImageManager->edit(
            $uploadedImage,
            $data['original_name'],
            $data['status']
        );

        return $this->jsonResponse([
            'data' => [
                'uploaded_image' => $this->convertUploadedImageToResponseJson($editedUploadedImage)
            ]
        ]);
    }


    /**
     * @SWG\Get(
     *   path="/api/v1/uploaded-images/purge",
     *   summary="Purge database and setup example images",
     *   @SWG\Response(
     *     response="200",
     *      description="DB has been purged"
     *     ),
     *   )
     * )
     */
    public function purge(Request $request)
    {
        Artisan::call('migrate:reset');
        Artisan::call('migrate');
        Artisan::call('team-rocket:fill-database');

        return $this->jsonResponse([]);
    }

    private function convertUploadedImageToResponseJson(UploadedImage $uploadedImage)
    {
        return [
            'id' => $uploadedImage->id()->toString(),
            'status' => $uploadedImage->status(),
            'relative_path_to_original_image' => route('image',
                [
                    'uploaded_image_id' => $uploadedImage->id()->toString(),
                    'file_name' => $uploadedImage->originalName()
                ],
                false
            ),
            'relative_path_to_thumbnail' => route('thumbnail',
                [
                    'uploaded_image_id' => $uploadedImage->id()->toString(),
                    'file_name' => $uploadedImage->originalName()
                ],
                false
            ),
            'original_name' => $uploadedImage->originalName(),
            'created_at' => $uploadedImage->createdAt()->toDateTimeString(),
            'updated_at' => $uploadedImage->updatedAt()->toDateTimeString(),
        ];
    }

    private function addCustomStatusValidation($validator, $request)
    {
        $validator->after(function() use ($validator, $request) {
            if ($request->input('status') == null) {
                return;
            }

            $status = $request->input('status');
            if (!in_array($status, ['active', 'deleted'])) {
                $validator->errors()->add('status', 'Invalid status. Available: active, deleted');
            }
        });
    }
}
