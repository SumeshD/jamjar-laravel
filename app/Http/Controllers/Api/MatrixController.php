<?php

namespace JamJar\Http\Controllers\Api;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\PricingTier;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Setting;

class MatrixController extends Controller
{
    /**
     * Update the Partners Position
     *
     * @param  Illuminate\Http\Request $request
     * 
     * @return Illuminate\Http\Response
     */
    public function updatePosition(Request $request)
    {
        auth()->user()->company->position()->update(
            [
            'position' => $request->position
            ]
        );
        return response(['success' => true], 200);
    }

    /**
     * Calculate the Position Cost
     *
     * @param  Illuminate\Http\Request $request
     * 
     * @return float
     */
    public function calculatePositionCost(Request $request)
    {
        $valuation = (int)preg_replace('/[^0-9]/', '', $request->valuation);

        $calculator = new PartnerFeeCalculator($request->position, $valuation);
        $fee = $calculator->calculate(false,null,false);
        return $fee;
    }
}
