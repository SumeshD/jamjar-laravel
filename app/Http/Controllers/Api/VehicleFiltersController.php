<?php

namespace JamJar\Http\Controllers\Api;

use Illuminate\Support\Facades\Cache;
use JamJar\Http\Controllers\Controller;
use JamJar\Services\VehicleFilter\VehicleFilters;
use JamJar\Services\VehicleFilter\VehicleFilterService;
use Symfony\Component\HttpFoundation\JsonResponse;

class VehicleFiltersController extends Controller
{
    /** @var VehicleFilterService */
    private $filterService;

    public function __construct(VehicleFilterService $filterService)
    {
        $this->filterService = $filterService;
    }

    public function getManufacturersGroupsList()
    {
        return new JsonResponse(['manufacturers' => $this->filterService->getManufacturersList()->asArray()]);
    }

    public function getModelsGroupsList()
    {

        $models = $this->filterService->getModelsList()->asArray();

        foreach($models as $key=>$model) {

           usort($models[$key]['modelsGroups'], function ($item1, $item2) {
                return $item1['label'] <=> $item2['label'];
            });
        }

        return new JsonResponse(['models' => $models]);
    }

    public function getVehiclesSummary()
    {
        $summary = $this->filterService->getFilteredResultsSummary(new VehicleFilters(), auth()->user())->asArray();

        return new JsonResponse(['summary' => $summary]);
    }
}
