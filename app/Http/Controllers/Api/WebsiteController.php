<?php

namespace JamJar\Http\Controllers\Api;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\PartnerWebsite;

class WebsiteController extends Controller
{
    /**
     * Delete a Logo
     *
     * @param  JamJar\PartnerWebsite $website
     * 
     * @return Illuminate\Http\Response
     */
    public function deleteLogo(PartnerWebsite $website)
    {
        if (!auth()->user()->id == $website->user_id) {
            return response(['success' => false], 403);
        }

        if (env('FILESYSTEM_DRIVER') != 's3') {
            unlink(public_path('storage/' . $website->logo_image));
        }

        $website->update(
            [
            'logo_image' => ''
            ]
        );

        return response(['success' => true], 200);
    }

    /**
     * Delete a Hero
     *
     * @param  JamJar\PartnerWebsite $website
     * 
     * @return Illuminate\Http\Response
     */
    public function deleteHero(PartnerWebsite $website)
    {
        if (!auth()->user()->id == $website->user_id) {
            return response(['success' => false], 403);
        }

        if (env('FILESYSTEM_DRIVER') != 's3') {
            unlink(public_path('storage/' . $website->hero_image));
        }

        $website->update(
            [
            'hero_image' => ''
            ]
        );
        return response(['success' => true], 200);
    }
}
