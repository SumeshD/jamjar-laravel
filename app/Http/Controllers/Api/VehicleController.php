<?php

namespace JamJar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use JamJar\Api\CapApiInterface;
use JamJar\Http\Controllers\Controller;
use JamJar\Http\Controllers\Matrix\ValuationDraftsController;
use JamJar\OfferDerivative;
use JamJar\Throttle;
use JamJar\Vehicle;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleMeta;
use JamJar\VehicleModel;

class VehicleController extends Controller
{
    /** @var CapApiInterface */
    protected $capApi;

    public function __construct(CapApiInterface $capApi)
    {
        $this->capApi = $capApi;
    }

    /**
     * Get a list of vehicle manufacturers from the CAP API
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getManufacturers($vehicleType)
    {
        $manufacturers = VehicleManufacturer::where('vehicle_type', $vehicleType)->get();

        if ($manufacturers->isEmpty()) {
            $this->fetchManufacturersFromApi($vehicleType);
            return $this->getManufacturers($vehicleType);
        }

        return $manufacturers;
    }

    /**
     * Get a list of models for the specified manufacturer ID
     *
     * @param  int $manufacturerId
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getModels($manufacturerId, $stopRecursion = false)
    {
        $vehicleType = strtoupper(request()->get('vehicleType')) ?? 'CAR';

        if ($stopRecursion !== true) {
            $this->fetchModelsFromApi($manufacturerId, $vehicleType);
            return $this->getModels($manufacturerId, true);
        }

        $dbVehicleType = ($vehicleType == 'CAR' ? 'CAR' : 'LCV');
        $manufacturerId = (int) $manufacturerId;

        $fuelType = ucfirst(request()->get('fuelType'));
        $minAge = (int)explode('-', request()->get('minAge'))[0];
        $maxAge = (int)explode('-', request()->get('maxAge'))[0];

        $models = VehicleModel::getModelByManufacturerAndTimeFrame($manufacturerId, $minAge, $maxAge, $dbVehicleType, $fuelType);



        if ($models->isEmpty()) {
            return response(['status' => 'error', 'code' => 404, 'message' => 'NO_MODELS_FOUND'], 404);
        }

        $manufacturer = VehicleManufacturer::where([
            ['cap_manufacturer_id', '=', $manufacturerId],
            ['vehicle_type', '=', $dbVehicleType],
        ])->first();

        foreach ($models as $model) {
            $model->manufacturer = $manufacturer;
        }

        return $models->sortBy('name')->unique('cap_model_id')->values()->all();
    }

    /**
     * Get the derivative from the api
     *
     * @param  int $modelId
     * 
     * @return array
     */
    public function getDerivative($modelId = null) : array
    {
        $modelId = request()->get('modelId');
        $internalManufacturerId = request()->get('internalManufacturerId');
        $vehicleType = request()->get('vehicleType') ?? 'CAR';

        $model = VehicleModel::where('cap_model_id',$modelId)->where('internal_manufacturer_id',$internalManufacturerId)->first();

        $derivatives = VehicleDerivative::where('model_id', $modelId)->where('internal_model_id',$model->id)->get();

        if ($derivatives->isEmpty()) {
            $derivatives = $this->fetchDerivsFromApi($modelId, $vehicleType);
            return $this->getDerivative($modelId);
        }

        return $derivatives->toArray();
    }

    /**
     * Get a list of derivatives for the specified model ID
     *
     * @param  int $modelId
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getDerivatives($modelId, $offerId) : array
    {
        $derivatives = OfferDerivative::with('derivative')->where('cap_model_id', $modelId)->where('offer_id', $offerId)->get();
        return $derivatives->toArray();
    }

    /**
     * Get a single derivative by it's ID
     *
     * @param  Illuminate\Http\Request $request
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getDerivativeById(Request $request)
    {
        return VehicleDerivative::where('cap_derivative_id', $request->id)->first();
    }

    /**
     * Fetch Manufacturers from API
     *
     * @param  string $vehicleType
     * 
     * @return array
     */
    protected function fetchManufacturersFromApi($vehicleType)
    {
        $api = $this->capApi;
        $manufacturers = $api->getManufacturers($vehicleType);
        foreach ($manufacturers as $manufacturer) {
            VehicleManufacturer::updateOrCreate($manufacturer->toArray());
        }
        return $manufacturers;
    }

    /**
     * Fetch Models from API
     *
     * @param  int $id manufacturer id
     * 
     * @return array
     */
    protected function fetchModelsFromApi($id, $vehicleType)
    {
        $models = $this->capApi->getModels($id, $vehicleType);

        foreach ($models as $model) {
            $introduced = $model->get('introduced');
            $discontinued = $model->get('discontinued') == 0 ? date('Y') : $model->get('discontinued');
            $plateYears = range($introduced, $discontinued);

            $fuelType = str_contains($model->get('name'), 'DIESEL') ? 'Diesel' : 'Petrol';

            $created = VehicleModel::updateOrCreate(
                [
                    'cap_model_id' => $model->get('cap_model_id'),
                    'manufacturer_id' => (int) $id
                ],
                [
                    'cap_model_id' => $model->get('cap_model_id'),
                    'name' => $model->get('name'),
                    'manufacturer_id' => (int) $id,
                    'introduced_date' => $introduced,
                    'discontinued_date' => $discontinued,
                    'fuel_type' => $fuelType,
                    'plate_years' => implode(',', $plateYears)
                ]
            );
        }

        return $models;
    }

    /**
     * Fetch Derivatives from API
     *
     * @param  int $id
     * 
     * @return array
     */
    protected function fetchDerivsFromApi($id, $vehicleType)
    {
        $api = $this->capApi;
        $derivatives = $api->getDerivatives($id, $vehicleType);

        foreach ($derivatives as $derivative) {
            $deriv = VehicleDerivative::create(
                [
                'cap_derivative_id' => $derivative->get('cap_derivative_id'),
                'name' => $derivative->get('name'),
                'model_id' => $id,
                ]
            );
            $collection = collect()->merge($deriv->load('model'));
        }

        return $collection;
    }

    /**
     * Store Vehicle 
     *
     * @param  Request $request
     * 
     * @return Illuminate\Http\Response          
     */
    public function store(Request $request)
    {

        $vehicle = new Vehicle;
        $vehicle->cap_id = 0;
        $vehicle->capcode = 0;
        $vehicle->numberplate = strtoupper(session('vrm')) ?? '';
        $vehicle->type = $manufacturer->vehicle_type ?? 'CAR';

        $manufacturer = null;
        $model = null;
        $derivative = null;

        $manufacturer = VehicleManufacturer::where([
            ['cap_manufacturer_id', '=', $request['data']['meta']['manufacturer']],
            ['vehicle_type', '=', $vehicle->type == 'CAR' ? 'CAR' : 'LCV'],
        ])->first();

        if ($manufacturer) {
            $vehicle->setVehicleManufacturer($manufacturer);

            $model = VehicleModel::where([
                ['cap_model_id', '=', $request['data']['meta']['model']],
                ['internal_manufacturer_id', '=', $manufacturer->getId()],
            ])->first();

            if ($model) {
                $vehicle->setVehicleModel($model);

                $derivative = VehicleDerivative::where([
                    ['cap_derivative_id', '=', $request['data']['meta']['derivative']],
                    ['internal_model_id', '=', $model->getId()],
                ])->first();

                if ($derivative) {
                    // hadonek!
                    $vehicle->setVehicleDerivative($derivative);
                }
            }
        }

        $vehicle->save();

        $meta = new VehicleMeta([
            'manufacturer' => $manufacturer ? ucwords($manufacturer->name) : null,
            'model' => $model ? ucwords($model->name) : null,
            'derivative' => $derivative ? ucwords($derivative->name) : null,
            'color' => 0,
            'transmission' => ucwords($request['data']['meta']['transmission']),
            'plate_year' => str_replace('-', ' ', $request['data']['meta']['plate_year']),
            'fuel_type' => ucwords($request['data']['meta']['fuel_type']),
        ]);

        $vehicle->meta()->save($meta);

        $request->session()->forget('vehicle-details');
        $request->session()->put('vrm', $vehicle->numberplate);

        return response(['success' => true, 'redirect_uri' => route('showVehicleInformation', $vehicle)], 200);
    }

    /**
     * Get All Models and Derivatives
     *
     * @param  Request $request
     * 
     * @return Illuminate\Http\JsonResponse      
     */
    public function getModelsAndDerivatives(Request $request)
    {
        if (is_array($request->ids)) {
            $modelIds = $request->ids;
        } else {
            $modelIds = explode(',', $request->ids);
        }

        $vehicleType = $request->get('vehicleType') ? strtoupper($request->get('vehicleType')) : 'CAR';

        $models = VehicleModel::select('vehicle_models.*')
            ->where('vehicle_type', '=', $vehicleType)
            ->whereIn('cap_model_id', $modelIds)
            ->join('vehicle_manufacturers', 'vehicle_manufacturers.id', '=', 'vehicle_models.internal_manufacturer_id')
            ->with(['manufacturer', 'derivatives'])
            ->orderBy('vehicle_models.name', 'ASC')
            ->get()
            ->keyBy('cap_model_id');

        $unique = collect();
        $models->each(
            function ($model) use ($unique) {
                if (!$unique->has('cap_model_id', $model->cap_model_id)) {
                    $unique->push($model);
                }
            }
        );

        return response()->json($unique->toArray());
    }

    /**
     * Get Models and Derivatives
     *
     * @param  Request $request
     * 
     * @return Illuminate\Http\JsonResponse      
     */
    public function getModelsAndDerivativesCount(Request $request)
    {
        if (is_array($request->ids)) {
            $modelIds = $request->ids;
        } else {
            $modelIds = explode(',', $request->ids);
        }

        return response()->json(['count' => VehicleModel::select(['id', 'cap_model_id'])->whereIn('cap_model_id', $modelIds)->count()]);
    }

    /**
     * Get the Suggested Value
     *
     * @param  JamJar\VehicleDerivative $derivative
     * 
     * @return Illuminate\Http\JsonResponse
     */
    public function getSuggestedValue($derivativeId)
    {
        $derivative = VehicleDerivative::where('cap_derivative_id', $derivativeId)->first();
        return response()->json($derivative->suggestedValue);
    }

    /**
     * Populate a session from an external website. 
     * 
     * @param type|null $vrm 
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function populateSessionFromExternal(Request $request, $vrm = null)
    {
        if ((!request()->has('vrm')) && (!$vrm)) {
            return redirect()->route('/');
        }

        $vrm = preg_replace('/\s+/', '', trim(request()->vrm));

        if (auth()->user() && auth()->user()->matrix_partner == 1) {
            return redirect(route('matrixValuationDraftsCreateVehicleWrapper') . '?numberplate=' . $vrm);
        }

        if (!is_null(cache('vrm_history'))) {
            $history = array_unique(array_filter(array_flatten(cache('vrm_history'))));
        } else {
            $history = [];
        }

        if (!in_array(strtoupper($vrm), $history)) {
            $throttle = new Throttle($request->instance(), 3, 1);
            if (env('DISABLE_THROTTLE') != true) {
                if (!$throttle->check()) {
                    if (auth()->user()) {
                        \Session::flash('sweet_alert.alert', json_encode([
                            'title' => "Lookup limit reached",
                            'text' => 'It looks like you\'re a dealer as you\'ve hit our lookup limit. For unlimited free lookups, become an associate today. You\'ll be logged out and redirected to the registration page.',
                            'icon' => "warning",
                            'buttons' => [
                                'reject' => ['text' => "Cancel", 'className' => 'swal-button-grey'],
                                'confirm' => ['text' => "OK", 'className' => 'redirect:' . route('forceRedirectToAssociateApplyForm')],
                            ],
                        ]));

                        return back()->with('history', $history);
                    } else {
                        \Session::flash('sweet_alert.alert', json_encode([
                            'title' => "Lookup limit reached",
                            'text' => 'It looks like you\'re a dealer as you\'ve hit our lookup limit. For unlimited free lookups, become an associate today.',
                            'icon' => "warning",
                            'buttons' => [
                                'confirm' => ['text' => "Learn More", 'className' => 'swal-button-orange'],
                            ],
                        ]));

                        return redirect()->route('becomeAPartner');
                    }
                }
            }
            cache(
                [
                    'vrm_history' =>
                        [
                            'previous' => cache('vrm_history'),
                            'current' => strtoupper($vrm)
                        ]
                ],
                now()->addWeeks(1)
            );
        }

        $vehicle = Vehicle::where('numberplate', '=', $vrm)->first();

        if ($vehicle === null) {
            $car = (new Vehicle)->getVehicle($vrm);

            // No car returned from getVehicle = manual entry
            if ($car === null) {
                $request->session()->put('vrm', $vrm);
                return redirect()->route('noVehicleFound', $vrm);
            }
        } else {
            $clone = Vehicle::cloneVehicle($vehicle);
            // let the application know what vehicle we're now referencing
            // Not an instance of JamJar\Vehicle?
            if (is_bool($clone)) {
                alert()->error('Something went wrong, please enter your numberplate again.');
                return redirect()->route('noVehicleFound', $vrm);
            }
            $car = $clone;
        }

        /**
         * In order to stop bots using this as a search engine,
         * we store the vrm in session and check against it
         * in periodic intervals.
         */
        $request->session()->forget('vrm');
        $request->session()->forget('vehicle-details');
        $request->session()->put('vrm', $car->numberplate);


        return redirect()->route('showVehicleInformation', $car->numberplate);
    }

}
