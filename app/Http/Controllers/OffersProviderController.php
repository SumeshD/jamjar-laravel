<?php
namespace JamJar\Http\Controllers;

use Illuminate\Http\JsonResponse;
use JamJar\Api\CapApi;
use JamJar\Exceptions\VehicleDoesNotExistException;
use JamJar\Services\Valuations\ExternalDataProviders\CTBDataProvider;
use JamJar\Services\Valuations\ExternalDataProviders\M4YMDataProvider;
use JamJar\Services\Valuations\ExternalDataProviders\MWDataProvider;
use JamJar\Services\Valuations\ExternalDataProviders\WBCTDataProvider;
use JamJar\Services\Valuations\ExternalDataProviders\WWACDataProvider;
use JamJar\Services\Valuations\InternalDataProviders\AutoPartnerDataProvider;
use JamJar\Services\Valuations\InternalDataProviders\RulesDataProvider;
use JamJar\Services\Valuations\VehiclePriceEstimators\CapPriceEstimator;
use JamJar\Services\Valuations\VehicleValuationsCollector;
use JamJar\Services\Valuations\VehicleValuationsCreator;
use JamJar\Services\Valuations\VehicleValuers\VehicleValuer;
use JamJar\User;
use JamJar\Vehicle;
use Illuminate\Http\Request;
use JamJar\VehicleOwner;
use Ramsey\Uuid\Uuid;

class OffersProviderController extends Controller
{
    const OFFERS_SOURCE_CTB = 1;
    const OFFERS_SOURCE_WWAC = 2;
    const OFFERS_SOURCE_M4YM = 3;
    const OFFERS_SOURCE_WBCT = 4;
    const OFFERS_SOURCE_MW = 5;
    const OFFERS_SOURCE_INTERNAL = 6;

    /** @var VehicleValuationsCreator */
    private $vehicleValuationsCreator;

    public function __construct(VehicleValuationsCreator $vehicleValuationsCreator)
    {
        $this->vehicleValuationsCreator = $vehicleValuationsCreator;
    }

    public function saveValuationDraftFromSource(Request $request)
    {
        $sourceId = (int) $request->get('source-id');
        $vehicleId = (int) $request->get('vehicle-id');
        $createValuation = (bool) $request->get('create-valuation');
        $valuationGroup = $request->get('valuation-group');
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        if (!in_array($sourceId, $this->getAvailableProvidersIds()) || !$vehicle) {
            return new JsonResponse([], 404);
        }

        if ($vehicle->owner->user_id && (!auth()->check() || auth()->user()->id != $vehicle->owner->user_id)) {
            return new JsonResponse([], 404);
        }
        switch ($sourceId) {
            case self::OFFERS_SOURCE_CTB:
                return $this->saveCTBValuation($vehicle, $createValuation, $valuationGroup);
            case self::OFFERS_SOURCE_WWAC:
                return $this->saveWWACValuation($vehicle, $createValuation, $valuationGroup);
            case self::OFFERS_SOURCE_M4YM:
                return $this->saveM4YMValuation($vehicle, $createValuation, $valuationGroup);
            case self::OFFERS_SOURCE_WBCT:
                return $this->saveWBCTValuation($vehicle, $createValuation, $valuationGroup);
            case self::OFFERS_SOURCE_MW:
                return $this->saveMWValuation($vehicle, $createValuation, $valuationGroup);
            case self::OFFERS_SOURCE_INTERNAL:
                return $this->saveInternalValuations($vehicle, $createValuation, $valuationGroup);
        }
    }

    private function getAvailableProvidersIds(): array
    {
        return [
            self::OFFERS_SOURCE_CTB,
            self::OFFERS_SOURCE_WWAC,
            self::OFFERS_SOURCE_M4YM,
            self::OFFERS_SOURCE_WBCT,
            self::OFFERS_SOURCE_MW,
            self::OFFERS_SOURCE_INTERNAL,
        ];
    }

    private function saveCTBValuation(Vehicle $vehicle, bool $createValuation = false, $valuationGroup = '')
    {

        foreach ($vehicle->getValuations() as $valuation)
        {
            if ($valuation->getBuyerCompany()->name == CTBDataProvider::CTB_COMPANY_NAME) {
                return $this->convertValuationsToResponse($vehicle, [$valuation]);
            }
        }

        foreach ($vehicle->getDrafts() as $draft) {
            if ($draft->getBuyerCompany()->name == CTBDataProvider::CTB_COMPANY_NAME) {
                return $this->convertDraftsToResponse($vehicle, [$draft]);
            }
        }

        $collector = new VehicleValuationsCollector([], [new VehicleValuer(new CTBDataProvider())]);
        $drafts = $collector->createValuationDrafts($vehicle);
        if($createValuation){
            $valuations = [];
            foreach ($drafts as $draft){
                $capApi = new CapApi();
                $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();
                $valuationGroup = Uuid::fromString($valuationGroup);
                $valuations[] = $this->vehicleValuationsCreator->createValuationFromDraft($draft, $valuationGroup, 1, $capValuation);
            }

            return $this->convertValuationsToResponse($vehicle, $valuations);
        }

        return $this->convertDraftsToResponse($vehicle, $drafts);
    }

    private function saveWWACValuation(Vehicle $vehicle, bool $createValuation = false, $valuationGroup = '')
    {

        foreach ($vehicle->getValuations() as $valuation)
        {
            if ($valuation->getBuyerCompany()->name == WWACDataProvider::WWAC_COMPANY_NAME) {
                return $this->convertValuationsToResponse($vehicle, [$valuation]);
            }
        }

        foreach ($vehicle->getDrafts() as $draft) {
            if ($draft->getBuyerCompany()->name == WWACDataProvider::WWAC_COMPANY_NAME) {
                return $this->convertDraftsToResponse($vehicle, [$draft]);
            }
        }

        $collector = new VehicleValuationsCollector([], [new VehicleValuer(new WWACDataProvider())]);
        $drafts = $collector->createValuationDrafts($vehicle);

        if($createValuation){
            $valuations = [];
            foreach ($drafts as $draft){
                $capApi = new CapApi();
                $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();
                $valuationGroup = Uuid::fromString($valuationGroup);
                $valuations[] = $this->vehicleValuationsCreator->createValuationFromDraft($draft, $valuationGroup, 1, $capValuation);
            }

            return $this->convertValuationsToResponse($vehicle, $valuations);
        }

        return $this->convertDraftsToResponse($vehicle, $drafts);
    }

    private function saveM4YMValuation(Vehicle $vehicle, bool $createValuation = false, $valuationGroup = '')
    {

        foreach ($vehicle->getValuations() as $valuation)
        {
            if ($valuation->getBuyerCompany()->name == M4YMDataProvider::M4YM_COMPANY_NAME) {
                return $this->convertValuationsToResponse($vehicle, [$valuation]);
            }
        }

        foreach ($vehicle->getDrafts() as $draft) {
            if ($draft->getBuyerCompany()->name == M4YMDataProvider::M4YM_COMPANY_NAME) {
                return $this->convertDraftsToResponse($vehicle, [$draft]);
            }
        }

        $collector = new VehicleValuationsCollector([], [new VehicleValuer(new M4YMDataProvider())]);
        $drafts = $collector->createValuationDrafts($vehicle);

        if($createValuation){
            $valuations = [];
            foreach ($drafts as $draft){
                $capApi = new CapApi();
                $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();
                $valuationGroup = Uuid::fromString($valuationGroup);
                $valuations[] = $this->vehicleValuationsCreator->createValuationFromDraft($draft, $valuationGroup, 1, $capValuation);
            }

            return $this->convertValuationsToResponse($vehicle, $valuations);
        }

        return $this->convertDraftsToResponse($vehicle, $drafts);
    }

    private function saveWBCTValuation(Vehicle $vehicle, bool $createValuation = false, $valuationGroup = '')
    {

        foreach ($vehicle->getValuations() as $valuation)
        {
            if ($valuation->getBuyerCompany()->name == WBCTDataProvider::WBCT_COMPANY_NAME) {
                return $this->convertValuationsToResponse($vehicle, [$valuation]);
            }
        }

        foreach ($vehicle->getDrafts() as $draft) {
            if ($draft->getBuyerCompany()->name == WBCTDataProvider::WBCT_COMPANY_NAME) {
                return $this->convertDraftsToResponse($vehicle, [$draft]);
            }
        }

        $collector = new VehicleValuationsCollector([], [new VehicleValuer(new WBCTDataProvider())]);
        $drafts = $collector->createValuationDrafts($vehicle);

        if($createValuation){
            $valuations = [];
            foreach ($drafts as $draft){
                $capApi = new CapApi();
                $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();
                $valuationGroup = Uuid::fromString($valuationGroup);
                $valuations[] = $this->vehicleValuationsCreator->createValuationFromDraft($draft, $valuationGroup, 1, $capValuation);
            }

            return $this->convertValuationsToResponse($vehicle, $valuations);
        }

        return $this->convertDraftsToResponse($vehicle, $drafts);
    }

    private function saveMWValuation(Vehicle $vehicle, bool $createValuation = false, $valuationGroup = '')
    {
        //return json_encode(["views" => []]); //disable MW integration
        foreach ($vehicle->getValuations() as $valuation)
        {
            if ($valuation->getBuyerCompany()->name == MWDataProvider::MW_COMPANY_NAME) {
                return $this->convertValuationsToResponse($vehicle, [$valuation]);
            }
        }

        foreach ($vehicle->getDrafts() as $draft) {
            if ($draft->getBuyerCompany()->name == MWDataProvider::MW_COMPANY_NAME) {
                return $this->convertDraftsToResponse($vehicle, [$draft]);
            }
        }

        $collector = new VehicleValuationsCollector([], [new VehicleValuer(new MWDataProvider())]);
        $drafts = $collector->createValuationDrafts($vehicle);

        if($createValuation){
            $valuations = [];
            foreach ($drafts as $draft){
                $capApi = new CapApi();
                $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();
                $valuationGroup = Uuid::fromString($valuationGroup);
                $valuations[] = $this->vehicleValuationsCreator->createValuationFromDraft($draft, $valuationGroup, 1, $capValuation);
            }

            return $this->convertValuationsToResponse($vehicle, $valuations);
        }

        return $this->convertDraftsToResponse($vehicle, $drafts);
    }

    private function saveInternalValuations(Vehicle $vehicle, bool $createValuation = false, $valuationGroup = '')
    {
        foreach ($vehicle->getValuations() as $valuation)
        {
            if ($valuation->getBuyerCompany()->company_type == 'matrix') {
                return $this->convertValuationsToResponse($vehicle, [$valuation]);
            }
        }

        $internalValuations = [];
        foreach ($vehicle->getDrafts() as $draft) {
            if ($draft->getBuyerCompany()->company_type == 'matrix') {
                $internalValuations[] = $draft;
            }
        }

        if (count($internalValuations) > 0) {
            return $this->convertDraftsToResponse($vehicle, $internalValuations);
        }

        $valuationDrafts = $vehicle->getDrafts();
        $priceEstimators= [new CapPriceEstimator()];

        foreach ($valuationDrafts as $valuationDraft) {
            switch ($valuationDraft->getCompany()->name) {
                case CTBDataProvider::CTB_COMPANY_NAME:
                    $priceEstimators[] = CTBDataProvider::getPriceEstimationsFromDraft($valuationDraft);
                    break;
                case WWACDataProvider::WWAC_COMPANY_NAME:
                    $priceEstimators[] = WWACDataProvider::getPriceEstimationsFromDraft($valuationDraft);
                    break;
                case M4YMDataProvider::M4YM_COMPANY_NAME:
                    $priceEstimators[] = M4YMDataProvider::getPriceEstimationsFromDraft($valuationDraft);
                    break;
                case WBCTDataProvider::WBCT_COMPANY_NAME:
                    $priceEstimators[] = WBCTDataProvider::getPriceEstimationsFromDraft($valuationDraft);
                    break;
            }
        }

        $valuers = [new VehicleValuer(new RulesDataProvider()), new VehicleValuer(new AutoPartnerDataProvider())];

        $collector = new VehicleValuationsCollector($priceEstimators, $valuers);
        $drafts = $collector->createValuationDrafts($vehicle);

        if($createValuation){
            $valuations = [];
            foreach ($drafts as $draft){
                $capApi = new CapApi();
                $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();
                $valuationGroup = Uuid::fromString($valuationGroup);
                $valuations[] = $this->vehicleValuationsCreator->createValuationFromDraft($draft, $valuationGroup, 1, $capValuation);
            }

            $this->vehicleValuationsCreator->collectFeeForValuation($vehicle);
            return $this->convertValuationsToResponse($vehicle, $valuations);
        }

        return $this->convertDraftsToResponse($vehicle, $drafts);
    }

    private function convertDraftsToResponse(Vehicle $vehicle, array $drafts)
    {
        $views = [];

        foreach ($drafts as $draft) {
            $renderData = ['offeredValue' => $draft, 'vehicle' => $vehicle];
            $views[] = view('quote.single-offered-value-table-row', $renderData)->render();
        }

        return new JsonResponse(['views' => $views]);
    }

    private function convertValuationsToResponse(Vehicle $vehicle, array $valuations)
    {
        $views = [];

        foreach ($valuations as $valuation) {
            $renderView = ['valuation' => $valuation, 'vehicle' => $vehicle];
            $views[] = view('quote.single-valuation-table-row', $renderView)->render();
        }

        return new JsonResponse(['views' => $views]);
    }

    /**
     * @param Vehicle $vehicle
     * @param User $user
     * @throws VehicleDoesNotExistException
     */
    private function guardAgainstUserOwnership(Vehicle $vehicle, User $user)
    {
        $vehicleOwner = VehicleOwner::where('vehicle_id', '=', $vehicle->id)->first();
        if ($vehicleOwner->user_id != $user->id) {
            throw new VehicleDoesNotExistException();
        }
    }

}
