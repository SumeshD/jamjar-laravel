<?php

namespace JamJar\Http\Controllers\Webhooks;

use Carbon\Carbon;
use Mail;
use Log;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use \GuzzleHttp\Client as HttpClient;
use JamJar\Valuation;
use JamJar\Notifications\ApiFailure;
use JamJar\Http\Controllers\Controller;
use JamJar\Mail\Money4YourMotorsAccepted;



class Money4YourMotorsController extends Controller
{
    public function show(Request $request) 
    {
        $data = $request->all();

        return view('m4ym-accept', compact('data'));
    }

    public function acceptValuation(Request $request)
    {
        $base_uri = env('M4YMv2_ENV') == 'sandbox' ? 'https://www.m4ym.com/affiliateapi-m4ym-sandbox/api' : 'https://www.m4ym.com/affiliateapi-m4ym-live/api';

        $valuationId = (int) $request->input('valuationId');
        $valuation = Valuation::where([['id', '=', $valuationId], ['user_id', '=', auth()->user()->id]])->first();

        if ($valuation->acceptance_uri) {
            return [
                'SUCCESS' => true,
                'URI' => $valuation->acceptance_uri
            ];
        }

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'ClientIdentifier' => env('M4YMv2_CLIENT_IDENTIFIER'),
                    'ClientAPIKey' => env('M4YMv2_API_KEY')
                ],
                'verify' => false
            ]
        );

        \Log::debug('M4YMv2 Webhook:'. print_r($request->all(), true));

        $dataToSend = $this->createAcceptValuationRequest($valuation);

        try {
            // Get the valuation from M4YM.
            $response = $client->post(
                "{$base_uri}/Valuation/AcceptValuation",
                [
                    'body' => json_encode($dataToSend, JSON_UNESCAPED_SLASHES),
                    'headers' => [
                        'ClientIdentifier' => env('M4YMv2_CLIENT_IDENTIFIER'),
                        'ClientAPIKey' => env('M4YMv2_API_KEY'),
                        'Content-Type' => 'application/json'
                    ]
                ]
            );

        } catch (\Exception $exception) {
            \Log::debug('M4YM Version 2 API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                        ->notify(new ApiFailure('M4YM Version 2 failure'));
            // Return false so the application doesn't crash
            return false;
        }
        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody()->getContents())->ColumnMappings;
            //Make response into nicer array
            $responseData = [];
            if(isset($response) && !empty($response)){
                foreach($response as $data){
                    $responseData[$data->Id] = $data->Value;
                }
            }
            \Log::debug('M4YM Success:'. print_r($responseData));
            $result = Valuation::where('temp_ref', $request->all()['ColumnMappings'][0]['Value'])
            ->update([
                'acceptance_uri' => $responseData['LandingPage'],
                'temp_ref' => $responseData['ValuationReference'],
                ]);
            
            if($result > 0){
                if ($valuation && !$valuation->getAcceptButtonClickedAt()) {
                    $valuation->accept_button_clicked_at = Carbon::now();
                    $valuation->save();
                }

                return [
                    'SUCCESS' => true,
                    'URI' => $responseData['LandingPage']
                ];
            } else {
                return [
                    'SUCCESS' => false
                ];
            }
        }
        
    }

    public function store(Request $request) 
    {
        Mail::to('keith@money4yourmotors.com')
            ->bcc(['info@jamjar.com'])
            ->send(new Money4YourMotorsAccepted($request->all()));
        
        return view('m4ym-accepted', ['ref' => $request->ref]);
    }

    private function createAcceptValuationRequest(Valuation $valuation): array
    {
        $vehicle = $valuation->getVehicle();

        return [
            'ColumnMappings' => [
                [
                    'Id' => 'TempReference',
                    'Value' => $valuation->temp_ref,
                ],
                [
                    'Id' => 'CustomerName',
                    'Value' => $vehicle->latest_owner->name,
                ],
                [
                    'Id' => 'CustomerEmail',
                    'Value' => $vehicle->latest_owner->email,
                ],
                [
                    'Id' => 'CustomerPhone',
                    'Value' => $vehicle->latest_owner->telephone,
                ],
                [
                    'Id' => 'CustomerPostcode',
                    'Value' => $vehicle->latest_owner->postcode,
                ],
                [
                    'Id' => 'CustomerIPAddress',
                    'Value' =>  '-',
                ],
                [
                    'Id' => 'VehicleValuation',
                    'Value' => $valuation->value_int,
                ],
            ]
        ];
    }
}
