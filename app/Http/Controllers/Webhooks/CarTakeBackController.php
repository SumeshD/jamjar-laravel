<?php

namespace JamJar\Http\Controllers\Webhooks;

use Carbon\Carbon;
use Illuminate\Http\Request;
use JamJar\Api\CarTakeBack;
use JamJar\Http\Controllers\Controller;
use JamJar\Valuation;
use JamJar\Vehicle;

class CarTakeBackController extends Controller
{
    public function updateCustomerData(Request $request)
    {
        $valuationId = (int) $request->input('valuationId');
        $valuation = Valuation::where([['id', '=', $valuationId], ['user_id', '=', auth()->user()->id]])->first();

        if (!$valuation) {
            return response()->json(['success' => false]);
        }

        if ($valuation && !$valuation->getAcceptButtonClickedAt()) {
            $valuation->accept_button_clicked_at = Carbon::now();
            $valuation->save();
        }

        if ($valuation->temp_ref == null) {
            return response()->json(['success' => true]);
        }

        $vehicle = Vehicle::where('id', '=', $valuation->vehicle_id)->first();

        $carTakeBackAPIService = new CarTakeBack();
        $carTakeBackAPIService->tryToUpdateCustomerData($valuation->temp_ref, $vehicle);

        return response()->json(['success' => true]);
    }
}
