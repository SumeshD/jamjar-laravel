<?php

namespace JamJar\Http\Controllers\Webhooks;

use Carbon\Carbon;
use JamJar\Sale;
use JamJar\Services\Valuations\ExternalDataProviders\WWACDataProvider;
use JamJar\User;
use JamJar\Vehicle;
use JamJar\Valuation;
use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;

class WebhookController extends Controller
{
    public function __construct() 
    {
        if ($this->authenticate() == false) {
            return response([
                'success' => false, 
                'error' => 'You have not provded the required parameters to access this API',
                'fields' => ['X-API-TOKEN', 'X-PARTNER-ID']
            ]);
        }

        if ($user = $this->authenticate()) {
            // No company? Go away.
            if (!$user->company) {
                abort(403);
            }
            $this->user = $user;
            $this->company = $user->company;
        }
    }
    /**
     * @api {post} /webhooks 
     * @apiName Jamjar Webhooks
     * @apiGroup Jamjar Webhooks
     * @apiVersion 1.0.0
     * @apiHeader {String} X-API-TOKEN Associates Unique API Token
     * @apiHeader {String} X-PARTNER-ID Associates Unique Partner ID
     * @apiParam {String} jj_valuation_ref The Jamjar Valuation Reference (Sent in initial request to associate to get Valuation)
     * @apiParam {String} valuation_ref The Associate's Valuation Reference
     * @apiParam {String} status The Status of the Valuation. Accepted: <code>pending</code>, <code>complete</code>, <code>rejected</code>
     * @apiParam {Float} agreed_sale_price The Final Agreed Sale Price
     * @apiParam {String} method Method of acquisition. Accepted: <code>collection</code> <code>delivery</code>
     * 
     * @apiSuccess {Boolean} success
     * @apiSuccess {Boolean} [message] Optional message
     * 
     * @apiError {Boolean} success
     * @apiError {String} error Error Message
     * @apiError {Object} [fields] Optional list of fields which are required.
     */
    public function store(Request $request) 
    {
        if (!$request->has(['jj_valuation_ref', 'valuation_ref', 'status', 'agreed_sale_price', 'method'])) {
            return response([
                'success' => false, 
                'error' => 'You have not provided the required fields to access this API',
                'fields' => ['jj_valuation_ref', 'valuation_ref', 'status', 'agreed_sale_price', 'method']
            ]);
        }

        // Get the valuation
        $valuation = $this->extractValuation($request->get('jj_valuation_ref'));

        // Create a sale
        $sale = (new Sale)->createSale(
                                $valuation->vehicle, 
                                $valuation, 
                                $this->company, 
                                [
                                    'value' => $request->agreed_sale_price, 
                                    'deliveryMethod' => $request->method
                                ]
                            );

        $sale->update([
            'final_price' => $request->agreed_sale_price,
            'dealer_notes' => $request->valuation_ref,
            'status' => $request->status
        ]);

        // Return a nice message
        return response(['success' => true, 'message' => 'Thank you for submitting your follow up for this Lead.']);
    }

    public function acceptValuation(Request $request)
    {
        $valuationId = (int) $request->valuationId;

        if (!$valuationId) {
            return response(['success' => true]);
        }

        $valuation = Valuation::where([['id', '=', $valuationId], ['user_id', '=', auth()->user()->id]])->first();

        if (!$valuation || $valuation->getAcceptButtonClickedAt()) {
            return response(['success' => true]);
        }

        if ($valuation->getBuyerCompany()->name == WWACDataProvider::WWAC_COMPANY_NAME) {
            $wwacDataProvider = new WWACDataProvider();
            $wwacDataProvider->updateCustomerData($valuation);
        }

        $valuation->accept_button_clicked_at = Carbon::now();
        $valuation->save();

        return response(['success' => true]);
    }

    protected function extractValuation($valuationReference) 
    {
        // Remove Jamjar
        $valuationReference = str_replace('Jamjar-', '', $valuationReference);
        // Only integers
        preg_match_all('!\d+!', $valuationReference, $matches);
        // Get user
        $userId = $matches[0][0];
        // Get vehicle
        $vehicleId = $matches[0][1];
        // get the valuations for this user, vehicle and company
        $valuation = Valuation::where('user_id', $userId)
                                ->where('vehicle_id', $vehicleId)
                                ->where('company_id', $this->company->id)
                                ->latest()
                                ->first();
        // return
        return $valuation;
    }

    /**
     * Authenticate the incoming request
     * 
     * @return boolean|JamJar\User
     */
    protected function authenticate() 
    {
        $token = request()->header('X-API-TOKEN');
        $partnerId = request()->header('X-PARTNER-ID');
        
        if ((!$token) || (!$partnerId)) {
            return false;
        }

        $user = User::where('api_partner_id', $partnerId)->firstOrFail();

        if ($user->api_token !== $token) {
            return false;
        }

        return $user;
    }

}
