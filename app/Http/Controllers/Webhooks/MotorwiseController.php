<?php

namespace JamJar\Http\Controllers\Webhooks;

use Carbon\Carbon;
use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\Model\ValuationDraft;
use JamJar\Sale;
use JamJar\Services\Integrations\MotorwiseIntegration;
use JamJar\Valuation;
use JamJar\Vehicle;

class MotorwiseController extends Controller{
    public function createNewSale(Request $request, MotorwiseIntegration $mwIntegration){
        $valuationId = (int) $request->input('valuationId');
        $valuation = Valuation::where([['id', '=', $valuationId], ['user_id', '=', auth()->user()->id]])->first();
        $valuationDraft = ValuationDraft::where('valuation_id', $valuationId)->first();
        if (!$valuation) {
            return response()->json(['success' => false]);
        }

        if ($valuation && !$valuation->getAcceptButtonClickedAt()) {
            $valuation->accept_button_clicked_at = Carbon::now();
            $valuation->save();
        }
        if ($valuation->temp_ref == null) {
            return response()->json(['success' => true]);
        }

        $previousSale = Sale::where([['valuation_id', '=', $valuation->id], ['company_id', '=', $valuation->company->id]])->first();
        if ($previousSale) {
            alert()->error('Sale for this valuation has been created already. Check your email box.');
            return response()->json(['success' => false]);
        }

        $mwIntegration->acceptQuote($valuation->temp_ref);

        $sale = (new Sale)->createSale($valuation->getVehicle(), $valuation, $valuation->company, ['value' => $valuationDraft->price_in_pence/100, 'deliveryMethod' => 'collection']);
        return response()->json(['success' => true]);
    }
}