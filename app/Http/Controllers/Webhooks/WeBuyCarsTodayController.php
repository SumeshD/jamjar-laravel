<?php

namespace JamJar\Http\Controllers\Webhooks;

use Carbon\Carbon;
use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;
use JamJar\Valuation;
use JamJar\Sale;
use JamJar\Notifications\ApiFailure;


class WeBuyCarsTodayController extends Controller
{
    public $key;
    public $username;
    public $password;

    public function __construct($key = null, $username = null, $password = null)
    {
        $this->key = env('WBCT_ENV') == 'live' ?
            'live:3,_3VO=M!>2LpnAjHcShYI]V]|VzeuYom|FB<vIaeg<^7#PrR<w!{P+3ib42Gk$' :
            'dev:UT7tAiR]y7nChw7&?sdJ?n5hu{9GDhjW+x?+5d[_Bk?Zy!Yu`a/CrgxYKLC.q?!';
        $this->username = env('WBCT_USERNAME');
        $this->password = env('WBCT_PASSWORD');
    }
    
    public function acceptValuation(Request $request)
    {
        $valuationId = (int) $request->input('valuationId');
        $valuation = Valuation::where([['id', '=', $valuationId], ['user_id', '=', auth()->user()->id]])->first();

        if ($valuation->acceptance_uri) {
            return [
                'SUCCESS' => true,
                'URI' => $valuation->acceptance_uri
            ];
        }

        // Build our client
        $client = new HttpClient([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ]);

        try {
            $response = $client->post(
                'https://www.webuycarstoday.co.uk/wbct-api/linear_services/register_quote',
                [
                    'form_params' => [
                        'key' => $this->key,
                        'username' => env('WBCT_USERNAME', 'JamJar.com'),
                        'password' => env('WBCT_PASSWORD', 'jamjar1234'),
                        'registration_id' => $request->registration_id,
                        'fullname' => $request->fullname,
                        'email' => $request->email,
                        'contact_no' => $request->contact_no,
                        'postcode' => $request->postcode
                    ]
                ]
            );
            $body = $response->getBody()->getContents();
            $body = json_decode($body, true);


        } catch (\Exception $exception) {
            \Log::debug('WBCT Version 2 API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                        ->notify(new ApiFailure('WBCT failure'));
            // Return false so the application doesn't crash
            return false;
        }

        if($response->getStatusCode() == 200){  
           
            $result = Valuation::where('temp_ref', $request->registration_id)->update([
                'acceptance_uri' => 'https://www.webuycarstoday.co.uk/offer/'.$request->vrm.'/'.$body['data']['quote_no'].'/secure',
                'temp_ref' => $body['data']['quote_no']
            ]);
            if($result > 0){
                if ($valuation && !$valuation->getAcceptButtonClickedAt()) {
                    $valuation->accept_button_clicked_at = Carbon::now();
                    $valuation->save();
                }

                return [
                    'SUCCESS' => true,
                    'URI' => 'https://www.webuycarstoday.co.uk/offer/'.$request->vrm.'/'.$body['data']['quote_no'].'/secure'
                ];
            } else {
                return [
                    'SUCCESS' => false
                ];
            }
        }
    }
    
    public function store(Request $request)
    {
     	\Log::debug("WBCT Webhook hit");
        if (($request->quotes) && ($request->quote_status_definition)) {
            $status = $request->quote_status_definition;
         foreach ($request->quotes as $quote) {
                if($quote['status'] == "1"){
                \Log::debug("WBCT Vehicle purchased ". $quote['quote_id'] . "with status: ". $quote['status']);

                $valuation = Valuation::where([
                        'temp_ref' => $quote['quote_id']
                    ])->with(['company', 'vehicle'])
                    ->first();

                    $result = Valuation::where([
                        'temp_ref' => $quote['quote_id']
                    ])->update(['accepted_at' => $quote['updatedon']]);

                    if($result == 0){
                        \Log::debug("Couldn't update quote: ". $quote['quote_id'] . "with status: ". $quote['status']);
                    } else {
                        if($valuation->company->name == 'WeBuyCarsToday'){
                            // Create a sale
                            $sale = (new Sale)->createSale(
                                $valuation->vehicle,
                                $valuation,
                                $valuation->company,
                                [
                                    'value' => $valuation->getOriginal('collection_value'),
                                    'deliveryMethod' => 'collection',
                                    'dealer_notes' => $quote['quote_id']
                                ]
                            );

                            $sale->status = 'complete';
                            $sale->save();

                            \Log::debug("Sale created for ". $quote['quote_id'] . "with status: ". $quote['status']);
                        }
                    }
                }
            }
	}

	return response()->json(['received' => true]);
    }
}
