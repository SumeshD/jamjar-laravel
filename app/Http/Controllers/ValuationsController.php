<?php

namespace JamJar\Http\Controllers;

use JamJar\Events\SaleHasBeenRemoved;
use JamJar\Exceptions\VehicleDoesNotExistException;
use JamJar\Sale;
use Carbon\Carbon;
use JamJar\Company;
use JamJar\Profile;
use JamJar\Vehicle;
use JamJar\Valuation;
use Illuminate\Http\Request;
use JamJar\Api\TextMarketer;
use JamJar\Events\SaleCreated;
use Illuminate\Support\Facades\Redirect;
use JamJar\Services\Valuations\ExternalDataProviders\MWDataProvider;


class ValuationsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isPartner()) {
            return redirect()->route('partnerDashboard');
        }

        $offers = Vehicle::getTopVehicleOffersForClient(auth()->id());
        return view('valuations.index', compact('offers'));
    }

    public function cancelValuation(Request $request)
    {

        $sale = Sale::where('valuation_id', '=', $request->vid)->first();

        $sale->status = 'rejected';
        $sale->save();

        $vehicle = Vehicle::where('id', '=', $request->vehicle_id)->first();

        $oldVehicle = clone $vehicle;
        $oldVehicle->meta = clone $vehicle->meta;

        $valuation = Valuation::getByVehicle($vehicle);

        event(new SaleHasBeenRemoved($sale->company->user, $vehicle->owner, $sale, $vehicle, $oldVehicle, $valuation, true));

        alert()->info('Your offer has been cancelled');

        return redirect()->route('showSavedValuation', $request->uuid);

    }

    public function show($uuid, $valuationId = '')
    {
        if($valuationId){
            $valuations = Valuation::where('id', $valuationId)->get();
        } else {
            $valuations = Valuation::where('uuid', $uuid)
                ->with('company', 'company.website')
                ->orderBy('valuations.collection_value', 'DESC')
                ->orderBy('valuations.dropoff_value', 'DESC')
                ->get()
                ->unique('company_id');
        }

        if (count($valuations) == 0) {
            return redirect(route('dashboard'));
        }

        $vehicle = $valuations->first()->vehicle;

        if ($vehicle->owner->user_id != auth()->user()->id) {
            throw new VehicleDoesNotExistException();
        }



        $hasExpired = false;

        $hasComplete = $vehicle->hasCompleteSale();
        $hasPending = $vehicle->hasPendingSale();
        $hasRejected = $vehicle->hasRejectedSale();


        foreach ($valuations as $key=>$valuation) {
            $expires = $valuation->created_at->addDay($valuation->company->valuation_validity);
            if (now()->gte($expires)) {
                $hasExpired = true;
            }

            $valuation->complete = $hasComplete;
            $valuation->pending = $valuation->hasPendingSale();
            $valuation->rejected = $valuation->hasRejectedSale();
            $valuation->pendingOverall = $hasPending;

            //Separate Collection and Dropoff for CarTakeBack
            if ($valuation->company->name == 'CarTakeBack' && $valuation->isCollectionAvailable() && $valuation->isDropOffAvailable()) {

                $valuation->collection_part = true;
                $valuation->dropoff_part = false;
                $valuation->scrap = true;
                $valuation->collection_new_price = $valuation->collection_value;

                $oldValuation = clone $valuation;
                $oldValuation->scrap = true;
                $oldValuation->dropoff_part = true;
                $oldValuation->collection_part = false;
                $oldValuation->dropoff_new_price = $oldValuation->dropoff_value;

                $valuations->push($oldValuation);



            }else{
                $valuation->scrap = false;
            }

        }
        // Sort the valuations by descending order again
        $valuations = $valuations->sortByDesc(
            function ($valuation) use ($vehicle) {
                // sort by the highest singular value
                if ($valuation->getOriginal('dropoff_value') > $valuation->getOriginal('collection_value')) {
                    $value = $valuation->getOriginal('dropoff_value');
                } else {
                    $value = $valuation->getOriginal('collection_value');
                }

                //Reorder for CarTakeBack
                if($valuation->dropoff_part) {
                    $value = $valuation->getOriginal('dropoff_value');
                }

                if($valuation->collection_part) {
                    $value = $valuation->getOriginal('collection_value');
                }

                // Shortcut the $location variable
                $location = $valuation->location ?? $valuation->company->location->first();
                // Build a valuation collection
                $dropoff = $valuation->getOriginal('dropoff_value') ?? null;
                $collection = $valuation->getOriginal('collection_value') ?? null;

                $universalFees = $valuation->company->bank_transfer_fee + $valuation->company->admin_fee;

                $calculatedValues = [
                    'collection_value' => $collection ? $collection - (int)$location->collection_fee - $universalFees : null,
                    'dropoff_value' => $dropoff ? $dropoff - (int)$location->dropoff_fee - $universalFees : null,
                ];

                $_valuation = array_merge(['uuid' => $valuation->uuid], $calculatedValues);

                foreach (['good', 'fair', 'poor'] as $offerQuality) {
                    $fieldName = 'vehicle_condition_value_' . $offerQuality;
                    $_valuation[$offerQuality] = [
                        'collection_value' => ($collection && $valuation->$fieldName) ? $valuation->$fieldName - (int)$location->collection_fee - $universalFees : null,
                        'dropoff_value' => ($dropoff && $valuation->$fieldName) ? $valuation->$fieldName - (int)$location->dropoff_fee - $universalFees : null,
                    ];
                }

                // put it all in the cache for later access
                $cacheKey = 'vrm-'.$vehicle->numberplate.'-company-'.$valuation->company->id;
                cache([ $cacheKey => $_valuation], Carbon::now()->addMonths(1));

                return $value;
            }
        );

        $isSaved = true;

        if ($hasRejected) {

            $valuations = $valuations->sortBy('rejected');
        }

        
        return view('valuations.show', compact('uuid', 'valuations', 'vehicle', 'hasExpired', 'isSaved', 'valuationId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Vehicle $vehicle)
    {
        $valuation = new Valuation(['value' => $request->value, 'user_id' => auth()->user()->id]);
        $vehicle->valuations()->save($valuation);
        
        return redirect(route('dashboard'));
    }

    /**
     * Update a Valuation and Create a Sale.
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\Vehicle          $vrm
     * @param  JamJar\Company          $company
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vrm, Company $company)
    {
        if($company->name === MWDataProvider::MW_COMPANY_NAME) {
            $acceptedValuation = (int)$request->price;
            $valuation = Valuation::find(decrypt($request->vid));
            $vrm = $valuation->vehicle;
            if (!$valuation) {
                alert()->error('We\'re Sorry, Something went wrong. Please try again.');
                return back();
            }
            $previousSale = Sale::where([['valuation_id', '=', $valuation->id], ['company_id', '=', $company->id]])->first();
            if ($previousSale) {
                alert()->error('Sale for this valuation has been created already. Check your email box.');
                return back();
            }
            $sale = (new Sale)->createSale($vrm, $valuation, $company, ['value' => $acceptedValuation]);

        }
        $this->validate($request, [
            'keys-count' => 'required|numeric|min:1|max:3',
            'service-history-information' => 'string|max:255|nullable',
            'additional-specification-information' => 'string|max:255|nullable',
        ]);

        $acceptedValuation = (int)$request->price;
        $valuation = Valuation::find(decrypt($request->vid));

        // there is a problem with several vehicles with the same numberplate
        // so we take vehicle directly from valuation for now
        $vrm = $valuation->vehicle;


        $deliveryMethod = $this->getDeliveryMethod($request->deliveryMethod);

        // $value = $this->getValueFromDatabase($acceptedValuation, $valuation, $deliveryMethod);

        if (!$valuation) {
            alert()->error('We\'re Sorry, Something went wrong. Please try again.');
            return back();
        }


        $availableConditions = ['great'];

        foreach (['good', 'fair', 'poor'] as $condition) {
            $fieldName = 'vehicle_condition_value_' . $condition;
            if ($valuation->$fieldName > 0) {
                $availableConditions[] = $condition;
            }
        }

        if (!in_array($request->condition, $availableConditions)) {
            alert()->error('Selected condition is not available.');
            return back();
        }

        $previousSale = Sale::where([['valuation_id', '=', $valuation->id], ['company_id', '=', $company->id]])->first();

        if ($previousSale) {
            alert()->error('Sale for this valuation has been created already. Check your email box.');
            return back();
        }

        // create a sale
        $sale = (new Sale)->createSale($vrm, $valuation, $company, ['value' => $acceptedValuation, 'deliveryMethod' => $deliveryMethod, 'condition' => $request->condition, 'condition-information' => $request->input('condition-information')]);
        // get the profile

        $profile = Profile::where('user_id', $sale->user_id)->first();
        // update the profile with the request information just submitted (address, etc)
        $profile->update($request->except('email'));

        $vehicle = Vehicle::where('id',$vrm->id)->first();
        $vehicle->keys_count = $request->input('keys-count');
        $vehicle->service_history_information = $request->input('service-history-information');
        $vehicle->additional_specification_information = $request->input('additional-specification-information');
        $vehicle->save();
        /**
         * SMS Messages
         */

        $smsContentsToPartner = "{$profile->name} has agreed to your offer for {$vrm->meta->formatted_name} of {$acceptedValuation} GBP, {$deliveryMethod}. Click ". route('partnerShowSale', $sale);

        $smsContentsToUser = "Thanks for choosing our associate {$company->name} ({$company->telephone}). They have valued your vehicle at {$acceptedValuation} GBP, {$deliveryMethod}. They will contact you within 3 working hours. Thanks for using Jamjar";

        (new TextMarketer)->sendSms($valuation->company->user->profile->mobile_number, 'jamjar.com', $smsContentsToPartner);
        (new TextMarketer)->sendSms($profile->mobile_number, 'jamjar.com', $smsContentsToUser);

        /**
         * Email Notifications
         */
        event(new SaleCreated($sale));

        session(['got-valuation-for-'.$vrm->numberplate.'-from-company-'.$company->id => true]);

        alert()->success('Thanks for choosing '.$company->name.'. They will contact you within 3 working hours to arrange payment and handover.')->persistent("Great!");
        
        return redirect()->route('partnerWebsitePage', [$company->website->slug, 'offer-complete']);
    }

    /**
     * Get the Delivery Method for the Sale
     *
     * @param  string $deliveryMethod
     * @return string
     */
    protected function getDeliveryMethod($deliveryMethod)
    {
        return $deliveryMethod;
    }

    /**
     * Check our provided valuation hasn't been tampered with
     *
     * @param      int               $acceptedValuation
     * @param      \JamJar\Valuation $valuation
     * @param      string            $deliveryMethod
     * @return     int|Illuminate\Http\Response
     * @deprecated
     */
    protected function getValueFromDatabase($acceptedValuation, $valuation, $deliveryMethod)
    {
        if ($deliveryMethod == 'collection') {
            $value = $valuation->getOriginal('collection_value');
            if ($acceptedValuation !== $value) {
                alert()->error('We\'re sorry, Something went wrong. Please try again. (ERR001)');
                return back();
            }
            return $value;
        }

        if ($deliveryMethod == 'dropoff') {
            $value = $valuation->getOriginal('dropoff_value');
            if ($acceptedValuation !== $value) {
                alert()->error('We\'re sorry, Something went wrong. Please try again. (ERR002)');
                return back();
            }
            return $value;
        }
    }

    public function acceptGoodNewsLead(Valuation $valuation)
    {
        if (!$valuation) {
            alert()->error('We\'re Sorry, Something went wrong. Please try again.');
            return back();
        }

        if ($valuation->getOriginal('dropoff_value') == 0) {
            $acceptedValuation = $valuation->getOriginal('collection_value');
            $deliveryMethod = 'collection';
        } else {
            $acceptedValuation = $valuation->getOriginal('dropoff_value');
            $deliveryMethod = 'dropoff';
        }

        $sale = (new Sale)->createSale($valuation->vehicle, $valuation, $valuation->company, ['value' => $acceptedValuation, 'deliveryMethod' => $deliveryMethod]);

        session(['got-valuation-for-'.$valuation->vehicle->meta->numberplate.'-from-company-'.$valuation->company->id => true]);

        alert()->success('Thanks for choosing '.$valuation->company->name.'. They will contact you within 3 working hours to arrange payment and handover.')->persistent("Great!");

        // fire event which sends email to partner on sale agreed
        event(new SaleCreated($sale));

        return redirect(route('partnerWebsitePage', [$valuation->company->website->slug, 'home']));
    }
}
