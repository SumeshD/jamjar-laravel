<?php

namespace JamJar\Http\Controllers\Matrix;

use JamJar\Events\PartnerApproved;
use JamJar\Events\PartnerDenied;
use JamJar\User;
use Newsletter;

class Controller extends \JamJar\Http\Controllers\Controller
{
    /**
     * Approve a Matrix Partner
     *
     * @param  \JamJar\User $user
     * @return \Illuminate\Http\Response
     */
    public function approve(User $user)
    {
        if ($user) {
            // if (auth()->check()) {
            event(new PartnerApproved($user));

            $user->matrix_partner = 1;
            $user->save();

            $name = $this->formatName($user->name);

            $mergeFields = [
                'MMERGE1' => $name['firstname'],
                'MMERGE2' => $name['lastname'],
                'MMERGE3' =>
                    $user->profile->address_line_one . ' ' .
                    $user->profile->address_line_two . ' ' .
                    $user->profile->postcode . ' ' .
                    $user->profile->city . ' ' .
                    $user->profile->county,
                'MMERGE4' => $user->email,
                'MMERGE6' => $user->profile->telephone_number,
                'MMERGE9' => $user->profile->postcode,
            ];

            Newsletter::subscribe($user->email, $mergeFields, 'matrix_subscribers');

            alert()
                    ->success($user->company->name . ' represented by ' . $user->profile->name . ' has just been approved as a Matrix Partner')
                    ->persistent('Close');

            return redirect(route('/'));
            // }

            // if (\UrlSigner::validate(request()->fullUrl())) {
            // } else {
            //     alert()->error('Sorry, this link has expired.');
            //     return redirect(route('/'));
            // }
        }
    }

    /**
     * Deny a Matrix Partner
     *
     * @param  \JamJar\User $user
     * @return \Illuminate\Http\Response
     */
    public function deny(User $user)
    {
        if ($user) {
            // if (\UrlSigner::validate(request()->fullUrl())) {
            event(new PartnerDenied($user));

            alert()
                    ->info($user->profile->company_name . ' has been denied as a Matrix Partner.')
                    ->persistent('Close');

            return redirect(route('/'));
            // } else {
            //     alert()->error('Sorry, this link has expired.');
            //     return redirect(route('/'));
            // }
        }
    }


    /**
     * Format the given name.
     *
     * @param  string $name
     * @return array
     */
    private function formatName($name)
    {
        $fullname = explode(' ', $name);

        if (array_key_exists(1, $fullname)) {
            $firstname = $fullname[0];
            $lastname = $fullname[1];
        } else {
            $firstname = $name;
            $lastname = ' ';
        }

        return compact('firstname', 'lastname');
    }
}
