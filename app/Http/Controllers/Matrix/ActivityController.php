<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Http\Request;
use JamJar\Order;
use JamJar\Valuation;

class ActivityController extends Controller
{
    public function index()
    {
        $statements = auth()->user()->statements->sortByDesc('created_at');

        return view('matrix.statements.index', compact('statements'));
    }
}
