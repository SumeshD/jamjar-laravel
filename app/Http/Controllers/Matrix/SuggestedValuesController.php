<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Http\Request;
use JamJar\Exceptions\NotAllowedException;
use JamJar\Http\Controllers\Controller;
use JamJar\Offer;
use JamJar\OfferDerivative;
use JamJar\OfferModel;
use JamJar\SuggestedValue;
use JamJar\VehicleDerivative;
use JamJar\VehicleModel;


class SuggestedValuesController extends Controller
{

    public function index($offerSlug) {
        if (auth()->user()->profile->is_scrap_dealer) {
            alert()->error('Page not found')->persistent();
            throw new NotAllowedException();
        }

        /** @var Offer $offer */
        $offer = Offer::where('slug',$offerSlug)->first();
        $this->guardAgainstOwnership($offer);
        $manufacturerName = $offer->manufacturer()->first()->name;
        $fuelType = $offer->fuel_type;
        $offerName = $offer->name;
        /** @var OfferModel $offerModel */
        $offerModel = OfferModel::where('offer_id', $offer->id)->first();

        $modelList = $offerModel->getModelIdsAndDerivativesCountForOffer();
        $modelList = array_map(function($model) use ($offer){
            $modelArray = [];
            $modelArray['cap_model_id'] = $model->cap_model_id;
            $modelArray['name'] = $model->name;
            $modelArray['derivative_count'] = $model->derivative_count;
            $modelArray['url'] = route('suggested-values.get-data-for-model',[
                'offerId' => $offer->id,
                'capModelId' => $model->cap_model_id
            ]);
            return $modelArray;
        }, $modelList);
        return view('matrix.suggested-values.index', [
            'modelList' => json_encode($modelList),
            'offerId' => $offer->id,
            'manufacturerName' => $manufacturerName,
            'fuelType' => $fuelType,
            'offerName' => $offerName,
            'offerCount' => $offer->getSuggestedValuesCountForOffer()[0]->derivative_count,
            'offerSlug' => $offer->slug
        ]);

    }

    public function updateSuggestedValuesModel($offerSlug, Request $request) {

        $offer = Offer::where('slug',$offerSlug)->first();

        $this->guardAgainstOwnership($offer);

        $checkOffer = Offer::where('id',$offer->id)->first();

        if(!$checkOffer) {
            alert()->error('Offer doesnt exist!')->persistent();
            throw new NotAllowedException();
        }

        $derivatives = OfferDerivative::where('offer_id',$offer->id)->get();

        if (!$derivatives) {
            alert()->error('Error saving suggested values')->persistent();
            throw new NotAllowedException();
        }

        foreach ($derivatives as $derivative) {

            if(($derivative->base_value != $offer->base_value) || ($derivative->getSuggestedValue())) {

                if ($derivative->getSuggestedValue()) {

                    if ($derivative->getSuggestedValue()->percentage) {

                        $derivativeModel = OfferDerivative::where('id', $derivative->id)->first();
                        $derivativeModel->update([
                            'base_value' => (int)$derivative->getSuggestedValue()->percentage
                        ]);
                    }
                }

            }

        }

        alert()->success('Your Jamjar Suggested Values have been updated!');

        if(!isset($request->redirect_rule)) {

            return redirect()->route('partnerOffers');
        }

        return redirect()->route('partnerOfferSuggestedValues',[$offerSlug]);

    }

    public function updateAllSuggestedValues() {

        $offers = Offer::with(['manufacturer', 'models', 'derivatives', 'derivatives.derivative'])
            ->where('user_id', auth()->id())
            ->where('finished', 1)
            ->where('active',1)
            ->get();

        foreach($offers as $offer) {

            if(isset($offer->models) && !$offer->models->isEmpty()) {

                foreach($offer->models as $model) {

                    if($model) {

                        if($model->model->manufacturer) {

                            foreach($model->derivatives as $derivative) {

                                if (($derivative->base_value != $offer->base_value) ||  ($derivative->getSuggestedValue())) {

                                    if ($derivative->getSuggestedValue()) {

                                        if ($derivative->getSuggestedValue()->percentage) {

                                            $derivativeModel = OfferDerivative::where('id', $derivative->id)->first();
                                            $derivativeModel->update([
                                                'base_value' => (int)$derivative->getSuggestedValue()->percentage
                                            ]);

                                            cache()->forget('derivatives-for-model-'.$derivativeModel->cap_model_id.'-and-user-'.$offer->user_id.'-and-offer-'.$offer->id);
                                        }
                                    }

                                }

                            }
                        }
                    }
                }

            }

        }

        alert()->success('Your Jamjar Suggested Values have been updated!');
        return redirect()->route('partnerOffers');

    }


    public function updateSuggestedValue($offerSlug, $derivativeId, $value, Request $request) {

        $offer = Offer::where('slug',$offerSlug)->first();

        $this->guardAgainstOwnership($offer);

        $derivativeModel = OfferDerivative::where('id', $derivativeId)->first();
        $derivativeModel->update([
            'base_value' => (int)$value
        ]);

        cache()->forget('derivatives-for-model-'.$derivativeModel->cap_model_id.'-and-user-'.$offer->user_id.'-and-offer-'.$offer->id);

        alert()->success('You have successfully accepted Jamjar Suggested Value!');

        return redirect()->route('suggestedValues',$offerSlug);

    }

    public function updateSuggestedValueManual($offerSlug, $derivativeId, Request $request) {

        $offer = Offer::where('slug',$offerSlug)->first();

        $this->guardAgainstOwnership($offer);

        $derivativeModel = OfferDerivative::where('id', $derivativeId)->first();
        $derivativeModel->update([
            'base_value' => (int)$request->value
        ]);

        cache()->forget('derivatives-for-model-'.$derivativeModel->cap_model_id.'-and-user-'.$offer->user_id.'-and-offer-'.$offer->id);

        alert()->success('You have successfully updated the value for this rule!');

        return redirect()->route('suggestedValues', $offerSlug);

    }

    private function guardAgainstOwnership(Offer $offer)
    {
        if (auth()->user()->id != $offer->user->id) {
            alert()->error('Page not found')->persistent();
            throw new NotAllowedException();
        }
    }

}
