<?php

namespace JamJar\Http\Controllers\Matrix;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use JamJar\Http\Controllers\Controller;
use JamJar\PartnerLocation;
use JamJar\PartnerPage;
use JamJar\PartnerWebsite;
use JamJar\Sale;
use JamJar\Valuation;
use JamJar\Vehicle;

class PartnerPageController extends Controller
{
    /**
     * Display the requested resource
     *
     * @param  JamJar\PartnerWebsite $website
     * @param  string                $slug
     * @return Illuminate\View\View
     */
    public function show(PartnerWebsite $website, $slug)
    {
        $page = $website->getPage($slug);

        if ($slug == 'your-offer') {
            return $this->showOfferPage($website, $page);
        }

        if ($slug == 'offer-complete') {
            return $this->showCompletePage($website, $page);
        }
        
        if (!view()->exists('matrix.website.pages.'.$page->slug)) {
            return view('matrix.website.pages.page', compact(['website', 'page']));
        }

        return view('matrix.website.pages.'.$page->slug, compact(['website', 'page']));
    }

    /**
     * Update the Partner Page.
     *
     * @param  JamJar\PartnerWebsite $website
     * @param  string                $slug
     * @return Illuminate\Http\Response
     */
    public function update(PartnerWebsite $website, $slug, Request $request)
    {
        $page = $website->getPage($slug);

        $this->validate(
            $request,
            [
                'content' => ['required']
            ]
        );

        $page->update(
            [
            'title' => $request->title ?? $page->title,
            'content' => $request->content ?? $page->content,
            'meta'  => serialize($request->openingtimes) ?? $page->meta,
            'active' => (bool)$request->onoffswitch
            ]
        );

        alert()->success($page->title . ' page was updated');

        return back();
    }

    /**
     * Show the "Offer Complete" special page
     *
     * @param  JamJar\PartnerWebsite $website
     * @param  JamJar\PartnerPage    $page
     * @return Illuminate\View\View
     */
    public function showCompletePage(PartnerWebsite $website, PartnerPage $page)
    {
        $vrm = session('vrm');
        $companyId = session('company_id');
        $vid = session('vid');

        // Load our valuation from laravel's internal cache
        $valuation = cache('vrm-'.$vrm.'-company-'.$companyId);
//        $valuation['id'] = cache($vrm . '-company-'.$companyId.'-valuation-id');

        if (!$valuation) {
            $valuationId = (int) Input::get('valuationId');

            if ($valuationId) {
                $valuation = Valuation::where('id', '=', $valuationId)->first();
            }
        }

        // Can't fetch valuation from cache, time to get it from the ID
        if (empty($valuation['id'])) {
            $valuation['id'] = $vid;
        }

        $valuation = Valuation::find($valuation['id']);

        // no valuations, no problems
        if ((!$valuation) || (!$valuation->sale)) {
            alert()->error('We couldn\'t find your valuation. Please try again (Code: 0001)');
            return redirect(route('partnerWebsitePage', [$website, 'home']));
        }
        // Load our vehicle information with relationships
        $vehicle = Vehicle::where('id', $valuation->vehicle_id)->latest()->first();
        $vehicle->load(['meta', 'owner']);

        $sale = Sale::where([
            ['valuation_id', '=', $valuation->id],
            ['user_id', '=', auth()->user()->id],
        ])->first();

        // Load our partner locations
        $locations = PartnerLocation::where('company_id', $companyId)->get();
        return view('matrix.website.pages.offer-complete', compact(['website', 'page', 'valuation', 'vehicle', 'locations', 'sale']));
    }

    /**
     * Show the "Your Offer" special page
     * 
     * When the customer hits "See Details" on the Quote Screen, this method is hit by both GET and PATCH.
     * This page is hit first by a PATCH request which sets the VRM, Company ID, UUID and Valuation ID.
     * The user is then redirected through a Javascript promise which is the GET request
     * This necessitates storing the information in the session for a short time
     * We use this later on in the method so it's assigned to shortcut 
     * variables no matter what the request type is.
     *
     * @param  JamJar\PartnerWebsite $website
     * @param  JamJar\PartnerPage    $page
     * @return Illuminate\View\View
     */
    public function showOfferPage(PartnerWebsite $website, PartnerPage $page)
    {
        // Check if it's GET or PATCH
        if (request()->getMethod() == 'GET') {
            // If it's GET we need to get the info from the session
            $vrm = session()->get('vrm');
            $companyId = session()->get('company_id');
            $uuid = session()->get('uuid');
            $vid = session()->get('vid');
        } else {
            // If it's PATCH we need to get the info from the request
            $vrm = request()->vrm;
            $companyId = request()->company_id;
            $uuid = request()->uuid;
            $vid = request()->vid;
            // annnnd store it in the session for GET to use later
            session([
                'vrm' => request()->vrm,
                'company_id' => request()->company_id,
                'uuid' => request()->uuid,
                'vid' => request()->vid,
            ]);
        }

        // Load our valuation from laravel's internal cache
        $valuation = cache('vrm-'.$vrm.'-company-'.$companyId);
//        $valuation['id'] = cache($vrm . '-company-'.$companyId.'-valuation-id');
        if (empty($valuation['id'])) {
            $valuation['id'] = $vid;
        }



        // write to session variable to load in footer of matrix site
        session(['valuation' => $valuation]);

        // no valuations, no problems
        if (!$valuation) {
            alert()->error('We couldn\'t find your valuation. Please try again (Code: 0002)');
            return redirect(route('partnerWebsitePage', [$website, 'home']));
        }

        $dbValuation = Valuation::where('id', '=', $valuation['id'])->first();

        if ($dbValuation) {
            if (!$dbValuation->accept_button_clicked_at) {
                $dbValuation->accept_button_clicked_at = Carbon::now();
                $dbValuation->save();
            }
            $vehicle = Vehicle::where('id', '=', $dbValuation->vehicle_id)->first();
        } else {
            $vehicle = Vehicle::where('numberplate', $vrm)->first();
        }

        $vehicle->load(['meta', 'owner']);

        // Load our partner locations
        $locations = PartnerLocation::where('company_id', $companyId)->get();

        // Do we want JSON?
        if (request()->expectsJson()) {
            return response(['success' => true, 'redirect_uri' => route('partnerWebsitePage', [$website,'your-offer'])], 200);
        }

        $sellerUser = auth()->user();

        // return a view
        return view('matrix.website.pages.your-offer', compact(['sellerUser', 'website', 'page', 'valuation', 'vehicle', 'locations', 'dbValuation']));
    }
}
