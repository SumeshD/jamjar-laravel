<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use JamJar\Events\NewValuationOffered;
use JamJar\Exceptions\InsufficientFundsException;
use JamJar\Exceptions\VehicleExpiredException;
use JamJar\Mail\PartnerHasBeenOutbidEmail;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Profile;
use JamJar\Rules\ValueGreaterThan;
use JamJar\Rules\ValueLowerOrEqual;
use JamJar\Rules\ValueLowerThan;
use JamJar\UserStatement;
use JamJar\Valuation;
use JamJar\Vehicle;

class ValuationsController extends Controller
{
    /**
     * Instantiate a new instance of Matrix\ValuationsController
     */
    public function __construct()
    {
        $this->middleware(['partner', 'auth']);
    }

    /**
     * Display a list of Valuations
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return view('matrix.valuations.index');
    }

    public function show(Valuation $valuation)
    {
        $this->guardAgainstOwnership($valuation);

        $paidLead = Valuation::getPaidLeadsByValuations([$valuation])[0];
        $proposedPrice = $paidLead->highestValuation->getValueIntAttribute() + LeadsController::BASE_PROPOSED_PRICE_ADVANTAGE_IN_POUNDS;

        $calculator = new PartnerFeeCalculator(2, $proposedPrice);
        $newFee = $calculator->calculateForGoodNewsLead(true, auth()->user()->company, false);

        if($valuation->is_created_by_automated_bids) {

            $statement = UserStatement::where('meta','like','%"id":'.$valuation->id.'%')->where('user_id',auth()->user()->id)->first();

            if($statement) {
                $oldFee = $statement->amount*100;
            } else {
                $oldFee = 0;
            }
        } else {

            $oldFee = (int) round($valuation->fee / PartnerFeeCalculator::getVATMultiplier());
        }

        $additionalFee = $newFee - $oldFee;

        $additionalFee = $additionalFee < 0 ? 0 : $additionalFee;
        $oldFee = $oldFee < 0 ? 0 : $oldFee;

        $highestMatrixValuation = Valuation::getHighestByUser($valuation->vehicle, auth()->user());
        $showUpdateButton = true;

        return view('matrix.valuations.show', compact('valuation', 'paidLead', 'additionalFee', 'proposedPrice', 'oldFee', 'highestMatrixValuation', 'showUpdateButton'));
    }

    public function submitHigherValueForValuation(Request $request, Valuation $valuation)
    {
        $this->guardAgainstOwnership($valuation);

        $paidLead = Valuation::getPaidLeadsByValuations([$valuation])[0];

        $this->validateLead($request, $paidLead->highestValuation);

        $newValue = $request->valueGreat;

        $calculator = new PartnerFeeCalculator(2, $newValue);
        $newFee = $calculator->calculateForGoodNewsLead(true, auth()->user()->company);

        $this->guardAgainstInsufficientFounds($newFee / 100);

        $oldFee = $valuation->fee;
        $additionalFee = $newFee - $oldFee;

        if ($valuation->getOriginal('collection_value') && $valuation->getOriginal('dropoff_value')) {
            $valuation->collection_value = $valuation->dropoff_value = $newValue;
        } else {
            $valueFieldName = strtolower($valuation->value_type) . '_value';
            $valuation->$valueFieldName = $newValue;
        }

        $valuation->vehicle_condition_value_good = ($request->valueGood && $request->valueGood > 0) ? $request->valueGood : null;
        $valuation->vehicle_condition_value_fair = ($request->valueFair && $request->valueFair > 0) ? $request->valueFair : null;
        $valuation->vehicle_condition_value_poor = ($request->valuePoor && $request->valuePoor > 0) ? $request->valuePoor : null;
        $valuation->fee = $newFee;
        $valuation->save();

        $this->createStatementEntry($valuation, auth()->user()->company, $valuation->vehicle, $additionalFee);

        \Log::info('test3');
        event(new NewValuationOffered($valuation, $valuation->vehicle));

        $highestValuation = $paidLead->highestValuation;

        if (
            $highestValuation->company->user->matrix_partner == 1 &&
            $highestValuation->company->user->send_additional_emails &&
            $highestValuation->company_id != $valuation->company_id
        ) {
            $email = new PartnerHasBeenOutbidEmail($paidLead->highestValuation->company->user, $valuation->vehicle, $paidLead->highestValuation, $valuation);
            Mail::to($paidLead->highestValuation->company->user->email)->send($email);
        }

        $profile = auth()->user()->profile;

        if ($profile && $request->saveConditionsAsDefault) {
            $this->updateMatrixDefaultVehicleConditionPercentages(
                auth()->user()->profile,
                (int) $request->vehicleConditionPercentageGood,
                (int) $request->vehicleConditionPercentageFair,
                (int) $request->vehicleConditionPercentagePoor
            );
        }

        if ((bool)$request->valuationVehicleConditionShow != (bool)$profile->valuation_vehicle_condition_show) {
            $profile->valuation_vehicle_condition_show = (bool)$request->valuationVehicleConditionShow;
            $profile->save();
        }

        alert()->success('Your offer has been updated.');
        return redirect(route('partnerShowValuation', [$valuation->id]));
    }

    public function saveNoteForValuation(Request $request, Valuation $valuation)
    {
        $this->guardAgainstOwnership($valuation);

        $this->validate($request, [
            'valuation-note' => 'string|max:65535|nullable'
        ]);
        $valuation->setNote($request->get('valuation-note'));
        $valuation->save();

        alert()->success('Your note has been saved.');
        return redirect(route('partnerShowValuation', [$valuation->id]));
    }

    /**
     * Create the statement entry
     *
     * @param  array $valuation
     * @param  array $company
     * @param  array $event
     * @param  int $fee
     * @return JamJar\UserStatement
     */
    protected function createStatementEntry($valuation, $company, $vehicle, $fee)
    {
        $statement = new UserStatement();

        $meta = json_encode([
            'valuation' => $valuation,
            'vehicle' => $vehicle->load('meta'),
            'position' => $valuation['position']
        ]);

        $statement = $statement->create([
            'user_id' => $company->user->id,
            'type' => 'withdrawal',
            'amount' => $fee ?? 0,
            'meta' => $meta
        ]);

        return $statement;
    }

    /**
     * @param Valuation $valuation
     * @throws InsufficientFundsException
     */
    private function guardAgainstInsufficientFounds(int $fee)
    {
        if (auth()->user()->funds->getOriginal('funds') < $fee) {
            alert()->error('You do not have enough funds to update this offer. Please add more.')->persistent();

            throw new InsufficientFundsException();
        }
    }

    private function guardAgainstOwnership(Valuation $valuation)
    {
        if (auth()->user()->company->id != $valuation->company_id) {
            alert()->error('Sorry, this vehicle has now been sold')->persistent();

            throw new VehicleExpiredException();
        }
    }

    private function validateLead(Request $request, Valuation $highestValuation)
    {
        $request->validate([
            'valueGreat' => [
                'required',
                new ValueGreaterThan((int) $highestValuation->getValueIntAttribute()),
                new ValueLowerThan(LeadsController::MAX_VALUATION_VALUE)
            ],
            'valueGood' => [
                new ValueLowerOrEqual((int) $request->valueGreat),
            ],
            'valueFair' => [
                new ValueLowerOrEqual((int) $request->valueGreat),
            ],
            'valuePoor' => [
                new ValueLowerOrEqual((int) $request->valueGreat),
            ]
        ]);
    }

    private function updateMatrixDefaultVehicleConditionPercentages(Profile $profile, int $good, int $fair, int $poor)
    {
        if (
            $profile->getVehicleConditionDefaultPercentageGood() == $good &&
            $profile->getVehicleConditionDefaultPercentageFair() == $fair &&
            $profile->getVehicleConditionDefaultPercentagePoor() == $poor
        ) {
            return;
        }

        $profile->vehicle_condition_default_percentage_good = $good;
        $profile->vehicle_condition_default_percentage_fair = $fair;
        $profile->vehicle_condition_default_percentage_poor = $poor;

        $profile->save();
    }
}
