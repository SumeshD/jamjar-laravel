<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Http\Request;
use JamJar\Exceptions\NotAllowedException;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlert;
use JamJar\Services\VehicleFilter\MarketplaceAlertsService;
use JamJar\Services\VehicleFilter\VehicleFilters;
use JamJar\Services\VehicleFilter\VehicleFilterService;

class MarketplaceAlertsController extends Controller
{
    /** @var VehicleFilterService */
    private $filterService;

    /** @var MarketplaceAlertsService */
    private $marketplaceAlertsService;

    public function __construct(VehicleFilterService $filterService, MarketplaceAlertsService $marketplaceAlertsService)
    {
        $this->filterService = $filterService;
        $this->marketplaceAlertsService = $marketplaceAlertsService;
    }

    public function showEnabledList(Request $request)
    {
        $alertsIds = $this->getAlertIdsFromRequest($request);

        $alertsQuery = MarketplaceAlert::where([
            ['user_id', '=', auth()->user()->id],
            ['is_enabled', '=', true],
        ])->orderBy('name', 'asc');

        if ($alertsIds) {
            $alertsQuery->whereIn('id', $alertsIds);
        }

        $alerts = $alertsQuery->paginate(5);

        return view('matrix.marketplace-alerts.index', ['alerts' => $alerts, 'enabledAlertsOnly' => true]);
    }

    public function showDisabledList()
    {
        $alerts = MarketplaceAlert::where([
            ['user_id', '=', auth()->user()->id],
            ['is_enabled', '=', false],
        ])->paginate(5);

        return view('matrix.marketplace-alerts.index', ['alerts' => $alerts, 'disabledAlertsOnly' => true]);
    }

    public function showCreateForm()
    {
        $lastAlert = MarketplaceAlert::where('user_id','=',auth()->user()->id)->latest()->first();

        return view('matrix.marketplace-alerts.create-form', ['vehicleFilters' => new VehicleFilters(),'lastAlert'=>$lastAlert]);
    }

    public function processCreateForm(Request $request)
    {
        $name = $request->get('alert-name') ? $request->get('alert-name') : \JamJar\Http\Controllers\Api\MarketplaceAlertsController::getNextFreeDefaultName();
        $alertFrequency = $request->get('alert-frequency');

        $filters = $this->filterService->createFiltersFromRequest($request);

        $validator = \JamJar\Http\Controllers\Api\MarketplaceAlertsController::getAlertValidation(
            $filters,
            $name,
            $alertFrequency
        );

        if ($validator->fails()) {
            return view('matrix.marketplace-alerts.create-form', [
                'vehicleFilters' => $filters,
                'errors' => $validator->errors(),
                'alertName' => $name,
                'alertFrequency' => $alertFrequency,
            ]);
        }

        $this->marketplaceAlertsService->createAlert(auth()->user(), $filters, $name, $alertFrequency);

        alert()->success('Marketplace Alert has been created!')->persistent();

        return redirect(route('marketplaceEnabledAlertsList'));
    }

    public function showEditForm(MarketplaceAlert $alert)
    {
        $this->guardAgainstAlertOwnership($alert);

        return view('matrix.marketplace-alerts.edit-form', [
            'alert' => $alert,
            'vehicleFilters' => $alert->convertToFilters(),
        ]);
    }

    public function processEditForm(Request $request, MarketplaceAlert $alert)
    {
        $this->guardAgainstAlertOwnership($alert);

        $name = $request->get('alert-name') ? $request->get('alert-name') : \JamJar\Http\Controllers\Api\MarketplaceAlertsController::getNextFreeDefaultName();
        $alertFrequency = $request->get('alert-frequency');
        $newFilters = $this->filterService->createFiltersFromRequest($request);

        $validator = \JamJar\Http\Controllers\Api\MarketplaceAlertsController::getAlertValidation(
            $newFilters,
            $name,
            $alertFrequency,
            $alert
        );

        if ($validator->fails()) {
            return view('matrix.marketplace-alerts.edit-form', [
                'alert' => $alert,
                'vehicleFilters' => $newFilters,
                'errors' => $validator->errors(),
                'alertName' => $name,
                'alertFrequency' => $alertFrequency,
            ]);
        }

        $alert = $this->marketplaceAlertsService->updateAlert($alert, $newFilters, $name, $alertFrequency);

        alert()->success('Marketplace Alert has been updated!')->persistent();

        return redirect(route($alert->isEnabled() ? 'marketplaceEnabledAlertsList' : 'marketplaceDisabledAlertsList'));
    }

    public function disableAlert(MarketplaceAlert $alert)
    {
        $this->guardAgainstAlertOwnership($alert);

        $alert->setIsEnabled(false);
        $alert->save();

        alert()->success('Marketplace Alert has been paused.')->persistent();
        return back();
    }

    public function enableAlert(MarketplaceAlert $alert)
    {
        $this->guardAgainstAlertOwnership($alert);

        $alert->setIsEnabled(true);
        $alert->save();

        alert()->success('Marketplace Alert has been activated.')->persistent();
        return back();
    }

    public function deleteAlert(MarketplaceAlert $alert)
    {
        $this->guardAgainstAlertOwnership($alert);

        $alert->delete();

        alert()->success('Marketplace Alert has been removed.')->persistent();
        return back();
    }

    private function guardAgainstAlertOwnership(MarketplaceAlert $alert)
    {
        if (auth()->user()->id != $alert->getUser()->id) {
            alert()->error('Marketplace Alert not found.')->persistent();

            throw new NotAllowedException();
        }
    }

    private function getAlertIdsFromRequest(Request $request): array
    {
        $alertsIdsString = $request->get('alerts');
        if (!$alertsIdsString) {
            return [];
        }

        try {
            $alertsIds = json_decode($alertsIdsString, true);
        } catch (\Exception $e) {
            return [];
        }

        if (!$alertsIds) {
            return [];
        }

        $alertsIdsToReturn = [];
        foreach ($alertsIds as $alertId) {
            if ($alertId == (int) $alertId) {
                $alertsIdsToReturn[] = (int) $alertId;
            }
        }

        return $alertsIdsToReturn;
    }
}
