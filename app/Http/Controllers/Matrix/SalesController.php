<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\Sale;
use JamJar\Services\VehicleService;
use JamJar\Vehicle;

class SalesController extends Controller
{
    public function agreedPurchasesList(Request $request)
    {

        $createdByAutomatedBidsOnly = (bool) $request->get('automated_bids');
        $createdByMarketplace = (bool) $request->get('marketplace');

        $sales = Sale::select('sales.*')->where('sales.company_id', auth()->user()->company->id)
            ->leftJoin('valuations','valuations.id','sales.valuation_id');


        if ($createdByAutomatedBidsOnly && !$createdByMarketplace) {
            $sales->where('valuations.is_created_by_automated_bids', '=', true);
        }

        if ($createdByMarketplace && !$createdByAutomatedBidsOnly) {
            $sales->where('valuations.is_created_by_automated_bids', '=', false);
        }

        $sales = $sales->orderBy('sales.created_at', 'DESC')->paginate(10);


        return view('matrix.sale.agreed-purchases',['sales' => $sales]);
    }

    public function agreedSalesList(Request $request)
    {
        $showActiveOnly = (bool) $request->get('active-only');

        $sales = Sale::select('sales.*')
            ->where('user_id', auth()->user()->id)
            ->orderBy('created_at', 'DESC')
            ->join('vehicles', 'vehicles.id', '=', 'sales.vehicle_id');

        if ($showActiveOnly) {
            if ($showActiveOnly) {
                $sales = $sales
                    ->where('vehicles.added_to_marketplace_at', '>=', Vehicle::getGoodNewsLeadVehicleDisappearDate())
                    ->where('status', '=', 'pending');
            }
        }

        $sales = $sales->paginate(10);

        return view('matrix.sale.agreed-sales', ['sales' => $sales]);
    }

    public function show(Sale $sale)
    {
        if ($sale->company->user_id != auth()->id()) {
            alert()->error('You do not have permission to view this.')->persistent();
            return redirect()->route('partnerDashboard');
        }

        return view('matrix.sale.show', compact('sale'));
    }

    public function update(Request $request, Sale $sale)
    {
        $sale->update(
            [
            'status' => $request->status ?? 'pending',
            'final_price' => $request->final_price ?? $sale->price,
            'dealer_notes' => $request->dealer_notes,
            ]
        );

        alert()->success('Status Updated.');
        return back();
    }

    public function rejectSale(Request $request, int $saleId)
    {
        $sale = Sale::where([
            ['id', '=', (int) $saleId],
            ['user_id', '=', auth()->user()->id],
        ])->first();

        if (!$sale) {
            alert()->error('Sale not found.')->persistent();

            return redirect()->back();
        }

        if ($sale->status == 'complete') {
            alert()->error('Sorry, you can not reject already accepted sale.')->persistent();

            return redirect()->back();
        }

        $sale->status = 'rejected';
        $sale->save();

        VehicleService::refundValuation($sale->valuation);

        alert()->error('Sale has been rejected.')->persistent();

        return redirect()->back();
    }
}
