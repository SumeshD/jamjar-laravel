<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Http\Request;
use JamJar\PartnerLocation;
use JamJar\Rules\UKPostcode;
use JamJar\Rules\ValueGreaterOrEqual;
use JamJar\Rules\ValueGreaterThan;
use JamJar\User;
use JamJar\Valuation;

class LocationsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['partner', 'auth']);
    }

    public function index()
    {
        if (!auth()->user()->company) {
            alert()->error(
                '
                    Sorry!
                    You don\'t have permission to access that part of the dashboard yet. 
                    Perhaps your account is pending approval?
                '
            )
                ->persistent();
            return redirect()->route('partnerDashboard');
        }
        $locations = PartnerLocation::where('company_id', auth()->user()->company->id)->get();

        return view('matrix.locations.index', compact('locations'));
    }

    public function create()
    {
        return view('matrix.locations.create');
    }

    public function edit(PartnerLocation $location)
    {
        return view('matrix.locations.edit', compact('location'));
    }

    public function store(Request $request, PartnerLocation $location)
    {
        $this->validate($request, $this->getValidation());

        $hasLocations = PartnerLocation::where('company_id', auth()->user()->company->id)->first();
        if (!$hasLocations) {
            request()->primary_location = true;
        }
        if ((bool)request()->primary_location == true) {
            $this->markAllMatrixLocationsAsNonPrimary(auth()->user());
        }

        // Currently fees are set globally so every location should have the same fees setting, it must be refactored
        $previousLocation = $hasLocations;

        $location->create([
            'user_id' => auth()->user()->id,
            'company_id' => auth()->user()->company->id,
            'name' => request()->name,
            'telephone' => request()->telephone,
            'address_line_one' => request()->address_line_one,
            'address_line_two' => request()->address_line_two,
//            'town' => request()->town, // for now we keep town as city
            'town' => request()->city,
            'city' => request()->city,
            'county' => request()->county ?? '',
            'postcode' => request()->postcode,
            'allow_collection' => $previousLocation->allow_collection ?? false,
            'collection_fee' => $previousLocation->collection_fee ?? 0,
            'allow_dropoff' => $previousLocation->allow_dropoff ?? false,
            'dropoff_fee' => $previousLocation->dropoff_fee ?? 0,
            'primary' => (bool)request()->primary_location
        ]);

        if ($location->primary) {
            $this->updateMatrixLocationDetails($location, auth()->user());
        } else {

        }

        alert()->success('Location Saved');

        return redirect(route('partnerAccount'));
    }

    public function update(Request $request, PartnerLocation $location)
    {
        $this->validate($request, $this->getValidation());

        if ((bool)request()->primary_location == true) {
            $this->markAllMatrixLocationsAsNonPrimary(auth()->user());
        }

        $isLocationPrimary = $location->primary;

        $location->update([
            'user_id' => auth()->user()->id,
            'company_id' => auth()->user()->company->id,
            'name' => request()->name,
            'telephone' => request()->telephone,
            'address_line_one' => request()->address_line_one,
            'address_line_two' => request()->address_line_two,
//            'town' => request()->town,
            'town' => request()->city,
            'city' => request()->city,
            'county' => request()->county ?? '',
            'postcode' => request()->postcode,
            'primary' => (bool)request()->primary_location
        ]);

        $message = 'Location Updated';

        if ($location->primary) {
            $this->updateMatrixLocationDetails($location, auth()->user());
        } else if ($isLocationPrimary) {
            // there must be at least 1 primary location!
            $location->primary = true;
            $location->save();

            $message = 'Location Updated but it is still primary!';
        }

        alert()->success($message);

        return redirect(route('partnerAccount'));
    }

    public function markLocationAsPrimary(Request $request, PartnerLocation $location)
    {
        if ($location->user_id != auth()->user()->id) {
            alert()->error('Not allowed');
            return redirect(route('partnerAccount'));
        }

        $locations = PartnerLocation::where(['primary' => 1, 'company_id' => $location->company_id])->get();

        foreach ($locations as $oldPrimaryLocation) {
            $oldPrimaryLocation->primary = 0;
            $oldPrimaryLocation->save();
        }

        $location->primary = 1;
        $location->save();

        $this->updateMatrixLocationDetails($location, auth()->user());

        alert()->success('Location Updated');

        return redirect(route('partnerAccount'));
    }

    public function destroy(PartnerLocation $location)
    {
        $locations = PartnerLocation::where('user_id', '=', auth()->user()->id)->get();

        if (count($locations) <= 1) {
            alert()->error('You must always have at least one location. To remove this location you must first add another location.');
            return redirect(route('partnerLocations'));
        }

        // if we're removing primary location we have to mark another as primary
        if ($location->primary) {
            $otherLocation = PartnerLocation::where([['user_id', '=', auth()->user()->id], ['id', '<>', $location->id]])->first();

            $otherLocation->primary = true;
            $otherLocation->save();
        } else {
            $otherLocation = PartnerLocation::where([['user_id', '=', auth()->user()->id], ['primary', '=', true]])->first();
        }

        /*
        $valuationsWithRemovedLocation = Valuation::where('location_id', '=', $location->id)->get();

        foreach ($valuationsWithRemovedLocation as $valuation) {
            $valuation->location_id = $otherLocation->id;
            $valuation->save();
        }
        */

        $location->is_active = false;
        $location->save();

        alert()->success('Location Deleted');
        return redirect(route('partnerAccount'));
    }

    protected function markAllMatrixLocationsAsNonPrimary(User $user)
    {
        $locations = PartnerLocation::where(['primary' => 1, 'company_id' => $user->company->id])->get();

        foreach ($locations as $location) {
            $location->primary = 0;
            $location->save();
        }
    }

    private function getValidation(): array
    {
        return [
            'name' => 'required',
            'address_line_one' => 'required',
            'telephone' => 'required|max:20|phone:GB',
//            'town' => 'required',
            'city' => 'required',
            'county' => 'nullable',
            'postcode' => ['required', new UKPostcode],
        ];
    }

    private function updateMatrixLocationDetails(PartnerLocation $location, User $user)
    {
        $company = $user->company;

        $company->address_line_one = $location->address_line_one;
        $company->address_line_two = $location->address_line_two;
        $company->town = $location->town;
        $company->city = $location->city;
        $company->county = $location->county;
        $company->postcode = $location->postcode;

        $company->save();

        $user->profile->address_line_one = $location->address_line_one;
        $user->profile->address_line_two = $location->address_line_two;
        $user->profile->town = $location->town;
        $user->profile->city = $location->city;
        $user->profile->county = $location->county;
        $user->profile->postcode = $location->postcode;

        $user->profile->save();
    }
}
