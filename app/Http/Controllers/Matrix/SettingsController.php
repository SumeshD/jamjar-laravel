<?php

namespace JamJar\Http\Controllers\Matrix;

use JamJar\PartnerLocation;
use JamJar\Rules\ValueGreaterOrEqual;
use Newsletter;
use JamJar\User;
use JamJar\Postcode;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use JamJar\Events\PartnerCreated;
use JamJar\Http\Requests\StorePartner;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','partner'])->except(['create', 'store']);
    }

    public function edit()
    {
        $user = auth()->user();
        $user->load('company');
        if (!$user->company) {
            alert()->error(
                '
                    Sorry!
                    You don\'t have permission to access that part of the dashboard yet. 
                    Perhaps your account is pending approval?
                '
            )
                ->html()
                ->persistent();
            return redirect()->route('partnerDashboard');
        }

        $postcodes = Postcode::select(['postcode', 'county', 'country'])->orderBy('county','asc')->get()->groupBy('country');
        $location =
            PartnerLocation::where(['primary' => 1, 'company_id' => auth()->user()->company->id])->first() ??
            PartnerLocation::where(['company_id' => auth()->user()->company->id])->first() ??
            new PartnerLocation();

        return view('matrix.settings', compact('user', 'postcodes', 'location'));
    }

    public function update(Request $request)
    {
        if ($request->postcodes) {
            $postcodes = [];
            foreach ($request->postcodes as $key => $value) {
                $postcodes[] = $key;
            }
            $postcodes = implode(',', $postcodes);
        }

        $this->validate(request(), [
            'valuation_validity' => 'required|numeric',
            'bank_transfer_fee' => 'required|numeric',
            'admin_fee' => 'required|numeric',
            'collection_fee' => [new ValueGreaterOrEqual(0)],
            'delivery_fee' => [new ValueGreaterOrEqual(0)],
        ]);

        $user = auth()->user();

        $user->company->update([
            'valuation_validity' => $request->valuation_validity,
            'bank_transfer_fee' => $request->bank_transfer_fee,
            'admin_fee' => $request->admin_fee,
            'buy_vehicles_from_other_partners' => $request->buy_vehicles_from_other_partners == 'on' ? true : false,
            'postcode_area' => $postcodes ?? null
        ]);

        $user->send_additional_vehicle_information_emails = $request->send_additional_vehicle_information_emails == 'on' ? true : false;
        $user->save();

        $this->updateLocationsFees($request, auth()->user());

        alert()->success('Your account has been updated.');
        return redirect()->route('partnerDashboard');
    }

    public function additionalVehicleInformationUnsubscribe(Request $request)
    {
        $user = auth()->user();

        $user->send_additional_vehicle_information_emails = false;
        $user->save();

        alert()->info('You have successfully been unsubscribed from emails informing you of additional vehicle information')->persistent();

        return response()->redirectTo(route('/'));
    }

    /**
     * Handle the given marketing preferences (or lack thereof)
     *
     * @param  array $request
     * @return void
     */
    protected function handleMarketingPreferences($request, $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        if (!isset($request['marketing']) || $request['marketing'] == false) {
            return false;
        }

        $marketingOptions = ['email' => true, 'telephone' => true, 'sms' => true];

        foreach ($marketingOptions as $key => $value) {
            $user->update(["marketing_{$key}" => (int)$value]);
        }

        $name = $this->formatName($request['name']);

        $mergeFields = [
            'MMERGE1' => $name['firstname'],
            'MMERGE2' => $name['lastname'],
            'MMERGE3' =>
                $request['address_line_one'] . ' ' .
                $request['address_line_two'] . ' ' .
                $request['postcode'] . ' ' .
                $request['city'] . ' ' .
                $request['town'] . ' ' .
                $request['county'],
            'MMERGE4' => $request['email'],
            'MMERGE6' => $request['telephone_number'],
            'MMERGE9' => $request['postcode'],
        ];

        Newsletter::subscribe($request['email'], $mergeFields, 'matrix_subscribers');
    }

    /**
     * Format the given name.
     *
     * @param  string $name
     * @return array
     */
    private function formatName($name)
    {
        $fullname = explode(' ', $name);

        if (array_key_exists(1, $fullname)) {
            $firstname = $fullname[0];
            $lastname = $fullname[1];
        } else {
            $firstname = $name;
            $lastname = ' ';
        }

        return compact('firstname', 'lastname');
    }

    private function updateLocationsFees(Request $request, User $user)
    {
        $locations = PartnerLocation::where(['company_id' => $user->company->id])->get();

        foreach ($locations as $location) {
            $location->allow_collection = (bool) request()->enable_collection;
            $location->collection_fee = (int) request()->collection_fee;
            $location->allow_dropoff = (bool) request()->enable_delivery;
            $location->dropoff_fee = (int) request()->dropoff_fee;

            $location->save();
        }
    }
}
