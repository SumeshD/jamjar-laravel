<?php

namespace JamJar\Http\Controllers\Matrix;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use JamJar\Company;
use JamJar\Exceptions\InsufficientFundsException;
use JamJar\Exceptions\VehicleExpiredException;
use JamJar\Mail\PartnerHasBeenOutbidEmail;
use JamJar\PartnerLocation;
use JamJar\PlateYear;
use JamJar\Profile;
use JamJar\Rules\ValueGreaterThan;
use JamJar\Rules\ValueLowerOrEqual;
use JamJar\Rules\ValueLowerThan;
use JamJar\Sale;
use JamJar\Services\DistanceCalculatorService;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\VehicleFilter\VehicleFilters;
use JamJar\Services\VehicleFilter\VehicleFilterService;
use JamJar\Setting;
use JamJar\Valuation;
use JamJar\Api\CapApi;
use JamJar\UserStatement;
use Illuminate\Http\Request;
use JamJar\Primitives\Money;
use JamJar\Events\NewValuationOffered;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Vehicle;
use JamJar\VehicleManufacturer;
use JamJar\VehicleMeta;
use JamJar\VehicleModel;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LeadsController extends Controller
{
    const MAX_VALUATION_VALUE = 14000000;

    const BASE_PROPOSED_PRICE_ADVANTAGE_IN_POUNDS = 50;

    const MAX_AVAILABLE_MILEAGE = 10000000;

    /** @var VehicleFilterService */
    private $filterService;

    /**
     * Instantiate a new instance of Matrix\ValuationsController
     */
    public function __construct(VehicleFilterService $filterService)
    {
        $this->middleware(['partner', 'auth']);
        $this->filterService = $filterService;
    }

    /** Show good news board */
    public function index(Request $request)
    {
        // manual pagination is needed because paginate() function does not support a 'having' clause </3
        $perPage = 15;

        $pageNum = (int) $request->get('page') ? (int) $request->get('page') : 1;

        $vehicleFilters = $this->filterService->createFiltersFromRequest($request);
        list($resultsCount, $rawVehicles) = Vehicle::getRawLeadsVehiclesForMatrix(auth()->user(), $vehicleFilters, $perPage, $pageNum);
        $leads = Vehicle::getLeadsByRawVehicles($rawVehicles);

        foreach ($leads as $lead) {
            $lead->fee = $this->getFeeForLead(
                null,
                false,
                $lead->vehicle->getHighestValue(),
                false,
                $lead->vehicle->id
            );
        }

        return view('matrix.leads.index', [
            'leads' => $leads,
            'vehicles' => $rawVehicles,
            'vehicleFilters' => $vehicleFilters,
            'resultsCount' => $resultsCount,
            'page' => $pageNum,
            'perPage' => $perPage,
        ]);
    }

    /** Show the requested lead to the user */
    public function show(Valuation $lead)
    {
        $fee = $this->getFeeForLead(
            null,
            false,
            $lead->vehicle->getHighestValue() + self::BASE_PROPOSED_PRICE_ADVANTAGE_IN_POUNDS,
            false
        );

        if (($lead->partner_type == 'api') && ($lead->cap_value == 0)) {
            $this->fetchCapValuesForApiLead($lead);
        }

        $this->checkIfVehicleIsValid($lead->vehicle);

        $goodNewsLead = Vehicle::getGoodNewsLeadByVehicle($lead->vehicle);
        $highestValuation = $goodNewsLead->highestValuation;
        $highestExpiredValuation = $goodNewsLead->highestExpiredValuation;

        $vehicle = $highestValuation->vehicle;

        $highestMatrixValuation = Valuation::getHighestByUser($vehicle, auth()->user());

        if ($highestMatrixValuation && $highestMatrixValuation->id != $highestValuation->id) {
            return redirect(route('partnerShowValuation', $highestMatrixValuation->id));
        }

        return view('matrix.leads.show', compact(['fee', 'highestValuation', 'highestExpiredValuation', 'vehicle', 'highestMatrixValuation']));
    }

    /** Show new valuation create form */
    public function addNewValuation(int $vehicleId)
    {
        $vehicle = Vehicle::where('id', (int) $vehicleId)->first();

        $this->checkIfVehicleIsValid($vehicle);

        $goodNewsLead = Vehicle::getGoodNewsLeadByVehicle($vehicle);
        $highestValuation = $goodNewsLead->highestValuation;

        $highestMatrixValuation = Valuation::getHighestByUser($vehicle, auth()->user());
        $highestExpiredValuation = $goodNewsLead->highestExpiredValuation;

        $baseFeeValue = ($highestValuation ?? (new Valuation())->setVehicle($vehicle))->getValueIntAttribute();

        $fee = $this->getFeeForLead(
            null,
            false,
            $vehicle->getHighestValue() + self::BASE_PROPOSED_PRICE_ADVANTAGE_IN_POUNDS,
            false,
            $vehicleId
        );
        $formUrl = route('partnerAddNewLead', ['vehicleId' => $vehicle->id]);

        return view('matrix.leads.show', compact(['fee', 'formUrl', 'highestValuation', 'highestExpiredValuation', 'vehicle', 'highestMatrixValuation']));
    }

    /** Save new vehicle base on previous one */
    public function update(Request $request, Valuation $lead)
    {
        $this->checkIfVehicleIsValid($lead->vehicle);

        $goodNewsLead = Vehicle::getGoodNewsLeadByVehicle($lead->vehicle);
        $highestValuation = $goodNewsLead->highestValuation;

        $this->validateLead($request, $highestValuation);

        $this->guardAgainstInsufficientFounds($highestValuation, $request->valueGreat);
        $fee = $this->getFeeForLead($highestValuation, true, $request->valueGreat, $lead->vehicle->id);

        $valuation = $highestValuation->replicate();

        $valueField = $request->delivery_method == 'collection' ? 'collection_value' : 'dropoff_value';
        $nullField = $request->delivery_method == 'collection' ? 'dropoff_value' : 'collection_value';

        $highestMatrixValuation = Valuation::getHighestByUser($lead->vehicle, auth()->user());

        if ($highestMatrixValuation && $highestMatrixValuation->getOriginal('collection_value') && $highestMatrixValuation->getOriginal('dropoff_value')) {
            $valuation->collection_value = $valuation->dropoff_value = $request->valueGreat;
        } else {
            $valuation->$valueField = $request->valueGreat;
            $valuation->$nullField = null;
        }

        list($closestLocation, $closestDistance) = $this->getClosestLocation($lead->vehicle);

        $valuation->user_id = $lead->vehicle->owner ? $lead->vehicle->owner->user_id : 0;
        $valuation->company_id = auth()->check() ? auth()->user()->company->id : 0;
        $valuation->fee = $fee;
        $valuation->location_id = $closestLocation->id;
        $valuation->distance = $closestDistance;
        $valuation->accept_button_clicked_at = null;
        $valuation->temp_ref = null;
        $valuation->acceptance_uri = null;
        $valuation->is_created_by_automated_bids = false;
        $valuation->admin_fee = auth()->user()->company->getAdminFeeInPence() / 100;
        $valuation->vehicle_condition_value_good = ($request->valueGood && $request->valueGood > 0) ? $request->valueGood : null;
        $valuation->vehicle_condition_value_fair = ($request->valueFair && $request->valueFair > 0) ? $request->valueFair : null;
        $valuation->vehicle_condition_value_poor = ($request->valuePoor && $request->valuePoor > 0) ? $request->valuePoor : null;

        $valuation->save();

        $profile = auth()->user()->profile;

        if ($profile && $request->saveConditionsAsDefault) {
            $this->updateMatrixDefaultVehicleConditionPercentages(
                auth()->user()->profile,
                (int) $request->vehicleConditionPercentageGood,
                (int) $request->vehicleConditionPercentageFair,
                (int) $request->vehicleConditionPercentagePoor
            );
        }

        if ((bool)$request->valuationVehicleConditionShow != (bool)$profile->valuation_vehicle_condition_show) {
            $profile->valuation_vehicle_condition_show = (bool)$request->valuationVehicleConditionShow;
            $profile->save();
        }

        PartnerFeeCalculator::createStatementEntry($valuation, auth()->user()->company, $valuation->vehicle, $fee);

        event(new NewValuationOffered($valuation, $valuation->vehicle));
        alert()->success('New Valuation Sent!');

        if (
            $highestValuation->company->user->matrix_partner == 1 &&
            $highestValuation->company->user->send_additional_emails &&
            $highestValuation->company_id != $valuation->company_id
        ) {
            $email = new PartnerHasBeenOutbidEmail($highestValuation->company->user, $valuation->vehicle, $highestValuation , $valuation);
            Mail::to($highestValuation->company->user->email)->send($email);
        }

        return redirect()->route('partnerShowLead', $valuation);
    }

    /** Save valuation to vehicle without valuations */
    public function createLeadByVehicle(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', (int) $vehicleId)->first();

        $this->checkIfVehicleIsValid($vehicle);

        $goodNewsLead = Vehicle::getGoodNewsLeadByVehicle($vehicle);
        $validationValuation = $goodNewsLead->highestValuation ?? null;

        $this->validateLead($request, $validationValuation);

        $this->guardAgainstInsufficientFounds($validationValuation, $request->valueGreat);
        $fee = $this->getFeeForLead($validationValuation, true, $request->valueGreat, false, $vehicle->id);

        $valuation = $goodNewsLead->highestValuation ?
            $goodNewsLead->highestValuation->replicate() :
            (new Valuation())->setVehicle($vehicle);

        $valueField = $request->delivery_method == 'collection' ? 'collection_value' : 'dropoff_value';
        $nullField = $request->delivery_method == 'collection' ? 'dropoff_value' : 'collection_value';

        list($closestLocation, $closestDistance) = $this->getClosestLocation($vehicle);

        $valuation->position = 1;
        $valuation->user_id = ($vehicle && $vehicle->owner) ? $vehicle->owner->user_id : 0;
        $valuation->company_id = auth()->check() ? auth()->user()->company->id : 0;
        $valuation->$valueField = $request->valueGreat;
        $valuation->$nullField = null;
        $valuation->fee = $fee;
        $valuation->admin_fee = auth()->user()->company->getAdminFeeInPence() / 100;
        $valuation->cap_value = 0;
        $valuation->uuid = $valuation->uuid ?? Uuid::uuid4()->toString();
        $valuation->vehicle_condition_value_good = ($request->valueGood && $request->valueGood > 0) ? $request->valueGood : null;
        $valuation->vehicle_condition_value_fair = ($request->valueFair && $request->valueFair > 0) ? $request->valueFair : null;
        $valuation->vehicle_condition_value_poor = ($request->valuePoor && $request->valuePoor > 0) ? $request->valuePoor : null;
        $valuation->location_id = $closestLocation->id;
        $valuation->distance = $closestDistance;
        $valuation->is_created_by_automated_bids = false;
        $valuation->save();

        $profile = auth()->user()->profile;

        if ($profile && $request->saveConditionsAsDefault) {
            $this->updateMatrixDefaultVehicleConditionPercentages(
                auth()->user()->profile,
                (int) $request->vehicleConditionPercentageGood,
                (int) $request->vehicleConditionPercentageFair,
                (int) $request->vehicleConditionPercentagePoor
            );
        }

        if ((bool)$request->valuationVehicleConditionShow != (bool)$profile->valuation_vehicle_condition_show) {
            $profile->valuation_vehicle_condition_show = (bool)$request->valuationVehicleConditionShow;
            $profile->save();
        }

        PartnerFeeCalculator::createStatementEntry($valuation, auth()->user()->company, $valuation->vehicle, $fee);

        event(new NewValuationOffered($valuation, $valuation->vehicle));
        alert()->success('New Valuation Sent!');

        return redirect()->route('partnerShowLead', $valuation);
    }

    public function getFeeByValue(Request $request)
    {
        $value = (int) $request->get('value');
        $vehicleId = $request->get('vehicle_id');

        $value = $value <= self::MAX_VALUATION_VALUE ? $value : self::MAX_VALUATION_VALUE;

        return response()->json([
            'fee' => number_format($this->getFeeForLead(null, false, $value, false, $vehicleId), 2),
        ]);
    }

    public function getFeeForLead(OfferedValueInterface $lead = null, $inPence=false, int $priceInPounds = null, $includingVAT = true, $vehicleId = null): float
    {
        $calculator = new PartnerFeeCalculator(2, $lead ? ($lead->getPriceInPence() / 100) : $priceInPounds, $vehicleId);

        return (float) $calculator->calculateForGoodNewsLead($inPence, auth()->user()->company, $includingVAT);
    }

    /**
     * Fetch the CAP Values for the API Lead
     *
     * @param JamJar\Valuation $lead
     * @return JamJar\Valuation $lead
     */
    protected function fetchCapValuesForApiLead($lead)
    {
        $valuation = (new CapApi)->getValuation($lead->vehicle, $lead->vehicle->type)->first();

        $lead = $lead->update([
            'cap_value' => $valuation['average'] ?? 0,
            'cap_value_retail' => $valuation['retail'] ?? 0,
            'cap_value_clean' => $valuation['clean'] ?? 0,
            'cap_value_below' => $valuation['below'] ?? 0,
        ]);

        return $lead;
    }

    /**
     * @param Vehicle|null $vehicle
     * @throws VehicleExpiredException
     */
    private function guardAgainstEmptyVehicle(Vehicle $vehicle = null)
    {
        if (!$vehicle) {
            alert()->error('Sorry, vehicle does not exist.')->persistent();

            throw new VehicleExpiredException();
        }
    }

    /**
     * @param Vehicle|null $vehicle
     * @throws VehicleExpiredException
     */
    private function guardAgainstExpiredVehicle(Vehicle $vehicle = null)
    {
        if (!$vehicle || $vehicle->isOfferExpired()) {
            alert()->error('Sorry, vehicle does not exist.')->persistent();

            throw new VehicleExpiredException();
        }
    }

    /**
     * @param Vehicle|null $vehicle
     * @throws VehicleExpiredException
     */
    private function guardAgainstSoldVehicle(Vehicle $vehicle = null)
    {
        $sale = Vehicle::getActiveSaleForVehicle($vehicle);

        if ($sale) {
            alert()->error('Sorry, this vehicle has been sold.')->persistent();

            throw new VehicleExpiredException();
        }
    }

    /**
     * @param Valuation $valuation
     * @throws InsufficientFundsException
     */
    private function guardAgainstInsufficientFounds(?Valuation $valuation = null, int $price = null)
    {
        if (auth()->user()->funds->getOriginal('funds') < $this->getFeeForLead($valuation, false, $price)) {
            alert()->error('You do not have enough funds to purchase this lead. Please add more.')->persistent();

            throw new InsufficientFundsException();
        }
    }

    private function validateLead(Request $request, Valuation $highestValuation = null)
    {
        $validators = [
            'valueGood' => [
                new ValueLowerOrEqual((int) $request->valueGreat),
            ],
            'valueFair' => [
                new ValueLowerOrEqual((int) $request->valueGreat),
            ],
            'valuePoor' => [
                new ValueLowerOrEqual((int) $request->valueGreat),
            ]
        ];

        if ($highestValuation) {
            $validators['valueGreat'] = [
                'required',
                new ValueGreaterThan((int) $highestValuation->getValueIntAttribute()),
                new ValueLowerThan(self::MAX_VALUATION_VALUE)
            ];
        } else {
            $validators['valueGreat'] = [
                'required',
                new ValueLowerThan(self::MAX_VALUATION_VALUE)
            ];
        }

        $request->validate($validators);
    }

    private function guardAgainstVehicleOwnership(Vehicle $vehicle = null)
    {
        if ($vehicle->owner->user_id == auth()->user()->id) {
            alert()->error('Sorry, you can\'t buy your own car')->persistent();

            throw new VehicleExpiredException();
        }
    }

    private function guardMarketplaceListing(Vehicle $vehicle = null)
    {
        if (!$vehicle->is_visible_on_marketplace) {
            alert()->error(
                'The vehicle owner may have changed the details of their vehicle.',
                'Sorry, we couldn’t find this vehicle in the Marketplace'
            )->persistent();

            throw new VehicleExpiredException();
        }
    }

    /**
     * @param Vehicle $vehicle
     * @throws VehicleExpiredException
     */
    private function checkIfVehicleIsValid(Vehicle $vehicle)
    {
        $this->guardAgainstEmptyVehicle($vehicle);
        $this->guardAgainstExpiredVehicle($vehicle);
        $this->guardAgainstSoldVehicle($vehicle);
        $this->guardAgainstVehicleOwnership($vehicle);
        $this->guardMarketplaceListing($vehicle);
    }

    private function updateMatrixDefaultVehicleConditionPercentages(Profile $profile, int $good, int $fair, int $poor)
    {
        if (
            $profile->getVehicleConditionDefaultPercentageGood() == $good &&
            $profile->getVehicleConditionDefaultPercentageFair() == $fair &&
            $profile->getVehicleConditionDefaultPercentagePoor() == $poor
        ) {
            return;
        }

        $profile->vehicle_condition_default_percentage_good = $good;
        $profile->vehicle_condition_default_percentage_fair = $fair;
        $profile->vehicle_condition_default_percentage_poor = $poor;

        $profile->save();
    }

    private function getClosestLocation(Vehicle $vehicle) : array
    {
        $distanceService = new DistanceCalculatorService();
        $closestLocation = auth()->user()->company->activeLocation->first();
        $closestDistance = null;

        foreach (PartnerLocation::where('user_id', '=', auth()->user()->id)->where('is_active',1)->get() as $location) {
            $distance = $distanceService->getDistanceBetweenLocationsInMeters($location->postcode, $vehicle->owner->postcode);

            if ($distance !== null && ($closestDistance === null || $distance < $closestDistance)) {
                $closestLocation = $location;
                $closestDistance = $distance;
            }
        }

        return [$closestLocation, $closestDistance];
    }
}
