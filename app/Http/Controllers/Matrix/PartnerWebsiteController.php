<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use JamJar\PartnerWebsite;

class PartnerWebsiteController extends Controller
{
    /**
     * Instantiate a new instance of Matrix\ThemeController
     */
    public function __construct()
    {
        $this->middleware('partner')->only('create');
    }

    /**
     * Display the partner's website homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PartnerWebsite $website)
    {
        return redirect(route('partnerWebsitePage', [$website, 'home']));
    }

    /**
     * [store description]
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $website = auth()->user()->website;

        $this->validate(
            $request,
            [
            'hero_image' => ['image'],
            'logo_image' => ['image']
            ]
        );

        $filePath = 'partner-websites/images/' . $website->slug;

        if ($request->hero_image) {
            $heroImage = $request->file('hero_image')->storeAs($filePath, 'hero.jpg', 's3');
            $heroUrl = Storage::url($heroImage);
        } elseif ($website->hero_image) {
            $heroImage = $website->hero_image;
            $heroUrl = $heroImage;
        }

        if ($request->logo_image) {
            $logoImage = $request->file('logo_image')->storeAs($filePath, 'logo.jpg', 's3');
            $logoUrl = Storage::url($logoImage);
        } elseif ($website->logo_image) {
            $logoImage = $website->logo_image;
            $logoUrl = $logoImage;
        }

        $website->update(
            [
            'logo_image'    => $logoUrl ?? 'partner-websites/default/jamjar_partner.jpg',
            'hero_image'    => $heroUrl ?? 'partner-websites/default/hero.jpg',
            'bg_color'      => $request->bg_color ?? '000000',
            'brand_color'   => $request->brand_color ?? 'FFFFFF',
            ]
        );

        alert()->success('Website updated');

        return back();
    }

    /**
     * Display the Theme Options Page
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->website) {
            alert()->error(
                '
                    Sorry!
                    You don\'t have permission to access that part of the dashboard yet.
                    Perhaps your account is pending approval?
                '
            )
                ->html()
                ->persistent();
            return redirect()->route('partnerDashboard');
        }
        $openingtimes = unserialize(auth()->user()->website->getPage('contact')->meta);
        
        return view('matrix.theme.index', compact('openingtimes'));
    }
}
