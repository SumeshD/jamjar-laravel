<?php

namespace JamJar\Http\Controllers\Matrix;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use JamJar\Api\CapApi;
use JamJar\Api\TextMarketer;
use JamJar\Colour;
use JamJar\Defect;
use JamJar\Events\SaleCreated;
use JamJar\Events\VehicleAddedToMarketplace;
use JamJar\Exceptions\InsufficientFundsException;
use JamJar\Exceptions\NotAllowedException;
use JamJar\Http\Controllers\VehicleController;
use JamJar\Model\ValuationDraft;
use JamJar\Model\ValuationDraftDeliveryMethod;
use JamJar\PartnerWebsite;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Profile;
use JamJar\Rules\MinimumAskingPrice;
use JamJar\Rules\NonRunnerReasonsRequire;
use JamJar\Rules\RelatedFieldNotInRule;
use JamJar\Sale;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\OfferedValues\OfferedValueService;
use JamJar\Services\Valuations\VehicleValuationsCollector;
use JamJar\Services\Valuations\VehicleValuationsCreator;
use JamJar\Services\VehicleService;
use JamJar\Valuation;
use JamJar\Vehicle;
use JamJar\VehicleMeta;
use JamJar\VehicleOwner;
use Ramsey\Uuid\Uuid;
use \Exception;
use JamJar\Services\Integrations\SellItTradeIntegration;

class ValuationDraftsController extends Controller
{
    /** @var VehicleValuationsCollector */
    private $valuationsCollector;

    /** @var VehicleValuationsCreator */
    private $valuationsCreator;

    public function __construct(VehicleValuationsCollector $valuationsCollector, VehicleValuationsCreator $valuationsCreator)
    {
        $this->valuationsCollector = $valuationsCollector;
        $this->valuationsCreator = $valuationsCreator;

        $this->middleware('partner');
    }

    public function index(Request $request)
    {
        $whereColumns = [
            ['vehicle_owners.user_id', '=', auth()->user()->id],
            ['vehicles.is_finished', '=', true],
        ];

        $whereColumns = $this->addSearchByNumberplate($request, $whereColumns);

        $vehicles = Vehicle::select('vehicles.*')
            ->where($whereColumns)
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->orderBy('vehicles.created_at', 'DESC')
            ->paginate(20);

        return view('matrix.valuation-drafts.index', ['vehicles' => $vehicles, 'dateLabel' => 'Date']);
    }

    public function listedVehiclesList(Request $request)
    {
        $vehicles = Vehicle::getListedVehiclesForUserQuery(auth()->user());
        $vehicles = $this->addSearchByNumberplateStatement($request, $vehicles);

        $vehicles = $vehicles->paginate(20);

        return view('matrix.valuation-drafts.index', [
            'vehicles' => $vehicles,
            'page' => 'listed-vehicles',
            'showSearchBox' => false,
            'pageTitle' => 'Marketplace',
            'pageDescription' => 'Listed Vehicles',
            'highlightVehiclesWithValuations' => true,
        ]);
    }

    public function offersReceived(Request $request)
    {
        $offersType = $request->get('offers-type') ? $request->get('offers-type') : 'open';

        $vehicles = Vehicle::getOffersReceivedForUserQuery(auth()->user(), $offersType);
        $vehicles = $this->addSearchByNumberplateStatement($request, $vehicles)->paginate(20);

        return view('matrix.valuation-drafts.index', [
            'vehicles' => $vehicles,
            'page' => 'offers-received',
            'showSearchBox' => false,
            'pageTitle' => 'Marketplace',
            'pageDescription' => 'Offers Received',
            'highlightVehiclesWithValuations' => false,
            'viewButtonText' => 'VIEW OFFERS',
            'showOfferTypes' => true,
            'offersType' => $offersType,
        ]);
    }

    public function createVehicle(Request $request)
    {
        $seller = auth()->user();

        $numberplate =  $request->numberplate;

        if (!$numberplate) {
            $numberplate = $request->get('numberplate');
        }

        $numberplate = (string) str_replace(' ', '', $numberplate);

        if ($previousFinishedVehicle = Vehicle::getFinishedVehicle(auth()->user(), $numberplate)) {
            $this->showVehicleExistsAlert($previousFinishedVehicle);

            return redirect(route('matrixValuationDraftsIndex') . '?numberplate=' . $numberplate);
        }

        if ($previousUnfinishedVehicle = Vehicle::getUnfinishedVehicle(auth()->user(), $numberplate)) {
            return redirect(route('matrixValuationDraftsShowVehicleInformationForm', [$previousUnfinishedVehicle->id]));
        }

        $vehicle = (new Vehicle)->createVehicleFromApiData($numberplate);

        if (!$vehicle) {
            alert()->error('Sorry, we couldn\'t find your car. Please try entering your numberplate again.')->persistent();

            return redirect(route('matrixValuationDraftsIndex') . '?numberplate='.$numberplate);
        }

        $vehicle->is_visible_on_marketplace = false;
        $vehicle->is_finished = false;
        $vehicle->save();

        VehicleOwner::create([
            'firstname' => $seller->first_name,
            'lastname' =>$seller->last_name,
            'telephone' => $seller->profile->mobile_number,
            'postcode' => $seller->company->postcode,
            'email' => $seller->profile->email,
            'user_id' => $seller->id,
            'vehicle_id' => $vehicle->id
        ]);

        return redirect(route('matrixValuationDraftsShowVehicleInformationForm', [$vehicle->id]));
    }

    public function showVehicleInformationForm(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        $this->guardAgainstVehicleOwnership($vehicle);

        return view('matrix.valuation-drafts.vehicle-information', [
            'vehicle' => $vehicle,
            'defects' => Defect::all(),
            'colors' => Colour::all(),
            'serviceHistories' => VehicleMeta::getAvailableServicesHistories(),
            'mots' => VehicleMeta::getAvailableMots(),
            'writeOffCategories' => VehicleMeta::getAvailableWriteOffCategories(),
        ]);
    }

    public function showAddToMarketplaceForm(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        $this->guardAgainstVehicleOwnership($vehicle);
        $this->guardAgainstVehicleMarketplacePosition($vehicle);

        $sellerFeeInPence = $this->getFeeInPenceForSellerForAddingToMarketplace($vehicle, 0);

        return view('matrix.valuation-drafts.add-to-marketplace', [
            'vehicle' => $vehicle,
            'sellerFeeInPence' => $sellerFeeInPence,
        ]);
    }

    public function addToMarketplace(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        $this->guardAgainstVehicleOwnership($vehicle);

        $vehicleValidations = $this->getAdditionalVehicleValidation();

        $this->validate($request, array_merge(
            $vehicleValidations
        ));

        $askingPriceInPence = (int) ((int) $request->get('asking-price-in-pounds') * 100);
        $sellerFeeInPence = $this->getFeeInPenceForSellerForAddingToMarketplace($vehicle, $askingPriceInPence);

        $this->guardAgainstInsufficientFundsForSellerByVehicle($vehicle, $sellerFeeInPence);

        $vehicle->setAskingPriceInPence($askingPriceInPence);

        $this->updateAdditionalVehicleInformation($request, $vehicle);

        $vehicle->setAddedToMarketplaceAt(Carbon::now());
        $vehicle->setSellerFeeInPence($sellerFeeInPence);
        $vehicle->setIsVisibleOnMarketplace(true);

        event(new VehicleAddedToMarketplace($vehicle));

        $vehicle->save();

        if (!$vehicle->hasSale()) {
            $this->chargeSellerForAddingToMarketplace($vehicle, $sellerFeeInPence);
        }

        alert()->success(
            'We\'ll let you know as soon as another partner makes an improved offer for your vehicle ',
            'Your vehicle has been added to the Marketplace.'
        )->persistent("Great!");

        return redirect(route('matrixValuationDraftsShow', [$vehicle->id]));
    }

    public function removeFromMarketplace(int $vehicleId) {

        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        $this->guardAgainstVehicleOwnership($vehicle);

        $vehicle->is_visible_on_marketplace = 0;
        $vehicle->save();

        alert()->success(
            'Your vehicle has been removed from Marketplace.',
            'Vehicle Removed.'
        )->persistent("Great!");

        return redirect(route('matrixValuationDraftsShow', [$vehicle->id]));

    }

    public function saveVehicleInformation(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        $this->guardAgainstVehicleOwnership($vehicle);

        $this->validate($request, [
            'service-history' => 'required',
            'mot' => 'required',
            'mileage' => 'required|numeric|min:1000|max:' . LeadsController::MAX_AVAILABLE_MILEAGE,
            'car-colour' => 'required',
            'write-off' => [
                'required',
                new RelatedFieldNotInRule($request->input('write-off-category'), ['none'])
            ],
            'non-runner' => [
                'required',
                new NonRunnerReasonsRequire($request->input('non-runner-reason')),
            ],
        ]);

        $reasonIds = [];
        if ($request->input('non-runner-reason')) {
            foreach ($request->input('non-runner-reason') as $key => $value) {
                $reasonIds[] = $key;
            }
        }

        if ($request->input('non-runner') && !Defect::areNonMechanicalDefectsOnly($reasonIds)) {
            $vehicle->meta->non_runner = true;
            $reasons = [];
            foreach ($reasonIds as $reasonId) {
                $reason = Defect::where('id', '=', $reasonId)->first();
                if (in_array($reason->title, Defect::getNonMechanicalReasons())) {
                    continue;
                }
                $reasons[] = [
                    'id' => $reason->id,
                    'title' => $reason->title,
                ];
            }
            $vehicle->meta->non_runner_reason = serialize($reasons);
        } else {
            $vehicle->meta->non_runner = false;
            $vehicle->meta->non_runner_reason = serialize([]);
        }

        foreach ($request->all() as $key => $value) {
            session()->put([$key => $value]);
        }

        $vehicle->meta->mileage = $request->input('mileage');
        $vehicle->meta->service_history = $request->input('service-history');
        $vehicle->meta->mot = $request->input('mot');
        $vehicle->meta->color = $request->input('car-colour');
        $vehicle->meta->write_off = (bool)$request->input('write-off');
        $vehicle->meta->write_off_category = $request->input('write-off-category');
        $vehicle->meta->save();

        $this->valuationsCollector->createValuationDrafts($vehicle);

        $vehicle->is_finished = true;
        $vehicle->save();

        $capApi = new CapApi;
        $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();

        if ($capValuation) {
            $vehicle->setCapValues($capValuation);
            $vehicle->save();
        }

        return redirect(route('matrixValuationDraftsShow', [$vehicle->id]));
    }

    public function showValuationAcceptForm(Request $request, int $valuationId)
    {
        /** @var Valuation $valuation */
        $valuation = Valuation::where('id', '=', $valuationId)->first();

        $this->guardAgainstInsufficientFundsForSeller($valuation);
        $this->guardAgainstInsufficientFundsForBuyer($valuation);
        $this->guardAgainstExpiredOffer($valuation);
        $this->guardAgainstDraftOwnership($valuation);
        $this->guardAgainstExistingSale($valuation);

        $website = PartnerWebsite::where('company_id', '=', $valuation->company_id)->first();
        $page = $website->getPage('your-offer');

        $calculator = new PartnerFeeCalculator(0, $valuation->getPriceInPence() / 100);
        $feeInPence = $calculator->getFeeForSeller(true, $valuation->getSellerCompany(), false);

        return view('matrix.valuation-drafts.accept-offered-value', [
            'offeredValue' => $valuation,
            'website' => $website,
            'page' => $page,
            'vehicle' => $valuation->vehicle,
            'sellerUser' => auth()->user(),
            'feeInPence' => $feeInPence,
        ]);
    }

    public function showDraftAcceptForm(Request $request, int $draftId)
    {
        /** @var ValuationDraft $draft */
        $draft = ValuationDraft::where('id', '=', $draftId)->first();

        $this->guardAgainstUnwantedPartnerToPartnerSales($draft);
        $this->guardAgainstInsufficientFundsForSeller($draft);
        $this->guardAgainstInsufficientFundsForBuyer($draft);
        $this->guardAgainstExpiredOffer($draft);
        $this->guardAgainstDraftOwnership($draft);
        $this->guardAgainstExistingSale($draft);

        $website = PartnerWebsite::where('company_id', '=', $draft->buyer_company_id)->first();
        $page = $website->getPage('your-offer');

        $calculator = new PartnerFeeCalculator(0, $draft->getPriceInPence() / 100);
        $feeInPence = $calculator->getFeeForSeller(true, $draft->getSellerCompany(), false);

        return view('matrix.valuation-drafts.accept-offered-value', [
            'offeredValue' => $draft,
            'website' => $website,
            'page' => $page,
            'vehicle' => $draft->getVehicle(),
            'sellerUser' => auth()->user(),
            'feeInPence' => $feeInPence,
        ]);
    }

    public function submitValuationAcceptForm(Request $request, int $valuationId)
    {
        /** @var Valuation $$valuation */
        $valuation = Valuation::where('id', '=', $valuationId)->first();

        $this->guardAgainstInsufficientFundsForSeller($valuation);
        $this->guardAgainstInsufficientFundsForBuyer($valuation);
        $this->guardAgainstDraftOwnership($valuation);
        $this->guardAgainstExpiredOffer($valuation);
        $this->guardAgainstExistingSale($valuation);

        try {
            $this->completeAgreement($request, $valuation);
        } catch (Exception $e) {
            return back();
        }

        $vehicle = $valuation->getVehicle();
        $vehicle->setConditionInformation($request->get('condition-information'));
        $vehicle->save();

        $route = route('partnerWebsitePage', [$valuation->getBuyerCompany()->website->slug, 'offer-complete']) . '?valuationId=' . $valuation->id;

        return Redirect::to($route);
    }

    public function submitDraftAcceptForm(Request $request, int $draftId)
    {
        /** @var ValuationDraft $draft */
        $draft = ValuationDraft::where('id', '=', $draftId)->first();

        $this->guardAgainstUnwantedPartnerToPartnerSales($draft);
        $this->guardAgainstInsufficientFundsForSeller($draft);
        $this->guardAgainstInsufficientFundsForBuyer($draft);
        $this->guardAgainstDraftOwnership($draft);
        $this->guardAgainstExpiredOffer($draft);
        $this->guardAgainstExistingSale($draft);

        $this->validate($request, array_merge(['condition-information' => 'nullable|max:512'], $this->getAdditionalVehicleValidation()));

        $creator = new VehicleValuationsCreator();

        $valuation = $draft->getValuation();

        if (!$valuation) {
            $valuation = $creator->createValuationFromDraft($draft, Uuid::uuid4(), $draft->getListingPosition());
            $valuation->is_created_by_automated_bids = true;
            $valuation->save();
        }

        $vehicle = $draft->getVehicle();
        $vehicleHasSale = $vehicle->hasSale();

        try {
            $this->completeAgreement($request, $valuation);
        } catch (Exception $e) {
            return back();
        }

        $this->updateAdditionalVehicleInformation($request, $vehicle);
        $vehicle->setConditionInformation($request->get('condition-information'));
        $vehicle->save();

        if (!$vehicle->isVisibleOnMarketplace() && !$vehicleHasSale) {
            $this->chargeSellerForOfferedValue($valuation, $draft);
        }

        $this->chargeBuyerForOfferedValue($valuation, $draft);

        $route = route('partnerWebsitePage', [$draft->getBuyerCompany()->website->slug, 'offer-complete']) . '?valuationId=' . $valuation->id;

        return Redirect::to($route);
    }

    protected function completeAgreement(Request $request, Valuation $valuation, ValuationDraft $draft = null)
    {
        $company = $valuation->getBuyerCompany();

        // there is a problem with several vehicles with the same numberplate
        // so we take vehicle directly from valuation for now
        $vrm = $valuation->vehicle;

        $deliveryMethod = $request->deliveryMethod;

        if (!$valuation) {
            alert()->error('We\'re Sorry, Something went wrong. Please try again.');
            throw new Exception('error 0000');
        }

        $availableConditions = ['great'];

        foreach (['good', 'fair', 'poor'] as $condition) {
            $fieldName = 'vehicle_condition_value_' . $condition;
            if ($valuation->$fieldName > 0) {
                $availableConditions[] = $condition;
            }
        }

        if (!in_array($request->condition, $availableConditions)) {
            alert()->error('Selected condition is not available.');
            throw new Exception('error 0000');
        }

        $condition = $request->condition;

        $finalPrice = $deliveryMethod == 'dropoff' ?
            ($draft ? $draft->getFinalDropOffPriceInPence($condition) : $valuation->getFinalDropOffPriceInPence($condition)) :
            ($draft ? $draft->getFinalCollectionPriceInPence($condition) : $valuation->getFinalCollectionPriceInPence($condition));

        // create a sale
        $sale = (new Sale())->createSale($vrm, $valuation, $company, ['value' => $finalPrice / 100, 'deliveryMethod' => $deliveryMethod, 'condition' => $request->condition]);
        // get the profile

        $profile = Profile::where('user_id', $sale->user_id)->first();
        // update the profile with the request information just submitted (address, etc)
        $profile->update($request->except('email'));

        /**
         * SMS Messages
         */

        $displayedFinalPrice = $finalPrice / 100;

        $smsContentsToPartner = "{$profile->name} has agreed to your offer for {$vrm->meta->formatted_name} of {$displayedFinalPrice} GBP, {$deliveryMethod}. Click ". route('partnerShowSale', $sale);

        $smsContentsToUser = "Thanks for choosing our associate {$company->name} ({$company->telephone}). They have valued your vehicle at {$displayedFinalPrice} GBP, {$deliveryMethod}. They will contact you within 3 working hours. Thanks for using Jamjar";

        (new TextMarketer())->sendSms($valuation->company->user->profile->mobile_number, 'jamjar.com', $smsContentsToPartner);
        (new TextMarketer)->sendSms($profile->mobile_number, 'jamjar.com', $smsContentsToUser);

        /**
         * Email Notifications
         */
        event(new SaleCreated($sale));

        session(['got-valuation-for-'.$vrm->numberplate.'-from-company-'.$company->id => true]);

        alert()->success('Thanks for choosing '.$company->name.'. They will contact you within 3 working hours to arrange payment and handover.')->persistent("Great!");
    }

    public function showDrafts(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();
        $this->guardAgainstVehicleOwnership($vehicle);

        $offeredValuesService = new OfferedValueService();
        $offeredValues = $offeredValuesService->getOfferedValuesByVehicle($vehicle);

        foreach($offeredValues as $key=>$offeredValue) {

            if(!$offeredValue->company){
                unset($offeredValues[$key]);
                continue;
            }

            if ($offeredValue->company->name == 'CarTakeBack' && $offeredValue->isCollectionAvailable() && $offeredValue->isDropOffAvailable()) {


                libxml_use_internal_errors(true);
                $body = simplexml_load_string((string)$offeredValue->external_partner_response);

                if(!$body){
                    continue;
                }

                $results = (array)$body->valuations;

                $valuations = collect($results)->map(
                    function ($value, $key) use ($body) {
                        $hasCollectionValuation = isset($value[0]);
                        $hasDropoffValuation = isset($value[1]);
                        return [
                            'collection' => $hasCollectionValuation == true ? (int) $value[0]->amount : null,
                            'dropoff'   => $hasDropoffValuation == true ? (int) $value[1]->amount : null,
                        ];
                    }
                );


                $offeredValue->collection_part = true;
                $offeredValue->dropoff_part = false;
                $offeredValue->scrap = true;
                $offeredValue->collection_new_price = $valuations['valuation']['collection'];

                $oldOfferedValue = clone $offeredValue;
                $oldOfferedValue->scrap = true;
                $oldOfferedValue->dropoff_part = true;
                $oldOfferedValue->collection_part = false;
                $oldOfferedValue->dropoff_new_price = $valuations['valuation']['dropoff'];

                $offeredValues[] = $oldOfferedValue;

            } else {

                $offeredValue->scrap = false;
            }


        }

        usort($offeredValues, function($valueA, $valueB) {

            $value1 = $valueA->getPriceInPence()/100;

            //Reorder for CarTakeBack
            if($valueA->dropoff_part) {
                $value1 = $valueA->dropoff_new_price;
            }

            if($valueA->collection_part) {
                $value1 = $valueA->collection_new_price;
            }


            $value2 = $valueB->getPriceInPence()/100;

            //Reorder for CarTakeBack
            if($valueB->dropoff_part) {
                $value2 = $valueB->dropoff_new_price;
            }

            if($valueB->collection_part) {
                $value2 = $valueB->collection_new_price;
            }

            if($value1 > $value2) {

                return -1;
            }

            if($value1 == $value2) {

                return 0;
            }

            if($value1 < $value2) {

                return 1;
            }


        });

        return view('matrix.valuation-drafts.show-drafts', [
            'vehicle' => $vehicle,
            'offeredValues' => $offeredValues,
        ]);
    }


    public function showVehicleEditForm(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();
        $this->guardAgainstVehicleOwnership($vehicle);
        $this->guardAgainstCompletedSale($vehicle);

        return view('matrix.valuation-drafts.vehicle-information', [
            'vehicle' => $vehicle,
            'defects' => Defect::all(),
            'colors' => Colour::all(),
            'serviceHistories' => VehicleMeta::getAvailableServicesHistories(),
            'mots' => VehicleMeta::getAvailableMots(),
            'writeOffCategories' => VehicleMeta::getAvailableWriteOffCategories(),
            'isEditMode' => true,
        ]);
    }

    public function editVehicle(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();
        $this->guardAgainstVehicleOwnership($vehicle);
        $this->guardAgainstCompletedSale($vehicle);

        $this->validate($request, [
            'service-history' => 'required',
            'mot' => 'required',
            'mileage' => 'required|numeric|min:1000|max:' . LeadsController::MAX_AVAILABLE_MILEAGE,
            'car-colour' => 'required',
            'write-off' => [
                'required',
                new RelatedFieldNotInRule($request->input('write-off-category'), ['none'])
            ],
            'non-runner' => [
                'required',
                new NonRunnerReasonsRequire($request->input('non-runner-reason')),
            ],
        ]);

        $reasonIds = [];
        if ($request->input('non-runner-reason')) {
            foreach ($request->input('non-runner-reason') as $key => $value) {
                $reasonIds[] = $key;
            }
        }

        if ($request->input('non-runner') && !Defect::areNonMechanicalDefectsOnly($reasonIds)) {
            $vehicle->meta->non_runner = true;
            $reasons = [];
            foreach ($reasonIds as $reasonId) {
                $reason = Defect::where('id', '=', $reasonId)->first();
                if (in_array($reason->title, Defect::getNonMechanicalReasons())) {
                    continue;
                }
                $reasons[] = [
                    'id' => $reason->id,
                    'title' => $reason->title,
                ];
            }
            $vehicle->meta->non_runner_reason = serialize($reasons);
        } else {
            $vehicle->meta->non_runner = false;
            $vehicle->meta->non_runner_reason = serialize([]);
        }

        foreach ($request->all() as $key => $value) {
            session()->put([$key => $value]);
        }

        $vehicle->meta->mileage = $request->input('mileage');
        $vehicle->meta->service_history = $request->input('service-history');
        $vehicle->meta->mot = $request->input('mot');
        $vehicle->meta->color = $request->input('car-colour');
        $vehicle->meta->write_off = (bool)$request->input('write-off');
        $vehicle->meta->write_off_category = $request->input('write-off-category');
        $vehicle->meta->save();

        $vehicle->is_visible_on_marketplace = false;
        $vehicle->added_to_marketplace_at = null;
        $vehicle->save();

        $vehicleService = new VehicleService();
        $vehicleService->removeOldValuations($vehicle, $vehicle);

        $this->valuationsCollector->removeAllDrafts($vehicle);
        $this->valuationsCollector->createValuationDrafts($vehicle);

        $capApi = new CapApi;
        $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();

        if ($capValuation) {
            $vehicle->setCapValues($capValuation);
            $vehicle->save();
        }

        return redirect(route('matrixValuationDraftsShow', [$vehicle->id]));
    }

    public function getFeeForSeller(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();
        $this->guardAgainstVehicleOwnership($vehicle);

        $askPrice = (int) $request->get('value');

        $fee = $this->getFeeInPenceForSellerForAddingToMarketplace($vehicle, $askPrice * 100);

        return $this->jsonResponse(['fee' => (float) round($fee / 100, 2)]);
    }

    private function guardAgainstUnwantedPartnerToPartnerSales(ValuationDraft $draft)
    {
        if (!$draft->getCompany()->isBuyingVehiclesFromAnotherPartners()) {
            alert()->error('Sorry, this partner does not accept offers from another companies.')->persistent();

            throw new NotAllowedException();
        }
    }

    private function guardAgainstInsufficientFundsForSeller(OfferedValueInterface $offeredValue)
    {
        $calculator = new PartnerFeeCalculator(0, $offeredValue->getPriceInPence() / 100);

        $feeInPence = $calculator->getFeeForSeller(true, $offeredValue->getSellerCompany(), true);
        $sellerFundsInPence = $offeredValue->getSellerCompany()->user->funds->getOriginal('funds');

        if ($sellerFundsInPence < $feeInPence) {
            alert()->error('You do not have enough funds to purchase this lead. Please add more.')->persistent();

            throw new InsufficientFundsException();
        }
    }



    private function guardAgainstInsufficientFundsForBuyer(OfferedValueInterface $offeredValue)
    {
        $calculator = new PartnerFeeCalculator($offeredValue->getListingPosition(), $offeredValue->getPriceInPence() / 100);

        $feeInPence = $calculator->calculate(true, $offeredValue->getBuyerCompany(), true);
        $buyerFunds = $offeredValue->getBuyerCompany()->user->funds->getOriginal('funds');

        if ($buyerFunds < $feeInPence) {
            alert()->error('Sorry, this valuation is not available.')->persistent();

            throw new NotAllowedException();
        }
    }

    private function chargeSellerForOfferedValue(Valuation $valuation, OfferedValueInterface $offeredValue)
    {
        $calculator = new PartnerFeeCalculator(0, $offeredValue->getPriceInPence() / 100);
        $feeInPence = $calculator->getFeeForSeller(true, $offeredValue->getSellerCompany(), true);
        $statement = PartnerFeeCalculator::createStatementEntry($valuation, $offeredValue->getSellerCompany(), $offeredValue->getVehicle(), $feeInPence);
        $statement->setTransactionLabel('Sale');
        $statement->save();

        $company = $offeredValue->getSellerCompany();

        if ($company->user->funds) {
            $company->user->funds->spendFunds($feeInPence);
        }
    }

    private function chargeBuyerForOfferedValue(Valuation $valuation, OfferedValueInterface $offeredValue)
    {
        if ($offeredValue->getBuyerCompany()->company_type == 'api') {
            $valuation->fee = 0;
            $valuation->save();

            return;
        }

        $company = $offeredValue->getBuyerCompany();

        if (!$company->user->funds) {
            return;
        }

        $calculator = new PartnerFeeCalculator($offeredValue->getListingPosition(), $offeredValue->getPriceInPence() / 100);
        $feeInPence = $calculator->getFeeForSeller(true, $offeredValue->getBuyerCompany(), true);
        PartnerFeeCalculator::createStatementEntry($valuation, $offeredValue->getBuyerCompany(), $offeredValue->getVehicle(), $feeInPence);

        $company->user->funds->spendFunds($feeInPence);

        $valuation->fee = $feeInPence;
        $valuation->save();
    }

    private function guardAgainstInsufficientFundsForSellerByVehicle(Vehicle $vehicle, int $sellerFeeInPence)
    {
        $sellerFundsInPence = $vehicle->owner->user->funds->getOriginal('funds');

        if ($sellerFundsInPence < $sellerFeeInPence) {
            alert()->error('You do not have enough funds to add this vehicle to the marketplace. Please add more.')->persistent();

            throw new InsufficientFundsException();
        }
    }

    private function chargeSellerForAddingToMarketplace(Vehicle $vehicle, int $feeInPence)
    {
        $company = $vehicle->owner->user->company;
        $statement = PartnerFeeCalculator::createStatementEntryForSeller($company, $vehicle, $feeInPence);
        $statement->setTransactionLabel('Listed');
        $statement->save();

        if ($company->user->funds) {
            $company->user->funds->spendFunds($feeInPence);
        }
    }

    private function guardAgainstVehicleOwnership(Vehicle $vehicle)
    {
        if (!$vehicle || $vehicle->getLatestOwnerAttribute()->user_id != auth()->user()->id) {
            alert()->error('Vehicle not found.')->persistent();

            throw new NotAllowedException();
        }
    }

    private function guardAgainstNoDrafts($offeredValues)
    {
        if (empty($offeredValues)) {
            alert()->error('There are no valuation drafts for this vehicle.')->persistent();

            throw new NotAllowedException();
        }
    }

    private function guardAgainstDraftOwnership(OfferedValueInterface $offeredValue)
    {
        if (!$offeredValue || $offeredValue->getSellerCompany()->user_id != auth()->user()->id) {
            alert()->error('Valuation not found.')->persistent();

            throw new NotAllowedException();
        }
    }

    private function guardAgainstExpiredOffer(OfferedValueInterface $offeredValue)
    {
        if ($offeredValue->isExpired()) {
            alert()->error('Offer Expires.')->persistent();

            throw new NotAllowedException();
        }
    }

    private function showVehicleExistsAlert(Vehicle $previouslyAddedVehicle)
    {
        $buttons = [
            'view' => ['text' => "View", 'className' => 'redirect:' . route('matrixValuationDraftsShow', [$previouslyAddedVehicle->id], false)],
            'update' => ['text' => "Update", 'className' => 'redirect:' . route('matrixValuationDraftsShowEditVehicleForm', [$previouslyAddedVehicle->id], false)],
        ];

        \Session::flash('sweet_alert.alert', json_encode([
            'title' => "Vehicle already exists",
            'text' => 'It looks like you already have a lookup for this vehicle. Would you like view your existing lookup or edit your vehicle details for a new lookup?',
            'icon' => "warning",
            'buttons' => $buttons,
        ]));
    }

    private function guardAgainstVehicleMarketplacePosition(Vehicle $vehicle)
    {
        if ($vehicle->isVisibleOnMarketplace()) {
            alert()->error('Vehicle has been already added to the marketplace.')->persistent();

            throw new NotAllowedException();
        }
    }

    private function getFeeInPenceForSellerForAddingToMarketplace(Vehicle $vehicle, int $askingPriceInPence): int
    {
        $highestValuationDraft = ValuationDraft::where([
            ['vehicle_id', '=', $vehicle->id]
        ])->orderBy('price_in_pence', 'DESC')->first();

        $baseFeePriceInPence = $highestValuationDraft ? $highestValuationDraft->getPriceInPence() : $askingPriceInPence;

        $calculator = new PartnerFeeCalculator(0, $baseFeePriceInPence / 100);

        return $calculator->getFeeForSeller(true, $vehicle->owner->company, true);
    }

    private function guardAgainstExistingSale(OfferedValueInterface $offeredValue)
    {
        if ($offeredValue->getSale()) {
            alert()->error('Sale for this valuation has been created already. Check your email box.');

            throw new NotAllowedException();
        }
    }

    private function guardAgainstCompletedSale(Vehicle $vehicle)
    {
        if ($vehicle->hasCompleteSale()) {
            alert()->error('This sale is complete, you are no longer able to edit this vehicle.');

            throw new NotAllowedException();
        }
    }

    private function getAdditionalVehicleValidation(): array
    {
       return [
            'keys-count' => 'required|numeric|min:1|max:3',
            'service-history-information' => 'string|max:255|nullable',
            'additional-specification-information' => 'string|max:255|nullable',
            'is-log-book' => 'required|boolean',
            'is-in-stock' => 'required|boolean',
            'stock-availability-at' => 'nullable|date_format:Y-m-d',
        ];
    }

    private function updateAdditionalVehicleInformation(Request $request, Vehicle $vehicle)
    {
        $vehicle->setKeysCount((int) $request->get('keys-count'));
        $vehicle->setServiceHistoryInformation($request->get('service-history-information'));
        $vehicle->setAdditionalSpecificationInformation($request->get('additional-specification-information'));
        $vehicle->setIsLogBook((bool) $request->get('is-log-book'));
        $vehicle->setIsInStock((bool) $request->get('is-in-stock'));
        $stockAvailabilityAt = $request->get('stock-availability-at') === null ?
            null :
            Carbon::createFromFormat('Y-m-d', $request->get('stock-availability-at'));

        $vehicle->setStockAvailabilityAt($stockAvailabilityAt);
        $vehicle->save();
    }

    private function addSearchByNumberplate(Request $request, array $whereColumns)
    {
        if ($searchedNumberplate = $request->get('searched-numberplate')) {
            $searchedNumberplate = str_replace(' ', '', $searchedNumberplate);
            $whereColumns[] = ['vehicles.numberplate', '=', strtoupper($searchedNumberplate)];
        }

        return $whereColumns;
    }

    private function addSearchByNumberplateStatement(Request $request, Builder $vehicles): Builder
    {
        if ($searchedNumberplate = $request->get('searched-numberplate')) {
            $searchedNumberplate = str_replace(' ', '', $searchedNumberplate);
            $vehicles->where('vehicles.numberplate', '=', strtoupper($searchedNumberplate));
        }

        return $vehicles;
    }

    public function sellItTrade()
    {
        return view('matrix.valuation-drafts.sell-it-trade-test');
    }

    public function sellItTradeCreateDealer(SellItTradeIntegration $sitIntegration)
    {
        $response = $sitIntegration->createDealer();

        if($response->d->Valid == true){
            print_r('Dealer Created'); die;
        }

        print_r($response->d->ErrorMessage); die;
    }

    public function sellItTradeLoginDealer(SellItTradeIntegration $sitIntegration)
    {
        $response = $sitIntegration->loginDealer();

        if($response->d->Valid == true){
            print_r("<a href=".$response->d->RedirectUrl.">".$response->d->RedirectUrl."</a>");
        }
        print_r($response->d->ErrorMessage); die;
    }

    public function sellItTradeAddTradeValuation(Request $request, SellItTradeIntegration $sitIntegration)
    {
        $regNumber = $request->get('reg_number');
        $mileage = (int)$request->get('mileage');

        $vehicle = (new Vehicle)->createVehicleFromApiData($regNumber);
        $vehicle->meta->mileage = $mileage;
        $vehicle->save();

        $response = $sitIntegration->addCarDetails($vehicle)
            ->valueVehicle();

        if($response->d->Valid == true){
            print_r("<a href=".$response->d->RedirectUrl.">".$response->d->RedirectUrl."</a>");
        }
        print_r($response->d->ErrorMessage); die;
    }

    public function sellItTradeGetValuation(Request $request, SellItTradeIntegration $sellItTradeIntegration)
    {
        //create a Sell It Trade dealer
        try {
            $response = $sellItTradeIntegration->createDealer();

            if($response->d->Valid == false && $response->d->ErrorMessage != 'UserAlreadyRegistered'){
                throw new Exception('Cannot create Sell It Trade Dealer'. $response->d->ErrorMessage);
            }
        } catch (Exception $e){
            \Log::error('Sell It Trade Create Dealer: '. $e->getMessage());
        }


        $regNumber = $request->get('reg_number');
        $mileage = (int)$request->get('mileage');

        $vehicle = (new Vehicle)->createVehicleFromApiData($regNumber);
        $vehicle->meta->mileage = $mileage;
        $vehicle->save();

        try {
            $response = $sellItTradeIntegration->addCarDetails($vehicle)
                ->valueVehicle();
            if($response->d->Valid == false){
                \Log::error('Cannot create Sell It Trade Valuation: '.$response->d->ErrorMessage);
            }
        } catch (Exception $e) {
            \Log::error('Sell It Trade Get Valuation'. $e->getMessage());
            alert('Cannot create a valuation');
        }

        return redirect($response->d->RedirectUrl);
    }

    private function cleanApiResponse($data)
    {
        if (!is_numeric($data)) {
            return trim(ucwords(strtolower($data)));
        }
    }
}
