<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use JamJar\Model\VehicleImage;
use JamJar\Services\EloquentUploadedImageManager;
use JamJar\Services\ImagesManagement\ImagesManagerInterface;
use JamJar\Vehicle;
use Illuminate\Support\Facades\Validator;

class VehicleImagesController extends Controller
{
    /** @var EloquentUploadedImageManager */
    private $uploadedImageManager;

    /** @var ImagesManagerInterface */
    private $imagesManager;

    public function __construct(
        EloquentUploadedImageManager $uploadedImageManager,
        ImagesManagerInterface $imagesManager
    ) {
        $this->uploadedImageManager = $uploadedImageManager;
        $this->imagesManager = $imagesManager;
    }

    public function addVehicleImage(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        if (!$vehicle) {
            return $this->jsonResponse([], 404);
        }

        if (count($vehicle->getImages()) >= 8) {
            return $this->jsonResponse(['message' => 'Sorry, you can not upload more than 8 images.'], 400);
        }

        $file = $request->file('file');
        $fileContent = file_get_contents($file->getRealPath());

        $data = [
            'original_name' => $file->getClientOriginalName(),
            'image' => $fileContent,
        ];

        $validation = [
            'original_name' => 'required|min:5|max:1024',
            'image' => 'required',
        ];

        $validator = Validator::make($data, $validation);

        if ($validator->fails()) {
            return $this->jsonResponse(['errors' => $validator->messages()->messages()], 400);
        }

        $uploadedImage = $this->uploadedImageManager->create(
            $data['original_name'],
            'active'
        );

        $this->imagesManager->uploadImage($data['image'], $uploadedImage);
        $vehicleImage = new VehicleImage();
        $vehicleImage->uploaded_image_id = $uploadedImage->id;
        $vehicleImage->vehicle_id = $vehicle->id;
        $vehicleImage->order_number = 1;
        $vehicleImage->save();

        return $this->jsonResponse([
            'data' => [
                'vehicleImage' => $vehicleImage->toArray(),
                'uploadedImage' => $uploadedImage->toArray(),
                'imagePath' => route('image', [$uploadedImage->id(), $uploadedImage->originalName()]),
                'thumbnailPath' => route('thumbnail', [$uploadedImage->id(), $uploadedImage->originalName()]),
                'imageBoxHtml' => (string) View::make('matrix.valuation-drafts.single-vehicle-image', ['image' => $vehicleImage]),
            ]
        ]);
    }

    public function removeVehicleImage(Request $request, int $imageId)
    {
        $vehicleImage = VehicleImage::where('id', '=', $imageId)->first();

        if (!$vehicleImage) {
            return $this->jsonResponse([], 404);
        }

        $vehicleImage->delete();

        return $this->jsonResponse([]);
    }

}
