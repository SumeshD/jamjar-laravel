<?php

namespace JamJar\Http\Controllers\Matrix;

use JamJar\PartnerLocation;
use Newsletter;
use JamJar\User;
use JamJar\Postcode;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use JamJar\Events\PartnerCreated;
use JamJar\Http\Requests\StorePartner;

class UsersController extends Controller
{
    /**
     * Instantiate a new instance of Matrix\UsersController
     */
    public function __construct()
    {
        $this->middleware(['auth','partner'])->except(['create', 'store']);
    }

    /**
     * Show the Application Form for a Matrix Partner
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->check()) {
            if (auth()->user()->isPartner()) {
                return redirect()->route('partnerDashboard');
            } else {
                alert()->error('To become an associate you must create a new Associate only account by logging out and re-visiting this page.')->persistent();
                return redirect()->route('dashboard');
            }
        }
        return view('matrix.become-partner', ['applyPage' => true]);
    }

    /**
     * Store a new Matrix Partner
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePartner $request)
    {
        $partner = (new User)->registerUser($request->name, $request->email, $request->password, ['is_partner' => true]);
        
        $partner->update([
            'api_token' => str_random(60),
            'api_partner_id' => Uuid::uuid4()->toString()
        ]);

        $address = $partner->profile->update([
            'address_line_one' => $request->input('address_line_one'),
            'address_line_two' => $request->input('address_line_two'),
            'city' => $request->input('city'),
            'town' => $request->input('city'),
            'county' => $request->input('county'),
            'postcode' => $request->input('postcode'),
            'telephone_number' => $request->input('telephone_number'),
            'mobile_number' => $request->input('telephone_number'),
            'is_scrap_dealer' => $request->is_scrap_dealer == 'on' ? true : false,
        ]);
        
        $this->handleMarketingPreferences($request->all(), $partner);

        event(new PartnerCreated($partner, $request->all()));

        alert()
            ->success("Thanks for applying to become an associate. We will be in touch!")
            ->persistent('Close');

        return redirect()->route('becomeAPartner', ['applied' => true]);
    }
    
    /**
     * Display a form to edit the users account
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        /** @var User $user */
        $user = auth()->user();
        $user->load('company');
        if (!$user->company) {
            alert()->error(
                '
                    Sorry!
                    You don\'t have permission to access that part of the dashboard yet. 
                    Perhaps your account is pending approval?
                '
            )
                ->html()
                ->persistent();
            return redirect()->route('partnerDashboard');
        }

        $postcodes = Postcode::select(['postcode', 'county', 'country'])->get()->groupBy('country');

        $locations = $user->getActiveLocations();

        return view('matrix.account', compact('user', 'postcodes', 'locations'));
    }

    /**
     * Update a Partner in the Database
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->postcodes) {
            $postcodes = [];
            foreach ($request->postcodes as $key => $value) {
                $postcodes[] = $key;
            }
            $postcodes = implode(',', $postcodes);
        }

        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'mobile_number' => 'required|max:20|phone:GB,mobile,fixed_line',
            'company_telephone' => 'max:20|phone:GB',
        ]);

        $user = auth()->user();

        $user->profile()->update([
            'name' => request()->input('name'),
            'email' => request()->input('email'),
            'mobile_number' => request()->input('mobile_number'),
        ]);

        $user->company->update([
            'name'  => $request->company_name,
            'telephone' => $request->company_telephone,
            'vat_number' => $request->vat_number,
        ]);

        $user->email = request()->input('email');
        $user->save();

        alert()->success('Your account has been updated.');
        return redirect()->route('partnerDashboard');
    }

    /**
     * Handle the given marketing preferences (or lack thereof)
     *
     * @param  array $request
     * @return void
     */
    protected function handleMarketingPreferences($request, $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        if (!isset($request['marketing']) || $request['marketing'] == false) {
            return false;
        }

        $marketingOptions = ['email' => true, 'telephone' => true, 'sms' => true];

        foreach ($marketingOptions as $key => $value) {
            $user->update(["marketing_{$key}" => (int)$value]);
        }

        /*
        $name = $this->formatName($request['name']);

        $mergeFields = [
            'MMERGE1' => $name['firstname'],
            'MMERGE2' => $name['lastname'],
            'MMERGE3' =>
                $request['address_line_one'] . ' ' .
                $request['address_line_two'] . ' ' .
                $request['postcode'] . ' ' .
                $request['city'] . ' ' .
                $request['county'],
            'MMERGE4' => $request['email'],
            'MMERGE6' => $request['telephone_number'],
            'MMERGE9' => $request['postcode'],
        ];

        Newsletter::subscribe($request['email'], $mergeFields, 'matrix_subscribers');
        */

    }

    /**
     * Format the given name.
     *
     * @param  string $name
     * @return array
     */
    private function formatName($name)
    {
        $fullname = explode(' ', $name);

        if (array_key_exists(1, $fullname)) {
            $firstname = $fullname[0];
            $lastname = $fullname[1];
        } else {
            $firstname = $name;
            $lastname = ' ';
        }

        return compact('firstname', 'lastname');
    }
}
