<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Http\Request;
use JamJar\DashboardAlert;
use JamJar\Events\PartnerCreated;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlert;
use JamJar\Sale;
use JamJar\User;
use JamJar\AcceptedUserDashboardAlerts;
use JamJar\Valuation;
use JamJar\Vehicle;

class DashboardController extends Controller
{
    /**
     * Instantiate a new instance of Matrix\Controller
     */
    public function __construct()
    {
        $this->middleware('partner');
    }

    /**
     * Display the Matrix Partner Dashboard
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->isPartner()) {
            alert()
                ->info(
                    "
                    Your account hasn't yet been approved as a Partner.
                    Once your account has been approved, you will have access to the dashboard
                "
                )
                ->autoclose(9000000);
        }

        $agreedPurchases = Sale::where([
                ['sales.company_id', '=', auth()->user()->company->id],
                ['sales.status', '=', 'pending'],
            ])
            ->orderBy('sales.created_at', 'DESC')
            ->get();

        $automatedBidsCount = Valuation::where([
            ['company_id', '=', auth()->user()->company->id],
            ['is_created_by_automated_bids', '=', true],
            ['created_at', '>=', Vehicle::getGoodNewsLeadVehicleDisappearDate()],
        ])->count();

        $outbidOnCount = count(Valuation::getOutbidValuationsByUser(auth()->user()));
        $outstandingValues = auth()->user()->getSuggestedValues();
        $marketplaceAlerts = $this->getMarketplaceAlerts();
        $lastReceivedBids = $this->getLastReceivedBids();

        $listedVehicles = $this->getListedVehicles();
        $vehiclesWithAgreedSale = $this->getVehiclesWithAgreedSales();

        return view('matrix.dashboard.index', [
            'agreedPurchases'           => $agreedPurchases,
            'automatedBidsCount'        => $automatedBidsCount,
            'outbidOnCount'             => $outbidOnCount,
            'outstandingValues'         => $outstandingValues,
            'marketplaceAlerts'         => $marketplaceAlerts,
            'lastReceivedBids'          => $lastReceivedBids,
            'listedVehicles'            => $listedVehicles,
            'vehiclesWithAgreedSale'    => $vehiclesWithAgreedSale,
        ]);
    }

    private function getMarketplaceAlerts(): array
    {
        /** @var MarketplaceAlert[] $vehicleAlerts */
        $vehicleAlerts = MarketplaceAlert::where([
            ['user_id', '=', auth()->user()->id],
            ['is_enabled', '=', true],
        ])->get();

        $marketplaceAlerts = [];

        foreach ($vehicleAlerts as $alert) {
            $marketplaceAlerts[] = [
                'alert' => $alert,
                'vehicleCount' => $alert->getActiveVehiclesCount($alert),
            ];
        }

        usort($marketplaceAlerts, function($a, $b) {
            if ($a['vehicleCount'] == $b['vehicleCount']) {
                return 0;
            }

            return ($a['vehicleCount'] > $b['vehicleCount']) ? -1 : 1;
        });

        return $marketplaceAlerts;
    }

    /** @return Valuation[] */
    private function getLastReceivedBids()
    {
        /** @var Vehicle[] $partnerListedVehicles */
        $partnerListedVehicles = Vehicle::select('vehicles.*')->where([
            ['vehicle_owners.user_id', '=', auth()->user()->id],
            ['vehicles.added_to_marketplace_at', '>=', Vehicle::getGoodNewsLeadVehicleDisappearDate()],
        ])
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->join('valuations', 'valuations.vehicle_id', '=', 'vehicles.id')
            ->get();

        $lastReceivedBids = [];

        foreach ($partnerListedVehicles as $listedVehicle) {
            if ($listedVehicle->hasCompleteSale() || $listedVehicle->hasPendingSale()) {
                continue;
            }

            if ($highestNonExpiredValuation = $listedVehicle->getHighestNonExpiredValuation()) {
                $lastReceivedBids[] = $highestNonExpiredValuation;
            }
        }

        usort($lastReceivedBids, function(Valuation $a, Valuation $b) {
            if ($a->created_at == $b->created_at) {
                return 0;
            }

            return ($a->created_at > $b->created_at) ? -1 : 1;
        });

        return $lastReceivedBids;
    }

    /** @return array|Vehicle[] */
    private function getListedVehicles(): array
    {
        $marketplaceVehicles = Vehicle::select('vehicles.*')
            ->where([
                ['vehicle_owners.user_id', '=', auth()->user()->id],
                ['vehicles.is_finished', '=', true],
                ['vehicles.is_visible_on_marketplace', '=', true],
                ['vehicles.added_to_marketplace_at', '>=', Vehicle::getGoodNewsLeadVehicleDisappearDate()],
            ])
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->get();

        $listedVehicles = [];

        foreach ($marketplaceVehicles as $marketplaceVehicle) {
            if (!$marketplaceVehicle->hasCompleteSale()) {
                $listedVehicles[] = $marketplaceVehicle;
            }
        }

        return $listedVehicles;
    }

    private function getVehiclesWithAgreedSales()
    {
        $vehicles = Vehicle::select('vehicles.*')
            ->where([
                ['vehicle_owners.user_id', '=', auth()->user()->id],
                ['vehicles.is_finished', '=', true],
                ['vehicles.created_at', '>=', Vehicle::getGoodNewsLeadVehicleDisappearDate()],
            ])
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->get();

        $vehiclesWithAgreedSales = [];

        foreach ($vehicles as $vehicle) {
            if ($vehicle->hasPendingSale()) {
                $vehiclesWithAgreedSales[] = $vehicle;
            }
        }

        return $vehiclesWithAgreedSales;
    }

    public function getAlertForUser() {
        $dashboardAlert = DashboardAlert::where('status','1')->first();
        if(!$dashboardAlert){
            return $this->jsonResponse([
                'status' => false
            ]);
        }

        $userDashboardAlert = AcceptedUserDashboardAlerts::where('user_id', auth()->user()->id)->where('dashboard_alert_id', $dashboardAlert->id)->first();

        if($userDashboardAlert) {
            return $this->jsonResponse([
                'status' => false
            ]);
        }

        return $this->jsonResponse([
            'status' => true,
            'id' => $dashboardAlert->id,
            'subject' => $dashboardAlert->subject,
            'text' => $dashboardAlert->text
        ]);
    }

    public function acceptAlert(DashboardAlert $alert) {
        $acceptedAlert = new AcceptedUserDashboardAlerts(['user_id' => auth()->user()->id, 'dashboard_alert_id' => $alert->id]);
        $acceptedAlert->save();

        return $this->jsonResponse(['status' => true]);
    }
}
