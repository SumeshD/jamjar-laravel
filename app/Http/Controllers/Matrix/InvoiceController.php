<?php

namespace JamJar\Http\Controllers\Matrix;

use JamJar\Exceptions\NotAllowedException;
use JamJar\Http\Controllers\Controller;
use JamJar\Order;

class InvoiceController extends Controller
{
    public function index()
    {
        $orders = Order::where('user_id', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        return view('matrix.invoices.index', compact('orders'));
    }

    public function show(Order $order)
    {
        $this->guardAgainstOwnership($order);

        return view('matrix.invoices.show', compact('order'));
    }

    private function guardAgainstOwnership(Order $order)
    {
        if (!auth()->user() || ($order->getUser()->id != auth()->user()->id && !auth()->user()->isAdmin())) {
            alert()->error('Page not found')->persistent();

            throw new NotAllowedException();
        }
    }
}
