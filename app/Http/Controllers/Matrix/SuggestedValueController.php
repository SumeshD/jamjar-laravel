<?php

namespace JamJar\Http\Controllers\Matrix;

use JamJar\Exceptions\NotAllowedException;
use JamJar\Offer;
use JamJar\OfferDerivative;
use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\SuggestedValue;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;

class SuggestedValueController extends Controller
{
    /**
     * Display all suggested values for this offer
     * 
     * @param Offer $offer
     * @return Illuminate\View\View
     */
    public function show(Offer $offer) 
    {
        $this->guardAgainstOwnership($offer);

        $offerDerivatives = OfferDerivative::where('offer_id', '=', $offer->id)->get();

        $vehicleManufacturer = VehicleManufacturer::where([
            ['cap_manufacturer_id', '=', $offer->getCapManufacturerId()],
            ['vehicle_type', '=', strtoupper($offer->getVehicleType())],
        ])->first();

        if (!$vehicleManufacturer) {
            alert()->error('Sorry we can not find this manufacturer, error VO1001')->persistent();
            throw new NotAllowedException();
        }

        $derivativeSets = [];

        foreach ($offerDerivatives as $offerDerivative) {
            // there is very small chance to get incorrect suggested value, we don't keep cap_manufacturer_id and vehicle type in suggested values
            $suggestedValue = SuggestedValue::where([
                ['cap_model_id', '=', $offerDerivative->cap_model_id],
                ['cap_derivative_id', '=', $offerDerivative->cap_derivative_id],
            ])->first();

            if (!$suggestedValue) {
                continue;
            }

            $vehicleModel = VehicleModel::where([
                ['internal_manufacturer_id', '=', $vehicleManufacturer->getId()],
                ['cap_model_id', '=', $offerDerivative->cap_model_id],
            ])->first();

            if (!$vehicleModel) {
                \Log::debug('problem with vehicle cap model id ' . $offerDerivative->cap_model_id);
                continue;
            }

            $vehicleDerivative = VehicleDerivative::where([
                ['internal_model_id', '=', $vehicleModel->getId()],
                ['cap_derivative_id', '=', $offerDerivative->cap_derivative_id],
            ])->first();

            if (!$vehicleDerivative) {
                \Log::debug('problem with vehicle cap derivative id ' . $offerDerivative->cap_derivative_id);
                continue;
            }

            $derivativeSets[] = [
                'offerDerivative' => $offerDerivative,
                'vehicleManufacturer' => $vehicleManufacturer,
                'vehicleModel' => $vehicleModel,
                'vehicleDerivative' => $vehicleDerivative,
                'suggestedValue' => $suggestedValue,
            ];
        }

        return view('matrix.offers.suggested.show', compact('offer', 'derivativeSets'));
    }

    public function update(Offer $offer, Request $request) 
    {
        $this->guardAgainstOwnership($offer);

        foreach ($request->derivatives as $id => $baseValue) {
            $derivative = OfferDerivative::find($id);
            $this->guardAgainstOwnership($derivative->offer);
            $derivative->update([
                'base_value' => (int)$baseValue
            ]);
            cache()->forget('derivatives-for-model-'.$derivative->cap_model_id.'-and-user-'.$offer->user_id.'-and-offer-'.$offer->id);
        }
        alert()->success('Suggested Values Updated');
        return redirect()->route('partnerOffers');
    }

    private function guardAgainstOwnership(Offer $offer)
    {
        if (auth()->user()->id != $offer->user->id) {
            alert()->error('Page not found')->persistent();
            throw new NotAllowedException();
        }
    }
}
