<?php

namespace JamJar\Http\Controllers\Matrix;

use Illuminate\Support\Facades\Cache;
use JamJar\Company;
use JamJar\Exceptions\NotAllowedException;
use JamJar\Jobs\CacheAutomatedBids;
use JamJar\Jobs\UpdateAutomatedBidsVehicles;
use JamJar\Offer;
use JamJar\Colour;
use JamJar\Defect;
use JamJar\PlateYear;
use Ramsey\Uuid\Uuid;
use JamJar\OfferModel;
use JamJar\VehicleModel;
use JamJar\OfferDerivative;
use Illuminate\Http\Request;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use Symfony\Component\HttpFoundation\JsonResponse;

class OffersController extends Controller
{
    /**
     * Instantiate a new instance of Matrix\OffersController
     */
    public function __construct()
    {
        $this->middleware(['auth', 'partner']);
    }

    /**
     * Display a list of Offers
     *
     * @return \Illuminate\View\View
     */
    public function index(Offer $offer)
    {
        $funds = auth()->user()->funds->funds;
        $company = Company::select('postcode_area')->where('user_id',auth()->id())->first();

        //$paused = Offer::with(['manufacturer'])->where('user_id', auth()->id())->where('active', 0)->where('finished', 1)->get();

        $missing = [];
        if(floatval($funds)==0) {

            $missing[]='funds';
        }

        if($company->postcode_area==null) {

            $missing[]='postcode';
        }

        return view('matrix.offers.index',compact('missing'));
    }

    /**
     * Show an offer
     *
     * @param  Offer $offer
     * @return Illuminate\View\View
     */
    public function show(Offer $offer)
    {
        return view('matrix.offers.show', compact('offer'));
    }

    private function guardAgainstFundsPostcode() {

        $funds = auth()->user()->funds->funds;
        $company = Company::select('postcode_area')->where('user_id',auth()->id())->first();

        $missing = [];
        if(floatval($funds)==0) {

            $missing = ['funds'];

        }

        if($company->postcode_area==null) {

            $missing = ['postcode'];
        }

        if(!empty($missing)) {

            return false;
        }

        return true;

    }

    /**
     * Create a new offer
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        if(!$this->guardAgainstFundsPostcode()) {

            return redirect()->route('partnerOffers');
        }

        $colours = Colour::all();
        $defects = Defect::all();
        $manufacturers = VehicleManufacturer::all();
        $plateYears = PlateYear::orderBy('id', 'DESC')->get();
        $offer = auth()->user()->offers()->latest()->first();
        if (null === $offer) {
            $offer = collect();
            $offer->name = '';
            $offer->slug = '';
        }

        return view('matrix.offers.create', compact(['colours', 'defects', 'manufacturers', 'plateYears', 'offer']));
    }

    /**
     * Store an offer
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'base_value' => 'required|max:200|numeric',
            'minimum_mileage' => 'required|numeric',
            'maximum_mileage' => 'required|numeric',
            'minimum_value' => 'required|numeric',
            'maximum_value' => 'required|numeric',
            'minimum_service_history' => 'required',
            'minimum_mot' => 'required',
            'maximum_previous_owners' => 'required',
            'acceptable_colors' => 'required'
        ]);

        $offer = Offer::create(
            [
                'name'  => $request->get('name') ?? ' ',
                'user_id' => auth()->id(),
                'slug' => $request->get('slug') ?? Uuid::uuid4()->toString(),
                'base_value' => $request->get('base_value'),
                'fuel_type' => $request->get('fuel_type'),
                'vehicle_type' => $request->get('vehicle_type') ?? 'CAR',
                'minimum_mileage' => trim(str_replace(',', '', $request->get('minimum_mileage'))),
                'maximum_mileage' => trim(str_replace(',', '', $request->get('maximum_mileage'))),
                'minimum_value' => trim(str_replace(',', '', $request->get('minimum_value'))),
                'maximum_value' => trim(str_replace(',', '', $request->get('maximum_value'))),
                'minimum_service_history' => $request->get('minimum_service_history'),
                'minimum_mot' => $request->get('minimum_mot'),
                'maximum_previous_owners' => $request->get('maximum_previous_owners'),
                'accept_write_offs' => (bool)$request->get('accept_write_offs'),
                'acceptable_colors' => $request->get('acceptable_colors'),
                'acceptable_defects' => $request->get('acceptable_defects'),
                'great_condition_percentage_modifier' => $request->get('great_condition_percentage_modifier') ?? 100,
                'good_condition_percentage_modifier' => $request->get('good_condition_percentage_modifier') ?? 80,
                'avg_condition_percentage_modifier' => $request->get('avg_condition_percentage_modifier') ?? 60,
                'poor_condition_percentage_modifier' => $request->get('poor_condition_percentage_modifier') ?? 40,
                'minimum_age' => $request->get('minimum_age'),
                'maximum_age' => $request->get('maximum_age'),
                'maximum_write_off_category' => $request->get('maximum_write_off_category'),
                'manufacturer_id' => $request->get('manufacturer_id') ?? null,
                'model_ids' => $request->get('model_ids') ?? null,
                'derivative_ids' => $request->get('derivative_ids') ?? null,
                'active' => $request->get('active') ?? 0,
            ]
        );

        //CacheAutomatedBids::dispatch($offer);

        return redirect()->route('partnerOfferEdit', $offer);
    }

    /**
     * Edit Offer
     *
     * @param  JamJar\Offer $offer
     * @return Illuminate\View\View
     */
    public function edit(Offer $offer)
    {
        $colours = Colour::all();
        $defects = Defect::all();
        $plateYears = PlateYear::all();

        $colorIds = collect();
        $defectIds = collect();

        $selectedDerivatives = $offer->derivative_ids_flipped_array;
        $selectedModels = $offer->model_ids_flipped_array;

        $offer->load(['models', 'derivatives']);

        /**
        * If applicable, return a a list of accepted colours for this offer.
        */
        if ($offer->acceptable_colors) {
            $offer->acceptable_colors->each(
                function ($color) use ($colorIds) {
                    $colorIds->push($color->id);
                }
            );
        }

        /**
        * If applicable, return a list of accepted defects for this offer.
        */
        if ($offer->acceptable_defects) {
            $offer->acceptable_defects->each(
                function ($defect) use ($defectIds) {
                    $defectIds->push($defect->id);
                }
            );
        }

        return view('matrix.offers.edit', compact('offer', 'colours', 'defects', 'plateYears', 'colorIds', 'defectIds', 'selectedModels', 'selectedDerivatives'));
    }

    /**
     * [editVehicles description]
     *
     * @param  Offer $offer [description]
     * @return [type]        [description]
     */
    public function editVehicles(Offer $offer)
    {

        $data = [
            'offer' => $offer
        ];

        return view('matrix.offers.edit-vehicles', $data);
    }

    function getContents($str, $startDelimiter, $endDelimiter) {
        $contents = array();
        $startDelimiterLength = strlen($startDelimiter);
        $endDelimiterLength = strlen($endDelimiter);
        $startFrom = $contentStart = $contentEnd = 0;
        while (false !== ($contentStart = strpos($str, $startDelimiter, $startFrom))) {
            $contentStart += $startDelimiterLength;
            $contentEnd = strpos($str, $endDelimiter, $contentStart);
            if (false === $contentEnd) {
                break;
            }
            $contents[] = substr($str, $contentStart, $contentEnd - $contentStart);
            $startFrom = $contentEnd + $endDelimiterLength;
        }

        return $contents;
    }

    /**
     * [updateVehicles description]
     *
     * @param  Offer   $offer   [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateVehicles(Offer $offer, Request $request)
    {
        //UpdateAutomatedBidsVehicles::dispatch($offer, $request);

        // Collect the currently selected models and derivatives
        $selectedModels = $offer->model_ids_flipped_array;
        $selectedDerivatives = $offer->derivative_ids_flipped_array;

        // build some collections of models and derivatives to delete, we'll come to these later.
        $modelsToDelete = collect();
        $derivativesToDelete = collect();

        $data = json_decode($request->data,true);


        $newData = [];
        foreach($data as $key=>$item) {

            if(strpos($key,'models') !== false) {

                $insideData = $this->getContents($key,'[',']');
                $newData['models'][$insideData[0]] =[$insideData[1]=>$insideData[0]];

            }

            if(strpos($key,'derivatives') !== false) {

                $insideData = $this->getContents($key,'[',']');

                if(isset($newData['derivatives']) && isset($newData['derivatives'][$insideData[0]])){
                    $newData['derivatives'][$insideData[0]][$insideData[1]] = $item;
                }else{
                    $newData['derivatives'][$insideData[0]] = [$insideData[1]=>$item];
                }


            }


        }

        // loop through submitted models
        foreach ($newData['models'] as $key => $model) {

            if (!isset($model['model_id'])) {
                // push previously selected models which are now unselected to our deletion queue
                if (isset($selectedModels[$key])) {
                    $modelsToDelete->push($key);
                }
                // skip as they are unselected
                continue;
            }

            // save the model submitted
            $offer->models()->updateOrCreate(
                [
                    'cap_model_id' => $model['model_id']
                ],
                [
                    'cap_model_id' => $model['model_id'],
                    'base_value' => $offer->base_value
                ]
            );
        }



        foreach($newData['models'] as $model) {

            $found = false;
            foreach($selectedModels as $key=>$model2) {

                if($model['model_id']==$key) {

                    $found = true;
                }
            }

            if($found) {

                unset($selectedModels[$model['model_id']]);
            }
        }

        $modelsToDelete = array_keys($selectedModels);



        // loop through submitted derivatives
        foreach ($newData['derivatives'] as $key => $derivative) {
            // if they don't have a derivative ID
            if (!isset($derivative['derivative_id'])) {
                // push previously selected derivatives which are now unselected to our deletion queue
                if (isset($selectedDerivatives[$key])) {
                    $derivativesToDelete->push($key);
                }
                // skip as they are unselected
                continue;
            }

            $parts = explode('-', $derivative['derivative_id']);
            $modelId = (int)$parts[0];
            $derivativeId = (int)$parts[1];
            $base_value = (int)$derivative['base_value'];


            $offer->derivatives()->updateOrCreate(
                [
                    'cap_model_id' => $modelId,
                    'cap_derivative_id' => $derivativeId
                ],
                [
                    'cap_model_id' => $modelId,
                    'cap_derivative_id' => $derivativeId,
                    'base_value' => $base_value
                ]
            );
        }
        $offer->is_model_value_changed = true;
        $offer->save();
        cache()->forget('derivatives-for-model-'.$modelId.'-and-user-'.auth()->id().'-and-offer-'.$offer->id);
        cache()->forget('offer-'.$offer->slug);
        // Delete models which have been unselected
        OfferModel::whereIn('cap_model_id', $modelsToDelete)->where('offer_id', $offer->id)->delete();
        // Delete derivatives which have been unselected
        OfferDerivative::whereIn('cap_derivative_id', $derivativesToDelete->toArray())->where('offer_id', $offer->id)->delete();
        // send the user on their way

        //CacheAutomatedBids::dispatch($offer);

        return response(['success' => true], 200);
        //return redirect()->route('partnerOffers');
    }

    /**
     * Update an offer
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\Offer            $offer
     * @return Illuminate\Http\Redirect
     */
    public function update(Request $request, Offer $offer)
    {
        $this->validate($request, [
            'minimum_mileage' => 'numeric|min:0',
            'maximum_mileage' => 'numeric',
            'minimum_value' => 'numeric|min:0',
            'maximum_value' => 'numeric',
        ]);

        /**
        * Update the main parts of the offer,
        * excluding the parts we need to do custom code for.
        */
        $offer->update(
            $request->except([
                'derivative_ids', 'base_value', 'colours', 'maximum_write_off_category', 'editVehicles', 'unchecked', 'save', 'showFacebook', 'showTawk'
            ])
        );

        /**
         * If we have no defects submitted,
         * remove all defects from the rule
         */
        if (is_null($request->acceptable_defects)) {
            $offer->update([
                'acceptable_defects' => [],
            ]);
        }

        if ($request->has('maximum_write_off_category')) {

            $offer->update(['maximum_write_off_category' => $request->maximum_write_off_category]);
        }

        /**
        * Update Acceptable Colours
        */
        if ($request->colours) {
            $colors = [];
            foreach ($request->colours as $color) {
                $colors[] = (int)$color;
            }
        }


        //CacheAutomatedBids::dispatch($offer);
        /**
        * Redirect user back to the edit page
        */
        if ($request->has('editVehicles')) {
            return redirect()->route('partnerOfferEditVehicles', $offer);
        } else {
            return redirect()->route('partnerOffers');
        }
    }

    /**
     * Delete an Offer
     *
     * @param  Offer $offer
     * @return Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        cache()->forget('offers-for-'.auth()->id());


        /*
        $minAge = (int)explode('-', $offer->minimum_age)[0];
        $maxAge = (int)explode('-', $offer->maximum_age)[0];
        $cacheKey = 'vehicles-'.$offer->slug.'-'.$offer->manufacturer_id.'-'.$offer->fuel_type.'-'.$minAge.'-'.$maxAge;
        cache()->forget($cacheKey);
        $cacheKey2 = 'derivatives-'.$offer->slug.'-'.$offer->manufacturer_id.'-'.$offer->fuel_type.'-'.$minAge.'-'.$maxAge;
        cache()->forget($cacheKey2);
        */

        $offer->delete();
        alert()->success('Rule Deleted');
        return back();
    }

    /**
     * Complete an Offer
     *
     * @param  JamJar\Offer $offer
     * @return Illuminate\Http\Response
     */
    public function complete(Offer $offer)
    {
        $offer->active = 1;
        $offer->save();
        alert()->success('Your rule is now live.');
        return redirect(route('partnerOffers'));
    }

    /**
     * Pause an Offer
     *
     * @param  JamJar\Offer $offer
     * @return Illuminate\Http\Response
     */
    public function pause(Offer $offer)
    {
        $offer->active = 0;
        $offer->save();
        alert()->success('Your rule is now paused.');
        return redirect(route('partnerOffers'));
    }

    public function updateOfferValuesForAllVehicles(Offer $offer, Request $request)
    {
        $this->guardAgainstOwnership($offer);
        if($request->data){
            $data = json_decode($request->data,true);
            if(is_numeric($data['set-all']) && $data['set-all'] > 0) {
                $offer->derivatives()->update(['base_value' => $data['set-all']]);
                $offer->models()->update(['base_value' => $data['set-all']]);
                $offer->update(['base_value' => $data['set-all'], 'is_model_value_changed' => false]);
                $responseArray = [
                    'updated' => true
                ];
            }

            $responseArray = [
                'success' => true,
                'updated' => false
            ];

            return new JsonResponse($responseArray);
        } else {
            if(is_numeric($request->post('set-all')) && $request->post('set-all') > 0) {
                $offer->derivatives()->update(['base_value' => $request->post('set-all')]);
                $offer->models()->update(['base_value' => $request->post('set-all')]);
                $offer->update(['base_value' => $request->post('set-all'), 'is_model_value_changed' => false]);
            }

            return redirect(route('partnerOffers'));
        }
    }

    public function updateOfferValuesForAllRules(Request $request) {
        if(is_numeric($request->post('set-all')) && $request->post('set-all') > 0) {
            foreach (Offer::where(['user_id' => auth()->user()->id])->cursor() as $offer) {
                $offer->derivatives()->update(['base_value' => $request->post('set-all')]);
                $offer->models()->update(['base_value' => $request->post('set-all')]);
                $offer->update(['base_value' => $request->post('set-all'), 'is_model_value_changed' => false]);
            }
        }

        return redirect(route('partnerOffers'));
    }



    private function guardAgainstOwnership(Offer $offer)
    {
        if (auth()->user()->id != $offer->user->id) {
            alert()->error('Page not found')->persistent();
            throw new NotAllowedException();
        }
    }
}
