<?php

namespace JamJar\Http\Controllers\Matrix;

use Paypal;
use JamJar\User;
use JamJar\Order;
use JamJar\Valuation;
use JamJar\UserStatement;
use JamJar\FundingAmount;
use Illuminate\Http\Request;
use JamJar\Events\OrderReceived;
use JamJar\Traits\InteractsWithPayPal;

class BillingController extends Controller
{
    use InteractsWithPayPal;

    private $paypal;

    /**
     * Instantiate the trait
     */
    public function __construct()
    {
        $this->paypal = Paypal::ApiContext(
            getenv('PAYPAL_CLIENT_ID'),
            getenv('PAYPAL_CLIENT_SECRET')
        );

        $this->paypal->setConfig(
            [
            'mode'  => getenv('PAYPAL_ENV'),
            'service.EndPoint' => getenv('PAYPAL_ENV') === 'sandbox' ? 'https://sandbox.paypal.com' : 'https://api.paypal.com',
            'http.ConnectionTimeOut' => 60,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
            ]
        );

        $this->middleware('partner');
    }

    /**
     * Display a listing of Credits in the Partners Account
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\View\View
     */
    public function index(Request $request, FundingAmount $amount)
    {
        $amounts = FundingAmount::orderBy('price')->get();
        $amounts->first()->description = '(Administration fee)';
        $statements = auth()->user()->statements->sortByDesc('created_at');
        
        return view('matrix.credits.index', compact(['amounts', 'statements']));
    }

    /**
     * Allow the partner to purchase credits
     *
     * @todo   Move into BillingController@index
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\View\View
     */
    public function show(Request $request)
    {
        $amount = FundingAmount::find($request->get('amount'));
        return view('matrix.credits.purchase', compact('amount'));
    }

    /**
     * Process an XMLHttpRequest from PayPal as a part of the OAuth flow
     *
     * @uses   JamJar\Traits\InteractsWithPayPal::payWithPayPal
     * @param  Illuminate\Http\Request $request
     * @return PayPal\Api\Payment
     */
    public function process(Request $request)
    {
        $amount = FundingAmount::where('id', decrypt($request->get('productId')))->first();

        $response = $this->payWithPayPal($amount);

        return $response;
    }

    /**
     * Process the Payment Recieved Authorization from PayPal and update Partner's Account
     *
     * @uses   JamJar\Traits\InteractsWithPayPal::processPayment
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response = $this->processPayment($request);
        
        $amount = FundingAmount::where('id', decrypt($response->getData()->productId))->first();

        // add credits to users account, trigger event etc
        auth()->user()->funds->addFunds($amount->getOriginal('price'));

        // create
        $order = Order::create(
            [
            'product_name' => $amount->price,
            'product_price' => $amount->getOriginal('price'),
            'pp_payment_id' => $response->getData()->paymentID,
            'pp_payer_id' => $response->getData()->payerID,
            'user_id' => auth()->user()->id,
            'status' => $response->getData()->success,
            ]
        );

        $statement = new UserStatement;

        $statement->create(
            [
            'user_id' => auth()->user()->id,
            'type' => 'deposit',
            'amount' => $amount->getOriginal('price'),
            'meta' => json_encode(compact('order'))
            ]
        );

        // Fire an event
        event(new OrderReceived(auth()->user(), $amount));

        // trigger a lovely alert
        alert()->success('Thanks for your payment! Your funds have been added to your account');
        return $response;
    }

    /**
     * Process an error from PayPal (Usually Cancellation)
     *
     * @return Illuminate\Http\Response
     */
    public function destroy()
    {
        return response()->json(['error'], 422);
    }
}
