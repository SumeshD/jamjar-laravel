<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JamJar\Company;
use JamJar\Events\PartnerCreated;
use JamJar\PartnerLocation;
use JamJar\Primitives\Money;
use JamJar\Role;
use JamJar\User;
use JamJar\VehicleOwner;

class PartnersController extends Controller
{
    /**
     * Display index of partners
     *
     * @param  \JamJar\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $user->load('profile');
        $roleId = Role::select('id')->where('name', 'Partner')->first();
        $users = $user->where('matrix_partner', 1)->where('role_id', $roleId->id)->paginate(20);


        return view('admin.partners.index', compact('users'));
    }

    /**
     * Create a new partner
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partners.create');
    }

    /**
     * Store a new partner
     *
     * @param  \Illuminate\Http\Request $request
     * @param  JamJar\User              $user
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        try{
            $partner = $user->registerUser($request->name, $request->email, $request->password, ['verified' => true]);
        } catch (\Exception $exception){
            \Log::debug('Partner:'.$exception->getMessage());
        }

        event(new PartnerCreated($partner, $request->all()));

        $partner->role_id = 3;
        $partner->matrix_partner = 1;
        $partner->save();

        $partner->profile->telephone_number = $request->telephone_number;
        $partner->profile->mobile_number = $request->telephone_number;
        $partner->profile->save();

        $this->updateMatrixLocationDetails(PartnerLocation::where('user_id', '=', $partner->id)->first(), $partner);

        alert()->success('Partner Added!');

        return redirect(route('adminPartners'));
    }

    /**
     * Display an edit form for the partner
     *
     * @param  \JamJar\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.partners.edit', compact('user'));
    }

    /**
     * Update a user record
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \JamJar\User             $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // Check if email is more than 1
        $users = User::where('email', request()->input('email'))->get();

        // Check if email is more than 1
        if(sizeof($users) > 0 && request()->input('email') != $user->email){
            alert()->error('Email address already taken.');
            return back();
        }

        $user->update(
            [
            'name'  => $request->name,
            'verified' => (boolean)$request->verified ? 1 : 0,
            ]
        );

        $user->profile()->update(
            [
            'name' => $request->name,
            'email' => $request->email,
            'telephone_number' => $request->telephone_number,
            'address_line_one' => $request->address_line_one,
            'address_line_two' => $request->address_line_two,
            'town' => $request->town,
            'city' => $request->city,
            'county' => $request->county,
            'postcode' => $request->postcode,
            'use_flat_fee' => (boolean)$request->use_flat_fee ? 1 : 0,
            'is_scrap_dealer' => (boolean)$request->is_scrap_dealer ? 1 : 0
            ]
        );

        $user->profile->flat_fee = (boolean)$request->use_flat_fee == 1 ? (float) $request->flat_fee : null;
        $user->profile->save();

        $user->company()->update(
            [
            'name' => $request->company_name,
            'vat_number' => $request->vat_number,
            'telephone' => $request->telephone_number,
            'address_line_one' => $request->address_line_one,
            'address_line_two' => $request->address_line_two,
            'town' => $request->town,
            'city' => $request->city,
            'county' => $request->county,
            'postcode' => $request->postcode,
            ]
        );

        $user->email = $request->email;
        $user->save();

        $vehicleOwners = VehicleOwner::where('user_id', '=', $user->id)->get();

        foreach ($vehicleOwners as $vehicleOwner) {
            $vehicleOwner->email = $request->email;
            $vehicleOwner->save();
        }

        alert()->success('User Updated');
        return back();
    }

    /**
     * Delete a user record
     *
     * @param  \JamJar\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        alert()->success("User deleted.");
        return back();
    }

    /**
     * Return to the original logged in user
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function returnToOriginalUser()
    {
        if (session('is-impersonating-user') === true) {
            auth()->loginUsingId(session('original-user')->id);
            return back();
        }
    }

    /**
     * Login as user
     *
     * @param User $user
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function loginAsUser(User $user)
    {
        if (auth()->user()->isAdmin()) {
            session('is-impersonating-user', true);
            session('original-user', auth()->user());

            auth()->loginUsingId($user->id);
            alert()->success('You are now logged in as '. $user->name);
            return redirect()->route('partnerDashboard');
        } else {
            alert()->error('Unauthorized');
            return back();
        }
    }

    private function updateMatrixLocationDetails(PartnerLocation $location, User $user)
    {
        $company = $user->company;

        $company->address_line_one = $location->address_line_one;
        $company->address_line_two = $location->address_line_two;
        $company->town = $location->town;
        $company->city = $location->city;
        $company->county = $location->county;
        $company->postcode = $location->postcode;

        $company->save();

        $user->profile->address_line_one = $location->address_line_one;
        $user->profile->address_line_two = $location->address_line_two;
        $user->profile->town = $location->town;
        $user->profile->city = $location->city;
        $user->profile->county = $location->county;
        $user->profile->postcode = $location->postcode;

        $user->profile->save();
    }
}
