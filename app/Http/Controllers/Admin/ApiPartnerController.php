<?php

namespace JamJar\Http\Controllers\Admin;

use JamJar\Company;
use JamJar\Postcode;
use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;

class ApiPartnerController extends Controller
{
    /**
     * Display the list of auto partners
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $companies = Company::where('company_type', 'api')->paginate(25);
        return view('admin.api-partner.index', compact('companies'));
    }

    /**
     * Toggle the active state on each company
     *
     * @param  JamJar\Company $company
     * 
     * @return Illuminate\Http\Response
     */
    public function toggle(Company $company)
    {
        $state = $company->active == 1 ? 0 : 1;
        $action = $state == 1 ? 'Resumed' : 'Paused';
        $company->update(
            [
            'active' => $state
            ]
        );
        alert()->success($company->name . ' ' . $action);
        return back();
    }

    /**
     * Display an edit form for the partner
     *
     * @param  \JamJar\Company $company
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.api-partner.edit', compact('company'));
    }

    /**
     * Update a user record
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \JamJar\Company             $company   
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $company->update(
            [
                'name' => $request->company_name,
                'vat_number' => $request->vat_number,
                'telephone' => $request->telephone_number,
                'address_line_one' => $request->address_line_one,
                'address_line_two' => $request->address_line_two,
                'town' => $request->town,
                'city' => $request->city,
                'county' => $request->county,
                'postcode' => $request->postcode,
                'valuation_validity' => $request->valuation_validity,
                'bank_transfer_fee' => $request->bank_transfer_fee,
                'admin_fee' => $request->admin_fee,
            ]
        );

        alert()->success('Company Updated');

        return back();
    }
}
