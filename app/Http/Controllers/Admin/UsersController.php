<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JamJar\User;
use JamJar\Vehicle;
use JamJar\VehicleOwner;

class UsersController extends Controller
{
    /**
     * Show an index of users.
     *
     * @param  JamJar\User $user
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $search = (string) $request->search;

        $users = User::select('*');

        if ($search) {
            $users = $users->where('email', 'like', '%' . $search . '%')
                ->orWhere('name', 'like', '%' . $search . '%');
        }

        $users = $users->paginate(20);

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show a single user.
     * 
     * @param JamJar\User $user 
     * 
     * @return Illuminate\View\View
     */
    public function show(User $user)
    {
        $user->load('profile');

        $vehicles = Vehicle::select('vehicles.*')
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->where('vehicle_owners.user_id', '=', $user->id)
            ->orderBy('vehicles.created_at', 'DESC')
            ->get();

        return view('admin.users.show', compact('user', 'vehicles'));
    }

    /**
     * Display a form to create a user.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a user in the database
     *
     * @param  Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        if (User::where('email', $request->email)->exists()) {
            alert()->error('A user with this email address already exists');
            return back();
        }

        $user->registerUser($request->name, $request->email, $request->password);

        $user->update([
            'matrix_partner' => (boolean)$request->matrix_partner ? 1 : 0,
            'verified' => (boolean)$request->verified ? 1 : 0,
        ]);

        return redirect(route('adminUsers'));
    }

    /**
     * Display an edit view
     *
     * @param  JamJar\User $user
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update a user record
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\User    $user
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // Check if email is more than 1
        $users = User::where('email', request()->input('email'))->get();

        // Check if email is more than 1
        if(sizeof($users) > 0 && request()->input('email') != $user->email){
            alert()->error('Email address already taken.');
            return back();
        }

        $oldIsMatrix = $user->matrix_partner;
        $newIsMatrix = $request->matrix_partner === null ? $oldIsMatrix : (bool) $request->matrix_partner;

        $user->update([
            'name'    => $request->name,
            'email'    => $request->email,
            'matrix_partner' => $newIsMatrix ? 1 : 0,
            'verified' => (boolean)$request->verified ? 1 : 0,
            'send_additional_emails' => (bool) $request->send_additional_emails,
        ]);

        $user->profile->email = $request->email;
        $user->profile->save();

        $vehicleOwners = VehicleOwner::where('user_id', '=', $user->id)->get();

        foreach ($vehicleOwners as $vehicleOwner) {
            $vehicleOwner->email = request()->input('email');
            $vehicleOwner->save();
        }

        alert()->success('Account Updated');

        return back();
    }

    /**
     * Delete a user record
     *
     * @param  User $user
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        alert()->success("User deleted.");
        return back();
    }

    public function loginAsUser(User $user)
    {
        if (auth()->user()->isAdmin()) {
            session('is-impersonating-user', true);
            session('original-user', auth()->user());

            auth()->loginUsingId($user->id);
            alert()->success('You are now logged in as '. $user->name);
            return redirect()->route('/');
        } else {
            alert()->error('Unauthorized');
            return back();
        }
    }

}
