<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\PartnerWebsite;
use JamJar\User;
use Storage;

class AutoPartnerWebsiteController extends Controller
{
    /**
     * Show the form for customising the website
     *
     * @param  JamJar\User $partners
     * 
     * @return Illuminate\View\View
     */
    public function create(User $partner)
    {
        $openingtimes = unserialize($partner->website->getPage('contact')->meta);
        return view('admin.auto-partner.website.create', compact('partner', 'openingtimes'));
    }

    /**
     * Save the Website and Page Content
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\User             $partner
     * 
     * @return Illuminate\Http\Response
     */
    public function store(Request $request, User $partner)
    {
        // Validate the request
        $this->validate(
            $request,
            [
            'hero_image' => ['image'],
            'logo_image' => ['image']
            ]
        );

        // Shortcut for the website object
        $website = $partner->website;
        // Upload the submitted images
        $this->uploadImages($request, $website);
        // Save the brand colors
        $this->saveBrandingColors($request, $website);
        // Save Page Content
        $this->savePageContent($request, $website);

        return redirect()->route('autoPartnerIndex');
    }

    /**
     * Upload the images to S3
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\PartnerWebsite   $website
     * 
     * @return JamJar\PartnerWebsite
     */
    protected function uploadImages(Request $request, PartnerWebsite $website)
    {
        $filePath = 'partner-websites/images/' . $website->slug;

        if ($request->hero_image) {
            $heroImage = $request->file('hero_image')->storeAs($filePath, 'hero.jpg', 's3');
            $heroUrl = Storage::url($heroImage);
        } elseif ($website->hero_image) {
            $heroImage = $website->hero_image;
            $heroUrl = $heroImage;
        }

        if ($request->logo_image) {
            $logoImage = $request->file('logo_image')->storeAs($filePath, 'logo.jpg', 's3');
            $logoUrl = Storage::url($logoImage);
        } elseif ($website->logo_image) {
            $logoImage = $website->logo_image;
            $logoUrl = $logoImage;
        }

        $website->update(
            [
            'logo_image'    => $logoUrl ?? 'partner-websites/default/jamjar_partner.jpg',
            'hero_image'    => $heroUrl ?? 'partner-websites/default/hero.jpg',
            ]
        );

        return $website;
    }

    /**
     * Save Branding Colours
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\PartnerWebsite   $website
     * 
     * @return JamJar\PartnerWebsite
     */
    protected function saveBrandingColors(Request $request, PartnerWebsite $website)
    {
        $website->update(
            [
            'bg_color'      => $request->bg_color ?? '000000',
            'brand_color'   => $request->brand_color ?? 'FFFFFF',
            ]
        );

        return $website;
    }

    /**
     * Save Page Content
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\PartnerWebsite   $website
     * 
     * @return JamJar\PartnerWebsite
     */
    protected function savePageContent(Request $request, PartnerWebsite $website)
    {
        foreach ($request->pages as $key => $value) {
            $slug = str_slug($key);
            $page = $website->getPage($slug);
            $page->update(
                [
                'content' => $value,
                'meta'  => serialize($request->get('openingtimes')) ?? $page->meta,
                ]
            );
        }

        return $website;
    }
}
