<?php

namespace JamJar\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use JamJar\Company;
use Illuminate\Support\Facades\DB;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Valuation;

class KpiDataController extends Controller
{
    /**
     * Display a listing of API partners
     * 
     * @param Illuminate\Http\Request $request 
     * 
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->get('from') && $request->get('from') != "") {
            // $from = new Carbon($request->get('from'));
            $from = new Carbon($request->get('from')); 
        } else {
            $from = Carbon::minValue();
        }

        if ($request->get('to') && $request->get('to') != "") {
            // $to = Carbon::createFromFormat('Y-m-d', $request->get('to'));
            $to = new Carbon($request->get('to')); 
        } else {
            $to = new Carbon('now'); 
        }

        $company_id = $request->get('company_id');

        // $companies = Company::select(['id', 'name', 'company_type', 'valuations.created_at'])->where('company_type', 'api')->with('valuation')->whereBetween('valuation.created_at', [$from->toDateTimeString(), $to->toDateTimeString()])->get();
                
        $companies = Company::select(
                'companies.id',
                'companies.name',
                'companies.company_type',
                'valuations.created_at',
                DB::raw('count(distinct valuations.user_id, vehicles.numberplate) as total'),
                DB::raw('count(sales.company_id) as purchased'),
                DB::raw('count(valuations.accept_button_clicked_at) as clicks')
            )
            ->where(function($query) {
                return $query->where([['companies.company_type', 'api'], ['valuations.collection_value', '<>', 'NULL']])
                    ->orWhere([['companies.company_type', 'api'], ['valuations.dropoff_value', '<>', 'NULL']]);
            })
            ->whereBetween('valuations.created_at', [$from->toDateTimeString(), $to->toDateTimeString()])
            ->join('valuations', 'valuations.company_id', '=', 'companies.id')
            ->leftJoin('sales', 'valuations.temp_ref', '=', 'sales.dealer_notes')
            ->leftJoin('vehicles', 'valuations.vehicle_id', '=', 'vehicles.id')
            ->groupBy('companies.name')
            ->get();

        // $companiesPos = Valuation::select('companies.name', 'valuations.position', DB::raw('count(distinct valuations.user_id, vehicles.numberplate) as total'))
        //     ->where(function($query) {
        //         return $query->where([['companies.company_type', 'api'], ['valuations.collection_value', '<>', 'NULL']])
        //             ->orWhere([['companies.company_type', 'api'], ['valuations.dropoff_value', '<>', 'NULL']]);
        //     })
        //     ->whereBetween('valuations.created_at', [$from->toDateTimeString(), $to->toDateTimeString()])
        //     ->join('companies', 'valuations.company_id', '=', 'companies.id')
        //     ->leftJoin('vehicles', 'valuations.vehicle_id', '=', 'vehicles.id')
        //     ->groupBy('companies.name', 'valuations.position')
        //     ->get();


        return view('admin.kpi-data.index', compact(['valuations', 'companies', 'kpi_companies']));
    }
}
