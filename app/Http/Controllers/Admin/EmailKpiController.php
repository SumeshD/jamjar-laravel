<?php

namespace JamJar\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use jdavidbakr\MailTracker\Model\SentEmail;

class EmailKpiController extends Controller
{


    public function index(Request $request) {


        if ($request->get('from') && $request->get('from') != "") {
            // $from = new Carbon($request->get('from'));
            $from = new Carbon($request->get('from'));
        } else {
            $from = Carbon::minValue();
        }

        if ($request->get('to') && $request->get('to') != "") {
            // $to = Carbon::createFromFormat('Y-m-d', $request->get('to'));
            $to = new Carbon($request->get('to') . '23:59:59');
        } else {
            $to = new Carbon('now');
        }

        if($request->get('version') && $request->get('version') != "") {

            $version = $request->get('version');

        } else {
            $version = '%%';
        }

        if($request->get('type') && $request->get('type') != "") {

            $type = $request->get('type');

        } else {
            $type = '%%';
        }

        $fromDateTime = $from->toDateTimeString();
        $toDateTime = $to->toDateTimeString();


        $emailsCount = SentEmail::select('*')
            ->whereBetween('sent_emails.created_at', [$fromDateTime, $toDateTime])
            ->where('version','like',$version)
            ->where('type','like',$type)
            ->where('type','!=','other')
            ->count();

        $emailsOpenedCount = SentEmail::select('*')
            ->whereBetween('sent_emails.created_at', [$fromDateTime, $toDateTime])
            ->where('opens','>',0)
            ->where('version','like',$version)
            ->where('type','like',$type)
            ->where('type','!=','other')
            ->count();

        $emailsOpenedRate = ($emailsCount>0) ? number_format($emailsOpenedCount/$emailsCount * 100, 2) : 0;

        $emailsClickedCount = SentEmail::select('*')
            ->whereBetween('sent_emails.created_at', [$fromDateTime, $toDateTime])
            ->where('clicks','>',0)
            ->where('version','like',$version)
            ->where('type','like',$type)
            ->where('type','!=','other')
            ->count();

        $emailsClickedRate = ($emailsCount>0) ? number_format($emailsClickedCount/$emailsCount * 100, 2) : 0;

        $clickedEmails = SentEmail::select('sent_emails_url_clicked.url', DB::raw('count(sent_emails_url_clicked.clicks) as total_clicks'))
            ->whereBetween('sent_emails.created_at', [$fromDateTime, $toDateTime])
            ->where('version','like',$version)
            ->where('type','like',$type)
            ->where('type','!=','other')
            ->leftJoin('sent_emails_url_clicked','sent_emails_url_clicked.sent_email_id','sent_emails.id')
            ->groupBy('sent_emails_url_clicked.url')
            ->get();


        return view('admin.email-kpi.index',compact(['emailsCount', 'emailsOpenedCount', 'emailsOpenedRate', 'emailsClickedCount', 'emailsClickedRate', 'clickedEmails']));
    }

}