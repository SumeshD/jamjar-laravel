<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\AutoPartnerPricingTier;
use JamJar\Http\Controllers\Controller;
use JamJar\Http\Requests\StoreAutoPartnerPricingTier;
use JamJar\User;

class AutoPartnerPricingController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $partner)
    {
        $tiers = AutoPartnerPricingTier::where('company_id', $partner->company->id)->paginate(50);
        return view('admin.auto-partner.tiers.create', compact('partner', 'tiers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAutoPartnerPricingTier $request, User $partner)
    {
        $tiers = $this->buildTiersArray($request);

        foreach ($tiers as $tier) {
            $from = $this->generateValuationFrom($tier['valuation_to'], $partner);
            AutoPartnerPricingTier::create(
                [
                'name' => '£'.$from . ' - £'. $tier['valuation_to'],
                'valuation_to' => $tier['valuation_to'],
                'valuation_from' => $from,
                'percentage' => $tier['percentage'] * 100,
                'active' => 1,
                'user_id' => $partner->id,
                'company_id' => $partner->company->id
                ]
            );
        }

        alert()->success('Pricing Tiers Saved');
        return back();
    }

    public function destroy(User $partner, AutoPartnerPricingTier $tier)
    {
        $tier->delete();
        return back();
    }

    /**
     * Build a Pricing Tiers Array
     *
     * @param  Request $request
     * 
     * @return array
     */
    protected function buildTiersArray(Request $request)
    {
        $tiers = [];
        
        foreach ($request->valuation_to as $key => $value) {
            $tiers[$key]['valuation_to'] = (int)$value;
        }

        foreach ($request->percentage as $key => $value) {
            $tiers[$key]['percentage'] = $value;
        }

        return $tiers;
    }

    /**
     * Generate a valuation_from for the currently submitted "to" value
     *
     * @param  int $value
     * 
     * @return int
     */
    protected function generateValuationFrom($value, $partner)
    {
        $check = AutoPartnerPricingTier::where('user_id', $partner->id)->where('valuation_to', '<=', $value)->orderBy('valuation_to', 'DESC')->first();
        if (!$check) {
            return 0;
        } else {
            $valuationFrom = (int)$check->valuation_to + 1;
            $higherValuationFrom = AutoPartnerPricingTier::where('valuation_from', $valuationFrom)->first();
            if ($higherValuationFrom) {
                $highest = $value + 1;
                $higherValuationFrom->update(
                    [
                    'name' => '£'.$highest.' - £'.$higherValuationFrom->valuation_to,
                    'valuation_from' => $highest
                    ]
                );
            }

            return $valuationFrom;
        }
    }
}
