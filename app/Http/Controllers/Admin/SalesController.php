<?php

namespace JamJar\Http\Controllers\Admin;

use JamJar\Sale;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    /** Show Accepted Valuations by Matrix or API Partners */
    public function completedList(Request $request)
    {
        $pageName = 'Completed Sales';

        $availablePartnersNames = Sale::getAvailablePartnerNames();
        $availableValuesRanges = $this->getAvailableValuesRanges();

        $whereSearches = Sale::getSearchColumnsByReqest($request);
        list($orderColumn, $orderDirection) = Sale::getOrderingByRequest($request);

        $sales = Sale::join('valuations', 'valuations.id', '=', 'sales.valuation_id')
            ->join('companies', 'valuations.company_id', '=', 'companies.id')
            ->join('vehicles', 'valuations.vehicle_id', '=', 'vehicles.id')
            ->where($whereSearches)
            ->orderBy($orderColumn, $orderDirection)
            ->paginate(20);

        return view('admin.sales.index', compact('pageName', 'sales', 'availablePartnersNames', 'availableValuesRanges'));
    }

    private function getAvailableValuesRanges()
    {
        return [
            'Up to £1,000' => '[0,1000]',
            'Up to £2,500' => '[1001,2500]',
            'Up to £5,000' => '[2501,5000]',
            'Up to £10,000' => '[5001,10000]',
            'Up to £25,000' => '[10001,25000]',
            'Up to £50,000' => '[25001,50000]',
            'Up to £75,000' => '[50001,75000]',
            'Up to £100,000' => '[75001,100000]',
            'Up to £150,000' => '[100001,150000]',
            'Above £150,000' => '[150001,200000000]',
        ];
    }
}
