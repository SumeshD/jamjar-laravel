<?php

namespace JamJar\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use JamJar\Company;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Valuation;

class ValuationsController extends Controller
{
    public function index(Request $request, Valuation $valuation)
    {
        $availableValuesRanges = $this->getAvailableValuesRanges();
        $availableValuationStatuses = Valuation::getAvailableValuationsStatuses();
        $availablePartnersNames = Valuation::getAvailablePartnerNames();

        $valuations = Valuation::getForAdminByRequest($request);

        return view('admin.valuations.index', compact('valuations', 'availableValuesRanges', 'availablePartnersNames', 'availableValuationStatuses'));
    }

    /**
     * Display a listing of API partners
     * 
     * @param Illuminate\Http\Request $request 
     * 
     * @return Illuminate\View\View
     */
    public function apiPartnerIndex(Request $request)
    {
        if ($request->get('from')) {
            $from = Carbon::createFromFormat('Y-m-d', $request->get('from'));
        }

        if ($request->get('to')) {
            $to = Carbon::createFromFormat('Y-m-d', $request->get('to'));
        }

        $company_id = $request->get('company_id');

        $valuations = Valuation::latest()->get()->filter(
            function ($valuation) {
                return $valuation->partner_type == 'api';
            }
        );

        if ($request->get('from')) {
            $valuations = $valuations->reject(
                function ($valuation) use ($from) {
                    return $valuation->created_at->lte($from);
                }
            );
        }

        if ($request->get('to')) {
            $valuations = $valuations->reject(
                function ($valuation) use ($to) {
                    return $valuation->created_at->gte($to);
                }
            );
        }

        if ($request->get('company_id')) {
            $valuations = $valuations->reject(
                function ($valuation) use ($company_id) {
                    return $valuation->company_id != $company_id;
                }
            );
        }

        $valuations = $valuations->load(['vehicle','vehicle.meta','vehicle.owner','vehicle.meta.colour','company'])->paginate(20);

        $companies = Company::select(['id', 'name', 'company_type'])->where('company_type', 'api')->get();

        return view('admin.valuations.api-index', compact(['valuations', 'companies']));
    }

    private function getAvailableValuesRanges()
    {
        return [
            'Up to £1,000' => '[0,1000]',
            'Up to £2,500' => '[1001,2500]',
            'Up to £5,000' => '[2501,5000]',
            'Up to £10,000' => '[5001,10000]',
            'Up to £25,000' => '[10001,25000]',
            'Up to £50,000' => '[25001,50000]',
            'Up to £75,000' => '[50001,75000]',
            'Up to £100,000' => '[75001,100000]',
            'Up to £150,000' => '[100001,150000]',
            'Above £150,000' => '[150001,200000000]',
        ];
    }
}
