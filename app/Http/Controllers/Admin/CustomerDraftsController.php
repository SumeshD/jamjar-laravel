<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Vehicle;
use JamJar\Valuation;

class CustomerDraftsController extends Controller
{

    public function index(Request $request)
    {

        $numberplate = str_replace(' ', '', strtoupper($request->get('numberPlate')));

        $vehicles = Vehicle::select('vehicles.*')
            ->whereRaw('(users.matrix_partner = 0 or vehicle_owners.user_id IS NULL) and users.deleted_at is NULL 
            and (vehicle_meta.vehicle_id IS NOT NULL and (vehicle_meta.mileage IS NOT NULL and vehicle_meta.service_history IS NOT NULL and vehicle_meta.mot IS NOT NULL
            and vehicle_meta.number_of_owners IS NOT NULL and vehicle_meta.color IS NOT NULL and vehicle_meta.write_off IS NOT NULL and vehicle_meta.non_runner IS NOT NULL))')
            ->leftJoin('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->leftJoin('users', 'users.id', '=', 'vehicle_owners.user_id')
            ->leftJoin('vehicle_meta', 'vehicle_meta.vehicle_id', '=', 'vehicles.id')
            ->orderBy('vehicles.created_at', 'DESC');

        if ($numberplate) {
            $vehicles = $vehicles->where('numberplate', '=', $numberplate);
        }

        $vehicles = $vehicles->paginate(20);

        return view('admin.customer-drafts.index',[
            'vehicles' => $vehicles,
        ]);
    }


}