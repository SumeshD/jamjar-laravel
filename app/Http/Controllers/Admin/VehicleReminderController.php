<?php

namespace JamJar\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use JamJar\Company;
use Illuminate\Support\Facades\DB;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Valuation;
use JamJar\VehicleReminder;

class VehicleReminderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->get('from') && $request->get('from') != "") {
            // $from = new Carbon($request->get('from'));
            $from = new Carbon($request->get('from')); 
        } else {
            $from = Carbon::minValue();
        }

        if ($request->get('to') && $request->get('to') != "") {
            // $to = Carbon::createFromFormat('Y-m-d', $request->get('to'));
            $to = new Carbon($request->get('to')); 
        } else {
            $to = new Carbon('now'); 
        }

        $reminders = VehicleReminder::whereBetween('vehicle_reminders.created_at', [
            $from->toDateTimeString(),
            $to->toDateTimeString()
        ])
            ->orderBy('created_at', 'DESC')
            ->paginate(20);

        return view('admin.vehicle-reminder.index', compact(['reminders']));
    }
}
