<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\Http\Requests\StorePricingTier;
use JamJar\PricingTier;
use Validator;

class PricingTiersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiers = PricingTier::orderBy('valuation_from', 'asc')->paginate(25);
        return view('admin.billing.tiers.index', compact('tiers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.billing.tiers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(StorePricingTier $request)
    {
        $tiers = $this->buildTiersArray($request);

        foreach ($tiers as $tier) {
            $from = $this->generateValuationFrom($tier['valuation_to']);
            PricingTier::create(
                [
                'name' => '£'.$from . ' - £'. $tier['valuation_to'],
                'valuation_to' => $tier['valuation_to'],
                'valuation_from' => $from,
                'fee' => $tier['fee'] * 100,
                'active' => 1
                ]
            );
        }

        alert()->success('Pricing Tiers Saved');
        return redirect()->route('adminTiersIndex');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(PricingTier $tier)
    {
        return view('admin.billing.tiers.edit', compact('tier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PricingTier $tier)
    {
        $to = (int)$request->valuation_to;
        $existingTier = PricingTier::where('valuation_to', $to)->first();
        $from = $this->generateValuationFrom($to);
        if (!$existingTier) {
            $tier->update(
                [
                'name' => '£'.$from.' - £'.$to,
                'valuation_from' => $from,
                'valuation_to' => $to,
                'fee' => $request->fee
                ]
            );
            return redirect()->route('adminTiersIndex');
        } else {
            alert()->error('There is already a tier ('.$existingTier->name.') which covers that valuation');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy(PricingTier $tier)
    {
        $tier->delete();
        alert()->success('Pricing Tier Deleted');
        return back();
    }

    /**
     * Pause a pricing tier
     *
     * @param  \JamJar\PricingTier $tier
     * 
     * @return \Illuminate\Http\Response
     */
    public function pause(PricingTier $tier)
    {
        $tier->update(['active' => 0]);
        alert()->success('Pricing Tier Paused');
        return back();
    }

    /**
     * Resume a pricing tier
     *
     * @param  \JamJar\PricingTier $tier
     * 
     * @return \Illuminate\Http\Response
     */
    public function resume(PricingTier $tier)
    {
        $tier->update(['active' => 1]);
        alert()->success('Pricing Tier Resumed');
        return back();
    }

    /**
     * Build a Pricing Tiers Array
     *
     * @param  Request $request
     * 
     * @return array
     */
    protected function buildTiersArray(Request $request)
    {
        $tiers = [];
        
        foreach ($request->valuation_to as $key => $value) {
            $tiers[$key]['valuation_to'] = (int)$value;
        }

        foreach ($request->fee as $key => $value) {
            $tiers[$key]['fee'] = $value;
        }

        return $tiers;
    }

    /**
     * Generate a valuation_from for the currently submitted "to" value
     *
     * @param  int $value
     * 
     * @return int
     */
    protected function generateValuationFrom($value)
    {
        $check = PricingTier::where('valuation_to', '<=', $value)->orderBy('valuation_to', 'DESC')->first();
        if (!$check) {
            return 0;
        } else {
            $valuationFrom = (int)$check->valuation_to + 1;
            $higherValuationFrom = PricingTier::where('valuation_from', $valuationFrom)->first();
            if ($higherValuationFrom) {
                $highest = $value + 1;
                $higherValuationFrom->update(
                    [
                    'name' => '£'.$highest.' - £'.$higherValuationFrom->valuation_to,
                    'valuation_from' => $highest
                    ]
                );
            }

            return $valuationFrom;
        }
    }
}
