<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\ThrottlingWhitelistIp;

class ThrottlingWhitelistIpsController extends Controller
{
    public function index(Request $request)
    {
        $ips = ThrottlingWhitelistIp::get();

        return view('admin.throttling-whitelist-ips.index', compact('ips'));
    }

    public function add(Request $request)
    {
        $ip = $request->ip;
        $note = $request->note;

        $this->validate($request, [
            'ip' => 'required'
        ]);

        $whitelistIp = new ThrottlingWhitelistIp();
        $whitelistIp->ip = $ip;
        $whitelistIp->note = $note;
        $whitelistIp->save();

        alert()->success('New Ip has been added successfully!');

        return redirect(route('adminThrottlingWhitelistIpsList'));
    }

    public function delete(Request $request, string $ipId)
    {
        $whitelistIp = ThrottlingWhitelistIp::where('id', '=', $ipId)->first();

        if ($whitelistIp) {
            $whitelistIp->delete();
        }

        alert()->success("Whitelist ip removed");

        return back();
    }
}
