<?php

namespace JamJar\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use JamJar\Company;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Valuation;
use JamJar\Vehicle;

class ValuationDraftsController extends Controller
{
    public function index(Request $request, Valuation $valuation)
    {
        $numberplate = str_replace(' ', '', strtoupper($request->get('numberPlate')));

        $vehicles = Vehicle::select('vehicles.*')
            ->where([
                ['users.matrix_partner', '=', true],
                ['users.deleted_at', '=', null],
            ])
            ->join('vehicle_owners', 'vehicle_owners.vehicle_id', '=', 'vehicles.id')
            ->join('users', 'users.id', '=', 'vehicle_owners.user_id')
            ->orderBy('vehicles.created_at', 'DESC');

        if ($numberplate) {
            $vehicles = $vehicles->where('numberplate', '=', $numberplate);
        }

        $vehicles = $vehicles->paginate(20);

        return view('admin.valuation-drafts.index',[
            'vehicles' => $vehicles,
        ]);
    }

}
