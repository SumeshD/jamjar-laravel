<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Mail\AutomatedBiddingRuleMatched;
use JamJar\Mail\BalanceLow;
use JamJar\Mail\EmailDebuggerInterface;
use JamJar\Mail\EmailVerification;
use JamJar\Mail\Invoice;
use JamJar\Mail\MarketplaceAlerts\DailyEmail;
use JamJar\Mail\MarketplaceAlerts\InstantEmail;
use JamJar\Mail\MatrixAdminNewPartner;
use JamJar\Mail\MatrixPartnerApproved;
use JamJar\Mail\MatrixPartnerWelcome;
use JamJar\Mail\Money4YourMotorsAccepted;
use JamJar\Mail\NewSaleCreated;
use JamJar\Mail\NewSaleCreatedSellerConfirmation;
use JamJar\Mail\OrderReceived;
use JamJar\Mail\PartnerHasBeenOutbidEmail;
use JamJar\Mail\SaleHasBeenRemovedEmail;
use JamJar\Mail\UpdatedSuggestedValue;
use JamJar\Mail\UserValuationEmail;
use JamJar\Mail\ValuationReminderEmail;
use JamJar\Mail\VehicleAdditionalInformationEmail;
use JamJar\Mail\VehicleOwnerPassword;
use ReflectionClass;

class EmailDebuggerController extends Controller
{
    public function index(Request $request)
    {
        $emails = $this->getAvailableEmailsToDebug();

        return view('admin.email-debugger.index', [
            'emails' => $emails
        ]);
    }

    public function showSingleType(Request $request, string $emailType)
    {
        /** @var EmailDebuggerInterface $emailClass */
        $emailClass = $this->getEmailClassByType($emailType);

        if (!$emailClass) {
            alert()->error('Sorry, email type not found.');

            return redirect()->route('/admin');
        }

        if ($warningMessage = $emailClass::getWarningMessage()) {
            alert()->warning($warningMessage);
        }

        return view('admin.email-debugger.single-type-form', [
            'email' => $emailClass
        ]);
    }

    public function processSingleType(Request $request, string $emailType)
    {
        /** @var EmailDebuggerInterface $emailClass */
        $emailClass = $this->getEmailClassByType($emailType);

        if (!$emailClass) {
            alert()->error('Sorry, email type not found.');

            return redirect()->route('/admin');
        }

        $argumentsFromInput = $request->get('arguments');

        $alternativeArgumentsClasses = $request->get('alternative-arguments-field-class') ?? [];
        $alternativeArgumentsNames = $request->get('alternative-arguments-field-name') ?? [];
        $alternativeArgumentsValues = $request->get('alternative-arguments-field-value') ?? [];

        $arguments = [];

        // convert "regular" fields to email parameters
        $argumentsIterator = 0;
        foreach ($argumentsFromInput as $argumentFromInput) {
            if ($alternativeArgumentsClasses[$argumentsIterator] == 'array') {
                try {
                    $json = json_decode($argumentFromInput, true);
                    if ($json === null) {
                        throw new \Exception('Invalid json format');
                    }
                    $arguments[] = $json;
                } catch (\Exception $e) {
                    alert()->error('Sorry, invalid value for ' .
                        $alternativeArgumentsClasses[$argumentsIterator] .
                        ' -> ' . $argumentFromInput . ' => ' . $e->getMessage()
                    );

                    return back();
                }
            } else if ($alternativeArgumentsClasses[$argumentsIterator] == 'bool') {
                $arguments[] = (bool) $argumentFromInput;
            } else if ($alternativeArgumentsClasses[$argumentsIterator] == 'string') {
                $arguments[] = (string) $argumentFromInput;
            } else if ($alternativeArgumentsClasses[$argumentsIterator] == 'int') {
                $arguments[] = (int) $argumentFromInput;
            } else {
                if (is_array($argumentFromInput)) {
                    $rows = [];
                    foreach ($argumentFromInput as $oneRow) {
                        list($entityClass, $entityId) = explode(';', $oneRow);
                        $rows[] = $entityClass::where('id', '=', $entityId)->first();
                    }
                    $arguments[] = $rows;
                } else {
                    list($entityClass, $entityId) = explode(';', $argumentFromInput);
                    $arguments[] = $entityClass::where('id', '=', $entityId)->first();
                }
            }
            $argumentsIterator++;
        }

        // let's try overwrite some of them when admin provides something unusual
        $alternativeArgumentsIterator = 0;
        foreach ($alternativeArgumentsNames as $alternativeArgumentsName) {
            $fieldName = $alternativeArgumentsName;
            $fieldValue = $alternativeArgumentsValues[$alternativeArgumentsIterator];

            if ($fieldName && $fieldValue) {
                try {
                    $entityClass = $alternativeArgumentsClasses[$alternativeArgumentsIterator]::where($fieldName, 'like', '%'. $fieldValue .'%')
                        ->orderBy('created_at', 'DESC')
                        ->first();

                    if (!$entityClass) {
                        throw new \Exception('record doesnt exist');
                    }
                } catch (\Exception $e) {
                    alert()->error('Sorry, invalid value for ' .
                        $alternativeArgumentsClasses[$alternativeArgumentsIterator] .
                        ' -> ' . $fieldName . ' -> ' . $fieldValue . ' => ' . $e->getMessage()
                    );

                    return back();
                }
                $arguments[$alternativeArgumentsIterator] = $entityClass;
            }

            $alternativeArgumentsIterator++;
        }

        $reflection = new ReflectionClass($emailClass);
        $email = $reflection->newInstanceArgs($arguments);

        return $email->build();
    }

    private function getEmailClassByType(string $emailType): ?string
    {
        foreach ($this->getAvailableEmailsToDebug() as $email) {
            if ($emailType == $email::getType()) {
                return $email;
            }
        }

        return null;
    }

    /** @return array|EmailDebuggerInterface[] */
    private function getAvailableEmailsToDebug(): array
    {
        return [
            BalanceLow::class,
            EmailVerification::class,
            NewSaleCreated::class,
            Invoice::class,
            MatrixAdminNewPartner::class,
            MatrixPartnerApproved::class,
            MatrixPartnerWelcome::class,
            Money4YourMotorsAccepted::class,
            NewSaleCreatedSellerConfirmation::class,
            OrderReceived::class,
            PartnerHasBeenOutbidEmail::class,
            SaleHasBeenRemovedEmail::class,
            UpdatedSuggestedValue::class,
            //UserValuationEmail::class,
            //ValuationReminderEmail::class,
            VehicleAdditionalInformationEmail::class,
//            VehicleOwnerPassword::class, unused? doesnt work
            DailyEmail::class,
            InstantEmail::class,
            AutomatedBiddingRuleMatched::class
        ];
    }
}
