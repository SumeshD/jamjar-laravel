<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\FundingAmount;
use JamJar\Http\Controllers\Controller;

class FundingAmountsController extends Controller
{
    /**
     * Show a listing of all products
     *
     * @param  JamJar\FundingAmount $fundingAmount
     * 
     * @return Illuminate\View\View
     */
    public function index(FundingAmount $fundingAmount)
    {
        $amounts = $fundingAmount->orderBy('price', 'ASC')->paginate(15);
        return view('admin.billing.funding-amounts.index', compact('amounts'));
    }

    /**
     * Create a product
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('admin.billing.funding-amounts.create');
    }

    /**
     * Edit a product
     *
     * @param  JamJar\FundingAmount $amount
     * 
     * @return Illuminate\View\View
     */
    public function edit(FundingAmount $amount)
    {
        return view('admin.billing.funding-amounts.edit', compact('amount'));
    }

    /**
     * Store a product
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\FundingAmount    $fundingAmount
     * 
     * @return Illuminate\Http\Response
     */
    public function store(Request $request, FundingAmount $fundingAmount)
    {
        $fundingAmount->create(
            [
            'price' => request()->get('price'),
            'active' => request()->get('active'),
            ]
        );

        alert()->success('Funding Amount Saved');

        return redirect(route('adminFundingAmounts'));
    }

    /**
     * Update a product
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\FundingAmount    $amount
     * 
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, FundingAmount $amount)
    {
        $price = str_replace(',', '', request()->get('price'));

        $amount->update(
            [
            'price' => $price,
            'active' => request()->get('active'),
            ]
        );

        alert()->success('Funding Amount Updated');

        return redirect(route('adminFundingAmounts'));
    }

    /**
     * Delete a product from storage
     *
     * @param  JamJar\FundingAmount $amount
     * 
     * @return Illuminate\Http\Response
     */
    public function destroy(FundingAmount $amount)
    {
        $amount->delete();
        alert()->success('Funding Amount Deleted');
        return redirect(route('adminFundingAmounts'));
    }


    /**
     * Pause a pricing tier
     *
     * @param  \JamJar\FundingAmount $amount
     * 
     * @return \Illuminate\Http\Response
     */
    public function pause(FundingAmount $amount)
    {
        $amount->update(['active' => 0]);
        alert()->success('Funding Amount Paused');
        return back();
    }

    /**
     * Resume a pricing amount
     *
     * @param  \JamJar\FundingAmount $amount
     * 
     * @return \Illuminate\Http\Response
     */
    public function resume(FundingAmount $amount)
    {
        $amount->update(['active' => 1]);
        alert()->success('Funding Amount Resumed');
        return back();
    }
}
