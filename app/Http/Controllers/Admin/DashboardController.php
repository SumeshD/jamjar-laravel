<?php

namespace JamJar\Http\Controllers\Admin;

use JamJar\DashboardAlert;
use DB;
use League\Flysystem\Exception;

class DashboardController extends Controller
{
    /**
     * Display the Administration Dashboard
     * 
     * @return Illuminate\View\View
     */
    public function index()
    {
        return view('admin.index');
    }

    public function showAlerts(DashboardAlert $dashboardAlert)
    {
        $alerts = DashboardAlert::all()->where('status', '!=', 2)->paginate(15);
        return view('admin.dashboard.index', compact('alerts'));
    }

    public function dashboardAlertCreate() {
        return view('admin.dashboard.alert-create');
    }

    public function dashboardAlertStore(DashboardAlert $dashboardAlert) {
        DB::beginTransaction();
        try {
            DashboardAlert::query()->where('status', '!=', '2')->update(['status' => '0']);
            $dashboardAlert->create(
                [
                    'subject' => request()->get('subject'),
                    'text' => request()->get('text'),
                    'status' => request()->get('active'),
                ]
            );
        } catch (Exception $e) {
            DB::rollBack();
        }

        DB::commit();
        alert()->success('Dashboard Alert Saved');

        return redirect(route('showAlerts'));
    }

    public function dashboardAlertActivate(DashboardAlert $alert) {
        DashboardAlert::query()->where('status', '!=', '2')->update(['status' => '0']);

        $alert->update(['status' => '1']);

        return back();
    }

    public function dashboardAlertDeactivate(DashboardAlert $alert) {
        DashboardAlert::query()->where('status', '!=', '2')->update(['status' => '0']);
        return back();
    }

    public function dashboardAlertDelete(DashboardAlert $alert) {
        $alert->update(['status' => '2']);

        return back();
    }
}
