<?php
namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Api\CapApi;
use JamJar\Vehicle;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;

class ToolsController extends Controller
{
    /**
     * Show the CAP search tool
     * 
     * @return Illuminate\View\View
     */
    public function showCap()
    {
        return view('admin.tools.cap');
    }

    /**
     * Process the search of the CAP database
     * 
     * @return Illuminate\View\View
     */
    public function processCap(Request $request)
    {
        $vehicle = (new Vehicle)->getVehicle($request->vrm);

        if (!$vehicle) {
            alert()->error('No vehicle data returned from CAP');
            return back();
        }

        $vehicle->meta->update(
            [
            'mileage' => $request->mileage
            ]
        );
        $valuation = (new CapApi)->getValuation($vehicle)->first();

        return view('admin.tools.capinfo', compact(['vehicle', 'valuation']));
    }

    /**
     * Show a list of manufacturers from the database
     * 
     * @param Illuminate\Http\Request $request
     * 
     * @return Illuminate\View\View
     */
    public function showManufacturers(Request $request)
    {
        $manufacturers = VehicleManufacturer::paginate(50);
        return view('admin.tools.manufacturers', compact('manufacturers'));
    }

    /**
     * Show a list of models for a given manufacturer. Loaded from CAPAPI
     * 
     * @param Illuminate\Http\Request $request 
     * @param int $manufacturer 
     * 
     * @return Illuminate\View\View
     */
    public function showModels(Request $request, $manufacturer)
    {
        $api = new CapApi;
        $models = collect($api->getModels($manufacturer))->paginate(50);
        $manufacturer = VehicleManufacturer::where('cap_manufacturer_id', $manufacturer)->first();
        return view('admin.tools.models', compact('models', 'manufacturer'));
    }

    /**
     * Show a list of the derivatives for the given model ID, loaded from CAP API
     * 
     * @param Illuminate\Http\Request $request 
     * @param int $model 
     * 
     * @return Illuminate\View\View
     */
    public function showDerivatives(Request $request, $model)
    {
        $api = new CapApi;
        $derivatives = collect($api->getDerivatives($model))->paginate(50);

        return view('admin.tools.derivatives', compact('derivatives'));
    }

    /**
     * Save the data from the CAP API for a given 
     * 
     * @param Illuminate\Http\Request $request 
     * @param int $manufacturer 
     * 
     * @return void
     */
    public function saveCapData(Request $request, $manufacturer)
    {
        $api = new CapApi;
        $models = collect($api->getModels($manufacturer))->paginate(50);
        $models->each(
            function ($model) use ($api, $manufacturer) {
                $this->saveModel($model, $manufacturer);
                $this->saveDerivatives($model);
            }
        );
    }

    /**
     * Save the given model from the CAP API
     * 
     * @param int $model 
     * @param int $manufacturer 
     * 
     * @return JamJar\VehicleModel
     */
    protected function saveModel($model, $manufacturer)
    {
        $fuelType = str_contains($model->get('name'), 'DIESEL') ? 'Diesel' : 'Petrol';

        $introduced = $model->get('introduced');
        $discontinued = $model->get('discontinued') == 0 ? date('Y') : $model->get('discontinued');
        $plateYears = range($introduced, $discontinued);

        return VehicleModel::updateOrCreate(
            [
                'cap_model_id' => $model->get('cap_model_id'),
            ],
            [
                'cap_model_id' => $model->get('cap_model_id'),
                'name' => $model->get('name'),
                'manufacturer_id' => (int) $manufacturer,
                'introduced_date' => $model->get('introduced'),
                'discontinued_date' => $model->get('discontinued'),
                'fuel_type' => $fuelType,
                'plate_years' => implode(',', $plateYears)
            ]
        );
    }

    /**
     * Save the derivative from the CAP API
     * 
     * @param int $model 
     * 
     * @return Illuminate\Support\Collection
     */
    protected function saveDerivatives($model)
    {
        $api = new CapApi;
        $derivatives = $api->getDerivatives($model->get('cap_model_id'));

        foreach ($derivatives as $derivative) {
            $deriv = VehicleDerivative::updateOrCreate(
                [
                'cap_derivative_id' => $derivative->get('cap_derivative_id'),
                ],
                [
                'cap_derivative_id' => $derivative->get('cap_derivative_id'),
                'name' => $derivative->get('name'),
                'model_id' => $model->cap_model_id,
                ]
            );
            $collection = collect()->merge($deriv->load('model'));
        }

        return $collection;
    }
}
