<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\Setting;

class PricingPositionsController extends Controller
{
    /**
     * Display an index of pricing positions which are selectable
     * 
     * @return Illuminate\View\View
     */
    public function index()
    {
        $keys = $this->getSettingKeys();

        $settings = Setting::whereIn('key', $keys)->get();
        if ($settings->isEmpty()) {
            $this->createSettings($keys);
            return redirect()->route('adminPricingPositionsIndex');
        }

        return view('admin.billing.positions.index', compact('settings'));
    }

    /**
     * Create the settings necessary
     * 
     * @return void
     */
    protected function createSettings($keys)
    {
        foreach ($keys as $key) {
            Setting::create(
                [
                'key' => $key,
                'value' => '0'
                ]
            );
        }
    }

    /**
     * Get the setting keys
     * 
     * @return array
     */
    protected function getSettingKeys()
    {
        return [
            'position_cost_1',
            'position_cost_2',
            'position_cost_3',
            'position_cost_4',
            'position_cost_5',
            'position_cost_6',
            'good_news_lead_percentage_modifier',
            'marketplace_with_4_plus_photos'
        ];
    }
}
