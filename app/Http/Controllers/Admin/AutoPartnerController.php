<?php

namespace JamJar\Http\Controllers\Admin;

use JamJar\User;
use JamJar\Company;
use JamJar\Profile;
use JamJar\PartnerLocation;
use Illuminate\Http\Request;
use JamJar\Events\PartnerCreated;
use JamJar\AutoPartnerPricingTier;
use JamJar\Http\Controllers\Controller;
use JamJar\Http\Requests\StoreAutoPartner;

class AutoPartnerController extends Controller
{
    /**
     * Display the list of auto partners
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $partners = Profile::where('auto_partner', 1)->get()->reject(function($partner) {
            return is_null($partner->user);
        })->paginate(25);
        return view('admin.auto-partner.index', compact('partners'));
    }

    /**
     * Display the creation form
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('admin.auto-partner.create');
    }

    /**
     * Display the form to edit the auto partner
     *
     * @return Illuminate\View\View
     */
    public function edit(User $partner)
    {
        $location = $partner->company->location->first();
        return view('admin.auto-partner.edit', compact('partner', 'location'));
    }

    /**
     * Update the requested resource in storage
     *
     * @param  Illuminate\Http\Request $request
     * 
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, User $partner)
    {
        // Check if email is more than 1
        $users = User::where('email', request()->input('email'))->get();

        // Check if email is more than 1
        if(sizeof($users) > 0 && request()->input('email') != $partner->email){
            alert()->error('Email address already taken.');
            return back();
        }

        // Update the Partner's name
        $name = $request->first_name . ' '. $request->last_name;
        $partner->update([
            'name' => $name,
            'email' => $request->email
        ]);

        $partner->profile->update([
            'email' => $request->email
        ]);

        // Update Partner's password
        if ($request->password) {
            $partner->update([
                'password' => Hash::make($request->password)
            ]);
        }

        // Update the Partner's address
        $this->updatePartnerAddress($partner, $request);

        // Update the Partner's Company
        $partner->company->update(
            [
            'name' => $request->company_name,
            'telephone' => $request->telephone_number,
            'vat_number' => $request->vat_number,
            'address_line_one' => $request->address_line_one,
            'address_line_two' => $request->address_line_two,
            'town' => $request->town,
            'city' => $request->city,
            'county' => $request->county,
            'postcode' => $request->postcode,
            'valuation_validity' => $request->valuation_validity,
            'bank_transfer_fee' => $request->bank_transfer_fee,
            'admin_fee' => $request->admin_fee,
            ]
        );

        // Update the Partner's Company Location Fees
        $partner->company->location->first()->update(
            [
            'collection_fee' => $request->collection_fee,
            'allow_collection' => (bool)$request->allow_collection,
            'dropoff_fee' => $request->dropoff_fee,
            'allow_dropoff' => (bool)$request->allow_dropoff
            ]
        );

        alert()->success('Partner updated');
        return redirect()->route('autoPartnerPricing', $partner);
    }

    /**
     * Store the requested resource in storage.
     *
     * @param  JamJar\Http\Requests\StoreAutoPartner $request
     * 
     * @return Illuminate\Http\Response
     */
    public function store(StoreAutoPartner $request)
    {
        $name = $request->first_name . ' '. $request->last_name;
        // Register the Partner (we forgo welcome emails etc.)
        $partner = (new User)->registerUser($name, $request->email, $request->password, ['verified' => true]);
        $partner->update([
            'matrix_partner' => 1,
            'role_id' => 3
        ]);
        // Update the Partner's address
        $this->updatePartnerAddress($partner, $request);
        // Register their Company
        $company = $this->registerCompany($partner, $request);
        // Register their Website
        $this->registerWebsite($partner, $request);
        // Register their Location
        $this->registerLocation($partner, $request, $company);
        // Create user funding row
        $this->createUserFunds($partner);
        // redirect to next page
        alert()->success('Saved User, Company, Location and Website. Please now set up pricing tiers for this Associate.')->persistent();
        return redirect()->route('autoPartnerPricing', $partner);
    }

    /**
     * Update the partner's address to that which was provided
     *
     * @param  JamJar\User             $user
     * @param  Illuminate\Http\Request $request
     * 
     * @return void
     */
    protected function updatePartnerAddress(User $user, Request $request)
    {
        $user->profile()->update(
            [
            'address_line_one' => $request->address_line_one,
            'address_line_two' => $request->address_line_two,
            'town' => $request->town,
            'city' => $request->city,
            'county' => $request->county,
            'postcode' => $request->postcode,
            'auto_partner' => 1,
            'telephone_number' => $request->mobile_number
            ]
        );
    }

    /**
     * Register a Company based on the request input
     *
     * @param  JamJar\User             $user
     * @param  Illuminate\Http\Request $request
     * 
     * @return JamJar\Company
     */
    protected function registerCompany(User $user, Request $request)
    {
        $company = $user->company()->create(
            [
            'name' => $request->company_name,
            'telephone' => $request->telephone_number,
            'vat_number' => $request->vat_number,
            'address_line_one' => $request->address_line_one,
            'address_line_two' => $request->address_line_two,
            'town' => $request->town,
            'city' => $request->city,
            'county' => $request->county,
            'postcode' => $request->postcode,
            'valuation_validity' => $request->valuation_validity,
            'bank_transfer_fee' => $request->bank_transfer_fee,
            'admin_fee' => $request->admin_fee,
            ]
        );
        
        $company->position()->create(
            [
            'position' => null,
            'user_id' => $user->id
            ]
        );

        $user->profile()->update(['company_id' => $company->id]);

        return $company;
    }

    /**
     * Register a website for the company
     *
     * @param  JamJar\User             $user
     * @param  Illuminate\Http\Request $request
     * 
     * @return void
     */
    protected function registerWebsite(User $user, Request $request)
    {
        $user->website()->create(
            [
            'company_id'    => $user->company->id,
            'slug'          => str_slug($user->company->name)
            ]
        );
    }

    /**
     * Register a location for the company
     *
     * @param  JamJar\User             $user
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\Company          $company
     * 
     * @return void
     */
    protected function registerLocation(User $user, Request $request, Company $company)
    {
        PartnerLocation::create(
            [
            'user_id' => $user->id,
            'company_id' => $company->id,
            'name' => $request->company_name,
            'telephone' => $request->telephone_number,
            'address_line_one' => $request->address_line_one,
            'address_line_two' => $request->address_line_two,
            'town' => $request->town,
            'city' => $request->city,
            'county' => $request->county ?? $request->city,
            'postcode' => $request->postcode,
            'primary' => 1,
            'collection_fee' => $request->collection_fee ?? 0,
            'allow_collection' => (bool)$request->allow_collection,
            'dropoff_fee' => $request->dropoff_fee ?? 0,
            'allow_dropoff' => (bool)$request->allow_dropoff
            ]
        );
    }


    /**
     * Create the users funds
     * 
     * @param JamJar\User $user 
     * @return JamJar\User
     */
    protected function createUserFunds(User $user) 
    {
        $user->funds()->create([
            'funds' => 999999999
        ]);

        return $user;
    }

    /**
     * Delete an auto partner
     * 
     * @param JamJar\User $partner 
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function destroy(User $partner)
    {
        $partner->company->delete();
        $partner->company->location->each->delete();
        $partner->website->delete();
        $partner->profile->delete();
        AutoPartnerPricingTier::where('user_id', $partner->id)->delete();
        $partner->delete();
        alert()->success('Automatic Associate Deleted');
        return back();
    }
}
