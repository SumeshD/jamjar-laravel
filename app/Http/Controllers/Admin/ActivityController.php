<?php

namespace JamJar\Http\Controllers\Admin;

use Spatie\Activitylog\Models\Activity;

class ActivityController extends Controller
{
    /**
     * Display an index of Activity Items
     *  test charge
     * @return Illuminate\View\View
     */
    public function index()
    {
        $activities = Activity::orderBy('id', 'DESC')->paginate(20);

        $activityViews = [];

        foreach ($activities as $activity) {
            try {
                $type = $activity->description .'_'. strtolower(str_replace('JamJar\\', '', $activity->subject_type));
                $viewName = "admin.activity.types.{$type}";
                if (view()->exists($viewName)) {

                    $activityViews[] = view($viewName, ['event' => $activity])->render();
                }
            } catch (\Exception $e) {
                $activityViews[] = view('admin.activity.warning', ['viewName' => $viewName, 'activityId' => $activity->id])->render();
            }
        }
        
        return view('admin.activity.index', compact('activityViews', 'activities'));
    }
}
