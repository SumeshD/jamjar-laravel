<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\Product;

class ProductsController extends Controller
{
    /**
     * Show a listing of all products
     *
     * @param  JamJar\Product $product
     * 
     * @return Illuminate\View\View
     */
    public function index(Product $product)
    {
        $products = $product->orderBy('price', 'ASC')->paginate(15);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Create a product
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Edit a product
     *
     * @param  JamJar\Product $product
     * 
     * @return Illuminate\View\View
     */
    public function edit(Product $product)
    {
        return view('admin.products.edit', compact('product'));
    }

    /**
     * Store a product
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\Product          $product
     * 
     * @return Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        $product->create(
            [
            'name' => request()->get('name'),
            'sku' => request()->get('sku'),
            'description' => request()->get('description'),
            'price' => request()->get('price'),
            'active' => request()->get('active'),
            'credit_amount' => request()->get('credit_amount')
            ]
        );

        alert()->success('Product Saved');

        return redirect(route('adminProducts'));
    }

    /**
     * Update a product
     *
     * @param  Illuminate\Http\Request $request
     * @param  JamJar\Product          $product
     * 
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->update(
            [
            'name' => request()->get('name'),
            'sku' => request()->get('sku'),
            'description' => request()->get('description'),
            'price' => request()->get('price'),
            'active' => request()->get('active'),
            'credit_amount' => request()->get('credit_amount')
            ]
        );

        alert()->success('Product Updated');

        return redirect(route('adminProducts'));
    }

    /**
     * Delete a product from storage
     *
     * @param  JamJar\Product $product
     * 
     * @return Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        alert()->success('Product Deleted');
        return redirect(route('adminProducts'));
    }
}
