<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Setting;

class SettingsController extends Controller
{
    /**
     * Show an index of the settings stored in the database
     *
     * @param  \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (explode('@', auth()->user()->email)[1] != 'sellyourjamjar.co.uk' && explode('@', auth()->user()->email)[1] != 'jamjar.com') {
            alert()->error('You do not have permission to change settings');
            return back();
        }
        
        $settings = Setting::all();
        return view('admin.settings.index', compact('settings'));
    }

    /**
     * Update a record in the settings table
     *
     * @param  \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        foreach ($request->all() as $key => $value) {
            if (($key == '_token') || ($key == '_method')) {
                continue;
            }

            if ($value === null) {
                $value = '';
            }

            Setting::set($key, $value);
        }

//        die();

        alert()->success('Settings updated.');
        return back();
    }
}
