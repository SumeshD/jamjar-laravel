<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;

class BillingController extends Controller
{
    /**
     * Display a list of all billing records
     * 
     * @return Illuminate\View\View
     */
    public function index()
    {
        return view('admin.billing.index');
    }
}
