<?php

namespace JamJar\Http\Controllers\Admin;

use Illuminate\Http\Request;
use JamJar\Http\Controllers\Controller;
use JamJar\Model\SubscribersTagsChangelog;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SubscribersTagsChangelogController extends Controller
{
    public function index()
    {
        $changelogs = SubscribersTagsChangelog::orderBy('created_at', 'DESC')->paginate(20);

        return view('admin.subscribers-tags-changelog.index', ['changelogs' => $changelogs]);
    }

    public function showDetails(Request $request, int $changelogId)
    {
        $changelog = SubscribersTagsChangelog::where('id', '=', $changelogId)->first();

        if (!$changelog) {
            throw new NotFoundHttpException();
        }

        return view('admin.subscribers-tags-changelog.show-details', ['changelog' => $changelog]);
    }
}
