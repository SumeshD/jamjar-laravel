<?php

namespace JamJar\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @SWG\Info(title="Team Rocket Backent API", version="1.0")
     */
    protected function jsonResponse(array $data, int $status = 200)
    {
        if (substr((string) $status, 0, 1) == '2') {
            $data['ok'] = true;
        } else {
            $data['ok'] = false;
        }

        return new JsonResponse($data, $status);
    }
}
