<?php

namespace JamJar\Http\Controllers\Utilities;

use Illuminate\Http\Request;
use JamJar\Colour;
use JamJar\Defect;
use JamJar\Http\Controllers\Controller;
use JamJar\PlateYear;

class ToolsController extends Controller
{
    /**
     * Show a standalone Calculator
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\View\View
     */
    public function showCalculator(Request $request)
    {
        return view('tools.calculator');
    }

    public function vuePlayground()
    {
        $colors = Colour::all();
        $plateYears = PlateYear::all();
        $defects = Defect::all();

        return view('testing', compact(['colors', 'plateYears', 'defects']));
    }
}
