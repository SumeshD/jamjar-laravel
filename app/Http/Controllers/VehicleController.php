<?php
namespace JamJar\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use JamJar\Events\AdditionalVehicleInformationHasBeenProvidedByClient;
use JamJar\Events\VehicleAddedToMarketplace;
use JamJar\Exceptions\VehicleDoesNotExistException;
use JamJar\Exceptions\VehicleHasCompletedSalesException;
use JamJar\Http\Controllers\Matrix\LeadsController;
use JamJar\Http\Requests\EditVehicleRequest;
use JamJar\Http\Requests\StoreVehicleUserDetailsLogin;
use JamJar\Jobs\SendAutomaticBiddingRuleMatchedEmail;
use JamJar\Jobs\SendNotEnoughFundsEmail;
use JamJar\Model\ValuationDraft;
use JamJar\Model\VehicleImage;
use JamJar\PartnerPosition;
use JamJar\Rules\LoqateEmail;
use JamJar\Rules\MinimumAskingPrice;
use JamJar\Rules\MustContainSpace;
use JamJar\Rules\NonRunnerReasonsRequire;
use JamJar\Rules\RelatedFieldNotInRule;
use JamJar\Rules\UKPostcode;
use JamJar\Sale;
use JamJar\Services\APIValuationsService;
use JamJar\Services\DistanceCalculatorService;
use JamJar\Services\EloquentUploadedImageManager;
use JamJar\Services\ImagesManagement\ImagesManagerInterface;
use JamJar\Services\Valuations\OfferedValues\OfferedValueService;
use JamJar\Services\Valuations\VehicleValuationsCollector;
use JamJar\Services\Valuations\VehicleValuationsCreator;
use JamJar\Services\VehicleFilter\MarketplaceAlertsService;
use JamJar\Services\VehicleService;
use JamJar\Setting;
use JamJar\UserStatement;
use JamJar\Valuation;
use JamJar\ValueObjects\VehicleMetaEdit;
use JamJar\ValueObjects\VehicleOwnerEdit;
use JamJar\VehicleMeta;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Session;
use Postcode;
use Newsletter;
use JamJar\User;
use JamJar\Offer;
use Carbon\Carbon;
use JamJar\Colour;
use JamJar\Defect;
use JamJar\Company;
use JamJar\Profile;
use JamJar\Vehicle;
use JamJar\Api\M4YM;
use JamJar\Throttle;
use JamJar\Api\CapApi;
use JamJar\VehicleModel;
use JamJar\VehicleOwner;
use JamJar\Api\CarTakeBack;
use JamJar\Api\M4YMv2;
use JamJar\PartnerLocation;
use Illuminate\Http\Request;
use JamJar\Api\WeWantAnyCar;
use JamJar\VehicleDerivative;
use JamJar\Api\WeBuyCarsToday;
use JamJar\VehicleManufacturer;
use JamJar\Events\VehicleCreated;
use JamJar\AutoPartnerPricingTier;
use JamJar\Events\ValuationCreated;
use JamJar\Events\NewValuationOffered;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Traits\HandlesPartnerValuations;
use JamJar\Http\Requests\StoreVehicleUserDetails;

class VehicleController extends Controller
{
    use HandlesPartnerValuations;

    /** @var VehicleValuationsCollector */
    private $valuationsCollector;

    /** @var VehicleValuationsCreator */
    private $valuationsCreator;

    /** @var MarketplaceAlertsService */
    private $marketplaceAlertsService;

    /** @var EloquentUploadedImageManager */
    private $uploadedImageManager;

    /** @var ImagesManagerInterface */
    private $imagesManager;

    public function __construct(
        VehicleValuationsCollector $valuationsCollector,
        VehicleValuationsCreator $valuationsCreator,
        MarketplaceAlertsService $marketplaceAlertsService,
        EloquentUploadedImageManager $uploadedImageManager,
        ImagesManagerInterface $imagesManager
    ) {
        $this->valuationsCollector = $valuationsCollector;
        $this->valuationsCreator = $valuationsCreator;
        $this->marketplaceAlertsService = $marketplaceAlertsService;
        $this->uploadedImageManager = $uploadedImageManager;
        $this->imagesManager = $imagesManager;
    }

    /**
     * Display the index view
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(env('APP_ENV') == 'local' || env('APP_ENV') == 'staging') {
            return view('quote.index');
        } elseif(Session::has('sweet_alert.alert')){
            return view('quote.index');
        } else {
            return view('quote.index');
            //return redirect()->to('https://jamjar.com/');
        }
    }

    /**
     * Store a record in storage
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \JamJar\Vehicle          $vehicle
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeVehicleMeta(Request $request, $vrm)
    {
        $this->validate($request, array_merge($this->getVehicleDataValidation($request), ['postcode' => ['required', new UKPostcode()]]));

        $vehicle = $this->getVehicle($vrm);

        $vehicleMetaEditRequest = VehicleMetaEdit::createFromRequest($request);

        $vehicleService = new VehicleService();
        $vehicleService->editVehicleMeta($vehicle, $vehicleMetaEditRequest);

        $car = $vehicle->load('meta');

        cache()->forget('valuations-for-vrm-'.$car->numberplate.'-and-user-'.auth()->id());

        $capApi = new CapApi;
        $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();

        if ($capValuation) {
            $vehicle->setCapValues($capValuation);
            $vehicle->save();
        }

        if (auth()->user()) {
            $profile = auth()->user()->profile;
            $name = $this->formatName($profile->name);
            VehicleOwner::updateOrCreate(['vehicle_id' => $vehicle->id], [
                'firstname' => $name['firstname'],
                'lastname' => $name['lastname'],
                'telephone' => $profile->mobile_number,
                'postcode' => strtoupper($profile->postcode),
                'email' => auth()->user()->email,
                'user_id' => auth()->user()->id,
                'vehicle_id' => $vehicle->id
            ]);
        } else {
            VehicleOwner::updateOrCreate(['vehicle_id' => $vehicle->id], [
                'firstname' => '',
                'lastname' => '',
                'telephone' => '',
                'postcode' => strtoupper($request->postcode),
                'email' => '',
                'user_id' => null,
                'vehicle_id' => $vehicle->id
            ]);
        }

        if (count($vehicle->getDrafts()) > 0) {
            $this->valuationsCollector->removeAllDrafts($vehicle);
        }
        $request->session()->put('vehicle-details',$request->all());
        if (auth()->user()) {
            //$this->valuationsCollector->createValuationDrafts($vehicle);

            return redirect()->route('showValuationsAjax', $vehicle);
            //return redirect()->route('showValuations', $vehicle);
        } else {
            return redirect()->route('showValuationDraftsForClient', $car);
        }
    }

    public function removeFromMarketplace($vehicleId) {

        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        $vehicle->is_visible_on_marketplace = 0;
        $vehicle->save();

        alert()->success(
            'Your vehicle has been removed from Marketplace.',
            'Vehicle Removed.'
        )->persistent("Great!");

        return redirect(route('dashboard'));

    }

    public function showValuationDraftsForClient(Request $request, $numberplate)
    {
        if(auth()->user()) {
            return redirect()->route('dashboard');
        }
        $vehicle = $this->getVehicle($numberplate);

        $offeredValuesService = new OfferedValueService();
        $offeredValues = $offeredValuesService->getOfferedValuesByVehicle($vehicle);

        foreach($offeredValues as $key=>$offeredValue) {

            if(!$offeredValue->company){
                unset($offeredValues[$key]);
                continue;
            }

            if ($offeredValue->company->name == 'CarTakeBack' && $offeredValue->isCollectionAvailable() && $offeredValue->isDropOffAvailable()) {


                libxml_use_internal_errors(true);
                $body = simplexml_load_string((string)$offeredValue->external_partner_response);

                if(!$body){
                    continue;
                }

                $results = (array)$body->valuations;

                $valuations = collect($results)->map(
                    function ($value, $key) use ($body) {
                        $hasCollectionValuation = isset($value[0]);
                        $hasDropoffValuation = isset($value[1]);
                        return [
                            'collection' => $hasCollectionValuation == true ? (int) $value[0]->amount : null,
                            'dropoff'   => $hasDropoffValuation == true ? (int) $value[1]->amount : null,
                        ];
                    }
                );


                $offeredValue->collection_part = true;
                $offeredValue->dropoff_part = false;
                $offeredValue->scrap = true;
                $offeredValue->collection_new_price = $valuations['valuation']['collection'];

                $oldOfferedValue = clone $offeredValue;
                $oldOfferedValue->scrap = true;
                $oldOfferedValue->dropoff_part = true;
                $oldOfferedValue->collection_part = false;
                $oldOfferedValue->dropoff_new_price = $valuations['valuation']['dropoff'];

                $offeredValues[] = $oldOfferedValue;

            } else {

                $offeredValue->scrap = false;
            }


        }

        usort($offeredValues, function($valueA, $valueB) {

            $value1 = $valueA->getPriceInPence()/100;

            //Reorder for CarTakeBack
            if($valueA->dropoff_part) {
                $value1 = $valueA->dropoff_new_price;
            }

            if($valueA->collection_part) {
                $value1 = $valueA->collection_new_price;
            }


            $value2 = $valueB->getPriceInPence()/100;

            //Reorder for CarTakeBack
            if($valueB->dropoff_part) {
                $value2 = $valueB->dropoff_new_price;
            }

            if($valueB->collection_part) {
                $value2 = $valueB->collection_new_price;
            }

            if($value1 > $value2) {

                return -1;
            }

            if($value1 == $value2) {

                return 0;
            }

            if($value1 < $value2) {

                return 1;
            }


        });

        return view('quote.show-valuation-drafts', ['vehicle' => $vehicle, 'offeredValues' => $offeredValues]);
    }



    /**
     * Store the Vehicle Owner Details
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \JamJar\Vehicle          $vehicle
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeVehicleUserDetails(Request $request, $vrm, LoqateEmail $loqateEmail)
    {
        if(auth()->user()) {
            return redirect()->route('dashboard');
        }
        $validator = new StoreVehicleUserDetails();
        $rules = $validator->rules($loqateEmail);
        $messages = $validator->messages();
        $this->validate($request, $rules, $messages);

        $vehicle = $this->getVehicle($vrm);

        if ($request->has('log-in-form') && auth()->guest()) {
            alert()->error('Email or password does not match, please check your credentials again.');
            return back();
        }

        if (auth()->guest()) {
            // Register a User Account for the Vehicle Owner
            $user = User::where('email', $request->email)->first();
            if ($user === null) {
                $user = new User;
                $user = $user->registerUser($request->fullname, $request->email, $request->password);
                $user->profile->update([
                    'postcode' => $request->postcode,
                    'mobile_number' => $request->telephone_number,
                    'email' => $request->email,
                    'address_line_one' => $request->address_line_one,
                    'address_line_two' => $request->address_line_two,
                    'city' => $request->city,
                    'town' => $request->city,
                ]);
                auth()->loginUsingId($user->id);
                $this->handleMarketingPreferences($request->all());
            }
        } else {
            $user = auth()->user();
            // Update the currently authenticated user's preferences.
            $this->handleMarketingPreferences($request->all());
            // Log the user in.
            $this->updateAuthenticatedUser($request->all());
        }

        $name = $this->formatName($request->fullname);

        $previouslyAddedVehicle = Vehicle::getFirstVehicleOfUserByNumberplate($user, $vrm);

        if ($previouslyAddedVehicle) {
            $this->showVehicleExistsAlert($previouslyAddedVehicle);

            return redirect()->route('dashboard');
        }

        // Create the new Vehicle Owner so we can trace Vehicles to Accounts
        $owner = VehicleOwner::updateOrCreate(['vehicle_id' => $vehicle->id], [
            'firstname' => $name['firstname'],
            'lastname' => $name['lastname'],
            'telephone' => $request->telephone_number,
            'postcode' => $request->postcode,
            'email' => $request->email,
            'user_id' => auth()->check() ? auth()->user()->id : null,
            'vehicle_id' => $vehicle->id
        ]);

        if($request->showcase) {
            $this->guardAgainstVehicleExistence($vehicle);
            $this->guardAgainstUserOwnership($vehicle, auth()->user());

            $vehicle->setKeysCount((int) $request->get('nrKeys'));
            $vehicle->setServiceHistoryInformation($request->get('serviceInfo'));
            $vehicle->setAdditionalSpecificationInformation($request->get('specsInfo'));
            $vehicle->save();
            if(empty($request->image) && empty($request->get('serviceInfo')) && empty($request->get('specsInfo'))) {
                Session::flash('no-details', 'true');
                return redirect()->route('showValuations', [$vehicle, $request->valuation]);
            }
            Session::flash('showcase', 'true');
            if(empty($request->image)){
                return redirect()->route('showValuations', [$vehicle, $request->valuation]);
            }
            foreach ($request->image as $image) {
                /** @var  \Illuminate\Http\UploadedFile $image */
                $fileContent = file_get_contents($image->getRealPath());

                $data = [
                    'original_name' => $image->getClientOriginalName(),
                    'image' => $fileContent,
                ];

                $validation = [
                    'original_name' => 'required|min:5|max:1024',
                    'image' => 'required',
                ];

                $validator = Validator::make($data, $validation);

                if ($validator->fails()) {
                    return $this->jsonResponse(['errors' => $validator->messages()->messages()], 400);
                }

                $uploadedImage = $this->uploadedImageManager->create(
                    $data['original_name'],
                    'active'
                );

                $this->imagesManager->uploadImage($data['image'], $uploadedImage);
                $vehicleImage = new VehicleImage();
                $vehicleImage->uploaded_image_id = $uploadedImage->id;
                $vehicleImage->vehicle_id = $vehicle->id;
                $vehicleImage->order_number = 1;
                $vehicleImage->save();
            }
        }

        return redirect()->route('showValuations', [$vehicle, $request->valuation]);
    }

    /**
     * Store the Vehicle Owner Details
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \JamJar\Vehicle          $vehicle
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeVehicleUserDetailsByLogin(StoreVehicleUserDetailsLogin $request, $vrm)
    {
        $vehicle = $this->getVehicle($vrm);

        if (auth()->guest()) {
            return back()
                ->withErrors(['Email or password does not match, please check your credentials again'])
                ->withInput(Input::all());
        }

        /** @var User $user */
        $user = auth()->user();
        $previouslyAddedVehicle = Vehicle::getFirstVehicleOfUserByNumberplate($user, $vrm);

        if ($previouslyAddedVehicle) {
            $this->showVehicleExistsAlert($previouslyAddedVehicle);

            return redirect()->route('dashboard');
        }

        // Create the new Vehicle Owner so we can trace Vehicles to Accounts
        VehicleOwner::updateOrCreate(['vehicle_id' => $vehicle->id], [
            'firstname' => $user->getFirstNameAttribute(),
            'lastname' => $user->getLastNameAttribute(),
            'telephone' => $user->profile->mobile_number,
            'postcode' => $user->profile->postcode,
            'email' => $user->email,
            'user_id' => $user->id,
            'vehicle_id' => $vehicle->id
        ]);

        return redirect()->route('showValuations', $vehicle);
    }

    /**
     * Retrieve the specified resource from storage.
     *
     * @param  \JamJar\Vehicle $vehicle
     * @return \Illuminate\Http\Response
     */
    public function showVehicleInformation(Request $request,$vrm)
    {
        //print_r($request->session()->get('vehicle-details')); die;
        //$request->session()->forget('vehicle-details');
//        if($request->session()->get('vehicle-details')){
//            die('ok');
//        }
        $vehicle = $this->getVehicle($vrm);
        if (request()->has('edit')) {
            $vehicle = Vehicle::cloneVehicle($vehicle);
        }

        if ($vehicle->exists()) {
            if ($this->isSessionAuthorized($vehicle->numberplate) || request()->has('edit')) {
                $defects = Defect::all();
                $colors = Colour::all();
                $postcode = '';
                if ($user = auth()->user()) {
                    $postcode = auth()->user()->profile->postcode ? auth()->user()->profile->postcode: '';
                    $previouslyAddedVehicle = Vehicle::getFirstVehicleOfUserByNumberplate($user, $vrm);

                    if ($previouslyAddedVehicle) {
                        $this->showVehicleExistsAlert($previouslyAddedVehicle);

                        return redirect()->route('dashboard');
                    }
                }

                return view('quote.your-car', compact('vehicle', 'defects', 'colors', 'postcode'));
            } else {
                alert()->error('Sorry, please try entering your numberplate again.');
                return redirect()->route('/');
            }
        }
    }

    /**
     * Retrieve the specified resource from storage.
     *
     * @param  Vehicle $vehicle
     * @return \Illuminate\Http\Response
     */
    public function showVehicleDetails( $vrm, $valuationDraftId = 0, $showcase = '' )
    {
        $vehicle = $this->getVehicle($vrm);

        if ($vehicle->exists()) {
            if ($this->isSessionAuthorized($vehicle->numberplate)) {
                $owner = VehicleOwner::find(request()->owner);
                if (auth()->check()) {
                    $owner = auth()->user();
                }
                $postCode = '';
                $addressLabel = 'Postcode';
                if($vehicle->owner()->first()){
                    $postCode = $vehicle->owner()->first()->postcode;
                }
                $valuationDraft = false;
                if($valuationDraftId) {
                    $valuationDraft = ValuationDraft::find($valuationDraftId);
                }
                return view('quote.your-details', compact('vehicle', 'owner', 'postCode', 'addressLabel', 'valuationDraft', 'showcase', 'valuationDraft'));
            } else {
                alert()->error('Sorry, please try entering your numberplate again.');
                return redirect()->route('/');
            }
        }
    }

    public function vehicleEditForm(int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        $this->guardAgainstVehicleExistence($vehicle);
        $this->guardAgainstUserOwnership($vehicle, auth()->user());
        $this->guardAgainstCompletedSales($vehicle);

        $owner = VehicleOwner::where('vehicle_id', '=', $vehicle->id)->first();

        return view('vehicle.edit', [
            'vehicle' => $vehicle,
            'defects' => Defect::all(),
            'colors' => Colour::all(),
            'owner' => $owner,
            'serviceHistories' => VehicleMeta::getAvailableServicesHistories(),
            'mots' => VehicleMeta::getAvailableMots(),
            'writeOffCategories' => VehicleMeta::getAvailableWriteOffCategories(),
            'postcode' => auth()->user()->profile->postcode
        ]);
    }

    public function vehicleEdit(Request $request, int $vehicleId)
    {
        /** @var Vehicle $vehicle */
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();

        $oldUuid = (count($vehicle->valuations) > 0 && isset($vehicle->valuations[0])) ? Uuid::fromString($vehicle->valuations[0]->uuid) : Uuid::uuid4();

        $this->validate($request, $this->getVehicleDataValidation($request));

        $this->guardAgainstVehicleExistence($vehicle);
        $this->guardAgainstUserOwnership($vehicle, auth()->user());
        $this->guardAgainstCompletedSales($vehicle);

        $vehicleMetaEditRequest = VehicleMetaEdit::createFromRequest($request);

        $oldVehicle = clone $vehicle;
        $oldVehicle->meta = clone $vehicle->meta;

        $vehicleService = new VehicleService();
        $vehicleService->editVehicleMeta($vehicle, $vehicleMetaEditRequest);
        $vehicleService->removeOldValuations($vehicle, $oldVehicle);

        $this->valuationsCollector->removeAllDrafts($vehicle);
        $this->valuationsCollector->createValuationDrafts($vehicle);

        $valuations = $this->createValuationsForVehicle($vehicle, $oldUuid);

        $vehicle->start_sending_reminders_at = Carbon::now();
        $vehicle->is_visible_on_marketplace = true;
        $vehicle->added_to_marketplace_at = Carbon::now();
        $vehicle->save();

        event(new VehicleAddedToMarketplace($vehicle));

        alert()->info('Your vehicle has been updated');
        if (count($valuations) == 0) {
            if (empty((array) $valuations)) {
                alert()->warning('Your vehicle is now visible to the UK\'s best online car buying companies, we will notify you when you receive a great offer!');

                return response()->redirectTo(route('dashboard'));
            }
        }

        $uuid = $valuations[0]->uuid;

        return redirect()->route('showSavedValuation', $uuid);
    }

    public function showValuationsAjax($vrm)
    {
        $vehicle = $this->getVehicle($vrm);

        if (!$vehicle) {
            alert()->warning('Vehicle not found. Please try again.');

            return response()->redirectTo(route('dashboard'));
        }
        $this->guardAgainstUserOwnership($vehicle, auth()->user());
        $valuations = [];
        $valuationGroup = Uuid::uuid4()->toString();
        return view('valuations.showAjax', compact('vehicle', 'valuations', 'valuationGroup'));
    }

    public function showValuations($vrm, $valuationDraftId = '')
    {
        $vehicle = $this->getVehicle($vrm);

        if (!$vehicle) {
            alert()->warning('Vehicle not found. Please try again.');

            return response()->redirectTo(route('dashboard'));
        }

        $valuations = $this->createValuationsForVehicle($vehicle, Uuid::uuid4());
        $vehicle->is_visible_on_marketplace = true;
        $vehicle->added_to_marketplace_at = Carbon::now();
        $vehicle->save();

        event(new VehicleAddedToMarketplace($vehicle));

        if (count($valuations) == 0) {
            alert()->warning('Your vehicle is now visible to the UK\'s best online car buying companies, we will notify you when you receive a great offer!');

            return response()->redirectTo(route('dashboard'));
        }
        if(Session::has('showcase')){
            Session::flash('showcase', 'true');
            //return response()->redirectTo(route('dashboard'));
            return response()->redirectTo(route('showSavedValuation', $valuations[0]->uuid));
        }

        if(Session::has('no-details')) {
            return response()->redirectTo(route('showShowcaseForm', [$vehicle->id]));
        }

        if($valuationDraftId){
            $valuationDraft = ValuationDraft::find($valuationDraftId);
            $valuation = $valuationDraft->getValuation();
            return response()->redirectTo(route('showSavedValuation', [$valuations[0]->uuid, $valuation->getId()]));
        }

        return response()->redirectTo(route('showSavedValuation', $valuations[0]->uuid));
    }

    /**
     * If there's no vehicle found, the user must enter it manually
     *
     * @param  string $vrm
     * @return \Illuminate\View\View
     */
    public function noVehicleFound($vrm)
    {
        return view('quote.enter-car', compact('vrm'));
    }

    public function showAdditionalInformationForm(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();
        $this->guardAgainstVehicleExistence($vehicle);
        $this->guardAgainstUserOwnership($vehicle, auth()->user());

        return view('vehicle.show-additional-information-form', ['vehicle' => $vehicle]);
    }

    public function showShowcaseForm(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();
        $this->guardAgainstVehicleExistence($vehicle);
        $this->guardAgainstUserOwnership($vehicle, auth()->user());
        $images = $vehicle->getImages();

        return view('vehicle.show-showcase-form', compact('vehicle', 'images'));
    }

    public function processAdditionalInformationForm(Request $request, int $vehicleId)
    {
        //print_r($request->all()); die;
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();
        $this->guardAgainstVehicleExistence($vehicle);
        $this->guardAgainstUserOwnership($vehicle, auth()->user());

        $this->validate($request, array_merge(
            [
//                'asking-price-in-pounds' => ['required', 'numeric' , new MinimumAskingPrice($vehicleId), 'max:' . LeadsController::MAX_VALUATION_VALUE],
                'keys-count' => 'required|numeric|min:1|max:3',
                'service-history-information' => 'string|max:255|nullable',
                'additional-specification-information' => 'string|max:255|nullable',
            ]
        ));

        $askingPriceInPence = (int) ((int) $request->get('asking-price-in-pounds') * 100);

//        $vehicle->setAskingPriceInPence($askingPriceInPence);
        $vehicle->setKeysCount((int) $request->get('keys-count'));
        $vehicle->setServiceHistoryInformation($request->get('service-history-information'));
        $vehicle->setAdditionalSpecificationInformation($request->get('additional-specification-information'));
        $vehicle->save();

        alert()->success(
            'We\'ll let you know via text message and email as soon as one of our marketplace partners makes an improved offer for your vehicle ',
            'Your vehicle information has been updated.'
        )->persistent("Great!");

        event(new AdditionalVehicleInformationHasBeenProvidedByClient($vehicle));

        $uuid = $vehicle->valuations[0]->uuid;

        if ($uuid) {
            return redirect(route('showSavedValuation', [$uuid]));
        }

        return redirect(route('showAdditionalInformationForm', [$vehicle->id]));
    }

    public function processShowcaseForm(Request $request, int $vehicleId)
    {
        $vehicle = Vehicle::where('id', '=', $vehicleId)->first();
        $this->guardAgainstVehicleExistence($vehicle);
        $this->guardAgainstUserOwnership($vehicle, auth()->user());

        $this->validate($request, array_merge(
            [
                'nrKeys' => 'required|numeric|min:1|max:3',
                'serviceInfo' => 'string|max:255|nullable',
                'specsInfo' => 'string|max:255|nullable',
            ]
        ));
        $vehicle->setKeysCount((int) $request->get('nrKeys'));
        $vehicle->setServiceHistoryInformation($request->get('serviceInfo'));
        $vehicle->setAdditionalSpecificationInformation($request->get('specsInfo'));
        $vehicle->save();

        if(empty($request->get('serviceInfo')) && empty($request->get('specsInfo'))) {
            $vehicleImage = VehicleImage::where('vehicle_id', '=', $vehicleId)->first();
            if(!$vehicleImage){
                Session::flash('no-details-showcase');
            } else {
                Session::flash('showcase', 'true');
            }
        } else {
            Session::flash('showcase', 'true');
        }
        event(new AdditionalVehicleInformationHasBeenProvidedByClient($vehicle));

        $uuid = $vehicle->valuations[0]->uuid;

        return redirect(route('showSavedValuation', [$uuid]));
    }

    /**
     * @param Vehicle $vehicle
     * @param Uuid $uuid
     * @return array|Valuation[]
     */
    private function createValuationsForVehicle(Vehicle $vehicle, UuidInterface $uuid): array
    {
        /** @var Collection|ValuationDraft[] $valuationDrafts */
        $valuationDrafts = $vehicle->getDrafts()->sortByDesc(function(ValuationDraft $draft) {
            return $draft->getPriceInPence();
        });

        $valuations = [];
        $position = 1;

        $capApi = new CapApi;
        $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();

        foreach ($valuationDrafts as $valuationDraft) {
            $valuation = $this->valuationsCreator->createValuationFromDraft($valuationDraft, $uuid, $position, $capValuation);

            $valuations[] = $valuation;

            if ($valuationDraft->getCompany()->company_type == 'api') {
                $position++;
                continue;
            }

            $valuationCompany = $valuationDraft->company;
            $usesFlatFee = false;
            $flatFee = 0;

            if( $valuationCompany ) {
                if( $valuationCompany->user ) {
                    if( $valuationCompany->user->profile ) {

                        if($valuationCompany->user->profile->use_flat_fee == 1 && $valuationCompany->user->profile->flat_fee != null) {

                            $usesFlatFee = true;
                            $flatFee = $valuationCompany->user->profile->flat_fee;

                        }

                    }

                }

            }

            if($usesFlatFee) {

                $feeInPence = ($flatFee * 100)+(($flatFee*100)*env('VAT_FEE_IN_PERCENT')/100);

            } else {

                $calculator = new PartnerFeeCalculator($position, $valuationDraft->getPriceInPence() / 100);
                $feeInPence = $calculator->calculate(true, $valuationCompany);

            }

            $companyPosition = PartnerPosition::where('company_id','=',$valuationCompany->id)->where('user_id','=',$valuationCompany->user_id)->first();

            if($companyPosition && $companyPosition->position) {

                if($position<=$companyPosition->position || $companyPosition->position==6) {

                    if($feeInPence<=$valuationCompany->user->funds->getOriginal('funds')) {

                        if($valuationCompany) {

                            PartnerFeeCalculator::createStatementEntry($valuation, $valuationCompany, $valuation->getVehicle(), $feeInPence);
                            $valuationCompany->user->funds->spendFunds($feeInPence);
                            if((bool) Setting::get('sendAutomaticBiddingRuleMatchEmail') && $valuationCompany->user->isMatrixPartner()){
                                SendAutomaticBiddingRuleMatchedEmail::dispatch($valuationCompany->user, $valuation, $valuation->getVehicle(), $position);
                            }
                        }

                        $valuation->fee = $feeInPence;
                        $valuation->is_created_by_automated_bids = true;
                        $valuation->save();

                    }else {
                        SendNotEnoughFundsEmail::dispatch($valuationCompany->user, $vehicle, $feeInPence, $valuationCompany->user->funds->getOriginal('funds'));
                    }

                } else {

                    $position++;
                    continue;
                }

            } else {

                if($feeInPence<=$valuationCompany->user->funds->getOriginal('funds')) {

                    if($valuationCompany) {

                        PartnerFeeCalculator::createStatementEntry($valuation, $valuationCompany, $valuation->getVehicle(), $feeInPence);
                        $valuationCompany->user->funds->spendFunds($feeInPence);
                        if((bool) Setting::get('sendAutomaticBiddingRuleMatchEmail') && $valuationCompany->user->isMatrixPartner()){
                            SendAutomaticBiddingRuleMatchedEmail::dispatch($valuationCompany->user, $valuation, $valuation->getVehicle(), $position);
                        }
                    }

                    $valuation->fee = $feeInPence;
                    $valuation->is_created_by_automated_bids = true;
                    $valuation->save();

                }else {
                    SendNotEnoughFundsEmail::dispatch($valuationCompany->user, $vehicle, $feeInPence, $valuationCompany->user->funds->getOriginal('funds'));
                }
            }

            $position++;


        }

        return $valuations;
    }

    /**
     * Check if this session is authorized
     *
     * @param  string $vrm
     * @return bool
     */
    private function isSessionAuthorized($vrm)
    {
        $vrmCheck = strtoupper(request()->session()->get('vrm'));

        if ($vrmCheck == $vrm) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Format the given name.
     *
     * @param  string $name
     * @return array
     */
    private function formatName($name)
    {
        $fullname = explode(' ', $name);

        if (array_key_exists(1, $fullname)) {
            $firstname = $fullname[0];
            $lastname = $fullname[1];
        } else {
            $firstname = $name;
            $lastname = ' ';
        }

        return compact('firstname', 'lastname');
    }

    /**
     * Handle the given marketing preferences (or lack thereof)
     *
     * @param  array $request
     * @return void
     */
    protected function handleMarketingPreferences($request)
    {
        if (!isset($request['marketing']) || $request['marketing'] == false) {
            return false;
        }

        // These are our marketing options
        $marketingOptions = [
            'email' => true,
            'telephone' => true,
            'sms' => true
        ];

        // Loop through again to update our user
        foreach ($marketingOptions as $key => $value) {
            // because we default to a falsy value, any checkboxes
            // not checked will convert to a 0, thus disabled.
            auth()->user()->update(
                [
                "marketing_{$key}" => (int)$value
                ]
            );
            // dump("marketing_{$key} set to ".(int)$value);
        }

        $name = $this->formatName($request['fullname']);

        $mergeFields = [
            'FNAME' => $name['firstname'],
            'LNAME' => $name['lastname'],
            'PHONE' => $request['telephone_number'],
            'MMERGE4' => $request['email'],
            'MMERGE9' => $request['postcode'],
        ];

        Newsletter::subscribe($request['email'], $mergeFields, 'customer_subscribers');
    }

    /**
     * Update the authenticated users information with what they have just submitted through the valuation process
     *
     * @param  array $request
     * @return void
     */
    protected function updateAuthenticatedUser($request)
    {
        $email = $request['email'];
        $telephone = $request['telephone_number'];
        $postcode = $request['postcode'];

        if ($email != auth()->user()->email) {
            auth()->user()->update(
                [
                'email' => $email
                ]
            );
            // trigger notice that email was changed
        }

        if ($telephone != auth()->user()->profile->mobile_number) {
            auth()->user()->profile()->update(
                [
                'mobile_number' => $telephone
                ]
            );
        }

        if ($postcode != auth()->user()->profile->postcode) {
            auth()->user()->profile()->update(
                [
                'postcode' => $postcode
                ]
            );
        }
    }

    /**
     * Get the API Fee
     *
     * @param  array $valuation
     * @return float
     */
    public function getApiFee($valuation)
    {
        $calculator = new PartnerFeeCalculator($valuation['position'], $valuation['valuation']['dropoff']);
        $fee = $calculator->calculate(true);

        return $fee;
    }

    protected function getVehicle(string $vrm): Vehicle
    {
        return Vehicle::where('numberplate', $vrm)->latest()->first();
    }

    /**
     * @param Vehicle|null $vehicle
     * @throws VehicleDoesNotExistException
     */
    private function guardAgainstVehicleExistence(Vehicle $vehicle = null)
    {
        if (!$vehicle) {
            throw new VehicleDoesNotExistException();
        }
    }

    /**
     * @param Vehicle $vehicle
     * @param User $user
     * @throws VehicleDoesNotExistException
     */
    private function guardAgainstUserOwnership(Vehicle $vehicle, User $user)
    {
        $vehicleOwner = VehicleOwner::where('vehicle_id', '=', $vehicle->id)->first();
        if ($vehicleOwner->user_id != $user->id) {
            throw new VehicleDoesNotExistException();
        }
    }

    /**
     * @param Vehicle $vehicle
     * @throws VehicleHasCompletedSalesException
     */
    private function guardAgainstCompletedSales(Vehicle $vehicle)
    {
        if ($vehicle->hasCompleteSale()) {
            throw new VehicleHasCompletedSalesException();
        }
    }

    private function showVehicleExistsAlert(Vehicle $previouslyAddedVehicle)
    {
        $buttons = [];
        if (count($previouslyAddedVehicle->valuations) > 0) {
            $buttons['view'] = ['text' => "View", 'className' => 'redirect:' . route('showSavedValuation', [$previouslyAddedVehicle->valuations->first()->uuid], false)];
        }
        $buttons['update'] = ['text' => "Update", 'className' => 'redirect:' . route('vehicleEditForm', [$previouslyAddedVehicle->id], false)];

        \Session::flash('sweet_alert.alert', json_encode([
            'title' => "Vehicle already exists",
            'text' => 'It looks like you already have a valuation for this vehicle. Would you like view your existing valuation or edit your vehicle details for a new valuation?',
            'icon' => "warning",
            'buttons' => $buttons,
        ]));
    }

    private function getVehicleDataValidation(Request $request): array
    {
        return [
            'service-history' => 'required',
            'mot' => 'required',
            'previous-owners' => 'required',
            'mileage' => 'required|numeric|min:1000',
            'car-colour' => 'required',
            'write-off' => [
                'required',
                new RelatedFieldNotInRule($request->input('write-off-category'), ['none'])
            ],
            'non-runner' => [
                'required',
                new NonRunnerReasonsRequire($request->input('non-runner-reason')),
            ],
        ];
    }

    private function getVehicleOwnerValidation(Request $request): array
    {
        return [
            'name' => ['required', new MustContainSpace()],
            'postcode' => ['required', new UKPostcode()],
            'telephone' => 'required|max:20|phone:GB,mobile,fixed_line',
        ];
    }
}
