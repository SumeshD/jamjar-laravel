<?php

namespace JamJar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JamJar\Rules\MustContainSpace;
use JamJar\Rules\UKPostcode;

class EditVehicleRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'name' => ['required', new MustContainSpace],
            'postcode' => ['required', new UKPostcode],
            'telephone' => 'required|max:20|phone:GB,mobile,fixed_line',

            'service-history' => 'required',
            'mot' => 'required',
            'previous-owners' => 'required',
            'mileage' => 'required|numeric|min:0',
            'car-colour' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Please enter your full name.',
            'email.required' => 'Please enter your email.',
            'email.unique' => 'Email already in-use, do you need to <a href="'.route('password.request').'">reset your password?</a>',
        ];
    }
}
