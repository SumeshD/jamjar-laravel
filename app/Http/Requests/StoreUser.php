<?php

namespace JamJar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JamJar\Rules\MustContainSpace;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', new MustContainSpace],
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'marketing' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'marketing.required' => 'Please tick the box to confirm your marketing preferences',
            'name.required' => 'Please enter your full name.',
            'email.required' => 'Please enter your email.',
            'email.unique' => 'Email already in-use, do you need to <a href="'.route('password.request').'">reset your password?</a>',
        ];
    }
}
