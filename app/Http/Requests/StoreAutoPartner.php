<?php

namespace JamJar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JamJar\Rules\MustContainSpace;

class StoreAutoPartner extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'telephone_number' => 'required|max:20|phone:GB',
            'mobile_number' => 'required|max:20|phone:GB,mobile',
            'address_line_one' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'company_name' => 'required',
            'vat_number' => 'required',
            'valuation_validity' => 'required',
            'bank_transfer_fee' => 'required|numeric',
            'admin_fee' => 'required|numeric',
            'marketing' => 'required',
        ];

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'marketing.required' => 'Please tick the box to confirm your marketing preferences',
            'email.required' => 'Please enter your email.',
            'email.unique' => 'Email already in-use, do you need to <a href="'.route('password.request').'">reset your password?</a>',
        ];
    }
}
