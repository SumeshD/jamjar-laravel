<?php

namespace JamJar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JamJar\Rules\MustContainSpace;
use JamJar\Rules\UKPostcode;

class StorePartner extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'company_name' => 'required',
            'name' => ['required', new MustContainSpace],
            'telephone_number' => 'required',
            'address_line_one' => 'required',
            'city' => 'required',
            'postcode' => ['required', new UKPostcode],
            'vat_number' => 'required',
            'email' => 'required|email|unique:users,email',
            'marketing' => 'required',
            'buy_vehicles_from_other_partners' => 'nullable',
            'is_scrap_dealer' => 'nullable',
        ];

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'marketing.required' => 'Please tick the box to confirm your marketing preferences',
            'fullname.required' => 'Please enter your full name.',
            'email.required' => 'Please enter your email.',
            'email.unique' => 'Email already in-use, do you need to <a href="'.route('password.request').'">reset your password?</a>',
        ];
    }
}
