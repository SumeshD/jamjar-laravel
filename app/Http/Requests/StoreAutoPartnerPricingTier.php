<?php

namespace JamJar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAutoPartnerPricingTier extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'valuation_to' => 'required',
            'percentage' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'valuation_to.required' => 'A "To" valuation is required',
            'valuation_to.unique' => 'You already have a valuation which covers this amount.',
            'percentage.required'  => 'A percentage is required',
        ];
    }
}
