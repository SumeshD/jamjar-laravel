<?php

namespace JamJar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JamJar\Rules\LoqateEmail;
use JamJar\Rules\MustContainSpace;
use JamJar\Rules\UKPostcode;

class StoreVehicleUserDetails extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules( LoqateEmail $loqateEmail)
    {
        // log the user in if the email and password match.
        auth()->attempt(['email' => request()->email, 'password' => request()->password]);

        $rules = [
            'marketing' => 'required',
            'fullname' => ['required', new MustContainSpace],
            'postcode' => ['required', new UKPostcode],
            'telephone_number' => 'required|max:20|phone:GB,mobile',
            'address_line_one' => 'required',
            'g-recaptcha-response' => 'required|captcha',
            'service-history-information' => 'string|max:255|nullable',
            'additional-specification-information' => 'string|max:255|nullable'
        ];

        if (auth()->check()) {
            $rules['email'] = 'required|email|unique:users,email,'.auth()->id();
        } else {
            $rules['email'] = ['required', 'email', 'unique:users', $loqateEmail];
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'marketing.required' => 'Please tick the box to confirm your marketing preferences',
            'fullname.required' => 'Please enter your full name.',
            'email.required' => 'Please enter your email.',
            'email.unique' => 'Email already in-use, do you need to <a href="'.route('password.request').'">reset your password?</a>',
            'address_line_one.required' => 'Please enter collection address.',
            'postcode.required' => 'Please enter collection address.',
            'city.required' => 'Please enter collection address.'
        ];
    }
}
