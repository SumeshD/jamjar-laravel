<?php

namespace JamJar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVehicleUserDetailsLogin extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        // log the user in if the email and password match.
        auth()->attempt(['email' => request()->get('login-email'), 'password' => request()->password]);

        $rules['login-email'] = 'required|email';

        return $rules;
    }

    public function messages()
    {
        return [
            'login-email.required' => 'Please enter your email.',
            'login-email.unique' => 'Email or password does not match, please check your credentials again',
        ];
    }
}
