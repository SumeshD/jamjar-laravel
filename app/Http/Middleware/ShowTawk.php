<?php

namespace JamJar\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ShowTawk
{
    protected $pagesWithTawk = [
        'associates*',
        'valuations*',
        'account*',
        'vehicle*',
    ];

    public function handle(Request $request, Closure $next)
    {
        $showTawk = $this->isInPagesWithTawkArray($request) ? true : false;
        $request->merge(['showTawk' => $showTawk]);

        return $next($request);
    }

    protected function isInPagesWithTawkArray(Request $request): bool
    {
        foreach ($this->pagesWithTawk as $page) {
            if ($page !== '/') {
                $page = trim($page, '/');
            }

            if ($request->fullUrlIs($page) || $request->is($page)) {
                return true;
            }
        }

        return false;
    }
}
