<?php

namespace JamJar\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = \JamJar\Role::where('name', 'Administrator')->first();
        
        if ($admin) {
            if (auth()->user() && auth()->user()->role->id == $admin->id) {
                return $next($request);
            }
        }

        alert()
            ->error('Sorry! You don\'t seem to have permission to do that.')
            ->autoclose(5000);

        return redirect(route('/'));
    }
}
