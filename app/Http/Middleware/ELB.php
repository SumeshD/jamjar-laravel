<?php 

namespace JamJar\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Request;


/**
 * Put this file in app/Http/Middleware
 * Read more here https://github.com/peppeocchi/laravel-elb-middleware
 * 
 */
class ELB
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // This contains the possible ips for our load balancer in AWS.
        $request->setTrustedProxies(['127.0.0.1', '10.0.10.0/24', '10.0.15.0/24']);

        $request->setTrustedHeaderName(Request::HEADER_FORWARDED, null);
        $request->setTrustedHeaderName(Request::HEADER_CLIENT_IP, 'HTTP_X_FORWARDED_FOR');
        $request->setTrustedHeaderName(Request::HEADER_CLIENT_HOST, null);
        $request->setTrustedHeaderName(Request::HEADER_CLIENT_PROTO, 'HTTP_X_FORWARDED_PROTO');
        $request->setTrustedHeaderName(Request::HEADER_CLIENT_PORT, 'HTTP_X_FORWARDED_PORT');

        return $next($request);
    }
}