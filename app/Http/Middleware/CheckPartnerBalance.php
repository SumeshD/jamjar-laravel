<?php

namespace JamJar\Http\Middleware;

use Mail;
use Closure;
use JamJar\Api\TextMarketer;
use JamJar\Mail\BalanceLow;

class CheckPartnerBalance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Don't want to be doing this in tests.
        if (app()->environment('testing')) {
            return $next($request);
        }

        // if we are impersonating a user we don't want to remind people.
        if (cache()->has('is-impersonating-user')) {
            return $next($request);
        }

        if (auth()->check()) {
            // Is the user a partner?
            if (auth()->user()->isPartner()) {
                // If their funds are exactly 0, ignore. They likely just signed up.
                if (auth()->user()->funds->getOriginal('funds') != 0) {
                    // If their funds are below £50.00, we need to act.
                    if (auth()->user()->funds->getOriginal('funds') <= 5000) {
                        // Have we already nagged this user in the last 3 days?
                        if (!cache()->has('credit-nag-sent-to-'.auth()->id())) {
                            // Write to the cache to prevent an SMS being sent every page load.
                            cache(['credit-nag-sent-to-'.auth()->id() => true], now()->addDays(3));
                            // Get the mobile number
                            $mobileNumber = auth()->user()->profile->mobile_number;
                            // Set the SMS Content
                            $smsContent = 'Hi '. auth()->user()->first_name .', Your funds are running low on jamjar.com. To top-up, please visit '. route('partnerCredits') .'.';

                            // Send the SMS
                            (new TextMarketer)->sendSms($mobileNumber, 'jamjar.com', $smsContent);

                            // Send an Email
                            Mail::to(auth()->user()->email)->send(new BalanceLow(auth()->user()));
                        }
                    }
                }
            }
        }
        return $next($request);
    }
}
