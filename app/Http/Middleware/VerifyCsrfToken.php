<?php

namespace JamJar\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'partners/checkout',
        'webhooks',
        'webhooks/ctb',
        'webhooks/wbct',
        'api/images/*',
        'api/marketplace-alerts*',
        'api/amazon/instance-change'
    ];
}
