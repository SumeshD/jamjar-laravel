<?php

namespace JamJar\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ShowFacebook
{
    protected $pagesWithFacebook = [
        'associates*',
        'valuations*',
        'account*',
        'vehicle*',
    ];

    public function handle(Request $request, Closure $next)
    {
        $showFacebook = $this->isInPagesWithFacebookArray($request) ? true : false;
        $request->merge(['showFacebook' => $showFacebook]);

        return $next($request);
    }

    protected function isInPagesWithFacebookArray(Request $request): bool
    {
        foreach ($this->pagesWithFacebook as $page) {
            if ($page !== '/') {
                $page = trim($page, '/');
            }

            if ($request->fullUrlIs($page) || $request->is($page)) {
                return true;
            }
        }

        return false;
    }
}
