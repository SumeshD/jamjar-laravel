<?php

namespace JamJar\Http\Middleware;

use Closure;

class IsPartner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user() && auth()->user()->isPartner()) {
            return $next($request);
        }

        alert()
            ->info('You need to be an Associate to access the Associate Area. You can apply to become an Associate here.')
            ->autoclose(5000);

        return redirect(route('becomeAPartner'));
    }
}
