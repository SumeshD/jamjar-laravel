<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use JamJar\Primitives\Money;

class PricingTier extends Model
{
    public $fillable = ['name', 'valuation_to', 'valuation_from', 'fee', 'active'];

    /**
     * Get the fee attribute and format it accordingly.
     *
     * @param  int $funds
     * @return float
     */
    public function getFeeAttribute($fee)
    {
        $money = Money::fromPence($fee);
        return $money->inPoundsAndPence();
    }
}
