<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Defect extends Model
{
    use LogsActivity;

    public $timestamps = false;

    public static function getNonMechanicalReasons(): array
    {
        return ['No Tax', 'SORN', 'No Valid Insurance'];
    }

    public static function areNonMechanicalDefectsOnly(array $nonRunnerReasonIds): bool
    {
        foreach ($nonRunnerReasonIds as $reasonId) {
            $reason = Defect::where('id', '=', $reasonId)->first();
            if (!in_array($reason->title, self::getNonMechanicalReasons())) {
                return false;
            }
        }

        return true;
    }
}
