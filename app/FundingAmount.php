<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use JamJar\Primitives\Money;
use Spatie\Activitylog\Traits\LogsActivity;

class FundingAmount extends Model
{
    use LogsActivity;

    protected $fillable = ['price', 'amount', 'active'];

    public function setPriceAttribute($price)
    {
        $amount = Money::fromPounds($price);
        $price = $amount->inPence();
        $this->attributes['price'] = (int) $price;
    }

    public function getPriceAttribute($price)
    {
        $money = Money::fromPence($price);
        return $money->inPoundsAndPence();
    }
}
