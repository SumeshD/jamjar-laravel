<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;

class AcceptedUserDashboardAlerts extends Model
{

    protected $fillable = ['user_id', 'dashboard_alert_id'];

    public function setStatus($status) {
        $this->attributes['status'] = $status;
    }

}
