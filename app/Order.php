<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use JamJar\Primitives\Money;
use Spatie\Activitylog\Traits\LogsActivity;

class Order extends Model
{
    use LogsActivity;

    /**
     * The fields which aren't protected from Mass Assignment
     *
     * @var array
     */
    protected $fillable = ['product_name', 'product_price', 'pp_payment_id', 'pp_payer_id', 'user_id', 'status'];

    /**
     * Format the Product Price from pence to pounds
     *
     * @param  int $price [description]
     * @return string
     */
    public function getProductPriceAttribute($price)
    {
        $money = Money::fromPence($price);
        return $money->inPoundsAndPence();
    }

    /**
     * Adjust the column this model uses as it's lookup for Dynamic Route Resolution
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'pp_payment_id';
    }

    /**
     * An order belongs to a User
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
