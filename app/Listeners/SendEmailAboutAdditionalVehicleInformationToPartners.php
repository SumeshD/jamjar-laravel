<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\Events\AdditionalVehicleInformationHasBeenProvidedByClient;
use JamJar\Mail\VehicleAdditionalInformationEmail;
use Mail;

class SendEmailAboutAdditionalVehicleInformationToPartners implements ShouldQueue
{
    public function handle(AdditionalVehicleInformationHasBeenProvidedByClient $event)
    {
        $vehicle = $event->vehicle;

        if ($vehicle->hasSale() && ($vehicle->hasCompleteSale() || $vehicle->hasPendingSale())) {
            return;
        }

        foreach ($vehicle->getValuations() as $valuation) {
            $buyer = $valuation->company->user;
            if ($buyer->matrix_partner == 1 && $buyer->send_additional_vehicle_information_emails) {
                $email = new VehicleAdditionalInformationEmail($buyer, $vehicle, $valuation);
                Mail::to($buyer->email)->send($email);
            }
        }
    }
}
