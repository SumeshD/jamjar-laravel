<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use JamJar\Events\ValuationCreated;
use JamJar\Mail\UserValuationEmail;

class SendUserValuations implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  ValuationCreated $event
     * @return void
     */
    public function handle(ValuationCreated $event)
    {
        $userToNotify = $event->vehicle->owner;
        $vehicle = $event->vehicle;
        $valuation = $event->valuations;

        if ($userToNotify) {
            $email = new UserValuationEmail($userToNotify, $vehicle, $valuation);
            Mail::to($userToNotify->email)->send($email);
        }
    }
}
