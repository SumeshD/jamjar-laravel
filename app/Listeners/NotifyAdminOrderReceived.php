<?php

namespace JamJar\Listeners;

use JamJar\Events\OrderReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class NotifyAdminOrderReceived implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  OrderReceived $event
     * @return void
     */
    public function handle(OrderReceived $event)
    {
        $admin = [];
        $admin['name'] = env('MATRIX_ADMIN_NAME', 'Matrix Admin @ jamjar.com');
        $admin['email'] = env('MATRIX_ADMIN_EMAIL', 'info@jamjar.com');
        $email = new \JamJar\Mail\OrderReceived($event->user, $event->product, $admin);

        Mail::to($admin['email'])->send($email);
    }
}
