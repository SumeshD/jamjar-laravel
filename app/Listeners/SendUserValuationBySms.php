<?php

namespace JamJar\Listeners;

use JamJar\Api\TextMarketer;
use JamJar\Events\NewValuationOffered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserValuationBySms
{
    /**
     * Handle the event.
     *
     * @param  NewValuationOffered  $event
     * @return void
     */
    public function handle(NewValuationOffered $event)
    {
        $userToNotify = $event->vehicle->owner;
        $vehicle = $event->vehicle;
        $valuation = $event->valuations;
        $bestValue = number_format($valuation->value_int);
        $acceptanceUri = route('showSavedValuation', $valuation->uuid);

        if (($userToNotify) && ($userToNotify->telephone)) {
            $smsContent = "Good News! Your best offer for your {$vehicle->meta->manufacturer} has improved. View details: {$acceptanceUri}";
            
            (new TextMarketer)->sendSms($userToNotify->telephone, 'jamjar.com', $smsContent);
        }

        /*
        $partnerToNotify = $valuation->company;

        if (($partnerToNotify) && ($partnerToNotify->user) && $partnerToNotify->user->profile && $partnerToNotify->user->profile->mobile_number) {
            $smsContent = "Good News! Your best offer for your {$vehicle->meta->manufacturer} has improved. View details: {$acceptanceUri}";

            (new TextMarketer)->sendSms($partnerToNotify->user->profile->mobile_number, 'jamjar.com', $smsContent);
        }
        */
    }
}
