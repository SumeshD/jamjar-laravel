<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use JamJar\Events\NewValuationOffered;
use JamJar\Primitives\Money;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Setting;

class ChargePartnerForGoodNewsLead
{
    /**
     * Handle the event.
     *
     * @param  NewValuationOffered $event
     * @return void
     */
    public function handle(NewValuationOffered $event)
    {
        $valuation = $event->valuations;
        $fee = $this->getFeeForLead($valuation, true);
        $company = $valuation->company;

        $company->user->funds->spendFunds($fee);
    }

    /**
     * Get the fee amount for the Good News Lead with the Surcharge
     *
     * @param  JamJar\Valuation $lead
     * @return float
     */
    protected function getFeeForLead($lead, $inPence=false)
    {
        if ($lead->company->user->profile->use_flat_fee == 1) {
            if ($inPence == true) {
                return $lead->company->user->profile->getOriginal('flat_fee');
            }
            return $lead->company->user->profile->flat_fee;
        }

        // Get the fee for position 1, for the value of the vehicle
        $calculator = new PartnerFeeCalculator(1, $lead->value_int);

        // calculate and give it to me in pence
        $fee = $calculator->calculate(true);

        // bring it back to an int
        $baseFee = (int) $fee;

        // Get the percentage we want to charge on top of the base fee (e.g, 15% of £13.13)
        $goodNewsPercentage = (int) Setting::get('good_news_lead_percentage_modifier');

        // Good News Fee is the Percentage of the base fee
        $goodNewsFee = ($goodNewsPercentage / 100) * $baseFee;

        // add that to the base fee to get our final fee
        $fee = (int)$baseFee + (int)$goodNewsFee;

        // convert it to pounds and pence and give it back
        $money = Money::fromPence($fee);

        if ($inPence == false) {
            return $money->inPoundsAndPence();
        } else {
            return $money->inPence();
        }
    }
}
