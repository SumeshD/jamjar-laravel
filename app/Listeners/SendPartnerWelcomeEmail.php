<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use JamJar\Events\PartnerCreated;
use JamJar\Mail\MatrixPartnerWelcome;
use Mail;

class SendPartnerWelcomeEmail
{
    use SerializesModels;
    
    /**
     * Handle the event.
     *
     * @param  PartnerCreated $event
     * @return void
     */
    public function handle(PartnerCreated $event)
    {
        $email = new MatrixPartnerWelcome($event->user);
        Mail::to($event->user->email)->send($email);
    }
}
