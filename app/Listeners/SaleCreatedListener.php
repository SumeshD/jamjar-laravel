<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use JamJar\Events\SaleCreated;
use JamJar\Mail\NewSaleCreated;
use JamJar\Mail\NewSaleCreatedSellerConfirmation;
use Mail;

class SaleCreatedListener //implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  SaleCreated $event
     * @return void
     */
    public function handle(SaleCreated $event)
    {
        $company = $event->sale->company;
        $vehicle = $event->sale->valuation->vehicle;
        $sale = $event->sale;

        $emailToBuyer = new NewSaleCreated($company, $vehicle, $sale);
        Mail::to($company->user->email)->send($emailToBuyer);

        $emailToSeller = new NewSaleCreatedSellerConfirmation($company, $vehicle, $sale);
        Mail::to($sale->user->email)->send($emailToSeller);
    }
}
