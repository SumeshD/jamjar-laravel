<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use JamJar\Company;
use JamJar\Events\PartnerCreated;
use JamJar\PartnerLocation;

class CreatePartnerLocation
{
    /**
     * Handle the event.
     *
     * @param  PartnerCreated $event
     * @return void
     */
    public function handle(PartnerCreated $event)
    {
        $company = Company::where('name', $event->request['company_name'])->orderBy('created_at', 'DESC')->first();

        PartnerLocation::create([
            'user_id' => $event->user->id,
            'company_id' => $company ? $company->id : ($event->user->company ? $event->user->company->id : null),
            'name' => $event->request['company_name'],
            'telephone' => $event->request['telephone_number'],
            'address_line_one' => $event->request['address_line_one'],
            'address_line_two' => $event->request['address_line_two'],
            'town' => $event->request['town'] ?? null,
            'city' => $event->request['city'],
            'county' => $event->request['county'] ?? $event->request['city'],
            'postcode' => $event->request['postcode'],
            'allow_collection' => 1,
            'allow_dropoff' => 1,
            'primary' => 1
        ]);
    }
}
