<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use JamJar\Events\PartnerApproved;
use JamJar\Events\VehicleAddedToMarketplace;
use JamJar\Mail\MarketplaceAlerts\InstantEmail;
use JamJar\Mail\MatrixPartnerApproved;
use JamJar\Services\VehicleFilter\MarketplaceAlertsService;
use JamJar\Api\TextMarketer;
use Mail;
use \Exception;

class SendInstantMarketplaceAlerts implements ShouldQueue
{
    use SerializesModels;

    public function handle(VehicleAddedToMarketplace $event)
    {
        $marketplaceAlertService = new MarketplaceAlertsService();
        $alertsToRemind = $marketplaceAlertService->getAlertsToSendInstantReminders($event->vehicle);

        foreach ($alertsToRemind as $alertToRemind) {
            $user = $alertToRemind->getUser();

            try {
                $email = new InstantEmail($user, $event->vehicle, $alertToRemind);
                Mail::to($user->email)->send($email);
            } catch (Exception $e) {
                \Log::info($e);
            }

            if($user->profile) {
                try{
                    $smsContent = 'A new vehicle has been added to the Marketplace that matches one of your alert rules ' . route('partnerAddNewLead', $event->vehicle->getId());

                    (new TextMarketer)->sendSms($user->profile->mobile_number, 'jamjar.com', $smsContent);
                }catch(Exception $e){
                    \Log::info($e);
                }
            }
        }
    }
}
