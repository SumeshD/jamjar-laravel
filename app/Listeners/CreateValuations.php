<?php

namespace JamJar\Listeners;

use JamJar\Valuation;
use Ramsey\Uuid\Uuid;
use JamJar\Api\CapApi;
use JamJar\VehicleModel;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\Events\ValuationCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\Primitives\PartnerFeeCalculator;

class CreateValuations
{
    /**
     * Handle the event.
     *
     * @param  ValuationCreated $event
     * @return void
     */
    public function handle(ValuationCreated $event)
    {
        // Sort out the Type
        $type = ($event->vehicle->type == 'Light Commercial Vehicle') ? 'LCV' : 'CAR';

        // Get the Manufacturer
        $manufacturer = VehicleManufacturer::where('name', $event->vehicle->meta->manufacturer)->where('vehicle_type', $type)->first();

        // Get the model
        if ($event->vehicle->meta->cap_model_id) {
            $model = VehicleModel::where('cap_model_id', $event->vehicle->meta->cap_model_id)->first();
        } else {
            $model = VehicleModel::where('name', $event->vehicle->meta->model)->first();
        }

        // Cannot find the model?
        if (is_null($model)) {
            // Fetch it from the API
            $model = $this->fetchModelFromApi($manufacturer, $event->vehicle->meta->cap_model_id, $type);
        }

        // Grab the derivative (which we always have thanks to `fetchModelFromApi`)
        $derivative = VehicleDerivative::where('name', $event->vehicle->meta->derivative)
            ->where('model_id', $model->cap_model_id)
            ->first();

        if ($model && !$derivative) {
            // for some reason we don't get derivatives, let's try it again
            $modelFromApi = $this->fetchModelFromApi($manufacturer, $event->vehicle->meta->cap_model_id, $type);

            if ($modelFromApi) {
                $model = $modelFromApi;
                $derivative = VehicleDerivative::where('name', $event->vehicle->meta->derivative)
                    ->where('model_id', $model->cap_model_id)
                    ->first();
            }
        }

        // Generate a UUID
        $uuid = Uuid::uuid4()->toString();

        // Loop through our valuations
        foreach ($event->valuations as $valuation) {

            // Store it in the cache for 5 days
            cache(
                [
                    session('vrm') . '-company-'.$valuation['company']->id.'-valuation' => ['dropoff_value' => $valuation['dropoff_value'],'dropoff_value' => $valuation['collection_value']]
                ],
                now()->addDays(5)
            );

            // If they are paying for a position, we need to account for this
            if ($valuation['company']->position) {
                $calculator = new PartnerFeeCalculator($valuation['company']->position->position, $valuation['dropoff_value']);
            } else {
                $calculator = new PartnerFeeCalculator($valuation['position'], $valuation['dropoff_value']);
            }

            // If they're an API partner, we will just charge for position 1
            if ($valuation['partner_type'] == 'api') {
                $calculator = new PartnerFeeCalculator(1, $valuation['dropoff_value']);
            }

            // Get the acceptance URI to store 
            // If they're matrix, we can set this to null
            $acceptance_uri = null;
            // if they're an API partner we need to store this
            if ($valuation['partner_type'] == 'api') {
                $acceptance_uri = $valuation['website'];
            }

            // if(isset($valuation['valuation']['admin_fee'])){
            //     dd($valuation['valuation']);
            // }

            $conditionValues = [
                'good' => null,
                'fair' => null,
                'poor' => null,
            ];

            foreach ($conditionValues as $conditionName => $conditionValue) {
                if (
                    isset($valuation[$conditionName]) &&
                    (
                        isset($valuation[$conditionName]['collection']) ||
                        isset($valuation[$conditionName]['dropoff']
                    ))
                ) {
                    $conditionValues[$conditionName] = isset($valuation[$conditionName]['collection']) ?
                        $valuation[$conditionName]['collection'] :
                        $valuation[$conditionName]['dropoff'];
                }
            }

            // Create a new valuation
            $v = Valuation::firstOrCreate([
                'vehicle_id' => $event->vehicle->id,
                'manufacturer_id' => $manufacturer->cap_manufacturer_id,
                'model_id' => $model->cap_model_id ?? $event->vehicle->meta->cap_model_id,
                'derivative_id' => $derivative->cap_derivative_id ?? 0,
                'company_id' => $valuation['company']->id,
                'user_id' => auth()->check() ? auth()->user()->id : 0,
                'collection_value' => $valuation['collection_value'] ?? null,
                'dropoff_value' => $valuation['dropoff_value'] ?? null,
                'cap_value' => $valuation['valuation']['cap']['average'] ?? 0,
                'cap_value_retail' => $valuation['valuation']['cap']['retail'] ?? 0,
                'cap_value_clean' => $valuation['valuation']['cap']['clean'] ?? 0,
                'cap_value_below' => $valuation['valuation']['cap']['below'] ?? 0,
                'position' => $valuation['position'],
                'fee' => $calculator->calculate(true),
                'partner_type' => $valuation['partner_type'],
                'uuid' => $uuid,
                'admin_fee' => $valuation['admin_fee'] ?? null,
                'payment_method' => isset($valuation['payment_method']) ? $valuation['payment_method'] : 'Bank Transfer',
                'acceptance_uri' => $acceptance_uri ?? null,
                'temp_ref' => $valuation['temp_ref'] ?? null,
                'distance' => $valuation['distance'] ?? null,
                'vehicle_condition_value_good' => $conditionValues['good'],
                'vehicle_condition_value_fair' => $conditionValues['fair'],
                'vehicle_condition_value_poor' => $conditionValues['poor'],
                'location_id' => $valuation['location'] ? $valuation['location']->id : null,
            ]);
            
            // dd('vrm-' . session('vrm') . '-company-'.$valuation['company']->id.'-valuation-id', $v);
            // Store the valuation in the cache
            cache(
                [
                    session('vrm') . '-company-'.$valuation['company']->id.'-valuation-id' => $v->id
                ],
                now()->addDays(5)
            );

        }
    }

    /**
     * Fetch the model from the API
     *
     * @param  JamJar\VehicleManufacturer $manufacturer The VehicleManufacturer stored in the database
     * @param  int                        $capModelId   CAP Model ID
     * @param  string                     $type         The vehicles type, CAR or LCV
     * @return JamJar\VehicleModel
     */
    protected function fetchModelFromApi($manufacturer, $capModelId, $type)
    {
        if ($manufacturer) {
            // Get it from the API, store results in collection
            $models = collect((new CapApi)->getModels($manufacturer->cap_manufacturer_id, $type));

            // Use higher order collection to reject all models which are irrelevant to this request
            $model = $models->reject(
                function ($model) use ($capModelId) {
                    return $capModelId != $model['cap_model_id'];
                }
            )->first();

            if ($model) {
                // Save the model
                $this->saveModel($model, $manufacturer);

                // Save the derivatives
                $this->saveDerivatives($model, $manufacturer);

                $model = VehicleModel::where('cap_model_id', $capModelId)->first();
            }

            return $model;
        }
    }

    /**
     * Process the API result and save into the database
     *
     * @param  array                      $model        The returned Model information from the CAP-HPI API
     * @param  JamJar\VehicleManufacturer $manufacturer The manufacturer from our internal database
     * @return JamJar\VehicleModel
     */
    protected function saveModel($model, $manufacturer)
    {
        $fuelType = str_contains($model->get('name'), 'DIESEL') ? 'Diesel' : 'Petrol';

        $introduced = $model->get('introduced');
        $discontinued = $model->get('discontinued') == 0 ? date('Y') : $model->get('discontinued');
        $plateYears = range($introduced, $discontinued);

        return VehicleModel::updateOrCreate(
            [
                'cap_model_id' => $model->get('cap_model_id'),
            ],
            [
                'cap_model_id' => $model->get('cap_model_id'),
                'name' => $model->get('name'),
                'manufacturer_id' => (int) $manufacturer->cap_manufacturer_id,
                'introduced_date' => $model->get('introduced'),
                'discontinued_date' => $model->get('discontinued'),
                'fuel_type' => $fuelType,
                'plate_years' => implode(',', $plateYears)
            ]
        );
    }

    /**
     * Process the API results and save to the database
     *
     * @param  array                      $model        The returned Model information from the CAP-HPI API
     * @param  JamJar\VehicleManufacturer $manufacturer Vehicle Manufacturer from our Database
     * @return void
     */
    protected function saveDerivatives($model, $manufacturer)
    {
        $derivatives = (new CapApi)->getDerivatives($model['cap_model_id'], $manufacturer->vehicle_type);

        foreach ($derivatives as $derivative) {
            $deriv = VehicleDerivative::updateOrCreate(
                [
                    'cap_derivative_id' => $derivative->get('cap_derivative_id'),
                ],
                [
                    'cap_derivative_id' => $derivative->get('cap_derivative_id'),
                    'name' => $derivative->get('name'),
                    'model_id' => $model['cap_model_id'],
                ]
            );
        }
    }
}
