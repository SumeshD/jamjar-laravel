<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use JamJar\Events\PartnerApproved;
use JamJar\Role;

class CreatePartner
{
    use SerializesModels;

    /**
     * Handle the event.
     *
     * @param  PartnerApproved $event
     * @return void
     */
    public function handle(PartnerApproved $event)
    {
        $role = Role::where('name', 'Partner')->first();
        $event->user->update(['matrix_partner' => '1', 'role_id' => $role->id]);
    }
}
