<?php

namespace JamJar\Listeners;

use JamJar\Events\PartnerCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreatePartnerFunds
{
    /**
     * Handle the event.
     *
     * @param  PartnerCreated $event
     * @return void
     */
    public function handle(PartnerCreated $event)
    {
        $event->user->funds()->create(
            [
            'funds' => 0,
            ]
        );
    }
}
