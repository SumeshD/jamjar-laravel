<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use JamJar\Events\PartnerApproved;
use JamJar\Mail\MatrixPartnerApproved;
use Mail;

class SendPartnerApprovedEmail implements ShouldQueue
{
    use SerializesModels;

    /**
     * Handle the event.
     *
     * @param  PartnerApproved $event
     * @return void
     */
    public function handle(PartnerApproved $event)
    {
        $email = new MatrixPartnerApproved($event->user);
        Mail::to($event->user->email)->send($email);
    }
}
