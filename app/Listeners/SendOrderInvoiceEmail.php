<?php

namespace JamJar\Listeners;

use JamJar\Events\OrderReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\Mail\Invoice;
use Mail;

class SendOrderInvoiceEmail implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  OrderReceived $event
     * @return void
     */
    public function handle(OrderReceived $event)
    {
        $email = new Invoice($event->user);

        Mail::to($event->user->email)->send($email);
    }
}
