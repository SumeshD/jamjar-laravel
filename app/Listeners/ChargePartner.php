<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use JamJar\Events\ValuationCreated;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\UserStatement;

class ChargePartner
{
    /**
     * Handle the event.
     *
     * @param  ValuationCreated $event
     * @return void
     */
    public function handle(ValuationCreated $event)
    {
        foreach ($event->valuations as $valuation) {
            if ($valuation['api_partner']) {
                continue;
            }

            $company = $valuation['company'];
            if ($company) {
                // if ($valuation['position'] <= $company->position->position) {
                $fee = $this->getFee($company, $valuation);
                
                if ($company->position->position) {
                    !isset(request()->_debug) ?: dump($company->name .' charged for '.$company->position->position.'. Appeared in '.$valuation['position'] . '. Charged '. $fee . ' pence');
                }

                !isset(request()->_debug) ?: dump($company->name .' charged '. $fee . ' pence');

                $this->createStatementEntry($valuation, $company, $event, $fee);

                // Don't try to charge auto partners
                if ($company->user->funds) {
                    $company->user->funds->spendFunds($fee);
                }

                // }
            }
        }
    }

    /**
     * Get the fee to charge based on calculator or flat fee
     *
     * @param  array $company
     * @return int
     */
    protected function getFee($company, $valuation)
    {
        if ($company->user->profile->use_flat_fee == 1) {
            $fee = $company->user->profile->getOriginal('flat_fee');
        } else {
            $calculator = new PartnerFeeCalculator($valuation['position'], $valuation['dropoff_value']);
            $fee = $calculator->calculate(true);
        }
        return $fee;
    }

    /**
     * Create the statement entry
     *
     * @param  array $valuation
     * @param  array $company
     * @param  array $event
     * @param  int   $fee
     * @return JamJar\UserStatement
     */
    protected function createStatementEntry($valuation, $company, $event, $fee)
    {
        $statement = new UserStatement;

        $meta = json_encode(
            [
            'valuation' => [
                'collection_value' => $valuation['collection_value'],
                'dropoff_value' => $valuation['dropoff_value'],
                'good' => [
                    'collection_value' => $valuation['collection_value'],
                    'dropoff_value' => $valuation['dropoff_value'],
                ],
                'fair' => [
                    'collection_value' => $valuation['collection_value'],
                    'dropoff_value' => $valuation['dropoff_value'],
                ],
                'poor' => [
                    'collection_value' => $valuation['collection_value'],
                    'dropoff_value' => $valuation['dropoff_value'],
                ],
            ],
            'vehicle' => $event->vehicle,
            'position' => $valuation['position']
            ]
        );

        $statement = $statement->create(
            [
            'user_id' => $company->user->id,
            'type' => 'withdrawal',
            'amount' => $fee ?? 0,
            'meta' => $meta
            ]
        );

        return $statement;
    }
}
