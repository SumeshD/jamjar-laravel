<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use JamJar\Events\VehicleCreated;
use JamJar\Mail\VehicleOwnerPassword;
use JamJar\User;
use Mail;

class SendVehicleOwnerPassword implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  VehicleCreated $event
     * @return void
     */
    public function handle(VehicleCreated $event)
    {
        $user = User::where('email', $event->request['email'])->where('verified', 0)->first();
        if ($user) {
            $email = new VehicleOwnerPassword($user);
            Mail::to($user->email)->send($email);
        }
    }
}
