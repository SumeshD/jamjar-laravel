<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use JamJar\Events\PartnerCreated;
use JamJar\Mail\MatrixAdminNewPartner;
use Mail;

class SendAdminPartnerEmail
{
    use SerializesModels;
    
    /**
     * Handle the event.
     *
     * @param  PartnerCreated $event
     * @return void
     */
    public function handle(PartnerCreated $event)
    {
        $admin = [];
        $admin['name'] = env('MATRIX_ADMIN_NAME', 'Matrix Admin @ jamjar.com');
        $admin['email'] = env('MATRIX_ADMIN_EMAIL', 'info@jamjar.com');

        $email = new MatrixAdminNewPartner($event->user, $admin);

        Mail::to($admin['email'])->send($email);
    }
}
