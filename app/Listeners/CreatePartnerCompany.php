<?php

namespace JamJar\Listeners;

use JamJar\Events\PartnerCreated;

class CreatePartnerCompany
{
    /**
     * Handle the event.
     *
     * @param  PartnerCreated $event
     * @return void
     */
    public function handle(PartnerCreated $event)
    {
        $buyFromOtherPartners = array_key_exists('buy_vehicles_from_other_partners', $event->request) ?
            ($event->request['buy_vehicles_from_other_partners'] == 'on' ? true : false) :
            false;

        $company = $event->user->company()->create([
            'name' => $event->request['company_name'],
            'telephone' => $event->request['telephone_number'],
            'vat_number' => $event->request['vat_number'],
            'address_line_one' => $event->request['address_line_one'],
            'address_line_two' => $event->request['address_line_two'],
            'town' => $event->request['town'] ?? null,
            'city' => $event->request['city'],
            'county' => $event->request['county'],
            'postcode' => $event->request['postcode'],
            'buy_vehicles_from_other_partners' => $buyFromOtherPartners,
        ]);
        
        $company->position()->create([
            'position' => null,
            'user_id' => $event->user->id
        ]);

        $event->user->profile()->update(['company_id' => $company->id]);
    }
}
