<?php

namespace JamJar\Listeners;

use JamJar\Events\CreateOfferDerivative;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaveOfferDerivative implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  CreateOfferDerivative $event
     * @return void
     */
    public function handle(CreateOfferDerivative $event)
    {
        $event->offer->derivatives()->updateOrCreate(
            [
                'cap_model_id' => $event->derivative['cap_model_id'],
                'cap_derivative_id' => $event->derivative['cap_derivative_id']
            ],
            [
                'cap_model_id' => $event->derivative['cap_model_id'],
                'cap_derivative_id' => $event->derivative['cap_derivative_id'],
                'base_value' => $event->derivative['base_value'] ?? $event->offer->base_value
            ]
        );
    }
}
