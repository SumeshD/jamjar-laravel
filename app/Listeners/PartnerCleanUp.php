<?php

namespace JamJar\Listeners;

use JamJar\Events\PartnerDenied;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PartnerCleanUp
{
    /**
     * Handle the event.
     *
     * @param  PartnerDenied $event
     * @return void
     */
    public function handle(PartnerDenied $event)
    {
        $user = $event->user;
        $company = $user->company;
        $website = $user->company->website;
        $pages = $website->pages;
        $locations = $user->company->location;
        $funds = $user->funds;

        $company->delete();
        $website->delete();
        $pages->each(
            function ($page) {
                $page->delete();
            }
        );
        $locations->each(
            function ($location) {
                $location->delete();
            }
        );
        $funds->delete();
    }
}
