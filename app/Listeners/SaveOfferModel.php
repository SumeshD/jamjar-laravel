<?php

namespace JamJar\Listeners;

use JamJar\Events\CreateOfferModel;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaveOfferModel implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  CreateOfferModel $event
     * @return void
     */
    public function handle(CreateOfferModel $event)
    {
        $event->offer->models()->updateOrCreate(
            [
                'cap_model_id' => $event->model['cap_model_id'],
            ],
            [
                'cap_model_id' => $event->model['cap_model_id'],
                'base_value' => $event->model['base_value'] ?? $event->offer->base_value
            ]
        );
    }
}
