<?php

namespace JamJar\Listeners;

use Illuminate\Queue\SerializesModels;
use JamJar\Events\SaleHasBeenRemoved;
use JamJar\Mail\SaleHasBeenRemovedEmail;
use Mail;

class SendEmailAboutRemovedSaleToPartner
{
    use SerializesModels;

    public function handle(SaleHasBeenRemoved $event)
    {
        $email = new SaleHasBeenRemovedEmail($event->seller, $event->buyer, $event->sale, $event->newVehicle, $event->oldVehicle , $event->valuation, $event->customerCancel);

        if ($event->buyer->send_additional_emails) {
            Mail::to($event->buyer->email)->send($email);
        }
    }
}
