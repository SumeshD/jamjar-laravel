<?php

namespace JamJar\Listeners;

use JamJar\Events\PartnerCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreatePartnerPosition
{
    /**
     * Handle the event.
     *
     * @param  PartnerCreated $event
     * @return void
     */
    public function handle(PartnerCreated $event)
    {
        // @deprecated
    }
}
