<?php

namespace JamJar\Listeners;

use JamJar\Events\PartnerCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\PartnerWebsite;

class CreatePartnerWebsite
{
    /**
     * Handle the event.
     *
     * @param  PartnerCreated $event
     * @return void
     */
    public function handle(PartnerCreated $event)
    {
        $slugExists = PartnerWebsite::where('slug', '=', str_slug($event->user->company->name))->exists();

        $event->user->website()->create(
            [
            'company_id'    => $event->user->company->id,
            'slug'          => ($slugExists) ? str_slug($event->user->company->name). '-' .$event->user->company->id : str_slug($event->user->company->name)
            ]
        );
    }
}
