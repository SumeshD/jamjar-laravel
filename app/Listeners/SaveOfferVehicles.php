<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use JamJar\Events\OfferUpdated;

class SaveOfferVehicles implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  OfferUpdated $event
     * @return void
     */
    public function handle(OfferUpdated $event)
    {
        Log::debug('Models:'. $event->models);
        Log::debug('Derivatives:'. $event->derivatives);
        //save models
        foreach ($event->models as $model) {
            $event->offer->models()->updateOrCreate(
                [
                    'cap_model_id' => $model['cap_model_id'],
                ],
                [
                    'cap_model_id' => $model['cap_model_id'],
                    'base_value' => $model['base_value'] ?? $event->offer->base_value
                ]
            );

            Log::debug($model['cap_model_id'] . ' model saved');
        }

        // save derivs
        foreach ($event->derivatives as $derivative) {
            $event->offer->derivatives()->updateOrCreate(
                [
                    'cap_model_id' => $derivative['cap_model_id'],
                    'cap_derivative_id' => $derivative['cap_derivative_id']
                ],
                [
                    'cap_model_id' => $derivative['cap_model_id'],
                    'cap_derivative_id' => $derivative['cap_derivative_id'],
                    'base_value' => $derivative['base_value'] ?? $event->offer->base_value
                ]
            );
            Log::debug($derivative['cap_derivative_id'] . ' derivative saved');
        }

        // flush cache for offers
        
        // fire off an email saying their offers have been published.
    }
}
