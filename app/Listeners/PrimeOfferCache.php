<?php

namespace JamJar\Listeners;

use JamJar\Events\OfferSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PrimeOfferCache
{
    /**
     * Handle the event.
     *
     * @param  OfferSaved $event
     * @return void
     */
    public function handle(OfferSaved $event)
    {
        return cache()->remember(
            'offer-'.$event->offer->slug,
            now()->addMonths(1),
            function () use ($event) {
                return $event->offer;
            }
        );
    }
}
