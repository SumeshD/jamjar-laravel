<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use JamJar\Events\VehicleCreated;
use JamJar\User;

class RegisterVehicleOwner implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  VehicleCreated $event
     * @return void
     */
    public function handle(VehicleCreated $event)
    {
        $user = User::where('email', $event->request['email'])->first();
        if ($user === null) {
            $user = new User;
            $user->registerUser($event->request['fullname'], $event->request['email'], $event->request['password']);
        }
    }
}
