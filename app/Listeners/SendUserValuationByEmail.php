<?php

namespace JamJar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use JamJar\Events\NewValuationOffered;
use JamJar\Mail\UserValuationEmail;
use Mail;

class SendUserValuationByEmail implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  NewValuationOffered $event
     * @return void
     */
    public function handle(NewValuationOffered $event)
    {
        $userToNotify = $event->vehicle->owner;
        $vehicle = $event->vehicle;
        $valuation = $event->valuations;

        if ($userToNotify && $userToNotify->user && $userToNotify->user->send_additional_emails) {
            $email = new UserValuationEmail($userToNotify, $vehicle, $valuation);
            Mail::to($userToNotify->email)->send($email);
        }

        /*
        $partnerToNotify = $valuation->company;

        if($partnerToNotify && $partnerToNotify->user && $partnerToNotify->user->send_additional_emails) {

            $email = new UserValuationEmail($partnerToNotify->user, $vehicle, $valuation);
            Mail::to($partnerToNotify->user->email)->send($email);
        }
        */

    }
}
