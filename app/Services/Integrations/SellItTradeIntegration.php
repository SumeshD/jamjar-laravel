<?php

namespace JamJar\Services\Integrations;

use JamJar\Vehicle;
use JamJar\VehicleManufacturer;
use JamJar\Profile;
use JamJar\VehicleModel;
use JamJar\VehicleDerivative;
use \GuzzleHttp\Client as HttpClient;

class SellItTradeIntegration
{

    /** @var string $jamjarUsername */
    private $jamjarUsername;

    /** @var string $jamjarPassword */
    private $jamjarPassword;

    /** @var string $baseUrl */
    private $baseUrl;

    /** @var HttpClient */
    private $client;

    /** @var Profile */
    private $profile;

    /** @var array  */
    private $carDetails = [];

    public function __construct()
    {
        $this->jamjarUsername = env('SELL_IT_TRADE_JAMJAR_USERNAME');
        $this->jamjarPassword = env('SELL_IT_TRADE_JAMJAR_PASSWORD');
        $this->baseUrl = env('SELL_IT_TRADE_ENV') == 'sandbox' ? 'https://sellittrade.nadevelopment.co.uk/desktopmodules/cvsellittrade/externalservice.asmx/' : '';

        $this->client = new HttpClient([
            'base_uri' => $this->baseUrl
        ]);

        $this->profile = auth()->user()->profile;
    }

    public function createDealer(){
        $params = [
            'dealerName' => $this->profile->name,
            'dealerEmail' => $this->profile->email,
            'address1' => $this->profile->address_line_one,
            'address2' => $this->profile->address_line_two,
            'city' => $this->profile->city,
            'region' => $this->profile->country ?? ' ',
            'postcode' => $this->profile->postcode,
            'mobile' => $this->profile->mobile_number,
            'dealerPassword' => bin2hex(random_bytes(5))
        ];

        $response = $this->makeCall('CreateDealer', 'POST', $params);

        return $response;
    }

    public function addTradeValuation(){
        $params = $this->carDetails;

        $response = $this->makeCall('AddTradeValuation', 'POST', $params);

        return $response;
    }

    public function valueVehicle()
    {
        $params = $this->carDetails;

        $response = $this->makeCall('ValueVehicle', 'POST', $params);

        return $response;
    }

    public function loginDealer(){
        $params = [
            'dealerUsername' => $this->profile->email
        ];

        $response = $this->makeCall('LoginDealer', 'POST', $params);

        return $response;
    }

    private function makeCall($url, $method = 'GET', $params = []){
        $params = array_merge(['username' => $this->jamjarUsername, 'password' => $this->jamjarPassword], $params);

        $requestParams = [
            'json' => $params,
            'header' => [
                'Content-Type' => 'application/json'
            ]
        ];
        try{
            $response = $this->client->request($method, $url, $requestParams);
        } catch (Exception $e) {
            \Log::debug('SellItTrade API ('.$url.') Failure:'. $e->getMessage());

            throw new \Exception();
        }

        return json_decode($response->getBody());
    }

    public function addCarDetailsFull(Vehicle $vehicle)
    {

        $manufacturer = VehicleManufacturer::find($vehicle->manufacturer_id);
        $model = VehicleModel::find($vehicle->model_id);
        $derivative = VehicleDerivative::find($vehicle->derivative_id);
        $this->carDetails = [
            'sellingDealerUsername' => $this->profile->email,
            'bodyType' => $vehicle->meta->body_type,
            'colour' => $vehicle->meta->colour->title,
            'cylinder' => 0,
            'doors' => 0,
            'engineCapacity' => $vehicle->meta->engine_capacity,
            'fuelType' => $vehicle->meta->fuel_type,
            'gears' => 0,
            'manufacturer' => $manufacturer->name,
            'mileage' => $vehicle->meta->mileage,
            'yearOfManufacture' => substr($vehicle->meta->plate_year,0,4),
            'registrationNumber' => $vehicle->getNumberPlate(),
            'model' => $model->name,
            'variant' => $derivative->name,
            'transmissionDrive' => '-',
            'tradeInPrice' => 0,
            'transmission' => $vehicle->meta->transmission,
            'description' => ''
        ];

        return $this;
    }

    public function addCarDetails(Vehicle $vehicle)
    {
        $this->carDetails = [
            'sellingDealerUsername' => $this->profile->email,
            'mileage' => floatval(preg_replace('/[^\d.]/', '', $vehicle->meta->mileage)),
            'registrationNumber' => $vehicle->getNumberPlate(),
        ];

        return $this;
    }

}