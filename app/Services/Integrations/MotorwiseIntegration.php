<?php

namespace JamJar\Services\Integrations;

use JamJar\Company;
use JamJar\Vehicle;
use JamJar\VehicleManufacturer;
use JamJar\Profile;
use JamJar\VehicleModel;
use JamJar\VehicleDerivative;
use \GuzzleHttp\Client as HttpClient;

class MotorwiseIntegration
{
    const MOTORWISE_COMPANY_NAME = 'Motorwise';

    const MOTORWISE_QUOTE_STATUS_CANCELLED = 'CANCELLED';
    const MOTORWISE_QUOTE_STATUS_EXPIRED = 'EXPIRED';
    const MOTORWISE_QUOTE_STATUS_COMPLETED = 'COMPLETED';
    const MOTORWISE_QUOTE_STATUS_ACCEPTED = 'ACCEPTED';
    const MOTORWISE_QUOTE_STATUS_PENDING = 'PENDING';

    private $response = "";

    public function __construct($baseUrl = null, $username = null, $password = null)
    {
        $this->baseUrl = env('MW_ENV') == 'live' ?
            'https://api.motorwise.com' :
            'https://api.motorwise.com/staging';
        $this->username = env('MW_USERNAME');
        $this->password = env('MW_PASSWORD');
    }

    public function acceptQuote(int $quoteId) {
        // Build our client
        $client = new HttpClient([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode($this->username.':'.$this->password)
            ]
        ]);
        $username = explode(' ', auth()->user()->name);
        $formParams = [
            'firstName' => $username[0],
            'lastName' =>  $username[1],
            'email' => auth()->user()->email,
            'phone' => auth()->user()->profile->mobile_number
        ];

        // Get a Valuation Reference
        try {
            $response = $client->post($this->baseUrl.'/acceptquote/'.$quoteId, [
                'form_params' => $formParams,
                'timeout' => 3,
            ]);

            $this->request = json_encode($formParams);

        } catch (\Exception $exception) {
            \Log::debug('MW API Failure:'. $exception->getMessage());
            alert()->error('Can\'t accept this valuation');
            print_r($exception->getMessage()); die;
            // Report the Exception here.
            return [];
        }

        // Get the body
        $this->response = $response->getBody()->getContents();
        // Parse it
        $body = json_decode($this->response, true);

        \Log::debug('MW API Response:\n\n'.print_r($body, true));
    }

    public function checkQuoteStatus(int $quoteId) {
        $client = new HttpClient([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode($this->username.':'.$this->password)
            ]
        ]);

        try {
            $response = $client->get($this->baseUrl.'/getquotestatus/'.$quoteId, [
                'timeout' => 3
            ]);

        } catch (\Exception $exception) {
            \Log::debug('MW API Failure:'. $exception->getMessage());
            print_r($exception->getMessage());
            // Report the Exception here.
            return [];
        }

        $this->response = $response->getBody()->getContents();

        $body = json_decode($this->response, true);

        \Log::debug('MW API Response:\n\n'.print_r($body, true));

        if($body['status'] == 1){
            return $body['returnData']['status'];
        }
        return self::MOTORWISE_QUOTE_STATUS_PENDING;

    }

    public function getCompanyId() {
        $motorwiseCompany = Company::where('name',self::MOTORWISE_COMPANY_NAME)->first();
        return $motorwiseCompany->id;
    }

}