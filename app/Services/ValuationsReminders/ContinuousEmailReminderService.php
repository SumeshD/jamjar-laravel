<?php

namespace JamJar\Services\ValuationsReminders;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use JamJar\Mail\ValuationReminderEmail;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;
use Illuminate\Support\Facades\Mail;
use JamJar\VehicleReminder;

class ContinuousEmailReminderService extends ReminderService
{
    protected function getReminderName(): string
    {
        return 'continuous_reminder';
    }

    protected function getReminderMinTimeInMinutes(): int
    {
        return 60 * 24;
    }

    protected function getReminderMaxTimeInMinutes(): int
    {
        return 60 * 24 * 4;
    }

    protected function sendReminderEmail(Vehicle $vehicle, Valuation $valuation)
    {
        $user = $vehicle->owner->user;

        Mail::to($user->email)->send(new ValuationReminderEmail($user, $valuation, $vehicle, 'continuous_reminder'));
    }

    protected function legacyReminderHasBeenSend(Vehicle $vehicle): bool
    {
        return (bool) Valuation::where([
            ['vehicle_id', '=', $vehicle->id],
            ['24h_email_reminder_sent', '=', true],
        ])->first();
    }
}
