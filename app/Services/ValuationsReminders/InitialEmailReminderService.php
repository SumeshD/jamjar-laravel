<?php

namespace JamJar\Services\ValuationsReminders;

use JamJar\Mail\ValuationReminderEmail;
use JamJar\Valuation;
use JamJar\Vehicle;
use Illuminate\Support\Facades\Mail;

class InitialEmailReminderService extends ReminderService
{
    protected function getReminderName(): string
    {
        return 'initial_reminder';
    }

    protected function getReminderMinTimeInMinutes(): int
    {
        return 5;
    }

    protected function getReminderMaxTimeInMinutes(): int
    {
        return 60 * 24;
    }

    protected function sendReminderEmail(Vehicle $vehicle, Valuation $valuation)
    {
        $user = $vehicle->owner->user;

        Mail::to($user->email)->send(new ValuationReminderEmail($user, $valuation, $vehicle, 'initial_reminder'));
    }

    protected function legacyReminderHasBeenSend(Vehicle $vehicle): bool
    {
        return (bool) Valuation::where([
            ['vehicle_id', '=', $vehicle->id],
            ['1h_email_reminder_sent', '=', true],
        ])->first();
    }
}
