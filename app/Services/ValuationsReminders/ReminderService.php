<?php

namespace JamJar\Services\ValuationsReminders;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use JamJar\Valuation;
use JamJar\Vehicle;
use JamJar\VehicleReminder;

abstract class ReminderService
{
    /** @var int */
    private $sentEmailsCount = 0;

    public function sendReminders() {
        $vehicles = $this->getVehiclesToRemind();

        foreach ($vehicles as $vehicle) {

            if ($this->legacyReminderHasBeenSend($vehicle)) {
                continue;
            }

            if ($vehicle->hasSale()) {
                continue;
            }

            if ($vehicle->hasGoodNewsLeadValuation()) {
                continue;
            }

            $lead = Vehicle::getGoodNewsLeadByVehicle($vehicle);

            if ($lead->highestNonExpiredValuation == null) {
                continue;
            }

            if ($vehicle->owner->user == null) {
                continue;
            }

            if (!$vehicle->owner->user->send_additional_emails) {
                continue;
            }

            $this->sendReminderEmail($vehicle, $lead->highestNonExpiredValuation);
            $this->markReminderAsSent($vehicle);
            $this->sentEmailsCount++;
        }
    }

    public function getSentMessagesCount(): int
    {
        return $this->sentEmailsCount;
    }

    /** @return array|Vehicle[] */
    protected function getVehiclesToRemind()
    {
        $query = Vehicle::select(['*', 'vehicles.id as vehicle_id'])->leftJoin('vehicle_reminders', function($join) {
                $join
                    ->on('vehicle_reminders.vehicle_id', '=', 'vehicles.id')
                    ->where([
                        ['vehicle_reminders.type', '=', $this->getReminderName()],
                        ['vehicle_reminders.start_date_of_sending_reminders',  '=', DB::raw('vehicles.start_sending_reminders_at')],
                    ]);
            })
            ->where([
                ['vehicles.start_sending_reminders_at', '>=', Carbon::now()->subMinutes($this->getReminderMaxTimeInMinutes())],
                ['vehicles.start_sending_reminders_at', '<=', Carbon::now()->subMinutes($this->getReminderMinTimeInMinutes())],
                ['vehicle_reminders.id', '=', null],
            ]);

        $results = $query->get();

        $vehicleIds = [];

        foreach ($results as $result) {
            $vehicleIds[] = $result->vehicle_id;
        }

        return Vehicle::whereIn('id', $vehicleIds)->get();
    }

    protected function markReminderAsSent(Vehicle $vehicle)
    {
        VehicleReminder::create([
            'type' => $this->getReminderName(),
            'vehicle_id' => $vehicle->id,
            'start_date_of_sending_reminders' => $vehicle->start_sending_reminders_at
        ])->save();
    }

    protected abstract function getReminderName(): string;

    protected abstract function getReminderMinTimeInMinutes(): int;

    protected abstract function getReminderMaxTimeInMinutes(): int;

    protected abstract function legacyReminderHasBeenSend(Vehicle $vehicle): bool;

    protected abstract function sendReminderEmail(Vehicle $vehicle, Valuation $valuation);
}
