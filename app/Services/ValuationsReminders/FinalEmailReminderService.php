<?php

namespace JamJar\Services\ValuationsReminders;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use JamJar\Mail\ValuationReminderEmail;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;
use Illuminate\Support\Facades\Mail;
use JamJar\VehicleReminder;

class FinalEmailReminderService extends ReminderService
{
    protected function getReminderName(): string
    {
        return 'final_reminder';
    }

    protected function getReminderMinTimeInMinutes(): int
    {
        return 60 * 24 * 4;
    }

    protected function getReminderMaxTimeInMinutes(): int
    {
        return 60 * 24 * 5;
    }

    protected function sendReminderEmail(Vehicle $vehicle, Valuation $valuation)
    {
        $user = $vehicle->owner->user;

        Mail::to($user->email)->send(new ValuationReminderEmail($user, $valuation, $vehicle, 'final_reminder'));
    }

    protected function legacyReminderHasBeenSend(Vehicle $vehicle): bool
    {
        return (bool) Valuation::where([
            ['vehicle_id', '=', $vehicle->id],
            ['96h_email_reminder_sent', '=', true],
        ])->first();
    }
}
