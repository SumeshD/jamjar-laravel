<?php

namespace JamJar\Services;

use JamJar\Api\Responses\WWACAcceptedOfferResponse;
use JamJar\Vehicle;
use Ramsey\Uuid\UuidInterface;

class FakeValuationsService implements ValuationsServiceInterface
{
    /** @param array|WWACAcceptedOfferResponse[] $valuations */
    public function acceptWWACValuations(array $valuations): void
    {

    }
}