<?php

namespace JamJar\Services\ImagesManagement;

use JamJar\Model\UploadedImage;

class InMemoryImagesManager implements ImagesManagerInterface
{
    public $images = [];

    public function uploadImage(string $image, UploadedImage $uploadedImage)
    {
        $this->images[$uploadedImage->id()->toString()] = $image;
    }

    /**
     * @throws ImageNotFoundException
     * @param UploadedImage $uploadedImage
     * @return string
     */
    public function getImage(UploadedImage $uploadedImage)
    {
        if (!array_key_exists($uploadedImage->id()->toString(), $this->images)) {
            throw new ImageNotFoundException();
        }

        return $this->images[$uploadedImage->id()->toString()];
    }

    /**
     * @throws ImageNotFoundException
     * @param UploadedImage $uploadedImage
     * @return string
     */
    public function getThumbnail(UploadedImage $uploadedImage)
    {
        if (!array_key_exists($uploadedImage->id()->toString(), $this->images)) {
            throw new ImageNotFoundException();
        }

        return $this->images[$uploadedImage->id()->toString()];
    }
}
