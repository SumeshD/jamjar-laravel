<?php

namespace JamJar\Services\ImagesManagement;

use JamJar\Model\UploadedImage;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FileImagesManager implements ImagesManagerInterface
{
    public $images = [];

    public function uploadImage(string $image, UploadedImage $uploadedImage)
    {
        Storage::disk('local')->put('/images/' . $uploadedImage->id()->toString(), $image);

        $imageFromDisc = Storage::disk('local')->get('/images/' . $uploadedImage->id()->toString());

        $image = Image::make($imageFromDisc);
        $image->fit(120, 120);

        Storage::disk('local')->put('/images/' . $uploadedImage->id()->toString() . '_thumbnail', (string) $image->encode());
    }

    /**
     * @throws ImageNotFoundException
     * @param UploadedImage $uploadedImage
     * @return string
     */
    public function getImage(UploadedImage $uploadedImage)
    {
        $image = Storage::disk('local')->get('/images/' . $uploadedImage->id()->toString());

        if (!$image) {
            throw new ImageNotFoundException();
        }

        return $image;
    }

    /**
     * @throws ImageNotFoundException
     * @param UploadedImage $uploadedImage
     * @return string
     */
    public function getThumbnail(UploadedImage $uploadedImage)
    {
        $image = Storage::disk('local')->get('/images/' . $uploadedImage->id()->toString() . '_thumbnail');

        if (!$image) {
            throw new ImageNotFoundException();
        }

        return $image;
    }
}
