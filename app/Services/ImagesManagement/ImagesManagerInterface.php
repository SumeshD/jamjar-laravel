<?php

namespace JamJar\Services\ImagesManagement;

use JamJar\Model\UploadedImage;

interface ImagesManagerInterface
{
    public function uploadImage(string $image, UploadedImage $uploadedImage);

    /**
     * @throws ImageNotFoundException
     * @param UploadedImage $uploadedImage
     * @return string
     */
    public function getImage(UploadedImage $uploadedImage);

    /**
     * @throws ImageNotFoundException
     * @param UploadedImage $uploadedImage
     * @return string
     */
    public function getThumbnail(UploadedImage $uploadedImage);
}
