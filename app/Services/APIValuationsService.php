<?php

namespace JamJar\Services;

use Carbon\Carbon;
use JamJar\Api\Responses\WWACAcceptedOfferResponse;
use JamJar\Sale;
use JamJar\Traits\HandlesPartnerValuations;
use JamJar\Valuation;

class APIValuationsService implements ValuationsServiceInterface
{
    use HandlesPartnerValuations;

    private $processedValuationsCount = 0;

    /** @param array|WWACAcceptedOfferResponse[] $valuations */
    public function acceptWWACValuations(array $valuations): void
    {
        \Log::debug('accept WWAC valuations service----------------------------------------------------------------------------------------------');
        //for debugging purpose!
        $valuationsToAcceptByIdentifier = '';
        foreach ($valuations as $WWACValuation) {
            \Log::debug('WWAC valuation temp_ref: '. $WWACValuation->valuationIdentifier->toString());
            $valuationsToAcceptByIdentifier .= $WWACValuation->valuationIdentifier->toString().',';
            /** @var Valuation $valuation */
            $valuation = Valuation::where('temp_ref', '=', $WWACValuation->valuationIdentifier->toString())->first();

            if (!$valuation) {
                // try to find one more valuation by acceptance URI - for some reason temp_ref is not saved from time to time
                $valuation = Valuation::where('acceptance_uri', 'LIKE', '%id=' . $WWACValuation->valuationIdentifier->toString())->first();
            }

            if (!$valuation || $valuation->accepted_at != null) {
                continue;
            }

            $valuation->accepted_at = Carbon::now()->format('Y-m-d');
            $valuation->save();

            $sale = new Sale();
            $sale->valuation_id = $valuation->id;
            $sale->company_id = $valuation->company_id;
            $sale->user_id = $valuation->user_id;
            $sale->vehicle_id = $valuation->vehicle_id;
            $sale->final_price = $WWACValuation->valuationOffer;
            $sale->price = $WWACValuation->valuationOffer;
            $sale->vehicle_condition = 'great';
            $sale->delivery_method = 'dropoff';
            $sale->status = 'complete';
            $sale->dealer_notes = $WWACValuation->valuationIdentifier->toString();

            $sale->save();

            $this->processedValuationsCount++;
        }
        \Log::debug('Valuations to accept (WWAC) : '.$valuationsToAcceptByIdentifier);
        print_r('WWAC processed Valuations Count: '.$this->processedValuationsCount);
    }
}
