<?php

namespace JamJar\Services;

use GuzzleHttp\Client as HttpClient;

class LoqateValidatorService {

    const LOQATE_BASE_EMAIL_VALIDATION_URL = 'https://api.addressy.com/EmailValidation/Batch/Validate/v1.20/xmla.ws?';

    /** @var string $key */
    private $key;

    /** @var HttpClient */
    private $client;

    private $data = [];


    public function __construct()
    {
        $this->key = env('LOQATE_SERVICE_KEY');
        $this->client = new HttpClient([
            'base_uri' => self::LOQATE_BASE_EMAIL_VALIDATION_URL
        ]);
    }

    public function checkEmailAddress($email): bool {
        $url = self::LOQATE_BASE_EMAIL_VALIDATION_URL.'&Key='. urlencode($this->key) . '&Emails=' . urlencode($email);
        try {
            $this->makeCall($url);
        } catch (\Exception $e) {
            return false;
        }
        $json = json_encode($this->data);
        $responseArray = json_decode($json,TRUE);
        if($responseArray[0]['Status'][0] === 'Valid' || $responseArray[0]['Status'][0] === 'Valid_CatchAll' ||  $responseArray[0]['Status'][0] === 'Accept_All' || $responseArray[0]['Status'][0] === 'Unknown') {
            return true;
        }
        return false;
    }

    private function makeCall($url){
        $file = simplexml_load_file($url);

        if ($file->Columns->Column->attributes()->Name == "Error")
        {
            throw new Exception("[ID] " . $file->Rows->Row->attributes()->Error . " [DESCRIPTION] " . $file->Rows->Row->attributes()->Description . " [CAUSE] " . $file->Rows->Row->attributes()->Cause . " [RESOLUTION] " . $file->Rows->Row->attributes()->Resolution);
        }
        if ( !empty($file->Rows) )
        {
            foreach ($file->Rows->Row as $item)
            {
                $this->data[] = array('Status'=>$item->attributes()->Status,'EmailAddress'=>$item->attributes()->EmailAddress,'Account'=>$item->attributes()->Account,'Domain'=>$item->attributes()->Domain,'IsDisposible'=>$item->attributes()->IsDisposible,'IsSystemMailbox'=>$item->attributes()->IsSystemMailbox);
            }
        }
    }
}