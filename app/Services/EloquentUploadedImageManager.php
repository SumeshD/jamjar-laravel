<?php

namespace JamJar\Services;

use JamJar\Model\UploadedImage;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Illuminate\Database\Eloquent\Collection;

class EloquentUploadedImageManager
{
    /** @return UploadedImage[] */
    public function getAll($status = null, $orderBy = null, $orderDirection = null, $perPage = 20, $pageNum = 1): Collection
    {
        $result = UploadedImage::query();

        $orderBy = $orderBy ?: 'created_at';
        $orderDirection = $orderDirection ?: 'desc';

        if ($status) {
            $result->where('status', $status);
        }

        $result->limit($perPage);
        $result->offset(($pageNum-1) * $perPage);

        $result->orderBy($orderBy, $orderDirection)->get();

        return $result->get();
    }

    public function create(string $originalName, string $status) : UploadedImage
    {
        $uploadedImage = new UploadedImage();
        $uploadedImage->id = Uuid::uuid4();
        $uploadedImage->original_name = $originalName;
        $uploadedImage->status = $status;
        $uploadedImage->save();

        return $uploadedImage;
    }

    public function edit(
        UploadedImage $uploadedImage,
        string $originalName,
        string $status
    ) : UploadedImage
    {
        $uploadedImage->original_name = $originalName;
        $uploadedImage->status = $status;
        $uploadedImage->save();

        return $uploadedImage;
    }

    /** @throws UploadedImageNotFoundException */
    public function getById(UuidInterface $id) : UploadedImage
    {
        $uploadedImages = UploadedImage::query()
            ->where('id', $id)
            ->limit(1)
            ->get();

        if (count($uploadedImages) == 0) {
            throw new UploadedImageNotFoundException();
        }

        return $uploadedImages[0];
    }
}
