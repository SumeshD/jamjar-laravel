<?php

namespace JamJar\Services;

use JamJar\Defect;
use JamJar\Events\SaleHasBeenRemoved;
use JamJar\Events\ValuationHasBeenRemoved;
use JamJar\Sale;
use JamJar\UserFund;
use JamJar\UserStatement;
use JamJar\Valuation;
use JamJar\ValueObjects\VehicleMetaEdit;
use JamJar\ValueObjects\VehicleOwnerEdit;
use JamJar\Vehicle;
use JamJar\VehicleOwner;
use Ramsey\Uuid\Uuid;

class VehicleService
{
    public static function refundValuation(Valuation $valuation)
    {
        $fee = $valuation->fee;
        $funds = UserFund::where('user_id', '=', $valuation->company->user_id)->first();

        if (!$funds) {
            return;
        }

        $funds->funds = $funds->getOriginal('funds') + $fee;
        $funds->save();

        $statement = new UserStatement();
        $statement->user_id = $valuation->company->user_id;
        $statement->type = 'deposit';
        $statement->amount = $fee;
        $statement->save();
    }

    public function editVehicleMeta(Vehicle $vehicle, VehicleMetaEdit $vehicleMetaEdit)
    {
        $vehicle->meta->mileage = $vehicleMetaEdit->mileage;
        $vehicle->meta->number_of_owners = $vehicleMetaEdit->previousOwners;
        $vehicle->meta->service_history = $vehicleMetaEdit->serviceHistory;
        $vehicle->meta->mot = $vehicleMetaEdit->mot;
        $vehicle->meta->color = $vehicleMetaEdit->carColourId;
        $vehicle->meta->write_off = $vehicleMetaEdit->isWriteOff;

        if ($vehicleMetaEdit->isWriteOff) {
            $vehicle->meta->write_off_category = $vehicleMetaEdit->writeOffCategory;
        } else {
            $vehicle->meta->write_off_category = 'none';
        }

        if ($vehicleMetaEdit->isNonRunner && !Defect::areNonMechanicalDefectsOnly($vehicleMetaEdit->nonRunnerReasonIds)) {
            $vehicle->meta->non_runner = true;
            $reasons = [];
            foreach ($vehicleMetaEdit->nonRunnerReasonIds as $reasonId) {
                $reason = Defect::where('id', '=', $reasonId)->first();
                if (in_array($reason->title, Defect::getNonMechanicalReasons())) {
                    continue;
                }
                $reasons[] = [
                    'id' => $reason->id,
                    'title' => $reason->title,
                ];
            }
            $vehicle->meta->non_runner_reason = serialize($reasons);
        } else {
            $vehicle->meta->non_runner = false;
            $vehicle->meta->non_runner_reason = serialize([]);
        }

        $vehicle->meta->save();
    }

    public function editVehicleOwner(Vehicle $vehicle, VehicleOwnerEdit $vehicleOwnerEdit)
    {
        $owner = VehicleOwner::where('vehicle_id', '=', $vehicle->id)->first();

        $name = $this->formatName($vehicleOwnerEdit->name);

        $owner->firstname = $name['firstname'];
        $owner->lastname = $name['lastname'];
        $owner->telephone = $vehicleOwnerEdit->telephone;
        $owner->postcode = $vehicleOwnerEdit->postcode;

        $owner->save();
    }

    public function removeOldValuations(Vehicle $newVehicle, Vehicle $oldVehicle)
    {
        $oldUuid = null;
        /** @var Valuation $valuation */
        foreach (Valuation::getByVehicle($newVehicle) as $valuation) {
            $oldUuid = Uuid::fromString($valuation->uuid);
            $sale = Sale::where('valuation_id', '=', $valuation->id)->first();
            if ($sale) {
                if ($valuation->company->company_type == 'matrix' && $sale->status == 'pending') {
                    event(new SaleHasBeenRemoved($sale->company->user, $newVehicle->owner, $sale, $newVehicle, $oldVehicle, $valuation));
                    self::refundValuation($valuation);
                }

                $sale->delete();
                foreach (Sale::where('valuation_id', '=', $valuation->id)->get() as $redundantSale) {
                    $redundantSale->delete();
                }
            } elseif ($valuation->company->company_type == 'matrix') {
                if (!$valuation->isExpired()) {
                    self::refundValuation($valuation);
                }
            }
            session(['got-valuation-for-'.$oldVehicle->numberplate.'-from-company-'.$valuation->company->id => false]);

            $valuation->delete();
        }
    }

    private function formatName(string $name)
    {
        $fullname = explode(' ', $name);

        if (array_key_exists(1, $fullname)) {
            $firstname = $fullname[0];
            $lastname = $fullname[1];
        } else {
            $firstname = $name;
            $lastname = ' ';
        }

        return compact('firstname', 'lastname');
    }

}