<?php

namespace JamJar\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


class DistanceCalculatorService
{
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://maps.googleapis.com/',
        ]);
    }

    public function getDistanceBetweenLocationsInMeters(string $startPostcode, string $stopPostcode): ?int
    {
        if (substr($startPostcode, 0, 2) == substr($stopPostcode, 0, 2)) {
            return 0;
        }

        $response = $this->lookupLoop($startPostcode, $stopPostcode);

        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);

        if (
            isset($body['rows']) &&
            isset($body['rows'][0]) &&
            isset($body['rows'][0]['elements']) &&
            isset($body['rows'][0]['elements'][0]) &&
            isset($body['rows'][0]['elements'][0]['distance']) &&
            isset($body['rows'][0]['elements'][0]['distance']['value'])
        ) {
            return (int) $body['rows'][0]['elements'][0]['distance']['value'];
        }

        return null;
    }

    public function lookupLoop ($startPostcode, $stopPostcode)
    {
        $numTries = 0;

        try{

            for ($i=0;$i<3;$i++) {

                $numTries++;

                $response = $this->client->get(
                    sprintf(
                        'maps/api/distancematrix/json?origins=%s&destinations=%s&mode=driving&language=en-EN&sensor=false&key=%s',
                        $startPostcode,
                        $stopPostcode,
                        config('googlemaps.key')
                    )
                , ['http_errors' => false]);

                if ($response->getStatusCode() == 200) {

                    break;

                }

            }

            if ($numTries == 3) {

                \Log::error('Google Maps API distance search failed 3 times!');
            }

        } catch (RequestException $e) {

            \Log::error('Google Maps API distance search failed!');

        }

        return $response;

    }
}