<?php

namespace JamJar\Services;

use JamJar\Api\Responses\WWACAcceptedOfferResponse;
use JamJar\Valuation;
use JamJar\Vehicle;
use Ramsey\Uuid\UuidInterface;

interface ValuationsServiceInterface
{
    /** @param array|WWACAcceptedOfferResponse[] $valuations */
    public function acceptWWACValuations(array $valuations): void;
}