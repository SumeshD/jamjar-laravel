<?php

namespace JamJar\Services\Valuations;

use JamJar\Company;
use JamJar\Model\ValuationDraft;
use JamJar\PartnerPosition;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\VehiclePriceEstimatorInterface;
use JamJar\Services\Valuations\VehicleValuers\VehicleValuerInterface;
use JamJar\Vehicle;

class VehicleValuationsCollector
{
    /** @var array|VehiclePriceEstimatorInterface[] */
    private $vehiclePriceEstimators;

    /** @var array|VehicleValuerInterface[] */
    private $vehicleValuers;

    /**
     * @param array|VehiclePriceEstimatorInterface[] $vehiclePriceEstimators
     * @param array|VehicleValuerInterface[] $vehicleValuers
     */
    public function __construct(array $vehiclePriceEstimators, array $vehicleValuers)
    {
        $this->vehiclePriceEstimators = $vehiclePriceEstimators;
        $this->vehicleValuers = $vehicleValuers;
    }

    /**
     * @param Vehicle $vehicle
     * @return array|ValuationDraft[]
     */
    public function createValuationDrafts(Vehicle $vehicle): array
    {
        $estimatedPrices = [];

        foreach ($this->vehiclePriceEstimators as $estimator) {
            $estimatedPrices = array_merge($estimatedPrices, $estimator->getPriceEstimations($vehicle));
        }

        $valuationDrafts = [];

        foreach ($this->vehicleValuers as $valuer) {
            if (!$valuer->acceptsNonRunners() && $vehicle->isNonRunner()) {
                continue;
            }

            $valuerDrafts = $valuer->createValuationDrafts($estimatedPrices, $vehicle);

            $dataProvider = $valuer->getDataProvider();
            if ($dataProvider instanceof VehiclePriceEstimatorInterface) {
                $estimatedPrices = array_merge($estimatedPrices, $dataProvider->getPriceEstimations($vehicle));
            }

            $valuationDrafts = array_merge($valuationDrafts, $valuerDrafts);
        }

        $valuationDrafts = $this->sortDrafts($valuationDrafts);
        $valuationDrafts = $this->removeDraftsFromCompaniesWithInsufficientFounds($valuationDrafts);

        return $valuationDrafts;
    }

    public function removeAllDrafts(Vehicle $vehicle)
    {
        foreach (ValuationDraft::where('vehicle_id', '=', $vehicle->id)->get() as $draft) {
            if ($valuation = $draft->getValuation()) {
                if ($sale = $valuation->sale) {
                    $sale->delete();
                }
                $valuation->delete();
            }

            $draft->removeCompletely();
        }
    }

    /**
     * @param array|ValuationDraft[] $valuationDrafts
     */
    private function sortDrafts(array $valuationDrafts)
    {
        usort($valuationDrafts, function(ValuationDraft $draftA, ValuationDraft $draftB) {
            if ($draftA->getPriceInPence() == $draftB->getPriceInPence()) {
                return 0;
            }

            return $draftA->getPriceInPence() < $draftB->getPriceInPence() ? 1 : -1;
        });

        return $valuationDrafts;
    }

    /**
     * @param array|ValuationDraft[] $valuationDrafts
     * @return array|ValuationDraft[]
     */
    private function removeDraftsFromCompaniesWithInsufficientFounds(array $valuationDrafts): array
    {
        $position = 1;

        $draftsAfterFilter = [];

        foreach ($valuationDrafts as $valuationDraft) {
            if ($valuationDraft->getCompany()->company_type == 'api') {
                $draftsAfterFilter[] = $valuationDraft;
                $position++;
                continue;
            }

            $calculator = new PartnerFeeCalculator($position, $valuationDraft->getPriceInPence() / 100);
            $fee = $calculator->calculate(true, $valuationDraft->company);

            if ($fee > $valuationDraft->getCompany()->user->funds->getOriginal('funds')) {
                $valuationDraft->removeCompletely();
                continue;
            }

            $minimumListingPosition = $this->getMinimumListingPositionByDraft($valuationDraft);

            if ($minimumListingPosition &&  $minimumListingPosition < PartnerPosition::MAX_POSITION && $minimumListingPosition < $position) {
                $valuationDraft->removeCompletely();
                continue;
            }

            $draftsAfterFilter[] = $valuationDraft;
            $position++;
        }

        return $draftsAfterFilter;
    }

    private function getMinimumListingPositionByDraft(ValuationDraft $valuationDraft): int
    {
        $company = $valuationDraft->getCompany();

        if (!$company) {
            return 0;
        }

        $positionEntity = $company->getPosition();

        if (!$positionEntity) {
            return 0;
        }

        $minimumListingPosition = (int) $positionEntity->getPosition();

        return $minimumListingPosition;
    }
}
