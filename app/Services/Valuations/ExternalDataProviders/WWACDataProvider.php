<?php

namespace JamJar\Services\Valuations\ExternalDataProviders;

use Carbon\Carbon;
use JamJar\Api\Responses\WWACAcceptedOfferResponse;
use JamJar\Company;
use JamJar\Model\ValuationDraft;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Services\Valuations\VehiclePriceEstimators\VehiclePriceEstimatorInterface;
use JamJar\Valuation;
use Log;
use JamJar\Vehicle;
use JamJar\Api\ApiInterface;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;
use \Exception;

class WWACDataProvider implements ValuationDataProviderInterface, VehiclePriceEstimatorInterface
{
    const MINIMUM_ACCEPTABLE_PRICE = 1000;
    const MAXIMUM_ACCEPTABLE_PRICE = 9999999;
    const VALUATION_PURCHASED_STATUS = 3;
    const VALUATION_SOLD_STATUS = 4;
    const WWAC_COMPANY_NAME = 'WeWantAnyCar';

    public $environment;
    public $key;
    public $partnerIdentifier;

    private $request = '';

    private $response = '';

    /** @var int */
    private $estimatedPrice;

    public function __construct($environment = null, $key = null, $partnerIdentifier = null)
    {
        $this->environment = env('WWAC_ENV');
        $this->key = env('WWAC_ENV') == 'sandbox' ? env('WWAC_SANDBOX_API_KEY') : env('WWAC_LIVE_API_KEY');
        $this->partnerIdentifier = env('WWAC_ENV') == 'sandbox' ? env('WWAC_SANDBOX_PARTNER_IDENTIFIER') : env('WWAC_LIVE_PARTNER_IDENTIFIER');
    }

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationData[]
     */
    public function getValuationsData(array $priceEstimations, Vehicle $vehicle): array
    {
        if ((int) substr($vehicle->meta->plate_year, 0, 4) < (int) Carbon::now()->subYear(6)->format('Y') && $vehicle->meta->mot == 'Expired') {
            return [];
        }

        $base_uri = env('WWAC_ENV') == 'sandbox' ? 'https://api.wwac.co.uk/apiv4-sb/GuidePriceAPILookup/api' : 'https://api.wwac.co.uk/apiv4/GuidePriceAPILookup/api';

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . base64_encode($this->key),
                ]
            ]
        );

        $ValuationRequest = [
            'ColumnMappings' => [
                [
                    'Id' => 'VRM',
                    'Value' => $vehicle->numberplate,
                ],
                [
                    'Id' => 'Mileage',
                    'Value' => $vehicle->meta->getOriginal('mileage'),
                ],
                [
                    'Id' => 'DateFirstReg',
                    'Value' => $vehicle->meta->registration_date,
                ],
                [
                    'Id' => 'CustomerPostcode',
                    'Value' => $vehicle->latest_owner->postcode,
                ],
                [
                    'Id' => 'CustomerName',
                    'Value' => ($vehicle->latest_owner->firstname && $vehicle->latest_owner->lastname) ?
                        ($vehicle->latest_owner->firstname . ' ' . $vehicle->latest_owner->lastname) :
                        'John Smith',
                ],
                [
                    'Id' => 'CustomerEmail',
                    'Value' => $vehicle->latest_owner->email ? $vehicle->latest_owner->email : 'john-smith@example.com',
                ],
                [
                    'Id' => 'CustomerPhone',
                    'Value' => $vehicle->latest_owner->telephone ? $vehicle->latest_owner->telephone : '1234567890',
                ],
                [
                    'Id' => 'PartnerIdentifier',
                    'Value' =>  $this->partnerIdentifier,
                ],
                [
                    'Id' => 'ServiceHistory',
                    'Value' => $vehicle->meta->service_history,
                ],
                [
                    'Id' => 'VehicleCondition',
                    'Value' => 'Average',
                ],
                [
                    'Id' => 'WriteOff',
                    'Value' => $vehicle->meta->write_off ? 'True' : 'False',
                ],
                [
                    'Id' => 'PreviousOwners',
                    'Value' => (string)$vehicle->meta->number_of_owners,
                ],
                [
                    'Id' => 'Transmission',
                    'Value' => $vehicle->meta->transmission,
                ],
                [
                    'Id' => 'Mot',
                    'Value' => str_replace('+', '', $vehicle->meta->mot),
                ],
                [
                    'Id' => 'ExteriorColour',
                    'Value' => $vehicle->meta->colour->title,
                ]
            ]
        ];

        $this->request = json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES);

        try {
            // Get the valuation from WWAC.
            $response = $client->post(
                "{$base_uri}/Vehicle/GetValuation",
                [
                    'body' => $this->request,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Authorization' => 'Basic ' . base64_encode($this->key),
                    ]
                ]
            );

        } catch (\Exception $exception) {
            \Log::debug('WeWantAnyCar API Failure for numberplate: '.$vehicle->numberplate.' '. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                        ->notify(new ApiFailure('We Want Any Car'));
            // Return false so the application doesn't crash
            return [];
        }

        $this->response = $response->getBody()->getContents();
        $body = json_decode($this->response, true);

        \Log::debug('WWAC API Response for numberplate: '.$vehicle->numberplate.' '.print_r($body, true));

        $valuation = [];
        if (isset($body['ColumnMappings'])) {

            $DistanceRequest = [
                'ColumnMappings' => [
                    [
                        'Id' => 'CustomerPostcode',
                        'Value' => $vehicle->latest_owner->postcode
                    ]
                ]
            ];

            try {
                // Get the valuation from WWAC.
                $distanceResponse = $client->post(
                    "{$base_uri}/Vehicle/GetNearestBranches",
                    [
                        'body' => json_encode($DistanceRequest, JSON_UNESCAPED_SLASHES),
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Authorization' => 'Basic ' . base64_encode($this->key),
                        ]
                    ]
                );

                $distanceBody = json_decode($distanceResponse->getBody()->getContents(), true);

                $body['ColumnMappings'] = array_merge($distanceBody['ColumnMappings'][0]['Options'], $body['ColumnMappings']);
    
            } catch (\Exception $exception) {
                \Log::debug('WeWantAnyCar API Failure:'. $exception->getMessage());
                // Report the Exception here.
                Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                            ->notify(new ApiFailure('We Want Any Car'));
            }

            foreach ($body['ColumnMappings'] as $data) {
                $valuation[$data['Id']] = $data['Value'];
            }

            $valuationPrice = $valuation['GuidePrice'];

            if ($valuationPrice < self::MINIMUM_ACCEPTABLE_PRICE || $valuationPrice > self::MAXIMUM_ACCEPTABLE_PRICE) {
                return [];
            }

            $WWACCompany = Company::where('name', '=', self::WWAC_COMPANY_NAME)->first();

            $this->estimatedPrice = (int) ($valuationPrice * 100);

            try {
                $data = ValuationData::create(
                    $WWACCompany,
                    $this->estimatedPrice,
                    false,
                    true,
                    ValuationDataProviderInterface::DATA_SOURCE_EXTERNAL
                );
            } catch (Exception $e) {
                return [];
            }

            try {
                $dateValidUntil = Carbon::createFromFormat('d/m/Y', $valuation['DateValidUntil']);
                $valuationValidityInDays = (int) $dateValidUntil->diffInDays(Carbon::now());
            } catch (\Exception $e) {
                $valuationValidityInDays = 5;
            }

            $data->acceptanceUrl = $valuation['ValuationDeepLink'];
            $data->tempRef = $valuation['ValuationIdentifier'];
            $data->adminFeeInPence = ((int) ($valuation['AdminFee']) * 100);
            $data->valuationValidityInDays = $valuationValidityInDays;
            $data->distanceInMiles = (int) $valuation['DrivingDistanceMiles'] ? $valuation['DrivingDistanceMiles'] : null;
            $data->request = $this->request;
            $data->response = $this->response;

            return [$data];
        }

        return [];
    }

    /**
     * @param Vehicle $vehicle
     * @return array|PriceEstimation[]
     */
    public function getPriceEstimations(Vehicle $vehicle): array
    {
        if ($this->estimatedPrice) {
            return [PriceEstimation::create($this->estimatedPrice, PriceEstimation::TYPE_EXTERNAL)];
        }

        return [];
    }

    public static function getPriceEstimationsFromDraft(ValuationDraft $draft): VehiclePriceEstimatorInterface
    {
        $dataProvider = new self();
        $dataProvider->estimatedPrice = $draft->getPriceInPence();

        return $dataProvider;
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array|WWACAcceptedOfferResponse[]
     */
    public function getAcceptedValuations(Carbon $startDate, Carbon $endDate): array
    {
        $base_uri = env('WWAC_ENV') == 'sandbox' ? 'https://api.wwac.co.uk/apiv4-sb/GuidePriceAPILookup/api' : 'https://api.wwac.co.uk/apiv4/GuidePriceAPILookup/api';

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . base64_encode($this->key),
                ]
            ]
        );

        $soldRequest = $this->getSoldRequest($startDate, $endDate);
        $purchasedRequest = $this->getPurchasedRequest($startDate, $endDate);

        try {
            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode($this->key),
            ];

            $soldResponse = $client->post("{$base_uri}/report/GetValuationsByStatus", [
                'body' => json_encode($soldRequest, JSON_UNESCAPED_SLASHES),
                'headers' => $headers
            ]);

            $purchasedResponse = $client->post("{$base_uri}/report/GetValuationsByStatus", [
                'body' => json_encode($purchasedRequest, JSON_UNESCAPED_SLASHES),
                'headers' => $headers
            ]);
        } catch (\Exception $exception) {
            \Log::debug('WeWantAnyCar API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new ApiFailure('We Want Any Car'));
            // Return false so the application doesn't crash
            return [];
        }

        $soldRecords = WWACAcceptedOfferResponse::createFromJsonResponse($soldResponse->getBody()->getContents());
        $purchasedRecords = WWACAcceptedOfferResponse::createFromJsonResponse($purchasedResponse->getBody()->getContents());

        return array_merge($soldRecords, $purchasedRecords);
    }

    public function updateCustomerData(Valuation $valuation)
    {
        $vehicle = $valuation->getVehicle();

        $base_uri = env('WWAC_ENV') == 'sandbox' ? 'https://api.wwac.co.uk/apiv4-sb/GuidePriceAPILookup/api' : 'https://api.wwac.co.uk/apiv4/GuidePriceAPILookup/api';

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . base64_encode($this->key),
                ]
            ]
        );

        $ValuationRequest = [
            'ColumnMappings' => [
                [
                    'Id' => 'ValuationIdentifier',
                    'Value' => $valuation->temp_ref,
                ],
                [
                    'Id' => 'CustomerPostcode',
                    'Value' => $vehicle->latest_owner->postcode,
                ],
                [
                    'Id' => 'CustomerName',
                    'Value' => $vehicle->latest_owner->firstname . ' ' . $vehicle->latest_owner->lastname,
                ],
                [
                    'Id' => 'CustomerEmail',
                    'Value' => $vehicle->latest_owner->email,
                ],
                [
                    'Id' => 'CustomerPhone',
                    'Value' => $vehicle->latest_owner->telephone,
                ],
            ]
        ];

        $this->request = json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES);

        try {
            $response = $client->post(
                "{$base_uri}/Vehicle/UpdateCustomerDetails",
                [
                    'body' => $this->request,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Authorization' => 'Basic ' . base64_encode($this->key),
                    ]
                ]
            );

        } catch (\Exception $exception) {
            \Log::debug('WeWantAnyCar API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new ApiFailure('We Want Any Car'));
        }
    }

    public function acceptsNonRunners(): bool
    {
        return false;
    }

    private function getSoldRequest(Carbon $startDate, Carbon $endDate): array
    {
        return [
            'ColumnMappings' => [
                [
                    'Id' => 'ValuationStartDate',
                    'Value' => $startDate->format('d/m/Y'),
                ],
                [
                    'Id' => 'ValuationEndDate',
                    'Value' => $endDate->format('d/m/Y'),
                ],
                [
                    'Id' => 'ValuationStatus',
                    'Value' => self::VALUATION_SOLD_STATUS,
                ],
            ],
        ];
    }

    private function getPurchasedRequest(Carbon $startDate, Carbon $endDate)
    {
        return [
            'ColumnMappings' => [
                [
                    'Id' => 'ValuationStartDate',
                    'Value' => $startDate->format('d/m/Y'),
                ],
                [
                    'Id' => 'ValuationEndDate',
                    'Value' => $endDate->format('d/m/Y'),
                ],
                [
                    'Id' => 'ValuationStatus',
                    'Value' => self::VALUATION_PURCHASED_STATUS,
                ],
            ],
        ];
    }
}
