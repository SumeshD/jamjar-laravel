<?php

namespace JamJar\Services\Valuations\ExternalDataProviders;

use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Vehicle;

interface ValuationDataProviderInterface
{
    const DATA_SOURCE_EXTERNAL = 'external';

    const DATA_SOURCE_MATRIX_RULES = 'matrix_rules';

    const DATA_SOURCE_AUTO_PARTNER = 'auto_partner';

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationData[]
     */
    public function getValuationsData(array $priceEstimations, Vehicle $vehicle): array;

    public function acceptsNonRunners(): bool;
}
