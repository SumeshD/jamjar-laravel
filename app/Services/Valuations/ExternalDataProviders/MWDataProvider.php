<?php
namespace JamJar\Services\Valuations\ExternalDataProviders;

use Carbon\Carbon;
use JamJar\Company;
use JamJar\Model\ValuationDraft;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Services\Valuations\VehiclePriceEstimators\VehiclePriceEstimatorInterface;
use Log;
use JamJar\Vehicle;
use JamJar\Api\ApiInterface;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;
use \Exception;

class MWDataProvider implements ValuationDataProviderInterface, VehiclePriceEstimatorInterface
{
    const MINIMUM_ACCEPTABLE_PRICE = 1000;

    const MW_COMPANY_NAME = 'Motorwise';

    public $baseUrl;
    public $username;
    public $password;

    private $request = '';

    private $response = '';

    /** @var int */
    private $estimatedPrice;

    public function __construct($baseUrl = null, $username = null, $password = null)
    {
        $this->baseUrl = env('MW_ENV') == 'live' ?
            'https://api.motorwise.com' :
            'https://api.motorwise.com/staging';
        $this->username = env('MW_USERNAME');
        $this->password = env('MW_PASSWORD');
    }

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationData[]
     */
    public function getValuationsData(array $priceEstimations, Vehicle $vehicle): array
    {

        if ((int) substr($vehicle->meta->plate_year, 0, 4) > (int) Carbon::now()->subYear(12)->format('Y') && (bool) $vehicle->meta->non_runner == false && (bool) $vehicle->meta->write_off == false ) {
            return [];
        }

        // Build our client
        $client = new HttpClient([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode($this->username.':'.$this->password),
                'Accept-Encoding' => '*'
            ]
        ]);

        $formParams = [
            'vrm' => $vehicle->numberplate,
            'mileage' => $vehicle->meta->getOriginal('mileage'),
            'postcode' => $vehicle->latest_owner->postcode,
        ];

        // Get a Valuation Reference
        try {
            \Log::debug('MW API Call '. $this->baseUrl.'/generatequote' . 'params: vrm - '.$formParams['vrm'].' mileage - '.$formParams['mileage'].' postcode - '.$formParams['postcode']);
            $response = $client->post($this->baseUrl.'/generatequote', [
                'json' => $formParams,
                'timeout' => 10,
            ]);

            $this->request = json_encode($formParams);

        } catch (\Exception $exception) {
            \Log::debug('MW API Failure:'. $exception->getMessage());
            //print_r($exception->getMessage()); die;
            // Report the Exception here.
            //Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                //->notify(new ApiFailure('We Buy Cars Today'));
            // Return false so the application doesn't crash
            return [];
        }

        // Get the body
        $this->response = $response->getBody()->getContents();
        // Parse it
        $body = json_decode($this->response, true);

        \Log::debug('MW API Response:\n\n'.print_r($body, true));

        if ($body['status'] == 1) {
            $id = $body['returnData']['quoteId'];
            $valuationPrice = $body['returnData']['quotedValue'];

            $MWCompany = Company::where('name', '=', self::MW_COMPANY_NAME)->first();

            $this->estimatedPrice = (int) ($body['returnData']['quotedValue'] * 100);

            try {
                $data = ValuationData::create(
                    $MWCompany,
                    $this->estimatedPrice,
                    true,
                    false,
                    ValuationDataProviderInterface::DATA_SOURCE_EXTERNAL
                );
            } catch (Exception $e) {
                return [];
            }

            $data->tempRef = $id;
            $data->acceptanceUrl = $body['returnData']['quoteURL'];
            $data->request = $this->request;
            $data->response = $this->response;

            return [$data];
        }

        return [];
    }

    /**
     * @param Vehicle $vehicle
     * @return array|PriceEstimation[]
     */
    public function getPriceEstimations(Vehicle $vehicle): array
    {
        if ($this->estimatedPrice) {
            return [PriceEstimation::create($this->estimatedPrice, PriceEstimation::TYPE_EXTERNAL)];
        }

        return [];
    }

    public static function getPriceEstimationsFromDraft(ValuationDraft $draft): VehiclePriceEstimatorInterface
    {
        $dataProvider = new self();
        $dataProvider->estimatedPrice = $draft->getPriceInPence();

        return $dataProvider;
    }

    public function acceptsNonRunners(): bool
    {
        return true;
    }
}
