<?php
namespace JamJar\Services\Valuations\ExternalDataProviders;

use Carbon\Carbon;
use JamJar\Company;
use JamJar\Model\ValuationDraft;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Services\Valuations\VehiclePriceEstimators\VehiclePriceEstimatorInterface;
use Log;
use JamJar\Vehicle;
use JamJar\Api\ApiInterface;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;
use \Exception;

class WBCTDataProvider implements ValuationDataProviderInterface, VehiclePriceEstimatorInterface
{
    const MINIMUM_ACCEPTABLE_PRICE = 1000;

    const WBCT_COMPANY_NAME = 'WeBuyCarsToday';

    public $key;
    public $username;
    public $password;

    private $request = '';

    private $response = '';

    /** @var int */
    private $estimatedPrice;

    public function __construct($key = null, $username = null, $password = null)
    {
        $this->key = env('WBCT_ENV') == 'live' ?
            'live:3,_3VO=M!>2LpnAjHcShYI]V]|VzeuYom|FB<vIaeg<^7#PrR<w!{P+3ib42Gk$' :
            'dev:UT7tAiR]y7nChw7&?sdJ?n5hu{9GDhjW+x?+5d[_Bk?Zy!Yu`a/CrgxYKLC.q?!';
        $this->username = env('WBCT_USERNAME');
        $this->password = env('WBCT_PASSWORD');
    }

    public function getVehicle($vrm)
    {
        //
    }

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationData[]
     */
    public function getValuationsData(array $priceEstimations, Vehicle $vehicle): array
    {
        return [];

        if ((int) substr($vehicle->meta->plate_year, 0, 4) < (int) Carbon::now()->subYear(6)->format('Y') && $vehicle->meta->mot == 'Expired') {
            return [];
        }

        // Build our client
        $client = new HttpClient([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ]);

        $formParams = [
            'key' => $this->key,
            'username' => env('WBCT_USERNAME', 'JamJar.com'),
            'password' => env('WBCT_PASSWORD', 'jamjar1234'),
            'car_reg' => $vehicle->numberplate,
            'miles' => $vehicle->meta->getOriginal('mileage'),
            'postcode' => $vehicle->latest_owner->postcode,
        ];

        // Get a Valuation Reference
        try {
            $response = $client->post('https://www.webuycarstoday.co.uk/wbct-api/linear_services/get_quote', [
                'form_params' => $formParams,
                'timeout' => 3,
            ]);

            $this->request = json_encode($formParams);

        } catch (\Exception $exception) {
            \Log::debug('WeBuyCarsToday API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new ApiFailure('We Buy Cars Today'));
            // Return false so the application doesn't crash
            return [];
        }

        // Get the body
        $this->response = $response->getBody()->getContents();
        // Parse it
        $body = json_decode($this->response, true);

        \Log::debug('WBCT API Response:\n\n'.print_r($body, true));

        if ($body['status'] == 'SUCCESS') {
            $id = $body['data']['registration_id'];
            $valuationPrice = $body['data']['quoted_price']['price'];

            if ($valuationPrice < self::MINIMUM_ACCEPTABLE_PRICE) {
                return [];
            }

            $WBCTCompany = Company::where('name', '=', self::WBCT_COMPANY_NAME)->first();

            $this->estimatedPrice = (int) ($body['data']['quoted_price']['price'] * 100);

            try {
                $data = ValuationData::create(
                    $WBCTCompany,
                    $this->estimatedPrice,
                    true,
                    false,
                    ValuationDataProviderInterface::DATA_SOURCE_EXTERNAL
                );
            } catch (Exception $e) {
                return [];
            }

            $data->tempRef = $id;
            $data->adminFeeInPence = (int) ($body['data']['quoted_price']['appointment_fee'] * 100);
            $data->request = $this->request;
            $data->response = $this->response;

            return [$data];
        }

        return [];
    }

    /**
     * @param Vehicle $vehicle
     * @return array|PriceEstimation[]
     */
    public function getPriceEstimations(Vehicle $vehicle): array
    {
        if ($this->estimatedPrice) {
            return [PriceEstimation::create($this->estimatedPrice, PriceEstimation::TYPE_EXTERNAL)];
        }

        return [];
    }

    public static function getPriceEstimationsFromDraft(ValuationDraft $draft): VehiclePriceEstimatorInterface
    {
        $dataProvider = new self();
        $dataProvider->estimatedPrice = $draft->getPriceInPence();

        return $dataProvider;
    }

    public function acceptsNonRunners(): bool
    {
        return false;
    }
}
