<?php

namespace JamJar\Services\Valuations\ExternalDataProviders;

use JamJar\Company;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Vehicle;
use \Exception;

class FakeDataProvider implements ValuationDataProviderInterface
{
    /** @var Company */
    private $buyerCompany;

    public function __construct(Company $buyerCompany)
    {
        $this->buyerCompany = $buyerCompany;
    }

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationData[]
     */
    public function getValuationsData(array $priceEstimations, Vehicle $vehicle): array
    {
        try {
            $data = ValuationData::create(
                $this->buyerCompany,
                200000,
                true,
                false,
                ValuationDataProviderInterface::DATA_SOURCE_EXTERNAL
            );
        } catch (Exception $e) {
            return [];
        }

        $data->tempRef = 'XYZ';
        $data->acceptanceUrl = 'https://example.com/finalize/XYZ';
        $data->adminFeeInPence = 2000;

        return [$data];
    }

    public function acceptsNonRunners(): bool
    {
        return true;
    }
}
