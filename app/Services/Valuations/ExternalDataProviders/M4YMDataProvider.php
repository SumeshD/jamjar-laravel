<?php

namespace JamJar\Services\Valuations\ExternalDataProviders;

use Carbon\Carbon;
use JamJar\Company;
use JamJar\Model\ValuationDraft;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Services\Valuations\VehiclePriceEstimators\VehiclePriceEstimatorInterface;
use Log;
use JamJar\Vehicle;
use JamJar\Api\ApiInterface;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;
use \Exception;

class M4YMDataProvider implements ValuationDataProviderInterface, VehiclePriceEstimatorInterface
{
    const MAXIMUM_ACCEPTABLE_PRICE = 9999999;

    const M4YM_COMPANY_NAME = 'Money4yourMotors';

    public $environment;
    public $key;
    public $partnerIdentifier;

    private $request = '';

    private $response = '';

    /** @var int */
    private $estimatedPrice;

    public function __construct($environment = null, $key = null, $partnerIdentifier = null)
    {
        $this->environment = env('M4YMv2_ENV');
        $this->key = env('M4YMv2_API_KEY');
        $this->partnerIdentifier = env('M4YMv2_CLIENT_IDENTIFIER');
    }

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationData[]
     */
    public function getValuationsData(array $priceEstimations, Vehicle $vehicle): array
    {
        if ((int) substr($vehicle->meta->plate_year, 0, 4) < (int) Carbon::now()->subYear(6)->format('Y') && $vehicle->meta->mot == 'Expired') {
            return [];
        }

        $base_uri = env('M4YMv2_ENV') == 'sandbox' ? 'https://www.m4ym.com/affiliateapi-m4ym-sandbox/api' : 'https://www.m4ym.com/affiliateapi-m4ym-live/api';

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'ClientIdentifier' => $this->partnerIdentifier,
                    'ClientAPIKey' => $this->key
                ],
                'verify' => false
            ]
        );


        $ValuationRequest = [
            'ColumnMappings' => [
                [
                    'Id' => 'RegistrationPlate',
                    'Value' => $vehicle->numberplate
                ]
            ]
        ];
        try {
            // dd(json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES), $this->partnerIdentifier, $this->key);
            // Get the valuation from M4YM.
            $response = $client->post(
                "{$base_uri}/Valuation/GetVehicle",
                [
                    'body' => json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES),
                    'headers' => [
                        'ClientIdentifier' => $this->partnerIdentifier,
                        'ClientAPIKey' => $this->key,
                        'Content-Type' => 'application/json'
                    ]
                ]
            );

        } catch (\Exception $exception) {
            \Log::debug('M4YM Version 2 API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                        ->notify(new ApiFailure('M4YM Version 2 failure'));
            // Return false so the application doesn't crash
            return [];
        }

        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);

        if (isset($body['ColumnMappings'])) {
            foreach ($body['ColumnMappings'] as $data) {
                if($data['Id'] == 'TempReference'){
                    $tempRef = $data['Value'];
                }
            }
            if(isset($tempRef) && !empty($tempRef)){

                $ValuationRequest = [
                    'ColumnMappings' => [
                        [
                            'Id' => 'TempReference',
                            'Value' => $tempRef,
                        ],
                        [
                            'Id' => 'CustomerName',
                            'Value' => $vehicle->latest_owner->name,
                        ],
                        [
                            'Id' => 'CustomerEmail',
                            'Value' => $vehicle->latest_owner->email,
                        ],
                        [
                            'Id' => 'CustomerPhone',
                            'Value' => $vehicle->latest_owner->telephone,
                        ],
                        [
                            'Id' => 'CustomerPostcode',
                            'Value' => $vehicle->latest_owner->postcode,
                        ],
                        [
                            'Id' => 'PartnerIdentifier',
                            'Value' =>  $this->partnerIdentifier,
                        ],
                        [
                            'Id' => 'Mileage',
                            'Value' => $vehicle->meta->getOriginal('mileage'),
                        ],
                        [
                            'Id' => 'NumberOfPreviousOwners',
                            'Value' => (string)$vehicle->meta->number_of_owners,
                        ]
                    ]
                ];

                $this->request = json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES);

                try {
                    // Trigger offer acceptance from M4YM.
                    $acceptResponse = $client->post(
                        "{$base_uri}/Valuation/GetMyValuation",
                        [
                            'body' => $this->request,
                            'headers' => [
                                'ClientIdentifier' => $this->partnerIdentifier,
                                'ClientAPIKey' => $this->key,
                                'Content-Type' => 'application/json'
                            ]
                        ]
                    );

                    $this->response = $acceptResponse->getBody()->getContents();
                    $acceptResponse = json_decode($this->response, true);

                    //Make response into nicer array
                    $responseData = [];
                    if(isset($acceptResponse['ColumnMappings']) && !empty($acceptResponse['ColumnMappings'])){
                        foreach($acceptResponse['ColumnMappings'] as $data){
                            $responseData[$data['Id']] = $data['Value'];
                        }
                    }


                } catch (\Exception $exception) {
                    \Log::debug('M4YM Version 2 API Failure for numberplate : '.$vehicle->numberplate.' '. $exception->getMessage());
                    // Report the Exception here.
                    Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                                ->notify(new ApiFailure('M4YM Version 2 failure'));
                    // Return false so the application doesn't crash
                    return [];
                }
            }
            
            if(isset($responseData)){
                \Log::debug('M4YM Version 2 API Response for numberplate: '.$vehicle->numberplate.' '. print_r($responseData, true));
            } else {
                return [];
            }



            if($responseData['ManualValuationRequired'] == 1){
                return [];
            } else {
                if ($responseData['VehicleValuation'] > self::MAXIMUM_ACCEPTABLE_PRICE) {
                    return [];
                }

                $M4YMCompany = Company::where('name', '=', self::M4YM_COMPANY_NAME)->first();

                $this->estimatedPrice = (int) ($responseData['VehicleValuation'] * 100);

                try {
                    $data = ValuationData::create(
                        $M4YMCompany,
                        $this->estimatedPrice,
                        true,
                        false,
                        ValuationDataProviderInterface::DATA_SOURCE_EXTERNAL
                    );
                } catch (Exception $e) {
                    return [];
                }

                $data->tempRef = (string) $responseData['TempReference'];
                $data->adminFeeInPence = ((int) $responseData['M4YMAdminFee_short'] * 100);
                $data->request = $this->request;
                $data->response = $this->response;

                return [$data];
            }
        }

        return [];
    }

    /**
     * @param Vehicle $vehicle
     * @return array|PriceEstimation[]
     */
    public function getPriceEstimations(Vehicle $vehicle): array
    {
        if ($this->estimatedPrice) {
            return [PriceEstimation::create($this->estimatedPrice, PriceEstimation::TYPE_EXTERNAL)];
        }

        return [];
    }

    public static function getPriceEstimationsFromDraft(ValuationDraft $draft): VehiclePriceEstimatorInterface
    {
        $dataProvider = new self();
        $dataProvider->estimatedPrice = $draft->getPriceInPence();

        return $dataProvider;
    }

    public function acceptsNonRunners(): bool
    {
        return false;
    }
}
