<?php

namespace JamJar\Services\Valuations\ExternalDataProviders;

use JamJar\Company;
use JamJar\Model\ValuationDraft;
use JamJar\Profile;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Services\Valuations\VehiclePriceEstimators\VehiclePriceEstimatorInterface;
use JamJar\VehicleOwner;
use Log;
use JamJar\Vehicle;
use Spatie\ArrayToXml\ArrayToXml;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;
use \Exception;
use Carbon\Carbon;

class CTBDataProvider implements ValuationDataProviderInterface, VehiclePriceEstimatorInterface
{
    const FAKE_NUMBERPLATE = 'ABC1234';

    const CTB_COMPANY_NAME = 'CarTakeBack';

    protected $subscriberID;
    protected $password;
    protected $baseUrl;
    protected $client;

    /** @var string */
    private $request = '';

    /** @var string */
    private $response = '';

    /** @var array|int[] */
    private $estimatedPrices = [];

    public function __construct($subscriberID = null, $password = null)
    {
        $this->subscriberID = empty($subscriberID) ? env('CARTAKEBACK_API_USER') : $subscriberID;
        $this->password = empty($password) ? env('CARTAKEBACK_API_KEY') : $password;
        $this->baseUrl = (env('CARTAKEBACK_ENV') == 'sandbox' ? 'https://dev.cartakeback.com/' : 'https://fads.cartakeback.com/');

        $this->client = new HttpClient([
            'base_uri' => $this->baseUrl,
        ]);
    }

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationData[]
     */
    public function getValuationsData(array $priceEstimations, Vehicle $vehicle): array
    {
        if ((int) substr($vehicle->meta->plate_year, 0, 4) > (int) Carbon::now()->subYear(9)->format('Y') && (bool) $vehicle->meta->non_runner == false && (bool) $vehicle->meta->write_off == false ) {
            return [];
        }

        try {
            $response = $this->getValuationsResponse($vehicle);
        } catch (\Exception $e) {
            return [];
        }

        $body = $response->getBody()->getContents();

        // CTB has his own validation for numberplate, in this case we shoot again with fake one
        if ($this->isProblemWithNumberplate($body)) {
            try {
                $response = $this->getValuationsResponse($vehicle, true);
                $body = $response->getBody()->getContents();
            } catch (\Exception $e) {
                return [];
            }
        }

        $jsonError = json_decode($body, true);

        if (json_last_error() === JSON_ERROR_NONE) {
            $this->logError($jsonError, $vehicle);
            return [];
        }

        $this->response = (string)$body;

        $body = simplexml_load_string($this->response);

        $results = (array)$body->valuations;

        $CTBCompany = Company::where('name', '=', self::CTB_COMPANY_NAME)->first();

        if (!$CTBCompany) {
            return [];
        }

        $valuationsData = [];

        foreach ($results as $value) {
            $hasCollectionValuation = isset($value[0]);
            $hasDropOffValuation = isset($value[1]);

            $collectionAmount = $hasCollectionValuation == true ? (int) $value[0]->amount : 0;
            $dropOffAmount = $hasDropOffValuation == true ? (int) $value[1]->amount : 0;

            $price = (int) (max($collectionAmount, $dropOffAmount) * 100);

            $this->estimatedPrices[] = [
                'highest' => $price,
                'collection' => $collectionAmount * 100,
                'dropOff' => $dropOffAmount * 100,
            ];

            try {
                $data = ValuationData::create(
                    $CTBCompany,
                    $price,
                    $hasCollectionValuation,
                    $hasDropOffValuation,
                    ValuationDataProviderInterface::DATA_SOURCE_EXTERNAL
                );
            } catch (Exception $e) {
                continue;
            }

            $data->distanceInMiles = $hasDropOffValuation == true ? (int) $value[1]->servicesOffered->distanceToNearestDriveInCentre : null;
            $data->tempRef = (string) $body->identification->partnervehicleref;
            $data->acceptanceUrl = (string) $body->retrievalUrl;
            $data->request = $this->request;
            $data->response = $this->response;
            $data->paymentMethod = 'cheque';

            if ($collectionAmount > $dropOffAmount) {
                $data->dropOffFeeInPence = (int) (($collectionAmount - $dropOffAmount) * 100);
            }

            if ($collectionAmount < $dropOffAmount) {
                $data->collectionFeeInPence = (int) (($dropOffAmount - $collectionAmount) * 100);
            }

            $valuationsData[] = $data;
        }

        return $valuationsData;
    }

    public function tryToUpdateCustomerData(string $partnerVehicleRef, $vehicle)
    {
        try {
            $requestXml = $this->buildCustomerDataUpdate($vehicle, $partnerVehicleRef);

            $this->client->post(
                'api/enquiry/customerdataupdate',
                [
                    'body' => $requestXml,
                    'headers' => [
                        'X-API-USER' => $this->subscriberID,
                        'X-API-KEY' => $this->password
                    ]
                ]
            );
        } catch (\Exception $exception) {
            \Log::debug('CarTakeBack API (update customer only) Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new ApiFailure('CarTakeBack'));
        }
    }

    /**
     * @param Vehicle $vehicle
     * @return array|PriceEstimation[]
     */
    public function getPriceEstimations(Vehicle $vehicle): array
    {
        $estimations = [];

        foreach ($this->estimatedPrices as $estimatedPrice) {
            $estimation = PriceEstimation::create($estimatedPrice['highest'], PriceEstimation::TYPE_EXTERNAL);
            $estimation->providerName = PriceEstimation::PROVIDER_NAME_CTB;
            $estimation->collectionValueInPence = $estimatedPrice['collection'];
            $estimation->dropOffValueInPence = $estimatedPrice['dropOff'];
            $estimations[] = $estimation;
        }

        return $estimations;
    }

    public static function getPriceEstimationsFromDraft(ValuationDraft $draft): VehiclePriceEstimatorInterface
    {
        $dataProvider = new self();
        $dataProvider->estimatedPrices[] = [
            'highest' => $draft->getPriceInPence(),
            'collection' => $draft->getPriceInPence() - $draft->getCollectionFeeInPence(),
            'dropOff' => $draft->getPriceInPence() - $draft->getDropOffFeeInPence(),
        ];

        return $dataProvider;
    }

    public function acceptsNonRunners(): bool
    {
        return true;
    }

    public static function convertVehicleServiceHistoryToCTBVehicleHistory(string $internalServiceHistory): string
    {
        if ($internalServiceHistory == 'Full Franchise History') {
            return 'Full';
        }

        return $internalServiceHistory == 'None' ? 'None' : 'Partial';
    }

    public static function convertVehicleMotToCTBMot(string $internalMot): string
    {
        if($internalMot == 'Expired') {
            return 'Expired';
        }

        return in_array($internalMot, ['6 Months+', '3-6 Months']) ? 'MoreThan3Months' : 'LessThan3Months';
    }

    protected function buildNewEnquiryRequest(Vehicle $vehicle, $useFakeNumberplate = false)
    {
        $hasReasons = false;
        if($vehicle->meta->non_runner_reason) {
            $hasReasons = true;
            $reasons = array_column(json_decode($vehicle->meta->non_runner_reason),'title');
        }

        $serviceHistory = $vehicle->meta->service_history ? self::convertVehicleServiceHistoryToCTBVehicleHistory($vehicle->meta->service_history) : null;
        $mot = $vehicle->meta->mot ? self::convertVehicleMotToCTBMot($vehicle->meta->mot) : null;

        $vehicleRequest = [
            'Identification' => [
                'VehicleID' => $vehicle->id,
                'RequestID' => $vehicle->id,
                'VRM' => $useFakeNumberplate ? self::FAKE_NUMBERPLATE : $vehicle->numberplate,
                'Ref' => 'Jamjar-U'.$vehicle->latest_owner->user_id.'-V'.$vehicle->id,
                'PartnerVehicleRef' => '',
                'PartnerValuationRef' => ''
            ],
            'VehicleData' => [
                'RegNoWarning' => 0,
                'Age' => $vehicle->meta->age,
                'CustomData' => [
                    'MotRemaining' => $mot,
                    'ServiceHistory' => $serviceHistory,
                    'CurrentMileage' => $vehicle->meta->mileage,
                    'PostCode' => $vehicle->latest_owner->postcode,
                    'NonRunner' => ($vehicle->meta->non_runner == 1) ? 'true' : 'false',
                    'WrittenOff' => ($vehicle->meta->write_off == 1) ? 'true' : 'false',
                    'NonRunnerReason' => [
                        'AccidentDamage' => $hasReasons ? (in_array('Accident Damage', $reasons) == true) ? 'true' : 'false' : null,
                        'Alternator' => $hasReasons ? (in_array('Alternator', $reasons) == true) ? 'true' : 'false' : null,
                        'Battery' => $hasReasons ? (in_array('Battery', $reasons) == true) ? 'true' : 'false' : null,
                        'Brakes' => $hasReasons ? (in_array('Brakes', $reasons) == true) ? 'true' : 'false' : null,
                        'CamBelt' =>  $hasReasons ? (in_array('Cam Belt', $reasons) == true) ? 'true' : 'false' : null,
                        'CamShaft' => $hasReasons ? (in_array('Cam Shaft', $reasons) == true) ? 'true' : 'false' : null,
                        'Clutch' => $hasReasons ? (in_array('Clutch', $reasons) == true) ? 'true' : 'false' : null,
                        'Corrosion' => $hasReasons ? (in_array('Corrosion/Excessive Rust', $reasons) == true) ? 'true' : 'false' : null,
                        'DriveShaft' => $hasReasons ? (in_array('Driveshaft', $reasons) == true) ? 'true' : 'false' : null,
                        'Electrical' => $hasReasons ? (in_array('Electrical', $reasons) == true) ? 'true' : 'false' : null,
                        'EngineSeized' => $hasReasons ? (in_array('Engine Seized', $reasons) == true) ? 'true' : 'false' : null,
                        'GearBox' => $hasReasons ? (in_array('Gearbox', $reasons) == true) ? 'true' : 'false' : null,
                        'HeadGasket' => $hasReasons ? (in_array('Head Gasket', $reasons) == true) ? 'true' : 'false' : null,
                        'MissingEngine' => null,
                        'MissingGearBox' => null,
                        'NoValidInsurance' => null,
                        'NoValidRoadTax' => null,
                        'PowerSteering' => $hasReasons ? (in_array('Power Steering', $reasons) == true) ? 'true' : 'false' : null,
                        'Steering' => $hasReasons ? (in_array('Steering', $reasons) == true) ? 'true' : 'false' : null,
                        'Suspension' => $hasReasons ? (in_array('Suspension', $reasons) == true) ? 'true' : 'false' : null,
                        'TimingChain' => $hasReasons ? (in_array('Timing Chain', $reasons) == true) ? 'true' : 'false' : null,
                        'Turbo' => $hasReasons ? (in_array('Turbo', $reasons) == true) ? 'true' : 'false' : null,
                        'Tyres' => $hasReasons ? (in_array('Tyres', $reasons) == true) ? 'true' : 'false' : null,
                    ],
                ],
                'CapData' => [
                    'CapID' => $vehicle->cap_id,
                    'Manufacturer' => $vehicle->meta->manufacturer,
                    'Model' => $vehicle->meta->model,
                    'VehicleType' => strtoupper($vehicle->type)
                ]
            ]
        ];

        $xml = ArrayToXml::convert($vehicleRequest, 'Request');

        Log::info($xml);
        $this->request = (string) $xml;

        return $xml;
    }


    protected function buildCustomerDataUpdate(Vehicle $vehicle, string $partnerVehicleRef)
    {
        $profile = Profile::where('user_id', '=', $vehicle->latest_owner->user_id)->first();

        /** @var VehicleOwner $vehicleOwner */
        $vehicleOwner = $vehicle->latest_owner;

        $phoneNumber = $vehicleOwner->getPhoneForCarTakeBack();

        $customerRequest = [
            'Identification' => [
                'VehicleID' => $vehicle->id,
                'RequestID' => $vehicle->id,
                'VRM' => $vehicle->numberplate,
                'Ref' => 'Jamjar-U'.$vehicle->latest_owner->user_id.'-V'.$vehicle->id,
                'PartnerVehicleRef' => $partnerVehicleRef,
                'PartnerValuationRef' => null,
            ],
            'CustomerData' => [
                'DaytimeTelNumber' => $phoneNumber,
                'EmailAddress' => $vehicle->latest_owner->email,
                'FullName' => $vehicle->latest_owner->firstname . ' ' . $vehicle->latest_owner->lastname,
                'FirstName' => $vehicle->latest_owner->firstname,
                'LastName' => $vehicle->latest_owner->lastname,
                'PostCode' => $vehicle->latest_owner->postcode,
                'AddressCity' => $profile->city,
                'AddressLine1' => $profile->address_line_one,
                'AddressLine2' => $profile->address_line_two,
                'AddressLine3' => null,
            ]
        ];

        $xml = ArrayToXml::convert($customerRequest, 'CustomerDataUpdate');

        return $xml;
    }

    /**
     * @param Vehicle $vehicle
     * @param bool $useFakeNumberplate
     * @return \Psr\Http\Message\ResponseInterface
     * @throws Exception
     */
    private function getValuationsResponse(Vehicle $vehicle, bool $useFakeNumberplate = false)
    {
        $requestXml = $this->buildNewEnquiryRequest($vehicle, $useFakeNumberplate);

        try {
            $response = $this->client->post('api/enquiry/newenquiry', [
                'body' => $requestXml,
                'headers' => [ 'X-API-USER' => $this->subscriberID,  'X-API-KEY' => $this->password ]
            ]);
        } catch (\Exception $exception) {
            \Log::debug('CarTakeBack API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new ApiFailure('CarTakeBack'));
            // Return false so the application doesn't crash

            throw new \Exception();
        }

        return $response;
    }

    private function logError($jsonError, $vehicle)
    {
        Log::error(
            $jsonError['identification']['partnervehicleref'],
            [
                'vehicle' => [
                    'id' => $vehicle->id,
                    'vrm' => $vehicle->numberplate,
                    'meta' => [
                        'manufacturer' => $vehicle->meta->manufacturer,
                        'model' => $vehicle->meta->model,
                        'derivative' => $vehicle->meta->derivative,
                        'plate_year' => $vehicle->meta->plate_year,
                        'mileage' => $vehicle->meta->mileage,
                    ]
                ]
            ]
        );
    }

    private function isProblemWithNumberplate($body)
    {
        $jsonError = json_decode($body, true);

        return
            $jsonError['identification'] &&
            $jsonError['identification']['partnervehicleref'] &&
            $jsonError['identification']['partnervehicleref'] == 'Error: VRM format incorrect';
    }
}
