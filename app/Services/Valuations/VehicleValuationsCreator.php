<?php

namespace JamJar\Services\Valuations;

use Illuminate\Database\Eloquent\Collection;
use JamJar\Api\CapApi;
use JamJar\Jobs\SendAutomaticBiddingRuleMatchedEmail;
use JamJar\Jobs\SendNotEnoughFundsEmail;
use JamJar\Model\ValuationDraft;
use JamJar\Model\ValuationDraftPrice;
use JamJar\PartnerLocation;
use JamJar\PartnerPosition;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\VehiclePriceEstimatorInterface;
use JamJar\Services\Valuations\VehicleValuers\VehicleValuerInterface;
use JamJar\Setting;
use JamJar\Valuation;
use JamJar\Vehicle;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;
use Ramsey\Uuid\UuidInterface;

class VehicleValuationsCreator
{
    public function createValuationFromDraft(ValuationDraft $draft, UuidInterface $valuationsUuid, int $position = 1, array $capValuation = null)
    {
        // Sort out the Type
        $type = strtolower($draft->getVehicle()->type) === 'car' ? 'CAR' : 'LCV';

        // Get the Manufacturer
        $manufacturer = VehicleManufacturer::where('name', $draft->getVehicle()->meta->manufacturer)->where('vehicle_type', $type)->first();

        if ($draft->getVehicle()->meta->cap_model_id) {
            $model = VehicleModel::where('cap_model_id', $draft->getVehicle()->meta->cap_model_id)->first();
        } else {
            $model = VehicleModel::where('name', $draft->getVehicle()->meta->model)->first();
        }

        if ($model) {
            $derivative = VehicleDerivative::where('name', $draft->getVehicle()->meta->derivative)
                ->where('model_id', $model->cap_model_id)
                ->first();
        } else {
            // probably vehicle is quite new and we don't have the derivative at this point, TODO - search for this new one
            $derivative = null;
        }

        $collectionValue = null;

        if ($draft->isCollectionAvailable()) {
            $collectionValue = (int) ($draft->price_in_pence / 100);
            if ($draft->getBuyerCompany()->company_type == 'api') {
                $collectionValue -= (int) ($draft->getCollectionFeeInPence() / 100);
            }
        }

        $dropOffValue = null;

        if ($draft->isDropOffAvailable()) {
            $dropOffValue = (int) ($draft->price_in_pence / 100);
            if ($draft->getBuyerCompany()->company_type == 'api') {
                $dropOffValue -= (int) ($draft->getDropOffFeeInPence() / 100);
            }
        }


        $location_id = $draft->buyer_location_id;

        $checkLocationDeleted = PartnerLocation::where('id',$draft->buyer_location_id)->first();
        if($checkLocationDeleted->is_active==0){

            $location_id =  PartnerLocation::where('company_id',$draft->getCompany()->id)->where('is_active',1)->first()->id;
        }


        $valuation = new Valuation([
            'vehicle_id' => $draft->getVehicle()->id,
            'manufacturer_id' => $manufacturer ? $manufacturer->cap_manufacturer_id : null,
            'model_id' => $draft->getVehicle()->meta->cap_model_id ?? 0,
            'derivative_id' => $derivative ? $derivative->cap_derivative_id : 0,
            'company_id' => $draft->getCompany()->id,
            'user_id' => $draft->getVehicle()->owner->user_id,
            'collection_value' => $collectionValue,
            'dropoff_value' => $dropOffValue,
            'cap_value' => $capValuation ? (int) $capValuation['average'] : 0,
            'cap_value_retail' => $capValuation ? (int) $capValuation['retail'] : 0,
            'cap_value_clean' => $capValuation ? (int) $capValuation['clean'] : 0,
            'cap_value_below' => $capValuation ? (int) $capValuation['below'] : 0,
            'position' => $position,
            'fee' => 0,
            'partner_type' => $draft->getCompany()->type,
            'uuid' => $valuationsUuid,
            'admin_fee' => (int) ($draft->getAdminFeeInPence() / 100),
            'payment_method' => $draft->getPaymentMethod(),
            'acceptance_uri' => $draft->acceptance_uri,
            'temp_ref' => $draft->temp_ref,
            'distance' => $draft->distance_to_seller_in_miles,
            'vehicle_condition_value_good' => $draft->getPriceForConditionInPence(OfferedValueInterface::VEHICLE_CONDITION_GOOD) / 100,
            'vehicle_condition_value_fair' => $draft->getPriceForConditionInPence(OfferedValueInterface::VEHICLE_CONDITION_FAIR) / 100,
            'vehicle_condition_value_poor' => $draft->getPriceForConditionInPence(OfferedValueInterface::VEHICLE_CONDITION_POOR) / 100,
            'location_id' =>  $location_id,
        ]);

        $valuation->save();

        $draft->valuation_id = $valuation->id;
        $draft->save();

        return $valuation;
    }

    /**
     * @param Vehicle $vehicle
     * @param Uuid $uuid
     * @return array|Valuation[]
     */
    public function collectFeeForValuation(Vehicle $vehicle)
    {
        /** @var Collection|Valuation[] $valuations */
        $valuations = $vehicle->getValuations()->sortByDesc(function (Valuation $valuation) {
            return $valuation->getPriceInPence();
        });

        $position = 1;


        foreach ($valuations as $valuation) {
            if ($valuation->company->company_type == 'api') {
                $position++;
                continue;
            }

            $valuationCompany = $valuation->company;
            $usesFlatFee = false;
            $flatFee = 0;

            if( $valuationCompany ) {
                if( $valuationCompany->user ) {
                    if( $valuationCompany->user->profile ) {

                        if($valuationCompany->user->profile->use_flat_fee == 1 && $valuationCompany->user->profile->flat_fee != null) {

                            $usesFlatFee = true;
                            $flatFee = $valuationCompany->user->profile->flat_fee;

                        }

                    }

                }

            }

            if($usesFlatFee) {

                $feeInPence = ($flatFee * 100)+(($flatFee*100)*env('VAT_FEE_IN_PERCENT')/100);

            } else {

                $calculator = new PartnerFeeCalculator($position, $valuation->getPriceInPence() / 100);
                $feeInPence = $calculator->calculate(true, $valuationCompany);

            }

            $companyPosition = PartnerPosition::where('company_id','=',$valuationCompany->id)->where('user_id','=',$valuationCompany->user_id)->first();

            if($companyPosition && $companyPosition->position) {

                if($position<=$companyPosition->position || $companyPosition->position==6) {

                    if($feeInPence<=$valuationCompany->user->funds->getOriginal('funds')) {

                        if($valuationCompany) {

                            PartnerFeeCalculator::createStatementEntry($valuation, $valuationCompany, $valuation->getVehicle(), $feeInPence);
                            $valuationCompany->user->funds->spendFunds($feeInPence);
                            if((bool) Setting::get('sendAutomaticBiddingRuleMatchEmail') && $valuationCompany->user->isMatrixPartner()){
                                SendAutomaticBiddingRuleMatchedEmail::dispatch($valuationCompany->user, $valuation, $valuation->getVehicle(), $position);
                            }
                        }

                        $valuation->fee = $feeInPence;
                        $valuation->is_created_by_automated_bids = true;
                        $valuation->position = $position;
                        $valuation->save();

                    }else {
                        SendNotEnoughFundsEmail::dispatch($valuationCompany->user, $vehicle, $feeInPence, $valuationCompany->user->funds->getOriginal('funds'));
                    }

                } else {

                    $position++;
                    continue;
                }

            } else {

                if($feeInPence<=$valuationCompany->user->funds->getOriginal('funds')) {

                    if($valuationCompany) {

                        PartnerFeeCalculator::createStatementEntry($valuation, $valuationCompany, $valuation->getVehicle(), $feeInPence);
                        $valuationCompany->user->funds->spendFunds($feeInPence);
                        if((bool) Setting::get('sendAutomaticBiddingRuleMatchEmail') && $valuationCompany->user->isMatrixPartner()){
                            SendAutomaticBiddingRuleMatchedEmail::dispatch($valuationCompany->user, $valuation, $valuation->getVehicle(), $position);
                        }
                    }

                    $valuation->fee = $feeInPence;
                    $valuation->is_created_by_automated_bids = true;
                    $valuation->position = $position;
                    $valuation->save();

                }else {
                    SendNotEnoughFundsEmail::dispatch($valuationCompany->user, $vehicle, $feeInPence, $valuationCompany->user->funds->getOriginal('funds'));
                }
            }

            $position++;


        }
    }
}
