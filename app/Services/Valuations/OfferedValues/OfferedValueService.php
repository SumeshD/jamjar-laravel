<?php

namespace JamJar\Services\Valuations\OfferedValues;

use Illuminate\Support\Facades\DB;
use JamJar\Model\ValuationDraft;
use JamJar\Valuation;
use JamJar\Vehicle;

/**
 * Class OfferedValueService
 * @package JamJar\Services\Valuations\OfferedValues
 *
 * This service has been created to merge Valuations and ValuationDrafts
 */
class OfferedValueService
{
    /**
     * @param Vehicle $vehicle
     * @return array|OfferedValueInterface[]
     */
    public function getOfferedValuesByVehicle(Vehicle $vehicle): array
    {
        /** @var ValuationDraft[] $draftsCollection */
        $draftsCollection = ValuationDraft::where('vehicle_id', '=', $vehicle->id)
            ->orderBy('price_in_pence', 'DESC')
            ->get();

        $drafts = [];
        foreach ($draftsCollection as $singleDraftFromCollection) {
            $drafts[$singleDraftFromCollection->getId()] = $singleDraftFromCollection;
        }

        $highestValuationStatement = DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC, valuations.created_at', 'DESC');

        $valuations = Valuation::where('vehicle_id', '=', $vehicle->id)
            ->orderBy($highestValuationStatement)
            ->get();

        $offeredValues = [];

        foreach ($valuations as $valuation) {
            $offeredValues[] = $valuation;

            /** @var ValuationDraft|null $draft */
            if ($draft = $valuation->getDraft()) {
                unset($drafts[$draft->getId()]);
            }
        }

        foreach ($drafts as $draft) {
            $offeredValues[] = $draft;
        }

        $this->sortOffers($offeredValues);

        return $offeredValues;
    }

    private function sortOffers(array &$offeredValues)
    {
        usort($offeredValues, function(OfferedValueInterface $valueA, OfferedValueInterface $valueB) {
            if ($valueA->hasCompleteSale()) {
                return -1;
            }

            if ($valueB->hasCompleteSale()) {
                return 1;
            }

            if ($valueA->hasRejectedSale() && !$valueB->hasRejectedSale()) {
                return 1;
            }

            if (!$valueA->hasRejectedSale() && $valueB->hasRejectedSale()) {
                return -1;
            }

            if ($valueA->getPriceInPence() === $valueB->getPriceInPence()) {
                return 0;
            }

            return $valueA->getPriceInPence() > $valueB->getPriceInPence() ? -1 : 1;
        });
    }
}
