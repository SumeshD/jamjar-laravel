<?php

namespace JamJar\Services\Valuations\OfferedValues;

use JamJar\Company;
use JamJar\PartnerLocation;
use JamJar\Sale;
use JamJar\Vehicle;

interface OfferedValueInterface
{
    const VALUE_SOURCE_EXTERNAL = 'external';

    const VALUE_SOURCE_MATRIX_RULES = 'matrix_rules';

    const VALUE_SOURCE_ASSOCIATES = 'associates';

    const PAYMENT_METHOD_BANK_TRANSFER = 'Bank Transfer';

    const VEHICLE_CONDITION_GREAT = 'great';

    const VEHICLE_CONDITION_GOOD = 'good';

    const VEHICLE_CONDITION_FAIR = 'fair';

    const VEHICLE_CONDITION_POOR = 'poor';

    public function getPriceInPence(): int;

    public function getSource(): string;

    public function getBuyerCompany(): Company;

    public function shouldBeCollectedFromAnyLocation(): bool;

    public function shouldBeCollectedFromClientLocation(): bool;

    public function getDistanceToSellerInMiles(): ?int;

    public function shouldBeDroppedAtDistance(): bool;

    public function shouldBeDroppedAtLocation(): bool;

    public function getLocation(): ?PartnerLocation;

    public function getValuationValidityInDays(): int;

    public function getCollectionFeeInPence(): int;

    public function isCollectionAvailable(): bool;

    public function isDropOffAvailable(): bool;

    public function getDropOffFeeInPence(): int;

    public function getBankTransferFeeInPence(): int;

    public function getPaymentMethod(): string;

    public function getAdminFeeInPence(): int;

    public function hasSale(): bool;

    public function hasCompleteSale(): bool;

    public function hasPendingSale(): bool;

    public function hasRejectedSale(): bool;

    public function getSale(): ?Sale;

    public function hasBuyerEnoughFunds(): ?bool;

    public function isExpired(): ?bool;

    public function getId(): int;

    public function getSellerCompany(): ?Company;

    public function getFinalDropOffPriceInPence(string $vehicleCondition = OfferedValueInterface::VEHICLE_CONDITION_GREAT): ?int;

    public function getFinalCollectionPriceInPence(string $vehicleCondition = OfferedValueInterface::VEHICLE_CONDITION_GREAT): ?int;

    public function getVehicle(): Vehicle;

    public function getListingPosition(): int;
}
