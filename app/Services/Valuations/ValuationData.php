<?php

namespace JamJar\Services\Valuations;

use JamJar\Company;
use \Exception;
use JamJar\PartnerLocation;

class ValuationData
{
    /** @var Company */
    public $buyerCompany;

    /** @var int */
    public $priceInPence;

    /** @var bool */
    public $isCollectionMethodAvailable = false;

    /** @var bool */
    public $isDropOffMethodAvailable = false;

    /** @var string */
    public $acceptanceUrl = '';

    /** @var string */
    public $tempRef = '';

    /** @var int */
    public $adminFeeInPence = 0;

    /** @var int */
    public $distanceInMiles;

    /** @var int */
    public $valuationValidityInDays = 5;

    /** @var int */
    public $dropOffFeeInPence = 0;

    /** @var int */
    public $collectionFeeInPence = 0;

    /** @var string */
    public $request = '';

    /** @var string */
    public $response = '';

    /** @var string */
    public $valuationDataSource = '';

    /** @var int */
    public $priceInPenceConditionGood;

    /** @var int */
    public $priceInPenceConditionFair;

    /** @var int */
    public $priceInPenceConditionPoor;

    /** @var null|PartnerLocation */
    public $buyerLocation;

    /** @var null|int */
    public $bankTransferFeeInPence;

    /** @var null|string */
    public $paymentMethod = 'Bank Transfer';

    /** @var int */
    public $position;

    /**
     * @param Company $buyerCompany
     * @param int $priceInPence
     * @param bool $isCollectionMethodAvailable
     * @param bool $isDropOffMethodAvailable
     * @param string $valuationDataSource
     * @return ValuationData
     * @throws Exception
     */
    public static function create(
        Company $buyerCompany,
        int $priceInPence,
        bool $isCollectionMethodAvailable,
        bool $isDropOffMethodAvailable,
        string $valuationDataSource
    ) {
        if ($isCollectionMethodAvailable == false && $isDropOffMethodAvailable == false) {
            throw new Exception('At least one delivery method must be available');
        }

        $data = new ValuationData();

        $data->buyerCompany = $buyerCompany;
        $data->priceInPence = $priceInPence;
        $data->isCollectionMethodAvailable = $isCollectionMethodAvailable;
        $data->isDropOffMethodAvailable = $isDropOffMethodAvailable;
        $data->valuationDataSource = $valuationDataSource;

        return $data;
    }

    private function __construct()
    {
    }
}
