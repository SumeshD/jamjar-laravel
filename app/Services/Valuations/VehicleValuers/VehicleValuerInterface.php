<?php

namespace JamJar\Services\Valuations\VehicleValuers;

use JamJar\Model\ValuationDraft;
use JamJar\Services\Valuations\ExternalDataProviders\ValuationDataProviderInterface;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Vehicle;

interface VehicleValuerInterface
{
    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationDraft[]
     */
    public function createValuationDrafts(array $priceEstimations, Vehicle $vehicle): array;

    public function getDataProvider(): ValuationDataProviderInterface;

    public function acceptsNonRunners(): bool;
}
