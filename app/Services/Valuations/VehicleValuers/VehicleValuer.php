<?php

namespace JamJar\Services\Valuations\VehicleValuers;

use JamJar\Model\ValuationDraft;
use JamJar\Model\ValuationDraftDeliveryMethod;
use JamJar\Model\ValuationDraftDeliveryMethodFee;
use JamJar\Model\ValuationDraftPrice;
use JamJar\PartnerLocation;
use JamJar\Services\Valuations\ExternalDataProviders\ValuationDataProviderInterface;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\User;
use JamJar\Vehicle;
use JamJar\Services\Valuations\ValuationData;

class VehicleValuer implements VehicleValuerInterface
{
    /** @var ValuationDataProviderInterface */
    private $dataProvider;

    public function __construct(ValuationDataProviderInterface $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationDraft[]
     */
    public function createValuationDrafts(array $priceEstimations, Vehicle $vehicle): array
    {
        $externalData = $this->dataProvider->getValuationsData($priceEstimations, $vehicle);

        $valuationDrafts = [];

        foreach ($externalData as $valuationData) {
            $location = PartnerLocation::where('company_id', '=', $valuationData->buyerCompany->id)->first();

            $valuationDraft = ValuationDraft::create(
                $vehicle,
                $valuationData->buyerCompany,
                $valuationData->priceInPence,
                $location,
                $valuationData->valuationDataSource,
                round($valuationData->distanceInMiles * 0.621371,0), //convert to miles
                $valuationData->acceptanceUrl,
                $valuationData->tempRef,
                $valuationData->request,
                $valuationData->response,
                $valuationData->valuationValidityInDays,
                $valuationData->paymentMethod
            );

            if ($valuationData->isCollectionMethodAvailable) {
                $this->createCollectionDeliveryMethod($valuationData, $valuationDraft);
            }

            if ($valuationData->isDropOffMethodAvailable) {
                $this->createDropOffDeliveryMethod($valuationData, $valuationDraft);
            }

            $this->createDraftPrices($valuationData, $valuationDraft);

            $valuationDrafts[] = $valuationDraft;
        }

        return $valuationDrafts;
    }

    public function getDataProvider(): ValuationDataProviderInterface
    {
        return $this->dataProvider;
    }

    public function acceptsNonRunners(): bool
    {
        return $this->dataProvider->acceptsNonRunners();
    }

    private function createCollectionDeliveryMethod(ValuationData $valuationData, ValuationDraft $valuationDraft)
    {
        $deliveryMethod = ValuationDraftDeliveryMethod::create(
            $valuationDraft,
            ValuationDraftDeliveryMethod::DELIVERY_METHOD_COLLECTION
        );

        if ($valuationData->collectionFeeInPence) {
            ValuationDraftDeliveryMethodFee::create(
                $deliveryMethod,
                ValuationDraftDeliveryMethodFee::FEE_TYPE_COLLECTION,
                $valuationData->collectionFeeInPence
            );
        }

        if ($valuationData->adminFeeInPence) {
            ValuationDraftDeliveryMethodFee::create(
                $deliveryMethod,
                ValuationDraftDeliveryMethodFee::FEE_TYPE_ADMIN,
                $valuationData->adminFeeInPence
            );
        }

        if ($valuationData->bankTransferFeeInPence) {
            ValuationDraftDeliveryMethodFee::create(
                $deliveryMethod,
                ValuationDraftDeliveryMethodFee::FEE_TYPE_BANK_TRANSFER,
                $valuationData->bankTransferFeeInPence
            );
        }
    }

    private function createDropOffDeliveryMethod(ValuationData $valuationData, ValuationDraft $valuationDraft)
    {
        $deliveryMethod = ValuationDraftDeliveryMethod::create(
            $valuationDraft,
            ValuationDraftDeliveryMethod::DELIVERY_METHOD_DROP_OFF
        );

        if ($valuationData->dropOffFeeInPence) {
            ValuationDraftDeliveryMethodFee::create(
                $deliveryMethod,
                ValuationDraftDeliveryMethodFee::FEE_TYPE_DROP_OFF,
                $valuationData->dropOffFeeInPence
            );
        }

        if ($valuationData->adminFeeInPence) {
            ValuationDraftDeliveryMethodFee::create(
                $deliveryMethod,
                ValuationDraftDeliveryMethodFee::FEE_TYPE_ADMIN,
                $valuationData->adminFeeInPence
            );
        }

        if ($valuationData->bankTransferFeeInPence) {
            ValuationDraftDeliveryMethodFee::create(
                $deliveryMethod,
                ValuationDraftDeliveryMethodFee::FEE_TYPE_BANK_TRANSFER,
                $valuationData->bankTransferFeeInPence
            );
        }
    }

    private function createDraftPrices(ValuationData $valuationData, ValuationDraft $valuationDraft)
    {
        ValuationDraftPrice::create(
            $valuationDraft,
            $valuationData->priceInPence,
            OfferedValueInterface::VEHICLE_CONDITION_GREAT
        );

        if ($valuationData->priceInPenceConditionGood) {
            ValuationDraftPrice::create(
                $valuationDraft,
                $valuationData->priceInPenceConditionGood,
                OfferedValueInterface::VEHICLE_CONDITION_GOOD
            );
        }

        if ($valuationData->priceInPenceConditionFair) {
            ValuationDraftPrice::create(
                $valuationDraft,
                $valuationData->priceInPenceConditionFair,
                OfferedValueInterface::VEHICLE_CONDITION_FAIR
            );
        }

        if ($valuationData->priceInPenceConditionPoor) {
            ValuationDraftPrice::create(
                $valuationDraft,
                $valuationData->priceInPenceConditionPoor,
                OfferedValueInterface::VEHICLE_CONDITION_POOR
            );
        }
    }
}
