<?php

namespace JamJar\Services\Valuations\VehicleValuers;

use JamJar\Company;
use JamJar\Model\ValuationDraft;
use JamJar\Model\ValuationDraftDeliveryMethod;
use JamJar\Model\ValuationDraftDeliveryMethodFee;
use JamJar\PartnerLocation;
use JamJar\Services\Valuations\ExternalDataProviders\ValuationDataProviderInterface;
use JamJar\Services\Valuations\InternalDataProviders\RulesDataProvider;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\User;
use JamJar\Vehicle;

class FakeVehicleValuer implements VehicleValuerInterface
{
    /** @var Company */
    private $buyerCompany;

    /** @var PartnerLocation */
    private $buyerLocation;

    public function __construct(Company $buyerCompany, PartnerLocation $buyerLocation)
    {
        $this->buyerCompany = $buyerCompany;
        $this->buyerLocation = $buyerLocation;
    }

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationDraft[]
     */
    public function createValuationDrafts(array $priceEstimations, Vehicle $vehicle): array
    {
        if ($vehicle->getNumberPlate() == 'XYZ1234') {
            $draft = ValuationDraft::create(
                $vehicle,
                $this->buyerCompany,
                500000,
                $this->buyerLocation,
                OfferedValueInterface::VALUE_SOURCE_ASSOCIATES,
                10,
                '',
                '',
                '',
                '',
                5,
                'Bank Transfer'
            );

            $collectionMethod = ValuationDraftDeliveryMethod::create(
                $draft,
                ValuationDraftDeliveryMethod::DELIVERY_METHOD_COLLECTION
            );

            ValuationDraftDeliveryMethodFee::create(
                $collectionMethod, ValuationDraftDeliveryMethodFee::FEE_TYPE_COLLECTION,
                3000
            );
            ValuationDraftDeliveryMethodFee::create(
                $collectionMethod, ValuationDraftDeliveryMethodFee::FEE_TYPE_ADMIN,
                5000
            );

            return [$draft];
        }

        return [];
    }

    public function getDataProvider(): ValuationDataProviderInterface
    {
        return new RulesDataProvider();
    }

    public function acceptsNonRunners(): bool
    {
        return true;
    }
}
