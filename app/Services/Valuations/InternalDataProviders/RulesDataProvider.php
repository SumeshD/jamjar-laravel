<?php

namespace JamJar\Services\Valuations\InternalDataProviders;

use JamJar\Api\CapApi;
use JamJar\Offer;
use JamJar\OfferDerivative;
use JamJar\PartnerLocation;
use JamJar\Services\DistanceCalculatorService;
use JamJar\Services\Valuations\ExternalDataProviders\ValuationDataProviderInterface;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Services\Valuations\VehiclePriceEstimators\VehiclePriceEstimatorInterface;
use JamJar\Traits\HandlesPartnerValuations;
use JamJar\User;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;
use JamJar\Vehicle;
use \Exception;

class RulesDataProvider implements ValuationDataProviderInterface, VehiclePriceEstimatorInterface
{
    use HandlesPartnerValuations;

    /** @var array|int[] */
    private $estimatedPrices = [];

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationData[]
     */
    public function getValuationsData(array $priceEstimations, Vehicle $vehicle): array
    {
        $capApi = new CapApi();
        $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();

        $manufacturer = VehicleManufacturer::where('name', $vehicle->meta->manufacturer)->where('vehicle_type', $vehicle->getVehicleTypeCapName())->first();

        $rules = Offer::where('manufacturer_id', $manufacturer->cap_manufacturer_id)->where('active', 1)->where('finished', 1)->get();

        // loop through the rules to check the model ID's and derivative ID's against our submitted vehicle

        /** @var array|int[] $partnersWithCreatedValuation */
        $partnersWithCreatedValuation = [];

        $drafts = [];

        if (!$rules) {
            return [];
        }

        \Log::info('start');
        foreach ($rules as $rule) {
            if ($vehicle->owner->user_id == $rule->user_id) {
                continue;
            }

            // Check we have a user
            if (!$rule->user) {
                !request()->has('_debug') ?: dump('FAIL: Rule has no user. Rule ID: '. $rule->id);
                \Log::info('FAIL: Rule has no user. Rule ID: '. $rule->id);
                continue;
            }

            if (in_array($rule->user->id, $partnersWithCreatedValuation)) {
                continue;
            }

            // Check we have a company
            if (!$rule->user->company) {
                !request()->has('_debug') ?: dump('FAIL: Rule has no company. Rule ID: '. $rule->id);
                \Log::info('FAIL: Rule has no company. Rule ID: '. $rule->id);
                continue;
            }

            // General Debug
            !request()->has('_debug') ?: dump('SUCCESS: '. $rule->user->company->name .' has a rule for this manufacturer.');

            \Log::info('SUCCESS: '. $rule->user->company->name .' has a rule for this manufacturer.');
            /*
            We need to know where the company is buying the vehicles from.
            Someone in Aberdeen doesn't want vehicles from Brighton.
             */
            if (empty(array_filter($rule->user->company->postcodes))) {
                !request()->has('_debug') ?: dump('FAIL: '. $rule->user->company->name . ' hasn\'t chosen postcodes to buy from.');
                \Log::info('FAIL: '. $rule->user->company->name . ' hasn\'t chosen postcodes to buy from.');
                continue;
            }

            $sellerPostcodeArea = $this->convertPostcodeIntoPostcodeArea((string) $vehicle->owner->postcode);

            try {
                $isSellerPostcodeAreaInBuyerAreas = in_array(
                    $sellerPostcodeArea,
                    explode(',', $rule->user->company->postcode_area)
                );
            } catch (\Exception $e) {
                $isSellerPostcodeAreaInBuyerAreas = false;
            }

            // is the current postcode within acceptable params?
            if (!$isSellerPostcodeAreaInBuyerAreas) {
                !request()->has('_debug') ?: dump('FAIL: '. $rule->user->company->name . ' does not buy from this postcode.');
                \Log::info('FAIL: '. $rule->user->company->name . ' does not buy from this postcode.');
                continue;
            }

            // is the model within acceptable rule params? (model_ids)
            // @deprecated - adds massive query overhead and unecessary api calls.
            // $model = VehicleModel::where('cap_model_id', $capApi->getModelIdForCapId($vehicle->cap_id))->first();

            $model = VehicleModel::where('cap_model_id', $vehicle->meta->cap_model_id)->first();
            if (!$model) {
                !request()->has('_debug') ?: dump('FAIL: model check failed');
                \Log::info('FAIL: model check failed '.$rule->id);
                continue;
            }

            // is the derivative within acceptable rule params? (derivative_ids)
            $derivative = VehicleDerivative::where('name', $vehicle->meta->derivative)->where('model_id', $vehicle->meta->cap_model_id)->first();
            if (!$derivative) {
                !request()->has('_debug') ?: dump('FAIL: derivative check failed');
                \Log::info('FAIL: derivative check failed '.$rule->id);
                continue;
            }

            // is the cap valuation within acceptable rule params? (minimum_value) / (maximum_value)
            if (!$this->checkValuation($rule, $capValuation)) {
                !request()->has('_debug') ?: dump('FAIL: valuation check failed');
                \Log::info('FAIL: valuation check failed '.$rule->id);
                continue;
            }

            // are the other vehicle details such as mot, service history, number of owners within acceptable rule params?
            if (!$this->validateOfferRules($vehicle, $rule)) {
                !request()->has('_debug') ?: dump('FAIL: rule validation failed');
                \Log::info('FAIL: rule validation failed '.$rule->id);
                continue;
            }

            // does this vehicle model match what the found rule wants?
            if (in_array($model->cap_model_id, $rule->model_ids_array)) {
                !request()->has('_debug') ?: dump('SUCCESS: '. $rule->user->company->name .' has a rule for this model.');
                // does the vehicle's derivative match what the found rule wants?
                $derivatives = $rule->derivative_ids_array;
                if (in_array($derivative->cap_derivative_id, $derivatives)) {
                    !request()->has('_debug') ?: dump('SUCCESS: '. $rule->user->company->name .' has a rule for this derivative.');
                    if (!$rule->user->funds->getOriginal('funds')) {
                        // send email saying they're missing out?
                        !request()->has('_debug') ?: dump('FAIL: '. $rule->user->company->name .' has no funds.');
                        \Log::info('FAIL: '. $rule->user->company->name .' has no funds.');
                        continue;
                    }

                    // Build a data array for this valuation.

                    $distanceService = new DistanceCalculatorService();

                    /** @var User $user */
                    $user = $rule->user;

                    $locations = $user->getActiveLocations();

                    if (count($locations) == 0) {
                        continue;
                    }

                    $closestLocation = $locations[0];
                    $closestDistance = null;

                    foreach ($locations as $location) {
                        $distance = $distanceService->getDistanceBetweenLocationsInMeters($location->postcode, $vehicle->owner->postcode);

                        if ($distance !== null && ($closestDistance === null || $distance < $closestDistance)) {
                            $closestLocation = $location;
                            $closestDistance = $distance;
                        }
                    }

                    //$valuation = $this->buildValuationCollection($rule, $vehicle, $capValuation);
                    $valuation = $this->getValuationWithBaseValue($rule, $vehicle, $capValuation);
                    $price = (int) (($valuation['base'] * 100));

                    $this->estimatedPrices[] = $price;

                    try {
                        $data = ValuationData::create(
                            $rule->user->company,
                            $price,
                            $closestLocation->allow_collection,
                            $closestLocation->allow_dropoff,
                            ValuationDataProviderInterface::DATA_SOURCE_MATRIX_RULES
                        );
                    } catch (Exception $e) {
                        return [];
                    }

                    $data->distanceInMiles = $closestDistance !== null ? round($closestDistance / 1000) : null;
                    $data->buyerLocation = $closestLocation;
                    $data->dropOffFeeInPence = ($closestLocation->dropoff_fee * 100) ?? null;
                    $data->collectionFeeInPence = ($closestLocation->collection_fee * 100) ?? null;
                    $data->adminFeeInPence = (int) ($rule->user->company->getOriginal('admin_fee'));
                    $bankTransferFee = (int) ($rule->user->company->getOriginal('bank_transfer_fee'));
                    $data->bankTransferFeeInPence = $bankTransferFee ?? null;

                    $data->priceInPenceConditionGood = (int) ($price * $rule->good_condition_percentage_modifier / 100);
                    $data->priceInPenceConditionFair = (int) ($price * $rule->avg_condition_percentage_modifier / 100);
                    $data->priceInPenceConditionPoor = (int) ($price * $rule->poor_condition_percentage_modifier / 100);

                    $drafts[] = $data;

                    $partnersWithCreatedValuation[] = $rule->user->id;
                } else {
                    !request()->has('_debug') ?: dump('FAIL: derivative ID does not match');
                    \Log::info('FAIL: derivative ID does not match '. $rule->id);
                }
            } else {
                !request()->has('_debug') ?: dump('FAIL: model ID does not match');
                \Log::info('FAIL: model ID does not match '. $rule->id);
            }
        }

        return $drafts;
    }

    public function acceptsNonRunners(): bool
    {
        return true;
    }

    /**
     * Build a valuation collection
     *
     * @param  JamJar\Offer   $rule
     * @param  JamJar\Vehicle $vehicle
     * @param  string         $capValuation
     * @return array
     */
    protected function buildValuationCollection(Offer $rule, Vehicle $vehicle, $capValuation)
    {
        foreach ($rule->derivatives as $derivative) {
            if ($vehicle->meta->derivative_id == $derivative['cap_derivative_id']) {
                if ($derivative->base_value) {
                    $baseValue = [
                        'value' => ((int)$capValuation['average'] / 100) * $derivative->base_value,
                        'percentage' => $derivative->base_value,
                    ];
                } else {
                    $baseValue = [
                        'value' => ((int)$capValuation['average'] / 100) * $derivative->base_value,
                        'percentage' => $derivative->base_value,
                    ];
                }
            }
        }

        $valuation = [
            'base' => $baseValue['value'],
            'cap' => [
                'retail' => (int)$capValuation['retail'],
                'clean' => (int)$capValuation['clean'],
                'average' => (int)$capValuation['average'],
                'below' => (int)$capValuation['below'],
            ],
            'collection' => null,
            'dropoff' => null,
            'great' => [
                'base' => ($baseValue['value'] / 100) * $rule->great_condition_percentage_modifier,
                'collection' => null,
                'dropoff' => null
            ],
            'good' => [
                'base' => ($baseValue['value'] / 100) * $rule->good_condition_percentage_modifier,
                'collection' => null,
                'dropoff' => null
            ],
            'fair' => [
                'base' => ($baseValue['value'] / 100) * $rule->avg_condition_percentage_modifier,
                'collection' => null,
                'dropoff' => null
            ],
            'poor' => [
                'base' => ($baseValue['value'] / 100) * $rule->poor_condition_percentage_modifier,
                'collection' => null,
                'dropoff' => null
            ]
        ];

        return $valuation;
    }

    protected function getValuationWithBaseValue(Offer $rule, Vehicle $vehicle, $capValuation) {
        $offerDerivative = OfferDerivative::where('offer_id', $rule->id)->where('cap_derivative_id', $vehicle->meta->derivative_id)->first();
        if ($offerDerivative->base_value) {
            $baseValue = [
                'value' => ((int)$capValuation['average'] / 100) * $offerDerivative->base_value,
                'percentage' => $offerDerivative->base_value,
            ];
        } else {
            $baseValue = [
                'value' => ((int)$capValuation['average'] / 100) * $offerDerivative->base_value,
                'percentage' => $offerDerivative->base_value,
            ];
        }

        $valuation['base'] = $baseValue['value'];

        return $valuation;
    }


    /**
     * @param Vehicle $vehicle
     * @return array|PriceEstimation[]
     */
    public function getPriceEstimations(Vehicle $vehicle): array
    {
        $estimations = [];

        foreach ($this->estimatedPrices as $estimatedPrice) {
            $estimations[] = PriceEstimation::create($estimatedPrice, PriceEstimation::TYPE_INTERNAL);
        }

        return $estimations;
    }

    private function convertPostcodeIntoPostcodeArea(string $postcode): string
    {
        $postcode = trim(strtoupper($postcode));

        $secondCharacter = substr($postcode, 1, 1);

        if ($secondCharacter == (string) (int) $secondCharacter) {
            return substr($postcode, 0, 1);
        }

        return substr($postcode, 0, 2);
    }
}
