<?php

namespace JamJar\Services\Valuations\InternalDataProviders;

use JamJar\AutoPartnerPricingTier;
use JamJar\Company;
use JamJar\Profile;
use JamJar\Services\Valuations\ExternalDataProviders\ValuationDataProviderInterface;
use JamJar\Services\Valuations\ValuationData;
use JamJar\Services\Valuations\VehiclePriceEstimators\PriceEstimation;
use JamJar\Traits\HandlesPartnerValuations;
use JamJar\User;
use JamJar\Vehicle;
use \Exception;

class AutoPartnerDataProvider implements ValuationDataProviderInterface
{
    use HandlesPartnerValuations;

    /**
     * @param array|PriceEstimation[] $priceEstimations
     * @param Vehicle $vehicle
     * @return array|ValuationData[]
     */
    public function getValuationsData(array $priceEstimations, Vehicle $vehicle): array
    {
        $automaticPartners = Profile::with(['user', 'user.company', 'user.company.location', 'user.website'])->where('auto_partner', 1)->get();

        $apiEstimations = [];
        $apiNonScrapEstimations = [];
        $ctbEstimation = null;

        foreach ($priceEstimations as $priceEstimation) {
            if ($priceEstimation->type == PriceEstimation::TYPE_EXTERNAL) {
                $apiEstimations[] = $priceEstimation;

                if ($priceEstimation->providerName != PriceEstimation::PROVIDER_NAME_CTB) {
                    $apiNonScrapEstimations[] = $priceEstimation;
                } else {
                    $ctbEstimation = $priceEstimation;
                }
            }
        }

        $sortFunction = function(PriceEstimation $estimationA, PriceEstimation $estimationB) {
            if ($estimationA->valueInPence == $estimationB->valueInPence) {
                return 0;
            }

            return ($estimationA->valueInPence < $estimationB->valueInPence) ? 1 : -1;
        };

        // sort DESC (highest first)
        usort($apiEstimations, $sortFunction);
        usort($apiNonScrapEstimations, $sortFunction);

        $lowestEstimation = null;
        $baseValue = null;

        /** @var PriceEstimation $highestApiEstimation */
        $highestApiEstimation = (count($apiEstimations) > 0) ? $apiEstimations[0] : null;

        if ($highestApiEstimation && $highestApiEstimation->valueInPence >= 120000 && count($apiEstimations) >= 6 && count($apiNonScrapEstimations) >= 6) {
            $lowestEstimation = $apiNonScrapEstimations[5];
            $baseValue = $lowestEstimation->valueInPence;
        } else if ($highestApiEstimation && $highestApiEstimation->valueInPence >= 120000 && count($apiEstimations) < 6 && count($apiNonScrapEstimations) > 0) {
            $lowestEstimation = $apiNonScrapEstimations[count($apiNonScrapEstimations) - 1];
            $baseValue = $lowestEstimation->valueInPence;
        } else if ($ctbEstimation) {
            $lowestEstimation = $ctbEstimation;
            $baseValue = $ctbEstimation->collectionValueInPence;
        }

        $collectedData = [];

        if (!$lowestEstimation) {
            return [];
        }

        foreach ($automaticPartners as $auto) {
            if (!$auto) {
                continue;
            }

            $value = (int) ($baseValue / 100);

            // Get the percentage from the nearest tier
            $percentage = AutoPartnerPricingTier::select('percentage')
                ->where('valuation_to', '>=', $value)
                ->where('user_id', $auto->user_id)
                ->first();

            if (!$percentage) {
                continue;
            }

            $percentage = $percentage->toArray();

            $percentage = $percentage['percentage'];
            // Get the automatic value based on above percentage for the tier we're in
            $autoValue = $value - (($value / 100) * $percentage);
            // Round up to the nearest five
            $autoValue = (ceil($autoValue)%5 === 0) ? ceil($autoValue) : round(($autoValue+5/2)/5)*5;

            if (!$auto->user) {
                // Usually happens when the user row is deleted
                \Log::debug('Auto Partner - No User Fail!', $auto->toArray());
                continue;
            }

            /** @var User $user */
            $user = $auto->user;

            // Shortcut the $location variable
            $locations = $user->getActiveLocations();
            if (!$locations) {
                continue;
            }

            $location = $locations[0];

            try {
                $data = ValuationData::create(
                    $auto->user->company,
                    $autoValue * 100,
                    $location->allow_collection,
                    $location->allow_dropoff,
                    ValuationDataProviderInterface::DATA_SOURCE_AUTO_PARTNER
                );
            } catch (Exception $e) {
                continue;
            }

            $data->collectionFeeInPence = $location->allow_collection ? (int) $location->collection_fee * 100 : null;
            $data->dropOffFeeInPence = $location->allow_dropoff ? (int) $location->dropoff_fee * 100 : null;
            $data->distanceInMiles = $this->getLocationRadius($vehicle, $auto->user->company);
            $data->buyerLocation = $location;
            $bankTransferFee = (int) ($auto->user->company->getOriginal('bank_transfer_fee'));
            $data->bankTransferFeeInPence = $bankTransferFee ?? null;
            $data->adminFeeInPence = (int) ($auto->user->company->getOriginal('admin_fee'));

            // Push our data array onto the main valuations collection
            $collectedData[] = $data;
        }

        return $collectedData;
    }

    public function acceptsNonRunners(): bool
    {
        return true;
    }

    protected function getLocationRadius(Vehicle $vehicle, Company $company): ?int
    {
        $response = \GoogleMaps::load('distancematrix')->setParam(
            [
                'origins' => $vehicle->owner->postcode,
                'destinations' => $company->postcode,
                'units' => 'imperial'
            ]
        )->get();

        $response = json_decode($response, true);

        if ($response['rows'][0]['elements'][0]['status'] != "NOT_FOUND") {
            $distance = str_replace(' mi', '', $response['rows'][0]['elements'][0]['distance']['text']);
            if ($distance) {
                return (int) $distance;
            }

            return null;
        }

        return null;
    }
}
