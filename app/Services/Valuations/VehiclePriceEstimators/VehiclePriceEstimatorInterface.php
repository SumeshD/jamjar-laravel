<?php

namespace JamJar\Services\Valuations\VehiclePriceEstimators;

use JamJar\Vehicle;

interface VehiclePriceEstimatorInterface
{
    /**
     * @param Vehicle $vehicle
     * @return array|PriceEstimation[]
     */
    public function getPriceEstimations(Vehicle $vehicle): array;
}