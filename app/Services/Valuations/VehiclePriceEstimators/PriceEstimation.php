<?php

namespace JamJar\Services\Valuations\VehiclePriceEstimators;

class PriceEstimation
{
    const TYPE_CAP = 'cap';

    const TYPE_EXTERNAL = 'external';

    const TYPE_INTERNAL = 'internal';

    const PROVIDER_NAME_CTB = 'CTB';

    /** @var string */
    public $type;

    /** @var string */
    public $providerName;

    /** @var int */
    public $valueInPence;

    /** @var int */
    public $collectionValueInPence;

    /** @var int */
    public $dropOffValueInPence;

    public static function create(int $valueInPence, string $type)
    {
        $estimation = new PriceEstimation();

        $estimation->valueInPence = $valueInPence;
        $estimation->type = $type;

        return $estimation;
    }

    private function __construct()
    {
    }
}
