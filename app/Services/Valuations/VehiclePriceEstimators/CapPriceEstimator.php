<?php

namespace JamJar\Services\Valuations\VehiclePriceEstimators;

use JamJar\Api\CapApi;
use JamJar\Vehicle;

class CapPriceEstimator implements VehiclePriceEstimatorInterface
{
    /**
     * @return array|PriceEstimation[]
     */
    public function getPriceEstimations(Vehicle $vehicle): array
    {
        $capApi = new CapApi;
        $capValuation = $capApi->getValuation($vehicle, $vehicle->type)->first();

        return [PriceEstimation::create($capValuation['average'] * 100, PriceEstimation::TYPE_CAP)];
    }
}
