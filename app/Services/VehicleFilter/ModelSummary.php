<?php

namespace JamJar\Services\VehicleFilter;

use JamJar\VehicleModel;

class ModelSummary
{
    /** @var VehicleModel */
    public $model;

    /** @var int */
    public $vehiclesCount;
}
