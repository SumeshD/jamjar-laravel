<?php

namespace JamJar\Services\VehicleFilter;

class MaximumMileageSummary
{
    /** @var int */
    public $maximumMileageNumber;

    /** @var int */
    public $vehiclesCount;
}
