<?php

namespace JamJar\Services\VehicleFilter;

use JamJar\VehicleManufacturer;

class ManufacturersListElement
{
    /** @var string */
    public $label;

    /** @var array|VehicleManufacturer[] */
    public $manufacturers;

    /** @var string */
    public $type;

    /**
     * ManufacturersListElement constructor.
     * @param string $label
     * @param array|VehicleManufacturer[] $manufacturers
     * @param string $type
     */
    public function __construct(string $label, array $manufacturers, string $type)
    {
        $this->label = $label;
        $this->manufacturers = $manufacturers;
        $this->type = $type;
    }

    /** @return array|int[] */
    public function getManufacturersIds(): array
    {
        return array_map(function(VehicleManufacturer $manufacturer) {
            return $manufacturer->getId();
        }, $this->manufacturers);
    }
}
