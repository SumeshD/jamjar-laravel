<?php

namespace JamJar\Services\VehicleFilter;

class VehicleManufacturerFilter
{
    /** @var array|int[] */
    public $manufacturersIds = [];

    /** @var array|int[] */
    public $modelsIds = [];
}
