<?php

namespace JamJar\Services\VehicleFilter;

use JamJar\VehicleManufacturer;

class ManufacturersList
{
    /** @var array|ManufacturersListElement[] */
    public $elements;

    public function asArray()
    {
        return array_map(function(ManufacturersListElement $element) {
            return [
                'label' => $element->label,
                'manufacturersIds' => $element->getManufacturersIds(),
                'type' => $element->type
            ];
        }, $this->elements);
    }
}
