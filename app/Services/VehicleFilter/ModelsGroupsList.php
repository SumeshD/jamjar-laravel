<?php

namespace JamJar\Services\VehicleFilter;

class ModelsGroupsList
{
    /** @var array|ModelsGroupListElement[] */
    public $elements;

    public function asArray(): array
    {
        $result = [];

        foreach ($this->elements as $element) {
            $models = ['manufacturerLabel' => $element->manufacturerLabel, 'modelsGroups' => []];

            foreach ($element->modelsGroups as $modelsGroup) {
                $models['modelsGroups'][] = [
                    'label'     => $modelsGroup->label,
                    'modelsIds' => $modelsGroup->getModelsIds(),
                    'type' => $modelsGroup->type
                ];
            }

            $result[$element->manufacturerLabel] = $models;
        }

        return $result;
    }
}
