<?php

namespace JamJar\Services\VehicleFilter;

use JamJar\VehicleModel;

class ModelsGroup
{
    /** @var string */
    public $label;

    /** @var array|VehicleModel[] */
    public $models;

    /** @return array|int[] */
    public function getModelsIds(): array
    {
        return array_map(function(VehicleModel $model) {
            return $model->getId();
        }, $this->models);
    }
}
