<?php

namespace JamJar\Services\VehicleFilter;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use JamJar\PlateYear;
use JamJar\User;
use JamJar\Vehicle;
use JamJar\VehicleManufacturer;
use JamJar\VehicleMeta;
use JamJar\VehicleModel;
use Illuminate\Http\Request;

class VehicleFilterService
{
    /** @var Carbon */
    private $dateLimit;

    public function __construct()
    {
        $this->dateLimit = Carbon::now()->subMonth();
    }

    public static function getAvailableMileagesRanges(): array
    {
        return [
            'Up to 5,000 miles' => '[0,5000]',
            'Up to 10,000 miles' => '[0,10000]',
            'Up to 15,000 miles' => '[0,15000]',
            'Up to 20,000 miles' => '[0,20000]',
            'Up to 25,000 miles' => '[0,25000]',
            'Up to 30,000 miles' => '[0,30000]',
            'Up to 35,000 miles' => '[0,35000]',
            'Up to 40,000 miles' => '[0,40000]',
            'Up to 45,000 miles' => '[0,45000]',
            'Up to 50,000 miles' => '[0,50000]',
            'Up to 60,000 miles' => '[0,60000]',
            'Up to 70,000 miles' => '[0,70000]',
            'Up to 80,000 miles' => '[0,80000]',
            'Up to 90,000 miles' => '[0,90000]',
            'Up to 100,000 miles' => '[0,100000]',
            'Up to 125,000 miles' => '[0,125000]',
            'Up to 150,000 miles' => '[0,150000]',
        //    '150,000 miles+' => '[150000,200000000]',
        ];
    }

    public static function getMinAvailableValues(): array
    {
        return [
            'From £1,000' => 1000,
            'From £2,500' => 2500,
            'From £5,000' => 5000,
            'From £10,000' => 10000,
            'From £20,000' => 20000,
            'From £30,000' => 30000,
            'From £40,000' => 40000,
            'From £50,000' => 50000,
            'From £75,000' => 75000,
            'From £100,000' => 100000,
            '£150,000+' => 150000,
        ];
    }

    public static function getMaxAvailableValues(): array
    {
        return [
            'Up to £1,000' => 1000,
            'Up to £2,500' => 2500,
            'Up to £5,000' => 5000,
            'Up to £10,000' => 10000,
            'Up to £20,000' => 20000,
            'Up to £30,000' => 30000,
            'Up to £40,000' => 40000,
            'Up to £50,000' => 50000,
            'Up to £75,000' => 75000,
            'Up to £100,000' => 100000,
            'Up to £150,000' => 150000,
        ];
    }

    public static function getAvailableFuelTypes(): array
    {
        /*
        $uniqueFuelTypes = VehicleMeta::select([DB::raw('distinct (fuel_type)')])->get();

        $fuelTypes = [];
        foreach ($uniqueFuelTypes as $uniqueFuelType) {
            $fuelTypes[] = $uniqueFuelType->fuel_type;
        }*/

        $fuelTypes = ['Petrol','Diesel'];

        return $fuelTypes;
    }

    public static function getAvailablePlateYears(): array
    {
        $plateYears = PlateYear::orderBy('id', 'ASC')->get();

        $displayedPlateYears = [];
        foreach ($plateYears as $plateYear) {
            $displayedPlateYears[] = $plateYear->plate_year . ' ' . $plateYear->plate_number;
        }

        return $displayedPlateYears;
    }

    public static  function getAvailableDatesRanges(): array
    {
        return [
            'Last 24 hours' => '[24,0]',
            'Last 3 days' => '[72,0]',
            'Last 7 days' => '[168,0]',
        ];
    }

    private function excludeManufacturers() {

        $excluded = [
            'AC',
            'AIXAM',
            'ALLIED',
            'ALPINE',
            'ASIA',
            'AUSTIN',
            'AUVERLAND',
            'AZURE DYNAMICS',
            'BEDFORD',
            "BREMACH",
            'BRISTOL',
            'COLEMAN MILNE',
            'DE TOMASO',
            'DFSK',
            'FBS',
            'FUSO',
            'GREAT WALL',
            'LADA',
            'LEVC',
            'LIGIER',
            'LTI',
            'MARCOS',
            'MARLIN',
            'MEGA',
            'MIA',
            'MICROCAR',
            'PAMPAS',
            'PGO',
            'PRINDIVILLE',
            'PROTON',
            'RELIANT',
            'SAN',
            'SANTANA',
            "SAO",
            'SMITH',
            'TALBOT',
            'TD CARS',
            'UMM',
            'WESTFIELD',
            'YUEJIN'

        ];

        return $excluded;

    }

    public function getManufacturersList(): ManufacturersList
    {
        $excluded = $this->excludeManufacturers();

        $manufacturers = VehicleManufacturer::whereNotIn('name', $excluded)->orderBy('name', 'ASC')->orderBy('vehicle_type', 'ASC')->get();

        $manufacturersArrayList = [];
        $typeClasses = [];

        foreach ($manufacturers as $manufacturer) {
            if (!isset($manufacturersArrayList[$manufacturer->getName()])) {
                $manufacturersArrayList[$manufacturer->getName()] = [];
                $typeClasses[$manufacturer->getName()] = [];
            }

            $manufacturersArrayList[$manufacturer->getName()][] = $manufacturer;
            $typeClasses[$manufacturer->getName()][] = $manufacturer->vehicle_type;
        }

        $manufacturersList = new ManufacturersList();

        foreach ($manufacturersArrayList as $manufacturerName => $manufacturers) {
            $manufacturersList->elements[] = new ManufacturersListElement($manufacturerName, $manufacturers, implode(' ', $typeClasses[$manufacturerName]));
        }

        return $manufacturersList;
    }

    public function getModelsList(): ModelsGroupsList
    {
        $models = VehicleModel::select('vehicle_models.*', 'vehicle_manufacturers.name as manufacturer_name','vehicle_manufacturers.vehicle_type as type')
            ->join('vehicle_manufacturers', 'vehicle_manufacturers.id', '=', 'vehicle_models.internal_manufacturer_id')
            ->get();

        $modelsArray = [];

        foreach ($models as $model) {
            if (!isset($modelsArray[$model->manufacturer_name])) {
                $modelsArray[$model->manufacturer_name] = [
                    'manufacturerLabel' => $model->manufacturer_name,
                    'modelsGroups' => [],
                ];
            }

            if (!isset($modelsArray[$model->manufacturer_name]['modelsGroups'][$model->name])) {
                $modelsArray[$model->manufacturer_name]['modelsGroups'][$model->name] = [
                    'label' => $model->name,
                    'models' => [],
                    'type' => $model->type
                ];
            }

            $modelsArray[$model->manufacturer_name]['modelsGroups'][$model->name]['models'][] = $model;
        }

        $modelsGroupsList = new ModelsGroupsList();

        foreach ($modelsArray as $modelsGroupsElementArray) {
            $modelsGroupsListElement = new ModelsGroupListElement();

            $modelsGroupsListElement->manufacturerLabel = $modelsGroupsElementArray['manufacturerLabel'];

            foreach ($modelsGroupsElementArray['modelsGroups'] as $modelsGroupsArray) {
                $modelGroup = new ModelsGroup();
                $modelGroup->label = $modelsGroupsArray['label'];
                $modelGroup->type =  $modelsGroupsArray['type'];

                foreach ($modelsGroupsArray['models'] as $model) {
                    $modelGroup->models[] = $model;
                }

                $modelsGroupsListElement->modelsGroups[] = $modelGroup;
            }

            $modelsGroupsList->elements[] = $modelsGroupsListElement;
        }

        return $modelsGroupsList;
    }

    public function getFilteredResultsSummary(VehicleFilters $filters, User $buyer): FilteredResultsSummary
    {
        $summary = new FilteredResultsSummary();

        $manufacturersIds = $this->getUniqueManufacturersIds($buyer);
        $summary->manufacturersSummaries = $this->getSortedManufacturerSummaries($buyer, $manufacturersIds);
        $summary->modelsSummaries = $this->getSortedModelSummaries($filters, $summary->manufacturersSummaries, $buyer);
        $summary->vehiclesCount = $this->getSelectedVehiclesCount($filters, $summary);

        return $summary;
    }

    /**
     * @param User $buyer
     * @return array|int[]
     */
    private function getUniqueManufacturersIds(User $buyer): array
    {
        $vehicles = Vehicle::select([DB::raw('distinct (vehicles.manufacturer_id)')]);
        $vehicles = $this->addSearchSelectors($vehicles, $buyer)->get();

        $manufacturersIds = [];
        foreach ($vehicles as $vehicle) {
            $manufacturersIds[] = $vehicle->manufacturer_id;
        }

        return $manufacturersIds;
    }

    /**
     * @param array|int[] $manufacturersIds
     * @return array|ManufacturerSummary[]
     */
    private function getSortedManufacturerSummaries(User $buyer, array $manufacturersIds): array
    {
        $summaries = [];

        $vehicles = Vehicle::select([
            DB::raw('count(vehicles.id) as count'),
            DB::raw('vehicles.manufacturer_id as manufacturer_id')
        ])
        ->groupBy('vehicles.manufacturer_id');

        $vehicles = $this->addSearchSelectors($vehicles, $buyer, false)->get();

        foreach ($vehicles as $vehicle) {
            $manufacturersSummary = new ManufacturerSummary();
            $manufacturersSummary->vehiclesCount = $vehicle->count;
            $manufacturersSummary->manufacturer = VehicleManufacturer::where('id', '=', $vehicle->manufacturer_id)->first();
            $summaries[] = $manufacturersSummary;
        }

        usort($summaries, function(ManufacturerSummary $summaryA, ManufacturerSummary $summaryB) {
            return $summaryB->vehiclesCount <=> $summaryA->vehiclesCount;
        });

        return $summaries;
    }

    /**
     * @param array|int[] $manufacturerIds
     * @param array|ManufacturerSummary[] $manufacturerSummaries
     * @return array|ModelSummary[]
     */
    private function getSortedModelSummaries(VehicleFilters $filters, array $manufacturerSummaries, User $buyer)
    {
        $summaries = [];

        $vehicles = Vehicle::select([
                DB::raw('count(vehicles.id) as count'),
                DB::raw('vehicles.model_id as model_id')
            ])
            ->groupBy('vehicles.manufacturer_id', 'vehicles.model_id');

        $vehicles = $this->addSearchSelectors($vehicles, $buyer, false)
            ->where([['vehicles.model_id', '<>', null]])
            ->get();

        foreach ($vehicles as $vehicle) {
            $model = VehicleModel::where('id', '=', $vehicle->model_id)->first();

            if (!$model) {
                continue;
            }

            $modelSummary = new ModelSummary();
            $modelSummary->vehiclesCount = $vehicle->count;
            $modelSummary->model = $model;

            $summaries[] = $modelSummary;
        }

        usort($summaries, function(ModelSummary $summaryA, ModelSummary $summaryB) {
            return $summaryB->vehiclesCount <=> $summaryA->vehiclesCount;
        });

        return $summaries;
    }

    private function getSelectedVehiclesCount(VehicleFilters $filters, FilteredResultsSummary $summary)
    {
        if (count($filters->vehicleManufacturersFilters) == 0) {
            return array_reduce($summary->manufacturersSummaries, function($carry, $manufacturerSummary) {
                $carry += $manufacturerSummary->vehiclesCount;

                return $carry;
            });
        }

        $count = 0;

        foreach ($filters->vehicleManufacturersFilters as $manufacturerFilter) {
            if ($manufacturerFilter->modelsIds) {
                foreach ($summary->modelsSummaries as $modelSummary) {
                    if (in_array($modelSummary->model->getId(), $manufacturerFilter->modelsIds)) {
                        $count += $modelSummary->vehiclesCount;
                    }
                }

                continue;
            }

            foreach ($summary->manufacturersSummaries as $manufacturerSummary) {
                if (in_array($manufacturerSummary->manufacturer->getId(), $manufacturerFilter->manufacturersIds)) {
                    $count += $manufacturerSummary->vehiclesCount;
                }
            }
        }

        return $count;
    }

    public function createFiltersFromRequest(Request $request): VehicleFilters
    {
        $filters = new VehicleFilters();

        if ($combinedManufacturersAndModelsList = $request->get('manufacturers-and-models')) {
            foreach ($combinedManufacturersAndModelsList as $combinedManufacturersAndModels) {
                list($manufacturersIds, $modelsIds) = explode(';', $combinedManufacturersAndModels);

                $models = new VehicleManufacturerFilter();
                $models->manufacturersIds = $manufacturersIds ?
                    array_map(function($el) { return (int) $el;}, explode(',', $manufacturersIds)) :
                    [];
                $models->modelsIds = $modelsIds ?
                    array_map(function($el) { return (int) $el;}, explode(',', $modelsIds)) :
                    [];

                $filters->vehicleManufacturersFilters[] = $models;
            }
        }

        $numberOfOwners = $request->get('number-of-owners');
        if ($numberOfOwners) {
            $filters->numberOfOwners = $numberOfOwners;
        }

        $mileageRange = $request->get('mileage-range');
        if ($mileageRange) {
            $mileageRange = json_decode($mileageRange);
            $filters->minMileage = (int) $mileageRange[0];
            $filters->maxMileage = (int) $mileageRange[1];
        }

        $minValue = $request->get('min-value');
        if ($minValue) {
            $filters->minValue = $minValue;
        }

        $maxValue = $request->get('max-value');
        if ($maxValue) {
            $filters->maxValue = $maxValue;
        }

        $fuelType = $request->get('fuel-type');
        if ($fuelType) {
            $filters->fuelType = $fuelType;
        }

        $minPlateYear = $request->get('min-plate-year');
        if ($minPlateYear) {
            $filters->minPlateYear = $minPlateYear;
        }

        $addedToMarketplaceFrom = $request->get('added-to-marketplace-date-from');
        if ($addedToMarketplaceFrom) {
            $filters->addedToMarketplaceDateFrom = Carbon::createFromFormat('Y-m-d', $addedToMarketplaceFrom);
        }

        $addedToMarketplaceTo = $request->get('added-to-marketplace-date-to');
        if ($addedToMarketplaceTo) {
            $filters->addedToMarketplaceDateTo = Carbon::createFromFormat('Y-m-d', $addedToMarketplaceTo);
        }

        $datesRange = $request->get('dates-range');
        if ($datesRange) {
            $decodedDatesRange = json_decode($datesRange);
            $hoursFrom = (int) $decodedDatesRange[0];
            $hoursTo = (int) $decodedDatesRange[1];
            $filters->addedToMarketplaceDateFrom = Carbon::now()->subHours($hoursFrom);
            $filters->addedToMarketplaceDateTo = Carbon::now()->subHours($hoursTo);
        }

        $carCondition = $request->get('car-condition');
        if ($carCondition && $carCondition != 'all') {
            $filters->carCondition = $carCondition;
        }

        $sellerType = $request->get('seller-type');
        if ($sellerType && $sellerType != 'all') {
            $filters->sellerType = $sellerType;
        }

        $vehicleType = $request->get('vehicle-type');
        if ($vehicleType && $vehicleType != 'all') {
            $filters->vehicleType = $vehicleType;
        }


        $vehicleLocation = $request->get('vehicle-location');
        if ($vehicleLocation && $vehicleLocation != 'all') {
            $filters->vehicleLocation = $vehicleLocation;
        }

        return $filters;
    }

    private function addSearchSelectors($vehicles, User $buyer, $addGroupBy = true)
    {
        return Vehicle::addSearchSelectors($vehicles, $buyer, $this->dateLimit, $addGroupBy);
    }
}
