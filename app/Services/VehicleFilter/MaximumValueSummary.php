<?php

namespace JamJar\Services\VehicleFilter;

class MaximumValueSummary
{
    /** @var int */
    public $maximumValueInPence;

    /** @var int */
    public $vehiclesCount;
}
