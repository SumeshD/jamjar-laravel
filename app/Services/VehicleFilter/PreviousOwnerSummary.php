<?php

namespace JamJar\Services\VehicleFilter;

class PreviousOwnerSummary
{
    /** @var int */
    public $previousOwnersNumber;

    /** @var int */
    public $vehiclesCount;
}
