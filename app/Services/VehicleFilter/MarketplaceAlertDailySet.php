<?php

namespace JamJar\Services\VehicleFilter;

use JamJar\Model\MarketplaceAlerts\MarketplaceAlert;
use JamJar\Vehicle;

class MarketplaceAlertDailySet
{
    /** @var MarketplaceAlert */
    public $alert;

    /** @var array|Vehicle[] */
    public $vehicles;
}
