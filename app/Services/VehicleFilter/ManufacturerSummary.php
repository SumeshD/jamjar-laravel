<?php

namespace JamJar\Services\VehicleFilter;

use JamJar\VehicleManufacturer;

class ManufacturerSummary
{
    /** @var VehicleManufacturer */
    public $manufacturer;

    /** @var int */
    public $vehiclesCount;
}
