<?php

namespace JamJar\Services\VehicleFilter;

use Carbon\Carbon;

class VehicleFilters
{
    /** @var array|VehicleManufacturerFilter[] */
    public $vehicleManufacturersFilters = [];

    /** @var int */
    public $numberOfOwners;

    /** @var int */
    public $minMileage;

    /** @var int */
    public $maxMileage;

    /** @var int */
    public $minValue;

    /** @var int */
    public $maxValue;

    /** @var Carbon */
    public $addedToMarketplaceDateFrom;

    /** @var Carbon */
    public $addedToMarketplaceDateTo;

    /** @var string */
    public $fuelType;

    /** @var string */
    public $minPlateYear;

    /** @var string */
    public $carCondition;

    /** @var string */
    public $sellerType;

    /** @var string */
    public $vehicleType;

    /** @var string */
    public $vehicleLocation;

    public function toArray(): array
    {
        $result = ['vehicleManufacturersFilters' => []];

        foreach ($this->vehicleManufacturersFilters as $manufacturerFilter) {
            $result['vehicleManufacturersFilters'][] = [
                'manufacturersIds' => $manufacturerFilter->manufacturersIds,
                'modelsIds' => $manufacturerFilter->modelsIds,
            ];
        }

        return $result;
    }

    public function isAnyFilterSelected(): bool
    {
        return $this->vehicleManufacturersFilters ||
            $this->minPlateYear ||
            $this->numberOfOwners ||
            $this->minMileage ||
            $this->maxMileage ||
            $this->minValue ||
            $this->maxValue ||
            $this->addedToMarketplaceDateFrom ||
            $this->addedToMarketplaceDateTo ||
            $this->fuelType ||
            $this->carCondition ||
            $this->sellerType ||
            $this->vehicleType ||
            $this->vehicleLocation;
    }
}
