<?php

namespace JamJar\Services\VehicleFilter;

class FilteredResultsSummary
{
    /** @var VehicleFilters */
    public $vehicleFilters;

    /** @var int */
    public $vehiclesCount = 0;

    /** @var array|ManufacturerSummary[] */
    public $manufacturersSummaries = [];

    /** @var array|ModelSummary[] */
    public $modelsSummaries = [];

    /** @var array|PreviousOwnerSummary[] */
    public $previousOwnersSummaries = [];

    /** @var array|MaximumMileageSummary[] */
    public $maximumMileageSummaries = [];

    /** @var array|MaximumValueSummary[] */
    public $maximumValuesSummaries = [];

    public function asArray()
    {
        return [
            'vehiclesCount' => $this->vehiclesCount,
            'manufacturersSummaries' => array_map(function(ManufacturerSummary $manufacturerSummary) {
                return [
                    'vehiclesCount'     => $manufacturerSummary->vehiclesCount,
                    'manufacturerId'    => $manufacturerSummary->manufacturer->getId(),
                    'label'             => $manufacturerSummary->manufacturer->getName(),
                ];
            }, $this->manufacturersSummaries),
            'modelsSummaries' => array_map(function(ModelSummary $modelSummary) {
                return [
                    'vehiclesCount'     => $modelSummary->vehiclesCount,
                    'modelId'           => $modelSummary->model->getId(),
                    'label'             => $modelSummary->model->getName(),
                    'manufacturerLabel' => $modelSummary->model->manufacturer->name,
                ];
            }, $this->modelsSummaries),
        ];
    }
}
