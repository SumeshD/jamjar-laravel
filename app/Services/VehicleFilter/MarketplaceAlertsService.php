<?php

namespace JamJar\Services\VehicleFilter;

use Carbon\Carbon;
use JamJar\Api\TextMarketer;
use JamJar\Mail\MarketplaceAlerts\DailyEmail;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlert;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlertsFilter;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlertsFiltersManufacturer;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlertsFiltersModel;
use JamJar\User;
use JamJar\Vehicle;
use Mail;
use \Exception;

class MarketplaceAlertsService
{
    public function createAlert(User $user, VehicleFilters $filters, string $name, string $alertFrequency)
    {
        $alert = new MarketplaceAlert();
        $this->bindDataToAlert($alert, $user, $filters, $name, $alertFrequency);

        return $alert;
    }

    public function updateAlert(MarketplaceAlert $alert, VehicleFilters $newFilters, $name, $alertFrequency)
    {
        $alert->removeManufacturersFilters();
        $this->bindDataToAlert($alert, $alert->getUser(), $newFilters, $name, $alertFrequency);

        return $alert;
    }

    /**
     * @param Vehicle $vehicle
     * @return array|MarketplaceAlert[]
     */
    public function getAlertsToSendInstantReminders(Vehicle $vehicle): array
    {
        $alerts = MarketplaceAlert::getActiveInstantAlerts();
        $alertsToRemind = [];

        foreach ($alerts as $alert) {
            $filtersSet = $alert->convertToFilters();
            if(!$alert->user){
                continue;
            }
            $isVehicleFound = (bool) Vehicle::getVehiclesQueryForMatrixByFilters($alert->getUser(), $filtersSet)
                ->where('vehicles.id', '=', $vehicle->id)
                ->first();

            if ($isVehicleFound) {
                $alertsToRemind[] = $alert;
            }
        }

        return $alertsToRemind;
    }

    /** @return array|MarketplaceAlertDailySet[] */
    public function getAlertsToSendDailyReminders(): array
    {
        $alerts = MarketplaceAlert::getActiveDailyAlerts();
        $alertsToRemind = [];

        foreach ($alerts as $alert) {
            $filtersSet = $alert->convertToFilters();

            $foundVehicles = Vehicle::getVehiclesQueryForMatrixByFilters($alert->getUser(), $filtersSet)
                ->where('vehicles.added_to_marketplace_at', '>=', Carbon::now()->subDay())
                ->get();

            if (count($foundVehicles) > 0) {
                $alertSet = new MarketplaceAlertDailySet();
                $alertSet->alert = $alert;
                $alertSet->vehicles = $foundVehicles;

                $alertsToRemind[] = $alertSet;
            }
        }

        return $alertsToRemind;
    }

    private function bindDataToAlert(MarketplaceAlert $alert, User $user, VehicleFilters $filters, string $name, string $alertFrequency): MarketplaceAlert
    {
        $alert
            ->setUser($user)
            ->setIsEnabled(true)
            ->setName($name)
            ->setFrequency($alertFrequency)
            ->setMinPlateYear($filters->minPlateYear)
            ->setNumberOfOwners($filters->numberOfOwners)
            ->setMinMileage($filters->minMileage)
            ->setMaxMileage($filters->maxMileage)
            ->setMinValue($filters->minValue)
            ->setMaxValue($filters->maxValue)
            ->setFuelType($filters->fuelType)
            ->setCarCondition($filters->carCondition)
            ->setSellerType($filters->sellerType)
            ->setVehicleType($filters->vehicleType)
            ->setVehicleLocation($filters->vehicleLocation)
            ->save();

        foreach ($filters->vehicleManufacturersFilters as $manufacturerFilter) {
            $marketFilter = new MarketplaceAlertsFilter();
            $marketFilter->setMarketplaceAlert($alert)->save();

            foreach ($manufacturerFilter->manufacturersIds as $manufacturerId) {
                $marketFilterManufacturer = new MarketplaceAlertsFiltersManufacturer();
                $marketFilterManufacturer->setFilter($marketFilter)->setManufacturerId($manufacturerId)->save();
            }

            foreach ($manufacturerFilter->modelsIds as $modelId) {
                $marketFilterModel = new MarketplaceAlertsFiltersModel();
                $marketFilterModel->setFilter($marketFilter)->setModelId($modelId)->save();
            }
        }

        return $alert;
    }

    public function sendDailyReminders()
    {
        $errors = [];
        $info = [];

        $alertsSets = $this->getAlertsToSendDailyReminders();
        $groupedAlertsSets = $this->groupAlertsSetsByUser($alertsSets);

        foreach ($groupedAlertsSets as $userId => $alertSets) {
            /** @var User $user */
            $user = User::where('id', '=', $userId)->first();

            if (!$user) {
                $info[] = 'Problem with user id: ' . $userId;
                continue;
            }

            /** @var MarketplaceAlert[] $alertsToRemind */
            $alertsToRemind = [];
            $alertsToRemindIds = [];
            /** @var Vehicle[] $vehicles */
            $vehicles = [];
            foreach ($alertSets as $alertSet) {
                /** @var MarketplaceAlertDailySet $alertSet */
                $alertsToRemind[] = $alertSet->alert;
                $alertsToRemindIds[] = $alertSet->alert->getId();
                foreach ($alertSet->vehicles as $vehicle) {
                    $vehicles[$vehicle->id] = $vehicle;
                }
            }

            $vehiclesCount = count($vehicles);

            try {
                $email = new DailyEmail($user, $vehicles, $alertsToRemind);
                Mail::to($user->email)->send($email);
                $info[] = 'Email sent to: ' . $user->email;
            } catch (Exception $e) {
                $errors[] = 'Email not sent, problematic user: ' . $user->getId();
            }

            if (!$user->getProfile()) {
                continue;
            }

            try {
                $smsContent = sprintf(
                    '%d newly added %s matching your Marketplace Alerts! %s',
                    $vehiclesCount,
                    ($vehiclesCount == 1 ? 'vehicle' : 'vehicles'),
                    route('marketplaceEnabledAlertsList') . '?alerts=['. implode(',', $alertsToRemindIds) .']'
                );

                (new TextMarketer)->sendSms($user->getProfile()->getMobileNumber(), 'jamjar.com', $smsContent);
                $info[] = 'SMS sent to: ' . $user->getFirstNameAttribute() . ' ' . $user->getLastNameAttribute();
            } catch(Exception $e) {
                $errors[] = 'SMS not sent, problematic user: ' . $user->getId();
            }
        }

        return [
            'info' => $info,
            'errors' => $errors,
        ];
    }

    /**
     * Returns alerts sets as associate table grouped by user (partner) id
     *
     * @param MarketplaceAlertDailySet[] $alertsSets
     * @return array
     */
    private function groupAlertsSetsByUser(array $alertsSets): array
    {
        $userAlertsGroups = [];

        foreach ($alertsSets as $alertsSet) {
            $userId = $alertsSet->alert->getUser()->getId();

            if (!isset($userAlertsGroups[$userId])) {
                $userAlertsGroups[$userId] = [];
            }

            $userAlertsGroups[$userId][] = $alertsSet;
        }

        return $userAlertsGroups;
    }
}
