<?php

namespace JamJar\Services\VehicleFilter;

class ModelsGroupListElement
{
    /** @var string */
    public $manufacturerLabel;

    /** @var array|ModelsGroup[] */
    public $modelsGroups;
}
