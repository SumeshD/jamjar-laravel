<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PlateYear extends Model
{
    use LogsActivity;

    public $timestamps = false;
}
