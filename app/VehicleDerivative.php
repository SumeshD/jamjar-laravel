<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use JamJar\Traits\HasSuggestedValues;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class VehicleDerivative extends Model
{
    use LogsActivity, HasSuggestedValues;

    protected $fillable = ['cap_derivative_id','name','model_id', 'internal_model_id'];

    protected $with = ['model'];

    /**
     * A vehicle derivative belongs to a vehicle model
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        /**
         * THIS MAY NOT WORK, PREVIOUSLY connected WITH model_id AND cap_model_id
         */
        return $this->belongsTo(VehicleModel::class, 'internal_model_id', 'id');
    }

    /**
     * Get the Suggested Value Attribute
     *
     * @return integer
     */
    public function getSuggestedValueAttribute()
    {
        return $this->getSuggestedValue();
    }

    public function getId()
    {
        return $this->id;
    }

    public static function getFullNameByOfferDerivativeId(int $offerDerivativeId): string
    {
        /** @var OfferDerivative $offerDerivative */
        $offerDerivative = OfferDerivative::where([['id', '=', $offerDerivativeId]])->first();

        if (!$offerDerivative) {
            return '';
        }

        $offer = $offerDerivative->getOffer();

        $vehicleType = strtoupper($offer->getVehicleType());
        $vehicleType = ($vehicleType == 'CAR' ? 'CAR' : 'LCV');

        $vehicleManufacturer = VehicleManufacturer::where([
            ['cap_manufacturer_id', '=', $offer->getCapManufacturerId()],
            ['vehicle_type', '=', $vehicleType]
        ])->first();

        if (!$vehicleManufacturer) {
            return '';
        }

        $fullName = $vehicleManufacturer->getName();

        $vehicleModel = VehicleModel::where([
            ['internal_manufacturer_id', '=', $vehicleManufacturer->getId()],
            ['cap_model_id', '=', $offerDerivative->getCapModelId()],
        ])->first();

        if (!$vehicleModel) {
            return $fullName;
        }

        $fullName .= ' ' . $vehicleModel->getName();

        /** @var VehicleDerivative $vehicleDerivative */
        $vehicleDerivative = VehicleDerivative::where([
            ['internal_model_id', '=', $vehicleModel->getId()],
            ['cap_derivative_id', '=', $offerDerivative->getCapDerivativeId()],
        ])->first();

        if (!$vehicleDerivative) {
            return $fullName;
        }

        return $fullName . ' ' . $vehicleDerivative->getName();
    }

    public function getName(): string
    {
        return $this->name;
    }
}
