<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Colour extends Model
{
    use LogsActivity;

    public $guarded = [];

    public $timestamps = false;
}
