<?php

namespace JamJar;

use JamJar\Primitives\Money;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Company extends Model
{
    use LogsActivity;

    // public $fillable = ['name', 'telephone', 'vat_number', 'address_line_one', 'address_line_two', 'town', 'city', 'county', 'postcode', 'company_type', 'valuation_validity'];
    public $guarded = [];

    /**
     * A company belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A company has many locations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function location()
    {
        return $this->hasMany(PartnerLocation::class);
    }

    /**
     * A company has many locations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeLocation()
    {
        return $this->hasMany(PartnerLocation::class)->where('is_active',1);
    }

    /**
     * A company has one website
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function website()
    {
        return $this->hasOne(PartnerWebsite::class);
    }

    /**
     * A Company pays for one position.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function position()
    {
        return $this->hasOne(PartnerPosition::class);
    }

    public function valuation()
    {
        return $this->hasMany(Valuation::class);
    }

    public function getPosition(): ?PartnerPosition
    {
        return $this->position;
    }

    public function getPostcodesAttribute()
    {
        return explode(',', $this->postcode_area);
    }

    public function getValuationValidityAttribute($valuation_validity)
    {
        return (is_null($valuation_validity)) ? '5 day' : $valuation_validity;
    }

    /**
     * Set Admin Fee Attribute
     *
     * @param float $value
     */
    public function setAdminFeeAttribute($value)
    {
        $value = str_replace('£', '', $value);
        if ((!is_null($value)) && (!empty($value))) {
            $amount = Money::fromPounds($value);
            $this->attributes['admin_fee'] = $amount->inPence();
        } else {
            $this->attributes['admin_fee'] = 000;
        }
    }

    /**
     * Get the Admin Fee Attribute
     *
     * @param  int $adminFee
     * @return float
     */
    public function getAdminFeeAttribute($adminFee)
    {
        if ($adminFee) {
            $money = Money::fromPence($adminFee);
            return $money->inPoundsAndPence();
        }
    }


    /**
     * Set Bank Transfer Fee Attribute
     *
     * @param float $value
     */
    public function setBankTransferFeeAttribute($value)
    {
        $value = str_replace('£', '', $value);
        if ((!is_null($value)) && (!empty($value))) {
            $amount = Money::fromPounds($value);
            $this->attributes['bank_transfer_fee'] = $amount->inPence();
        } else {
            $this->attributes['bank_transfer_fee'] = 000;
        }
    }

    /**
     * Get the Bank Transfer Fee Attribute
     *
     * @param  int $adminFee
     * @return float
     */
    public function getBankTransferFeeAttribute($adminFee)
    {
        if ($adminFee) {
            $money = Money::fromPence($adminFee);
            return $money->inPoundsAndPence();
        }
    }

    public function isVehicleConditionAvailable() : bool
    {
        return
            $this->user->profile->auto_partner == 0 &&
            !$this->user->profile->is_scrap_dealer;
    }

    public function isBuyingVehiclesFromAnotherPartners(): bool
    {
        return $this->buy_vehicles_from_other_partners;
    }

    public function getAdminFeeInPence(): int
    {
        return (int) ($this->getOriginal('admin_fee'));
    }

}
