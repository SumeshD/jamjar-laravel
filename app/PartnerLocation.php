<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PartnerLocation extends Model
{
    use LogsActivity;

    protected $guarded = [];

    public function valuation()
    {
        return $this->hasMany(Valuation::class);
    }

    public function getAddressAttribute()
    {
        return $this->address_line_one . '<br>' . $this->address_line_two . '<br>' . $this->city . '<br>' . $this->county . ' ' . '<br>' . $this->postcode;
    }

    public function getPlainAddressAttribute()
    {
        $address = '';
        $address .= $this->address_line_one;
        if ($this->address_line_two) {
            $address .= ', '.$this->address_line_two;
        }
//        if ($this->town) {
//            $address .= ', '.$this->town;
//        }
        if ($this->city) {
            $address .= ', '.$this->city;
        }
        if ($this->county) {
            $address .= ', '.$this->county;
        }
        if ($this->postcode) {
            $address .= ', '.$this->postcode;
        }
        return $address;
    }
}
