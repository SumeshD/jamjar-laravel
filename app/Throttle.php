<?php

namespace JamJar;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Throttle extends Model
{
    /**
     * @var \Illuminate\Http\Request
     */
    public $request;

    /**
     * @var int Number of attempts allowed before the user is throttled
     */
    public $attemptLimit;

    /**
     * @var int Number of weeks before throttle expires
     */
    public $expiryWeeks;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['ip', 'attempts', 'expires_at', 'created_at'];
    
    /**
     * @var array
     */
    protected $dates =  ['expires_at', 'created_at'];

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function __construct($request, $attemptLimit = 5, $expiryWeeks = 4)
    {
        $this->request = $request;
        $this->attemptLimit = $attemptLimit;
        $this->expiryWeeks = $expiryWeeks;
    }

    /**
     * Check if the IP address exists in the throttles table
     *
     * @return bool
     */
    public function check()
    {
        // if ($this->request->getClientIp() == '::1') {
        //     return true;
        // }

        if ($this->isOnWhitelist()) {
            return true;
        }

        $throttle = $this->get();

        if (is_bool($throttle)) {
            return $this->check();
        }

        if ($this->clearExpired($throttle)) {
            return true;
        }
        
        if ($throttle->attempts > $this->attemptLimit) {
            return false;
        }

        return true;
    }

    /**
     * Get or Create the Throttle Record
     *
     * @return \JamJar\Throttle
     */
    public function get()
    {
        $throttle = $this->where('ip', $this->getClientIpAddress())->first();
        if (!$throttle) {
            $throttle = $this->createThrottle();
        } else {
            $throttle = $this->hitThrottle($throttle);
        }

        return $throttle;
    }

    /**
     * Create a new Throttle Row
     *
     * @return \JamJar\Throttle
     */
    protected function createThrottle()
    {
        $this->ip = $this->getClientIpAddress();
        $this->attempts = 0;
        $this->expires_at = Carbon::now()->addWeeks($this->expiryWeeks);
        $this->created_at = Carbon::now();
        $throttle = $this->save();

        return $throttle;
    }

    /**
     * Hit the Throttle Row and increment the attempt counter
     *
     * @param  \JamJar\Throttle $throttle
     * @return \JamJar\Throttle
     */
    public function hitThrottle(Throttle $throttle)
    {
        $throttle->increment('attempts');
        $throttle->save();
        return $throttle;
    }

    /**
     * Clear Expired Throttles from the Database
     *
     * @param  \JamJar\Throttle $throttle
     * @return bool
     */
    public function clearExpired(Throttle $throttle)
    {
        $now = Carbon::now();
        if ($now->timestamp > $throttle->expires_at->timestamp) {
            $throttle->delete();
            return true;
        }
        return false;
    }

    private function getClientIpAddress(): string
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    private function isOnWhitelist(): bool
    {
        $clientIpAddresses = explode(',', $this->getClientIpAddress());

        foreach ($clientIpAddresses as $ip) {
            if ((bool) ThrottlingWhitelistIp::where('ip', '=', trim($ip))->first()) {
                return true;
            }
        }

        return false;
    }
}
