<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use JamJar\VehicleDerivative;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class VehicleModel extends Model
{
    use LogsActivity;

    protected $fillable = ['cap_model_id', 'name', 'manufacturer_id', 'introduced_date', 'discontinued_date', 'fuel_type', 'plate_years', 'internal_manufacturer_id'];

    public $casts = [
        'cap_model_id' => 'int'
    ];

    /**
     * A vehicle model has many vehicle derivatives
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function derivatives()
    {
        return $this->hasMany(VehicleDerivative::class, 'internal_model_id', 'id');
    }

    /**
     * A vehicle model belongs to a vehicle manufacturer
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manufacturer()
    {
        return $this->belongsTo(VehicleManufacturer::class, 'internal_manufacturer_id', 'id');
    }

    /**
     * Format the discontinued_date for consumption
     *
     * @param  string $discontinued_date
     * @return string
     */
    public function getDiscontinuedDateAttribute($discontinued_date)
    {
        if ($discontinued_date != 0) {
            return $discontinued_date;
        } else {
            return 'CURRENT';
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCapModelId(): ?int
    {
        return $this->cap_model_id;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the plate years attribute and unserialize it
     *
     * @param  string $plate_years
     * @return array
     */
    // public function getPlateYearsAttribute($plate_years)
    // {
    //     return json_decode($plate_years, true);
    // }

    /**
     * Get vehicle model by manufacturer and production time-frame
     *
     * @param int $manufacturerId
     * @param int $minAge
     * @param int $maxAge
     * @param string $dbVehicleType
     *
     * @return Collection
     */
    public static function getModelByManufacturerAndTimeFrame(int $manufacturerId, int $minAge, int $maxAge, string $dbVehicleType, string $fuelType = ''): Collection
    {

        $modelsQuery =  self::select('vehicle_models.*')
            ->where('manufacturer_id', $manufacturerId)
            ->join('vehicle_manufacturers', 'vehicle_manufacturers.id', '=', 'vehicle_models.internal_manufacturer_id')
            ->whereRaw('
                (vehicle_models.manufacturer_id = ' . $manufacturerId . ' and vehicle_type = \'' . $dbVehicleType . '\') and 
                (
                    (`introduced_date` <= '.$minAge.' and `discontinued_date` between '.$minAge.' and '.$maxAge.') or
                    (`introduced_date` between '.$minAge.' and '.$maxAge.' and `discontinued_date` >= '.$maxAge.') or
                    (`introduced_date` < '.$minAge.' and `discontinued_date` > '.$maxAge.') or
                    (`introduced_date` >= '.$minAge.' and `discontinued_date` <= '.$maxAge.')
                )
            ');

        if ($fuelType) {
            $modelsQuery = $modelsQuery->where('fuel_type', $fuelType);
        }

        $models = $modelsQuery
            ->orderBy('vehicle_models.name','asc')
            ->orderBy('vehicle_models.introduced_date','asc')->get();

        return $models;
    }
}
