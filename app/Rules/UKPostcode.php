<?php

namespace JamJar\Rules;

use Illuminate\Contracts\Validation\Rule;

class UKPostcode implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (preg_match_all("/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z]))))\s?[0-9][A-Za-z]{2})$/", $value, $matches) !== 0) {
            foreach ($matches as $matchesSet) {
                foreach ($matchesSet as $match) {
                    if (str_replace(' ', '', trim($match)) == str_replace(' ', '', trim($value))) {
                        return true;
                    }
                }
            }

            return false;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please enter a valid UK Postcode';
    }
}
