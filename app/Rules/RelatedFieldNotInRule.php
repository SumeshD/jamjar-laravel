<?php

namespace JamJar\Rules;

use Illuminate\Contracts\Validation\Rule;

class RelatedFieldNotInRule implements Rule
{
    /** @var array|string[] */
    private $notAcceptableValues;

    /** @var string */
    private $relatedFieldValue;

    private $attribute;

    public function __construct(string $relatedFieldValue, array $notAcceptableValues)
    {
        $this->relatedFieldValue = $relatedFieldValue;
        $this->notAcceptableValues = $notAcceptableValues;
    }

    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;

        if (!$value) {
            return true;
        }

        return !in_array($this->relatedFieldValue, $this->notAcceptableValues);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return sprintf('Please select a valid option for field %s.', $this->attribute);
    }
}
