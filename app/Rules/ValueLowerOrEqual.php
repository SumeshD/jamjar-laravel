<?php

namespace JamJar\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValueLowerOrEqual implements Rule
{
    /** @var int */
    private $maxValue;

    /** @var string */
    private $attribute;

    public function __construct(int $maxValue)
    {
        $this->maxValue = $maxValue;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;

        return $value <= $this->maxValue;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return sprintf('Field %s must be lower or equal than %s', $this->attribute, $this->maxValue);
    }
}
