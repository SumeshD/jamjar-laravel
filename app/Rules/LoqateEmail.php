<?php

namespace JamJar\Rules;

use Illuminate\Contracts\Validation\Rule;
use JamJar\Services\LoqateValidatorService;

class LoqateEmail implements Rule
{

    private $loqateService;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(LoqateValidatorService $loqateService)
    {
        $this->loqateService = $loqateService;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->loqateService->checkEmailAddress($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The email address is invalid';
    }
}
