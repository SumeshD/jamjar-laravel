<?php

namespace JamJar\Rules;

use Illuminate\Contracts\Validation\Rule;

class NonRunnerReasonsRequire implements Rule
{
    /** @var array|null */
    private $currentNonRunnerReasons;

    private $attribute;

    public function __construct(?array $currentNonRunnerReasons)
    {
        $this->currentNonRunnerReasons = $currentNonRunnerReasons;
    }

    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $isNonRunner = $value;

        if (!$isNonRunner) {
            return true;
        }

        if (!$this->currentNonRunnerReasons || count($this->currentNonRunnerReasons) <= 0) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return sprintf('Please select at least one non-runner reason');
    }
}
