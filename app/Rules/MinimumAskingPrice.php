<?php

namespace JamJar\Rules;

use Illuminate\Contracts\Validation\Rule;
use JamJar\Vehicle;

class MinimumAskingPrice implements Rule
{

    public $vehicleId;
    public $highestValuation;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($vehicleId)
    {
        $this->vehicleId = $vehicleId;

        $vehicle = Vehicle::where('id', '=', $this->vehicleId)->first();

        $this->highestValuation = $vehicle->getHighestValueInPence()/100;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {


        return $value >= $this->highestValuation;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Asking price cannot be less than your current highest offer: £' . $this->highestValuation;
    }
}
