<?php

namespace JamJar\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValueGreaterOrEqual implements Rule
{
    /** @var int */
    private $minValue;

    /** @var string */
    private $attribute;

    public function __construct(int $minValue)
    {
        $this->minValue = $minValue;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;

        return $value >= $this->minValue;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return sprintf('Field %s must be greater or equal %s', $this->attribute, $this->minValue);
    }
}
