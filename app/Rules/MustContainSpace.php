<?php

namespace JamJar\Rules;

use Illuminate\Contracts\Validation\Rule;

class MustContainSpace implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value == trim($value) && strpos($value, ' ') !== false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please enter your first and last names.';
    }
}
