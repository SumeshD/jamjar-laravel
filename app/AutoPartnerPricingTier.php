<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;

class AutoPartnerPricingTier extends Model
{
    public $fillable = ['name', 'valuation_to', 'valuation_from', 'percentage', 'user_id', 'company_id'];

    public function getPercentageAttribute($percentage)
    {
        return $percentage / 100;
    }
}
