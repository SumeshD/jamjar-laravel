<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use JamJar\PartnerPage;
use Spatie\Activitylog\Traits\LogsActivity;

class PartnerWebsite extends Model
{
    use LogsActivity;

    /**
     * The models fillable fields.
     */
    protected $fillable = ['company_id','slug','logo_image','hero_image','brand_color','bg_color'];

    /**
     * Boot the User Model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::created(
            function ($website) {
                $pages = [
                'Home',
                'Your Offer',
                'FAQ',
                'Contact',
                'Terms and Conditions',
                'Offer Complete'
                ];

                foreach ($pages as $page) {
                    PartnerPage::create(
                        [
                        'website_id' => $website->id,
                        'title' => $page,
                        'slug' => str_slug($page),
                        'content' => 'Default content for '. $page
                        ]
                    );
                }
            }
        );
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * A partner website has many pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pages()
    {
        return $this->hasMany(PartnerPage::class, 'website_id');
    }

    /**
     * A partner website belongs to a company
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    /**
     * Get a page by the slug
     *
     * @param  string $slug
     * @return JamJar\PartnerPage
     */
    public function getPage($slug)
    {
        return $this->pages()->where('slug', $slug)->firstOrFail();
    }
}
