<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use JamJar\Colour;
use JamJar\Defect;
use JamJar\Events\OfferSaved;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;

class Offer extends Model
{
    use LogsActivity;

    protected $guarded = [];
    protected $appends = ['minimum_value_int', 'maximum_value_int', 'minimum_mileage_int', 'maximum_mileage_int'];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => OfferSaved::class,
    ];

    /**
     * Get users for the offer
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Offer's Manufacturer
     *
     * @return JamJar\VehicleManufacturer
     */
    public function manufacturer()
    {
        return $this->belongsTo(VehicleManufacturer::class, 'manufacturer_id', 'cap_manufacturer_id');
    }

    /**
     * An Offer Has Many OfferModel
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function models()
    {
        return $this->hasMany(OfferModel::class);
    }

    /**
     * An Offer Has Many OfferDerivative
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function derivatives()
    {
        return $this->hasMany(OfferDerivative::class);
    }

    public function getVehicleManufacturer(): ?VehicleManufacturer
    {
        return VehicleManufacturer::where([
            ['cap_manufacturer_id', '=', $this->manufacturer_id],
            ['vehicle_type', '=', $this->vehicle_type],
        ])->first();
    }

    /**
     * Get route key name
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Set Acceptable Colours Attribute
     *
     * @param string $value
     */
    public function setAcceptableColorsAttribute($value)
    {
        $this->attributes['acceptable_colors'] = serialize($value);
    }

    /**
     * Get Acceptable Colours Attribute
     *
     * @param string $value
     */
    public function getAcceptableColorsAttribute($value)
    {
        $colorIds = unserialize($value);
        if (!$colorIds) {
            return false;
        }
        $colors = Colour::whereIn('id', $colorIds)->get();
        return $colors;
    }

    /**
     * Set Acceptable Defects Attribute
     *
     * @param string $value
     */
    public function setAcceptableDefectsAttribute($value)
    {
        $this->attributes['acceptable_defects'] = serialize($value);
    }

    /**
     * Get Acceptable Defects Attribute
     *
     * @param string $value
     */
    public function getAcceptableDefectsAttribute($value)
    {
        $defectIds = unserialize($value);
        if (!$defectIds) {
            return false;
        }
        $defects = Defect::whereIn('id', $defectIds)->get();
        return $defects;
    }

    /**
     * Get Minimum Mileage Attribute
     *
     * @param string $minimumMileage
     */
    public function getMinimumMileageAttribute($minimumMileage)
    {
        return number_format($minimumMileage);
    }

    /**
     * Get Minimum Mileage Attribute (for Vue)
     *
     * @param int $minimumMileage
     */
    public function getMinimumMileageIntAttribute()
    {
        return (int)$this->getOriginal('minimum_mileage');
    }

    /**
     * Get Maximum Mileage Attribute
     *
     * @param string $maximumMileage
     */
    public function getMaximumMileageAttribute($maximumMileage)
    {
        return number_format($maximumMileage);
    }

    /**
     * Get Maximum Mileage Attribute (for Vue)
     *
     * @param int $maximumMileage
     */
    public function getMaximumMileageIntAttribute()
    {
        return (int)$this->getOriginal('maximum_mileage');
    }

    /**
     * Get Minimum Value Attribute (for Vue)
     *
     * @param string $maximumValue
     */
    public function getMinimumValueIntAttribute()
    {
        return (int)$this->getOriginal('minimum_value');
    }


    /**
     * Get Maximum Value Attribute
     *
     * @param string $maximumValue
     */
    public function getMaximumValueAttribute($maximumValue)
    {
        return number_format($maximumValue);
    }

    /**
     * Get Maximum Value Attribute (for Vue)
     *
     * @param string $maximumValue
     */
    public function getMaximumValueIntAttribute()
    {
        return (int)$this->getOriginal('maximum_value');
    }

    /**
     * Get Model Id's Array Attribute
     *
     * @return array $collection
     */
    public function getModelIdsArrayAttribute()
    {
        $modelIds = $this->models->all('cap_model_id');
        // $modelIds = OfferModel::select('cap_model_id')->where('offer_id', $this->id)->get()->toArray();
        $collection = [];
        foreach ($modelIds as $model) {
            $collection[] = $model['cap_model_id'];
        }

        return $collection;
    }

    /**
     * Get Model Id's Array Attribute
     *
     * @return array $collection
     */
    public function getModelIdsFlippedArrayAttribute($value)
    {
        $modelIds = $this->models->pluck('cap_model_id');
        return array_flip($modelIds->toArray());
    }

    /**
     * [getDerivativeIdsArrayAttribute description]
     *
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function getDerivativeIdsArrayAttribute($value)
    {
        $derivativeIds = $this->derivatives->pluck('cap_derivative_id');
        return $derivativeIds->toArray();
    }

    public function getDerivativeIdsFlippedArrayAttribute($value)
    {
        $derivativeIds = $this->derivatives->pluck('cap_derivative_id');
        return array_flip($derivativeIds->toArray());
    }

    public function getBasePriceForDerivative($derivativeId)
    {
        return cache()->remember(
            'base-value-for-'.$derivativeId.'-in-offer-'.$this->id,
            now()->addWeeks(1),
            function () use ($derivativeId) {
                return $this->derivatives->where('cap_derivative_id', $derivativeId)->pluck('base_value')->first();
            }
        );
    }

    public function getMinimumPlateAttribute() 
    {
        return explode('-', $this->minimum_age)[1];
    }

    public function getMaximumPlateAttribute() 
    {
        return explode('-', $this->maximum_age)[1];
    }

    public function getVehicleType(): string
    {
        return $this->vehicle_type;
    }

    public function getCapManufacturerId(): int
    {
        return $this->manufacturer_id;
    }

    /**
     * Get Model ID's Attribute
     *
     * @param string $value
     */
    // public function getModelIdsAttribute($value)
    // {
    //     return unserialize($value);
    // }

    /**
     * Get Model Id's Array Attribute
     *
     * @return array $collection
     */
    // public function getModelIdsArrayAttribute($value)
    // {
    //     $collection = [];
    //     foreach ($this->model_ids as $model) {
    //         $collection[] = $model['cap_model_id'];
    //     }
    //     return $collection;
    // }

    /**
     * Get Derivative ID's Attribute
     *
     * @param string $value
     */
    // public function getDerivativeIdsAttribute($value)
    // {
    //     return unserialize($value);
    // }

    /**
     * [getDerivativeIdsArrayAttribute description]
     *
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    // public function getDerivativeIdsArrayAttribute($value)
    // {
    //     $collection = [];
    //     foreach ($this->derivative_ids as $derivative) {
    //         $collection[] = $derivative['cap_derivative_id'];
    //     }

    //     return $collection;
    // }

    /**
     * Get the Offer Models
     *
     * @return JamJar\VehicleModel
     */
    // public function models()
    // {
    //     $modelIds = [];
    //     foreach ($this->model_ids as $modelId) {
    //         $modelIds[] .= $modelId['cap_model_id'];
    //     }

    //     if ($modelIds) {
    //         $models = VehicleModel::whereIn('cap_model_id', $modelIds)
    //                                 ->with(['manufacturer','derivatives'])
    //                                 ->get()
    //                                 ->keyBy('cap_model_id');
                                    
    //         return $models;
    //     }
    // }

    /**
     * Get the derivatives count
     *
     * @param int $modelId
     */
    // public function derivativesCount($modelId)
    // {
    //     $derivatives = $this->derivative_ids;

    //     $collection = [];

    //     foreach ($derivatives as $derivative) {
    //         if ($derivative != null) {
    //             $collection[$derivative['cap_model_id']][] = $derivative;
    //         }
    //     }


    //     if (isset($collection[$modelId])) {
    //         return count($collection[$modelId]);
    //     } else {
    //         return 0;
    //     }
    // }

    /**
     * Get derivatives for the model
     *
     * @param  int $modelId
     * @return array $collection
     */
    // public function derivatives($modelId)
    // {
    //     $derivatives = VehicleDerivative::select(['name', 'model_id', 'cap_derivative_id'])
    //                                     ->whereIn('cap_derivative_id', $this->derivative_ids)
    //                                     ->with('model')
    //                                     ->get()
    //                                     ->keyBy('cap_derivative_id');
                                        
    //     $collection = collect();

    //     foreach ($derivatives as $derivative) {
    //         if ($derivative->model_id != $modelId) {
    //             continue;
    //         }

    //         $derivative->name = $derivative->model->name . ' - ' . $derivative->name . ' ('.$derivative->model->introduced_date.' - '.$derivative->model->discontinued_date.')';
    //         // $derivative->base_value = $deriv['base_value'];
    //         $collection->push($derivative);
    //     }

    //     return $collection;
    // }

    /**
    * Get derivatives for a specific model
    *
    * @param  int $modelId
    * @return array $collection
    */
    // public function getDerivativesForModel($modelId)
    // {
    //     $derivatives = VehicleDerivative::where('model_id', $modelId)->get();

    //     $collection = collect();

    //     $derivatives->each(function ($derivative) use ($collection) {
    //         foreach ($this->derivative_ids as $selectedDerivative) {
    //             if ($selectedDerivative['cap_derivative_id'] == $derivative->cap_derivative_id) {
    //                 return $collection->push([
    //                     'derivative' => $derivative->toArray(),
    //                     'base_value' => $selectedDerivative['base_value'],
    //                     'suggested_value' => $derivative->suggestedValue
    //                 ]);
    //             } else {
    //                 return $collection->push([
    //                     'derivative' => $derivative->toArray(),
    //                     'base_value' => $this->base_value,
    //                     'suggested_value' => $derivative->suggestedValue
    //                 ]);
    //             }
    //         }
    //     });

    //     // dump($collection);

    //     return $collection;
    // }

    /**
    * Get base price for a specific derivative
    *
    * @param  int $derivativeId
    * @return int $base_value
    */
    // public function getBasePriceForDerivative($derivativeId)
    // {
    //     $haystack = $this->derivative_ids;
    //     $needle = $derivativeId;

    //     foreach ($haystack as $derivative) {
    //         if ($derivative['cap_derivative_id'] == $needle) {
    //             return $derivative['base_value'];
    //         }
    //     }
    // }

    // public function getVehiclesAttribute()
    // {
    //     return Cache::remember('vehicles-for-'.$this->slug, now()->addMonths(1), function () {
    //         // create derivativeIds haystack
    //         $derivativeIds = [];

    //         // create modelIds haystack
    //         $modelIds = [];
    //         if ($this->derivative_ids) {
    //             // populate above haystacks
    //             foreach ($this->derivative_ids as $data) {
    //                 $modelIds[] = $data['cap_model_id'];
    //                 $derivativeIds[] = $data['cap_derivative_id'];
    //             }

    //             // get all models for this offer
    //             $models = $this->models();

    //             // loop through the models and lazy load the associated derivatives
    //             $models = $models->values()->reject(function ($model) use ($modelIds) {
    //                 // reject models which aren't in our haystack
    //                 // dump('rejected model: '. $model->cap_model_id);
    //                 return !in_array($model->cap_model_id, $modelIds);
    //             })->map(function ($model) use ($derivativeIds) {
    //                 // reject derivatives which aren't in our haystack
    //                 $derivatives = $model->derivatives->reject(function ($derivative) use ($derivativeIds) {
    //                     // dump('rejected deriv: '. $derivative->cap_derivative_id);
    //                     return !in_array($derivative->cap_derivative_id, $derivativeIds);
    //                 });

    //                 // insert our derivatives base value
    //                 $derivatives = $derivatives->values()->map(function($derivative) {
    //                     // cast to array for writing
    //                     $suggested = $derivative->suggested_value;
    //                     $derivative = $derivative->toArray();
    //                     // get the base price for the deriv
    //                     $derivative['base_value'] = $this->getBasePriceForDerivative($derivative['cap_derivative_id']);
    //                     $derivative['suggested_value'] = $suggested;
    //                     return $derivative;
    //                 });

    //                 // cast the model to an array
    //                 $model = $model->toArray();
    //                 // in order to override the derivatives
    //                 $model['derivatives'] = $derivatives;

    //                 // return it
    //                 return $model;
    //             });

    //             // return our now filtered collection
    //             return $models;
    //         }
    //     });
    // }

    public function getSuggestedValuesCountForOffer(){
        $sql = 'SELECT
            count(od.cap_derivative_id) as derivative_count
        FROM offer_models om
        LEFT JOIN offer_derivatives od ON (om.offer_id = od.offer_id AND om.cap_model_id = od.cap_model_id)
        LEFT join suggested_values on (suggested_values.cap_derivative_id = od.cap_derivative_id and suggested_values.cap_model_id = om.cap_model_id )
        LEFT JOIN offers ON om.offer_id = offers.id
        LEFT JOIN vehicle_models ON (om.cap_model_id = vehicle_models.cap_model_id AND offers.manufacturer_id = vehicle_models.manufacturer_id)
        WHERE om.offer_id =:offerId AND finished = 1 and active = 1 and od.base_value < suggested_values.percentage and offers.accept_write_offs = 0 and offers.acceptable_defects = \'a:0:{}\'';

        return DB::select(DB::raw($sql), [
            'offerId' => $this->id
        ]);
    }
}
