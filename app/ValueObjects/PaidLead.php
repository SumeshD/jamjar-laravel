<?php

namespace JamJar\ValueObjects;

use JamJar\Valuation;
use JamJar\Vehicle;

class PaidLead
{
    /** @var Vehicle */
    public $vehicle;

    /** @var Valuation|null */
    public $highestValuation;

    /** @var Valuation|null */
    public $partnerValuation;

    /** @var int */
    public $additionalFee = 0;

    public function __construct(Vehicle $vehicle, Valuation $highestValuation = null, Valuation $partnerValuation = null)
    {
        $this->vehicle = $vehicle;
        $this->highestValuation = $highestValuation;
        $this->partnerValuation = $partnerValuation;
    }
}
