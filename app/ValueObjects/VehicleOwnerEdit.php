<?php

namespace JamJar\ValueObjects;

use Illuminate\Http\Request;

class VehicleOwnerEdit
{
    /** @var string */
    public $name;

    /** @var string */
    public $postcode;

    /** @var string */
    public $telephone;

    public static function createFromRequest(Request $request): self
    {
        $vehicleOwnerEdit = new self();

        $vehicleOwnerEdit->name = $request->get('name');
        $vehicleOwnerEdit->postcode = $request->get('postcode');
        $vehicleOwnerEdit->telephone = $request->get('telephone');

        return $vehicleOwnerEdit;
    }

    private function __construct()
    {

    }
}