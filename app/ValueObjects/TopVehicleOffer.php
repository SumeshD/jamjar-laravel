<?php

namespace JamJar\ValueObjects;

use JamJar\Valuation;
use JamJar\Vehicle;

class TopVehicleOffer
{
    /** @var Vehicle */
    public $vehicle;

    /** @var Valuation|null */
    public $highestValuation;

    public function __construct(Vehicle $vehicle, Valuation $highestValuation = null)
    {
        $this->vehicle = $vehicle;
        $this->highestValuation = $highestValuation;
    }
}