<?php

namespace JamJar\ValueObjects;

use Illuminate\Http\Request;

class VehicleMetaEdit
{
    /** @var int */
    public $mileage;

    /** @var string */
    public $previousOwners;

    /** @var string */
    public $serviceHistory;

    /** @var string */
    public $mot;

    /** @var int */
    public $carColourId;

    /** @var bool */
    public $isWriteOff;

    /** @var string */
    public $writeOffCategory;

    /** @var bool */
    public $isNonRunner;

    /** @var array|int[] */
    public $nonRunnerReasonIds = [];

    public static function createFromRequest(Request $request): self
    {
        $vehicleMetaEdit = new self();

        $vehicleMetaEdit->mileage = (int) $request->get('mileage');
        $vehicleMetaEdit->previousOwners = $request->get('previous-owners');
        $vehicleMetaEdit->serviceHistory = $request->get('service-history');
        $vehicleMetaEdit->mot = $request->get('mot');
        $vehicleMetaEdit->carColourId = (int) $request->get('car-colour');
        $vehicleMetaEdit->isWriteOff = (bool) $request->get('write-off');
        if ($vehicleMetaEdit->isWriteOff) {
            $vehicleMetaEdit->writeOffCategory = $request->get('write-off-category');
        }
        $vehicleMetaEdit->isNonRunner = (bool) $request->get('non-runner');
        if ($vehicleMetaEdit->isNonRunner) {
            foreach ($request->get('non-runner-reason') as $reasonId => $flag) {
                if ($flag == 'on') {
                    $vehicleMetaEdit->nonRunnerReasonIds[] = (int) $reasonId;
                }
            }
        }

        return $vehicleMetaEdit;
    }

    private function __construct()
    {

    }
}