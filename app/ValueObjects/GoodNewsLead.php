<?php

namespace JamJar\ValueObjects;

use JamJar\Valuation;
use JamJar\Vehicle;

class GoodNewsLead
{
    /** @var Vehicle */
    public $vehicle;

    /** @var Valuation|null */
    public $highestValuation;

    /** @var Valuation|null */
    public $highestExpiredValuation;

    /** @var Valuation|null */
    public $highestNonExpiredValuation;

    /** @var int */
    public $fee = 0;

    public function __construct(
        Vehicle $vehicle,
        Valuation $highestValuation = null,
        Valuation $highestExpiredValuation = null,
        Valuation $highestNonExpiredValuation = null
    ) {
        $this->vehicle = $vehicle;
        $this->highestValuation = $highestValuation;
        $this->highestExpiredValuation = $highestExpiredValuation;
        $this->highestNonExpiredValuation = $highestNonExpiredValuation;
    }
}