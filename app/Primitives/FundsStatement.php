<?php

namespace JamJar\Primitives;

use Carbon\Carbon;
use JamJar\UserStatement;

class FundsStatement
{
    /** @var Carbon */
    public $createdAt;

    /** @var string */
    public $activity;

    /** @var int|null */
    public $valueInPence;

    /** @var string */
    public $listingPosition;

    /** @var int */
    public $fundsAmountInPence;

    private function __construct()
    {

    }

    /**
     * @param array|UserStatement $item
     * @return FundsStatement
     */
    public static function createFromRawData($item)
    {
        $statement = new FundsStatement();
        $statement->createdAt = $item->created_at;

        if ($item['type'] == 'withdrawal') {
            $name = [$item->vehicle['manufacturer'], $item->vehicle['model'], $item->vehicle['derivative']];
            $statement->activity =  implode(' ', $name);
        } else if ($item['type'] =='deposit') {
            $statement->activity = 'Funds Added';
        }

        if ($item['type'] == 'withdrawal') {
            if (isset($item['valuation']['dropoff'])) {
                $statement->valueInPence = $item['valuation']['dropoff'] * 100;
                $statement->listingPosition = $item->position;
            } else {
                if (isset($item['valuation']['collection'])) {
                    if ($item['valuation']['collection'] != '&pound;0') {
                        $statement->valueInPence = (int) $item['valuation']['collection'] * 100;
                    }
                } else {
                    if (isset($item->meta['valuation']['collection_value'])) {
                        $statement->valueInPence = (int) str_replace(
                            ['&pound;', ',', '.'],
                            ['','',''],
                            $item->meta['valuation']['collection_value']
                        ) * 100;
                    }
                    $statement->listingPosition = 'Marketplace';
                }
            }
        }

        if ($item['type'] == 'withdrawal') {
            $statement->fundsAmountInPence = $item->amount * -100;
        } else if ($item['type'] == 'deposit') {
            $statement->fundsAmountInPence = $item->amount * 100;
        }

        $valuation = ($item->meta && isset($item->meta['valuation']) && isset($item->meta['valuation']['id'])) ?
            $item->meta['valuation'] : null;

        if ($item->getTransactionLabel() !== null) {
            $statement->listingPosition = $item->getTransactionLabel();
        } else if (
            $valuation &&
            isset($valuation['is_created_by_automated_bids']) &&
            $valuation['is_created_by_automated_bids'] == true &&
            isset($valuation['position'])
        )
        {
            $statement->listingPosition = (int) $item->meta['valuation']['position'];
        }

        return $statement;
    }
}