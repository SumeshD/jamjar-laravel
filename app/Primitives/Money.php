<?php
namespace JamJar\Primitives;

class Money
{
    private $pence;

    private function __construct($pence)
    {
        $this->pence = (integer) $pence;
    }

    /**
     * Convert the integer from pounds
     *
     * @param  float $pounds
     * @return JamJar\Primitives\Money
     */
    public static function fromPounds(float $pounds)
    {
        return new static($pounds * 100);
    }

    /**
     * Convert the integer from pence
     *
     * @param  int $pence
     * @return JamJar\Primitives\Money
     */
    public static function fromPence(int $pence)
    {
        return new static($pence);
    }

    /**
     * Get the integer in pence
     *
     * @return int
     */
    public function inPence()
    {
        return (int) $this->pence;
    }

    /**
     * Get the integer in pounds
     *
     * @return float
     */
    public function inPounds()
    {
        return (float) ($this->pence / 100);
    }

    /**
     * Get the integer in pence and pence
     *
     * @return string
     */
    public function inPoundsAndPence()
    {
        return number_format($this->pence / 100, 2);
    }
}
