<?php
namespace JamJar\Primitives;

use JamJar\Company;
use JamJar\PricingTier;
use JamJar\Setting;
use JamJar\UserStatement;
use JamJar\Valuation;
use JamJar\Vehicle;

class PartnerFeeCalculator
{

    const MARKETPLACE_FEE_4_LESS_PHOTOS = 'marketplace-fee';
    /**
     * @var int position of the lead in the listing
     */
    public $position;
    /**
     * @var int (dropoff) valuation of the lead
     */
    public $valuation;

    public $vehicleId;

    /**
     * Instantiate a new instance of PartnerFeeCalculator
     *
     * @param  int $position
     * @param  int $valuation
     * @return void
     */
    public function __construct($position, $valuation, $vehicleId = null)
    {
        $this->position = $position;
        $this->valuation = $valuation;
        $this->vehicleId = $vehicleId;
    }

    public static function createStatementEntry(Valuation $valuation, Company $company, $vehicle, int $feeInPence): UserStatement
    {
        $statement = new UserStatement();

        $meta = json_encode([
            'valuation' => Valuation::where('id', '=', $valuation->id)->first(),
            'vehicle' => $vehicle->load('meta'),
            'position' => $valuation->position
        ]);

        $statement = $statement->create([
            'user_id' => $company->user->id,
            'type' => 'withdrawal',
            'amount' => $feeInPence ?? 0,
            'meta' => $meta
        ]);

        return $statement;
    }

    public static function createStatementEntryForSeller(Company $company, $vehicle, int $feeInPence): UserStatement
    {
        $statement = new UserStatement();

        $meta = json_encode([
            'vehicle' => $vehicle->load('meta')
        ]);

        $statement = $statement->create([
            'user_id' => $company->user->id,
            'type' => 'withdrawal',
            'amount' => $feeInPence ?? 0,
            'meta' => $meta
        ]);

        return $statement;
    }

    public static function getVATMultiplier()
    {
        $vatFeePercentage = env('VAT_FEE_IN_PERCENT') !== null ? env('VAT_FEE_IN_PERCENT') : 20;

        return round($vatFeePercentage / 100, 2) + 1;
    }

    /**
     * Get all Pricing Tiers from Database
     *
     * @return array
     */
    private function getTiers()
    {
        $pricingTiers = cache()->remember('pricing-tiers', now()->addWeeks(1), function() {
            return PricingTier::all();
        });
        
        if ($pricingTiers->isEmpty()) {
            return false;
        }

        $tiers = [];
        foreach ($pricingTiers as $tier) {
            $tiers[] = [
                'from' => $tier->valuation_from,
                'to' => $tier->valuation_to,
                'fee' => $tier->fee
            ];
        }
        return $tiers;
    }

    /**
     * Get the position percentage cost from the database
     *
     * @return int
     */
    private function getPositionCost()
    {
        if($this->position == self::MARKETPLACE_FEE_4_LESS_PHOTOS){
            return $positionCost = 100;
        }
        return (int)Setting::get('position_cost_'.$this->position);
    }

    /**
     * Find the matching pricing tier
     *
     * @return int
     */
    private function getMatchingPricingTier()
    {
        $matched = null;
        foreach ($this->getTiers() as $key => $tier) {
            if ($this->valuation >= $tier['from']) {
                $matched = $key;
            } else {
                continue;
            }
        }
        return $matched;
    }

    /**
     * Calculate the Balance for use in our percentage later.
     *
     * @param  array $tiers
     * @param  int   $tier
     * @return int
     */
    private function calculateBalance($tiers, $tier)
    {
        $balance = $this->valuation - $tiers[$tier]['from'];
        if ($balance == 0) {
            $this->valuation = $this->valuation + 1;
            $balance = $this->valuation - $tiers[$tier]['from'];
        }
        return $balance;
    }

    /**
     * Calculate the Range for use in our percentage later.
     *
     * @param  array $tiers
     * @param  int   $tier
     * @return int
     */
    private function calculateRange($tiers, $tier)
    {
        return $tiers[$tier]['to'] - $tiers[$tier]['from'];
    }
    
    /**
     * Calculate the percentage to deduct from final total to form our fee
     *
     * @param  int $range
     * @param  int $balance
     * @return float
     */
    private function calculatePercentage($range, $balance)
    {
        return (100 / ($range / $balance));
    }

    /**
     * Use all the previous maths to calculate our final fee
     *
     * @param  array $tiers
     * @param  int   $tier
     * @param  int   $percentage
     * @param  int   $positionCost
     * @return float
     */
    private function calculateFee($tiers, $tier, $percentage, $positionCost)
    {
        $previousFee = isset($tiers[$tier - 1]['fee']) ? $tiers[$tier - 1]['fee'] : 0;
        $modifier = ((($tiers[$tier]['fee'] - $previousFee) / 100) * $percentage);
        $fee = $previousFee + $modifier;
        $fee = round($fee, 2);
        $fee = (($fee / 100) * $positionCost);
        return (int) ($fee * 100) / 100;
    }

    public function calculateForGoodNewsLead($inPence = false, Company $buyerCompany = null, bool $includingVAT = true)
    {
        $vatFeeMultiplier = $includingVAT ? self::getVATMultiplier() : 1;

        if ($buyerCompany && $buyerCompany->user->profile->use_flat_fee == 1) {
            $feeInPence = (int) round($buyerCompany->user->profile->getOriginal('flat_fee') * $vatFeeMultiplier);

            return $inPence ? $feeInPence : ($feeInPence / 100);
        }

        $tiers = $this->getTiers();
        $tier = $this->getMatchingPricingTier();

        if (!$tiers || $tier === null) {
            return 0;
        }

        $positionCost = 100;
        $balance = $this->calculateBalance($tiers, $tier);
        $range = $this->calculateRange($tiers, $tier);
        $percentage = $this->calculatePercentage($range, $balance);
        $fee = $this->calculateFee($tiers, $tier, $percentage, $positionCost) * $vatFeeMultiplier;
        $marketplacePercentage = (int) Setting::get('good_news_lead_percentage_modifier');
        if($this->vehicleId) {
            $vehicle = Vehicle::where('id', $this->vehicleId)->first();
            if(count($vehicle->getImages()) >= 4) {
                //Vehicle with 4+ images
                $marketplacePercentage = (int) Setting::get('marketplace_with_4_plus_photos');
            }
        }
        $fee = (($marketplacePercentage / 100) + 1) * $fee;

        $fee = round($fee, 2);

        if ($inPence == true) {
            $fee = (int) ($fee * 100);
        }

        return $fee;
    }

    /**
     * Calculate the fee for the given Valuation and Position
     *
     * @return float
     */
    public function calculate($inPence = false, Company $buyerCompany = null, $includingVAT = true)
    {
        $vatFeeMultiplier = $includingVAT ? self::getVATMultiplier() : 1;

        if ($buyerCompany && $buyerCompany->user->profile->use_flat_fee == 1) {
            $feeInPence = (int) round($buyerCompany->user->profile->getOriginal('flat_fee') * $vatFeeMultiplier);

            return $inPence ? $feeInPence : $feeInPence / 100;
        }

        $tiers = $this->getTiers();
        $tier = $this->getMatchingPricingTier();
        if (!$tiers || $tier === null) {
            return 0;
        }

        $positionCost = $this->getPositionCost();
        $balance = $this->calculateBalance($tiers, $tier);
        $range = $this->calculateRange($tiers, $tier);
        $percentage = $this->calculatePercentage($range, $balance);
        $fee = $this->calculateFee($tiers, $tier, $percentage, $positionCost) * $vatFeeMultiplier;
        if($this->position == self::MARKETPLACE_FEE_4_LESS_PHOTOS) {
            $marketplacePercentage = (int) Setting::get('good_news_lead_percentage_modifier');
            $fee = (($marketplacePercentage / 100) + 1) * $fee;
        }

        $fee = round($fee, 2);

        if ($inPence == true) {
            $fee = (int) ($fee * 100);
        }

        return $fee;
    }

    public function getFeeForSeller(bool $inPence = false, Company $company = null, bool $includingVAT = true)
    {
        return $this->calculateForGoodNewsLead($inPence, $company, $includingVAT);
    }

}
