<?php

namespace JamJar\Providers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use JamJar\Services\EloquentUploadedImageManager;
use JamJar\Services\ImagesManagement\AWSImagesManager;
use JamJar\Services\ImagesManagement\FileImagesManager;
use JamJar\Services\ImagesManagement\ImagesManagerInterface;
use JamJar\Services\ImagesManagement\InMemoryImagesManager;
use JamJar\Services\Valuations\ExternalDataProviders\CTBDataProvider;
use JamJar\Services\Valuations\ExternalDataProviders\M4YMDataProvider;
use JamJar\Services\Valuations\ExternalDataProviders\WBCTDataProvider;
use JamJar\Services\Valuations\ExternalDataProviders\WWACDataProvider;
use JamJar\Services\Valuations\InternalDataProviders\AutoPartnerDataProvider;
use JamJar\Services\Valuations\InternalDataProviders\RulesDataProvider;
use JamJar\Services\Valuations\VehiclePriceEstimators\CapPriceEstimator;
use JamJar\Services\Valuations\VehicleValuationsCollector;
use JamJar\Services\Valuations\VehicleValuers\VehicleValuer;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \URL::forceScheme('https');
        Schema::defaultStringLength(191);
        
        if (app('env') == 'production') {
            $monolog = \Log::getMonolog();
            $slackHandler = new \Monolog\Handler\SlackHandler('xoxp-7821490135-13693875873-265284469874-8ab84b3e82fd558bed512bcbb88c65f4', '#jj-logs', 'JamJar Logs', true, null, \Monolog\Logger::ERROR);
            $monolog->pushHandler($slackHandler);
        }

        Blade::directive(
            'dump',
            function ($expression) {
                return "<?php dump($expression); ?>";
            }
        );

        if (!Collection::hasMacro('paginate')) {
            Collection::macro(
                'paginate',
                function ($perPage = 15, $page = null, $options = []) {
                    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
                    return (
                    new LengthAwarePaginator(
                        $this->forPage($page, $perPage),
                        $this->count(),
                        $perPage,
                        $page,
                        $options
                    )
                    )->withPath('');
                }
            );
        }

        Validator::extend(
            'recaptcha',
            'JamJar\\Rules\\ReCaptcha@validate'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (in_array(env('APP_ENV'), ['testing'])) {
            $this->app->bind('JamJar\Api\CapApiInterface', 'JamJar\Api\FakeCapApi');
            $this->app->bind('GuzzleHttp\ClientInterface', 'JamJar\Api\FakeHttpClient');
        } else {
            $this->app->bind('JamJar\Api\CapApiInterface', 'JamJar\Api\CapApi');
            $this->app->bind('GuzzleHttp\ClientInterface', 'GuzzleHttp\Client');
        }

        if (in_array(env('APP_ENV'), ['testing'])) {
            $this->app->singleton(ImagesManagerInterface::class, InMemoryImagesManager::class);
        } else {
            $this->app->bind(ImagesManagerInterface::class, AWSImagesManager::class);
        }

        $this->app->bind(EloquentUploadedImageManager::class, EloquentUploadedImageManager::class);

        $this->app->bind(VehicleValuationsCollector::class, function() {
            return new VehicleValuationsCollector([new CapPriceEstimator()], [
                new VehicleValuer(new CTBDataProvider()),
                new VehicleValuer(new M4YMDataProvider()),
                new VehicleValuer(new WBCTDataProvider()),
                new VehicleValuer(new WWACDataProvider()),
                new VehicleValuer(new RulesDataProvider()),
                new VehicleValuer(new AutoPartnerDataProvider()),
            ]);
        });
    }
}
