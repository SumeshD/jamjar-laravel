<?php

namespace JamJar\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'JamJar\Events\VehicleCreated' => [
            'JamJar\Listeners\RegisterVehicleOwner',
            // 'JamJar\Listeners\SendVehicleOwnerPassword',
        ],
        'JamJar\Events\PartnerCreated' => [
            'JamJar\Listeners\SendAdminPartnerEmail',
            'JamJar\Listeners\SendPartnerWelcomeEmail',
            'JamJar\Listeners\CreatePartnerCompany',
            'JamJar\Listeners\CreatePartnerWebsite',
            'JamJar\Listeners\CreatePartnerLocation',
            'JamJar\Listeners\CreatePartnerFunds',
        ],
        'JamJar\Events\PartnerApproved' => [
            'JamJar\Listeners\CreatePartner',
            'JamJar\Listeners\SendPartnerApprovedEmail',
        ],
        'JamJar\Events\PartnerDenied' => [
            'JamJar\Listeners\PartnerCleanUp',
        ],
        'JamJar\Events\OrderReceived' => [
            'JamJar\Listeners\SendOrderInvoiceEmail',
            'JamJar\Listeners\NotifyAdminOrderReceived'
        ],
        'JamJar\Events\ValuationCreated' => [
            'JamJar\Listeners\CreateValuations',
            'JamJar\Listeners\ChargePartner'
        ],
        'JamJar\Events\SaleCreated' => [
            'JamJar\Listeners\SaleCreatedListener',
        ],
        'JamJar\Events\OfferUpdated' => [
            'JamJar\Listeners\SaveOfferVehicles'
        ],
        'JamJar\Events\OfferSaved' => [
            'JamJar\Listeners\PrimeOfferCache'
        ],
        'JamJar\Events\NewValuationOffered' => [
            'JamJar\Listeners\SendUserValuationByEmail',
            'JamJar\Listeners\SendUserValuationBySms',
            'JamJar\Listeners\ChargePartnerForGoodNewsLead',
        ],
        'JamJar\Events\SaleHasBeenRemoved' => [
            'JamJar\Listeners\SendEmailAboutRemovedSaleToPartner',
        ],
        'JamJar\Events\VehicleAddedToMarketplace' => [
            'JamJar\Listeners\SendInstantMarketplaceAlerts',
        ],
        'JamJar\Events\AdditionalVehicleInformationHasBeenProvidedByClient' => [
            'JamJar\Listeners\SendEmailAboutAdditionalVehicleInformationToPartners',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
