<?php

namespace JamJar\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            alert()->error("Sorry, this vehicle has now been sold");
            return redirect()->route('/');
        }

        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
            return redirect()
                    ->back()
                    ->withInput($request->except('password'))
                    ->with([
                            'message' => 'Validation Token has expired. Please try again',
                            'message-type' => 'danger'
                    ]);
        }

        if ($exception instanceof VehicleExpiredException) {
            return redirect()->route('/');
        }

        if ($exception instanceof InsufficientFundsException) {
            return redirect()->route('partnerCredits');
        }

        if ($exception instanceof NotAllowedException) {
            return redirect()->route('partnerDashboard');
        }

        if ($exception instanceof ApiResponseException) {
            alert()->error('Sorry, we have encountered a problem with an external vehicle information provider\'s system. Please try again later or contact us.');
            return redirect()->route('/');
        }

        if ($exception instanceof VehicleDoesNotExistException) {
            alert()->error('Vehicle does not exist');
            return redirect()->route('dashboard');
        }

        if ($exception instanceof VehicleHasCompletedSalesException) {
            alert()->error('This vehicle has complete sale');
            return redirect()->route('dashboard');
        }


        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request                 $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
