<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use JamJar\Primitives\Money;

class UserStatement extends Model
{
    public $guarded = [];

    public function getMetaAttribute($meta)
    {
        return json_decode($meta, true);
    }

    public function getVehicleAttribute()
    {
        if (isset($this->meta['vehicle']['meta'])) {
            return array_merge($this->meta['vehicle'], $this->meta['vehicle']['meta']);
        }
        return $this->meta['vehicle'];
    }

    public function getValuationAttribute()
    {
        if (array_key_exists('valuation', $this->meta)) {
            return $this->meta['valuation'];
        }
    }

    public function getPositionAttribute()
    {
        if (array_key_exists('position', $this->meta)) {
            return $this->meta['position'];
        }
    }

    public function getAmountAttribute($amount)
    {
        $money = Money::fromPence($amount);
        return $money->inPoundsAndPence();
    }

    public function setTransactionLabel(string $string): self
    {
        $this->transaction_label = $string;

        return $this;
    }

    public function getTransactionLabel(): ?string
    {
        return $this->transaction_label;
    }
}
