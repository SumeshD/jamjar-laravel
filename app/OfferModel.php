<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OfferModel extends Model
{
    public $fillable = ['cap_model_id', 'base_value'];
    public $with = ['model'];

    /**
     * An OfferModel Belongs To an Offer
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }
    
    /**
     * An OfferModel Belongs To a VehicleModel
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(VehicleModel::class, 'cap_model_id', 'cap_model_id');
    }

    public function getVehicleModel(): ?VehicleModel
    {
        return VehicleModel::select('vehicle_models.*')
            ->where([
                ['vehicle_models.cap_model_id', '=', $this->cap_model_id],
                ['vehicle_manufacturers.vehicle_type', '=', $this->offer->vehicle_type],
            ])
            ->join('vehicle_manufacturers', 'vehicle_manufacturers.id', '=', 'vehicle_models.internal_manufacturer_id')
            ->first();
    }

    /**
     * Get the derivatives of the current model.
     *
     * @return JamJar\OfferDerivative
     */
    public function getDerivativesAttribute()
    {
        return cache()->remember(
            'derivatives-for-model-'.$this->cap_model_id.'-and-user-'.auth()->id().'-and-offer-'.$this->offer->id,
            now()->addMonths(1),
            function () {
                return OfferDerivative::where('cap_model_id', $this->cap_model_id)->where('offer_id', $this->offer->id)->get();
            }
        );
    }

    public function getModelIdsAndDerivativesCountForOffer(){
        $sql = 'SELECT
            om.cap_model_id as cap_model_id,
            vehicle_models.name,
            count(od.cap_derivative_id) as derivative_count
        FROM offer_models om
        LEFT JOIN offer_derivatives od ON (om.offer_id = od.offer_id AND om.cap_model_id = od.cap_model_id)
        LEFT join suggested_values on (suggested_values.cap_derivative_id = od.cap_derivative_id and suggested_values.cap_model_id = om.cap_model_id)
        LEFT JOIN offers ON om.offer_id = offers.id
        LEFT JOIN vehicle_models ON (om.cap_model_id = vehicle_models.cap_model_id AND offers.manufacturer_id = vehicle_models.manufacturer_id)
        WHERE om.offer_id =:offerId AND finished = 1 and active = 1 and od.base_value < suggested_values.percentage and offers.accept_write_offs = 0 and offers.acceptable_defects = \'a:0:{}\'
        GROUP BY cap_model_id
        ORDER BY vehicle_models.name;';

        return DB::select(DB::raw($sql), [
            'offerId' => $this->offer->id
        ]);
    }

    public function getSuggestedValuesForOfferAndModel($modelId){
        $sql = 'SELECT
            offer_derivatives.id as derivative_id,
            offer_derivatives.cap_derivative_id as cap_derivative_id,
            vd.name as derivative_name,
            offer_derivatives.base_value as base_value,
            suggested_values.percentage as suggested_value_percentage
            FROM offer_derivatives
            LEFT JOIN offers ON offer_derivatives.offer_id = offers.id
            LEFT JOIN offer_models ON (offer_models.offer_id = offers.id)
            LEFT JOIN suggested_values ON (suggested_values.cap_model_id = offer_models.cap_model_id AND suggested_values.cap_derivative_id=offer_derivatives.cap_derivative_id)
            LEFT JOIN vehicle_derivatives vd on offer_derivatives.cap_derivative_id = vd.cap_derivative_id AND offer_models.cap_model_id=vd.model_id
            WHERE offer_derivatives.offer_id =:offerId AND offer_derivatives.cap_model_id =:modelId AND finished = 1 and active = 1 and offer_derivatives.base_value < suggested_values.percentage and offers.accept_write_offs = 0 and offers.acceptable_defects = \'a:0:{}\'';

        return DB::select(DB::raw($sql), [
            'offerId' => $this->offer->id,
            'modelId' => $modelId
        ]);
    }
}
