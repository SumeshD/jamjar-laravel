<?php

namespace JamJar\Model\MarketplaceAlerts;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use JamJar\Company;
use JamJar\PartnerLocation;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Sale;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\OfferedValues\OfferedValueService;
use JamJar\Valuation;
use JamJar\Vehicle;
use JamJar\VehicleModel;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class MarketplaceAlertsFilter extends Model
{
    use LogsActivity;

    public $guarded = [];

    public function marketplaceAlertsFiltersManufacturers()
    {
        return $this->hasMany(MarketplaceAlertsFiltersManufacturer::class);
    }

    public function marketplaceAlertsFiltersModels()
    {
        return $this->hasMany(MarketplaceAlertsFiltersModel::class);
    }

    /** @return MarketplaceAlertsFiltersManufacturer[] */
    public function getMarketplaceAlertsFiltersManufacturers()
    {
        return $this->marketplaceAlertsFiltersManufacturers;
    }

    /** @return MarketplaceAlertsFiltersModel[] */
    public function getMarketplaceAlertsFiltersModels()
    {
        return $this->marketplaceAlertsFiltersModels;
    }

    public function setMarketplaceAlert(MarketplaceAlert $alert): self
    {
        $this->marketplace_alert_id = $alert->getId();

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /** @return array|VehicleModel[] */
    public function getUniqueModels()
    {

        $modelsIds = [];
        foreach ($this->getMarketplaceAlertsFiltersModels() as $model) {
            $modelsIds[] = $model->getModelId();
        }

        if (!$modelsIds) {
            return [];
        }

        $query = VehicleModel::whereRaw('id IN (' . implode(',', $modelsIds) . ')')->groupBy('name');

        return $query->get();
    }

    public function getManufacturerName(): string
    {
        if (count($this->getMarketplaceAlertsFiltersManufacturers()) > 0){
            return $this->getMarketplaceAlertsFiltersManufacturers()[0]->getManufacturerName();
        }

        return 'Unknown';
    }

    /** @return array|string[] */
    public function getUniqueModelsNames(): array
    {
        $modelsNames = [];

        foreach ($this->getUniqueModels() as $model) {
            $modelsNames[] = $model->getName();
        }

        return $modelsNames;
    }
}
