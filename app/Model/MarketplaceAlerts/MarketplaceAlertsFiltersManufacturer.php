<?php

namespace JamJar\Model\MarketplaceAlerts;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use JamJar\Company;
use JamJar\PartnerLocation;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Sale;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\OfferedValues\OfferedValueService;
use JamJar\Valuation;
use JamJar\Vehicle;
use JamJar\VehicleManufacturer;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class MarketplaceAlertsFiltersManufacturer extends Model
{
    use LogsActivity;

    public $guarded = [];

    public function setFilter(MarketplaceAlertsFilter $filter): self
    {
        $this->marketplace_alerts_filter_id = $filter->getId();

        return $this;
    }

    public function setManufacturerId(int $manufacturerId): self
    {
        $this->manufacturer_id = $manufacturerId;

        return $this;
    }

    public function getManufacturerId(): int
    {
        return $this->manufacturer_id;
    }

    public function getManufacturerName(): ?string
    {
        $manufacturer = VehicleManufacturer::where([['id', '=', $this->getManufacturerId()]])->first();

        return $manufacturer ? $manufacturer->getName() : 'Unknown manufacturer';
    }
}
