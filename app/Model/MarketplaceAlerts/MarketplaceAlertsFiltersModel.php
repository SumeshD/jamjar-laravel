<?php

namespace JamJar\Model\MarketplaceAlerts;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use JamJar\Company;
use JamJar\PartnerLocation;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Sale;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\OfferedValues\OfferedValueService;
use JamJar\Valuation;
use JamJar\Vehicle;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class MarketplaceAlertsFiltersModel extends Model
{
    use LogsActivity;

    public $guarded = [];

    public function setFilter(MarketplaceAlertsFilter $filter): self
    {
        $this->marketplace_alerts_filter_id = $filter->getId();

        return $this;
    }

    public function setModelId(int $modelId): self
    {
        $this->model_id = $modelId;

        return $this;
    }

    public function getModelId(): int
    {
        return $this->model_id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
