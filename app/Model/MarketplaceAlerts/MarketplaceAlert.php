<?php

namespace JamJar\Model\MarketplaceAlerts;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use JamJar\Company;
use JamJar\PartnerLocation;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Sale;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\OfferedValues\OfferedValueService;
use JamJar\Services\VehicleFilter\VehicleFilters;
use JamJar\Services\VehicleFilter\VehicleManufacturerFilter;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class MarketplaceAlert extends Model
{
    const FREQUENCY_DAILY = 'daily';
    const FREQUENCY_INSTANT = 'instant';

    const CONDITION_RUNNERS_ONLY = 'runners';
    const CONDITION_NON_RUNNERS_ONLY = 'non-runners';

    const VEHICLE_TYPE_CAR = 'car';
    const VEHICLE_TYPE_VAN = 'van';

    use LogsActivity;

    public $guarded = [];

    public function marketplaceAlertsFilters()
    {
        return $this->hasMany(MarketplaceAlertsFilter::class);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getUser(): User
    {
        return $this->user;
    }

    /** @return MarketplaceAlertsFilter[] */
    public function getMarketplaceAlertsFilters()
    {
        return $this->marketplaceAlertsFilters;
    }

    /** @return MarketplaceAlert[] */
    public static function getActiveInstantAlerts()
    {
        return MarketplaceAlert::where([
            ['is_enabled', '=', true],
            ['frequency', '=', self::FREQUENCY_INSTANT],
        ])->get();
    }

    /** @return MarketplaceAlert[] */
    public static function getActiveDailyAlerts()
    {
        return MarketplaceAlert::where([
            ['is_enabled', '=', true],
            ['frequency', '=', self::FREQUENCY_DAILY],
        ])->get();
    }

    public function setUser(User $user): self
    {
        $this->user_id = $user->id;

        return $this;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->is_enabled = $isEnabled;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->is_enabled;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setFrequency(string $frequency): self
    {
        $this->frequency = $frequency;

        return $this;
    }

    public function getFrequency(): string
    {
        return $this->frequency;
    }

    public function setMinPlateYear(?string $minPlateYear): self
    {
        $this->min_plate_year = $minPlateYear;

        return $this;
    }

    public function getMinPlateYear(): ?string
    {
        return $this->min_plate_year;
    }

    public function setNumberOfOwners(?int $numberOfOwners): self
    {
        $this->number_of_owners = $numberOfOwners;

        return $this;
    }

    public function getNumberOfOwners(): ?int
    {
        return $this->number_of_owners;
    }

    public function setMinMileage(?int $minMileage): self
    {
        $this->min_mileage = $minMileage;

        return $this;
    }

    public function getMinMileage(): ?int
    {
        return $this->min_mileage;
    }

    public function setMaxMileage(?int $maxMileage): self
    {
        $this->max_mileage = $maxMileage;

        return $this;
    }

    public function getMaxMileage(): ?int
    {
        return $this->max_mileage;
    }

    public function setMinValue(?int $minValue): self
    {
        $this->min_value = $minValue;

        return $this;
    }

    public function getMinValue(): ?int
    {
        return $this->min_value;
    }

    public function setMaxValue(?int $maxValue): self
    {
        $this->max_value = $maxValue;

        return $this;
    }

    public function getMaxValue(): ?int
    {
        return $this->max_value;
    }

    public function setFuelType(?string $fuelType): self
    {
        $this->fuel_type = $fuelType;

        return $this;
    }

    public function getFuelType(): ?int
    {
        return $this->fuel_type;
    }

    public function setCarCondition(?string $carCondition): self
    {
        $this->car_condition = $carCondition;

        return $this;
    }

    public function getCarCondition(): ?string
    {
        return $this->car_condition;
    }

    public function setSellerType(?string $sellerType): self
    {
        $this->seller_type = $sellerType;

        return $this;
    }

    public function getSellerType(): ?int
    {
        return $this->seller_type;
    }

    public function setVehicleType(?string $vehicleType): self
    {
        $this->vehicle_type = $vehicleType;

        return $this;
    }

    public function getVehicleType(): ?string
    {
        return $this->vehicle_type;
    }

    public function setVehicleLocation(?string $vehicleLocation): self
    {
        $this->vehicle_location = $vehicleLocation;

        return $this;
    }

    public function getVehicleLocation(): ?string
    {
        return $this->vehicle_location;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function convertToFilters(): VehicleFilters
    {
        $filters = new VehicleFilters();

        $filters->sellerType = $this->seller_type;
        $filters->carCondition = $this->car_condition;
        $filters->fuelType = $this->fuel_type;
        $filters->maxValue = $this->max_value;
        $filters->minValue = $this->min_value;
        $filters->maxMileage = $this->max_mileage;
        $filters->minMileage = $this->min_mileage;
        $filters->numberOfOwners = $this->number_of_owners;
        $filters->minPlateYear = $this->min_plate_year;
        $filters->vehicleType = $this->vehicle_type;
        $filters->vehicleLocation = $this->vehicle_location;

        foreach ($this->getMarketplaceAlertsFilters() as $marketplaceAlertsFilter) {
            $filter = new VehicleManufacturerFilter();

            foreach ($marketplaceAlertsFilter->getMarketplaceAlertsFiltersManufacturers() as $manufacturer) {
                $filter->manufacturersIds[] = $manufacturer->getManufacturerId();
            }

            foreach ($marketplaceAlertsFilter->getMarketplaceAlertsFiltersModels() as $model) {
                $filter->modelsIds[] = $model->getModelId();
            }

            $filters->vehicleManufacturersFilters[] = $filter;
        }

        return $filters;
    }

    public function getAsRequestParameters($showBanner = true): array
    {
        $filters = $this->convertToFilters();
        $parameters = [
            'min-plate-year' => $filters->minPlateYear,
            'number-of-owners' => $filters->numberOfOwners,
            'min-mileage' => $filters->minMileage,
            'max-mileage' => $filters->maxMileage,
            'min-value' => $filters->minValue,
            'max-value' => $filters->maxValue,
            'fuel-type' => $filters->fuelType,
            'car-condition' => $filters->carCondition,
            'seller-type' => $filters->sellerType,
            'vehicle-type' => $filters->vehicleType,
            'vehicle-location' => $filters->vehicleLocation,
            'mileage-range' => '[' . $filters->minMileage . ',' . $filters->maxMileage . ']',
            'manufacturers-and-models' => [],
            'banner' => $showBanner
        ];

        foreach ($filters->vehicleManufacturersFilters as $filter) {
            $parameters['manufacturers-and-models'][] =
                implode(',', $filter->manufacturersIds) . ';' . implode(',', $filter->modelsIds);
        }

        return $parameters;
    }


    /** @return array|string[] */
    public function getCarSpecification(): array
    {
        $carSpecification = [];

        if ($this->getMinPlateYear()) {
            $carSpecification['Year from'] = $this->getMinPlateYear();
        }

        if ($this->getNumberOfOwners()) {
            $carSpecification['Owners'] = trans('vehicles.owners_count_external.owners_' . $this->getNumberOfOwners());
        }

        if ($this->getMaxMileage()) {
            $carSpecification['Mileage'] = 'Up to ' . number_format($this->getMaxMileage());
        }

        if ($this->getMinValue()) {
            $carSpecification['Min. valuation'] = 'From £' . number_format($this->getMinValue());
        }

        if ($this->getMaxValue()) {
            $carSpecification['Max. valuation'] = 'Up to £' . number_format($this->getMaxValue());
        }

        return $carSpecification;
    }

    public function removeManufacturersFilters()
    {
        foreach ($this->getMarketplaceAlertsFilters() as $filter) {
            foreach ($filter->getMarketplaceAlertsFiltersManufacturers() as $manufacturer) {
                $manufacturer->delete();
            }

            foreach ($filter->getMarketplaceAlertsFiltersModels() as $model) {
                $model->delete();
            }

            $filter->delete();
        }
    }

    public function getActiveVehiclesCount(MarketplaceAlert $alert)
    {
        list($resultsCount, $rawVehicles) = Vehicle::getRawLeadsVehiclesForMatrix($alert->getUser(), $alert->convertToFilters());

        return $resultsCount;
    }
}
