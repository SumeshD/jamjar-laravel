<?php

namespace JamJar\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class ValuationDraftDeliveryMethodFee extends Model
{
    const FEE_TYPE_COLLECTION = 'collection';

    const FEE_TYPE_DROP_OFF = 'drop_off';

    const FEE_TYPE_ADMIN = 'admin';

    const FEE_TYPE_BANK_TRANSFER = 'bank_transfer';

    use LogsActivity;

    public $guarded = [];

    public static function create(ValuationDraftDeliveryMethod $deliveryMethod, string $feeType, int $amountInPence): self
    {
        $fee = new ValuationDraftDeliveryMethodFee();
        $fee->delivery_method_id = $deliveryMethod->id;
        $fee->fee_type = $feeType;
        $fee->fee_amount_in_pence = $amountInPence;
        $fee->save();

        return $fee;
    }

}
