<?php

namespace JamJar\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use JamJar\Company;
use JamJar\PartnerLocation;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Sale;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\OfferedValues\OfferedValueService;
use JamJar\Valuation;
use JamJar\Vehicle;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class ValuationDraft extends Model implements OfferedValueInterface
{
    use LogsActivity;

    public $guarded = [];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'buyer_company_id');
    }

    public function location()
    {
        return $this->belongsTo(PartnerLocation::class, 'buyer_location_id');
    }

    public function valuation()
    {
        return $this->belongsTo(Valuation::class);
    }

    public function getVehicle(): Vehicle
    {
        return $this->vehicle;
    }

    public function getVehicleId(): Int
    {
        return $this->vehicle_id;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function getBuyerCompany(): Company
    {
        return $this->getCompany();
    }

    public function getSellerCompany(): ?Company
    {
        return $this->getVehicle()->owner->user->company;
    }

    public function getLocation(): ?PartnerLocation
    {
        return $this->location;
    }

    public function getValuation(): ?Valuation
    {
        return $this->valuation;
    }

    public function hasCompleteSale(): bool
    {
        return $this->hasSaleWithStatus('complete');
    }

    public function hasPendingSale(): bool
    {
        return $this->hasSaleWithStatus('pending');
    }

    public function hasRejectedSale(): bool
    {
        return $this->hasSaleWithStatus('rejected');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isDropOffAvailable(): bool
    {
        return (bool) ValuationDraftDeliveryMethod::where([
            ['valuation_draft_id', '=', $this->id],
            ['delivery_method', '=', ValuationDraftDeliveryMethod::DELIVERY_METHOD_DROP_OFF]
        ])->first();
    }

    public function isCollectionAvailable(): bool
    {
        return (bool) ValuationDraftDeliveryMethod::where([
            ['valuation_draft_id', '=', $this->id],
            ['delivery_method', '=', ValuationDraftDeliveryMethod::DELIVERY_METHOD_COLLECTION]
        ])->first();
    }

    public function getCollectionFeeForCTB() : int
    {

        $body = simplexml_load_string((string)$this->external_partner_response);
        $results = (array)$body->valuations;

        $collection_fee = collect($results)->map(
            function ($value, $key) use ($body) {
                $hasCollectionValuation = isset($value[0]);
                return $hasCollectionValuation == true ? (int) $value[0]->fees->collection : null;
            }
        );


        return $collection_fee['valuation'];
    }

    public function getCollectionFeeInPence(): int
    {
        return $this->getFee(
            ValuationDraftDeliveryMethod::DELIVERY_METHOD_COLLECTION,
            ValuationDraftDeliveryMethodFee::FEE_TYPE_COLLECTION
        );
    }

    public function getDropOffFeeInPence(): int
    {
        return $this->getFee(
            ValuationDraftDeliveryMethod::DELIVERY_METHOD_DROP_OFF,
            ValuationDraftDeliveryMethodFee::FEE_TYPE_DROP_OFF
        );
    }

    public function getBankTransferFeeInPence(): int
    {
        $collectionBankFee = $this->getFee(
            ValuationDraftDeliveryMethod::DELIVERY_METHOD_COLLECTION,
            ValuationDraftDeliveryMethodFee::FEE_TYPE_BANK_TRANSFER
        );

        $dropOffBankFee = $this->getFee(
            ValuationDraftDeliveryMethod::DELIVERY_METHOD_DROP_OFF,
            ValuationDraftDeliveryMethodFee::FEE_TYPE_BANK_TRANSFER
        );

        return max($collectionBankFee, $dropOffBankFee);
    }

    public function getPaymentMethod(): string
    {
        return $this->payment_method;
    }

    public function getFinalDropOffPriceInPence(string $vehicleCondition = OfferedValueInterface::VEHICLE_CONDITION_GREAT): ?int
    {
        $price = ValuationDraftPrice::where([
            ['valuation_draft_id', '=', $this->id],
            ['vehicle_condition', '=', $vehicleCondition],
        ])->first();

        if (!$price) {
            return null;
        }

        return $price->price_in_pence - $this->getDropOffFeeInPence() - $this->getBankTransferFeeInPence() - $this->getAdminFeeInPence();
    }

    public function getFinalCollectionPriceInPence(string $vehicleCondition = OfferedValueInterface::VEHICLE_CONDITION_GREAT): ?int
    {
        $price = ValuationDraftPrice::where([
            ['valuation_draft_id', '=', $this->id],
            ['vehicle_condition', '=', $vehicleCondition],
        ])->first();

        if (!$price) {
            return null;
        }

        return $price->price_in_pence - $this->getCollectionFeeInPence() - $this->getBankTransferFeeInPence() - $this->getAdminFeeInPence();
    }

    public function getAdminFeeInPence(): int
    {
        $collectionBankFee = $this->getFee(
            ValuationDraftDeliveryMethod::DELIVERY_METHOD_COLLECTION,
            ValuationDraftDeliveryMethodFee::FEE_TYPE_ADMIN
        );

        $dropOffBankFee = $this->getFee(
            ValuationDraftDeliveryMethod::DELIVERY_METHOD_DROP_OFF,
            ValuationDraftDeliveryMethodFee::FEE_TYPE_ADMIN
        );

        return max($collectionBankFee, $dropOffBankFee);
    }

    protected function getFee(string $deliveryMethod, string $feeType): int
    {
        $fee = ValuationDraftDeliveryMethodFee::select('valuation_draft_delivery_method_fees.*')
            ->where([
                ['valuation_draft_id', '=', $this->id],
                ['delivery_method', '=', $deliveryMethod],
                ['fee_type', '=', $feeType],
            ])
            ->join(
                'valuation_draft_delivery_methods',
                'valuation_draft_delivery_method_fees.delivery_method_id',
                '=',
                'valuation_draft_delivery_methods.id'
            )
            ->first();

        return $fee ? $fee->fee_amount_in_pence : 0;
    }


    /**
     * @param Vehicle $vehicle
     * @param Company $buyerCompany
     * @param int $priceInPence
     * @param PartnerLocation $buyerLocation
     * @param string $valuationSource
     * @param int|null $distanceToSellerInMiles
     * @param string|null $acceptanceUri
     * @param string|null $tempRef
     * @param string|null $externalPartnerRequest
     * @param string|null $externalPartnerResponse
     * @param int|null $valuationValidityInDays
     * @param string|null $paymentMethod
     *
     * @return ValuationDraft
     */
    public static function create(
        Vehicle $vehicle,
        Company $buyerCompany,
        int $priceInPence,
        PartnerLocation $buyerLocation,
        string $valuationSource,
        int $distanceToSellerInMiles = null,
        string $acceptanceUri = null,
        string $tempRef = null,
        string $externalPartnerRequest = null,
        string $externalPartnerResponse = null,
        int $valuationValidityInDays = null,
        string $paymentMethod = null
    ): ValuationDraft {
        $draft = new ValuationDraft();

        $draft->vehicle_id = $vehicle->id;
        $draft->buyer_company_id = $buyerCompany->id;
        $draft->price_in_pence = $priceInPence;
        $draft->buyer_location_id = $buyerLocation->id;
        $draft->distance_to_seller_in_miles = $distanceToSellerInMiles;

        $draft->acceptance_uri = $acceptanceUri;
        $draft->temp_ref = $tempRef;
        $draft->external_partner_request = $externalPartnerRequest;
        $draft->external_partner_response = $externalPartnerResponse;
        $draft->valuation_source = $valuationSource;
        $draft->valuation_validity_in_days = $valuationValidityInDays;

        if ($paymentMethod) {
            $draft->payment_method = $paymentMethod;
        }

        $draft->save();

        return $draft;
    }

    public function getPriceInPence(): int
    {
        return $this->price_in_pence;
    }

    public function getPriceForConditionInPence(string $condition): ?int
    {
        $price = ValuationDraftPrice::where([
            ['valuation_draft_id', '=', $this->id],
            ['vehicle_condition', '=', $condition]
        ])->first();

        return $price ? $price->price_in_pence : null;
    }

    public function getSource(): string
    {
        return $this->valuation_source;
    }

    public function getDistanceToSellerInMiles(): ?int
    {
        return $this->distance_to_seller_in_miles;
    }

    public function getValuationValidityInDays(): int
    {
        return $this->valuation_validity_in_days;
    }

    public function shouldBeCollectedFromAnyLocation() : bool
    {
        return
            $this->getSource() == OfferedValueInterface::VALUE_SOURCE_EXTERNAL &&
            $this->isCollectionAvailable() &&
            !$this->isDropOffAvailable();
    }

    public function shouldBeCollectedFromAnyLocationScrapPartner() : bool
    {
        return
            $this->company->company_type == 'api' &&
            $this->getSource() == OfferedValueInterface::VALUE_SOURCE_EXTERNAL &&
            $this->isCollectionAvailable();
    }

    public function shouldBeCollectedFromClientLocation(): bool
    {
        return
            $this->getSource() != OfferedValueInterface::VALUE_SOURCE_EXTERNAL &&
            $this->isCollectionAvailable() &&
            !$this->isDropOffAvailable();
    }

    public function shouldBeCollectedFromClientLocationScrapPartner(): bool
    {
        return
            $this->company->company_type == 'api' &&
            $this->getSource() != OfferedValueInterface::VALUE_SOURCE_EXTERNAL &&
            $this->isCollectionAvailable();
    }

    public function shouldBeDroppedAtDistance() : bool
    {
        return
            !$this->shouldBeCollectedFromAnyLocation() &&
            $this->getSource() == OfferedValueInterface::VALUE_SOURCE_EXTERNAL &&
            $this->distance_to_seller_in_miles !== null &&
            $this->isDropOffAvailable();
    }

    public function shouldBeDroppedAtLocation() : bool
    {
        return
            !$this->shouldBeCollectedFromAnyLocation() &&
            !$this->shouldBeCollectedFromClientLocation() &&
            !$this->shouldBeDroppedAtDistance();
    }

    public function isExpired(): bool
    {
        $expires = $this->created_at->addDay($this->getValuationValidityInDays());

        return Carbon::now() > $expires;
    }

    public function removeCompletely()
    {
        foreach (ValuationDraftDeliveryMethod::where('valuation_draft_id', '=', $this->id)->get() as $deliveryMethod) {
            foreach (ValuationDraftDeliveryMethodFee::where('delivery_method_id', '=', $deliveryMethod->id)->get() as $fee) {
                $fee->delete();
            }
            $deliveryMethod->delete();
        }

        foreach (ValuationDraftPrice::where('valuation_draft_id', '=', $this->id)->get() as $price) {

            $price->delete();
        }

        $this->delete();
    }

    public function hasBuyerEnoughFunds(): bool
    {
        $fee = (new PartnerFeeCalculator(2, $this->getPriceInPence() / 100))->calculate(true, $this->getBuyerCompany(), true);

        return ($fee <= $this->getBuyerCompany()->user->funds->getOriginal('funds'));
    }

    protected function hasSaleWithStatus(string $status): bool
    {
        if (!$valuation = $this->getValuation()) {
            return false;
        }

        if (!$sale = $valuation->getSale()) {
            return false;
        }

        return $sale->status == $status;
    }

    public function getSale(): ?Sale
    {
        if (!$valuation = $this->getValuation()) {
            return null;
        }

        return Sale::where('valuation_id', '=', $valuation->id)->first();
    }

    public function hasSale(): bool
    {
        return (bool) $this->getSale();
    }

    public function getListingPosition(): int
    {
        $offeredValueService = new OfferedValueService();

        $offers = $offeredValueService->getOfferedValuesByVehicle($this->getVehicle());

        foreach ($offers as $offerPosition => $offer) {
            if ($offer instanceof ValuationDraft && $offer->getId() == $this->getId()) {
                return $offerPosition + 1;
            }
        }

        return 1;
    }

    public function getFormattedPrice(): string
    {
        return '&pound;' . number_format($this->price_in_pence/100);
    }

    public function hasConverted()
    {

        $converted = Valuation::where([
            ['vehicle_id', '=', $this->vehicle_id],
            ['company_id', '=', $this->buyer_company_id],
        ])->first();

        if ($converted) {

            return 'Yes';
        }

        return 'No';
    }
}
