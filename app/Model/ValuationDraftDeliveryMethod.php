<?php

namespace JamJar\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class ValuationDraftDeliveryMethod extends Model
{
    const DELIVERY_METHOD_COLLECTION = 'collection';

    const DELIVERY_METHOD_DROP_OFF = 'drop_off';

    use LogsActivity;

    public $guarded = [];

    public static function create(ValuationDraft $draft, string $deliveryMethod): self
    {
        $draftDeliveryMethod = new ValuationDraftDeliveryMethod();

        $draftDeliveryMethod->valuation_draft_id = $draft->id;
        $draftDeliveryMethod->delivery_method = $deliveryMethod;
        $draftDeliveryMethod->save();

        return $draftDeliveryMethod;
    }

}
