<?php

namespace JamJar\Model;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Illuminate\Support\Carbon as LaravelCarbon;

class UploadedImage extends Model
{
    public $incrementing = false;

    public function id() : UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    public function originalName() : string
    {
        return $this->original_name;
    }

    public function status() : string
    {
        return $this->status;
    }

    public function createdAt() : LaravelCarbon
    {
        return $this->created_at;
    }

    public function updatedAt() : LaravelCarbon
    {
        return $this->updated_at;
    }
}
