<?php

namespace JamJar\Model;

use Illuminate\Database\Eloquent\Model;
use JamJar\Company;
use JamJar\PartnerLocation;
use JamJar\User;
use JamJar\Vehicle;
use Ramsey\Uuid\UuidInterface;
use Spatie\Activitylog\Traits\LogsActivity;

/** @mixin \Eloquent */
class VehicleImage extends Model
{
    use LogsActivity;

    public $guarded = [];

    /** @return \Illuminate\Database\Eloquent\Relations\HasOne */
    public function uploadedImage()
    {
        return $this->hasOne(UploadedImage::class, 'id', 'uploaded_image_id');
    }

    public function getId(): int 
    {
        return $this->id;
    }

    public function getUploadedImage(): UploadedImage
    {
        return $this->uploadedImage;
    }

    public function getImageId(): UuidInterface
    {
        return $this->getUploadedImage()->id();
    }
}
