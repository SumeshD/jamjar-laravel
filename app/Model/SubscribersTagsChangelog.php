<?php

namespace JamJar\Model;

use Illuminate\Database\Eloquent\Model;

/** @mixin \Eloquent */
class SubscribersTagsChangelog extends Model
{
    public $guarded = [];

    public function records()
    {
        return $this->hasMany(SubscribersTagsChangelogRecord::class, 'changelog_id', 'id');
    }

    /** @return SubscribersTagsChangelogRecord[] */
    public function getRecords()
    {
        return SubscribersTagsChangelogRecord::where('changelog_id', '=', $this->id)
            ->orderBy('tag_type', 'ASC')
            ->get();
    }

    public function getRecordsGroupedByEmail()
    {
        return SubscribersTagsChangelogRecord::selectRaw('subscribers_tags_changelog_records.*, GROUP_CONCAT(tag_type SEPARATOR \', \') as tags')
            ->where('changelog_id', '=', $this->id)
            ->groupBy('email')
            ->get();
    }

    public function addRecord(string $tagType, string $email, string $memberId): SubscribersTagsChangelogRecord
    {
        $record = new SubscribersTagsChangelogRecord();
        $record->changelog_id = $this->id;
        $record->tag_type = $tagType;
        $record->email = $email;
        $record->member_id = $memberId;
        $record->save();

        return $record;
    }

}
