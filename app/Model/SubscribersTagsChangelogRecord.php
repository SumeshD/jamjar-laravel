<?php

namespace JamJar\Model;

use Illuminate\Database\Eloquent\Model;

/** @mixin \Eloquent */
class SubscribersTagsChangelogRecord extends Model
{
    public $guarded = [];

    public function changelog()
    {
        return $this->belongsTo(SubscribersTagsChangelog::class, 'changelog_id', 'id');
    }

    public function getChangelog(): SubscribersTagsChangelog
    {
        return $this->changelog;
    }

}
