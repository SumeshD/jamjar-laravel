<?php

namespace JamJar;

use GoogleMaps;
use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;

class VehicleOwner extends Model
{
    use LogsActivity;
    
    public $guarded = [];

    /**
     * An owner belongs to a Vehicle
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getNameAttribute($name)
    {
        return $this->firstname .' '. $this->lastname;
    }

    public function setPostcode(string $postcode) : VehicleOwner
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getAreaByPostcode() : string
    {
        if (!$this->postcode) {
            return '';
        }

        $postcode = strtoupper($this->postcode);

        $regExp = '/([A-Z]+)([0-9]+)([A-Z ]*[0-9][A-Z]{2})/';

        $matches = [];

        preg_match($regExp, $postcode, $matches);

        try {
            return trim($matches[1]) . trim($matches[2]);
        } catch (\Exception $e) {
            return $postcode;
        }

    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getPhoneForCarTakeBack(): string
    {
        $phoneNumber = (string) $this->telephone;
        if (strpos($phoneNumber, '+') !== false) {
            $spaceBarPosition = strpos($phoneNumber, ' ');
            $phoneNumber = substr($this->telephone, $spaceBarPosition);
        }

        $phoneNumber = trim($phoneNumber);

        if ($phoneNumber[0] !== '0') {
            $phoneNumber = '0' . $phoneNumber;
        }

        return $phoneNumber;
    }
}
