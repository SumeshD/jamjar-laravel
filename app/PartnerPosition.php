<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;

class PartnerPosition extends Model
{
    const MAX_POSITION = 6;

    protected $fillable = ['position', 'user_id'];

    public function getPosition(): ?int
    {
        return $this->position;
    }
}
