<?php

namespace JamJar;

use Log;
use JamJar\Primitives\Money;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class UserFund extends Model
{
    use LogsActivity;

    protected $fillable = ['funds'];

    /**
     * Add Funds
     *
     * @param  int $amount
     * @return bool
     */
    public function addFunds($amount)
    {
        $funds = $this->getOriginal('funds') + $amount;
        Log::info('Adding Funds to Account', ['funds_current' => $this->funds, 'funds_added' => $amount, 'new_current_funds' => $funds]);
        return $this->update(['funds' => $funds]);
    }

    /**
     * Spend Funds
     *
     * @param  int $amount
     * @return bool
     */
    public function spendFunds($amount)
    {
        $funds = $this->getOriginal('funds') - $amount;
        Log::info('Removing Funds from Account', ['funds_current' => $this->getOriginal('funds'), 'funds_removed' => $amount, 'new_current_funds' => $funds]);
        return $this->update(['funds' => $funds]);
    }

    /**
     * A Credit belongs to a User
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the funds attribute and format it accordingly.
     *
     * @param  int $funds
     * @return float
     */
    public function getFundsAttribute($funds)
    {
        $money = Money::fromPence($funds);
        return $money->inPoundsAndPence();
    }
}
