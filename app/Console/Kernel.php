<?php

namespace JamJar\Console;

use Illuminate\Console\Scheduling\Schedule;
use JamJar\Console\Commands\AcceptWWACValuations;
use JamJar\Console\Commands\CheckQueueScript;
use JamJar\Console\Commands\FixSpecialEditions;
use JamJar\Console\Commands\HourlyValuationReminders;
use JamJar\Console\Commands\HandleAbandonedValuations;
use JamJar\Console\Commands\MarketplaceAlertsSendDailyReminders;
use JamJar\Console\Commands\RecalculateSuggestedValues;
use JamJar\Console\Commands\AcceptedValuations;
use JamJar\Console\Commands\BackloggedAcceptedValuations;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use JamJar\Console\Commands\UpdateEmailsVersions;
use JamJar\Console\Commands\UpdateEmptyOffersBaseValues;
use JamJar\Console\Commands\UpdateMailchimpTags;
use JamJar\Console\Commands\UpdateVehiclesCapData;
use JamJar\Console\Commands\UpdateVehiclesFeesData;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        HandleAbandonedValuations::class,
        RecalculateSuggestedValues::class,
        HourlyValuationReminders::class,
        AcceptedValuations::class,
        BackloggedAcceptedValuations::class,
        AcceptWWACValuations::class,
        UpdateVehiclesCapData::class,
        MarketplaceAlertsSendDailyReminders::class,
        UpdateMailchimpTags::class,
        UpdateVehiclesFeesData::class,
        UpdateEmailsVersions::class,
        UpdateEmptyOffersBaseValues::class,
        CheckQueueScript::class,
        FixSpecialEditions::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Send notifications an hour after a valuation is created (runs every 10 minutes)
        $schedule->command('jamjar:hourly-valuation-notification')->everyFiveMinutes()->timezone('Europe/London');

        // Handle Abandoned Valuations
        $schedule->command('jamjar:abandoned-valuations')->twiceDaily(9, 17)->timezone('Europe/London')->emailOutputTo('info@jamjar.com');

        // Update Suggested Values
        // temporary disabled because it take something like 20 hours to execute on production
//        $schedule->command('jamjar:suggested')->timezone('Europe/London')->dailyAt('09:00');

        // Update Accepted Valuations
        $schedule->command('jamjar:accepted-valuations')->timezone('Europe/London')->twiceDaily(0, 12);

        $schedule->command('jamjar:marketplace-alerts-send-daily-reminders')->timezone('Europe/London')->dailyAt('6:00');

        // Update Mailchimp Tags
        $schedule->command('jamjar:update-mailchimp-tags')->timezone('Europe/London')->twiceDaily(0, 12);

        // Seed the CAP Database on the first of every month at 1am.
        $schedule->command('db:seed --class=CapSeeder')->timezone('Europe/London')->monthly(1, '01:00');

        $schedule->command('jamjar:check-queue-script')->everyFifteenMinutes()->timezone('Europe/London');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        include base_path('routes/console.php');
    }
}
