<?php

namespace JamJar\Console\Commands;

use Illuminate\Console\Command;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;

class UpdateVehiclesCapData extends Command
{
    /** @var string */
    protected $signature = 'jamjar:update-vehicles-cap-data';

    /** @var string */
    protected $description = 'Update vehicles manufacturer_id, model_id and derivative_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $vehicles = \JamJar\Vehicle::get();
        $count = count($vehicles);
        $counter = 0;
        foreach ($vehicles as $vehicle) {
            $this->info("Processing " . ++$counter . ' / ' . $count);
            $vehicleMeta = $vehicle->meta;

            if (!$vehicleMeta) {
                $vehicle->manufacturer_id = null;
                $vehicle->model_id = null;
                $vehicle->derivative_id = null;
                $vehicle->save();

                continue;
            }

            $vehicleManufacturer = VehicleManufacturer::where([
                ['name', '=', $vehicleMeta->manufacturer],
                ['vehicle_type', '=', ($vehicle->type == 'Car' ? 'CAR' : 'LCV')],
            ])->first();

            if (!$vehicleManufacturer) {
                $vehicle->manufacturer_id = null;
                $vehicle->model_id = null;
                $vehicle->derivative_id = null;
                $vehicle->save();

                continue;
            } else {
                $vehicle->manufacturer_id = $vehicleManufacturer->getId();
                $vehicle->save();
            }

            $vehicleModel = VehicleModel::where([
                ['cap_model_id', '=', $vehicleMeta->cap_model_id],
                ['internal_manufacturer_id', '=', $vehicleManufacturer->getId()],
            ])->first();

            if (!$vehicleModel) {
                $vehicle->model_id = null;
                $vehicle->derivative_id = null;
                $vehicle->save();

                continue;
            } else {
                $vehicle->model_id = $vehicleModel->getId();
                $vehicle->save();
            }

            $vehicleDerivative = \JamJar\VehicleDerivative::where([
                ['internal_model_id', '=', $vehicleModel->getId()],
                ['name', '=', $vehicle->meta->derivative]
            ])->first();

            if (!$vehicleDerivative) {
                $vehicle->derivative_id = null;
                $vehicle->save();

                continue;
            } else {
                $vehicle->derivative_id = $vehicleDerivative->getId();
                $vehicle->save();
            }
        }
    }
}
