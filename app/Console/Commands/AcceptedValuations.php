<?php

namespace JamJar\Console\Commands;

use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;
use JamJar\Api\WeWantAnyCar;
use JamJar\Notifications\ApiFailure;
use JamJar\Services\APIValuationsService;
use JamJar\Services\Integrations\MotorwiseIntegration;
use JamJar\Valuation;
use JamJar\Sale;
use \GuzzleHttp\Client as HttpClient;
use JamJar\Vehicle;

class AcceptedValuations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jamjar:accepted-valuations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a list of vehicles accepted by API Partners.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $m4ym = $this->getM4YMv2Vehicles();
        $this->acceptWWACValuations();
        $this->acceptMwQuotes();
        $this->info('Done!');
    }

    protected function getM4YMv2Vehicles()
    {
        $base_uri = env('M4YMv2_ENV') == 'sandbox' ? 'https://www.m4ym.com/affiliateapi-m4ym-sandbox/api' : 'https://www.m4ym.com/affiliateapi-m4ym-live/api';

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'ClientIdentifier' => env('M4YMv2_CLIENT_IDENTIFIER'),
                    'ClientAPIKey' => env('M4YMv2_API_KEY')
                ],
                'verify' => false
            ]
        );

        $now = date("d/m/Y H:i:s");
        $oneYear = date("d/m/Y H:i:s", strtotime(" -12 months"));

        $ValuationRequest = [
            'ColumnMappings' => [
                [
                    'Id' => 'PurchaseReportStartDate',
                    'Value' => $oneYear
                ],
                [
                    'Id' => 'PurchaseReportEndDate',
                    'Value' => $now
                ]
            ]
        ];
        try {
            // dd(json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES), $this->partnerIdentifier, $this->key);
            // Get the valuation from M4YM.
            $response = $client->post(
                "{$base_uri}/Reporting/PurchaseReport",
                [
                    'body' => json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES),
                    'headers' => [
                        'ClientIdentifier' => env('M4YMv2_CLIENT_IDENTIFIER'),
                        'ClientAPIKey' => env('M4YMv2_API_KEY'),
                        'Content-Type' => 'application/json'
                    ]
                ]
            );

        } catch (\Exception $exception) {
            \Log::debug('M4YMv2 Accept Valuations Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                        ->notify(new ApiFailure('M4YM Version 2 failure'));
            // Return false so the application doesn't crash
            return false;
        }

        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);

        if($body['ColumnMappings'][0]['Value'] == "There have been no purchases for the specified time-frame"){
            \Log::debug('M4YM Version 2 Accepted response:'.print_r($body['ColumnMappings'][0]['Value'], true));
            return false;
        }

        $exploded = explode(';', $body['ColumnMappings'][0]['Value']);

        //Remove last item (empty string)
        array_pop($exploded);

        //Separate cars into separate arrays
        $data = []; 
        foreach($exploded as $key => $car){
            array_push($data, str_getcsv($car));
        }

        \Log::debug('M4YM Version 2 Accepted response:'.print_r($data, true));

        if(sizeof($data) > 0){

            foreach($data as $acceptedVal){

                if(in_array($acceptedVal[4], ['Vehicle Purchased', 'Vehicle SOLD'])){

                    // If we can find valuations
                    $findValuation = Valuation::where('temp_ref', $acceptedVal[0])->with('company')->get();
                    $date = DateTime::createFromFormat('d/m/Y H:i:s', $acceptedVal[5])->format('Y-m-d H:i:s');

                    if (count($findValuation) == 0) {
                        $findValuation = Valuation::getByNumberplateForM4YM($acceptedVal[1], $date);

                        if ($findValuation) {
                            $findValuation = [$findValuation];
                        } else {
                            $this->info(sprintf('Valuation for vehicle with numberplate %s missing', $acceptedVal[1]));
                        }
                    }

                    // Update to be accepted
                    if($findValuation){
                        foreach($findValuation as $item){

                            if ($item->accepted_at) {
                                continue;
                            }

                            $item->update(
                                ['accepted_at' => $date]
                            );
                            // Get Value
                            $item->dropoff_value != '&pound;0' ? $value = $item->dropoff_value : $value = $item->collection_value;

                            $value = str_replace('&pound;', '', str_replace(',', '', $value));

                            // Create a sale
                            $sale = (new Sale)->createSale(
                                $item->vehicle, 
                                $item, 
                                $item->company,
                                [
                                    'value' => $value,
                                    'deliveryMethod' => $item->dropoff_value != '&pound;0' ? 'dropoff' : 'collection',
                                    'dealer_notes' => $acceptedVal[0]
                                    ]                 
                            );
                            $sale->status = 'complete';
                            $sale->save();

                            if ($item->temp_ref != $acceptedVal[0]) {
                                $item->temp_ref = $acceptedVal[0];
                                $item->save();
                            }
                        }
                    }
                }
                
                // ->update(['96h_email_reminder_sent' => 1]);
            }
        }

    }

    private function acceptWWACValuations()
    {
        $WWACApiService = new WeWantAnyCar();
        $valuations = $WWACApiService->getAcceptedValuations(Carbon::now()->subYear(), Carbon::now());
        \Log::debug('WWAC valuations to accept: '. print_r($valuations, true));

        $valuationsService = new APIValuationsService();
        $valuationsService->acceptWWACValuations($valuations);
    }

    private function acceptMwQuotes()
    {
        $mwIntegration = new MotorwiseIntegration();
        $nonAcceptedSales = Sale::where('created_at','>=', Carbon::now()->addDays(-1))
            ->where('company_id', $mwIntegration->getCompanyId())
            ->where('status','pending')
            ->get();

        foreach ($nonAcceptedSales as $sale) {
            $status = $mwIntegration->checkQuoteStatus($sale->valuation->temp_ref);
            switch ($status){
                case MotorwiseIntegration::MOTORWISE_QUOTE_STATUS_ACCEPTED:
                case MotorwiseIntegration::MOTORWISE_QUOTE_STATUS_COMPLETED:
                    $sale->status='complete';
                    break;
                case MotorwiseIntegration::MOTORWISE_QUOTE_STATUS_EXPIRED:
                case MotorwiseIntegration::MOTORWISE_QUOTE_STATUS_CANCELLED:
                    $sale->status='rejected';
                    break;
                default:
                    $sale->status='pending';
            }
            $sale->save();
        }
    }
}
