<?php

namespace JamJar\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use JamJar\Notifications\QueueServerFailed;

class CheckQueueScript extends Command
{
    /** @var string */
    protected $signature = 'jamjar:check-queue-script';

    /** @var string */
    protected $description = 'Checks queue script is running and eventually do an action';

    public function handle()
    {
        $jobs = DB::select( DB::raw("SELECT id FROM jobs;") );

        $jobsCount = count($jobs);

        if ($jobsCount > 6) {
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new QueueServerFailed());

            Artisan::call('queue:listen');
            $this->info("Queue server started manually");
        } else {
            $this->info("Queue server is working correctly");
        }
    }
}
