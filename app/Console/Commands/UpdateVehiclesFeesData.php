<?php

namespace JamJar\Console\Commands;

use Illuminate\Console\Command;
use JamJar\Model\ValuationDraft;

class UpdateVehiclesFeesData extends Command
{
    /** @var string */
    protected $signature = 'jamjar:update-valuations-fees-data';

    /** @var string */
    protected $description = 'Update payment_method, admin_fee, collection_value and dropoff_value for legacy valuations';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $valuationDrafts = ValuationDraft::get();

        $draftsCount = count($valuationDrafts);

        $this->info('Drafts to process: ' . $draftsCount);
        $counter = 0;

        foreach ($valuationDrafts as $draft) {
            $valuation = $draft->getValuation();
            $this->info('Process: ' . $counter++ . ' / ' . $draftsCount);
            if ($valuation) {
                $collectionValue = null;

                if ($draft->isCollectionAvailable()) {
                    $collectionValue = (int) ($draft->price_in_pence / 100);
                    if ($draft->getBuyerCompany()->company_type == 'api') {
                        $collectionValue -= (int) ($draft->getCollectionFeeInPence() / 100);
                    }
                }

                $dropOffValue = null;

                if ($draft->isDropOffAvailable()) {
                    $dropOffValue = (int) ($draft->price_in_pence / 100);
                    if ($draft->getBuyerCompany()->company_type == 'api') {
                        $dropOffValue -= (int) ($draft->getDropOffFeeInPence() / 100);
                    }
                }

                $valuation->payment_method = $draft->getPaymentMethod();
                $valuation->admin_fee = (int) ($draft->getAdminFeeInPence() / 100);
                $valuation->collection_value = $collectionValue;
                $valuation->dropoff_value = $dropOffValue;

                $valuation->save();
            }
        }
    }
}
