<?php

namespace JamJar\Console\Commands;

use Illuminate\Console\Command;
use JamJar\Company;
use JamJar\Model\SubscribersTagsChangelog;
use JamJar\Offer;
use JamJar\Order;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;
use Newsletter;

class UpdateMailchimpTags extends Command
{
    protected $signature = 'jamjar:update-mailchimp-tags';
    
    protected $description = 'Update mailchimp tags to Jamjar associates who are subscribed to the relevant Mailchimp audience.';

    private $apiInstance;

    /** @var SubscribersTagsChangelog */
    private $changelog;

    public function __construct()
    {
        $this->apiInstance = Newsletter::getApi();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->changelog = new SubscribersTagsChangelog();
        $this->changelog->save();

        $members = Newsletter::getMembers('matrix_subscribers',['offset' => 0, 'count' => 50]);

        $totalItems = $members['total_items'];

        $this->loopMembers($members);

        if ($totalItems > 50) {

            $loops = floor($totalItems/50);

            for ($i = 0;$i<$loops;$i++) {

                $members = Newsletter::getMembers('matrix_subscribers',['offset' => ($i+1)*50, 'count' => 50]);

                $this->loopMembers($members);

            }

        }

    }

    private function loopMembers(array $members) {

        foreach ($members['members'] as $member) {

            $memberEmail = strtolower($member['email_address']);
            $tags = $this->apiInstance->get('lists/'.env('MAILCHIMP_MATRIX_LIST_ID').'/members/'.$member['id'].'/tags')['tags'];

            $this->addCreatedTag($tags, $memberEmail, $member['id']);
            $this->addLookupsTag($tags, $memberEmail, $member['id']);
            $this->addFundsTag($tags, $memberEmail, $member['id']);
            $this->addMarketplaceTag($tags, $memberEmail, $member['id']);
            $this->addAutomatedBidsTag($tags, $memberEmail, $member['id']);

        }

    }


    /**
     * Check if AUTOMATED-BIDS tag is already added, if not add AUTOMATED-BIDS tag if there is any data in the specified table.
     *
     * @param array $tags
     * @param string $email
     * @param string $memberId
     */
    private function addAutomatedBidsTag(array $tags, string $email, string $memberId)
    {
        $tags = collect($tags)->pluck('name');

        if ($tags->contains('automated-bids')) {
            return;
        }

        $user = User::where('email', $email)->first();

        if (!$user) {
            return;
        }

        $rules = Offer::select('id', 'user_id')
            ->where('user_id', '=', $user->id)
            ->get();

        if ($rules->isEmpty()) {
            return;
        }

        $this->addTagToUser('automated-bids', $email, $memberId);

    }

    /**
     * Check if MARKETPLACE tag is already added, if not add MARKETPLACE tag if there is any data in the specified table.
     *
     * @param array $tags
     * @param string $email
     * @param string $memberId
     */
    private function addMarketplaceTag(array $tags, string $email, string $memberId)
    {
        $tags = collect($tags)->pluck('name');

        if ($tags->contains('marketplace')) {
            return;
        }

        $user = User::where('email', $email)->first();

        if (!$user) {
            return;
        }

        $company = Company::where('user_id', '=', $user->id)->first();

        if (!$company) {
            return;
        }

        $valuations = Valuation::select('id', 'vehicle_id', 'user_id', 'company_id', 'collection_value', 'dropoff_value')
            ->where('company_id', '=', $company->id)
            ->whereRaw('(vehicle_id is not null and company_id IS NOT NULL and (collection_value IS NOT NULL OR dropoff_value IS NOT NULL))')
            ->get();

        if ($valuations->isEmpty()) {
            return;
        }

        $this->addTagToUser('marketplace', $email, $memberId);

    }


    /**
     * Check if FUNDS tag is already added, if not add FUNDS tag if there is any data in the specified table.
     *
     * @param array $tags
     * @param string $email
     * @param string $memberId
     */
    private function addFundsTag(array $tags, string $email, string $memberId)
    {
        $tags = collect($tags)->pluck('name');

        if ($tags->contains('funds')) {
            return;
        }

        $user = User::where('email', $email)->first();

        if (!$user) {
            return;
        }

        $funds = Order::select('id')->where('user_id', '=', $user->id)->get();

        if ($funds->isEmpty()) {
            return;
        }

        $this->addTagToUser('funds', $email, $memberId);

    }


    /**
     * Check if LOOKUPS tag is already added, if not add LOOKUPS tag if there is any data in the specified tables.
     *
     * @param array $tags
     * @param string $email
     * @param string $memberId
     */
    private function addLookupsTag(array $tags, string $email, string $memberId)
    {
        $tags = collect($tags)->pluck('name');

        if ($tags->contains('lookups')) {
            return;
        }

        $user = User::where('email', $email)->first();

        if (!$user) {
            return;
        }

        $where = [
            ['users.id', '=', $user->id],
            ['vehicles.is_finished', '=', true]
        ];

        $lookups = Vehicle::select('vehicles.id', 'vehicles.is_finished', 'vehicle_owners.vehicle_id', 'vehicle_owners.user_id', 'users.id')
            ->join('vehicle_owners',  'vehicle_owners.vehicle_id',  '=',  'vehicles.id')
            ->join('users', 'users.id', '=', 'vehicle_owners.user_id')
            ->where($where)
            ->get();

        if ($lookups->isEmpty()) {
            return;
        }

        $this->addTagToUser('lookups', $email, $memberId);

    }

    /**
     * Check if CREATED tag is already added, if not add CREATED tag if the partner is approved.
     *
     * @param array $tags
     * @param string $email
     * @param string $memberId
     */
    private function addCreatedTag(array $tags, string $email, string $memberId)
    {
        $tags = collect($tags)->pluck('name');

        if ($tags->contains('created')) {
            return;
        }

        $user = User::where('email', $email)->first();

        if (!$user || $user->matrix_partner != 1) {
           return;
        }

        $this->addTagToUser('created', $email, $memberId);

    }

    private function addTagToUser(string $tagType, string $email, string $memberId)
    {

        $response = $this->apiInstance->post('lists/' . env('MAILCHIMP_MATRIX_LIST_ID') . '/members/' . $memberId . '/tags',
            ['tags' => [['name' => $tagType, 'status' => 'active']]]
        );

        if($response){
            $this->info('Tag "' . $tagType . '" added for ' . $email . ' - member!');
            $this->changelog->addRecord($tagType, $email, $memberId);
        }

    }
}
