<?php

namespace JamJar\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use JamJar\Api\WeWantAnyCar;
use JamJar\Services\APIValuationsService;

class AcceptWWACValuations extends Command
{
    /** @var string */
    protected $signature = 'jamjar:accepted-wwac-valuations';

    /** @var string */
    protected $description = 'Accept all WWAC valuations since last year';

    /** @var int */
    private $processedValuationsCount = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->acceptWWACValuations();

        $this->info('Done!');
        $this->info('Processed valuations: ' . $this->processedValuationsCount);
    }

    private function acceptWWACValuations()
    {
        $WWACApiService = new WeWantAnyCar();
        $valuations = $WWACApiService->getAcceptedValuations(Carbon::now()->subYears(2), Carbon::now());
        $this->processedValuationsCount = count($valuations);

        $valuationsService = new APIValuationsService();
        $valuationsService->acceptWWACValuations($valuations);
    }
}
