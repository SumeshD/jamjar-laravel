<?php

namespace JamJar\Console\Commands;

use Illuminate\Console\Command;
use JamJar\Setting;
use JamJar\VehicleDerivative;
use JamJar\VehicleModel;

class FixSpecialEditions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jamjar:fix-special-editions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $specialEditionsModels = VehicleModel::where('name', 'like', '%SPECIAL%')->get();
        foreach($specialEditionsModels as $model) {
            $newModel = $model;
            $newModel->fuel_type = 'Diesel';
            $attr = $newModel->attributesToArray();
            $dieselModel = VehicleModel::firstOrCreate([
                'cap_model_id' => $attr['cap_model_id'],
                'name' => $attr['name'],
                'manufacturer_id' => $attr['manufacturer_id'],
                'fuel_type' => $attr['fuel_type'],
                'introduced_date' => $attr['introduced_date'],
                'plate_years' => $attr['plate_years'],
                'internal_manufacturer_id' => $attr['internal_manufacturer_id'],
                'discontinued_date' => $attr['discontinued_date']
            ]);

            $derivatives = VehicleDerivative::where('internal_model_id', '=', $model->id)->get();
            $dieselCharacteristicDerivativeNames = Setting::get('diesel_characteristic_derivative_name');
            $dieselCharacteristicDerivativeNamesArray = explode(';',$dieselCharacteristicDerivativeNames);
            foreach ($derivatives as $derivative) {
                if($this->checkIfIsADiesel($dieselCharacteristicDerivativeNamesArray, $derivative->name)){
                    echo($derivative->id.'.'.$derivative->name.' ::: ');
                    $derivative->internal_model_id = $dieselModel->id;
                    $derivative->save();
                }
            }
        }
        echo('completed');
    }

    private function checkIfIsADiesel($dieselCharacteristicDerivativeNamesArray, $derivativeName) {
        foreach ($dieselCharacteristicDerivativeNamesArray as $dieselCharacteristicName) {
            if (strpos(strtoupper($derivativeName), strtoupper($dieselCharacteristicName)) !== FALSE) {
                return true;
            }
        }
        return false;
    }
}
