<?php

namespace JamJar\Console\Commands;

use Illuminate\Console\Command;
use JamJar\Services\VehicleFilter\MarketplaceAlertsService;

class MarketplaceAlertsSendDailyReminders extends Command
{
    /** @var string */
    protected $signature = 'jamjar:marketplace-alerts-send-daily-reminders';

    /** @var string */
    protected $description = 'Send emails to partners about new vehicles';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $service = new MarketplaceAlertsService();
        $summary = $service->sendDailyReminders();

        $this->info('DONE!');
        $this->info('Messages sent: ' . count($summary['info']));
        $this->info('Errors: ' . count($summary['errors']));

        foreach ($summary['errors'] as $error) {
            $this->error($error);
        }
    }
}
