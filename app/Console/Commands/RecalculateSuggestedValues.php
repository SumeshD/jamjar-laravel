<?php

namespace JamJar\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use JamJar\Mail\UpdatedSuggestedValue;
use JamJar\Offer;
use JamJar\OfferDerivative;
use JamJar\SuggestedValue;
use JamJar\User;
use JamJar\Vehicle;
use JamJar\VehicleDerivative;

class RecalculateSuggestedValues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jamjar:suggested';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate the Suggested Values.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    /*
    public function handle()
    {

        // get all derivatives
        $derivatives = $this->getDerivativesCollection();

        $checkedVehicleDerivatives = [];


        // loop through each derivative
        $derivatives->each(function ($derivative) use (&$checkedVehicleDerivatives) {

            if (in_array($derivative->derivative->id, $checkedVehicleDerivatives)) {
                return;
            }

            $checkedVehicleDerivatives[] = $derivative->derivative->id;


            if ($newSuggestedPercentage = (int) round($derivative->derivative->suggested_value)) {


                // check if the suggested value already exists in the database
                $previousValue = $this->checkExistingSuggestedValues($derivative);
                // if it does, save the previous percentage

                // Does the previous percentage match the current percentage?
                if ($previousValue) {
                    $previousPercentage = $previousValue->percentage;
                    $previousPercentageHasBeenChanged = $previousValue->percentage == $newSuggestedPercentage ? false : true;
                } else {
                    $previousPercentage = null;
                    $previousPercentageHasBeenChanged = true;
                }
                
                // cache()->forget('derivatives-for-model-'.$derivative->cap_model_id.'-and-user-'.auth()->id());

                if (!$previousPercentageHasBeenChanged) {
                    return;
                }

                // store a new suggested value via updateOrCreate

                $suggestedValue = SuggestedValue::updateOrCreate([
                    'cap_model_id' => $derivative->cap_model_id,
                    'cap_derivative_id' => $derivative->cap_derivative_id,

                ],
                [
                    'percentage' => $newSuggestedPercentage,
                    'previous_percentage' => $previousPercentage ?? null,
                    'cap_model_id' => $derivative->cap_model_id,
                    'cap_derivative_id' => $derivative->cap_derivative_id,
                ]);


                if (!$previousPercentage || $previousPercentageHasBeenChanged) {

                    $this->notifyUsers($previousPercentage, $suggestedValue);
                }

            }



        });
    }
    */

    public function handle()
    {

        // get all derivatives
        $derivatives = $this->getDerivativesCollection();

        $checkedVehicleDerivatives = [];

        $newSuggestedValues = [];

        // loop through each derivative
        foreach ($derivatives as $derivative) {

            if (!in_array($derivative->derivative->id, $checkedVehicleDerivatives)) {

                $checkedVehicleDerivatives[] = $derivative->derivative->id;

                if ($newSuggestedPercentage = (int) round($derivative->derivative->suggested_value)) {

                    // check if the suggested value already exists in the database
                    $previousValue = $this->checkExistingSuggestedValues($derivative);
                    // if it does, save the previous percentage

                    // Does the previous percentage match the current percentage?
                    if ($previousValue) {
                        $previousPercentage = $previousValue->percentage;
                        $previousPercentageHasBeenChanged = $previousValue->percentage == $newSuggestedPercentage ? false : true;
                    } else {
                        $previousPercentage = null;
                        $previousPercentageHasBeenChanged = true;
                    }

                    // cache()->forget('derivatives-for-model-'.$derivative->cap_model_id.'-and-user-'.auth()->id());

                    if (!$previousPercentageHasBeenChanged) {
                          continue;
                    }

                    // store a new suggested value via updateOrCreate

                    $suggestedValue = SuggestedValue::updateOrCreate([
                        'cap_model_id' => $derivative->cap_model_id,
                        'cap_derivative_id' => $derivative->cap_derivative_id,

                    ],
                        [
                            'percentage' => $newSuggestedPercentage,
                            'previous_percentage' => $previousPercentage ?? null,
                            'cap_model_id' => $derivative->cap_model_id,
                            'cap_derivative_id' => $derivative->cap_derivative_id,
                        ]);


                    if (!$previousPercentage || $previousPercentageHasBeenChanged) {

                        $newSuggestedValues[] = $suggestedValue;
                    }

                }

            }

        }

        if(!empty($newSuggestedValues)) {

            $this->notifyUsers($newSuggestedValues);
        }

    }

    /**
     * Get Derivatives Collection
     *
     * @return Illuminate\Support\Collection
     */
    protected function getDerivativesCollection()
    {
        // Build up a derivatives collection
        $derivatives = collect();

        // Loop through each offer
        $offers = Offer::get();

        $offers->each(function ($offer) use ($derivatives) {
            // Loop through each offer's derivatives
            $offer->derivatives->each(function ($derivative) use ($derivatives) {
                // push the derivative to the derivatives collection
                $derivatives->push($derivative);
            });
        });

        // ensure they're unique and return them
        return $derivatives;
    }

    /**
     * Check the existing suggested value
     *
     * @param  JamJar\OfferDerivative $derivative
     * @return JamJar\SuggestedValue
     */
    protected function checkExistingSuggestedValues(OfferDerivative $derivative)
    {
        $value = SuggestedValue::where('cap_model_id', $derivative->cap_model_id)
                                ->where('cap_derivative_id', $derivative->cap_derivative_id);

        if (!$value->exists()) {
            return false;
        }

        return $value->first();
    }

    protected function notifyUsers($suggestedValues)
    {

        $allOffers = collect();

        foreach ($suggestedValues as $suggestedValue) {

            $offers = Offer::select('offers.id','offers.user_id','offers.active','offers.finished')
                ->join('offer_derivatives', 'offer_derivatives.offer_id', '=', 'offers.id')
                ->where([
                    ['finished', '=', true],
                    ['active', '=', true],
                    ['cap_model_id', '=', $suggestedValue['cap_model_id']],
                    ['cap_derivative_id', '=', $suggestedValue['cap_derivative_id']],
                    ['accept_write_offs',false],
                    ['acceptable_defects','=','a:0:{}'],
                ])
                ->get();

            foreach ($offers as $key=>$offer) {

                $offerDerivative = OfferDerivative::where([
                    ['offer_id', '=', $offer->id],
                    ['cap_model_id', '=', $suggestedValue['cap_model_id']],
                    ['cap_derivative_id', '=', $suggestedValue['cap_derivative_id']],
                ])->first();

                if ($offerDerivative->base_value >= $suggestedValue['percentage']) {

                    $offers->forget($key);

                }
            }

            $allOffers = $allOffers->merge($offers);
        }

        $grouped = $allOffers->groupBy(function ($item, $key) {
            return $item['user_id'];
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        });

        foreach ($groupCount as $key=>$count) {

            $user = User::where('id',$key)->first();

            if ($user) {

                if ($user->send_additional_emails && !$user->profile->is_scrap_dealer) {

                    $this->info("Notifying {$user->email}");
                    $email = new UpdatedSuggestedValue($user, $count);
                    Mail::to($user->email)->send($email);
                }

            }

        }

    }

    /**
     * [notifyUsers description]
     *
     * @param  OfferDerivative $derivative         [description]
     * @param  [type]          $previousPercentage [description]
     * @return [type]                              [description]
     */
    /*
    protected function notifyUsers(?int $previousPercentage, SuggestedValue $suggestedValue)
    {
        $currentPercentage = $suggestedValue->percentage;

        $offers = Offer::select('offers.*')
            ->join('offer_derivatives', 'offer_derivatives.offer_id', '=', 'offers.id')
            ->where([
                ['finished', '=', true],
                ['active', '=', true],
                ['offers.user_id', '=', 513],
                ['cap_model_id', '=', $suggestedValue->cap_model_id],
                ['cap_derivative_id', '=', $suggestedValue->cap_derivative_id],
            ])
            ->get();

        foreach ($offers as $offer) {
            if (!$offer->user->send_additional_emails) {
                continue;
            }

            $offerDerivative = OfferDerivative::where([
                ['offer_id', '=', $offer->id],
                ['cap_model_id', '=', $suggestedValue->cap_model_id],
                ['cap_derivative_id', '=', $suggestedValue->cap_derivative_id],
            ])->first();


            if ($offerDerivative->base_value >= $currentPercentage) {
                continue;
            }

            $this->info("Notifying {$offer->user->email}");

            $email = new UpdatedSuggestedValue($offer->user, $currentPercentage, $previousPercentage ?? 0, $offerDerivative->derivative);
            Mail::to($offer->user->email)->send($email);

        }

    }
    */
}
