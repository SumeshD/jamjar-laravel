<?php

namespace JamJar\Console\Commands;

use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use JamJar\Valuation;
use JamJar\Sale;
use Illuminate\Support\Facades\Notification;
use JamJar\Notifications\ApiFailure;

use \GuzzleHttp\Client as HttpClient;

class BackloggedAcceptedValuations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jamjar:backlogged-accepted-valuations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a list of vehicles accepted by API Partners.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $m4ym = $this->getM4YMv2Vehicles();
    }

    protected function getM4YMv2Vehicles()
    {
        $base_uri = env('M4YMv2_ENV') == 'sandbox' ? 'https://www.m4ym.com/affiliateapi-m4ym-sandbox/api' : 'https://www.m4ym.com/affiliateapi-m4ym-live/api';

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'ClientIdentifier' => env('M4YMv2_CLIENT_IDENTIFIER'),
                    'ClientAPIKey' => env('M4YMv2_API_KEY')
                ],
                'verify' => false
            ]
        );

        $now = date("d/m/Y H:i:s");
        $halfDayAgo = date("d/m/Y H:i:s", strtotime(" -99999 hours"));

        $ValuationRequest = [
            'ColumnMappings' => [
                [
                    'Id' => 'PurchaseReportStartDate',
                    'Value' => $halfDayAgo
                ],
                [
                    'Id' => 'PurchaseReportEndDate',
                    'Value' => $now
                ]
            ]
        ];
        try {
            // dd(json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES), $this->partnerIdentifier, $this->key);
            // Get the valuation from M4YM.
            $response = $client->post(
                "{$base_uri}/Reporting/PurchaseReport",
                [
                    'body' => json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES),
                    'headers' => [
                        'ClientIdentifier' => env('M4YMv2_CLIENT_IDENTIFIER'),
                        'ClientAPIKey' => env('M4YMv2_API_KEY'),
                        'Content-Type' => 'application/json'
                    ]
                ]
            );

        } catch (\Exception $exception) {
            \Log::debug('M4YM Version 2 API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                        ->notify(new ApiFailure('M4YM Version 2 failure'));
            // Return false so the application doesn't crash
            return false;
        }

        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);


        //Make response into nicer array
        $responseData = [];
        // return dd($body['ColumnMappings'][0]);
        // if(isset($body['ColumnMappings'][0]) && !empty($body['ColumnMappings'][0])){
        //     foreach($body['ColumnMappings'][0] as $data){
        //         $responseData[$data['Id']] = $data['Value'];
        //     }
        // }

        $exploded = explode(';', $body['ColumnMappings'][0]['Value']);

        //Remove last item (empty string)
        array_pop($exploded);

        //Separate cars into separate arrays
        $data = []; 
        foreach($exploded as $key => $car){
            array_push($data, str_getcsv($car));
        }

        \Log::debug('M4YM Version 2 API data:'. print_r($data, true));


        if(sizeof($data) > 0){

            foreach($data as $acceptedVal){

                if($acceptedVal[4] == "Vehicle Purchased"){
                    // If we can find valuations
                    $findValuation = Valuation::where('temp_ref', $acceptedVal[0])->with('company')->get();
                    $date = DateTime::createFromFormat('d/m/Y h:i:s', $acceptedVal[5])->format('Y-m-d H:i:s');

                    // Update to be accepted
                    if($findValuation){
                        foreach($findValuation as $item){
                            $item->update(
                                ['accepted_at' => $date]
                            );
                            // Get Value
                            $item->dropoff_value != '&pound;0' ? $value = $item->dropoff_value : $value = $item->collection_value;

                            $value = str_replace('&pound;', '', str_replace(',', '', $value));

                            // Create a sale
                            $sale = (new Sale)->createSale(
                                $item->vehicle, 
                                $item, 
                                $item->company,
                                [
                                    'value' => $value,
                                    'deliveryMethod' => $item->dropoff_value != '&pound;0' ? 'dropoff' : 'collection',
                                    'dealer_notes' => $acceptedVal[0]
                                    ]                 
                            );

                            $sale->status = 'complete';
                            $sale->save();
                        }
                    }
                } 
                
                // ->update(['96h_email_reminder_sent' => 1]);
            }
        }

    }
}
