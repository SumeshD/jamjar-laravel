<?php

namespace JamJar\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use JamJar\Offer;
use JamJar\OfferDerivative;
use JamJar\OfferModel;

class UpdateEmptyOffersBaseValues extends Command
{
    protected $signature = 'jamjar:update-offers-base-values';

    protected $description = 'Fix empty base values for offers derivatives and models';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $offers = Offer::get();

        $offersCount = count($offers);
        $processedOffersCounter = 0;

        foreach ($offers as $offer) {
            $emptyModels = OfferModel::where([
                ['offer_id', '=', $offer->id],
                ['base_value', '=', 0]
            ])->get();

            $emptyDerivatives = OfferDerivative::where([
                ['offer_id', '=', $offer->id],
                ['base_value', '=', 0]
            ])->get();

            $emptyModelsCount = count($emptyModels);
            $emptyDerivativesCount = count($emptyDerivatives);

            if ($emptyModelsCount == 0 && $emptyDerivativesCount == 0) {
                $processedOffersCounter++;
                $this->info('Offer with id ' . $offer->id . ' is healthy!');
                $this->info('Progress: ' . $processedOffersCounter . ' / ' . $offersCount);

                continue;
            }

            $modelIds = [];

            foreach ($emptyModels as $emptyModel) {
                $modelIds[] = $emptyModel->id;
            }

            DB::table('offer_models')
                ->whereIn('id', $modelIds)
                ->update(['base_value' => $offer->base_value]);

            $derivativesIds = [];

            foreach ($emptyDerivatives as $emptyDerivative) {
                $derivativesIds[] = $emptyDerivative->id;
            }

            DB::table('offer_derivatives')
                ->whereIn('id', $derivativesIds)
                ->update(['base_value' => $offer->base_value]);

            $processedOffersCounter++;
            $this->info('Progress: ' . $processedOffersCounter . ' / ' . $offersCount);
        }

        $this->info('Done!');
    }
}
