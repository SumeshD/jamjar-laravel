<?php

namespace JamJar\Console\Commands;

use JamJar\Services\ValuationsReminders\InitialEmailReminderService;
use Illuminate\Console\Command;

class HourlyValuationReminders extends Command
{
    protected $signature = 'jamjar:hourly-valuation-notification';

    protected $description = 'Send the user an email about their valuation an hour after they receive it.';

    public function handle()
    {
        $this->info('Sending hourly reminders');

        $reminderService = new InitialEmailReminderService();
        $reminderService->sendReminders();

        $this->info(sprintf('Job done! I sent %d emails in total', $reminderService->getSentMessagesCount()));
    }
}
