<?php

namespace JamJar\Console\Commands;

use JamJar\Services\ValuationsReminders\ContinuousEmailReminderService;
use JamJar\Services\ValuationsReminders\FinalEmailReminderService;
use Illuminate\Console\Command;

class HandleAbandonedValuations extends Command
{
    protected $signature = 'jamjar:abandoned-valuations';

    protected $description = 'Find Abandoned Valuations and email the user about it after 24 hours, and then after 96 hours.';

    public function handle()
    {
        $this->info('Sending 24h and 96h reminders');

        $continuousReminderService = new ContinuousEmailReminderService();
        $continuousReminderService->sendReminders();

        $finalReminderService = new FinalEmailReminderService();
        $finalReminderService->sendReminders();

        $this->info('Job done! I sent: ');
        $this->info(sprintf('24h reminders: %d in total', $continuousReminderService->getSentMessagesCount()));
        $this->info(sprintf('96h reminders: %d in total', $finalReminderService->getSentMessagesCount()));
    }

}
