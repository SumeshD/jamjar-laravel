<?php

namespace JamJar\Console\Commands;

use Illuminate\Console\Command;
use jdavidbakr\MailTracker\Model\SentEmail;

class UpdateEmailsVersions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jamjar:update-emails-versions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update sent emails versions and types';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = SentEmail::get();

        $relations = [
            ['subject' => 'Marketplace Alert - jamjar.com', 'type' => 'marketplace-alert-daily-email'],
            ['subject' => 'Marketplace Alert - jamjar.com', 'type' => 'marketplace-alert-instant-email'],
            ['subject' => 'Welcome to Jamjar!', 'type' => 'user-verification'],
            ['subject' => "Don't miss out on the best offer on your vehicle!", 'type' => 'initial-reminder'],
            ['subject' => "Don't miss out on the best offer on your vehicle!", 'type' => 'final-reminder'],
            ['subject' => "Reset Password", 'type' => 'reset-password'],
            ['subject' => "Good News! You have an improved offer! - Jamjar", 'type' => 'improved-offer'],
            ['subject' => "New Sale Agreed - jamjar.com", 'type' => 'new-sale-created-seller-confirmation'],
            ['subject' => "New Sale Agreed - jamjar.com", 'type' => 'sale-received'],
            ['subject' => "Additional vehicle information has been provided on Jamjar", 'type' => 'vehicle-additional-information'],
            ['subject' => "New Associate Application on jamjar.com", 'type' => 'matrix-admin-new-partner'],
            ['subject' => "Thanks for Applying to Become an Associate of jamjar.com", 'type' => 'matrix-partner-welcome'],
            ['subject' => "Accepted Offer Cancelled - jamjar.com", 'type' => 'sale-has-been-removed'],
            ['subject' => "A Suggested Value has been updated on Jamjar", 'type' => 'update-suggested-value'],
            ['subject' => "Suggested Values have been updated on Jamjar", 'type' => 'update-suggested-value'],
            ['subject' => "Congratulations, you are now an Associate on jamjar.com", 'type' => 'matrix-partner-approved'],
            ['subject' => "Your Order Receipt - jamjar.com", 'type' => 'matrix-invoice'],
            ['subject' => "New Order Received - jamjar.com", 'type' => 'order-received'],
        ];

        foreach ($emails as $email) {

            if($email->subject == 'Marketplace Alert - jamjar.com' && strpos($email->content,'A new vehicle has been added to the Marketplace that matches one of your alert rules.') !==false) {


                $email->type = 'marketplace-alert-instant-email';
                $email->version = 'v1';
                $email->description = 'Email sent instantly when new vehicle is related to marketplace alert';
                $email->save();
                continue;
            }

            if($email->subject == 'Marketplace Alert - jamjar.com' && strpos($email->content,'A new vehicle has been added to the Marketplace that matches one of your alert rules.') ===false) {


                $email->type = 'marketplace-alert-daily-email';
                $email->version = 'v1';
                $email->description = 'Email sent to partner once a day if there are vehicles related to alert';
                $email->save();
                continue;

            }

            if($email->subject == 'Welcome to Jamjar!') {


                $email->type = 'user-verification';
                $email->version = 'v1';
                $email->description = 'Email sent when user create an account';
                $email->save();
                continue;

            }

            if($email->subject == "Don't miss out on the best offer on your vehicle!" && strpos($email->content,"Don't miss out on the best offer on your vehicle!") !==false) {


                $email->type = 'initial-reminder';
                $email->version = 'v1';
                $email->description = 'Email sent to customer to remind them of valuations';
                $email->save();
                continue;

            }

            if($email->subject == "Don't miss out on the best offer on your vehicle!" && strpos($email->content,"Time's almost up on your best offer!") !==false) {


                $email->type = 'final-reminder';
                $email->version = 'v1';
                $email->description = 'Email sent to customer to remind them of valuations';
                $email->save();
                continue;

            }

            if($email->subject == 'Reset Password') {


                $email->type = 'reset-password';
                $email->version = 'v1';
                $email->description = 'Reset Password email';
                $email->save();
                continue;

            }

            if($email->subject == 'Good News! You have an improved offer! - Jamjar') {


                $email->type = 'improved-offer';
                $email->version = 'v1';
                $email->description = 'Email sent to customer when there is an improved offer';
                $email->save();
                continue;

            }

            if($email->subject == 'New Sale Agreed - jamjar.com' && strpos($email->content,"Congratulations! You have had an offer of") !==false) {


                $email->type = 'new-sale-created-seller-confirmation';
                $email->version = 'v1';
                $email->description = 'Email sent to seller when new sale is created';
                $email->save();
                continue;

            }

            if($email->subject == 'New Sale Agreed - jamjar.com' && strpos($email->content,"Congratulations! You have accepted an offer of") !==false) {


                $email->type = 'sale-received';
                $email->version = 'v1';
                $email->description = 'Email sent to buyer when seller accept the valuatio';
                $email->save();
                continue;

            }

            if($email->subject == 'Additional vehicle information has been provided on Jamjar' || $email->subject == 'A Vehicle Additional Information has been provided on Jamjar') {


                $email->type = 'vehicle-additional-information';
                $email->version = 'v1';
                $email->description = 'Email sent to buyer when new vehicle data is provided by seller';
                $email->save();
                continue;

            }

            if($email->subject == 'New Associate Application on jamjar.com') {


                $email->type = 'matrix-admin-new-partner';
                $email->version = 'v1';
                $email->description = 'Email sent to admin when new partner fill the application form';
                $email->save();
                continue;

            }

            if($email->subject == 'Thanks for Applying to Become an Associate of jamjar.com') {


                $email->type = 'matrix-partner-welcome';
                $email->version = 'v1';
                $email->description = 'Email sent to matrix partner at the very beginning';
                $email->save();
                continue;

            }

            if($email->subject == 'Accepted Offer Cancelled - jamjar.com') {


                $email->type = 'sale-has-been-removed';
                $email->version = 'v1';
                $email->description = 'Email sent to buyer when seller get new results for vehicle and buyer received sale already';
                $email->save();
                continue;

            }

            if($email->subject == 'A Suggested Value has been updated on Jamjar') {


                $email->type = 'update-suggested-value';
                $email->version = 'v1';
                $email->description = 'Suggested Value updated';
                $email->save();
                continue;

            }

            if($email->subject == 'Congratulations, you are now an Associate on jamjar.com') {


                $email->type = 'matrix-partner-approved';
                $email->version = 'v1';
                $email->description = 'Email sent to partner when application is accepted by admin';
                $email->save();
                continue;

            }

            if($email->subject == 'Your Order Receipt - jamjar.com') {


                $email->type = 'matrix-invoice';
                $email->version = 'v1';
                $email->description = 'Email sent to partner when add funds to account';
                $email->save();
                continue;

            }

            if($email->subject == 'New Order Received - jamjar.com') {


                $email->type = 'order-received';
                $email->version = 'v1';
                $email->description = 'Email sent to admin when partner adds some funds';
                $email->save();
                continue;

            }

            $email->type = 'other';
            $email->version = 'v1';
            $email->description = 'Other';
            $email->save();


        }

        $this->info('Email versions updated!');

    }
}
