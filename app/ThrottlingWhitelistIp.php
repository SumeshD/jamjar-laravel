<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;

/** @mixin \Eloquent */
class ThrottlingWhitelistIp extends Model
{
    protected $fillable = ['ip', 'note'];
}
