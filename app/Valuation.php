<?php

namespace JamJar;

use Barryvdh\Reflection\DocBlock\Location;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use JamJar\Model\ValuationDraft;
use JamJar\Primitives\Money;
use Illuminate\Database\Eloquent\Model;
use JamJar\Primitives\PartnerFeeCalculator;
use JamJar\Services\Valuations\OfferedValues\OfferedValueInterface;
use JamJar\Services\Valuations\OfferedValues\OfferedValueService;
use JamJar\ValueObjects\PaidLead;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Http\Request;
use \Exception;

/** @mixin \Eloquent */
class Valuation extends Model implements OfferedValueInterface
{
    use LogsActivity;

    // protected $fillable = ['uuid', 'collection_value', 'dropoff_value', 'vehicle_id', 'user_id', 'company_id', 'manufacturer_id', 'model_id', 'derivative_id', 'cap_value', 'cap_value_retail', 'cap_value_below', 'cap_value_clean', 'position', 'fee', 'partner_type', '24h_sms_reminder_sent', '96h_sms_reminder_sent', '24h_email_reminder_sent', '96h_email_reminder_sent'];
    public $guarded = [];

    protected $dates = ['accept_button_clicked_at'];

    /** @var int|null */
    private $realPositionCache;

    public static function getByVehicle(Vehicle $vehicle)
    {
        return Valuation::where('vehicle_id', '=', $vehicle->id)->get();
    }

    /**
     * @param array|Valuation[] $valuations
     * @return array|PaidLead[]
     */
    public static function getPaidLeadsByValuations($valuations, $offerStatus = false, $leadSource = false): array
    {
        $paidLeads = [];


        foreach ($valuations as $valuation) {

            $vehicle = $valuation->vehicle;
            $highestValuation = Valuation::where('vehicle_id', '=', $vehicle->id)
                ->orderByRaw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC');

            if($leadSource!=false) {
                if($leadSource == 'marketplace') {

                    $createdByAutoBids = false;
                } else {
                    $createdByAutoBids = true;
                }
                $highestValuation->where('is_created_by_automated_bids', $createdByAutoBids);
            }

            $highestValuation = $highestValuation->first();

            if($offerStatus) {

                if ($offerStatus != 'all') {

                    $expires = $valuation->created_at->addDay($valuation->company->valuation_validity);
                    $expired = false;
                    if (now()->gte($expires)) {
                        $expired = true;
                    }

                }

                if($offerStatus == 'expired' && isset($expired) && $expired) {


                    $paidLeads[] = new PaidLead($vehicle, $highestValuation, $valuation);
                    continue;
                }

                if($offerStatus == 'active' && isset($expired) && !$expired) {

                    $paidLeads[] = new PaidLead($vehicle, $highestValuation, $valuation);
                    continue;
                }

                if($offerStatus == 'all') {

                    $paidLeads[] = new PaidLead($vehicle, $highestValuation, $valuation);
                }


            }else {

                $paidLeads[] = new PaidLead($vehicle, $highestValuation, $valuation);
            }



        }

        return $paidLeads;

    }

    public static function getByNumberplateForM4YM(string $numberplate, string $date): ?Valuation
    {
        $company = Company::where('name', '=', 'Money4yourMotors')->first();

        if (!$company) {
            return null;
        }

        $dbValuation = Valuation::select('valuations.id as id')
            ->where([
                ['valuations.created_at', '<=', $date],
                ['company_id', '=', $company->id],
                ['numberplate', '=', $numberplate],
            ])
            ->join('vehicles', 'vehicles.id', 'valuations.vehicle_id')
            ->orderBy('valuations.created_at', 'DESC')
            ->first();

        if ($dbValuation) {
            return Valuation::where('id', '=', $dbValuation->id)->first();
        }

        return null;
    }

    public static function getHighestByUser(Vehicle $vehicle, User $user)
    {
        return Valuation::where([
            ['vehicle_id', '=', $vehicle->id],
            ['company_id', '=', $user->company->id],
        ])
            ->orderByRaw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC')
            ->first();
    }

    public static function getAvailableValuationsStatuses()
    {
        return ['new', 'clicked', 'accepted', 'complete', 'rejected'];
    }

    public static function getForAdminByRequest(Request $request)
    {
        $valuations = Valuation::select('valuations.*')
            ->whereRaw('(vehicles.id is not null and companies.id IS NOT NULL and (collection_value IS NOT NULL OR dropoff_value IS NOT NULL))');

        if ($request->dateFrom) {
            try {
                $valuations->where('valuations.created_at', '>=', Carbon::createFromFormat('d/m/Y H:i:s', $request->dateFrom . ' 00:00:00'));
            } catch (Exception $e) {
            }
        }

        if ($request->dateTo) {
            try {
                $valuations->where('valuations.created_at', '<=', Carbon::createFromFormat('d/m/Y H:i:s', $request->dateTo . ' 23:59:59'));
            } catch (Exception $e) {
            }
        }

        if ($request->companyType && in_array($request->companyType, ['api', 'matrix'])) {
            $valuations->where('companies.company_type', '=', $request->companyType);
        }

        if ($request->companyName) {
            $valuations->where('companies.name', 'LIKE', '%' . $request->companyName . '%');
        }

        if ($request->valuationValueRange) {
            $finalValues = json_decode($request->valuationValueRange);

            $valuations->where(DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0))'), '>=', (int) $finalValues[0]);
            $valuations->where(DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0))'), '<=', (int) $finalValues[1]);
        }

        if ($request->numberPlate) {
            $valuations->where(
                DB::raw('LOWER(REPLACE(vehicles.numberplate, \' \', \'\'))'),
                '=',
                str_replace(' ', '', strtolower($request->numberPlate))
            );
        }

        if ($request->status) {
            switch ($request->status) {
                case 'complete':
                    $valuations->where('status', '=', 'complete');
                    break;
                case 'accepted':
                    $valuations->where('status', '=', 'pending');
                    break;
                case 'rejected':
                    $valuations->where('status', '=', 'rejected');
                    break;
                case 'clicked':
                    $valuations->whereRaw('(status is null and accept_button_clicked_at is NOT NULL)');
                    break;
                case 'new':
                    $valuations->whereRaw('(status is null and accept_button_clicked_at is NULL)');
                    break;
            }
        }

        list($orderColumn, $orderDirection) = Valuation::getOrderingByRequest($request);

        return $valuations
            ->join('companies', 'valuations.company_id', '=', 'companies.id', 'left')
            ->join('vehicles', 'valuations.vehicle_id', '=', 'vehicles.id', 'left')
            ->join('sales', 'valuations.id', '=', 'sales.valuation_id', 'left')
            ->orderBy($orderColumn, $orderDirection)
            ->paginate(20);
    }

    public static function getOrderingByRequest(Request $request): array
    {
        $availableOrderColumns = ['createdAt', 'vehicleType', 'valueOffered'];
        $availableOrderDirections = ['asc', 'desc'];

        $orderColumn = 'valuations.created_at';
        $orderDirection = 'desc';

        if ($request->orderColumn && in_array($request->orderColumn, $availableOrderColumns)) {
            switch ($request->orderColumn) {

                case 'vehicleType':
                    $orderColumn = 'vehicles.type';
                    break;
                case 'valueOffered':
                    $orderColumn = DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0))');
                    break;
                case 'createdAt';
                default:
                    $orderColumn = 'valuations.created_at';
                    break;
            }
        }

        if ($request->orderDirection && in_array(strtolower($request->orderDirection), $availableOrderDirections)) {
            $orderDirection = $request->orderDirection;
        }

        return [$orderColumn, $orderDirection];
    }


    /** @return array|string[] */
    public static function getAvailablePartnerNames(): array
    {
        $availablePartnersNames = [];

        foreach (Valuation::groupBy('company_id')->get() as $valuation) {
            if (!$valuation->company || isset($availablePartnersNames[$valuation->company->name])) {
                continue;
            }

            $availablePartnersNames[$valuation->company->name] = $valuation->company->name;
        }

        return $availablePartnersNames;
    }

    public function getDraft(): ?ValuationDraft
    {
        return ValuationDraft::where([
            ['vehicle_id', '=', $this->vehicle_id],
            ['buyer_company_id', '=', $this->company_id],
        ])->first();
    }

    /**
     * A valuation belongs to a company
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function location()
    {
        return $this->belongsTo(PartnerLocation::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A valuation has a sale
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sale()
    {
        return $this->hasOne(Sale::class);
    }

    /**
     * A Valuation belongs to a Vehicle
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    /**
     * A Valuation has many Vehicles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicles() 
    {
        return $this->hasMany(Vehicle::class, 'id', 'vehicle_id');
    }

    /**
     * Get the cache key name for this model.
     *
     * @return string
     */
    public function getCacheKeyName()
    {
        return session('vrm') . '-company-' . $this->company->id . '-valuation';
    }

    /**
     * Normalize collection_value attribute
     *
     * @param  int $collectionValue
     * @param  bool $format
     * @return string
     */
    public function getCollectionValueAttribute($collectionValue, $format = false)
    {
        if($format){
            return $collectionValue;
        } else {
            return '&pound;' .number_format($collectionValue);
        }
    }

    /**
     * Normalize dropoff_value attribute
     *
     * @param  int $dropOffValue
     * @return string
     */
    public function getDropoffValueAttribute($dropOffValue, $format = false)
    {
        if($format){
            return $dropOffValue;
        } else {
            return '&pound;' . number_format($dropOffValue);
        }
    }

    public function getDistanceToSellerInMiles(): ?int
    {
        return $this->distance;
    }

    /**
     * Scope a query to only include a certain company.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  \JamJar\Company                       $company
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeForCompany($query, $company)
    {
        return $query->where('company_id', $company->id);
    }

    /**
     * Scope a query to only include a certain company.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  \JamJar\Vehicle                       $vehicle
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeForVehicle($query, $vehicle)
    {
        return $query->where('vehicle_id', $vehicle->id);
    }

    /**
     * Scope a query to only include a certain company.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  int                                   $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithValues($query, $value)
    {
        return $query->where('collection_value', $value)->orWhere('dropoff_value', $value);
    }

    /*
     * Get the highest value type
     *
     * @param  int $value
     * @return string
     */
    public function getValueTypeAttribute($value)
    {
        if ((int)$this->getOriginal('dropoff_value') > (int)$this->getOriginal('collection_value')) {
            return 'Dropoff';
        } else {
            return 'Collection';
        }
    }

    /*
     * Get the highest value offered
     *
     * @param  int $value
     * @return string
     */
    public function getValueAttribute($value = null)
    {
        if ((int)$this->getOriginal('dropoff_value') > (int)$this->getOriginal('collection_value')) {
            return $this->dropoff_value;
        } else {
            return $this->collection_value;
        }
    }

    /*
     * Get the highest value offered
     *
     * @param  int $value
     * @return string
     */
    public function getValueIntAttribute($value = null)
    {
        if ((int)$this->getOriginal('dropoff_value') > (int)$this->getOriginal('collection_value')) {
            return $this->getOriginal('dropoff_value');
        } else {
            return $this->getOriginal('collection_value');
        }
    }

    /**
     * Set the Admin Fee Attribute to Pence
     * 
     * @param float $adminFee
     * @return int
     */
    public function setAdminFeeAttribute($adminFee) 
    {
        if ($adminFee) {
            $amount = Money::fromPounds($adminFee);
            $price = $amount->inPence();
            $this->attributes['admin_fee'] = (int) $price;
        }
    }


    /**
     * Get the Admin Fee for Display
     * 
     * @param int $adminFee
     * @return float
     */
    public function getAdminFeeAttribute($adminFee)
    {
        if ($adminFee) {
            $money = Money::fromPence($adminFee);
            return $money->inPoundsAndPence();
        }
    }

    public function shouldBeCollectedFromAnyLocation() : bool
    {
        return
            $this->company->company_type == 'api' &&
            (
                $this->getOriginal('collection_value') > 0 ||
                $this->stripFromCurrency($this->collection_value) > 0
            ) &&
            $this->getOriginal('dropoff_value') == null;
    }

    public function shouldBeCollectedFromAnyLocationScrapPartner() : bool
    {
        return
            $this->company->company_type == 'api' &&
            (
                $this->getOriginal('collection_value') > 0 ||
                $this->stripFromCurrency($this->collection_value) > 0
            );
    }

    public function shouldBeCollectedFromClientLocation(): bool
    {
        return
            ($this->company == null || $this->company->company_type != 'api') &&
            (
                $this->getOriginal('collection_value') > 0 ||
                $this->stripFromCurrency($this->collection_value) > 0
            ) &&
            $this->getOriginal('dropoff_value') == null;
    }

    public function shouldBeCollectedFromClientLocationScrapPartner(): bool
    {
        return
            ($this->company == null || $this->company->company_type != 'api') &&
            (
                $this->getOriginal('collection_value') > 0 ||
                $this->stripFromCurrency($this->collection_value) > 0
            );
    }

    public function shouldBeDroppedAtDistance() : bool
    {
        return
            !$this->shouldBeCollectedFromAnyLocation() &&
            $this->company->company_type == 'api' &&
            $this->distance > 0 &&
            (
                $this->getOriginal('dropoff_value') > 0 ||
                $this->stripFromCurrency($this->dropoff_value) > 0
            );
    }

    public function shouldBeDroppedAtLocation() : bool
    {
        return
            !$this->shouldBeCollectedFromAnyLocation() &&
            !$this->shouldBeCollectedFromClientLocation() &&
            !$this->shouldBeDroppedAtDistance();
    }


    public function stripFromCurrency($value)
    {
        return ($value === (int) $value) ? (int) $value : (int) substr($value, strlen('&pound;'));
    }

    public function setVehicle(Vehicle $vehicle): Valuation
    {
        $this->vehicle_id = $vehicle->id;

        return $this;
    }

    public function setCollectionValue(int $collectionValue): self
    {
        $this->collection_value = $collectionValue;

        return $this;
    }

    public function isExpired(): bool
    {
        return Carbon::now()->subWeek(1) > $this->created_at;
    }

    public function getAcceptButtonClickedAt(): ?Carbon
    {
        return $this->accept_button_clicked_at;
    }

    public function getVehicleConditionValueGood(): ?int
    {
        return $this->vehicle_condition_value_good;
    }

    public function getVehicleConditionValueFair(): ?int
    {
        return $this->vehicle_condition_value_fair;
    }

    public function getVehicleConditionValuePoor(): ?int
    {
        return $this->vehicle_condition_value_poor;
    }

    public function getFee(bool $includeVAT = true)
    {
        return $includeVAT ? $this->fee : ($this->fee / PartnerFeeCalculator::getVATMultiplier());
    }

    public function isCollectionAvailable(): bool
    {
        return $this->getOriginal('collection_value') !== null;
    }

    public function isDropOffAvailable(): bool
    {
        return $this->getOriginal('dropoff_value') !== null;
    }

    public function getPriceInPence(): int
    {
        return ($this->getOriginal('collection_value') > $this->getOriginal('dropoff_value') ?
            $this->getOriginal('collection_value') :
            $this->getOriginal('dropoff_value')) * 100;
    }

    public function getBuyerCompany(): Company
    {
        return $this->company;
    }

    public function getSale(): ?Sale
    {
        return Sale::where('valuation_id', '=', $this->id)->first();
    }

    public function getSource(): string
    {
        if ($this->getBuyerCompany()->company_type == 'api') {
            return OfferedValueInterface::VALUE_SOURCE_EXTERNAL;
        }

        return OfferedValueInterface::VALUE_SOURCE_MATRIX_RULES;
    }

    public function getLocation(): ?PartnerLocation
    {
        return $this->location;
    }

    public function getValuationValidityInDays(): int
    {
        if ($this->company->valuation_validity == null) {
            return 5;
        }

        return (int) $this->company->valuation_validity;
    }

    public function getCollectionFeeInPence(): int
    {
        if (
            $this->getOriginal('collection_value') &&
            $this->getOriginal('dropoff_value') &&
            $this->getOriginal('collection_value') < $this->getOriginal('dropoff_value')
        ) {
            return ($this->getOriginal('dropoff_value') - $this->getOriginal('collection_value')) * 100;
        }

        $location = $this->getLocation();

        if (!$location) {
            return 0;
        }

        if (!$location->allow_collection || $location->collection_fee === null) {
            return 0;
        }

        return (int) ($location->collection_fee * 100);
    }

    public function getDropOffFeeInPence(): int
    {
        if (
            $this->getOriginal('collection_value') &&
            $this->getOriginal('dropoff_value') &&
            $this->getOriginal('collection_value') > $this->getOriginal('dropoff_value')
        ) {
            return ($this->getOriginal('collection_value') - $this->getOriginal('dropoff_value')) * 100;
        }

        $location = $this->getLocation();

        if (!$location) {
            return 0;
        }

        if (!$location->allow_dropoff || $location->dropoff_fee === null) {
            return 0;
        }

        return (int) ($location->dropoff_fee * 100);
    }

    public function getBankTransferFeeInPence(): int
    {
        $buyerCompany = $this->company;

        if ($buyerCompany->bank_transfer_fee === null) {
            return 0;
        }

        return (int) $buyerCompany->bank_transfer_fee * 100;
    }

    public function getPaymentMethod(): string
    {
        return $this->payment_method;
    }

    public function getAdminFeeInPence(): int
    {
        if ($this->getOriginal('admin_fee') !== null) {
            return (int) $this->getOriginal('admin_fee');
        }

        /** @var Company $buyerCompany */
        $buyerCompany = $this->company;

        if ($buyerCompany->getAdminFeeInPence() === null) {
            return 0;
        }

        return $buyerCompany->getAdminFeeInPence();
    }

    public function hasCompleteSale(): bool
    {
        return $this->hasSaleWithStatus('complete');
    }

    public function hasPendingSale(): bool
    {
        return $this->hasSaleWithStatus('pending');
    }

    public function hasRejectedSale(): bool
    {
        return $this->hasSaleWithStatus('rejected');
    }

    private function hasSaleWithStatus(string $status): bool
    {
        return (bool) Sale::where([
            ['valuation_id', '=', $this->id],
            ['status', '=', $status],
        ])->first();
    }

    public function hasBuyerEnoughFunds(): ?bool
    {
        $fee = (new PartnerFeeCalculator(2, $this->getPriceInPence() / 100))->calculate(true, $this->getBuyerCompany(), true);

        return ($fee <= $this->getBuyerCompany()->user->funds->getOriginal('funds'));
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSellerCompany(): Company
    {
        return $this->user->company;
    }

    public function getFinalDropOffPriceInPence(string $vehicleCondition = OfferedValueInterface::VEHICLE_CONDITION_GREAT): ?int
    {
        $taxes = $this->getDropOffFeeInPence() + $this->getAdminFeeInPence() + $this->getBankTransferFeeInPence();

        switch ($vehicleCondition) {
            case OfferedValueInterface::VEHICLE_CONDITION_GREAT:
                return (int) ($this->getOriginal('dropoff_value') * 100 - $taxes);
            case OfferedValueInterface::VEHICLE_CONDITION_GOOD:
                return (int) ($this->vehicle_condition_value_good * 100 - $taxes);
            case OfferedValueInterface::VEHICLE_CONDITION_FAIR:
                return (int) ($this->vehicle_condition_value_fair * 100 - $taxes);
            case OfferedValueInterface::VEHICLE_CONDITION_POOR:
                return (int) ($this->vehicle_condition_value_poor * 100 - $taxes);
        }

        return 0;
    }

    public function getFinalCollectionPriceInPence(string $vehicleCondition = OfferedValueInterface::VEHICLE_CONDITION_GREAT): ?int
    {
        $taxes = $this->getCollectionFeeInPence() + $this->getAdminFeeInPence() + $this->getBankTransferFeeInPence();

        switch ($vehicleCondition) {
            case OfferedValueInterface::VEHICLE_CONDITION_GREAT:
                return (int) ($this->getOriginal('collection_value') * 100 - $taxes);
            case OfferedValueInterface::VEHICLE_CONDITION_GOOD:
                return (int) ($this->vehicle_condition_value_good * 100 - $taxes);
            case OfferedValueInterface::VEHICLE_CONDITION_FAIR:
                return (int) ($this->vehicle_condition_value_fair * 100 - $taxes);
            case OfferedValueInterface::VEHICLE_CONDITION_POOR:
                return (int) ($this->vehicle_condition_value_poor * 100 - $taxes);
        }

        return 0;
    }

    public function getVehicle(): Vehicle
    {
        return $this->vehicle;
    }

    public function getVehicleId(): int
    {
        return $this->vehicle_id;
    }

    public function hasSale(): bool
    {
        return (bool) $this->getSale();
    }

    public function getListingPosition(): int
    {
        $offeredValueService = new OfferedValueService();

        $offers = $offeredValueService->getOfferedValuesByVehicle($this->getVehicle());

        foreach ($offers as $offerPosition => $offer) {
            if ($offer instanceof Valuation && $offer->getId() == $this->getId()) {
                return $offerPosition + 1;
            }
        }

        return 1;
    }

    /**
     * @param User $user buyer
     * @return array|Valuation[]
     */
    public static function getOutbidValuationsByUser(User $user)
    {
        return Valuation::select([
                'valuations.*',
                DB::raw('(select GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) from valuations where vehicle_id = vehicles.id order by GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC limit 1) as max_value')
            ])->where([
                ['company_id', '=', $user->company->id],
                ['valuations.created_at', '>=', Vehicle::getGoodNewsLeadVehicleDisappearDate()],
            ])
            ->join('vehicles', 'vehicles.id', '=', 'valuations.vehicle_id')
            ->having('max_value', '>', DB::raw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0))'))
            ->get();
    }


    public function hasConverted()
    {

        $converted = ValuationDraft::where([
            ['vehicle_id', '=', $this->vehicle_id],
            ['buyer_company_id', '=', $this->company_id],
        ])->first();

        if ($converted) {

            return 'Yes';
        }

        return 'No';
    }

    public function getPaidFee()
    {

        if($this->company->type=='api')
        {
            return 0;
        }

        $calculator = new PartnerFeeCalculator($this->position, $this->getPriceInPence() / 100, $this->vehicle->id);
        $feeInPence = $calculator->calculate(true, $this->company);

        return $feeInPence;

    }

    public function getRealPosition(): int
    {
        if ($this->realPositionCache) {
            return $this->realPositionCache;
        }

        $allValuations = Valuation::where('vehicle_id', '=', $this->vehicle_id)
            ->orderByRaw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC')
            ->get();

        foreach ($allValuations as $key => $valuation) {
            if ($valuation->getId() == $this->getId()) {
                $this->realPositionCache = $key + 1;

                return $this->realPositionCache;
            }
        }

        $this->realPositionCache = 1;

        return $this->realPositionCache;
    }

    /**
     * @return string|null
     */
    public function getNote() :?string
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }
}
