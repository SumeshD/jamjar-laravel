<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;

class DashboardAlert extends Model
{

    protected $fillable = ['subject', 'text', 'status'];

    public function setStatus($status) {
        $this->attributes['status'] = $status;
    }

}
