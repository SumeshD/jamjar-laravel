<?php
namespace JamJar\Traits;

use Illuminate\Support\Facades\DB;
use JamJar\Model\ValuationDraft;
use JamJar\Valuation;
use JamJar\Api\CapApi;
use Illuminate\Http\Request;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;

trait HasSuggestedValues
{
    /**
     * Get valuations from database for the associated models and derivatives
     *
     * @return JamJar\Valuation
     */
    protected function getValuations()
    {
        return Valuation::select('id', 'collection_value', 'dropoff_value', 'vehicle_id', 'cap_value')
                            ->where('derivative_id', $this->cap_derivative_id)
                            ->where('cap_value', '<>', 0)
                            ->orderByRaw('GREATEST(COALESCE(collection_value, 0), COALESCE(dropoff_value, 0)) DESC')
                            ->get();
    }

    /**
     * Get Highest Valuation
     *
     * @param  Illuminate\Database\Eloquent\Collection $valuations
     * @return array
     */
    protected function getHighestValuation($valuations)
    {
        // Set the highest value
        $highest = [
            'value' => 0,
            'valuation' => []
        ];

        // Loop through and check previous highest until we have a winner
        foreach ($valuations as $valuation) {
            if ($valuation->value_int > $highest['value']) {
                $highest = [
                    'value' => $valuation->value_int,
                    'valuation' => $valuation
                ];
            }
        }

        // Build a lovely array to return
        $highestValuation = [
            'valuation' => $highest['value'],
            'cap' => $highest['valuation']->getOriginal('cap_value'),
            'delivery_method' => strtolower($highest['valuation']->value_type),
            'vehicle' => $highest['valuation']->vehicle
        ];

        return $highestValuation;
    }

    /**
     * Get the suggested value
     *
     * @return string
     */
    public function getSuggestedValue()
    {
        $valuations = $this->getValuations();
        $highestDraft = $this->getHighestValuationDraft();

        if ($valuations->isEmpty() && !$highestDraft) {
            return false;
        }

        $valuations = $valuations->reject(function ($valuation) {
            /** @var Valuation $valuation */
            return $valuation->cap_value == 0;
        })->sortByDesc(function ($valuation) {
            if ($valuation->getOriginal('collection_value') > $valuation->getOriginal('dropoff_value')) {
                return $valuation->collection_value;
            } else {
                return $valuation->dropoff_value;
            }
        });

        if ($valuations->isEmpty() && !$highestDraft) {
            return false;
        }

        if (!$valuations->isEmpty()) {
            $highestValuation = $this->getHighestValuation($valuations);
        } else {
            $highestValuation = ['valuation' => 0, 'cap' => 0];
        }

        if ($highestDraft && $highestDraft->getPriceInPence() / 100 > $highestValuation['valuation']) {
            $highestValuation['valuation'] = $highestDraft->getPriceInPence() / 100;
            $highestValuation['cap'] = $highestDraft->getVehicle()->getCapValueInPence() / 100;
        }

        $capValuation = (int)$highestValuation['cap'];

        /**
        * If the valuation is 0 we cannot give a suggest value
        * So do nothing
        */
        if ((int)$capValuation == 0) {
            return false;
        }

        $percentage = ($highestValuation['valuation'] / (int)$capValuation) * 100;

        return round($percentage, 2);
    }

    protected function getHighestValuationDraft():? ValuationDraft
    {
        $vehicleModel = VehicleModel::where('cap_model_id', '=', $this->model_id)->first();

        if (!$vehicleModel) {
            return null;
        }

        $vehicleManufacturer = VehicleManufacturer::where('cap_manufacturer_id', '=', $vehicleModel->manufacturer_id)->first();

        if (!$vehicleManufacturer) {
            return null;
        }

        return ValuationDraft::select('valuation_drafts.*')
            ->join('vehicle_meta', 'vehicle_meta.vehicle_id', '=', 'valuation_drafts.vehicle_id')
            ->join('vehicles', 'vehicles.id', '=', 'valuation_drafts.vehicle_id')
            ->where([
                ['vehicles.cap_value_in_pence', '<>', null],
                [DB::raw('lower(vehicle_meta.manufacturer)'), '=', strtolower($vehicleManufacturer->name)],
                [DB::raw('lower(vehicle_meta.model)'), '=', strtolower($vehicleModel->name)],
                [DB::raw('lower(vehicle_meta.derivative)'), '=', strtolower($this->name)]
            ])
            ->orderBy('price_in_pence', 'DESC')
            ->first();
    }

}
