<?php 
namespace JamJar\Traits;

use JamJar\Offer;
use JamJar\Defect;
use JamJar\Vehicle;
use JamJar\PlateYear;
use Illuminate\Http\Request;

trait HandlesPartnerValuations
{
    /**
     * Check the age of the Vehicle is within the rule paramters
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    protected function checkAge(Vehicle $vehicle, Offer $rule) : bool 
    {
        // Get a lovely collection of Plate Years
        $plateYears = PlateYear::all();

        // Find the sequential(!) identifier of the Vehicle's Plate Number
        $plateId = $plateYears->where('plate_number', $vehicle->meta->plate_number)->pluck('id')->first(); # Let's say this == "10" 

        // Find the id of the Rule's minimum plate number
        $minimumId = $plateYears->where('plate_number', $rule->minimum_plate)->pluck('id')->first(); # Let's say this == "1"

        // Find the id of the Rule's maximum plate number
        $maximumId = $plateYears->where('plate_number', $rule->maximum_plate)->pluck('id')->first(); # Let's say this == "9"

        // Check if vehicles plate_number ID is between the minimum plate number ID and the maximum plate number ID
        if (($plateId >= $minimumId) && ($plateId <= $maximumId)) { # From above example, 10 is not between 1 and 9, so the check fails
            // If it is, continue
            return true;
        }

        // Not between min and max, stop process for this partner
        return false;   
    }

    /**
     * Check the mileage of the Vehicle is within the rule paramters
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    protected function checkMileage(Vehicle $vehicle, Offer $rule) : bool
    {
        if (
            ($rule->getOriginal('minimum_mileage') <= $vehicle->meta->getOriginal('mileage'))
            && 
            ($vehicle->meta->getOriginal('mileage') <= $rule->getOriginal('maximum_mileage'))
        )
        {
            return true;
        }

        return false;
    }

    /**
     * Check the valuation of the Vehicle is within the rule parameters
     *
     * @param  Offer $rule
     * @param  int   $valuation
     * @return bool
     */
    protected function checkValuation(Offer $rule, $valuation) : bool
    {
        if (
            ($rule->getOriginal('minimum_value') <= (int)$valuation['average'])
            && 
            ((int)$valuation['average'] <= $rule->getOriginal('maximum_value'))
        ) {
            return true;
        }

        return false;
    }

    /**
     * Check the service history of the Vehicle is within the rule parameters
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    protected function checkServiceHistory(Vehicle $vehicle, Offer $rule) : bool
    {
        $serviceHistoriesWages = ['None' => 1, 'MinimalHistory' => 2, 'PartHistory' => 3, 'PartFranchise' => 4, 'FullFranchise' => 5];

        $vehicleServiceHistoryWage = $serviceHistoriesWages[$vehicle->meta->getOriginal('service_history')];
        $minimumRuleServiceHistoryWage = $serviceHistoriesWages[$rule->minimum_service_history];

        return $vehicleServiceHistoryWage >= $minimumRuleServiceHistoryWage;
    }

    /**
     * Check the mot of the Vehicle is within the rule parameters
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    protected function checkMot(Vehicle $vehicle, Offer $rule) : bool
    {
        $motsWages = ['Expired' => 1, 'LessThanOne' => 2, 'OneToThree' => 3, 'ThreetoSix' => 4, 'SixPlus' => 5];

        $vehicleMotWage = $motsWages[$vehicle->meta->getOriginal('mot')];
        $minimumRuleMotWage = $motsWages[$rule->minimum_mot];

        return $vehicleMotWage >= $minimumRuleMotWage;
    }

    /**
     * Check the write off status of the Vehicle is within acceptable rule parameters
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    protected function checkWriteOff(Vehicle $vehicle, Offer $rule) : bool
    {
        if ($rule->accept_write_offs == 1) {
            return true;
        }

        return !(bool) $vehicle->meta->write_off;
    }

    /**
     * Check the colour of the Vehicle is within acceptable rule parameters
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    protected function checkColour(Vehicle $vehicle, Offer $rule) : bool
    {
        if ($vehicle->meta->color == 'other') {
            return true;
        }

        if (empty($rule->acceptable_colors)) {
            return true;
        }
        
        if (in_array($vehicle->meta->color, $rule->acceptable_colors->pluck('id')->toArray())) {
            return true;
        }
        return false;
    }

    /**
     * Check the defects of the Vehicle are within acceptable rule parameters
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    protected function checkDefects(Vehicle $vehicle, Offer $rule) : bool
    {
        /**
         * If the defect is Battery, No Tax, No Valid Insurance, SORN or Tyres, we need to ignore it.
         * @todo implement on "road legal" feature
         * <code>
         *  $ignoredDefects = ['Battery', 'Tyres', 'No Tax', 'No Valid Insurance', 'SORN'];
         *  $defects = $vehicle->meta->non_runner_reason->reject(function($defect) use($ignoredDefects) {
         *      return in_array($defect->title, $ignoredDefects);
         *  });
         * </code>
         **/
        $defects = $vehicle->meta->non_runner_reason;

        // If there are no defects, we'll buy it anyway.
        if (!$defects) {
            return true;
        }
        // If acceptable defects is false, we dont accept any cars with defects
        if ($rule->acceptable_defects == false) {
            return false;
        }


        // If we have acceptable defects AND we have vehicle defects, time to do some checking
        if ((!$rule->acceptable_defects->isEmpty()) && (!$defects->isEmpty())) {
            // strip down to only ID's to compare against.
            $allowedDefects = $rule->acceptable_defects->pluck('id')->toArray();
            $vehicleDefects = $defects->pluck('id')->toArray();

            // default to not buying this vehicle
            $return = false;
            foreach ($vehicleDefects as $defect) {
                // if even one defect is allowed, we'll buy the vehicle
                if (in_array($defect, $allowedDefects)) {
                    $return = true;
                }
            }

            return $return;
        }
        // probably superfluous now
        return false;
    }

    /**
     * Check the previous owners of the Vehicle are within acceptable rule parameters
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    protected function checkPreviousKeepers(Vehicle $vehicle, Offer $rule) : bool
    {
        if ($rule->maximum_previous_owners >= 5) {
            return true;
        }
        
        if ($vehicle->meta->number_of_owners <= $rule->maximum_previous_owners) {
            return true;
        }
        return false;
    }

    /**
     * Format the derivative id's into an array
     *
     * @deprecated
     * @param      array $derivativeIds
     * @return     array
     */
    protected function formatDerivativesAsArray(array $derivativeIds) : array
    {
        $derivatives = collect();
        foreach ($derivativeIds as $derivative) {
            $derivative = explode('-', $derivative);
            $derivatives[] = $derivative[1];
        }
        return $derivatives->toArray();
    }

    /**
     * Check if the write off category matches the one we're buying.
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    protected function checkWriteOffCategory(Vehicle $vehicle, Offer $rule) : bool
    {
        if ($rule->maximum_write_off_category == 'none') {
            return true;
        }

        if ($vehicle->meta->write_off_category === null) {
            return true;
        }

        if (!$rule->maximum_write_off_category) {
            return false;
        }

        if ($rule->maximum_write_off_category == 'all') {
            return true;
        }

        if($rule->maximum_write_off_category == 'N' && ($vehicle->meta->write_off_category == 'none' || $vehicle->meta->write_off_category == 'N' || $vehicle->meta->write_off_category == 'D')) {

            return true;
        }

        if($rule->maximum_write_off_category == 'D' && ($vehicle->meta->write_off_category == 'none' || $vehicle->meta->write_off_category == 'N' || $vehicle->meta->write_off_category == 'D')) {

            return true;
        }

        if($rule->maximum_write_off_category == 'S' && ($vehicle->meta->write_off_category == 'none' || $vehicle->meta->write_off_category == 'S' || $vehicle->meta->write_off_category == 'C' || $vehicle->meta->write_off_category == 'N' || $vehicle->meta->write_off_category == 'D')) {

            return true;
        }

        if($rule->maximum_write_off_category == 'C' && ($vehicle->meta->write_off_category == 'none' || $vehicle->meta->write_off_category == 'S' || $vehicle->meta->write_off_category == 'C' || $vehicle->meta->write_off_category == 'N' || $vehicle->meta->write_off_category == 'D')) {

            return true;
        }

        return false;
    }

    /**
     * Validate the Offer Rules for the given Vehicle and Offer
     *
     * @param  Vehicle $vehicle
     * @param  Offer   $rule
     * @return bool
     */
    public function validateOfferRules(Vehicle $vehicle, Offer $rule) : bool
    {
        if (!$this->checkAge($vehicle, $rule)) {
            !request()->has('_debug') ?: dump('FAIL: Age check failed: Rule accepts plates between '.$rule->minimum_plate.' and '.$rule->maximum_plate.'. Actual Plate was: '. $vehicle->meta->plate_number);
            \Log::info('fail age');
            return false;
        }

        if (!$this->checkMileage($vehicle, $rule)) {
            !request()->has('_debug') ?: dump('FAIL: Mileage check failed.');

            return false;
        }

        if (!$this->checkServiceHistory($vehicle, $rule)) {
            !request()->has('_debug') ?: dump('FAIL: Service history check failed.');

            return false;
        }

        if (!$this->checkMot($vehicle, $rule)) {
            !request()->has('_debug') ?: dump('FAIL: MOT check failed.');

            return false;
        }

        if (!$this->checkWriteOff($vehicle, $rule)) {
            !request()->has('_debug') ?: dump('FAIL: Write Off check failed.');

            return false;
        }

        if (!$this->checkColour($vehicle, $rule)) {
            !request()->has('_debug') ?: dump('FAIL: Colour check failed.');

            return false;
        }

        if (!$this->checkDefects($vehicle, $rule)) {
            !request()->has('_debug') ?: dump('FAIL: Defect check failed.');

            return false;
        }

        if (!$this->checkWriteOffCategory($vehicle, $rule)) {
            !request()->has('_debug') ?: dump('FAIL: Write Off Category check failed.');

            return false;
        }

        if (!$this->checkPreviousKeepers($vehicle, $rule)) {
            !request()->has('_debug') ?: dump('FAIL: Previous Keepers check failed.');

            return false;
        }

        return true;
    }
}
