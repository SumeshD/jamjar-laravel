<?php 
namespace JamJar\Traits;

use JamJar\FundingAmount;
use Illuminate\Http\Request;
use JamJar\Primitives\Money;
use Netshell\Paypal\Facades\Paypal;

trait InteractsWithPayPal
{
    /**
     * Set a Payer with PayPal's API
     *
     * @return PayPal\Api\Payer
     */
    protected function setPayer()
    {
        $payer = PayPal::Payer();
        $payer->setPaymentMethod('paypal');
        return $payer;
    }

    /**
     * Set an Amount with PayPal's API
     *
     * @param  JamJar\FundingAmount $fundingAmount
     * @return PayPal\Api\Amount
     */
    protected function setAmount(FundingAmount $fundingAmount)
    {
        $money = Money::fromPence($fundingAmount->getOriginal('price'));
        $price = $money->inPounds();

        $amount = PayPal:: Amount();
        $amount->setCurrency('GBP');
        $amount->setTotal($price);
        return $amount;
    }

    /**
     * Set a Transaction with PayPal's API
     *
     * @param  JamJar\FundingAmount $fundingAmount
     * @param  int                  $amount
     * @return PayPal\Api\Transaction
     */
    protected function setTransaction(FundingAmount $fundingAmount, $amount)
    {
        $transaction = PayPal::Transaction();
        $transaction->setAmount($amount);
        // $transaction->setDescription($fundingAmount->name);
        return $transaction;
    }

    /**
     * Set the Redirect URL's with PayPal's API
     *
     * @return PayPal\Api\RedirectUrls
     */
    protected function setRedirectUrls()
    {
        $redirectUrls = PayPal::RedirectUrls();
        $redirectUrls->setReturnUrl(route('partnerCompletePayment'));
        $redirectUrls->setCancelUrl(route('partnerCancelPayment'));
        return $redirectUrls;
    }

    /**
     * Set a Payment with PayPal's API
     *
     * @param  PayPal\Api\Payer        $payer
     * @param  PayPal\Api\Amount       $amount
     * @param  PayPal\Api\RedirectUrls $redirectUrls
     * @param  PayPal\Api\Transaction  $transaction
     * @return PayPal\Api\Payment
     */
    protected function setPayment($payer, $amount, $redirectUrls, $transaction)
    {
        $payment = PayPal::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));
        $response = $payment->create($this->paypal);
        return $response;
    }

    /**
     * Get a Payment from PayPal's API
     *
     * @param  int $paymentId
     * @return PayPal\Api\Payment
     */
    protected function getPayment($paymentId)
    {
        $payment = PayPal::getById($paymentId, $this->paypal);
        if (!$payment) {
            return false;
        }
        return $payment;
    }

    /**
     * Execute a Payment with PayPal's API
     *
     * @param  PayPal\Api\Payment $payment [description]
     * @param  string             $payerId
     * @return PayPal\Api\PaymentExecution
     */
    protected function executePayment($payment, $payerId)
    {
        $paymentExecution = PayPal::PaymentExecution();
        $paymentExecution->setPayerId($payerId);
        $executePayment = $payment->execute($paymentExecution, $this->paypal);
        return $executePayment;
    }

    /**
     * Send a Payment to PayPal
     *
     * @param  JamJar\FundingAmount $fundingAmount
     * @return PayPal\Api\Payment
     */
    public function payWithPayPal(FundingAmount $fundingAmount)
    {
        return $this->setPayment(
            $this->setPayer(),
            $amount = $this->setAmount($fundingAmount),
            $this->setRedirectUrls(),
            $this->setTransaction($fundingAmount, $amount)
        );
    }

    /**
     * Process a Payment coming back from PayPal
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function processPayment(Request $request)
    {
        $payment = $this->getPayment($request->paymentID);
        
        if ($this->executePayment($payment, $request->payerID)) {
            return response()->json(
                [
                    'success' => true,
                    'payerID' => $request->payerID,
                    'productId' => $request->productId,
                    'paymentID' => $request->paymentID
                ],
                200
            );
        }
    }
}
