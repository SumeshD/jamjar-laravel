<?php

namespace JamJar;

use Illuminate\Database\Eloquent\Model;

class OfferDerivative extends Model
{
    public $fillable = ['cap_model_id', 'cap_derivative_id', 'base_value'];
    public $with = ['suggested'];

    /**
     * An OfferDerivative Belongs To an Offer
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    /**
     * An OfferDerivative Has One Suggested Value
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function suggested() 
    {
        return $this->hasOne(SuggestedValue::class, 'cap_derivative_id', 'cap_derivative_id');
    }

    /**
     * An OfferDerivative Belongs To a VehicleDerivative
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function derivative()
    {
        return $this->belongsTo(VehicleDerivative::class, 'cap_derivative_id', 'cap_derivative_id');
    }

    public function getSuggestedValue() : ?SuggestedValue
    {
        return SuggestedValue::where([
                ['cap_model_id', '=', $this->cap_model_id],
                ['cap_derivative_id', '=', $this->cap_derivative_id],
            ])->first() ?? null;
    }

    public function getOffer(): Offer
    {
        return $this->offer;
    }

    public function getCapModelId(): int
    {
        return $this->cap_model_id;
    }

    public function getCapDerivativeId(): int
    {
        return $this->cap_derivative_id;
    }

    public function getVehicleDerivative(): ?VehicleDerivative
    {
        return VehicleDerivative::where([
            ['cap_derivative_id', '=', $this->cap_derivative_id],
            ['model_id', '=', $this->cap_model_id],
        ])->first();
    }
}
