<?php
namespace JamJar\Api;

use Log;
use JamJar\Vehicle;
use JamJar\Api\ApiInterface;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;

class WeBuyCarsToday implements ApiInterface
{
    const MINIMUM_ACCEPTABLE_PRICE = 1000;

    public $key;
    public $username;
    public $password;

    public function __construct($key = null, $username = null, $password = null)
    {
        $this->key = env('WBCT_ENV') == 'live' ?
            'live:3,_3VO=M!>2LpnAjHcShYI]V]|VzeuYom|FB<vIaeg<^7#PrR<w!{P+3ib42Gk$' :
            'dev:UT7tAiR]y7nChw7&?sdJ?n5hu{9GDhjW+x?+5d[_Bk?Zy!Yu`a/CrgxYKLC.q?!';
        $this->username = env('WBCT_USERNAME');
        $this->password = env('WBCT_PASSWORD');
    }

    public function getVehicle($vrm)
    {
        //
    }

    public function getValuation(Vehicle $vehicle)
    {
        // Build our client
        $client = new HttpClient([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ]);

        // Get a Valuation Reference
        try {
            $response = $client->post('https://www.webuycarstoday.co.uk/wbct-api/linear_services/get_quote', [
                'form_params' => [
                    'key' => $this->key,
                    'username' => env('WBCT_USERNAME', 'JamJar.com'),
                    'password' => env('WBCT_PASSWORD', 'jamjar1234'),
                    'car_reg' => $vehicle->numberplate,
                    'miles' => $vehicle->meta->getOriginal('mileage'),
                    'postcode' => $vehicle->latest_owner->postcode
                ]
            ]);
        } catch (\Exception $exception) {
            \Log::debug('WeBuyCarsToday API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new ApiFailure('We Buy Cars Today'));
            // Return false so the application doesn't crash
            return false;
        }

        // Get the body
        $body = $response->getBody()->getContents();
        // Parse it
        $body = json_decode($body, true);

        \Log::debug('WBCT API Response:\n\n'.print_r($body, true));

        if (!$body || !isset($body['data']) || !isset($body['data']['car_details']) || !isset($body['data']['car_details']['regNo'])) {
            \Log::debug('WeBuyCarsToday API Failure problem with response:');
            return false;
        }

        $numberplate = $body['data']['car_details']['regNo'];

        if ($body['status'] == 'SUCCESS') {
            $id = $body['data']['registration_id'];
            $valuationPrice = $body['data']['quoted_price']['price'];

            if ($valuationPrice < self::MINIMUM_ACCEPTABLE_PRICE) {
                return false;
            }

            return [
                'valuation' => [
                    'collection' => $body['data']['quoted_price']['price'],
                    'dropoff' => null,
                    'accept_uri' => null,
                    'temp_ref' => $id,
                    // 'accept_uri' => "https://www.webuycarstoday.co.uk/offer/{$numberplate}/{}/secure",
                    'admin_fee' => $body['data']['quoted_price']['appointment_fee']
                ]
            ];
        }
    }
}
