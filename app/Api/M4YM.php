<?php
namespace JamJar\Api;

use Log;
use JamJar\Vehicle;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;

class M4YM implements ApiInterface
{
    protected $apiKey;
    protected $baseUrl;

    public function __construct($apiKey = null)
    {
        $this->apiKey = empty($apiKey) ? env('M4YM_API_KEY') : $apiKey;
        $this->baseUrl = 'http://uat.m4ym.com/';
    }

    public function getVehicle($vrm)
    {
        //
    }

    public function getValuation(Vehicle $vehicle)
    {
        // Build our client
        $client = new HttpClient(
            [
            'base_uri' => $this->baseUrl,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'ClientAPIKey' => $this->apiKey
            ]
            ]
        );

        try {
            // Get a Valuation Reference
            $response = $client->post(
                '/',
                [
                'body' => json_encode(['RegistrationPlate' => $vehicle->numberplate])
                ]
            );
        } catch (\Exception $exception) {
            \Log::debug('Money4YourMotors API Failure:'. $exception->getMessage());
            // Report the Exception here.
            return false;
        }

        // Get the body
        $body = $response->getBody()->getContents();
        // Parse it
        $body = json_decode($body, true);
        // Store our Valuation Ref
        $valuationRef = $body['ValuationReference'];

        // Make a PUT request to 'update the valuation record'
        $data = [
            'VehicleMilage' => (int)$vehicle->meta->getOriginal('mileage'),
            'VehiclePrevOwners' => $vehicle->meta->number_of_owners,
            'VehicleOwnerFirstname' => $vehicle->latest_owner->firstname,
            'VehicleOwnerSurname' => $vehicle->latest_owner->lastname,
            'VehicleOwnerEmailAddress' => $vehicle->latest_owner->email,
            'VehicleOnwerPostcode' => $vehicle->latest_owner->postcode,
            'VehicleOwnerTelphoneNumber' => $vehicle->latest_owner->telephone
        ];

        $response = $client->put(
            "/{$valuationRef}",
            [
            'body' => json_encode($data),
            ]
        );

        \Log::debug('M4YM API Response:\n\n'.print_r($response, true));

        // If this returns 200 ok, continue
        if ($response->getStatusCode() === 200) {

            try {
                // Get the valuation from M4YM.
                $response = $client->get("/{$valuationRef}");
            } catch (\Exception $exception) {
                // Report the Exception here.
                Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                            ->notify(new ApiFailure('Money 4 Your Motors'));
                // Log the error message so we have something to go on
                \Log::debug('Money4YourMotors API Failure:'. $exception->getMessage());
                // Return false so the application doesn't crash
                return false;
            }

            //
            $body = json_decode($response->getBody()->getContents(), true);
            if (empty($body['ManualValuationRequired'])) {

                if ($body['ValuationPrice'] >= 100) {
                    $adminFee = ($body['ValuationPrice'] <= 5000) ? '49.99' : '68.99';
                }

                return [
                    'valuation' => [
                        'collection' => (int) $body['ValuationPrice'],
                        'dropoff' => null,
                        'accept_uri' => '/webhooks/m4ym/accept',
                        'admin_fee' => $adminFee,
                        'their_ref' => $valuationRef
                    ]
                ];
            }
        }
    }
}
