<?php

namespace JamJar\Api\Responses;

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class WWACAcceptedOfferResponse
{
    /** @var UuidInterface */
    public $valuationIdentifier;

    /** @var Carbon|null */
    public $datePurchase;

    /** @var Carbon|null */
    public $dateValued;

    /** @var int */
    public $valuationOffer = 0;

    /**
     * @param string $jsonResponse
     * @return array|WWACAcceptedOfferResponse[]
     */
    public static function createFromJsonResponse(string $jsonResponse): array
    {
        $body = json_decode($jsonResponse, true);

        $results = [];

        foreach ($body['ColumnMappings'] as $valuation) {
            $response = new WWACAcceptedOfferResponse();
            foreach ($valuation['Options'] as $field) {
                if ($field['Id'] == 'ValuationIdentifier') {
                    $response->valuationIdentifier = Uuid::fromString($field['Value']);
                }

                if ($field['Id'] == 'DatePurchased') {
                    $response->datePurchase = $field['Value'] ?
                        Carbon::createFromFormat('d/m/Y H:i:s', $field['Value']) :
                        null;
                }

                if ($field['Id'] == 'DateValued') {
                    $response->dateValued = $field['Value'] ?
                        Carbon::createFromFormat('d/m/Y H:i:s', $field['Value']) :
                        null;
                }

                if ($field['Id'] == 'ValuationOffer') {
                    $response->valuationOffer = $field['Value'];
                }
            }

            $results[] = $response;
        }

        return $results;
    }
}