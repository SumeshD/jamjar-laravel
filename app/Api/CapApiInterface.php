<?php
namespace JamJar\Api;

use JamJar\Vehicle;

interface CapApiInterface extends ApiInterface
{
    public function getVehicle($vrm);

    public function getValuation(Vehicle $vehicle, $vehicleType='CAR');

    public function getManufacturers($vehicleType = 'CAR');

    public function getModels($manufacturerId, $vehicleType='CAR');

    public function getDerivatives($modelId, $vehicleType='CAR');

    public function getModelIdForCapId($capId, $vehicleType='CAR');
}