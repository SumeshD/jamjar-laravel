<?php

namespace JamJar\Api;

use Carbon\Carbon;
use Ramsey\Uuid\UuidInterface;

class FakeWeWantAnyCarValuation
{
    /** @var UuidInterface */
    public $refId;

    /** @var Carbon */
    public $datePurchase;

    /** @var Carbon */
    public $dateValued;

    /** @var int */
    public $valuationOffer;

    public function __construct(UuidInterface $refId, Carbon $datePurchase, Carbon $dateValued, int $valuationOffer)
    {
        $this->refId = $refId;
        $this->datePurchase = $datePurchase;
        $this->dateValued = $dateValued;
        $this->valuationOffer = $valuationOffer;
    }
}
