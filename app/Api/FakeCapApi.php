<?php
namespace JamJar\Api;

use JamJar\Vehicle;

class FakeCapApi implements CapApiInterface
{
    public function getVehicle($vrm)
    {

    }

    public function getValuation(Vehicle $vehicle, $vehicleType='CAR')
    {
    }

    public function getManufacturers($vehicleType = 'CAR')
    {
        return [
            collect([
                'cap_manufacturer_id' => 47156,
                'name' => 'ABARTH',
                'vehicle_type' => strtoupper($vehicleType)
            ]),
            collect([
                'cap_manufacturer_id' => 25545,
                'name' => 'AC',
                'vehicle_type' => strtoupper($vehicleType)
            ]),
        ];
    }

    public function getModels($manufacturerId, $vehicleType='CAR')
    {
        return [
            collect([
                    'cap_model_id' => 80445,
                    'name' => '124 SPIDER',
                    'introduced' => 2016,
                    'discontinued' => 2018
            ]),
        ];
    }

    public function getDerivatives($modelId, $vehicleType='CAR')
    {
        return [
            collect([
                'cap_derivative_id' => 1,
                'name' => 'test',
            ]),
        ];
    }

    public function getModelIdForCapId($capId, $vehicleType='CAR')
    {

    }
}
