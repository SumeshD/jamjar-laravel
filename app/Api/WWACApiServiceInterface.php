<?php

namespace JamJar\Api;

use Carbon\Carbon;
use JamJar\Api\Responses\WWACAcceptedOfferResponse;

interface WWACApiServiceInterface
{
    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array|WWACAcceptedOfferResponse[]
     */
    public function getAcceptedValuations(Carbon $startDate, Carbon $endDate): array;
}