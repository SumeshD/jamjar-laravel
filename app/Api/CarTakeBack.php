<?php
namespace JamJar\Api;

use JamJar\Profile;
use JamJar\Services\Valuations\ExternalDataProviders\CTBDataProvider;
use JamJar\VehicleOwner;
use Log;
use Carbon\Carbon;
use JamJar\Vehicle;
use \SimpleXMLElement;
use Spatie\ArrayToXml\ArrayToXml;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;

class CarTakeBack implements ApiInterface
{
    const FAKE_NUMBERPLATE = 'ABC1234';

    protected $subscriberID;
    protected $password;
    protected $baseUrl;
    protected $client;

    public function __construct($subscriberID = null, $password = null)
    {
        $this->subscriberID = empty($subscriberID) ? env('CARTAKEBACK_API_USER') : $subscriberID;
        $this->password = empty($password) ? env('CARTAKEBACK_API_KEY') : $password;
        $this->baseUrl = (env('CARTAKEBACK_ENV') == 'sandbox' ? 'https://dev.cartakeback.com/' : 'https://fads.cartakeback.com/');

        $this->client = new HttpClient([
            'base_uri' => $this->baseUrl,
        ]);
    }

    public function getValuation(Vehicle $vehicle)
    {
        try {
            $response = $this->getValuationsResponse($vehicle);
        } catch (\Exception $e) {
            return false;
        }

        $body = $response->getBody()->getContents();

        // CTB has his own validation for numberplate, in this case we shoot again with fake one
        if ($this->isProblemWithNumberplate($body)) {
            try {
                $response = $this->getValuationsResponse($vehicle, true);
                $body = $response->getBody()->getContents();
            } catch (\Exception $e) {
                return false;
            }
        }

        $jsonError = json_decode($body, true);

        if (json_last_error() === JSON_ERROR_NONE) {
            $this->logError($jsonError, $vehicle);
            return false;
        }

        $body = simplexml_load_string((string)$body);

        $results = (array)$body->valuations;

        $valuations = collect($results)->map(
            function ($value, $key) use ($body) {
                $hasCollectionValuation = isset($value[0]);
                $hasDropoffValuation = isset($value[1]);
                return [
                    'collection' => $hasCollectionValuation == true ? (int) $value[0]->amount : null,
                    'dropoff'   => $hasDropoffValuation == true ? (int) $value[1]->amount : null,
                    'distance'   => $hasDropoffValuation == true ? (int) $value[1]->servicesOffered->distanceToNearestDriveInCentre : null,
                    'accept_uri' => (string) $body->retrievalUrl,
                    'partner_vehicle_ref' => (string) $body->identification->partnervehicleref,
                ];
            }
        );

        return $valuations;
    }

    public function tryToUpdateCustomerData(string $partnerVehicleRef, $vehicle)
    {
        try {
            $requestXml = $this->buildCustomerDataUpdate($vehicle, $partnerVehicleRef);

            $this->client->post(
                'api/enquiry/customerdataupdate',
                [
                    'body' => $requestXml,
                    'headers' => [
                        'X-API-USER' => $this->subscriberID,
                        'X-API-KEY' => $this->password
                    ]
                ]
            );
        } catch (\Exception $exception) {
            \Log::debug('CarTakeBack API (update customer only) Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new ApiFailure('CarTakeBack'));
        }
    }

    public function getVehicle($vrm)
    {
        //
    }

    protected function buildNewEnquiryRequest(Vehicle $vehicle, $useFakeNumberplate = false)
    {

        $hasReasons = false;
        if($vehicle->meta->non_runner_reason) {

            $hasReasons = true;
            $reasons = array_column(json_decode($vehicle->meta->non_runner_reason),'title');
        }

        $serviceHistory = $vehicle->meta->service_history ? CTBDataProvider::convertVehicleServiceHistoryToCTBVehicleHistory($vehicle->meta->service_history) : null;
        $mot = $vehicle->meta->mot ? CTBDataProvider::convertVehicleMotToCTBMot($vehicle->meta->mot) : null;

        $vehicleRequest = [
            'Identification' => [
                'VehicleID' => $vehicle->id,
                'RequestID' => $vehicle->id,
                'VRM' => $useFakeNumberplate ? self::FAKE_NUMBERPLATE : $vehicle->numberplate,
                'Ref' => 'Jamjar-U'.$vehicle->latest_owner->user_id.'-V'.$vehicle->id,
                'PartnerVehicleRef' => '',
                'PartnerValuationRef' => ''
            ],
            'VehicleData' => [
                'RegNoWarning' => 0,
                'Age' => $vehicle->meta->age,
                'CustomData' => [
                    'MotRemaining' => $mot,
                    'ServiceHistory' => $serviceHistory,
                    'CurrentMileage' => $vehicle->meta->mileage,
                    'PostCode' => $vehicle->latest_owner->postcode,
                    'NonRunner' => ($vehicle->meta->non_runner == 1) ? 'true' : 'false',
                    'WrittenOff' => ($vehicle->meta->write_off == 1) ? 'true' : 'false',
                    'NonRunnerReason' => [
                        'AccidentDamage' => $hasReasons ? (in_array('Accident Damage', $reasons) == true) ? 'true' : 'false' : null,
                        'Alternator' => $hasReasons ? (in_array('Alternator', $reasons) == true) ? 'true' : 'false' : null,
                        'Battery' => $hasReasons ? (in_array('Battery', $reasons) == true) ? 'true' : 'false' : null,
                        'Brakes' => $hasReasons ? (in_array('Brakes', $reasons) == true) ? 'true' : 'false' : null,
                        'CamBelt' =>  $hasReasons ? (in_array('Cam Belt', $reasons) == true) ? 'true' : 'false' : null,
                        'CamShaft' => $hasReasons ? (in_array('Cam Shaft', $reasons) == true) ? 'true' : 'false' : null,
                        'Clutch' => $hasReasons ? (in_array('Clutch', $reasons) == true) ? 'true' : 'false' : null,
                        'Corrosion' => $hasReasons ? (in_array('Corrosion/Excessive Rust', $reasons) == true) ? 'true' : 'false' : null,
                        'DriveShaft' => $hasReasons ? (in_array('Driveshaft', $reasons) == true) ? 'true' : 'false' : null,
                        'Electrical' => $hasReasons ? (in_array('Electrical', $reasons) == true) ? 'true' : 'false' : null,
                        'EngineSeized' => $hasReasons ? (in_array('Engine Seized', $reasons) == true) ? 'true' : 'false' : null,
                        'GearBox' => $hasReasons ? (in_array('Gearbox', $reasons) == true) ? 'true' : 'false' : null,
                        'HeadGasket' => $hasReasons ? (in_array('Head Gasket', $reasons) == true) ? 'true' : 'false' : null,
                        'MissingEngine' => null,
                        'MissingGearBox' => null,
                        'NoValidInsurance' => null,
                        'NoValidRoadTax' => null,
                        'PowerSteering' => $hasReasons ? (in_array('Power Steering', $reasons) == true) ? 'true' : 'false' : null,
                        'Steering' => $hasReasons ? (in_array('Steering', $reasons) == true) ? 'true' : 'false' : null,
                        'Suspension' => $hasReasons ? (in_array('Suspension', $reasons) == true) ? 'true' : 'false' : null,
                        'TimingChain' => $hasReasons ? (in_array('Timing Chain', $reasons) == true) ? 'true' : 'false' : null,
                        'Turbo' => $hasReasons ? (in_array('Turbo', $reasons) == true) ? 'true' : 'false' : null,
                        'Tyres' => $hasReasons ? (in_array('Tyres', $reasons) == true) ? 'true' : 'false' : null,
                    ],
                ],
                'CapData' => [
                    'CapID' => $vehicle->cap_id,
                    'Manufacturer' => $vehicle->meta->manufacturer,
                    'Model' => $vehicle->meta->model,
                    'VehicleType' => strtoupper($vehicle->type)
                ]
            ]
        ];

        $xml = ArrayToXml::convert($vehicleRequest, 'Request');

        return $xml;
    }


    protected function buildCustomerDataUpdate(Vehicle $vehicle, string $partnerVehicleRef)
    {
        $profile = Profile::where('user_id', '=', $vehicle->latest_owner->user_id)->first();

        /** @var VehicleOwner $vehicleOwner */
        $vehicleOwner = $vehicle->latest_owner;

        $phoneNumber = $vehicleOwner->getPhoneForCarTakeBack();

        $customerRequest = [
            'Identification' => [
                'VehicleID' => $vehicle->id,
                'RequestID' => $vehicle->id,
                'VRM' => $vehicle->numberplate,
                'Ref' => 'Jamjar-U'.$vehicle->latest_owner->user_id.'-V'.$vehicle->id,
                'PartnerVehicleRef' => $partnerVehicleRef,
                'PartnerValuationRef' => null,
            ],
            'CustomerData' => [
                'DaytimeTelNumber' => $phoneNumber,
                'EmailAddress' => $vehicle->latest_owner->email,
                'FullName' => $vehicle->latest_owner->firstname . ' ' . $vehicle->latest_owner->lastname,
                'FirstName' => $vehicle->latest_owner->firstname,
                'LastName' => $vehicle->latest_owner->lastname,
                'PostCode' => $vehicle->latest_owner->postcode,
                'AddressCity' => $profile->city,
                'AddressLine1' => $profile->address_line_one,
                'AddressLine2' => $profile->address_line_two,
                'AddressLine3' => null,
            ]
        ];

        $xml = ArrayToXml::convert($customerRequest, 'CustomerDataUpdate');

        return $xml;
    }

    /**
     * @param Vehicle $vehicle
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    private function getValuationsResponse(Vehicle $vehicle, bool $useFakeNumberplate = false)
    {
        $requestXml = $this->buildNewEnquiryRequest($vehicle, $useFakeNumberplate);

        try {
            $response = $this->client->post('api/enquiry/newenquiry', [
                'body' => $requestXml,
                'headers' => [ 'X-API-USER' => $this->subscriberID,  'X-API-KEY' => $this->password ]
            ]);
        } catch (\Exception $exception) {
            \Log::debug('CarTakeBack API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new ApiFailure('CarTakeBack'));
            // Return false so the application doesn't crash

            throw new \Exception();
        }

        return $response;
    }

    private function logError($jsonError, $vehicle)
    {
        Log::error(
            $jsonError['identification']['partnervehicleref'],
            [
                'vehicle' => [
                    'id' => $vehicle->id,
                    'vrm' => $vehicle->numberplate,
                    'meta' => [
                        'manufacturer' => $vehicle->meta->manufacturer,
                        'model' => $vehicle->meta->model,
                        'derivative' => $vehicle->meta->derivative,
                        'plate_year' => $vehicle->meta->plate_year,
                        'mileage' => $vehicle->meta->mileage,
                    ]
                ]
            ]
        );
    }

    private function isProblemWithNumberplate($body)
    {
        $jsonError = json_decode($body, true);

        return
            $jsonError['identification'] &&
            $jsonError['identification']['partnervehicleref'] &&
            $jsonError['identification']['partnervehicleref'] == 'Error: VRM format incorrect';
    }

}
