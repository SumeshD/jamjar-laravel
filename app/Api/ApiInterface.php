<?php

namespace JamJar\Api;

interface ApiInterface
{
    public function getValuation(\JamJar\Vehicle $vehicle);

    public function getVehicle($vrm);
}
