<?php

namespace JamJar\Api;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

class FakeHttpClient implements ClientInterface
{
    public $currentAttempts = 0;

    public $failAttempts = 3;

    /**
     * @param string|UriInterface $uri
     * @param array $options
     * @return ResponseInterface
     *
     * @throws GuzzleException
     */
    public function get($uri, $options = [])
    {
        if ($this->currentAttempts++ < $this->failAttempts) {
            throw new BadResponseException('error', new Request('get', $uri));
        }

        return new Response(200, [], $this->getXmlVehicleResponse());
    }

    public function send(RequestInterface $request, array $options = [])
    {
    }

    public function sendAsync(RequestInterface $request, array $options = [])
    {
    }

    public function request($method, $uri, array $options = [])
    {
    }

    public function requestAsync($method, $uri, array $options = [])
    {
    }

    public function getConfig($option = null)
    {
    }

    private function getXmlVehicleResponse()
    {
        return '<?xml version="1.0" encoding="utf-8"?>
            <RESPONSE>
              <SUCCESS>true</SUCCESS>
              <ERRORMESSAGE>
              </ERRORMESSAGE>
              <TIMESTAMP>17/07/2019 16:27:51</TIMESTAMP>
              <AUDITID>86895680</AUDITID>
              <LOOKUP_VRM>NV08TXO</LOOKUP_VRM>
              <LOOKUP_VIN />
              <MONTHLYLOOKUPLIMITEXCEEDED>False</MONTHLYLOOKUPLIMITEXCEEDED>
              <AUTOCORRECTED>False</AUTOCORRECTED>
              <AUTOCORRECTEDREASON>
              </AUTOCORRECTEDREASON>
              <MATCHLEVEL>
                <DVLA>1</DVLA>
                <DVLAKEEPER>1</DVLAKEEPER>
                <DVLABASIC>1</DVLABASIC>
                <SMMT>0</SMMT>
                <CAP>1</CAP>
                <ALTERNATIVEVRMS>0</ALTERNATIVEVRMS>
                <ALTERNATIVEDERIVATIVES>0</ALTERNATIVEDERIVATIVES>
              </MATCHLEVEL>
              <DATA>
                <DVLA>
                  <VRM>NV08TXO</VRM>
                  <VIN>W0L0SDL0886089298</VIN>
                  <MANUFACTURER>VAUXHALL</MANUFACTURER>
                  <MODEL>CORSA SXI A/C</MODEL>
                  <FUELTYPE>Petrol</FUELTYPE>
                  <BODYTYPE>3 Door Hatchback</BODYTYPE>
                  <DOORS>3</DOORS>
                  <SEATING>5</SEATING>
                  <WHEELPLAN>2 Axle Rigid Body</WHEELPLAN>
                  <REGISTRATIONDATE>20080613</REGISTRATIONDATE>
                  <MANUFACTUREDDATE>20080613</MANUFACTUREDDATE>
                  <FIRSTREG_DATE>20080613</FIRSTREG_DATE>
                  <FIRSTREG_USEDBEFORE>0</FIRSTREG_USEDBEFORE>
                  <CO2>146</CO2>
                  <ENGINECAPACITY>1229</ENGINECAPACITY>
                  <ENGINENUMBER>19NU7278</ENGINENUMBER>
                  <ISSCRAPPED>No</ISSCRAPPED>
                  <SCRAPPEDDATE>
                  </SCRAPPEDDATE>
                  <ISEXPORTED>No</ISEXPORTED>
                  <EXPORTEDDATE>
                  </EXPORTEDDATE>
                  <ISIMPORTED>No</ISIMPORTED>
                  <IMPORTEDDATE>
                  </IMPORTEDDATE>
                  <COLOUR>SILVER</COLOUR>
                  <MASSINSERVICE>1130</MASSINSERVICE>
                  <MAXMASS>0</MAXMASS>
                  <MAXNETPOWER>59</MAXNETPOWER>
                  <MAXTOW_BRAKED>850</MAXTOW_BRAKED>
                  <MAXTOW_UNBRAKED>450</MAXTOW_UNBRAKED>
                  <SOUNDLEVEL_STATIONARY>81</SOUNDLEVEL_STATIONARY>
                  <SOUNDLEVEL_ENGINE>4200</SOUNDLEVEL_ENGINE>
                  <SOUNDLEVEL_DRIVEBY>72</SOUNDLEVEL_DRIVEBY>
                  <PREVIOUSAQUISITION>20170526</PREVIOUSAQUISITION>
                  <PREVIOUSDISPOSAL>20170217</PREVIOUSDISPOSAL>
                  <PREVIOUSKEEPERS>1</PREVIOUSKEEPERS>
                  <CHERISHEDTRANSFERMARKER>No</CHERISHEDTRANSFERMARKER>
                  <TRANSFERDATE>
                  </TRANSFERDATE>
                </DVLA>
                <CAP>
                  <VEHICLETYPE_ID>1</VEHICLETYPE_ID>
                  <VEHICLETYPE>Car</VEHICLETYPE>
                  <CAPID>34969</CAPID>
                  <CAPCODE>VACO12SXA3HPIM  3   </CAPCODE>
                  <ENHANCEDCAPCODE>0103496920090803200908030332075620027130</ENHANCEDCAPCODE>
                  <ENHANCEDCAPCODE_IDENTIFIER>010349692009080320090803</ENHANCEDCAPCODE_IDENTIFIER>
                  <ENHANCEDCAPCODE_TECNICAL>0332075620027130</ENHANCEDCAPCODE_TECNICAL>
                  <MANUFACTURER_CODE>10968</MANUFACTURER_CODE>
                  <MANUFACTURER>VAUXHALL</MANUFACTURER>
                  <RANGE_CODE>208</RANGE_CODE>
                  <RANGE>CORSA</RANGE>
                  <MODEL_CODE>37302</MODEL_CODE>
                  <MODEL>CORSA HATCHBACK</MODEL>
                  <MODELDATEDESC_SHORT>(06 - 10)</MODELDATEDESC_SHORT>
                  <MODELDATEDESC_LONG>(2006 - 2010)</MODELDATEDESC_LONG>
                  <DERIVATIVE>1.2i 16V SXi 3dr [AC]</DERIVATIVE>
                  <DERIVATIVEDATEDESC_SHORT>(06 - 10)</DERIVATIVEDATEDESC_SHORT>
                  <DERIVATIVEDATEDESC_LONG>(2006 - 2010)</DERIVATIVEDATEDESC_LONG>
                  <INTRODUCED>20061014</INTRODUCED>
                  <DISCONTINUED>20100101</DISCONTINUED>
                  <RUNOUTDATE>20100530</RUNOUTDATE>
                  <DOORS>3</DOORS>
                  <DRIVETRAIN>Front Wheel Drive</DRIVETRAIN>
                  <FUELDELIVERY>Injection</FUELDELIVERY>
                  <TRANSMISSION>Manual</TRANSMISSION>
                  <FUELTYPE>Petrol</FUELTYPE>
                  <PLATE>2008 08</PLATE>
                  <PLATE_SEQNO>87</PLATE_SEQNO>
                  <PLATE_YEAR>2008</PLATE_YEAR>
                  <PLATE_MONTH>3</PLATE_MONTH>
                </CAP>
              </DATA>
            </RESPONSE>';
    }

}
