<?php

namespace JamJar\Api;

use Carbon\Carbon;
use JamJar\Api\Responses\WWACAcceptedOfferResponse;
use JamJar\Vehicle;

class FakeWeWantAnyCar implements ApiInterface, WWACApiServiceInterface
{
    /** @var array|FakeWeWantAnyCarValuation[] */
    private $fakeValuations;

    public function __construct(array $fakeValuations = [])
    {
        $this->fakeValuations = $fakeValuations;
    }

    public function getVehicle($vrm)
    {
    }

    public function getValuation(Vehicle $vehicle)
    {
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array|WWACAcceptedOfferResponse[]
     */
    public function getAcceptedValuations(Carbon $startDate, Carbon $endDate): array
    {
        $response = $this->prepareResponse($startDate, $endDate);

        return WWACAcceptedOfferResponse::createFromJsonResponse($response);
    }

    private function prepareResponse(Carbon $startDate, Carbon $endDate): string
    {
        $jsonResponse = [];
        $jsonResponse['ValuationResponseStatuses'] = [
            ['Code' => '100A', 'Description' => 'Success'],
            ['Code' => '100B', 'Description' => '0.13'],
        ];

        $valuations = [];

        foreach ($this->fakeValuations as $fakeValuation) {
            if ($fakeValuation->datePurchase < $startDate || $fakeValuation->datePurchase > $endDate) {
                continue;
            }

            $valuation = [];
            $valuation['Options'] = [];
            $valuation['Options'][] = ['Id' => 'ValuationIdentifier', 'Value' => $fakeValuation->refId->toString()];
            $valuation['Options'][] = ['Id' => 'DatePurchased', 'Value' => $fakeValuation->datePurchase->format('d/m/Y H:i:s')];
            $valuation['Options'][] = ['Id' => 'DateValued', 'Value' => $fakeValuation->dateValued->format('d/m/Y H:i:s')];
            $valuation['Options'][] = ['Id' => 'ValuationOffer', 'Value' => $fakeValuation->valuationOffer];

            $valuations[] = $valuation;
        }

        $jsonResponse['ColumnMappings'] = $valuations;

        return json_encode($jsonResponse);
    }
}
