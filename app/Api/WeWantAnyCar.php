<?php

namespace JamJar\Api;

use Carbon\Carbon;
use JamJar\Api\Responses\WWACAcceptedOfferResponse;
use Log;
use JamJar\Vehicle;
use JamJar\Api\ApiInterface;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;

class WeWantAnyCar implements ApiInterface, WWACApiServiceInterface
{
    const MINIMUM_ACCEPTABLE_PRICE = 1000;
    const MAXIMUM_ACCEPTABLE_PRICE = 44999;
    const VALUATION_PURCHASED_STATUS = 3;
    const VALUATION_SOLD_STATUS = 4;

    public $environment;
    public $key;
    public $partnerIdentifier;

    public function __construct($environment = null, $key = null, $partnerIdentifier = null)
    {
        $this->environment = env('WWAC_ENV');
        $this->key = env('WWAC_ENV') == 'sandbox' ? env('WWAC_SANDBOX_API_KEY') : env('WWAC_LIVE_API_KEY');
        $this->partnerIdentifier = env('WWAC_ENV') == 'sandbox' ? env('WWAC_SANDBOX_PARTNER_IDENTIFIER') : env('WWAC_LIVE_PARTNER_IDENTIFIER');
    }

    public function getVehicle($vrm)
    {
        //
    }

    public function getValuation(Vehicle $vehicle)
    {
        $base_uri = env('WWAC_ENV') == 'sandbox' ? 'https://api.wwac.co.uk/apiv4-sb/GuidePriceAPILookup/api' : 'https://api.wwac.co.uk/apiv4/GuidePriceAPILookup/api';

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . base64_encode($this->key),
                ]
            ]
        );

        $ValuationRequest = [
            'ColumnMappings' => [
                [
                    'Id' => 'VRM',
                    'Value' => $vehicle->numberplate,
                ],
                [
                    'Id' => 'Mileage',
                    'Value' => $vehicle->meta->getOriginal('mileage'),
                ],
                [
                    'Id' => 'DateFirstReg',
                    'Value' => $vehicle->meta->registration_date,
                ],
                [
                    'Id' => 'CustomerPostcode',
                    'Value' => $vehicle->latest_owner->postcode,
                ],
                [
                    'Id' => 'CustomerName',
                    'Value' => $vehicle->latest_owner->firstname . ' ' . $vehicle->latest_owner->lastname,
                ],
                [
                    'Id' => 'CustomerEmail',
                    'Value' => $vehicle->latest_owner->email,
                ],
                [
                    'Id' => 'CustomerPhone',
                    'Value' => $vehicle->latest_owner->telephone,
                ],
                [
                    'Id' => 'PartnerIdentifier',
                    'Value' =>  $this->partnerIdentifier,
                ],
                [
                    'Id' => 'ServiceHistory',
                    'Value' => $vehicle->meta->service_history,
                ],
                [
                    'Id' => 'VehicleCondition',
                    'Value' => 'Average',
                ],
                [
                    'Id' => 'WriteOff',
                    'Value' => $vehicle->meta->write_off ? 'True' : 'False',
                ],
                [
                    'Id' => 'PreviousOwners',
                    'Value' => (string)$vehicle->meta->number_of_owners,
                ],
                [
                    'Id' => 'Transmission',
                    'Value' => $vehicle->meta->transmission,
                ],
                [
                    'Id' => 'Mot',
                    'Value' => str_replace('+', '', $vehicle->meta->mot),
                ],
                [
                    'Id' => 'ExteriorColour',
                    'Value' => $vehicle->meta->colour->title,
                ]
            ]
        ];
        try {
            // Get the valuation from WWAC.
            $response = $client->post(
                "{$base_uri}/Vehicle/GetValuation",
                [
                    'body' => json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES),
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Authorization' => 'Basic ' . base64_encode($this->key),
                    ]
                ]
            );

        } catch (\Exception $exception) {
            \Log::debug('WeWantAnyCar API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                        ->notify(new ApiFailure('We Want Any Car'));
            // Return false so the application doesn't crash
            return false;
        }

        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);

        \Log::debug('WWAC API Response:\n\n'.print_r($body, true));

        $valuation = [];
        if (isset($body['ColumnMappings'])) {

            $DistanceRequest = [
                'ColumnMappings' => [
                    [
                        'Id' => 'CustomerPostcode',
                        'Value' => $vehicle->latest_owner->postcode
                    ]
                ]
            ];

            try {
                // Get the valuation from WWAC.
                $distanceResponse = $client->post(
                    "{$base_uri}/Vehicle/GetNearestBranches",
                    [
                        'body' => json_encode($DistanceRequest, JSON_UNESCAPED_SLASHES),
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Authorization' => 'Basic ' . base64_encode($this->key),
                        ]
                    ]
                );

                $distanceBody = json_decode($distanceResponse->getBody()->getContents(), true);

                $body['ColumnMappings'] = array_merge($distanceBody['ColumnMappings'][0]['Options'], $body['ColumnMappings']);
    
            } catch (\Exception $exception) {
                \Log::debug('WeWantAnyCar API Failure:'. $exception->getMessage());
                // Report the Exception here.
                Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                            ->notify(new ApiFailure('We Want Any Car'));
            }

            foreach ($body['ColumnMappings'] as $data) {
                $valuation[$data['Id']] = $data['Value'];
            }

            if (!$valuation || !isset($valuation['GuidePrice'])) {
                \Log::debug('WeWantAnyCar API Failure problem with response:');
                return false;
            }

            $valuationPrice = $valuation['GuidePrice'];

            if ($valuationPrice < self::MINIMUM_ACCEPTABLE_PRICE || $valuationPrice > self::MAXIMUM_ACCEPTABLE_PRICE) {
                return false;
            }

            return [
                'valuation' => [
                    'collection' => null,
                    'dropoff' => $valuationPrice,
                    'accept_uri' => $valuation['ValuationDeepLink'],
                    'distance' => $valuation['DrivingDistanceMiles'] ? $valuation['DrivingDistanceMiles'] : null,
                    'admin_fee' => $valuation['AdminFee'],
                    'valuation_validity' => $valuation['DateValidUntil'],
                    'valuation_identifier' => $valuation['ValuationIdentifier'],
                ]
            ];
        }
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array|WWACAcceptedOfferResponse[]
     */
    public function getAcceptedValuations(Carbon $startDate, Carbon $endDate): array
    {
        $base_uri = env('WWAC_ENV') == 'sandbox' ? 'https://api.wwac.co.uk/apiv4-sb/GuidePriceAPILookup/api' : 'https://api.wwac.co.uk/apiv4/GuidePriceAPILookup/api';

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . base64_encode($this->key),
                ]
            ]
        );

        $soldRequest = $this->getSoldRequest($startDate, $endDate);
        \Log::debug('WWAC getSoldRequest: '. print_r($soldRequest, true));
        $purchasedRequest = $this->getPurchasedRequest($startDate, $endDate);
        \Log::debug('WWAC getPurchasedRequest: '. print_r($purchasedRequest, true));

        try {
            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode($this->key),
            ];

            $soldResponse = $client->post("{$base_uri}/report/GetValuationsByStatus", [
                'body' => json_encode($soldRequest, JSON_UNESCAPED_SLASHES),
                'headers' => $headers
            ]);

            $purchasedResponse = $client->post("{$base_uri}/report/GetValuationsByStatus", [
                'body' => json_encode($purchasedRequest, JSON_UNESCAPED_SLASHES),
                'headers' => $headers
            ]);
        } catch (\Exception $exception) {
            \Log::debug('WeWantAnyCar API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                ->notify(new ApiFailure('We Want Any Car'));
            // Return false so the application doesn't crash
            return [];
        }

        $soldRecords = WWACAcceptedOfferResponse::createFromJsonResponse($soldResponse->getBody()->getContents());
        \Log::debug('WWAC soldRecords: '. print_r($soldRecords, true));
        $purchasedRecords = WWACAcceptedOfferResponse::createFromJsonResponse($purchasedResponse->getBody()->getContents());
        \Log::debug('WWAC purchasedRecords: '. print_r($purchasedRecords, true));

        return array_merge($soldRecords, $purchasedRecords);
    }

    private function getSoldRequest(Carbon $startDate, Carbon $endDate): array
    {
        return [
            'ColumnMappings' => [
                [
                    'Id' => 'ValuationStartDate',
                    'Value' => $startDate->format('d/m/Y'),
                ],
                [
                    'Id' => 'ValuationEndDate',
                    'Value' => $endDate->format('d/m/Y'),
                ],
                [
                    'Id' => 'ValuationStatus',
                    'Value' => self::VALUATION_SOLD_STATUS,
                ],
            ],
        ];
    }

    private function getPurchasedRequest(Carbon $startDate, Carbon $endDate)
    {
        return [
            'ColumnMappings' => [
                [
                    'Id' => 'ValuationStartDate',
                    'Value' => $startDate->format('d/m/Y'),
                ],
                [
                    'Id' => 'ValuationEndDate',
                    'Value' => $endDate->format('d/m/Y'),
                ],
                [
                    'Id' => 'ValuationStatus',
                    'Value' => self::VALUATION_PURCHASED_STATUS,
                ],
            ],
        ];
    }
}
