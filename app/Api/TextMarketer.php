<?php
namespace JamJar\Api;

use \GuzzleHttp\Client as HttpClient;

class TextMarketer
{
    protected $username;
    protected $password;
    protected $baseUrl;
    protected $format;

    public function __construct($username = null, $password = null)
    {
        $this->username = empty($username) ? env('TEXTMARKETER_API_USER') : $username;
        $this->password = empty($password) ? env('TEXTMARKETER_API_PASS') : $password;
        $this->baseUrl = env('TEXTMARKETER_API_URL') ?? 'https://api.textmarketer.co.uk/gateway/';
        $this->format = env('TEXTMARKETER_API_FORMAT') ?? 'xml';
    }

    public function sendSms($to, $from, $message)
    {
        \Log::debug('SMS sending attempt!');
        $args = [
            'username' => $this->username,
            'password' => $this->password,
            'option' => $this->format,
            'to' => $to,
            'message' => htmlentities($message),
            'orig' => urlencode($from),
        ];
        
        $queryString = http_build_query($args);
        $url = $this->baseUrl . '?' . $queryString;

        $fp = fopen($url, 'r');
        $response = fread($fp, 1024);
        \Log::debug('SMS sent: ' . $response);
        return $response;
    }
}
