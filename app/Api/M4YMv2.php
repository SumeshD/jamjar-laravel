<?php

namespace JamJar\Api;

use Log;
use JamJar\Vehicle;
use JamJar\Api\ApiInterface;
use JamJar\Notifications\ApiFailure;
use \GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Notification;

class M4YMv2 implements ApiInterface
{
    const MAXIMUM_ACCEPTABLE_PRICE = 44999;

    public $environment;
    public $key;
    public $partnerIdentifier;

    public function __construct($environment = null, $key = null, $partnerIdentifier = null)
    {
        $this->environment = env('M4YMv2_ENV');
        $this->key = env('M4YMv2_API_KEY');
        $this->partnerIdentifier = env('M4YMv2_CLIENT_IDENTIFIER');
    }

    public function getVehicle($vrm)
    {
        //
    }

    public function getValuation(Vehicle $vehicle)
    {
        $base_uri = env('M4YMv2_ENV') == 'sandbox' ? 'https://www.m4ym.com/affiliateapi-m4ym-sandbox/api' : 'https://www.m4ym.com/affiliateapi-m4ym-live/api';

        $client = new HttpClient(
            [
                'base_uri' => $base_uri,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'ClientIdentifier' => $this->partnerIdentifier,
                    'ClientAPIKey' => $this->key
                ],
                'verify' => false
            ]
        );


        $ValuationRequest = [
            'ColumnMappings' => [
                [
                    'Id' => 'RegistrationPlate',
                    'Value' => $vehicle->numberplate
                ]
            ]
        ];
        try {
            // dd(json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES), $this->partnerIdentifier, $this->key);
            // Get the valuation from M4YM.
            $response = $client->post(
                "{$base_uri}/Valuation/GetVehicle",
                [
                    'body' => json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES),
                    'headers' => [
                        'ClientIdentifier' => $this->partnerIdentifier,
                        'ClientAPIKey' => $this->key,
                        'Content-Type' => 'application/json'
                    ]
                ]
            );

        } catch (\Exception $exception) {
            \Log::debug('M4YM Version 2 API Failure:'. $exception->getMessage());
            // Report the Exception here.
            Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                        ->notify(new ApiFailure('M4YM Version 2 failure'));
            // Return false so the application doesn't crash
            return false;
        }

        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);

        if (isset($body['ColumnMappings'])) {
            foreach ($body['ColumnMappings'] as $data) {
                if($data['Id'] == 'TempReference'){
                    $tempRef = $data['Value'];
                }
            }
            if(isset($tempRef) && !empty($tempRef)){

                $ValuationRequest = [
                    'ColumnMappings' => [
                        [
                            'Id' => 'TempReference',
                            'Value' => $tempRef,
                        ],
                        [
                            'Id' => 'CustomerName',
                            'Value' => $vehicle->latest_owner->name,
                        ],
                        [
                            'Id' => 'CustomerEmail',
                            'Value' => $vehicle->latest_owner->email,
                        ],
                        [
                            'Id' => 'CustomerPhone',
                            'Value' => $vehicle->latest_owner->telephone,
                        ],
                        [
                            'Id' => 'CustomerPostcode',
                            'Value' => $vehicle->latest_owner->postcode,
                        ],
                        [
                            'Id' => 'PartnerIdentifier',
                            'Value' =>  $this->partnerIdentifier,
                        ],
                        [
                            'Id' => 'Mileage',
                            'Value' => $vehicle->meta->getOriginal('mileage'),
                        ],
                        [
                            'Id' => 'NumberOfPreviousOwners',
                            'Value' => (string)$vehicle->meta->number_of_owners,
                        ]
                    ]
                ];
                try {
                    // Trigger offer acceptance from M4YM.
                    $acceptResponse = $client->post(
                        "{$base_uri}/Valuation/GetMyValuation",
                        [
                            'body' => json_encode($ValuationRequest, JSON_UNESCAPED_SLASHES),
                            'headers' => [
                                'ClientIdentifier' => $this->partnerIdentifier,
                                'ClientAPIKey' => $this->key,
                                'Content-Type' => 'application/json'
                            ]
                        ]
                    );

                    $acceptResponse = $acceptResponse->getBody()->getContents();
                    $acceptResponse = json_decode($acceptResponse, true);

                    //Make response into nicer array
                    $responseData = [];
                    if(isset($acceptResponse['ColumnMappings']) && !empty($acceptResponse['ColumnMappings'])){
                        foreach($acceptResponse['ColumnMappings'] as $data){
                            $responseData[$data['Id']] = $data['Value'];
                        }
                    }


                } catch (\Exception $exception) {
                    \Log::debug('M4YM Version 2 API Failure:'. $exception->getMessage());
                    // Report the Exception here.
                    Notification::route('mail', env('API_FAILURE_EMAIL', 'info@jamjar.com'))
                                ->notify(new ApiFailure('M4YM Version 2 failure'));
                    // Return false so the application doesn't crash
                    return false;
                }
            }
            
            if(isset($responseData)){
                \Log::debug('M4YM Version 2 API Response:'. print_r($responseData, true));
            } else {
                return false;
            }



            if($responseData['ManualValuationRequired'] == 1){
                return false;
            } else {
                if ($responseData['VehicleValuation'] > self::MAXIMUM_ACCEPTABLE_PRICE) {
                    return false;
                }

                return [
                    'valuation' => [
                        'collection' => $responseData['VehicleValuation'],
                        'dropoff' => null,
                        'temp_ref' => $responseData['TempReference'],
                        'acceptance_uri' => null,
                        'admin_fee' => $responseData['M4YMAdminFee_short'],
                        'valuation_validity' => date("Y-m-d", strtotime("+5 days"))
                    ]
                ];
            }
        }
    }
    
}
