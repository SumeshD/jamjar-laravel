<?php

namespace JamJar\Api;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use JamJar\Exceptions\ApiResponseException;
use JamJar\Vehicle;
use \SimpleXMLElement;

class CapApi implements CapApiInterface
{
    /** how many fail calls to the CAP API are allowed */
    const CAP_API_ALLOWED_ATTEMPTS = 3;

    protected $subscriberID;
    protected $password;
    protected $baseUrl;

    /** @var ClientInterface|Client|FakeHttpClient */
    private $httpClient;

    public function __construct($subscriberID = null, $password = null)
    {
        $this->subscriberID = empty($subscriberID) ? config('capapi.username') : $subscriberID;
        $this->password = empty($password) ? config('capapi.password') : $password;
        $this->baseUrl = 'http://webservices.capnetwork.co.uk';

        $this->httpClient = resolve('GuzzleHttp\ClientInterface');
    }

    /** @return Client|ClientInterface|FakeHttpClient|mixed */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    public function getVehicle($vrm)
    {
        $args = [
            'SubscriberID' => $this->subscriberID,
            'Password' => $this->password,
            'vrm' => $vrm
        ];
        
        $queryString = http_build_query($args);

        $url = $this->baseUrl . "/capdvla_webservice/capdvla.asmx/DVLALookupVRM?{$queryString}";
        $response = $this->callForResponse($url);
        $body = simplexml_load_string((string)$response->getBody());

        return $body;
    }

    public function getValuation(Vehicle $vehicle, $vehicleType='CAR')
    {
        $type = ($vehicleType == 'Light Commercial Vehicle') ? 'LIGHTS' : 'CAR';

        $args = [
            'Subscriber_ID' => $this->subscriberID,
            'Password' => $this->password,
            'Database' => $type,
            'CAPID' => $vehicle->cap_id,
            'CAPCode' => '',
            'RegistrationDate' => Carbon::parse($vehicle->meta->getOriginal('registration_date'))->format('Y/m/d'),
            'DatasetDate' => Carbon::now()->format('Y/m/d'),
            'JustCurrent' => 'true',
            'Mileage' => str_replace(',', '', $vehicle->meta->getOriginal('mileage'))
        ];

        $queryString = http_build_query($args);

        $url = $this->baseUrl . "/CAPUsedValues_Webservice/capusedvalues.asmx/GetUsedValuation?{$queryString}";

        $response = $this->callForResponse($url);
        $body = (string) $response->getBody();

        $xml = new SimpleXMLElement($body);
        $xml->registerXPathNamespace('d', 'urn:schemas-microsoft-com:xml-diffgram-v1');
        $results = $xml->xpath("//UsedValues");

        $valuations = collect();

        collect($results)->map(
            function ($value, $key) use ($valuations) {
                $valuations->push(
                    [
                    'retail' => (int) $value->Valuation[0]->Retail,
                    'clean' => (int) $value->Valuation[0]->Clean,
                    'average' => (int) $value->Valuation[0]->Average,
                    'below' => (int) $value->Valuation[0]->Below
                    ]
                );
            }
        );
        
        return $valuations;
    }

    public function getManufacturers($vehicleType = 'CAR')
    {
        $queryString = http_build_query(
            [
            'Subscriber_ID' => $this->subscriberID,
            'Password' => $this->password,
            'Database' => $vehicleType,
            'JustCurrentManufacturers' => 'FALSE',
            'BodyStyleFilter' => ''
            ]
        );

        $url = $this->baseUrl . "/CAPVehicles_Webservice/capvehicles.asmx/GetCAPMan?{$queryString}";

        $response = $this->callForResponse($url);
        $body = (string) $response->getBody();

        $xml = new SimpleXMLElement($body);
        $xml->registerXPathNamespace('d', 'urn:schemas-microsoft-com:xml-diffgram-v1');
        $results = $xml->xpath("//Table");
        
        $manufacturers = [];

        foreach ($results as $result) {
            $manufacturers[] = collect(
                [
                'cap_manufacturer_id' => (int)$result->CMan_Code,
                'name' => (string)trim($result->CMan_Name),
                'vehicle_type' => strtoupper($vehicleType)
                ]
            );
        }

        return $manufacturers;
    }

    public function getModels($manufacturerId, $vehicleType='CAR')
    {
        $type = (in_array($vehicleType, ['Light Commercial Vehicle', 'LCV'])) ? 'LIGHTS' : 'CAR';

        $queryString = http_build_query(
            [
            'Subscriber_ID' => $this->subscriberID,
            'Password' => $this->password,
            'Database' => $type,
            'ManRanCode' => $manufacturerId,
            'ManRanCode_IsMan' => 'TRUE',
            'JustCurrentModels' => 'FALSE',
            'BodyStyleFilter' => ''
            ]
        );

        $url = $this->baseUrl . "/CAPVehicles_Webservice/capvehicles.asmx/GetCAPMod?{$queryString}";

        $response = $this->callForResponse($url);
        $body = (string) $response->getBody();

        $xml = new SimpleXMLElement($body);
        $xml->registerXPathNamespace('d', 'urn:schemas-microsoft-com:xml-diffgram-v1');
        $results = $xml->xpath("//Table");

        $models = [];

        foreach ($results as $result) {
            $models[] = collect(
                [
                'cap_model_id' => (int)$result->CMod_Code,
                'name' => (string)trim($result->CMod_Name),
                'introduced' => (string)trim($result->CMod_Introduced),
                'discontinued' => (string)trim($result->CMod_Discontinued)
                ]
            );
        }

        return $models;
    }

    public function getDerivatives($modelId, $vehicleType='CAR')
    {
        $queryString = http_build_query(
            [
            'Subscriber_ID' => $this->subscriberID,
            'Password' => $this->password,
            'Database' => strtoupper($vehicleType),
            'ModCode' => $modelId,
            'JustCurrentDerivatives' => 'FALSE'
            ]
        );

        $url = $this->baseUrl . "/CAPVehicles_Webservice/capvehicles.asmx/GetCAPDer?{$queryString}";

        $response = $this->callForResponse($url);
        $body = (string) $response->getBody();

        $xml = new SimpleXMLElement($body);
        $xml->registerXPathNamespace('d', 'urn:schemas-microsoft-com:xml-diffgram-v1');
        $results = $xml->xpath("//Table");

        $derivatives = [];

        foreach ($results as $result) {
            $derivatives[] = collect(
                [
                'cap_derivative_id' => (int)$result->CDer_ID,
                'name' => (string)trim($result->CDer_Name),
                ]
            );
        }

        return $derivatives;
    }

    public function getModelIdForCapId($capId, $vehicleType='CAR')
    {
        $queryString = http_build_query(
            [
            'Subscriber_ID' => $this->subscriberID,
            'Password' => $this->password,
            'Database' => strtoupper($vehicleType),
            'CAPID' => $capId,
            ]
        );

        $url = $this->baseUrl . "/CAPVehicles_Webservice/capvehicles.asmx/GetCAPDescriptionFromID?{$queryString}";

        $response = $this->callForResponse($url);
        $body = (string) $response->getBody();

        $xml = new SimpleXMLElement($body);
        $xml->registerXPathNamespace('d', 'urn:schemas-microsoft-com:xml-diffgram-v1');
        if (isset($xml->xpath("//Table")[0])) {
            $results = $xml->xpath("//Table")[0];

            $modelId = (int)$results->CVehicle_ModTextCode;
            
            return $modelId;
        }
    }

    /**
     * @param string $url
     * @param array $params = []
     * @return \Psr\Http\Message\ResponseInterface
     * @throws ApiResponseException
     */
    private function callForResponse(string $url, array $params = [])
    {
        for ($i = 0; $i < self::CAP_API_ALLOWED_ATTEMPTS; $i++) {
            try {

                return $this->httpClient->get($url, $params);

            } catch (GuzzleException $e) {
                usleep(500000); // wait 0.5 second
            }
        }

        throw new ApiResponseException();
    }

}
