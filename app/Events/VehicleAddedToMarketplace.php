<?php

namespace JamJar\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlert;
use JamJar\Vehicle;

class VehicleAddedToMarketplace
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var Vehicle */
    public $vehicle;

    public function __construct(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
