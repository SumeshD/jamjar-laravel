<?php

namespace JamJar\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CreateOfferModel
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $offer;
    public $model;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($offer, $model)
    {
        $this->offer = $offer;
        $this->model = $model;
    }
}
