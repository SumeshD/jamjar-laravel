<?php

namespace JamJar\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class SaleHasBeenRemoved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $buyer;
    public $seller;
    public $sale;
    public $newVehicle;
    public $oldVehicle;
    public $valuation;
    public $customerCancel;

    public function __construct($buyer, $seller, $sale, $newVehicle, $oldVehicle, $valuation, $customerCancel = false)
    {
        $this->buyer = $buyer;
        $this->seller = $seller;
        $this->sale = $sale;
        $this->newVehicle = $newVehicle;
        $this->oldVehicle = $oldVehicle;
        $this->valuation = $valuation;
        $this->customerCancel = $customerCancel;
    }
}
