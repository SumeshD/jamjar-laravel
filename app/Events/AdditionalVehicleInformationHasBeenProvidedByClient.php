<?php

namespace JamJar\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use JamJar\Vehicle;

class AdditionalVehicleInformationHasBeenProvidedByClient
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $vehicle;

    public function __construct(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
