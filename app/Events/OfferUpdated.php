<?php

namespace JamJar\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use JamJar\Offer;

class OfferUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $offer;
    public $models;
    public $derivatives;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($offer, $models, $derivatives)
    {
        $this->offer = $offer;
        $this->models = $models;
        $this->derivatives = $derivatives;
    }
}
