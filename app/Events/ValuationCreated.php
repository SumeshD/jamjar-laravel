<?php

namespace JamJar\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ValuationCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $valuations;
    public $vehicle;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($valuations, $vehicle)
    {
        $this->valuations = $valuations;
        $this->vehicle = $vehicle;
    }
}
