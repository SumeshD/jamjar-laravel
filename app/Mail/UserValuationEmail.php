<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;

class UserValuationEmail extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = '';

    public $user;
    public $vehicle;
    public $valuation;
    public $version;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $vehicle, $valuation, $version = 'v2')
    {
        $this->user = $user;
        $this->vehicle = $vehicle;
        $this->valuation = $valuation;
        $this->version = $version;
    }

    public function build()
    {

        $version = ($this->version!='v1') ? '.' . $this->version : '';

        $view = 'mail' . $version . '.user-good-news-valuation';

        $preheaderText = 'Accept your new highest offer of ' . $this->valuation->value . ' and sell your '. $this->vehicle->meta->manufacturer . ' today!';

        $vehicleType = ($this->vehicle->type=='Light Commercial Vehicle') ? 'van' : 'car';

        return $this->view($view)
            ->with(
                [
                    'user' => $this->user,
                    'vehicle' => $this->vehicle,
                    'valuation' => $this->valuation,
                    'preheaderText' => $preheaderText,
                    'vehicleType' => $vehicleType
                ]
            )
            ->withSwiftMessage(function ($message){
                $message->getHeaders()->addTextHeader('type', self::getType());
                $message->getHeaders()->addTextHeader('version', $this->version);
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->from('IMPROVED@jamjar.com')
            ->subject('🚗 Great news from jamjar.com, ' . $this->user->firstname . '! You have an improved offer for your ' . $vehicleType);
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {

        return [
            'user' => User::class,
            'valuation' => Valuation::class,
            'vehicle' => Vehicle::class,
            'version' => 'string'
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to customer when there is an improved offer';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'improved-offer';
    }

}
