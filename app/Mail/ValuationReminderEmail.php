<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;
use JamJar\VehicleDerivative;

class ValuationReminderEmail extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    public $user;
    public $valuation;
    public $vehicle;
    public $type;
    public $version;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $valuation, $vehicle, $type, $version = 'v2')
    {
        $this->user = $user;
        $this->valuation = $valuation;
        $this->vehicle = $vehicle;
        $this->type = $type;
        $this->version = $version;
    }

    public function build()
    {

        $version = ($this->version!='v1') ? '.' . $this->version : '';

        $view = 'mail' . $version . '.valuation-reminder';
        $preheaderText = '';
        $subject = 'Don\'t miss out on the best offer on your vehicle!';

        $vehicleType = ($this->vehicle->type=='Light Commercial Vehicle') ? 'van' : 'car';

        $emailType = '';

        if ($version!='v1') {


            $emailType = '';

            if ($this->type=='initial_reminder') {

                $preheaderText = $this->valuation->value . ' is the best price for your ' . $this->vehicle->meta->manufacturer . ' ' . $this->vehicle->meta->model . ".Think it's worth more? Use our Marketplace to see if you can improve your offers.";
                $subject = $this->user->first_name . ', accept the best offer for your ' . $this->vehicle->meta->manufacturer . ' now';

                $emailType = 'initial-reminder';
            }

            if ($this->type=='continuous_reminder') {

                $preheaderText = $this->valuation->value . ' is currently the best price for your ' . $this->vehicle->meta->manufacturer . ' ' . $this->vehicle->meta->model . ' but you could improve your offers by giving us more information about your ' . $vehicleType . '!';
                $subject = $this->user->first_name . ', don\'t miss out on the best offer for your ' . $this->vehicle->meta->manufacturer;

                $emailType = 'continuous-reminder';
            }

            if ($this->type=='final_reminder') {

                $preheaderText = 'If you\'re holding out for a higher offer, giving us more information about your ' . $vehicleType . ' could improve your offers! Use our Premium Marketplace system for free!';
                $subject = $this->user->first_name . ', your best offer of ' . $this->valuation->value . ' for your ' . $this->vehicle->meta->manufacturer . ' is about to expire';
                $subject = str_replace('&pound;', '£', $subject);

                $emailType = 'final-reminder';
            }

            $view = 'mail.'.$this->version.'.'.$emailType;

        }


        return $this->view($view)
            ->with([
                'type' => $this->type,
                'valuation' => $this->valuation,
                'vehicle' => $this->vehicle,
                'user' => $this->user,
                'vehicleType' => $vehicleType,
                'preheaderText' => $preheaderText
            ])
            ->subject($subject)
            ->withSwiftMessage(function ($message) use ($emailType){
                $message->getHeaders()->addTextHeader('type', $emailType);
                $message->getHeaders()->addTextHeader('version', $this->version);
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->from('no-reply@jamjar.com', 'jamjar.com');
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {

        return [
            'user' => User::class,
            'valuation' => Valuation::class,
            'vehicle' => Vehicle::class,
            'type' => 'string',
            'version' => 'string'
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to customer to remind them of valuations';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'valuation-reminder';
    }

}
