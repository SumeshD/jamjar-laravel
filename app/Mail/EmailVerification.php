<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\User;

class EmailVerification extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.user-verification';

    /** @var User */
    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->view(self::VIEW_NAME)
            ->with(['email_token' => $this->user->email_token])
            ->withSwiftMessage(function ($message){
                $message->getHeaders()->addTextHeader('type', self::getType());
                $message->getHeaders()->addTextHeader('version', 'v1');
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->subject('Welcome to Jamjar!')
            ->from('no-reply@jamjar.com', 'jamjar.com');
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'ordinary user' => User::class
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent when user create an account';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'user-verification';
    }
}
