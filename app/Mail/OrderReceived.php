<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\FundingAmount;
use JamJar\User;

class OrderReceived extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.order-received';

    protected $user;
    protected $product;
    protected $admin;

    public function __construct($user, $product, $admin)
    {
        $this->user = $user;
        $this->product = $product;
        $this->admin = $admin;
    }

    public function build()
    {
        if ($this->user) {
            return $this->view(self::VIEW_NAME)
                ->with(
                    [
                                'user' => $this->user,
                                'product' => $this->product,
                                'admin' => $this->admin
                            ]
                )
                ->withSwiftMessage(function ($message){
                    $message->getHeaders()->addTextHeader('type', self::getType());
                    $message->getHeaders()->addTextHeader('version', 'v1');
                    $message->getHeaders()->addTextHeader('description', self::getDescription());
                })
                ->subject('New Order Received - jamjar.com');
        }
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'matrix user with order' => User::class,
            'sold product' => FundingAmount::class,
            'admin user' => User::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to admin when partner adds some funds';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'order-received';
    }
}
