<?php

namespace JamJar\Mail;

trait BaseEmailBehaviour
{
    public static function getViewName(): string
    {
        return self::VIEW_NAME;
    }

    public static function getWarningMessage(): ?string
    {
        return null;
    }
}
