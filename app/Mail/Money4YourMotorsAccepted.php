<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Money4YourMotorsAccepted extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.m4ym-accepted';

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            return $this->view(self::VIEW_NAME)
                ->with(['data' => $this->data])
                ->withSwiftMessage(function ($message){
                    $message->getHeaders()->addTextHeader('type', self::getType());
                    $message->getHeaders()->addTextHeader('version', 'v1');
                    $message->getHeaders()->addTextHeader('description', self::getDescription());
                })
                ->subject('New Lead from jamjar.com');
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'Array (json) with fields: name, email, phone, postcode, vrm, (int)valuation, ref' => 'array',
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent when M4YM call back our API about sold vehicle';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'm4ym-accepted-sale';
    }
}
