<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\User;
use JamJar\Vehicle;
use phpDocumentor\Reflection\Types\Integer;

class NotEnoughFunds extends Mailable
{
    use Queueable, SerializesModels;

    const VIEW_NAME = 'mail.not-enough-funds';

    protected $user;
    protected $vehicle;
    protected $fee;
    protected $currentFunds;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Vehicle $vehicle, $fee, $currentFunds)
    {
        $this->user = $user;
        $this->vehicle = $vehicle;
        $this->fee = $fee;
        $this->currentFunds = $currentFunds;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->user) {
            return $this->view(self::VIEW_NAME)
                ->with(
                    [
                        'user' => $this->user,
                        'vehicle' => $this->vehicle,
                        'fee' => $this->fee/100,
                        'currentFunds' => $this->currentFunds/100
                    ]
                )
                ->withSwiftMessage(function ($message){
                    $message->getHeaders()->addTextHeader('type', self::getType());
                    $message->getHeaders()->addTextHeader('version', 'v1');
                    $message->getHeaders()->addTextHeader('description', self::getDescription());
                })
                ->subject('Not enough funds - jamjar.com');
        }
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'user' => User::class,
            'vehicle' => Vehicle::class

        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to partner when automated bidding rule cant be triggered because of lack of funds';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'auto-bidding-not-enough-funds';
    }
}
