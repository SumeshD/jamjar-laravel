<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;

class PartnerHasBeenOutbidEmail extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.partner-has-been-outbid';

    private $buyer;
    private $vehicle;
    private $lowerValuation;
    private $higherValuation;

    public function __construct($buyer, $vehicle, $lowerValuation, $higherValuation)
    {
        $this->buyer = $buyer;
        $this->vehicle = $vehicle;
        $this->lowerValuation = $lowerValuation;
        $this->higherValuation = $higherValuation;
    }

    public function build()
    {
        return $this->view(self::VIEW_NAME)
            ->with([
                'buyer' => $this->buyer,
                'vehicle' => $this->vehicle,
                'lowerValuation' => $this->lowerValuation,
                'higherValuation' => $this->higherValuation,
            ])
            ->withSwiftMessage(function ($message){
                $message->getHeaders()->addTextHeader('type', self::getType());
                $message->getHeaders()->addTextHeader('version', 'v1');
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->subject('Your offer is no longer the highest - Jamjar.com')
            ->from(env('MATRIX_ADMIN_EMAIL', 'info@jamjar.com'), 'jamjar.com');
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'buyer user' => User::class,
            'vehicle with owner' => Vehicle::class,
            'lower valuation' => Valuation::class,
            'higher valuation' => Valuation::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to buyer when his offer is no longer the highest';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'partner-has-been-outbid';
    }
}
