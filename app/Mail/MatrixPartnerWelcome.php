<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\User;

class MatrixPartnerWelcome extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.matrix-partner-welcome';

    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function build()
    {
        if ($this->user) {
            return $this->view(self::VIEW_NAME)
                ->with(
                    [
                                'user' => $this->user
                            ]
                )
                ->withSwiftMessage(function ($message){
                    $message->getHeaders()->addTextHeader('type', self::getType());
                    $message->getHeaders()->addTextHeader('version', 'v1');
                    $message->getHeaders()->addTextHeader('description', self::getDescription());
                })
                ->subject('Thanks for Applying to Become an Associate of jamjar.com')
                ->from(env('MATRIX_ADMIN_EMAIL', 'info@jamjar.com'), 'jamjar.com');
        }
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'matrix user' => User::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to matrix partner at the very beginning';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'matrix-partner-welcome';
    }
}
