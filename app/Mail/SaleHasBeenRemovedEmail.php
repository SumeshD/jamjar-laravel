<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use JamJar\Sale;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;

class SaleHasBeenRemovedEmail extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.sale-has-been-removed';

    private $seller;
    private $buyer;
    private $sale;
    private $newVehicle;
    private $oldVehicle;
    private $valuation;
    private $customerCancel;

    public function __construct($seller, $buyer, $sale, $newVehicle, $oldVehicle, $valuation, $customerCancel)
    {
        $this->seller = $seller;
        $this->buyer = $buyer;
        $this->sale = $sale;
        $this->newVehicle = $newVehicle;
        $this->oldVehicle = $oldVehicle;
        $this->valuation = $valuation;
        $this->customerCancel = $customerCancel;
    }

    public function build()
    {
        return $this->view(self::VIEW_NAME)
            ->with([
                'seller' => $this->seller,
                'buyer' => $this->buyer,
                'sale' => $this->sale,
                'newVehicle' => $this->newVehicle,
                'oldVehicle' => $this->oldVehicle,
                'valuation' => $this->valuation,
                'customerCancel' => $this->customerCancel
            ])
            ->withSwiftMessage(function ($message){
                $message->getHeaders()->addTextHeader('type', self::getType());
                $message->getHeaders()->addTextHeader('version', 'v1');
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->subject('Accepted Offer Cancelled - jamjar.com')
            ->from(env('MATRIX_ADMIN_EMAIL', 'info@jamjar.com'), 'jamjar.com');
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'seller user' => User::class,
            'buyer user with company' => User::class,
            'sale' => Sale::class,
            'new vehicle' => Vehicle::class,
            'old vehicle' => Vehicle::class,
            'removed valuation' => Valuation::class,
            'is offer cancelled by customer 1 = yes, 0 = fresh results' => 'bool',
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to buyer when seller get new results for vehicle and buyer received sale already';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'sale-has-been-removed';
    }
}
