<?php

namespace JamJar\Mail\MarketplaceAlerts;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use JamJar\Mail\BaseEmailBehaviour;
use JamJar\Mail\EmailDebuggerInterface;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlert;
use JamJar\User;
use JamJar\Vehicle;

class DailyEmail extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.marketplace-alerts.daily-alert';

    /** @var User */
    protected $user;

    /** @var array|Vehicle[] */
    private $vehicles;

    /** @var MarketplaceAlert[] */
    private $alerts;

    /**
     * DailyEmail constructor.
     * @param User $alertRecipient
     * @param array|Vehicle[] $vehicles
     * @param MarketplaceAlert[] $alerts
     */
    public function __construct(User $alertRecipient, array $vehicles, array $alerts)
    {
        $this->user = $alertRecipient;
        $this->vehicles = $vehicles;
        $this->alerts = $alerts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(self::VIEW_NAME)
            ->with(['user' => $this->user, 'vehicles' => $this->vehicles, 'alerts' => $this->alerts]
            )
            ->withSwiftMessage(function ($message){
                $message->getHeaders()->addTextHeader('type', self::getType());
                $message->getHeaders()->addTextHeader('version', 'v1');
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->subject('Marketplace Alert - jamjar.com')
            ->from(env('MATRIX_ADMIN_EMAIL', 'info@jamjar.com'), 'jamjar.com');
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'alert recipient user' => User::class,
            'collection of vehicles' => Vehicle::class.'[]',
            'marketplace alert' => MarketplaceAlert::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to partner once a day if there are vehicles related to alert';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'marketplace-alert-daily-email';
    }
}
