<?php

namespace JamJar\Mail\MarketplaceAlerts;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use JamJar\Mail\BaseEmailBehaviour;
use JamJar\Mail\EmailDebuggerInterface;
use JamJar\Model\MarketplaceAlerts\MarketplaceAlert;
use JamJar\User;
use JamJar\Vehicle;

class InstantEmail extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.marketplace-alerts.instant-alert';

    /** @var User */
    protected $user;

    /** @var Vehicle */
    private $vehicle;

    /** @var MarketplaceAlert */
    private $alert;

    public function __construct(User $alertRecipient, Vehicle $vehicle, MarketplaceAlert $alert)
    {
        $this->user = $alertRecipient;
        $this->vehicle = $vehicle;
        $this->alert = $alert;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(self::VIEW_NAME)
            ->with(['user' => $this->user, 'vehicle' => $this->vehicle, 'alert' => $this->alert])
            ->withSwiftMessage(function ($message){
                $message->getHeaders()->addTextHeader('type', self::getType());
                $message->getHeaders()->addTextHeader('version', 'v1');
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->subject('Marketplace Alert - jamjar.com')
            ->from(env('MATRIX_ADMIN_EMAIL', 'info@jamjar.com'), 'jamjar.com');
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'markerplace recipient user' => User::class,
            'vehicle' => Vehicle::class,
            'marketplace alert' => MarketplaceAlert::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent instantly when new vehicle is related to marketplace alert';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'marketplace-alert-instant-email';
    }
}
