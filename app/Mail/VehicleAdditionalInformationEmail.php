<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;

class VehicleAdditionalInformationEmail extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.additional-vehicle-information-provided';

    protected $buyer;
    protected $vehicle;
    protected $valuation;

    public function __construct(User $buyer,Vehicle $vehicle, Valuation $valuation)
    {
        $this->buyer = $buyer;
        $this->vehicle = $vehicle;
        $this->valuation = $valuation;
    }

    public function build()
    {
        return $this->view(self::VIEW_NAME)
            ->with(['vehicle' => $this->vehicle, 'company' => $this->buyer->company, 'valuation' => $this->valuation])
            ->withSwiftMessage(function ($message){
                $message->getHeaders()->addTextHeader('type', self::getType());
                $message->getHeaders()->addTextHeader('version', 'v1');
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->subject('Additional vehicle information has been provided on Jamjar');
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'buyer user with company' => User::class,
            'vehicle with owner' => Vehicle::class,
            'valuation sent by buyer' => Valuation::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to buyer when new vehicle data is provided by seller';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'vehicle-additional-information';
    }
}
