<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\User;

class MatrixAdminNewPartner extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.matrix-admin-new-partner';

    protected $user;
    protected $admin;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $admin)
    {
        $this->user = $user;
        $this->admin = $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->user) {
            $this->user->load('profile');
            return $this->view(self::VIEW_NAME)
                ->with(
                    [
                                'user' => $this->user,
                                'admin' => $this->admin
                            ]
                )
                ->withSwiftMessage(function ($message){
                    $message->getHeaders()->addTextHeader('type', self::getType());
                    $message->getHeaders()->addTextHeader('version', 'v1');
                    $message->getHeaders()->addTextHeader('description', self::getDescription());
                })
                ->subject('New Associate Application on jamjar.com');
        }
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'user' => User::class,
            'admin' => User::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to admin when new partner fill the application form';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'matrix-admin-new-partner';
    }
}
