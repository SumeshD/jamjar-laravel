<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use JamJar\User;

class VehicleOwnerPassword extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.user-random-password';

    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function build()
    {
        $password = Str::random();
        $user = User::where('email', $this->user->email)->first();
        if ($user) {
            $user->update(['password', bcrypt($password)]);
            return $this->view(self::VIEW_NAME)
                ->with(['password' => $password])
                ->withSwiftMessage(function ($message){
                    $message->getHeaders()->addTextHeader('type', self::getType());
                    $message->getHeaders()->addTextHeader('version', 'v1');
                    $message->getHeaders()->addTextHeader('description', self::getDescription());
                })
                ->subject('Your jamjar.com Password');
        }
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'user' => User::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to user with new password WARNING! IF YOU RENDER THIS VIEW PASSWORD WILL BE CHANGED!';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'vehicle-owner-password';
    }

    public static function getWarningMessage(): ?string
    {
        return 'Warning! If you render this view password for selected user will be changed automatically to random one!';
    }
}
