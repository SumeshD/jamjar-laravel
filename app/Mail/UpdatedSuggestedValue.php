<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\User;
use JamJar\VehicleDerivative;
use JamJar\VehicleManufacturer;
use JamJar\VehicleModel;

class UpdatedSuggestedValue extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.new-suggested-value';

    public $user;
   // public $currentPercentage;
   // public $previousPercentage;
   // public $derivative;
    public $suggestedCount;

    public function __construct($user, $suggestedCount)
    {
        $this->user = $user;
        $this->suggestedCount = $suggestedCount;
    }

    /*
    public function __construct($user, $currentPercentage, $previousPercentage, $derivative)
    {
        $this->user = $user;
        $this->currentPercentage = $currentPercentage;
        $this->previousPercentage = $previousPercentage;
        $this->derivative = $derivative;
    }

    public function build()
    {
        return $this->view(self::VIEW_NAME)
            ->with(
                [
                            'user' => $this->user,
                            'current' => $this->currentPercentage,
                            'previous' => $this->previousPercentage,
                            'derivative' => $this->derivative,
                            'model' => $this->derivative->model,
                            'manufacturer' => $this->derivative->model->manufacturer
                        ]
            )
            ->subject('Suggested Values have been updated on Jamjar');
    }
    */

    public function build()
    {
        return $this->view(self::VIEW_NAME)
            ->with(
                [
                    'user' => $this->user,
                    'suggestedCount' => $this->suggestedCount
                ]
            )
            ->withSwiftMessage(function ($message){
                $message->getHeaders()->addTextHeader('type', self::getType());
                $message->getHeaders()->addTextHeader('version', 'v1');
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->subject('Suggested Values have been updated on Jamjar');
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'partner user' => User::class,
            'current percentage' => 'int',
            'previous percentage' => 'int',
            'derivative' => VehicleDerivative::class,
            'model' => VehicleModel::class,
            'manufacturer' => VehicleManufacturer::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to partner when there is new suggested value for derivative';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'update-suggested-value';
    }
}
