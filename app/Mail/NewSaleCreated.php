<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\Company;
use JamJar\Sale;
use JamJar\Vehicle;

class NewSaleCreated extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.sale-received';

    protected $company;
    protected $vehicle;
    protected $sale;

    public function __construct($company, $vehicle, $sale)
    {
        $this->company = $company;
        $this->vehicle = $vehicle;
        $this->sale = $sale;
    }

    public function build()
    {
        if ($this->company) {
            return $this->view(self::VIEW_NAME)
                ->with(
                    [
                                'company' => $this->company,
                                'vehicle' => $this->vehicle,
                                'sale' => $this->sale
                            ]
                )
                ->withSwiftMessage(function ($message){
                    $message->getHeaders()->addTextHeader('type', self::getType());
                    $message->getHeaders()->addTextHeader('version', 'v1');
                    $message->getHeaders()->addTextHeader('description', self::getDescription());
                })
                ->subject('New Sale Agreed - jamjar.com');
        }
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'buyer company' => Company::class,
            'sold vehicle (there must be vehicle owner)' => Vehicle::class,
            'created sale' => Sale::class
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to buyer when seller accept the valuation';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'sale-received';
    }
}
