<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\User;

class Invoice extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.matrix-invoice';

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->user) {
            $order = $this->user->orders->last();
            return $this->view(self::VIEW_NAME)
                ->with(
                    [
                                'order' => $order,
                            ]
                )
                ->withSwiftMessage(function ($message){
                    $message->getHeaders()->addTextHeader('type', self::getType());
                    $message->getHeaders()->addTextHeader('version', 'v1');
                    $message->getHeaders()->addTextHeader('description', self::getDescription());
                })
                ->subject('Your Order Receipt - jamjar.com');
        }
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return ['matrix user with invoice' => User::class];
    }

    public static function getDescription(): string
    {
        return 'Email sent to partner when add funds to account';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'matrix-invoice';
    }
}
