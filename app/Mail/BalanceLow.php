<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\User;

class BalanceLow extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    /** @var string */
    const VIEW_NAME = 'mail.balance-low';

    /** @var User */
    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public static function getArgumentsTypes(): array
    {
        return ['matrix partner user' => User::class];
    }

    public static function getType(): string
    {
        return 'balance-low';
    }

    public static function getDescription(): string
    {
        return 'Email sent to matrix partner when balance is low';
    }

    public function build()
    {
        $funds = $this->user->funds ? $this->user->funds->funds : 0;

        return $this->view(self::VIEW_NAME)
            ->with(['balance' => $funds, 'user' => $this->user])
            ->withSwiftMessage(function ($message){
                $message->getHeaders()->addTextHeader('type', self::getType());
                $message->getHeaders()->addTextHeader('version', 'v1');
                $message->getHeaders()->addTextHeader('description', self::getDescription());
            })
            ->subject('Your Balance is low on jamjar.com')
            ->from('no-reply@jamjar.com', 'jamjar.com');
    }
}
