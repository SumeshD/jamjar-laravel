<?php

namespace JamJar\Mail;

interface EmailDebuggerInterface
{
    public static function getViewName(): string;

    /** @return array|string[] */
    public static function getArgumentsTypes(): array;

    public static function getDescription(): string;

    /** Unique email type */
    public static function getType(): string;

    public static function getWarningMessage(): ?string;
}
