<?php

namespace JamJar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use JamJar\Company;
use JamJar\Sale;
use JamJar\User;
use JamJar\Valuation;
use JamJar\Vehicle;

class AutomatedBiddingRuleMatched extends Mailable implements EmailDebuggerInterface
{
    use Queueable, SerializesModels, BaseEmailBehaviour;

    const VIEW_NAME = 'mail.automated-bidding-rule-matched';

    protected $valuation;
    protected $vehicle;
    protected $position;

    public function __construct($valuation, $vehicle, $position = 1)
    {
        $this->valuation = $valuation;
        $this->vehicle = $vehicle;
        $this->position = $position;
    }

    public function build()
    {
        if ($this->valuation) {
            return $this
                ->view(self::VIEW_NAME)
                ->with([
                    'valuation' => $this->valuation,
                    'vehicle' => $this->vehicle,
                    'seller' => $this->vehicle->owner->user,
                    'position' => $this->position
                ])
                ->withSwiftMessage(function ($message){
                    $message->getHeaders()->addTextHeader('type', self::getType());
                    $message->getHeaders()->addTextHeader('version', 'v1');
                    $message->getHeaders()->addTextHeader('description', self::getDescription());
                })
                ->subject('Jamjar RULE MATCH');
        }
    }

    /** @return array|string[] */
    public static function getArgumentsTypes(): array
    {
        return [
            'valuation' => Valuation::class,
            'vehicle with owner' => Vehicle::class,
        ];
    }

    public static function getDescription(): string
    {
        return 'Email sent to partner when automated bidding rule matched';
    }

    /** Unique email type */
    public static function getType(): string
    {
        return 'automated-bidding-rule-matched';
    }
}
