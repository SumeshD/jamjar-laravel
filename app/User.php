<?php

namespace JamJar;

use Illuminate\Auth\Events\Registered;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use JamJar\Jobs\SendVerificationEmail;
use JamJar\Profile;
use JamJar\Role;
use JamJar\UserFund;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_token', 'role_id', 'matrix_partner', 'verified', 'marketing_email', 'marketing_telephone', 'marketing_sms', 'send_additional_emails'
    ];

    /**
     * Log these attributes on model change
     *
     * @var array
     */
    protected static $logAttributes = [
        'name', 'email', 'password', 'email_token', 'role_id', 'matrix_partner', 'verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to carbon instances.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that we will append to the returned collection
     *
     * @var array
     */
    // protected $appends = ['credits', 'spent_credits'];

    /**
     * Boot the User Model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            Profile::firstOrCreate([
                'id'    => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'user_id' => $user->id,
            ]);
            
            if ($user->verified == 0) {
                if ($user->matrix_partner == 0) {
                    dispatch(new SendVerificationEmail($user));
                }
            }
        });

        static::deleting(function($user) {
            $user->profile()->delete();
            if ($user->isPartner()) {
                $user->offers()->delete();
                $user->company()->delete();
                $user->website()->delete();
                $user->orders()->delete();
                $user->funds()->delete();
                $user->statements()->delete();
            }
        });
    }

    /**
     * A profile (can) have a company
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class);
    }

    /**
     * A user has a profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    /**
     * A user belongs to a role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * A user has a partner website
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function website()
    {
        return $this->hasOne(PartnerWebsite::class);
    }

    /**
     * A user has many orders
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * A user has one fund row
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function funds()
    {
        return $this->hasOne(UserFund::class);
    }

    /**
     * A user has many statement entries
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statements()
    {
        return $this->hasMany(UserStatement::class);
    }

    /**
     * A user has many offers
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    /**
     * Is the current user a matrix partner
     *
     * @return boolean
     */
    public function isPartner()
    {
        // If they do not have a company they are not a matrix parter
        // fixes bug wherein checking the matrix partner checkbox
        // in the administration panel essentially breaks
        // entire site. @todo fix admin checkbox
        if (!$this->isStaff()) {
            if ($this->company == null) {
                // undo the matrix partner checkbox
                // and revert change to role
                $role = Role::where('name', 'Client')->first();
                if (!is_null($role)) {
                    $this->update(
                        [
                        'matrix_partner' => 0,
                        'role_id' => $role->id
                        ]
                    );
                }

                return false;
            }

            if (($this->matrix_partner == 1) || ($this->role->name == 'Partner')) {
                return true;
            }
        }

        return false;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Is the current user an administrator
     *
     * @return boolean
     */
    public function isAdmin()
    {
        if ($this->role->name == 'Administrator') {
            return true;
        }

        return false;
    }

    /**
     * Is the current user a member of staff
     *
     * @return boolean
     */
    public function isStaff()
    {
        if (($this->role->name == 'Staff') || ($this->role->name == 'Administrator')) {
            return true;
        }

        return false;
    }

    /**
     * Register a user
     *
     * @param  string      $username
     * @param  string      $email
     * @param  string|null $password
     * @param  array|null  $options  An optional array of arguments
     * @return \JamJar\User
     */
    public function registerUser($username, $email, $password=null, $options=[])
    {
        $password = empty($password) ? Hash::make(rand(0, 999999)) : Hash::make($password);

        $role = Role::where('name', 'Client')->first();

        $verified = false;
        $isPartner = false;

        if (is_array($options)) {
            if (isset($options['verified'])) {
                $verified = true;
            }
            if (isset($options['is_partner'])) {
                $isPartner = true;
            }
        }

        $user = User::firstOrCreate(
            [
            'name' => $username,
            'email' => $email,
            'password' => $password,
            'email_token' => base64_encode($email),
            'role_id' => $role->id,
            'verified' => $verified ? 1 : 0,
            'matrix_partner' => $isPartner ? 2 : 0,
            ]
        );

        if (is_array($options)) {
            if (isset($options['auto_login'])) {
                auth()->login($user);
            }
        }

        event(new Registered($user));
        return $user;
    }

    /**
     * Get the first name attribute
     *
     * @return array
     */
    public function getFirstNameAttribute()
    {
        return explode(' ', $this->name)[0];
    }

    /**
     * Get the last name attribute
     *
     * @return array
     */
    public function getLastNameAttribute()
    {
        $userNames = explode(' ', $this->name);

        return count($userNames) > 1 ? $userNames[1] : $userNames[0];
    }

    public function getSuggestedValuesCount(): int
    {
        $sql = 'select count(offer_derivatives.id) as suggested_values_count from offer_derivatives 
            left join offers on offers.id = offer_derivatives.offer_id
            right join suggested_values on suggested_values.cap_derivative_id = offer_derivatives.cap_derivative_id 
                and suggested_values.cap_model_id = offer_derivatives.cap_model_id
            where offers.user_id = :userId and finished = 1 and active = 1 and offer_derivatives.base_value < suggested_values.percentage and offers.accept_write_offs = 0 and offers.acceptable_defects = "a:0:{}"';

        $results = DB::select(DB::raw($sql), [
            'userId' => $this->id,
        ]);

        return (int) $results[0]->suggested_values_count;
    }

    /** @return OfferDerivative[] */
    public function getSuggestedValues()
    {
        $sql = 'select 
                offer_derivatives.id as id, 
                offer_derivatives.id as offer_derivative_id, 
                offers.name as offer_name, 
                offers.slug as offer_uuid, 
                offer_derivatives.base_value as base_value, 
                suggested_values.percentage as suggested_value
                from offer_derivatives 
            left join offers on offers.id = offer_derivatives.offer_id
            right join suggested_values on suggested_values.cap_derivative_id = offer_derivatives.cap_derivative_id 
                and suggested_values.cap_model_id = offer_derivatives.cap_model_id
            where offers.user_id = :userId and finished = 1 and active = 1 and offer_derivatives.base_value < suggested_values.percentage and offers.accept_write_offs = 0 and offers.acceptable_defects = "a:0:{}"';

        return DB::select(DB::raw($sql), [
            'userId' => $this->id,
        ]);
    }

    /** @return OfferDerivative[] */
    public function getSuggestedValuesWithModelsAndDerivatives($pageNumber = 1, $perPage = 1)
    {
        $sql = 'select 
                offer_derivatives.id as id, 
                offer_derivatives.id as offer_derivative_id, 
                offers.id as offer_id, 
                offers.name as offer_name, 
                offers.slug as offer_uuid, 
                offer_derivatives.base_value as base_value, 
                suggested_values.percentage as suggested_value,
                offer_derivatives.cap_derivative_id as cap_derivative_id,
                offer_derivatives.cap_model_id as cap_model_id,
                vehicle_manufacturers.name as manufacturer_name,
                vehicle_manufacturers.id as manufacturer_id,
                offers.fuel_type as fuel_type,
                offers.slug as slug,
                vehicle_models.introduced_date as introduced_date,
                vehicle_models.discontinued_date as discontinued_date,
                vehicle_models.name as model_name,
                vehicle_derivatives.name as derivative_name,
                (select id from vehicle_models where internal_manufacturer_id = vehicle_manufacturers.id limit 1) as internal_vehicle_model_id,
                (select count(id) from vehicle_derivatives where internal_model_id = vehicle_models.id) as derivatives_count
                from offer_derivatives 
            left join offers on offers.id = offer_derivatives.offer_id
            left join vehicle_manufacturers on (vehicle_manufacturers.cap_manufacturer_id = offers.manufacturer_id and vehicle_manufacturers.vehicle_type = offers.vehicle_type)
            left join vehicle_models on vehicle_models.id = (select id from vehicle_models where internal_manufacturer_id =  vehicle_manufacturers.id and cap_model_id = offer_derivatives.cap_model_id limit 1)
            left join vehicle_derivatives on vehicle_derivatives.id = (select id from vehicle_derivatives where internal_model_id = vehicle_models.id and cap_derivative_id = offer_derivatives.cap_derivative_id limit 1)
            right join suggested_values on suggested_values.cap_derivative_id = offer_derivatives.cap_derivative_id 
                and suggested_values.cap_model_id = offer_derivatives.cap_model_id
            where offers.user_id = :userId and finished = 1 and active = 1 and offer_derivatives.base_value < suggested_values.percentage and offers.accept_write_offs = 0 and offers.acceptable_defects = "a:0:{}"
            limit :offset, :perPage';

        return DB::select(DB::raw($sql), [
            'userId' => $this->id,
            'perPage' => $perPage,
            'offset' => ($pageNumber - 1) * $perPage,
        ]);
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function isAccountDetailsComplete(): bool
    {
        return true;
    }

    public function isAssociateSettingsComplete(): bool
    {
        $company = $this->company;

        if (!$company) {
            return false;
        }

        $primaryLocation = PartnerLocation::where([['user_id', '=', $this->id], ['primary', '=', true]])->first();

        $isCompanyPartCompleted = $company->getOriginal('valuation_validity') !== null &&
            $company->getOriginal('bank_transfer_fee') !== null &&
            $company->getOriginal('admin_fee') !== null &&
            $company->getOriginal('postcode_area') !== null;

        $isLocationPartCompleted = $primaryLocation ?
            (
                $primaryLocation->getOriginal('collection_fee') !== null &&
                $primaryLocation->getOriginal('dropoff_fee') !== null &&
                ($primaryLocation->getOriginal('allow_collection') !== null || $primaryLocation->getOriginal('allow_dropoff') !== null)
            ) :
            false;

        return $isCompanyPartCompleted && $isLocationPartCompleted;
    }

    public function isWebsiteThemeComplete(): bool
    {
        $partnerWebsite = PartnerWebsite::where([['user_id', '=', $this->id]])->first();

        if (!$partnerWebsite) {
            return false;
        }

        if (
            $partnerWebsite->getOriginal('logo_image') == 'partner-websites/default/jamjar_partner.jpg' ||
            $partnerWebsite->getOriginal('hero_image') == 'partner-websites/default/hero.jpg'
        ) {
            return false;
        }

        $pages = PartnerPage::where([['website_id', '=', $partnerWebsite->id]])->get();

        if (count($pages) == 0) {
            return false;
        }

        $pagesToComplete = ['home', 'faq', 'contact', 'terms-and-conditions'];

        foreach ($pages as $page) {
            if (!in_array($page->slug, $pagesToComplete)) {
                continue;
            }

            if ($page->content == 'Default content for ' . $page->title && $page->active == true) {
                return false;
            }
        }

        return true;
    }

    public function isFoundAndBillingsComplete(): bool
    {
        return (bool) UserStatement::where([['user_id', '=', $this->id]])->first();
    }

    public function isSetupComplete(): bool
    {
        return $this->isAccountDetailsComplete() &&
            $this->isAssociateSettingsComplete() &&
            $this->isWebsiteThemeComplete() &&
            $this->isFoundAndBillingsComplete();
    }

    /** @return array|PartnerLocation[] */
    public function getActiveLocations(): array
    {
        $company = $this->company;

        if (!$company) {
            return [];
        }

        $locations = PartnerLocation::where([
            ['company_id', '=', $company->id],
            ['is_active', '=', true],
        ])
            ->orderBy('created_at', 'DESC')
            ->get();

        $locationsArray = [];

        foreach ($locations as $location) {
            $locationsArray[] = $location;
        }

        return $locationsArray;
    }

    /**
     * It returns true when user is a matrix_partner and no api partner and auto associate
     * @return bool
     */
    public function isMatrixPartner(): bool
    {
        if($this->matrix_partner == 1 && $this->profile()->first()->auto_partner == 0){
            return true;
        }
        return false;
    }

}
