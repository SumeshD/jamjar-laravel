<?php $isVatNumberVisible = isset($isVatNumberVisible) ? $isVatNumberVisible : true; ?>
<?php $addressLabel = isset($addressLabel) ? $addressLabel : 'Company Address'; ?>
<?php $manualLabel = isset($manualLabel) ? $manualLabel : 'or enter address manually'; ?>
<?php $applyPage = isset($applyPage) ? $applyPage : false; ?>
<?php $postCode = isset($postCode) ? $postCode : ''; ?>
<?php
if($applyPage) {
    $manualLabel = 'Enter address manually';
}
?>

<div class="form__group form__group--half @if ($errors->has('address_line_one')) form--errors @endif @if (isset($isCreateAccountFromValuation)) form__group--create--account @endif" @if($applyPage) style="margin-right: 2%" @else style="margin-right: 0%" @endif>
    <label for="address_line_one" class="form__label"><span class="label-title">{{ $addressLabel }}</span><span
                class="form__required">*</span></label>
    <input
            type="text"
            name="address_line_one"
            id="address_line_one"
            class="form__input"
            placeholder="@if($postCode != ''){{$postCode}}@else{{old('address_line_one')}}@endif"
            autocomplete="new-password"
            value="@if(old('address_line_one')){{old('address_line_one')}}@endif"
    >
    <a style="float:right; margin-top: -10px; margin-bottom:-10px; z-index: 10; position: relative;" id="manual-address" class="manual">{{ $manualLabel }}</a>
</div>

<div class="form__group manual-hide form__group--half @if ($errors->has('address_line_two')) form--errors @endif @if (isset($isCreateAccountFromValuation)) form__group--create--account @endif" style="margin-right: 2%">
    <label for="address_line_two" class="form__label">Address Line Two</label>
    <input
            type="text"
            name="address_line_two"
            id="address_line_two"
            class="form__input"
            autocomplete="new-password"
            value="{{ old('address_line_two') }}"
    >
</div>


<div class="form__group manual-hide form__group--half @if ($errors->has('city')) form--errors @endif @if (isset($isCreateAccountFromValuation)) form__group--create--account @endif" style="margin-right: 0%">
    <label for="city" class="form__label">Town or City <span
                class="form__required">*</span></label>
    <input
            type="text"
            name="city"
            id="city"
            class="form__input"
            autocomplete="new-password"
            value="{{ old('city') }}"
    >
</div>

<div class="form__group manual-hide form__group--half @if ($errors->has('county')) form--errors @endif @if (isset($isCreateAccountFromValuation)) form__group--create--account @endif" style="margin-right: 2%">
    <label for="county" class="form__label">County</label>
    <input
            type="text"
            name="county"
            id="county"
            class="form__input"
            autocomplete="new-password"
            value="{{ old('county') }}"
    >
</div>

<div class="form__group manual-hide form__group--half @if ($errors->has('postcode')) form--errors @endif @if (isset($isCreateAccountFromValuation)) form__group--create--account @endif" style="margin-right: 0%">
    <label for="postcode" class="form__label">Postcode <span
                class="form__required">*</span></label>
    <input
            type="text"
            name="postcode"
            id="postcode"
            class="form__input"
            autocomplete="new-password"
            value="{{ old('postcode') }}"
    >
</div>

@if ($isVatNumberVisible)
    <div class="form__group form__group--half @if ($errors->has('vat_number')) form--errors @endif @if (isset($isCreateAccountFromValuation)) form__group--create--account @endif" style="margin-right: 2%">
        <label for="vat_number" class="form__label">VAT Number <span class="form__required">*</span></label>
        <input
                type="text"
                name="vat_number"
                id="vat_number"
                class="form__input"
                autocomplete="new-password"
                value="{{ old('vat_number') }}"
        >
    </div>
@endif

<div class="full-address-box" style="margin: 5px 3px 10px; font-weight: normal; width: 50%; float: right; padding-left: 15px; margin-top: -1%">
    <div style="display:none;" class="line-one">{{ old('address_line_one') }}</div>
    <div class="line-two">{{ old('address_line_two') }}</div>
    <div class="city">{{ old('city') }}</div>
    <div class="county">{{ old('county') }}</div>
    <div class="postcode">{{ old('postcode') }}</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#fullname").focus();
        pca.on("options", function (type, id, options) {
            if (type == "capture+") {
                options.prompt = false;
            }
        });
        pca.on("load", function(type, id, control) {
            $("#address_line_one").attr('autocomplete', 'nope');
            $("#address_line_one").on("focus", function(){
                if(type === "capture+"){
                    control.prompt = false;
                    control.search($("#address_line_one").attr('placeholder'));
                }
            });
            control.listen("populate", function(address) {
                $('.line-one').text(address.Line1);
                $('.line-two').text(address.Line2);
                $('.postcode').text(address.PostalCode);
                $('.city').text(address.City);
                $('.county').text(address.Province);
            });
        });
    });
</script>