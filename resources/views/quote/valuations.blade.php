@extends('layouts.app')
@section('content')
<section class="jamjar__container valuations">
    <div class="valuations__header">
        <div class="valuations__title">
            <h2>
                <img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}">
                <span>{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</span>

                <span class="valuations__badge js-show-vehicle-details">Show Details <i class="fa fa-chevron-down"></i></span>
            </h2>
            <div class="valuations__buttons mob-hide" style="float: none; width: 100%; margin-top: -60px;">
                @if (!$valuations->isEmpty())
                    <a href="{{ route('dashboard') }}" class="button button--grey-bg">Saved Valuations</a>
                @endif

                <a href="{{ route('showVehicleInformation', $vehicle) }}?mileage={{$vehicle->meta->getOriginal('mileage')}}&owners={{$vehicle->meta->number_of_owners}}&service_history={{ $vehicle->meta->getOriginal('service_history') }}&mot={{$vehicle->meta->getOriginal('mot')}}&color={{$vehicle->meta->color}}&writeoff={{$vehicle->meta->write_off}}&writeoffcat={{$vehicle->meta->write_off_category}}&defects={{$vehicle->meta->defect_ids}}&owner={{$vehicle->owner->id}}&edit=true" class="button button--grey-bg">Edit Details</a>
            </div>
        </div>
    </div>

    <div class="valuations__vehicle-details u-hide">
        <ul>
            <li>
                <span>Derivative:</span> <span>{{$vehicle->meta->derivative}}</span>
            </li>
            <li>
                <span>Plate Year:</span> <span>{{$vehicle->meta->plate_year}}</span>
            </li>
            <li>
                <span>Registration:</span> <span>{{$vehicle->numberplate}}</span>
            </li>
            <li>
                <span>Mileage:</span> <span>{{$vehicle->meta->mileage}}</span>
            </li>
            <li>
                <span>Previous Owners:</span> <span>{{trans('vehicles.owners_count.owners_'.($vehicle->meta->number_of_owners <= 5 ? $vehicle->meta->number_of_owners : 6)) }}</span>
            </li>
        </ul>
        <ul>
            <li>
                <span>Service History:</span> <span>{{$vehicle->meta->service_history}}</span>
            </li>
            <li>
                <span>MOT:</span> <span>{{$vehicle->meta->mot}}</span>
            </li>
            <li>
                <span>Colour:</span> <span>{{$vehicle->meta->colour->title}}</span>
            </li>
            <li>
                <span>Write Off:</span> <span>{{ trans('vehicles.WriteOff' . $vehicle->meta->write_off) }}</span>
            </li>
            <li>
                <span>Non Runner:</span> <span>{{ $vehicle->meta->non_runner ? 'Yes' : 'No' }}</span>
            </li>
        </ul>
    </div>
    <div class="box box--first">
        @if (!$valuations->isEmpty())
        <table style="width:100%;" class="valuation-table-header-only">
            <thead>
                <tr>
                    <th width="25%">Dealer</th>
                    <th width="35%">Details</th>
                    <th width="40%">Valuation</th>
                </tr>
            </thead>
        </table>

        @foreach ($valuations as $valuation)

            @if (($valuation['valuation']['dropoff']) || ($valuation['valuation']['collection']))
                <div class="valuation row">
                    <div class="valuation__image">
                        @if ($valuation['api_partner'])
                        <img src="{{ secure_asset('images/api-partners/'.strtolower($valuation['api_partner']).'/logo.jpg') }}" alt="">
                        @else
                        <img src="{{ $valuation['website']->logo_image }}" alt="">
                        @endif
                    </div><!--
                    --><div class="valuation__list mob-hide">
                        @if ($valuation['partner_type'] == 'api')
                            <p class="show-more-info">More Info <i class="fa fa-chevron-down"></i></p>
                            <p class="valuation__location">Location: <strong>Nationwide</strong></p>
                            @if ($valuation['distance'])
                                @if ($valuation['distance'] !== null)
                                    <p class="valuation__distance">Distance: <small>{{$valuation['distance']}} miles to Dealer</small></p>
                                @endif
                            @endif
                        @else
                            @if (($valuation['location']['allow_collection'] == 1) && ($valuation['location']['allow_dropoff'] == 0))
                                <p class="show-more-info">More Info <i class="fa fa-chevron-down"></i></p>
                                <p class="valuation__location">Location: <strong>Nationwide</strong></p>
                            @else
                                @if ($valuation['distance'])
                                    @if ($valuation['valuation']['dropoff'] !== null)
                                        <p class="valuation__distance">Distance: <small>{{$valuation['distance']}} miles to Dealer</small></p>
                                    @endif
                                @endif
                                @if ($valuation['location'])
                                 <p class="show-more-info">More Info <i class="fa fa-chevron-down"></i></p>
                                <p class="valuation__location">Location: <strong>{{ $valuation['location']->city }}</strong></p>
                                @endif
                            @endif
                        @endif
                        <ul class="valuation__ticklist">
                            @if ($valuation['company']->valuation_validity == null)
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> 5 Day Quote Guarantee
                                    </li>
                                @else
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> {{$valuation['company']->valuation_validity}} Quote Guarantee
                                    </li>
                            @endif

                            @if ($valuation['location']['allow_collection'])
                                @if (($valuation['location']['collection_fee'] == null) || ($valuation['location']['collection_fee'] == 0)) 
                                    @if ($valuation['valuation']['collection'])
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Free Collection
                                    </li>
                                    @endif
                                @else
                                    @if ($valuation['valuation']['collection'])
                                    <li class="u-text-orange">
                                        <i class="fa fa-info-circle"></i> &pound;{{$valuation['location']['collection_fee']}} Collection Fee
                                    </li>
                                    @endif
                                @endif
                            @endif


                            @if (($valuation['valuation']['dropoff']) && (!$valuation['valuation']['collection']))
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> You Drop Off
                                </li>
                            @endif

                            @if (!$valuation['api_partner'])
                                @if (($valuation['company']->bank_transfer_fee == 0) || ($valuation['company']->bank_transfer_fee == null))
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Free Bank Transfer
                                    </li>
                                @else
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Offers Bank Transfer
                                    </li>
                                    <li class="u-text-orange">
                                        <i class="fa fa-info-circle"></i> &pound;{{$valuation['company']->bank_transfer_fee}} Bank Transfer Fee
                                    </li>
                                @endif
                            @else
                                @if (isset($valuation['no_bank_transfer']) && ($valuation['no_bank_transfer'] == true))
                                    @if (isset($valuation['payment_method']))
                                        <li class="u-text-green">
                                            <i class="fa fa-check"></i> Payment via {{ ucwords($valuation['payment_method']) }}
                                        </li>
                                    @endif
                                @else
                                    @if (($valuation['company']->bank_transfer_fee == 0) || ($valuation['company']->bank_transfer_fee == null))
                                        <li class="u-text-green">
                                            <i class="fa fa-check"></i> Free Bank Transfer
                                        </li>
                                    @endif
                                @endif
                            @endif

                            @if ($valuation['partner_type'] == 'api')
                                @if (isset($valuation['admin_fee']))
                                    <li class="u-text-orange">
                                        <i class="fa fa-info-circle"></i> &pound;{{$valuation['admin_fee']}} Admin Fee
                                    </li>
                                @else
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> No Admin Fee
                                    </li>
                                @endif
                            @else
                                @if (($valuation['company']->admin_fee == null) || ($valuation['company']->admin_fee == 0))
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> No Admin Fee
                                    </li>
                                @else
                                    <li class="u-text-orange">
                                        <i class="fa fa-info-circle"></i> &pound;{{$valuation['company']->admin_fee}} Admin Fee
                                    </li>
                                @endif
                            @endif
                        </ul>
                    </div><!--
                    --><div class="valuation__prices">
                        
                            @php 
                                $collection_hide = false; 
                                $dropoff_hide = false; 
                            @endphp

                            @if ($valuation['valuation']['collection'] >= $valuation['valuation']['dropoff'])
                                @if ($valuation['valuation']['collection'] !== null)
                                    <div class="valuation__prices-item valuation__collection @if ($collection_hide == true) valuation__prices-item--mob-hide @endif">
                                        <small>They Collect</small>
                                        <span>£{{ number_format($valuation['valuation']['collection']) }}</span>
                                    </div>
                                @endif

                                @if (($valuation['valuation']['dropoff'] !== null) && ($valuation['valuation']['collection'] !== null))
                                    <div class="divider divider--small"></div>
                                @endif

                                @if ($valuation['valuation']['dropoff'] !== null)
                                    <div class="valuation__prices-item valuation__delivery @if ($dropoff_hide == true) valuation__prices-item--mob-hide @endif">
                                        <small>You Deliver</small>
                                        <span>£{{ number_format($valuation['valuation']['dropoff']) }}</span>
                                    </div>
                                @endif
                            @else 
                                @if ($valuation['valuation']['dropoff'] !== null)
                                    <div class="valuation__prices-item valuation__delivery @if ($dropoff_hide == true) valuation__prices-item--mob-hide @endif">
                                        <small>You Deliver</small>
                                        <span>£{{ number_format($valuation['valuation']['dropoff']) }}</span>
                                    </div>
                                @endif
                                
                                @if (($valuation['valuation']['dropoff'] !== null) && ($valuation['valuation']['collection'] !== null))
                                    <div class="divider divider--small"></div>
                                @endif

                                @if ($valuation['valuation']['collection'] !== null)
                                    <div class="valuation__prices-item valuation__collection @if ($collection_hide == true) valuation__prices-item--mob-hide @endif">
                                        <small>They Collect</small>
                                        <span>£{{ number_format($valuation['valuation']['collection']) }}</span>
                                    </div>
                                @endif
                            @endif

                        
                    </div>
                    <div class="valuation__list desktop-hide">
                        @if ($valuation['partner_type'] == 'api')
                            <p class="show-more-info">More Info <i class="fa fa-chevron-down"></i></p>
                            <p class="valuation__location">Location: <strong>Nationwide</strong></p>
                            @if ($valuation['distance'])
                                @if ($valuation['distance'] !== null)
                                    <p class="valuation__distance">Distance: <small>{{$valuation['distance']}} miles to Dealer</small></p>
                                @endif
                            @endif
                        @else
                            @if (($valuation['location']['allow_collection'] == 1) && ($valuation['location']['allow_dropoff'] == 0))
                                <p class="show-more-info">More Info <i class="fa fa-chevron-down"></i></p>
                                <p class="valuation__location">Location: <strong>Nationwide</strong></p>
                            @else
                                @if ($valuation['distance'])
                                    @if ($valuation['valuation']['dropoff'] !== null)
                                        <p class="valuation__distance">Distance: <small>{{$valuation['distance']}} miles to Dealer</small></small></p>
                                    @endif
                                @endif
                                @if ($valuation['location'])
                                    <p class="show-more-info">More Info <i class="fa fa-chevron-down"></i></p>
                                    <p class="valuation__location">Location: <strong>{{ $valuation['location']->city }}</strong></p>
                                @endif
                            @endif
                        @endif
                        <ul class="valuation__ticklist">
                            @if ($valuation['company']->valuation_validity == null)
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> 5 Day Quote Guarantee
                                </li>
                            @else
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> {{$valuation['company']->valuation_validity}} Quote Guarantee
                                </li>
                            @endif

                            @if ($valuation['location']['allow_collection'])
                                @if (($valuation['location']['collection_fee'] == null) || ($valuation['location']['collection_fee'] == 0))
                                    @if ($valuation['valuation']['collection'])
                                        <li class="u-text-green">
                                            <i class="fa fa-check"></i> Free Collection
                                        </li>
                                    @endif
                                @else
                                    @if ($valuation['valuation']['collection'])
                                        <li class="u-text-orange">
                                            <i class="fa fa-info-circle"></i> &pound;{{$valuation['location']['collection_fee']}} Collection Fee
                                        </li>
                                    @endif
                                @endif
                            @endif


                            @if (($valuation['valuation']['dropoff']) && (!$valuation['valuation']['collection']))
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> You Drop Off
                                </li>
                            @endif

                            @if (!$valuation['api_partner'])
                                @if (($valuation['company']->bank_transfer_fee == 0) || ($valuation['company']->bank_transfer_fee == null))
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Free Bank Transfer
                                    </li>
                                @else
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Offers Bank Transfer
                                    </li>
                                    <li class="u-text-orange">
                                        <i class="fa fa-info-circle"></i> &pound;{{$valuation['company']->bank_transfer_fee}} Bank Transfer Fee
                                    </li>
                                @endif
                            @else
                                @if (isset($valuation['no_bank_transfer']) && ($valuation['no_bank_transfer'] == true))
                                    @if (isset($valuation['payment_method']))
                                        <li class="u-text-green">
                                            <i class="fa fa-check"></i> Payment via {{ ucwords($valuation['payment_method']) }}
                                        </li>
                                    @endif
                                @else
                                    @if (($valuation['company']->bank_transfer_fee == 0) || ($valuation['company']->bank_transfer_fee == null))
                                        <li class="u-text-green">
                                            <i class="fa fa-check"></i> Free Bank Transfer
                                        </li>
                                    @endif
                                @endif
                            @endif

                            @if ($valuation['partner_type'] == 'api')
                                @if (isset($valuation['admin_fee']))
                                    <li class="u-text-orange">
                                        <i class="fa fa-info-circle"></i> &pound;{{$valuation['admin_fee']}} Admin Fee
                                    </li>
                                @else
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> No Admin Fee
                                    </li>
                                @endif
                            @else
                                @if (($valuation['company']->admin_fee == null) || ($valuation['company']->admin_fee == 0))
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> No Admin Fee
                                    </li>
                                @else
                                    <li class="u-text-orange">
                                        <i class="fa fa-info-circle"></i> &pound;{{$valuation['company']->admin_fee}} Admin Fee
                                    </li>
                                @endif
                            @endif
                        </ul>
                    </div>
                    <div class="valuation__button">
                        @if ($valuation['api_partner'])
                            {{-- This check is only temporary while M4YM fix their acceptance API. --}}
                            @if ($valuation['company']->name == 'Money4yourMotors')
                                @if (is_null($valuation['acceptance_uri']))
                                    {{-- For now we need to accept valuations on their behalf --}}
                                    <form id="accept-valuation-form-{{$valuation['company']->id}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                        <input type="hidden" id="valuation"  name="valuation" value="{{$valuation['valuation']['collection'] }}">
                                        <input type="hidden" id="name"  name="name" value="{{ $vehicle->latest_owner->name }}">
                                        <input type="hidden" id="email"  name="email" value="{{ $vehicle->latest_owner->email }}">
                                        <input type="hidden" id="phone"  name="phone" value="{{ $vehicle->latest_owner->telephone }}">
                                        <input type="hidden" id="postcode"  name="postcode" value="{{ $vehicle->latest_owner->postcode }}">
                                        <input type="hidden" id="ref" name="ref" value="{{ $valuation['valuation']['temp_ref'] }}">
                                        <input type="submit" class="button button--orange-bg button--block" id="accept-valuation-button-{{$valuation['company']->id}}" value="See details">
                                    </form>
                                @else
                                    <a href="{{$valuation['acceptance_uri']}}" target="_blank" id="accept-valuation-button-api-{{$valuation->company->id}}" class="button button--orange-bg button--block">See Details</a>
                                @endif
                            @elseif ($valuation['company']->name == 'WeBuyCarsToday')
                                @if (is_null($valuation['website']))
                                {{-- For now we need to accept valuations on their behalf --}}
                                <form id="accept-valuation-form-{{$valuation['company']->id}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                    <input type="hidden" id="valuation"  name="valuation" value="{{$valuation['valuation']['collection'] }}">
                                    <input type="hidden" id="name"  name="name" value="{{ $vehicle->owner->name }}">
                                    <input type="hidden" id="email"  name="email" value="{{ $vehicle->owner->email }}">
                                    <input type="hidden" id="phone"  name="phone" value="{{ $vehicle->owner->telephone }}">
                                    <input type="hidden" id="postcode"  name="postcode" value="{{ $vehicle->owner->postcode }}">
                                    <input type="hidden" id="ref" name="ref" value="{{$valuation['valuation']['temp_ref'] }}">
                                    <input type="submit" class="button button--orange-bg button--block" id="accept-valuation-button-{{$valuation['company']->id}}" value="See details">
                                </form>
                                @else
                                    <a href="{{$valuation['website']}}" target="_blank" id="accept-valuation-button-api-{{$valuation->company->id}}" class="button button--orange-bg button--block">See Details</a>
                                @endif
                            @else
                                {{-- This is the button for all other API partners --}}
                                <a href="{{$valuation['website']}}" target="_blank" id="accept-valuation-button-api-{{$valuation['company']->id}}" class="button button--orange-bg button--block">See Details</a>
                            @endif

                        @else
                        <a class="button button--orange-bg button--block" id="accept-valuation-button-{{$valuation['company']->id}}">See Details</a>
                        <form target="_blank" action="{{ route('partnerWebsitePage', [$valuation['website'],'your-offer']) }}" method="post" id="accept-valuation-form-{{$valuation['company']->id}}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <input type="hidden" id="company_id"  name="company_id" value="{{$valuation['company']->id}}">
                            <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                            {{-- <input type="submit" class="button button--orange-bg button--block" id="accept-valuation-button-{{$valuation['company']->id}}" value="See Details"> --}}
                        </form>
                        @endif
                    </div>
                </div>
                @push('scripts-after')
                <script>
                    @if ($valuation['api_partner'])
                    @if ($valuation['company']->name == 'Money4yourMotors')
                        $('#accept-valuation-form-{{$valuation['company']->id}}').on('submit', function(e) {
                            e.preventDefault();
                            $('.loading').show();
                            var data = {
                                "ColumnMappings": [
                                    {
                                        "Id": "TempReference",
                                        "Value": $(this).find('#ref').val().toString()
                                    },
                                    {
                                        "Id": "CustomerIPAddress",
                                        "Value": "-"
                                    },
                                    {
                                        "Id": "VehicleValuation",
                                        "Value": $(this).find('#valuation').val().replace("£","").replace(",","")
                                    },
                                ]
                            };

                          axios.post('{{ route('money4YourMotorsAcceptValuation') }}', data)
                            .then(function(res) {
                                if(res.data.SUCCESS){
                                    window.location = res.data.URI;
                                    // window.open(res.data.URI,'_blank');
                                } else {
                                  $('.loading').hide();
                                }
                            }).catch(function(err) {
                                $('.loading').show();
                                console.log("AXIOS ERROR: ", err);
                            });

                        });
                        @elseif ($valuation['company']->name == 'WeBuyCarsToday')
                        $('#accept-valuation-form-{{$valuation['company']->id}}').on('submit', function(e) {
                            e.preventDefault();
                            $('.loading').show();
                            var data = {
                                "fullname":$(this).find('#name').val().toString(),
                                "registration_id": $(this).find('#ref').val().toString(),
                                "postcode": $(this).find('#postcode').val().toString(),
                                "contact_no": $(this).find('#phone').val().toString(),
                                "email": $(this).find('#email').val().toString(),
                                "valuation_id": $(this).find('#valuation').val().toString(),
                                "vrm": $(this).find('#vrm').val().toString()
                            };

                          axios.post('{{ route('weBuyCarsTodayAcceptValuation') }}', data)
                            .then(function(res) {
                                if(res.data.SUCCESS){
                                    window.location = res.data.URI;
                                    // window.open(res.data.URI,'_blank');
                                } else {
                                  $('.loading').hide();
                                }
                            }).catch(function(err) {
                                $('.loading').show();
                                console.log("AXIOS ERROR: ", err);
                            });

                        });
                        @else 
                        $('#accept-valuation-button-api-{{$valuation['company']->id}}').on('click', function(e) {
                            e.preventDefault();
                            $('.loading').show();

                            var url = $(this).attr('href');
                            // axios.post('{{ route('saveClickthrough', [$valuation['company']->id])}}')
                            //     .then(({data}) => {
                            //         console.log(data);
                            //     });
                            setTimeout(function() {
                                $('.loading').hide();
                                window.location = url;
                            }, 1500);
                        });
                        @endif
                    @else
                    $('#accept-valuation-button-{{$valuation['company']->id}}').on('click', function(e) {
                        e.preventDefault();
                        $('.loading').show();
                        swal({
                            title: "Now leaving Jamjar.com",
                            text: 'You are now being taken to {{$valuation['company']->name}}',
                            icon: "info",
                            buttons: true,
                        }).then((willRedirect) => {
                            if (willRedirect) {
                                // Redirect the user
                                var data = {
                                    'company_id': '{{$valuation['company']->id}}',
                                    'vrm': '{{$vehicle->numberplate}}',
                                };
                                axios.patch('{{ route('partnerWebsitePage', [$valuation['website'],'your-offer']) }}', data)
                                    .then(({data}) => {
                                        setTimeout(() => {
                                            $('.loading').hide();
                                            window.location.href = '{{ route('partnerWebsitePage', [$valuation['website'],'your-offer']) }}';
                                        }, 1000);
                                    });
                            } else {
                                $('.loading').hide();
                            }
                        });
                        return false;
                    });
                    @endif
                </script>
                @endpush
            @endif
        @endforeach
    @else
        <div class="box__container">
            <h1 class="box__title">Sorry!</h1>
            <div class="box__content">
                <p>We couldn't find any valuations for your car.</p>
            </div>
        </div>
    @endif
    </div>
    <div class="valuations__buttons desktop-hide">
        @if (!$valuations->isEmpty())
            <a href="{{ route('dashboard') }}" class="button button--grey-bg">Saved Valuations</a>
        @endif

        <a href="{{ route('showVehicleInformation', $vehicle) }}?mileage={{$vehicle->meta->getOriginal('mileage')}}&owners={{$vehicle->meta->number_of_owners}}&service_history={{ $vehicle->meta->getOriginal('service_history') }}&mot={{$vehicle->meta->getOriginal('mot')}}&color={{$vehicle->meta->color}}&writeoff={{$vehicle->meta->write_off}}&writeoffcat={{$vehicle->meta->write_off_category}}&defects={{$vehicle->meta->defect_ids}}&owner={{$vehicle->owner->id}}&edit=true" class="button button--grey-bg">Edit Details</a>
    </div>
</section>

<div class="loading" style="display:none;"></div>

@push('scripts-after')
<script>
$('.js-show-vehicle-details').on('click', function(e) {
    e.preventDefault();

    // Toggle the display of the details
    $('.valuations__vehicle-details').stop().slideToggle();

    // Toggle an active class on the link
    $(this).toggleClass('active');

    // if this element has a class of active change the text
    if ($(this).hasClass('active')) {
        $(this).html('Hide Details <i class="fa fa-chevron-up"></i>');
    } else {
        $(this).html('Show Details <i class="fa fa-chevron-down"></i>');
    }
});
</script>
@endpush
@endsection
