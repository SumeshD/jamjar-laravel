@php
  $location = ($valuation->location ? $valuation->location : $valuation->company->location->first());
@endphp

@if ($valuation->getOriginal('dropoff_value') || $valuation->getOriginal('collection_value'))
    <tr class="valuation-holder" data-price="{{$valuation->getPriceInPence()}}">
        <td colspan="3">
            <div class="valuation row">
                <div class="valuation__image">
                    @if ($valuation->company->company_type == 'api')
                        <img src="{{ secure_asset('images/api-partners/'.strtolower($valuation->company->name).'/logo.jpg') }}" alt="">
                    @else
                        <img src="{{ substr($valuation->company->website->logo_image, 0, 4) == 'http' ? '' : '/' }}{{ $valuation->company->website->logo_image }}" alt="">
                    @endif
                    @if($valuation->company->company_type != 'api' && $valuation->company->website->logo_image=='partner-websites/default/jamjar_partner.jpg')
                        <p class="default-company">{{$valuation->company->name}}</p>
                    @endif
                </div>
                <div class="valuation__list mob-hide">

                    <p class="show-more-info">More Info <i class="fa fa-chevron-down"></i></p>
                    <p class="valuation__location">

                        @if(!$valuation->scrap)
                            @if ($valuation->shouldBeCollectedFromAnyLocation())
                                Collection: Mainland UK
                            @elseif ($valuation->shouldBeCollectedFromClientLocation())
                                Collection: From your address
                            @elseif ($valuation->shouldBeDroppedAtDistance())
                                Distance: <strong>{{$valuation->distance}} miles</strong>
                            @elseif ($valuation->shouldBeDroppedAtLocation())
                                Location: <strong>{{ $location->city }}</strong>
                            @endif
                        @else
                            @if($valuation->collection_part)
                                @if ($valuation->shouldBeCollectedFromAnyLocationScrapPartner())
                                    Collection: Mainland UK
                                @elseif ($valuation->shouldBeCollectedFromClientLocationScrapPartner())
                                    Collection: From your address
                                @endif
                            @else
                                @if ($valuation->shouldBeDroppedAtDistance())
                                    Distance: <strong>{{$valuation->distance}} miles</strong>
                                @elseif ($valuation->shouldBeDroppedAtLocation())
                                    Location: <strong>{{ $location->city }}</strong>
                                @endif
                            @endif
                        @endif

                    </p>
                    <ul class="valuation__ticklist">
                        @if ($valuation->getDraft() and $valuation->getDraft()->getValuationValidityInDays())
                            <li class="u-text-green">
                                <i class="fa fa-check"></i> {{$valuation->getDraft()->getValuationValidityInDays()}} days Quote Guarantee
                            </li>
                        @elseif ($valuation->company->valuation_validity == null)
                            <li class="u-text-green">
                                <i class="fa fa-check"></i> 5 Day Quote Guarantee
                            </li>
                        @else
                            <li class="u-text-green">
                                <i class="fa fa-check"></i> {{$valuation->company->valuation_validity}} Quote Guarantee
                            </li>
                        @endif

                        @if(!$valuation->scrap)

                            @if ($valuation->getCollectionFeeInPence())
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->getCollectionFeeInPence() / 100}} Collection Fee
                                </li>
                            @elseif ($location->allow_collection)
                                @if (($location->collection_fee == null) || ($location->collection_fee == 0))
                                    @if ($valuation->collection_value)
                                        <li class="u-text-green">
                                            <i class="fa fa-check"></i> Free Collection
                                        </li>
                                    @endif
                                @else
                                    @if ($valuation->getOriginal('collection_value'))
                                        <li class="u-text-orange">
                                            <i class="fa fa-info-circle"></i> &pound;{{$location->collection_fee}} Collection Fee
                                        </li>
                                    @endif
                                @endif
                            @endif

                            @if ($valuation->getDropOffFeeInPence())
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->getDropOffFeeInPence() / 100}} Dropoff Fee
                                </li>
                            @elseif ($location->allow_dropoff)
                                @if (($location->dropoff_fee != null) and ($location->dropoff_fee != 0))
                                    @if ($valuation->getOriginal('dropoff_value'))
                                        <li class="u-text-orange">
                                            <i class="fa fa-info-circle"></i> &pound;{{$location->dropoff_fee}} Dropoff Fee
                                        </li>
                                    @endif
                                @endif
                            @endif
                        @else


                            @if($valuation->collection_part)

                                @if ($location->allow_collection)

                                    @if (($location->collection_fee == null) || ($location->collection_fee == 0))
                                        @if ($valuation->collection_value)
                                            <li class="u-text-green">
                                                <i class="fa fa-check"></i> Free Collection
                                            </li>
                                        @endif
                                    @endif

                                @endif

                            @endif

                        @endif



                        @if (!$valuation->company->company_type == 'api')
                            @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> Free Bank Transfer
                                </li>
                            @else
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> Offers Bank Transfer
                                </li>
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->company->bank_transfer_fee}} Bank Transfer Fee
                                </li>
                            @endif
                        @else
                            @if ($valuation->payment_method == 'Bank Transfer')
                                @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Free Bank Transfer
                                    </li>
                                @else
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Offers Bank Transfer
                                    </li>
                                    <li class="u-text-orange">
                                        <i class="fa fa-info-circle"></i> &pound;{{$valuation->company->bank_transfer_fee}} Bank Transfer Fee
                                    </li>
                                @endif
                            @else
                                @if (isset($valuation['payment_method']))
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Payment via {{ ucwords($valuation->payment_method) }}
                                    </li>
                                @endif
                            @endif
                        @endif

                        @if ($valuation->company->company_type == 'api')
                            @if (isset($valuation->admin_fee))
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->admin_fee}} Admin Fee
                                </li>
                            @else
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> No Admin Fee
                                </li>
                            @endif
                        @else
                            @if (($valuation->company->admin_fee == null) || ($valuation->company->admin_fee == 0))
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> No Admin Fee
                                </li>
                            @else
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->company->admin_fee}} Admin Fee
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>
                <div class="valuation__prices">

                    @php
                        $collection_hide = false;
                        $dropoff_hide = false;
                    @endphp
                    <?php /** @var \JamJar\Valuation $valuation */ ?>
                    @if ($valuation->isCollectionAvailable() and $valuation->isDropOffAvailable() and !$valuation->scrap)
                        <div class="valuation__prices-item valuation__collection">
                            <small>Collection or delivery</small>
                            <span>&pound;{{ number_format(($valuation->getPriceInPence() / 100)) }}</span>
                        </div>

                    @elseif ($valuation->isCollectionAvailable() and $valuation->isDropOffAvailable() and $valuation->scrap)

                        @if ($valuation->collection_part)
                            <div class="valuation__prices-item valuation__collection">
                                <small>They Collect</small>
                                <span>{{$valuation->collection_new_price}}</span>
                            </div>
                        @else
                            <div class="valuation__prices-item valuation__collection">
                                <small>You Deliver</small>
                                <span>{{$valuation->dropoff_new_price}}</span>
                            </div>
                        @endif

                    @else
                        @if ($valuation->isCollectionAvailable())
                            <div class="valuation__prices-item valuation__collection">
                                <small>They Collect</small>
                                <span>&pound;{{ number_format(($valuation->getPriceInPence() / 100)) }}</span>
                            </div>
                        @else
                            <div class="valuation__prices-item valuation__delivery valuation__prices-item">
                                <small>You Deliver</small>
                                <span>&pound;{{ number_format(($valuation->getPriceInPence() / 100)) }}</span>
                            </div>
                        @endif
                    @endif
                </div>
                <div class="valuation__list desktop-hide">
                    <p class="show-more-info">More Info <i class="fa fa-chevron-down"></i></p>
                    <p class="valuation__location">
                        @if(!$valuation->scrap)
                            @if ($valuation->shouldBeCollectedFromAnyLocation())
                                Collection: Mainland UK
                            @elseif ($valuation->shouldBeCollectedFromClientLocation())
                                Collection: From your address
                            @elseif ($valuation->shouldBeDroppedAtDistance())
                                Distance: <strong>{{$valuation->distance}} miles</strong>
                            @elseif ($valuation->shouldBeDroppedAtLocation())
                                Location: <strong>{{ $location->city }}</strong>
                            @endif
                        @else
                            @if($valuation->collection_part)
                                @if ($valuation->shouldBeCollectedFromAnyLocationScrapPartner())
                                    Collection: Mainland UK
                                @elseif ($valuation->shouldBeCollectedFromClientLocationScrapPartner())
                                    Collection: From your address
                                @endif
                            @else
                                @if ($valuation->shouldBeDroppedAtDistance())
                                    Distance: <strong>{{$valuation->distance}} miles</strong>
                                @elseif ($valuation->shouldBeDroppedAtLocation())
                                    Location: <strong>{{ $location->city }}</strong>
                                @endif
                            @endif
                        @endif
                    </p>
                    <ul class="valuation__ticklist">
                        @if ($valuation->getDraft() and $valuation->getDraft()->getValuationValidityInDays())
                            <li class="u-text-green">
                                <i class="fa fa-check"></i> {{$valuation->getDraft()->getValuationValidityInDays()}} days Quote Guarantee
                            </li>
                        @elseif ($valuation->company->valuation_validity == null)
                            <li class="u-text-green">
                                <i class="fa fa-check"></i> 5 Day Quote Guarantee
                            </li>
                        @else
                            <li class="u-text-green">
                                <i class="fa fa-check"></i> {{$valuation->company->valuation_validity}} Quote Guarantee
                            </li>
                        @endif


                        @if(!$valuation->scrap)

                            @if ($valuation->getCollectionFeeInPence())
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->getCollectionFeeInPence() / 100}} Collection Fee
                                </li>
                            @elseif ($location->allow_collection)
                                @if (($location->collection_fee == null) || ($location->collection_fee == 0))
                                    @if ($valuation->collection_value)
                                        <li class="u-text-green">
                                            <i class="fa fa-check"></i> Free Collection
                                        </li>
                                    @endif
                                @else
                                    @if ($valuation->getOriginal('collection_value'))
                                        <li class="u-text-orange">
                                            <i class="fa fa-info-circle"></i> &pound;{{$location->collection_fee}} Collection Fee
                                        </li>
                                    @endif
                                @endif
                            @endif

                            @if ($valuation->getDropOffFeeInPence())
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->getDropOffFeeInPence() / 100}} Dropoff Fee
                                </li>
                            @elseif ($location->allow_dropoff)
                                @if (($location->dropoff_fee != null) and ($location->dropoff_fee != 0))
                                    @if ($valuation->getOriginal('dropoff_value'))
                                        <li class="u-text-orange">
                                            <i class="fa fa-info-circle"></i> &pound;{{$location->dropoff_fee}} Dropoff Fee
                                        </li>
                                    @endif
                                @endif
                            @endif
                        @else


                            @if($valuation->collection_part)

                                @if ($location->allow_collection)

                                    @if (($location->collection_fee == null) || ($location->collection_fee == 0))
                                        @if ($valuation->collection_value)
                                            <li class="u-text-green">
                                                <i class="fa fa-check"></i> Free Collection
                                            </li>
                                        @endif
                                    @endif

                                @endif

                            @endif

                        @endif

                        @if (!$valuation->company->company_type == 'api')
                            @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> Free Bank Transfer
                                </li>
                            @else
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> Offers Bank Transfer
                                </li>
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->company->bank_transfer_fee}} Bank Transfer Fee
                                </li>
                            @endif
                        @else
                            @if ($valuation->payment_method == 'Bank Transfer')
                                @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Free Bank Transfer
                                    </li>
                                @else
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Offers Bank Transfer
                                    </li>
                                    <li class="u-text-orange">
                                        <i class="fa fa-info-circle"></i> &pound;{{$valuation->company->bank_transfer_fee}} Bank Transfer Fee
                                    </li>
                                @endif
                            @else
                                @if (isset($valuation['payment_method']))
                                    <li class="u-text-green">
                                        <i class="fa fa-check"></i> Payment via {{ ucwords($valuation->payment_method) }}
                                    </li>
                                @endif
                            @endif
                        @endif

                        @if ($valuation->company->company_type == 'api')
                            @if (isset($valuation->admin_fee))
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->admin_fee}} Admin Fee
                                </li>
                            @else
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> No Admin Fee
                                </li>
                            @endif
                        @else
                            @if (($valuation->company->admin_fee == null) || ($valuation->company->admin_fee == 0))
                                <li class="u-text-green">
                                    <i class="fa fa-check"></i> No Admin Fee
                                </li>
                            @else
                                <li class="u-text-orange">
                                    <i class="fa fa-info-circle"></i> &pound;{{$valuation->company->admin_fee}} Admin Fee
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>
                <div style="text-align: center; margin-left: -10%; width: 120%;">
                    <div class="valuation__button">
                        @php
                            $expires = $valuation->created_at->addDay($valuation->company->valuation_validity);
                        @endphp

                        @if ($valuation->company->company_type == 'api')
                            @if (now()->gte($expires))
                                <a class="button button--grey-bg button--block" style="cursor:not-allowed;">Expired</a>
                            @elseif ($valuation->complete)
                                <a class="button button--grey-bg button--block" style="cursor:not-allowed;">Accept</a>
                            @elseif ($valuation->rejected)
                                <a class="button button--grey-bg button--block" style="cursor:not-allowed;">Cancelled</a>
                            @elseif ($valuation->pending)
                                <a class="button button--orange-bg-front  button--block" >Cancel Offer</a>
                                <form  action="{{ route('cancelValuation',[$valuation->uuid]) }}" method="post" id="cancel-valuation-form-{{$valuation->uuid}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="company_id"  name="company_id" value="{{$valuation->company->id}}">
                                    <input type="hidden" id="uuid"  name="uuid" value="{{$valuation->uuid}}">
                                    <input type="hidden" id="vehicle_id"  name="vehicle_id" value="{{$vehicle->id}}">
                                    <input type="hidden" id="vid"  name="vid" value="{{$valuation->id}}">
                                    <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                </form>
                            @elseif ($valuation->pendingOverall)
                                <a class="button button--grey-bg button--block" style="cursor:not-allowed;">Accept</a>
                            @elseif($valuation['company']->name == 'Money4yourMotors')
                                @if (is_null($valuation->acceptance_uri) or !$valuation->acceptance_uri)
                                    {{-- For now we need to accept valuations on their behalf --}}
                                    <form id="accept-valuation-form-{{$valuation->company->id}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                        <input type="hidden" id="valuation"  name="valuation" value="{{$valuation->collection_value }}">
                                        <input type="hidden" id="name"  name="name" value="{{ $vehicle->latest_owner->name }}">
                                        <input type="hidden" id="email"  name="email" value="{{ $vehicle->latest_owner->email }}">
                                        <input type="hidden" id="phone"  name="phone" value="{{ $vehicle->latest_owner->telephone }}">
                                        <input type="hidden" id="postcode"  name="postcode" value="{{ $vehicle->latest_owner->postcode }}">
                                        <input type="hidden" id="ref" name="ref" value="{{ $valuation->temp_ref }}">
                                        <input type="hidden" name="valuation-id" value="{{$valuation->id }}">
                                        <input type="submit" class="button button--green-bg button--block" id="accept-valuation-button-{{$valuation->company->id}}" value="Accept">
                                    </form>
                                @else
                                    <a href="{{$valuation->acceptance_uri}}" id="accept-valuation-button-api-{{$valuation->company->id}}" class="button button--green-bg button--block">Accept</a>
                                @endif
                            @elseif ($valuation['company']->name == 'WeBuyCarsToday')
                                @if (is_null($valuation->acceptance_uri))
                                    {{-- For now we need to accept valuations on their behalf --}}
                                    <form id="accept-valuation-form-{{$valuation['company']->id}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                        <input type="hidden" id="valuation"  name="valuation" value="{{$valuation->collection_value }}">
                                        <input type="hidden" id="name"  name="name" value="{{ $vehicle->owner->name }}">
                                        <input type="hidden" id="email"  name="email" value="{{ $vehicle->owner->email }}">
                                        <input type="hidden" id="phone"  name="phone" value="{{ $vehicle->owner->telephone }}">
                                        <input type="hidden" id="postcode"  name="postcode" value="{{ $vehicle->owner->postcode }}">
                                        <input type="hidden" id="ref" name="ref" value="{{$valuation->temp_ref }}">
                                        <input type="hidden" name="valuation-id" value="{{$valuation->id }}">
                                        <input type="submit" class="button button--green-bg button--block" id="accept-valuation-button-{{$valuation->company->id}}" value="Accept">
                                    </form>
                                @else
                                    <a href="{{$valuation->acceptance_uri}}" id="accept-valuation-button-api-{{$valuation->company->id}}" class="button button--green-bg button--block">Accept</a>
                                @endif
                            @elseif ($valuation['company']->name == 'CarTakeBack')
                                <a data-csrf-token="{{ csrf_token() }}" data-valuation-id="{{ $valuation->id }}" href="{{$valuation->acceptance_uri}}" target="_blank" class="accept-ctb-button button button--green-bg button--block">Accept</a>
                            @elseif ($valuation['company']->name == 'Motorwise')
                                <a data-csrf-token="{{ csrf_token() }}" data-valuation-id="{{ $valuation->id }}" href="{{$valuation->acceptance_uri}}" target="_blank" class="accept-mw-button button button--green-bg button--block">Accept</a>
                            @else
                                {{-- This is the button for all other API partners --}}
                                <a data-csrf-token="{{ csrf_token() }}" data-valuation-id="{{ $valuation->id }}" href="{{$valuation->acceptance_uri}}" target="_blank" id="accept-valuation-button-api-{{$valuation['company']->id}}" class="accept-api-valuation button button--green-bg button--block">Accept</a>
                            @endif
                        @else

                            @if (now()->gte($expires))
                                <a class="button button--grey-bg button--block" style="cursor:not-allowed;">Expired</a>
                            @elseif ($valuation->complete)
                                <a class="button button--grey-bg button--block" style="cursor:not-allowed;">Accept</a>
                            @elseif ($valuation->rejected)
                                <a class="button button--grey-bg button--block" style="cursor:not-allowed;">Cancelled</a>
                            @elseif ($valuation->pending)
                                <a class="button button--orange-bg-front  button--block" >Cancel Offer</a>
                                <form  action="{{ route('cancelValuation',[$valuation->uuid]) }}" method="post" id="cancel-valuation-form-{{$valuation->uuid}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="company_id"  name="company_id" value="{{$valuation->company->id}}">
                                    <input type="hidden" id="uuid"  name="uuid" value="{{$valuation->uuid}}">
                                    <input type="hidden" id="vehicle_id"  name="vehicle_id" value="{{$vehicle->id}}">
                                    <input type="hidden" id="vid"  name="vid" value="{{$valuation->id}}">
                                    <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                </form>
                            @elseif ($valuation->pendingOverall)
                                <a class="button button--grey-bg button--block" style="cursor:not-allowed;">Accept</a>
                            @else
                                <a class="button button--green-bg button--block" id="accept-valuation-button-{{$valuation->company->id}}">Accept</a>
                                <form target="_blank" action="{{ route('partnerWebsitePage', [$valuation->company->website,'your-offer']) }}" method="post" id="accept-valuation-form-{{$valuation->company->id}}">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    <input type="hidden" id="company_id"  name="company_id" value="{{$valuation->company->id}}">
                                    <input type="hidden" id="uuid"  name="uuid" value="{{$valuation->uuid}}">
                                    <input type="hidden" id="vid"  name="vid" value="{{$valuation->id}}">
                                    <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                    {{-- <input type="submit" class="button button--orange-bg button--block" id="accept-valuation-button-{{$valuation->company->id}}" value="Accept Offer"> --}}
                                </form>
                            @endif
                        @endif
                    </div>
                    <div class="valuation__button">
                        <a  class="button button--orange-bg button--block" href="#improve-wrapper">Improve</a>
                    </div>
                </div>
            </div>
                <script>
                        @if ($valuation->company->company_type == 'api')
                        @if ($valuation['company']->name == 'Money4yourMotors')
                        $('#accept-valuation-form-{{$valuation['company']->id}}').on('submit', function(e) {
                            e.preventDefault();
                            $('.loading').show();
                            var data = {
                                "ColumnMappings": [
                                    {
                                        "Id": "TempReference",
                                        "Value": $(this).find('#ref').val().toString()
                                    },
                                    {
                                        "Id": "CustomerIPAddress",
                                        "Value": "-"
                                    },
                                    {
                                        "Id": "VehicleValuation",
                                        "Value": $(this).find('#valuation').val().replace("£","").replace(",","")
                                    },
                                ],
                                'valuationId': $(this).find('[name="valuation-id"]').val().toString()
                            };

                            axios.post('{{ route('money4YourMotorsAcceptValuation') }}', data)
                                .then(function(res) {
                                    if(res.data.SUCCESS){
                                        window.location = res.data.URI;
                                        // window.open(res.data.URI,'_blank');
                                    } else {
                                        $('.loading').hide();
                                    }
                                }).catch(function(err) {
                                $('.loading').show();
                                console.log("AXIOS ERROR: ", err);
                            });

                        });
                        @elseif ($valuation['company']->name == 'WeBuyCarsToday')
                        $('#accept-valuation-form-{{$valuation['company']->id}}').on('submit', function(e) {
                            e.preventDefault();
                            $('.loading').show();
                            var data = {
                                "fullname":$(this).find('#name').val().toString(),
                                "registration_id": $(this).find('#ref').val().toString(),
                                "postcode": $(this).find('#postcode').val().toString(),
                                "contact_no": $(this).find('#phone').val().toString(),
                                "email": $(this).find('#email').val().toString(),
                                "vrm": $(this).find('#vrm').val().toString(),
                                'valuationId': $(this).find('[name="valuation-id"]').val().toString()
                            };

                            axios.post('{{ route('weBuyCarsTodayAcceptValuation') }}', data)
                                .then(function(res) {
                                    if(res.data.SUCCESS){
                                        window.location = res.data.URI;
                                        // window.open(res.data.URI,'_blank');
                                    } else {
                                        $('.loading').hide();
                                    }
                                }).catch(function(err) {
                                $('.loading').show();
                                console.log("AXIOS ERROR: ", err);
                            });

                        });
                        @else
                        $('#accept-valuation-button-api-{{$valuation->company->id}}').on('click', function(e) {
                            e.preventDefault();
                            $('.loading').show();

                            var url = $(this).attr('href');
                            setTimeout(function () {
                                $('.loading').hide();
                                window.location = url;
                            }, 1500);
                        });
                        @endif
                        @else
                        $('#accept-valuation-button-{{$valuation->company->id}}').on('click', function(e) {
                            e.preventDefault();
                            var data = {
                                'company_id': '{{$valuation->company->id}}',
                                'vrm': '{{$vehicle->numberplate}}',
                                'uuid': '{{$valuation->uuid}}',
                                'vid': '{{$valuation->id}}',
                            };

                            var $button = $(this);
                            $button.html('&nbsp;');
                            $button.addClass('button-spinner-image');
                            $button.unbind('click');

                            axios.patch('{{ route('partnerWebsitePage', [$valuation->company->website,'your-offer']) }}', data)
                                .then(({data}) => {
                                    setTimeout(() => {
                                        $('.loading').hide();
                                        window.location.href = '{{ route('partnerWebsitePage', [$valuation->company->website,'your-offer']) }}';
                                    }, 1000);
                                });

                            return false;
                        });
                        @endif
                </script>
        </td>
    </tr>
@endif