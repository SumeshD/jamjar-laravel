@push('scripts-after')
    <script type="text/javascript">
        $(function() {
            var $externalSources = $('.offers-source-external');
            var externalSourcesCount = $externalSources.length;
            var requestsCounter = 0;
            var $valuationsContainer = $('.box--first tbody');
            var $valuationsCounter = 0;

            var getAjaxCallData = function($form) {
                return {
                    'type': $form.attr('method'),
                    'url': $form.attr('action'),
                    'data': $form.serializeArray(),
                    'success': function(data) {
                        console.log(data);
                        console.log($form.serializeArray());
                        if(data.views.length > 0){
                            $valuationsCounter++;
                        }
                        for (var viewIndex in data.views) {
                            var view = data.views[viewIndex];
                            $valuationsContainer.prepend(view);
                        }
                        var $valuations = $('.valuation-holder');
                        $valuations.sort(function(elementA, elementB) {
                            var priceA = parseInt($(elementA).attr('data-price'));
                            var priceB = parseInt($(elementB).attr('data-price'));

                            return priceB - priceA;
                        }).appendTo($valuationsContainer);
                    }
                };
            };

            $externalSources.each(function() {
                var $form = $(this);
                var ajaxData = getAjaxCallData($form);
                ajaxData.complete = function() {
                    requestsCounter++;
                    if (requestsCounter >= externalSourcesCount) {
                        var $internalSources = $('.offers-source-internal');
                        $internalSources.each(function() {
                            var $internalSourceForm = $(this);
                            var internalAjaxData = getAjaxCallData($internalSourceForm);
                            internalAjaxData.complete = function() {
                                $('.offers-loading-box').hide();

                                if ($valuationsCounter == 0) {
                                    $('.valuation-table-header-only').hide();
                                    $('#improve-wrapper').hide();
                                    $('.empty-valuations-container').fadeIn();
                                }
                            };
                            $.ajax(internalAjaxData)
                        });
                    }
                };
                $.ajax(ajaxData);
            });
        });
    </script>
@endpush

@if (count($valuations) == 0)
    <form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
        {{ csrf_field() }}
        <input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_CTB }}">
        <input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
        <input type="hidden" name="create-valuation" value="1">
        <input name="valuation-group" type="hidden" value="{{$valuationGroup}}">
    </form>
    <form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
        {{ csrf_field() }}
        <input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_WWAC }}">
        <input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
        <input type="hidden" name="create-valuation" value="1">
        <input name="valuation-group" type="hidden" value="{{$valuationGroup}}">
    </form>
    <form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
        {{ csrf_field() }}
        <input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_M4YM }}">
        <input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
        <input type="hidden" name="create-valuation" value="1">
        <input name="valuation-group" type="hidden" value="{{$valuationGroup}}">
    </form>

    <form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
        {{ csrf_field() }}
        <input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_WBCT }}">
        <input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
        <input type="hidden" name="create-valuation" value="1">
        <input name="valuation-group" type="hidden" value="{{$valuationGroup}}">
    </form>

    <form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
        {{ csrf_field() }}
        <input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_MW }}">
        <input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
        <input type="hidden" name="create-valuation" value="1">
        <input name="valuation-group" type="hidden" value="{{$valuationGroup}}">
    </form>
    <form method="post" class="offers-source-internal" action="{{ route('saveValuationDraftFromSource') }}">
        {{ csrf_field() }}
        <input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_INTERNAL }}">
        <input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
        <input type="hidden" name="create-valuation" value="1">
        <input name="valuation-group" type="hidden" value="{{$valuationGroup}}">
    </form>
@endif