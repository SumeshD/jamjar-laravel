<?php /** @var \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface $offeredValue */ ?>
<?php /** @var \JamJar\Vehicle $vehicle */
if(!isset($firstOffer)) {
    $firstOffer = false;
}
if(!isset($prioritiseShowcase)) {
    $prioritiseShowcase = (bool) \JamJar\Setting::get('valuation_page_prioritise_showcase');
}
$freeCollection = false;
?>

<tr class="valuation-draft-holder" data-price="{{ $offeredValue->getPriceInPence() }}">
    <td style="text-align: center">
        @if(!$offeredValue->scrap)
            @if ($offeredValue->isCollectionAvailable())
                @if ($offeredValue->getCollectionFeeInPence() === 0)
                    @php($freeCollection = true)
                    Free collection included
                @endif
            @endif
        @else
            @if($offeredValue->collection_part)
                @if ($offeredValue->getCollectionFeeForCTB() === 0)
                    @php($freeCollection = true)
                    Free collection included
                @endif
            @endif
        @endif
        @if (!$freeCollection)
            Drop-off to branch
        @endif
{{--        {{ $offeredValue->getLocation()->city  }}--}}
    </td>
    <td>
        <ul>
            <li style="background-image: url(/images/ux/kb_check_icon.5c24e777.png)">{{$offeredValue->getValuationValidityInDays()}} days quote</li>
            @if ($offeredValue->getSource() !== \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VALUE_SOURCE_EXTERNAL)
                @if ((!$offeredValue->getBankTransferFeeInPence()))
                    <li style="background-image: url(/images/ux/kb_check_icon.5c24e777.png)">
                    Free Bank Transfer
                @else
                    <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                    Offers Bank Transfer
                    </li>
                    <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                    &#163;{{ number_format($offeredValue->getBankTransferFeeInPence() / 100) }} Bank Transfer Fee
                @endif
                    </li>
            @else
                @if ($offeredValue->getPaymentMethod() == \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::PAYMENT_METHOD_BANK_TRANSFER)
                    @if ((!$offeredValue->getBankTransferFeeInPence()))
                        <li style="background-image: url(/images/ux/kb_check_icon.5c24e777.png)">
                        Free Bank Transfer
                        </li>
                    @else
                        <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                        &#163;{{ number_format($offeredValue->getBankTransferFeeInPence() / 100) }} Bank Transfer Fee
                        </li>
                    @endif
                @else
                    <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                    Payment via {{ ucwords($offeredValue->getPaymentMethod()) }}
                    </li>
                @endif
            @endif
        </ul>
{{--    </td>--}}
{{--    <td>--}}
        <ul>

            @if(!$offeredValue->scrap)
                @if ($offeredValue->isCollectionAvailable())
                        @if ($offeredValue->getCollectionFeeInPence() === 0)
                        <li style="background-image: url(/images/ux/kb_check_icon.5c24e777.png)">
                            Free Collection
                        @else
                        <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                            &#163;{{ number_format($offeredValue->getCollectionFeeInPence() / 100) }} Collection Fee
                        @endif
                    </li>
                @endif
                @if ($offeredValue->isDropOffAvailable())
                    @if (($offeredValue->getDropOffFeeInPence() > 0))
                        <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                            &#163;{{ number_format($offeredValue->getDropOffFeeInPence() / 100) }} Dropoff Fee
                        </li>
                    @else
                        <li style="background-image: url(/images/ux/kb_check_icon.5c24e777.png);">
                            Drop-off to branch
                        </li>
                    @endif
                @endif
            @else
                @if($offeredValue->collection_part)
                        @if ($offeredValue->getCollectionFeeForCTB() === 0)
                            <li style="background-image: url(/images/ux/kb_check_icon.5c24e777.png)">
                            Free Collection
                        @else
                            <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                            &#163;{{ number_format($offeredValue->getCollectionFeeForCTB()) }} Collection Fee
                        @endif
                    </li>
                @endif
            @endif


            @if ($offeredValue->getAdminFeeInPence() > 0)
                <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                    &#163;{{ number_format($offeredValue->getAdminFeeInPence() / 100) }} Admin Fee
            @else
                    <li style="url(/images/ux/kb_check_icon.5c24e777.png)">
                No Admin Fee
            @endif
            </li>
        </ul>
{{--    </td>--}}
    <td class="clear-float"  style="    font-size: 3.6rem; font-weight: 500; line-height: 1.17em; text-align: center;">
        @if ($offeredValue->scrap and $offeredValue->isCollectionAvailable() and $offeredValue->isDropOffAvailable())
            @if ($offeredValue->collection_part)
                &#163;{{number_format($offeredValue->collection_new_price)}}
            @else
                &#163;{{number_format($offeredValue->dropoff_new_price)}}
            @endif
        @else
            @if ($offeredValue->hasCompleteSale())
                <?php /** @var \JamJar\Sale $sale */ $sale = $offeredValue->getSale(); ?>
                &#163;{{ number_format(($sale->price)) }}
            @elseif ($offeredValue->isCollectionAvailable() and $offeredValue->isDropOffAvailable())
                &#163;{{ number_format(($offeredValue->getPriceInPence() / 100)) }}
            @else
                @if ($offeredValue->isCollectionAvailable())
                    &#163;{{ number_format(($offeredValue->getPriceInPence() / 100)) }}
                @else
                    &#163;{{ number_format(($offeredValue->getPriceInPence() / 100)) }}
                @endif
            @endif
        @endif
    </td>
    <td class="width--36 padding-left--36" style=" text-align: center; font-size: 1.6rem">
        @if($prioritiseShowcase)
            <a href="{{ route('showVehicleDetails', [$vehicle, $offeredValue->getId(),'showcase']) }}" title="Showcase for 7 days">Showcase for free</a>
            <span>or</span><br>
            <a href="{{ route('showVehicleDetails', [$vehicle, $offeredValue]) }}">@if($firstOffer)Accept the best current instant offer now @else Accept this offer now @endif</a>
        @else
            <a class="width-390" id="accept-offer" style="background-color: #F47D0F; color: white" href="{{ route('showVehicleDetails', [$vehicle, $offeredValue]) }}">@if($firstOffer)Accept the best current instant offer now @else Accept this offer now @endif</a>
            <span>or</span><br>
            <a href="{{ route('showVehicleDetails', [$vehicle, $offeredValue->getId(),'showcase']) }}" title="Showcase for 7 days">Showcase for free</a>
        @endif
    </td>
</tr>
