@extends('newlayouts.app')

@section('content')
    <?php
    $errorArray = [];
    if($errors->all()) {
        $errorArray = array_unique($errors->all());
        //print_r($errorArray); die;
    }

    $selected = 0;
    if(old('keys-count') == 1 or $vehicle->getKeysCount() == 1) {
        $selected = 1;
    } elseif (old('keys-count') == 2 or $vehicle->getKeysCount() == 2) {
        $selected = 2;
    } elseif (old('keys-count') == 3 or $vehicle->getKeysCount() == 3) {
        $selected = 3;
    }
    $postCode = isset($postCode) ? $postCode : '';
    ?>

    <main role="main">
        <section class="page-section page-section--shaded">
            <div class="page-section__content page-section__content--valuation-progress">
                <table>
                    <tr>
                        <td>
                            <img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}" width="120">
                        </td>
                        <td class="padding-left--20">
                            <h3>{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</h3>
                            <p>{{$vehicle->numberplate}}</p>
                            <a href="{{ route('/')}}" title="Change vehicle"><small>Change vehicle</small></a> <span>•</span> <a href="{{ route('showVehicleInformation', $vehicle) }}" title="Edit vehicle"><small>Edit details</small></a> <span>•</span> <a href="#" hidden id="show-details-button" title="Show details"><small>Show</small></a><a href="#" id="hide-details-button" title="Hide details"><small>Hide</small></a>
                        </td>
                    </tr>
                </table>
                <table style="margin-left:50px; margin-top: 20px; font-size: 10pt" class="car-details">
                    <tr>
                        <td>Derivative:</td>
                        <td>{{$vehicle->meta->derivative}}</td>
                    </tr>
                    <tr>
                        <td>Plate Year:</td>
                        <td>{{$vehicle->meta->plate_year}}</td>
                    </tr>
                    <tr>
                        <td>Registration:</td>
                        <td>{{$vehicle->numberplate}}</td>
                    </tr>
                    <tr>
                        <td>Mileage:</td>
                        <td>{{$vehicle->meta->mileage}}</td>
                    </tr>

                </table>
                <table style="margin-left:50px; margin-top: 20px; font-size: 10pt" class="car-details">
                    <tr>
                        <td>Service History:</td>
                        <td>{{$vehicle->meta->service_history}}</td>
                    </tr>
                    <tr>
                        <td>MOT:</td>
                        <td>{{$vehicle->meta->mot}}</td>
                    </tr>
                    <tr>
                        <td>Colour:</td>
                        <td>{{$vehicle->meta->colour->title}}</td>
                    </tr>
                    <tr>
                        <td>Write Off:</td>
                        <td>{{ trans('vehicles.WriteOff' . $vehicle->meta->write_off) }}</td>
                    </tr>
                    <tr>
                        <td>Non Runner:</td>
                        <td>{{ trans('vehicles.NonRunner' . $vehicle->meta->non_runner) }}</td>
                    </tr>

                </table>
                <div class="progress-wrapper margin-right--15">
                    <div class="progress-wrapper__progress-point progress-wrapper__progress-point--passed">
                        <span>Vehicle</span>
                        1
                    </div>
                    <div class="progress-wrapper__progress-waypoint"></div>
                    <div class="progress-wrapper__progress-point progress-wrapper__progress-point--passed">
                        <span>Valuations</span>
                        2
                    </div>
                    <div class="progress-wrapper__progress-waypoint"></div>
                    <div class="progress-wrapper__progress-point progress-wrapper__progress-point--active">
                        <span style="width:100px;">Vehicle location</span>
                        3
                    </div>
                    <div class="progress-wrapper__progress-waypoint"></div>
                    <div class="progress-wrapper__progress-point">
                        <span></span>
                        4
                    </div>
                </div>
            </div>
        </section>
        @if($showcase)
            <section class="page-section">
                <div class="page-section__content page-section__content--valuation-step3 page-section__content--valuation">
                    <form id="showcaseForm" action="{{ route('storeVehicleUserDetails', $vehicle) }}" method="POST" enctype="multipart/form-data">
                        @if ($errors->any())
                            <div class="page-section__content page-section__content--valuation" style="display: inline">
                                <div class="alert alert--danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        {{--                    <input type="hidden" name="valuation" value="{{$valuationDraft->getId()}}">--}}
                        <div class="form-section">
                            <div class="form-section__form">
                                <h2>
                                    Location of your vehicle
                                </h2>
                                <label for="address_line_one">
                                    <span>Address *</span>
                                    <input type="text" personal="true" name="address_line_one" id="address_line_one"
                                           placeholder="@if($postCode != ''){{$postCode}}@else{{old('address_line_one')}}@endif">
                                    <a href="#" id="manualAddressPrompt" class="manual-address-prompt">or enter address manually</a>
                                </label>
                                <label for="address_line_two" class="hidden-address-field">
                                    Address 2
                                    <input type="text" name="address_line_two" id="address_line_two">
                                </label>
                                <label for="city" class="hidden-address-field">
                                    City
                                    <input type="text" personal="true" name="city" id="city">
                                </label>
                                <label for="county" class="hidden-address-field">
                                    County
                                    <input type="text" name="county" id="county">
                                </label>
                                <label for="postcode" class="hidden-address-field">
                                    Postcode *
                                    <input type="text" personal="true" name="postcode" id="postcode">
                                </label>
                                <label for="fullname">
                                    Your full name *
                                    <input type="text" personal="true" name="fullname" id="fullname" autofocus="">
                                </label>
                                <label for="email">
                                    Your email address *
                                    <input type="email" personal="true" name="email" id="email">
                                </label>
                                <label for="telephone_number">
                                    Your mobile number *
                                    <input type="tel" personal="true" name="telephone_number" id="telephone_number">
                                </label>
                                <label for="password">
                                    Set a password *
                                    <input type="password" personal="true" name="password" id="password">
                                </label>
                                <fieldset>
                                    <p class="label">Keep in touch with Jamjar.com</p>
                                    <input type="radio" id="keepInTouchYes" name="marketing" value="1">
                                    <label for="keepInTouchYes">Yes</label>
                                    <input type="radio" id="keepInTouchNo" name="marketing" value="0" checked="">
                                    <label for="keepInTouchNo">No</label>
                                </fieldset>
                                <hr>
                                <div class="clearfix"></div>

                                {!! app('captcha')->render() !!}

                                <div class="clearfix"></div>
                            </div>
                            <aside>
                                <article>
                                    <img src="/images/ux/kb_avatar_placeholder.c7013877.png" alt="Mr. H" width="72">
                                    <p>&#8220;Incredibly easy process&#8221;</p>
                                    <p class="small">Best price across all selling sites. I will definately use JamJar again and reccommended to all of my friends and family.</p>
                                    <p class="small">Mr H • Nottingham</p>
                                </article>
                                <img src="/images/ux/kb_trusted_site.274ddd7b.png" alt="Trusted site" width="264">
                            </aside>
                        </div>
                        <div class="form-section">
                            <input type="hidden" value="1" name="showcase">
                            <div class="form-section__form" style="cursor: pointer">
                                <div id="show-showcase-section" style="cursor: pointer;">
                                    <div class="submitShowcase" style="float: left; margin-bottom: 24px; font-size: 2.3rem; font-weight: 500; line-height: 1.25em; cursor: pointer">
                                        Showcase your vehicle
                                    </div>
                                    <div style="float: left; margin-top: 10px; margin-left: 10px; cursor: pointer;" class="arrow-down submitShowcase"></div>
                                    {{--                                <div style="float: left; margin-top: 10px; margin-left: 10px;" class="arrow-down"></div>--}}
                                    <div class="submitShowcase" style="font-size: 1.3rem; margin-bottom: 24px; float: left; cursor: pointer">
                                        When you showcase your vehicle it will be visible to another 912 of our buying partners who can offer you a bespoke valuation based on your location and the service history, specification and images.
                                    </div>
                                </div>
                                {{--                            <div class="form-section__form" id="hide-showcase-section" style="display: none; cursor: pointer;">--}}
                                {{--                                <div style="float: left; margin-bottom: 24px; font-size: 2.3rem; font-weight: 500; line-height: 1.25em;">--}}
                                {{--                                    Showcase your vehicle--}}
                                {{--                                </div>--}}
                                {{--                                <div style="float: left; margin-top: 10px; margin-left: 10px;" class="arrow-up"></div>--}}
                                {{--                                <div style="font-size: 1.3rem; margin-bottom: 24px; float: left">--}}
                                {{--                                    When you showcase your vehicle it will be visible to another 912 of our buying partners who can offer you a bespoke valuation based on your location and the service history, specification and images.--}}
                                {{--                                </div>--}}
                                {{--                            </div>--}}
                                {{--                            <div class="form-section__form" id="showcaseSection">--}}
                                {{--                            <label for="pictures">--}}
                                {{--                                RECOMMENDED<br>--}}
                                {{--                                We recommend you take some great photos--}}
                                {{--                                <div class="photo-booth">--}}
                                {{--                                    <label for="image1">--}}
                                {{--                                        <input type="file" accept="image/*" name="image[0]" id="image1">--}}
                                {{--                                    </label>--}}
                                {{--                                    <label for="image2">--}}
                                {{--                                        <input type="file" accept="image/*" name="image[1]" id="image2">--}}
                                {{--                                    </label>--}}
                                {{--                                    <label for="image3">--}}
                                {{--                                        <input type="file" accept="image/*" name="image[2]" id="image3">--}}
                                {{--                                    </label>--}}
                                {{--                                    <label for="image4">--}}
                                {{--                                        <input type="file" accept="image/*" name="image[3]" id="image4">--}}
                                {{--                                    </label>--}}
                                {{--                                    <label for="image5">--}}
                                {{--                                        <input type="file" accept="image/*" name="image[4]" id="image5">--}}
                                {{--                                    </label>--}}
                                {{--                                    <label for="image6">--}}
                                {{--                                        <input type="file" accept="image/*" name="image[5]" id="image6">--}}
                                {{--                                    </label>--}}
                                {{--                                    <label for="image7">--}}
                                {{--                                        <input type="file" accept="image/*" name="image[6]" id="image7">--}}
                                {{--                                    </label>--}}
                                {{--                                    <label for="image8">--}}
                                {{--                                        <input type="file" accept="image/*" name="image[7]" id="image8">--}}
                                {{--                                    </label>--}}
                                {{--                                </div>--}}
                                {{--                                <a href="#" title="Guide">Unsure? Take a look at our guide</a>--}}
                                {{--                            </label>--}}
                                {{--                            <label for="nrKeys">--}}
                                {{--                                How many keys do you have for the vehicle--}}
                                {{--                                <select name="nrKeys" id="nrKeys">--}}
                                {{--                                    <option disabled="" selected="">Please select</option>--}}
                                {{--                                    <option {{ ($selected == 1) ? 'selected' : '' }} value="1">1</option>--}}
                                {{--                                    <option {{ ($selected == 2 || $selected == 0) ? 'selected' : '' }} value="2">2</option>--}}
                                {{--                                    <option {{ ($selected == 3) ? 'selected' : '' }} value="3">3+</option>--}}
                                {{--                                </select>--}}
                                {{--                            </label>--}}
                                {{--                            <label for="serviceInfo">--}}
                                {{--                                Additional service information--}}
                                {{--                                <textarea name="serviceInfo" id="serviceInfo" rows="6"></textarea>--}}
                                {{--                            </label>--}}
                                {{--                            <label for="specsInfo">--}}
                                {{--                                Additional specification information--}}
                                {{--                                <textarea name="specsInfo" id="specsInfo" rows="6"></textarea>--}}
                                {{--                            </label>--}}
                                {{--                                <p class="small">--}}
                                {{--                                    We will inform you of any improved valuations via text message and email.--}}
                                {{--                                </p>--}}
                                {{--                            </div>--}}
                                <button class="submitShowcase" type="submit">Continue</button>
                                <button id="safariSubmitForm" style="display: none" type="submit">Continue</button>
                                <a id="skipAddingMoreDetails" href="{{route("showValuationDraftsForClient",$vehicle)}}" title="Valuations"><small>Skip adding more vehicle details and go back to valuations</small></a>
                            </div>
                            <aside style="width: 50%">
                            </aside>
                        </div>
                    </form>
                </div>
            </section>
        @else
            <section class="page-section">
                <div class="page-section__content page-section__content--valuation-step4 page-section__content--centered">
                    <h2>
                        Great choice, are you ready to get a great deal on your vehicle?
                    </h2>
                    <p class="small">
                        We&#39;ve delivered on our promise to provide you with free valuations on your vehicle. Now we need to know a little more about where you vehicle is located so we can connect you with our buying partners.
                    </p>
                    <table>
                        <thead>
                        <tr>
                            <th style="text-align: center">Dealer</th>
                            <th style="text-align: center" colspan="2">Details</th>
                            <th style="text-align: center" >Valuation</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="valuation-draft-holder" data-price="{{ $valuationDraft->getPriceInPence() }}">
                            <td style="text-align: center">
                                {{ $valuationDraft->getLocation()->city  }}
                            </td>
                            <td style="width: 20%"></td>
                            <td>
                                <ul>
                                    <li>{{$valuationDraft->getValuationValidityInDays()}} days quote</li>
                                    <li>
                                        @if ($valuationDraft->getSource() !== \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VALUE_SOURCE_EXTERNAL)
                                            @if ((!$valuationDraft->getBankTransferFeeInPence()))
                                                Free Bank Transfer
                                            @else
                                                Offers Bank Transfer
                                    </li>
                                    <li>
                                        &#163;{{ number_format($valuationDraft->getBankTransferFeeInPence() / 100) }} Bank Transfer Fee
                                        @endif
                                        @else
                                            @if ($valuationDraft->getPaymentMethod() == \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::PAYMENT_METHOD_BANK_TRANSFER)
                                                @if ((!$valuationDraft->getBankTransferFeeInPence()))
                                                    Free Bank Transfer
                                                @else
                                                    &#163;{{ number_format($valuationDraft->getBankTransferFeeInPence() / 100) }} Bank Transfer Fee
                                                @endif
                                            @else
                                                Payment via {{ ucwords($valuationDraft->getPaymentMethod()) }}
                                            @endif
                                        @endif
                                    </li>
                                </ul>
                                {{--                        </td>--}}
                                {{--                        <td>--}}
                                <ul>

                                    @if(!$valuationDraft->scrap)
                                        @if ($valuationDraft->isCollectionAvailable())
                                            <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                @if ($valuationDraft->getCollectionFeeInPence() === 0)
                                                    Free Collection
                                                @else
                                                    &#163;{{ number_format($valuationDraft->getCollectionFeeInPence() / 100) }} Collection Fee
                                                @endif
                                            </li>
                                        @endif
                                        @if ($valuationDraft->isDropOffAvailable())
                                            @if (($valuationDraft->getDropOffFeeInPence() > 0))
                                                <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                    &#163;{{ number_format($valuationDraft->getDropOffFeeInPence() / 100) }} Dropoff Fee
                                                </li>
                                            @endif
                                        @endif
                                    @else
                                        @if($valuationDraft->collection_part)
                                            <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                @if ($valuationDraft->getCollectionFeeForCTB() === 0)
                                                    Free Collection
                                                @else
                                                    &#163;{{ number_format($valuationDraft->getCollectionFeeForCTB()) }} Collection Fee
                                                @endif
                                            </li>
                                        @endif
                                    @endif

                                    <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                        @if ($valuationDraft->getAdminFeeInPence() > 0)
                                            &#163;{{ number_format($valuationDraft->getAdminFeeInPence() / 100) }} Admin Fee
                                        @else
                                            No Admin Fee
                                        @endif
                                    </li>
                                </ul>
                            {{--                        </td>--}}
                            <td style="font-size: 3.6rem; font-weight: 500; line-height: 1.17em; text-align: center;">
                                @if ($valuationDraft->scrap and $valuationDraft->isCollectionAvailable() and $valuationDraft->isDropOffAvailable())
                                    @if ($valuationDraft->collection_part)
                                        &#163;{{number_format($valuationDraft->collection_new_price)}}
                                    @else
                                        &#163;{{number_format($valuationDraft->dropoff_new_price)}}
                                    @endif
                                @else
                                    @if ($valuationDraft->hasCompleteSale())
                                        <?php /** @var \JamJar\Sale $sale */ $sale = $valuationDraft->getSale(); ?>
                                        &#163;{{ number_format(($sale->price)) }}
                                    @elseif ($valuationDraft->isCollectionAvailable() and $valuationDraft->isDropOffAvailable())
                                        &#163;{{ number_format(($valuationDraft->getPriceInPence() / 100)) }}
                                    @else
                                        @if ($valuationDraft->isCollectionAvailable())
                                            &#163;{{ number_format(($valuationDraft->getPriceInPence() / 100)) }}
                                        @else
                                            &#163;{{ number_format(($valuationDraft->getPriceInPence() / 100)) }}
                                        @endif
                                    @endif
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <form action="{{ route('storeVehicleUserDetails', $vehicle) }}" method="POST" enctype="multipart/form-data">
                        @if ($errors->any())
                            <div class="page-section__content page-section__content--valuation" style="display: inline">
                                <div class="alert alert--danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <input type="hidden" name="valuation" value="{{$valuationDraft->getId()}}">
                        <label for="address_line_one">
                            <span>Address *</span>
                            <input type="text" name="address_line_one" id="address_line_one"
                                   placeholder="@if($postCode != ''){{$postCode}}@else{{old('address_line_one')}}@endif">
                            <a href="#" id="manualAddressPrompt" class="manual-address-prompt">or enter address manually</a>
                        </label>
                        <label for="address_line_two" class="hidden-address-field">
                            Address 2
                            <input type="text" name="address_line_two" id="address_line_two">
                        </label>
                        <label for="city" class="hidden-address-field">
                            City
                            <input type="text" name="city" id="city">
                        </label>
                        <label for="county" class="hidden-address-field">
                            County
                            <input type="text" name="county" id="county">
                        </label>
                        <label for="postcode" class="hidden-address-field">
                            Postcode *
                            <input type="text" name="postcode" id="postcode">
                        </label>
                        <label for="fullName">
                            Your full name *
                            <input type="text" name="fullname" id="fullname">
                        </label>
                        <label for="email">
                            Your email address *
                            <input type="email" name="email" id="email">
                        </label>
                        <label for="phone">
                            Your mobile number *
                            <input type="tel" name="telephone_number" id="telephone_number">
                        </label>
                        <label for="password">
                            Set a password *
                            <input type="password" name="password" id="password">
                        </label>
                        <fieldset>
                            <p class="label">Keep in touch with Jamjar.com</p>
                            <input type="radio" id="keepInTouchYes" name="marketing" value="1" checked="">
                            <label for="keepInTouchYes">Yes**</label>
                            <input type="radio" id="keepInTouchNo" name="marketing" value="0">
                            <label for="keepInTouchNo">No</label>
                        </fieldset>
                        <div class="clearfix"></div>

                        {!! app('captcha')->render() !!}

                        <div class="clearfix"></div>
                        <button type="submit">Continue</button>
                        <p class="small">** Keeping in touch with Jamjar.com will provide you with marketing updates, quotes, career opportunities or general queries.<br>You can unsubscribe at any time.</p>
                    </form>
                </div>
            </section>
        @endif
        <section class="page-section mob-hide">
            <div class="page-section__content page-section__content--centered">
                <h3>
                    As featured in
                </h3>
                <section class="page-section__content__media-logos">
                    <div class="page-section__content__media-logos__carousel-tape">
                        <img src="/images/ux/kb_media_logo_1.e77e5338.png" alt="The Guardian" width="154">
                        <img src="/images/ux/kb_media_logo_2.d4f98b37.png" alt="BBC" width="169">
                        <img src="/images/ux/kb_media_logo_3.12be4287.png" alt="Top Gear" width="224">
                        <img src="/images/ux/kb_media_logo_4.c8c95f5f.png" alt="The Telegraph" width="297">
                    </div>
                </section>
                <h3>
                    Sell any car fast
                </h3>
                <section class="page-section__content__auto-logos">
                    <div class="page-section__content__auto-logos__carousel-tape">
                        <img src="/images/ux/kb_auto_logo_1.e7ab6737.png" alt="BMW" width="120">
                        <img src="/images/ux/kb_auto_logo_2.c1b4defc.png" alt="Audi" width="120">
                        <img src="/images/ux/kb_auto_logo_3.0cb5b59c.png" alt="Mercedes" width="120">
                        <img src="/images/ux/kb_auto_logo_4.21c602f3.png" alt="Ford" width="120">
                        <img src="/images/ux/kb_auto_logo_5.f20e53f2.png" alt="Volkswagen" width="120">
                    </div>
                </section>
            </div>
        </section><!-- Media features -->
        <section class="page-section page-section--shaded mob-hide">
            <div class="page-section__content page-section__content--video-hero">
                <h3>
                    The original car buying comparison service
                </h3>
                <div class="page-section__content__video-hero paused">
                    <article class="video-hero__badge">
                        <p><strong>Watch the latest TV ad to see how it works</strong></p>
                    </article>
                    <video width="556" height="313" poster="/images/ux/kb_video_placeholder.ce2fac56.png" onclick="this.paused ? this.play() : this.pause();" playsinline="">
                        <source src="/images/ux/kb_ad_1.06f2fe74.mp4" type="video/mp4">
                        <source src="/images/ux/kb_ad_1.8abb1b5f.ogv" type="video/ogg">
                    </video>
                </div>
                <article>
                    <p><strong>Hundreds of UK car buyers</strong></p>
                    <p class="small">
                        See offers from the hundreds of expert car buying businesses that work with Jamjar.
                    </p>
                </article>
                <article>
                    <p><strong>Fully transparent, always</strong></p>
                    <p class="small">
                        Get all the details you need right here to make the best decision for your vehicle.
                    </p>
                </article>
                <article>
                    <p><strong>Cars, vans and scrap</strong></p>
                    <p class="small">
                        We&#39;ll help you find the right deal, whether it&#39;s a car, a van or destined for the scrapyard.
                    </p>
                </article>
                <article>
                    <a href="#" title="More about Jamjar">More about Jamjar</a>
                </article>
            </div>
        </section><!-- Media features -->
    </main><!-- Main page content -->

    {{-- <div class="loading" style="display:none;"></div> --}}
    <div class="loader" style="display: none;">
        <div class="loader__box">
            <div class="loader__content">
                <i class="fa fa-car"></i>
                <h3>Just a moment, we're collecting your offers</h3>
            </div>
        </div>
    </div>

    @push('scripts-after')
        <script>



            $('#storeUserDetails').on('submit', function(e) {
                $('.loader').show();
                console.log('track: reach valuations');
                window.adalyserTracker("trackEvent", "lce3", {},true);
            });
        </script>
    @endpush

    @push('scripts-after')
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            @if ($errors->any())
            $(document).ready(function(){
                Swal.fire({
                    html:
                        '<h2>' +
                        '<ul>' +
                            @foreach ($errors->all() as $error)
                                '<li>' + "{{ $error }}" + '</li>' +
                            @endforeach
                                '</ul>' +
                        '</h2>',
                    showCloseButton: true,
                    showCancelButton: false,
                    showConfirmButton: false,
                    width: "800px",
                    padding: "5%",
                    allowOutsideClick: false,
                    onAfterClose: () => window.scrollTo(0,0)

                });
            });
            @endif
            var isShowcaseVisible = false;

            $(".submitShowcase").on('click', function(e) {
                let $reCaptchaIframe = $('iframe[title="recaptcha challenge"]');
                if ($reCaptchaIframe) {
                    let $reCaptchaOverlay = $reCaptchaIframe.parent();
                    $reCaptchaOverlay.css('position', 'fixed');
                }
                e.preventDefault();
                var submit = false;
                var triggerNoPersonalDetailsDialog = false;
                var noOfMissingItems = 0;
                var missingFields = "";
                var firstMissed = "";

                const personalFieldMap = {
                    "address_line_one": "address",
                    "postcode": "postcode",
                    "fullname": "full name",
                    "email": "email address",
                    "telephone_number": "mobile number",
                    "password": "password"
                }

                $('input[type=file]').each(function(){
                    if($(this).val()){
                        submit = true;
                    }
                });
                if($("#serviceInfo").val()){
                    submit = true;
                }
                if($("#specsInfo").val()){
                    submit = true;
                }

                // if(!isShowcaseVisible){
                //     submit = true;
                // }

                $('input[personal]').each(function(index) {
                    if(!$(this).val()){
                        let property = $(this).attr("name");
                        if(property != 'city') {
                            console.log(property);
                            if(noOfMissingItems > 0){
                                missingFields += ', ';
                            } else {
                                firstMissed = property;
                            }
                            noOfMissingItems++;
                            missingFields += personalFieldMap[property];
                            triggerNoPersonalDetailsDialog = true;
                            submit = false;
                        }
                    }
                });
                if(submit){
                    let myForm = document.querySelector("form");
                    if(myForm.requestSubmit) {
                        myForm.requestSubmit();
                    } else {
                        $("#safariSubmitForm").click();
                    }
                } else {
                    if(triggerNoPersonalDetailsDialog){
                        console.log(firstMissed);
                        Swal.fire({
                            html:
                                '<h2>Oops we can’t create account without ' + missingFields + '</h2>' +
                                '<div style="padding-top: 20px;"><a id="close-swal" style="width:70%; margin: 0 auto;" class="showcase-success" href="#">Continue</a></div>' +
                                '<p class="small">&nbsp;</p>' +
                                '<a href=\"{{route("showValuationDraftsForClient",$vehicle)}}\" ><p class="small" style="color: #545454">Back to current offers</p></a>',
                            showCloseButton: true,
                            showCancelButton: false,
                            showConfirmButton: false,
                            width: "800px",
                            padding: "5%",
                            allowOutsideClick: false,
                            onAfterClose: () => window.scrollTo(0,0)

                        });
                    } else {
                        let myForm = document.querySelector("form");
                        if(myForm.requestSubmit) {
                            myForm.requestSubmit();
                        } else {
                            $("#safariSubmitForm").click();
                        }
                        {{--Swal.fire({--}}
                        {{--    html:--}}
                        {{--        '<h2>No added vehicle details</h2>' +--}}
                        {{--        '<p class="small">We recommend you add images, highlight its full specification and service history to another 912 professional UK buyers currently interested in your vehicle.</p>' +--}}
                        {{--        '<div style="padding-top: 20px;"><a id="add-details" style="width:70%; margin: 0 auto;" class="showcase-success" href="#" title="See your current best valuations">Add vehicle details</a></div>' +--}}
                        {{--        '<p class="small">or</p>' +--}}
                        {{--        '<a id="skipDetails" href=\"{{route("showValuationDraftsForClient",$vehicle)}}\" ><p class="small" style="color: #545454">Back to current offers</p></a>',--}}
                        {{--    showCloseButton: true,--}}
                        {{--    showCancelButton: false,--}}
                        {{--    showConfirmButton: false,--}}
                        {{--    width: "800px",--}}
                        {{--    padding: "5%",--}}
                        {{--    allowOutsideClick: false--}}

                        {{--})--}}
                    }
                }

                $("#add-details").on("click", function() {
                    let myForm = document.querySelector("form");
                    if(myForm.requestSubmit) {
                        myForm.requestSubmit();
                    } else {
                        $("#safariSubmitForm").click();
                    }
                })

                $("#close-swal").on("click", function() {
                    Swal.close();
                })
                $("#showcaseContinue").on("click", function() {
                    Swal.close();
                    let myForm = document.querySelector("form");
                    if(myForm.requestSubmit) {
                        myForm.requestSubmit();
                    } else {
                        $("#safariSubmitForm").click();
                    }
                });
                $("#skipDetails").on('click', function(e) {
                    e.preventDefault();
                    var submit = true;
                    $('input[personal]').each(function() {
                        if(!$(this).val()){
                            let property = $(this).attr("name");
                            if(property != 'city') {
                                submit = false;
                            }
                        }
                    });
                    if(submit) {
                        $('input[name=showcase]').val('0');
                        let myForm = document.querySelector("form");
                        if(myForm.requestSubmit) {
                            myForm.requestSubmit();
                        } else {
                            $("#safariSubmitForm").click();
                        }
                    } else {
                        window.location.href = $(this).attr("href");
                    }
                });

            });

            $("#skipAddingMoreDetails").on('click', function(e) {
                e.preventDefault();
                var submit = true;
                $('input[personal]').each(function() {
                    if(!$(this).val()){
                        let property = $(this).attr("name");
                        if(property != 'city') {
                            submit = false;
                        }
                    }
                });
                if(submit) {
                    $('input[name=showcase]').val('0');
                    let myForm = document.querySelector("form");
                    if(myForm.requestSubmit) {
                        myForm.requestSubmit();
                    } else {
                        $("#safariSubmitForm").click();
                    }
                } else {
                    window.location.href = $(this).attr("href");
                }
            });


            $('.js-show-vehicle-details').on('click', function(e) {
                e.preventDefault();
                $(this).toggleClass('active');
                if ($(this).hasClass('active')) {
                    $('.valuations__vehicle-details').slideDown();
                    $(this).html('Hide <i class="fa fa-chevron-up"></i>');
                    $(this).stop();
                } else {
                    $('.valuations__vehicle-details').slideUp();
                    $(this).html('Show <i class="fa fa-chevron-down"></i>');
                    $(this).stop();
                }
            });

            $("#show-details-button").on("click", function(event) {
                $(".car-details").show();
                $(".progress-wrapper").addClass("margin-right--15");
                $("#show-details-button").hide();
                $("#hide-details-button").show();
            });

            $("#hide-details-button").on("click", function(event) {
                $(".car-details").hide();
                $(".progress-wrapper").removeClass("margin-right--15");
                $("#hide-details-button").hide();
                $("#show-details-button").show();
            });

            // $("#show-showcase-section").click(function(e) {
            //     e.preventDefault();
            //     $("#showcaseSection").show();
            //     $("#hide-showcase-section").show();
            //     $(this).hide();
            //     isShowcaseVisible = true;
            // });

            $("#hide-showcase-section").click(function(e) {
                e.preventDefault();
                $("#showcaseSection").hide();
                $("#hide-showcase-section").hide();
                $("#show-showcase-section").show();
                isShowcaseVisible = false;
            });
        </script>
        <style type="text/css">

        </style>
    @endpush

@endsection
