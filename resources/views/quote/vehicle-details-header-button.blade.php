<?php $showLabel = isset($showLabel) ? $showLabel : true ?>
<div class="valuations__header">
    <div class="valuations__title">
        <h2>
            <img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}">
            <span>{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</span>
            <br />
                @if(!auth()->user()) <span class="valuations__badge js-show-vehicle-details">Show Details <i class="fa fa-chevron-down"></i></span> @else @if(!auth()->user()->isPartner()) <span class="valuations__badge js-show-vehicle-details">Show Details <i class="fa fa-chevron-down"></i></span> @else <span class="valuations__badge js-show-vehicle-details active">Hide Details <i class="fa fa-chevron-up"></i></span> @endif @endif
        </h2>
    </div>
</div>