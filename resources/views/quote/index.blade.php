@extends('newlayouts.app')
@section('content')
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
	<link href="{{ secure_asset('css/tw_home_page.css') }}" rel="stylesheet">
	<main role="main">
		<section id="masthead" class="main-banner gray-lite-bg">
			<div class="left-img">
				<img class="img-fluid" src="/images/ux/home/left-img.png" alt="">
			</div>
			<div class="masthead__content">
				<form class="reg-form needs-validation" action="{{ route('apiVrmInit') }}">
					<h1 class="mb-3 text-center text-white fw-900">The Car Selling Comparison Site</h1>
					<p class="mb-5 text-center text-white">
						Established in 1997 as the first online car selling comparison website!
					</p>
					<div class="compare-group mx-auto mt-3" role="group">
						<input type="text" id="vrm" name="vrm" required maxlength="8" placeholder="Enter reg" autofocus>
						<button class="go-btn my-4" type="submit">Go</button>
						@if (Auth::guest())
							<div>or <a class="color-blue fw-700 ls-22" href="/login" title="Sign in">SignIn</a> to view previews valuations</div>
						@endif
					</div>
					
				</form> <!-- Form -->
			</div>
			<div class="personal-info-needed orange-bg radius-21">
                    No personal <br> info needed
            </div>
		</section><!-- Masthead section -->

		<section class="masthead__vps gray-lite-bg">
			<div class="masthead__content">
				<article class="text-center">
					<div>
						<svg width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
							<circle cx="50" cy="50" r="50" fill="url(#paint0_linear_293_6172)" />
							<path
								d="M52.8332 67H47.1665V61.3333H52.8332V67ZM58.4998 55.6667H41.4998V72.6667H58.4998V55.6667ZM69.8332 42.35V27.3333H61.3332V34.7L49.9998 24.5L21.6665 50H30.1665L49.9998 32.1217L69.8332 50H78.3332L69.8332 42.35Z"
								fill="white" />
							<defs>
								<linearGradient id="paint0_linear_293_6172" x1="50" y1="0" x2="50" y2="100" gradientUnits="userSpaceOnUse">
									<stop stop-color="#E05013" />
									<stop offset="1" stop-color="#FF8932" />
								</linearGradient>
							</defs>
						</svg>
						<h2 class="color-dark">Home pickup</h2>
					</div>
				</article>
				<article class="text-center">
					<svg width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="50" cy="50" r="50" fill="url(#paint0_linear_293_6179)" />
						<path
							d="M80.5144 59.0853C80.131 57.6795 79.2364 56.4654 77.8945 55.7625C76.6165 55.0596 75.0828 54.9317 73.677 55.3791L60.1939 59.6604C59.7466 59.2131 59.2992 58.8936 58.7241 58.638L48.5638 54.5483C45.7522 53.3981 42.5571 53.2064 39.6816 53.9732L32.6524 55.8264V54.74C32.6524 53.8454 31.9495 53.0786 30.991 53.0786H21.6614C20.7668 53.0786 20 53.7815 20 54.74V75.444C20 76.3387 20.7029 77.1055 21.6614 77.1055H30.991C31.8856 77.1055 32.6524 76.4026 32.6524 75.444V74.1021L54.1232 77.8084C54.8262 77.9362 55.4652 78.0001 56.1681 78.0001C58.6602 78.0001 61.0246 77.2333 63.0694 75.7635L78.4696 64.9003C80.2588 63.5584 81.0896 61.258 80.5144 59.0853ZM29.3296 73.8465H23.259V56.4015H29.3296V73.8465ZM76.5526 62.2165L61.1524 73.1436C59.2353 74.4855 56.9349 74.9967 54.6345 74.6133L32.6524 70.8431V59.2131L40.5123 57.1683C42.7488 56.5932 45.1771 56.721 47.3497 57.6156L57.51 61.6414C57.7656 61.7692 58.0212 61.9609 58.149 62.2165C58.2768 62.4721 58.2768 62.7916 58.149 63.0472C57.9573 63.4945 57.4461 63.814 56.9349 63.6862L44.9854 61.7692C44.0908 61.6414 43.26 62.2165 43.1322 63.1111C43.0044 64.0057 43.5795 64.8364 44.4742 64.9642L56.4237 66.8813C56.6793 66.9452 56.871 66.9452 57.1266 66.9452C58.8519 66.9452 60.4495 65.9228 61.1524 64.2613C61.408 63.6862 61.4719 63.1111 61.4719 62.536L74.6994 58.4463C75.2745 58.2546 75.8497 58.3185 76.3609 58.638C76.8721 58.8936 77.2555 59.4048 77.3833 59.916C77.575 60.8107 77.2555 61.7053 76.5526 62.2165Z"
							fill="white" />
						<path
							d="M61.7275 54.4203C70.9292 54.4203 78.4696 46.9439 78.4696 37.7421C78.4696 28.4764 70.9292 21 61.7275 21C52.5257 21 44.9854 28.4764 44.9854 37.7421C44.9854 46.9439 52.4618 54.4203 61.7275 54.4203ZM61.7275 24.259C69.14 24.259 75.2106 30.2657 75.2106 37.7421C75.2106 45.1547 69.14 51.2253 61.7275 51.2253C54.3149 51.2253 48.2443 45.2186 48.2443 37.7421C48.2443 30.2657 54.3149 24.259 61.7275 24.259Z"
							fill="white" />
						<path
							d="M56.8709 42.9184C56.1041 43.1101 55.6568 43.813 55.8485 44.5798C56.0402 45.3466 56.7431 45.7939 57.5099 45.6022L61.0245 44.8354C61.4079 44.7715 61.8552 44.8354 62.1747 45.0271C62.8776 45.4744 63.7083 45.73 64.4751 45.73C65.0503 45.73 65.6254 45.6022 66.1366 45.4105L66.7756 45.1549C67.4785 44.8354 67.798 44.0686 67.4785 43.3657C67.159 42.6628 66.3922 42.3433 65.6893 42.6628L65.0503 42.9184C64.6029 43.1101 64.0278 43.0462 63.5805 42.7906C62.8137 42.2794 61.8552 42.0876 60.9606 42.1515C61.2162 41.193 61.2801 40.2345 61.2162 39.276H64.4112C65.1142 39.276 65.6254 38.7009 65.6254 38.0619C65.6254 37.359 65.0503 36.8477 64.4112 36.8477H60.7689C60.6411 36.4643 60.5133 36.017 60.3216 35.6336C59.6825 34.2917 59.6186 33.6527 59.6186 33.461C59.6186 32.2469 60.5772 31.3522 61.7913 31.3522C63.0054 31.3522 63.9639 32.3108 63.9639 33.5249C63.9639 34.2917 64.6029 34.8668 65.3059 34.8668C66.0727 34.8668 66.6478 34.2278 66.6478 33.5249C66.6478 30.841 64.4112 28.6045 61.7274 28.6045C58.9796 28.6045 56.807 30.7132 56.807 33.461C56.807 34.2917 57.1265 35.378 57.7655 36.7199C57.7655 36.7199 57.7655 36.7838 57.8294 36.7838H57.2543C56.5514 36.7838 56.0402 37.359 56.0402 37.998C56.0402 38.7009 56.6153 39.2121 57.2543 39.2121H58.4045C58.4684 40.4262 58.2767 41.6403 57.7655 42.7267L56.8709 42.9184Z"
							fill="white" />
						<defs>
							<linearGradient id="paint0_linear_293_6179" x1="50" y1="0" x2="50" y2="100" gradientUnits="userSpaceOnUse">
								<stop stop-color="#E05013" />
								<stop offset="1" stop-color="#FF8932" />
							</linearGradient>
						</defs>
					</svg>
					<h2 class="color-dark">Payment Within 24 hours</h2>
				</article>
				<article class="text-center">
					<svg width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="50" cy="50" r="50" fill="url(#paint0_linear_293_6187)" />
						<path
							d="M50.5 17L23.5 29V47C23.5 63.65 35.02 79.22 50.5 83C65.98 79.22 77.5 63.65 77.5 47V29L50.5 17ZM71.5 47C71.5 60.56 62.56 73.07 50.5 76.79C38.44 73.07 29.5 60.56 29.5 47V32.9L50.5 23.57L71.5 32.9V47ZM36.73 48.77L32.5 53L44.5 65L68.5 41L64.27 36.74L44.5 56.51L36.73 48.77Z"
							fill="white" />
						<defs>
							<linearGradient id="paint0_linear_293_6187" x1="50" y1="0" x2="50" y2="100" gradientUnits="userSpaceOnUse">
								<stop stop-color="#E05013" />
								<stop offset="1" stop-color="#FF8932" />
							</linearGradient>
						</defs>
					</svg>
					<h2 class="color-dark">Price Guarantee</h2>
				</article>
				<article class="text-center">
					<svg width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="50" cy="50" r="50" fill="url(#paint0_linear_293_6192)" />
						<path
							d="M81.23 50.74L54.23 23.74C53.15 22.66 51.65 22 50 22H29C25.7 22 23 24.7 23 28V49C23 50.65 23.66 52.15 24.77 53.26L51.77 80.26C52.85 81.34 54.35 82 56 82C57.65 82 59.15 81.34 60.23 80.23L81.23 59.23C82.34 58.15 83 56.65 83 55C83 53.35 82.31 51.82 81.23 50.74ZM56 76.03L29 49V28H50V27.97L77 54.97L56 76.03Z"
							fill="white" />
						<path
							d="M36.5 40C38.9853 40 41 37.9853 41 35.5C41 33.0147 38.9853 31 36.5 31C34.0147 31 32 33.0147 32 35.5C32 37.9853 34.0147 40 36.5 40Z"
							fill="white" />
						<path
							d="M43.7007 53.65C43.7007 55.36 44.3907 56.86 45.5007 58L56.0007 68.5L66.5007 58C67.6107 56.89 68.3007 55.33 68.3007 53.65C68.3007 50.26 65.5407 47.5 62.1507 47.5C60.4407 47.5 58.9107 48.19 57.8007 49.3L56.0007 51.1L54.2007 49.33C53.0907 48.19 51.5307 47.5 49.8507 47.5C46.4607 47.5 43.7007 50.26 43.7007 53.65Z"
							fill="white" />
						<defs>
							<linearGradient id="paint0_linear_293_6192" x1="50" y1="0" x2="50" y2="100" gradientUnits="userSpaceOnUse">
								<stop stop-color="#E05013" />
								<stop offset="1" stop-color="#FF8932" />
							</linearGradient>
						</defs>
					</svg>
					<h2 class="color-dark">Home pickup</h2>
				</article>
			</div>
		</section><!-- Section with four articles -->

		<!-- How it works section -->
            <section id="HowItWorks" class="page-section page-section--shaded HowItWorks gray-lite-bg">
                <div class="page-section__content page-section__content--centered">
                    <h1 class="color-black">
                    How it works
                    </h1>
                    <p class="fs-28 color-dark-light">Find out exactly how Jamjar works and how it helps you to <a class="fs-28 color-orange fw-600" href="https://www.jamjar.com/sell-my-car/" target="_blank" >sell your car.</a></p>
                    <section class="page-section__content__columns">
                        <div class="page-section__content__columns__carousel-tape">
                            <article class="m-work-box">
                               <div class="work-box radius-21">
                                    <div class="cricle-number">1</div>
                                    <h3 class="color-dark-m fw-500">Enter your registration</h3>
                                    <div class="work-img mx-auto">
                                        <img class="img-fluid"  src="/images/ux/home/work-1.png" alt="">
                                        <p class="text-white fs-14 lh-24">Enter your reg number and we'll fetch information about your car</p>
                                    </div>
                               </div>
                            </article>
                            <article class="m-work-box">
                                <div class="work-box radius-21">
                                    <div class="cricle-number">2</div>
                                    <h3 class="color-dark-m fw-500">Compare the best prices</h3>
                                    <div class="work-img mx-auto">                                    
                                        <img class="img-fluid" src="/images/ux/home/work-2.png" alt="">
                                        <p class="text-white fs-14 lh-24">We'll get prices from across the web, meaning you get the best price for your car</p>
                                    </div>
                                </div>
                            </article>
                            <article class="m-work-box">
                               <div class="work-box radius-21">
                                    <div class="cricle-number">3</div>
                                    <h3 class="color-dark-m fw-500">Improve or accept your offer</h3>
                                    <div class="work-img mx-auto">                                    
                                        <img class="img-fluid" src="/images/ux/home/work-3.png" alt="">
                                        <p class="text-white fs-14 lh-24">Either improve your offer by adding more info and photos, or accept an offer you're happy with</p>
                                    </div>
                               </div>
                            </article>
                            <article class="m-work-box">
                               <div class="work-box last-work-box radius-21">
                                    <div class="cricle-number">4</div>
                                    <h3 class="color-dark-m fw-700">Showcase Vehicle</h3>
                                    <div class="work-img mx-auto">                                    
                                        <img class="img-fluid off-hover" src="/images/ux/home/work-4.png" alt="">
                                        <img class="img-fluid on-hover" src="/images/ux/home/work-4-w.png" alt="">
                                    </div>
                               </div>
                            </article>
                        </div>
                    </section>
                </div>
            </section><!-- How it works section -->

		<section id="homeCustomerFeedback" class="page-section page-section--shaded bg-white">
			<div class="page-section__content page-section__content--centered">
				<h2 class="color-black">
					Join over a million happy customers
				</h2>
				<section class="page-section__content__columns">
					<div class="page-section__content__columns__carousel-tape">
						<article>
							<div class="rating-block mb-3">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
							</div>
							<p>Best price across all selling sites. I will definately use JamJar again and reccommended to all of my friends and family.</p>
							<p class="fw-800">Mr H • Nottingham</p>
						</article>
						<article>
							<div class="rating-block mb-3">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
							</div>
							<p>Found the company very easy to work with and no messier about and found it easy to use.</p>
							<p class="fw-800">M Kevin Phee</p>
						</article>
						<article>
							<div class="rating-block mb-3">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
							</div>
							<p>Excellent service from start to finish from Andy. I’d use these guys in a heartbeat and recommend to anybody looking to sell.</p>
							<p class="fw-800">M Mark Copper</p>
						</article>
					</div>
				</section>
				<div class="mb-4">
					<p class="fs-20">We're rated <span class="color-orange">Excellent</span> from 195 reviews</p>
				</div>                    
				<a href="https://www.reviews.co.uk/company-reviews/store/jamjar-com" target="_blank" class="main-btn">View Our Reviews</a>
				<div class="mt-4">
					<img src="/images/ux/kb_trusted_site.274ddd7b.png" alt="Trusted site" width="192">
				</div>
				
			</div>
		</section><!-- Happy customers section -->
		<section class="page-section gray-lite-bg">
			<div class="page-section__content page-section__content--centered">
				<h3 class="color-black">
					As featured in
				</h3>
				<section class="page-section__content__media-logos hp-logos-list">
					<div class="page-section__content__media-logos__carousel-tape">
						<img src="/images/ux/kb_media_logo_1.e77e5338.png" alt="The Guardian" width="154">
						<img src="/images/ux/kb_media_logo_2.d4f98b37.png" alt="BBC" width="169">
						<img src="/images/ux/kb_media_logo_3.12be4287.png" alt="Top Gear" width="224">
						<img src="/images/ux/kb_media_logo_4.c8c95f5f.png" alt="The Telegraph" width="297">
					</div>
				</section>
			</div>
		</section><!-- Media features -->
		<section class="page-section page-section--shaded">
			<div class="page-section__content page-section__content--video-hero">
				<h3 class="color-black">
					The original car buying comparison service
				</h3>
				<div class="page-section__content__video-hero paused">
					<article class="video-hero__badge">
						<p class="small text-white lh-24 fw-500">Watch the latest TV ad to see how it HowItWorks</p>
					</article>
					<video width="556" height="313" poster="/images/ux/kb_video_placeholder.ce2fac56.png" onclick="this.paused ? this.play() : this.pause();" playsinline="">
						<source src="/images/ux/kb_ad_1.06f2fe74.mp4" type="video/mp4">
						<source src="/images/ux/kb_ad_1.8abb1b5f.ogv" type="video/ogg">
					</video>
				</div>
				<article>
					<p><strong>Hundreds of UK car buyers</strong></p>
					<p class="small">
						See offers from the hundreds of expert car buying businesses that work with Jamjar.
					</p>
				</article>
				<article>
					<p><strong>Fully transparent, always</strong></p>
					<p class="small">
						Get all the details you need right here to make the best decision for your vehicle.
					</p>
				</article>
				<article>
					<p><strong>Cars, vans and scrap</strong></p>
					<p class="small">
						We&#39;ll help you find the right deal, whether it&#39;s a car, a van or destined for the scrapyard.
					</p>
				</article>
			</div>
		</section><!-- Media features -->

		<!-- About Info -->
		<section class="page-section gray-lite-bg">
			<div class="page-section__content page-section__content--centered">
				<h2 class="color-black">
					The UK's best 'sell my car comparison site
				</h2>
				<div>
					<p>We're proud to say that we've been <b class="color-black">helping millions of people value, sell or scrap their vehicles.</b> As the UK's first
					online comparison site for
					selling your car, we've been able to develop our processes over time. That's why you don't need to enter any of your
					personal information for a
					free car valuation.</p>
					<br>
					<p>All we need are a few details about the car and you'll find out how much your car is worth. you can even go on to
					selling with our help and it's
					all for free!</p>
				</div>
			</div>
		</section><!-- About Info -->

		<section id="homeQuote  " class="page-section page-section--orange">
			<div class="page-section__content page-section__content--centered">
				<form class="form__quote reg-form needs-validation" action="{{ route('apiVrmInit') }}">
					<h2 class="fs-28 fw-600">Compare free valuations from the best UK online buyers.</h2>
					<p class="small fw-500 fs-20">
						No personal data required *
					</p>                        
					<div role="group">
						<input type="text" id="vrm" name="vrm" required maxlength="8" placeholder="Enter Reg">
						<button class="go-btn-2 mx-auto" type="submit">GO <i class="fas fa-chevron-right"></i></button>
					</div>
					@if (Auth::guest())
					<p>
						or <a class="color-blue-dark fw-700" href="/login" title="SignIn"> SignIn</a> to view previews valuations
					</p>
					@endif
				</form> <!-- Form -->
			</div>
		</section><!-- Quote section -->
	</main><!-- Main page content -->
	<script>
		$(document).ready(function () {
				// same height div
				var maxHeight = 0;

				$(".work-box").each(function () {
					if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
				});

				$(".work-box").height(maxHeight);

		})
	</script>
@endsection



{{--@extends('layouts.app')

@section('content')
<section class="hero">
	<div class="hero__container">
		<div class="hero__content">
			<h1 class="hero__title">The Car Buying Comparison site.</h1>
			<div class="hero__form">
				<numberplate inline-template>
					<form action="{{ route('apiVrmInit') }}" method="GET" class="numberplate js-numberplate-form">
						<input type="text" class="numberplate__input" id="vrm" name="vrm" placeholder="ENTER REG" required maxlength="8" v-model="vrm">
						<button><i class="fa fa-angle-right"></i></button>
					</form>
				</numberplate>
				<p class="hero__text">Get valuations for your vehicle in just <b>60 seconds</b></p>
			</div>
		</div>
		<img class="hero__image" src="{{ secure_asset('images/car.png') }}" alt="Car">
	</div>
	--}}{{--<div class="hero__footer">--}}{{--
        --}}{{--@if (session()->has('history'))--}}{{--
            --}}{{--<p>Previous Searches</p>--}}{{--
            --}}{{--@foreach (session()->get('history') as $plate)--}}{{--
                --}}{{--<span class="mini-plate js-fill-form" data-plate="{{$plate}}">--}}{{--
                    --}}{{--{{$plate}}--}}{{--
                --}}{{--</span>--}}{{--
            --}}{{--@endforeach--}}{{--
            --}}{{--@push('scripts-after')--}}{{--
            --}}{{--<script>--}}{{--
                --}}{{--$('.js-fill-form').on('click', function(e) {--}}{{--
                    --}}{{--var plate = $(this).attr('data-plate');--}}{{--
                    --}}{{--$('.numberplate__input').val(plate);--}}{{--
                --}}{{--});--}}{{--
            --}}{{--</script>--}}{{--
            --}}{{--@endpush--}}{{--
        --}}{{--@else--}}{{--
		--}}{{--<p>or</p>--}}{{--
		--}}{{--<p><a href="{{ route('login') }}">Login</a> to view your previous valuations</p>--}}{{--
        --}}{{--@endif--}}{{--
	--}}{{--</div>--}}{{--
</section>

<section class="fp-how-it-works">
	<div class="fp-how-it-works__container">
		<h2 class="fp-how-it-works__title"><span>How</span> it Works</h2>
		<div class="fp-how-it-works__col">
			<img src="{{ secure_asset('images/icon-free-reg.png') }}" alt="Free Valuation Comparison">
			<span>Enter your vehicle’s registration number</span>
		</div>
		<div class="fp-how-it-works__col">
			<img src="{{ secure_asset('images/icon-magnifying-glass.png') }}" alt="Choose the best price">
			<span>Compare values from the best online buyers in the UK for your vehicle.</span>
		</div>
		<div class="fp-how-it-works__col">
			<img src="{{ secure_asset('images/icon-calendar.png') }}" alt="Arrange collection from approved dealers">
			<span>Simply click and choose the best deal and arrange for collection and payment.</span>
		</div>
		<div class="fp-how-it-works__footer">
			<a href="{{ url('/info/how-it-works') }}" class="button button--orange">how it works</a>
		</div>
	</div>
</section>

<section class="about-jamjar">
	<div class="about-jamjar__container">
		<h2 class="about-jamjar__title">About Jamjar.com</h2>
		<div class="about-jamjar__content">
            <p>We have been helping Customers find the right deal for their vehicles since 1997 and have served over 1 million happy customers.</p>
            <p>We’re in business to help you realise the best price for your car with the minimum fuss.</p>
		</div>
		<div class="about-jamjar__footer">
			<a href="{{ url('/info/about') }}" class="button button--orange">about us</a>
		</div>
	</div>
</section>

<section class="calltoaction">
	<div class="calltoaction__container">
		<div class="calltoaction__col">
		<h2 class="calltoaction__title">Get the best price for your car</h2>
		<strong class="calltoaction__subtitle">Enter your registration for a FREE online comparison</strong>
		</div>
		<div class="calltoaction__col calltoaction__col--numberplate">
			<form class="numberplate numberplate--mini" action="{{ route('/') }}" method="POST">
				{{ csrf_field() }}
				<input type="text" class="numberplate__input" name="vrm" id="vrm" placeholder="Enter Reg"  maxlength="8" required>
				<button><i class="fa fa-angle-right"></i></button>
			</form>
		</div>
	</div>
</section>
@endsection--}}
