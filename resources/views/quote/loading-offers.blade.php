@push('scripts-after')
	<script type="text/javascript">
		$(function() {
			var $externalSources = $('.offers-source-external');
			var externalSourcesCount = $externalSources.length;
			var requestsCounter = 0;
			var $offersContainer = $('.offers-container tbody');

			var getAjaxCallData = function($form) {
				return {
					'type': $form.attr('method'),
					'url': $form.attr('action'),
					'data': $form.serializeArray(),
					'success': function(data) {
						console.log(data);
						console.log($form.serializeArray());
						for (var viewIndex in data.views) {
							var view = data.views[viewIndex];
							$offersContainer.prepend(view);
							$(".box__content").show();
							$('.offers-count').text(parseInt($('.offers-count').text()) + 1);
							$('#valuation-word').text('valuations');
						}
						var $offers = $('.valuation-draft-holder');
						$offers.sort(function(elementA, elementB) {
							var priceA = parseInt($(elementA).attr('data-price'));
							var priceB = parseInt($(elementB).attr('data-price'));

							return priceB - priceA;
						}).appendTo($offersContainer);
					}
				};
			};

			$externalSources.each(function() {
				var $form = $(this);
				var ajaxData = getAjaxCallData($form);
				ajaxData.complete = function() {
					requestsCounter++;
					if (requestsCounter >= externalSourcesCount) {
						var $internalSources = $('.offers-source-internal');
						$internalSources.each(function() {
							var $internalSourceForm = $(this);
							var internalAjaxData = getAjaxCallData($internalSourceForm);
							internalAjaxData.complete = function() {
								$('.offers-loading-box').hide();
								if (parseInt($('.offers-count').text()) == 0) {
									$(".offers-container").hide();
									$('.partner-add-to-marketplace').fadeIn();
								}
								$('.offers-continue-button').text('CONTINUE');
								const td = $(".offers-container").find('tbody tr:first').find('td:nth-child(5)');
								let html = td.html();
								html = html.replace('Accept this offer now', 'Accept the best current instant offer now');
								td.html(html);

							};
							$.ajax(internalAjaxData)
						});
					}
				};
				$.ajax(ajaxData);
			});
		});
	</script>
@endpush

@if (count($offeredValues) == 0)
	<form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
		{{ csrf_field() }}
		<input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_CTB }}">
		<input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
	</form>

	<form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
		{{ csrf_field() }}
		<input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_WWAC }}">
		<input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
	</form>

	<form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
		{{ csrf_field() }}
		<input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_M4YM }}">
		<input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
	</form>

	<form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
		{{ csrf_field() }}
		<input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_WBCT }}">
		<input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
	</form>

	<form method="post" class="offers-source-external" action="{{ route('saveValuationDraftFromSource') }}">
		{{ csrf_field() }}
		<input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_MW }}">
		<input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
	</form>

	<form method="post" class="offers-source-internal" action="{{ route('saveValuationDraftFromSource') }}">
		{{ csrf_field() }}
		<input type="hidden" name="source-id" value="{{ \JamJar\Http\Controllers\OffersProviderController::OFFERS_SOURCE_INTERNAL }}">
		<input type="hidden" name="vehicle-id" value="{{ $vehicle->getId() }}">
	</form>
@endif