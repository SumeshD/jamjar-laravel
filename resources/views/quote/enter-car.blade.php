@extends('layouts.app')

@section('content')
<section class="jamjar__container jamjar__container--small">
	<div class="box">
		<h1 class="box__title box__title--centered">Sorry, we couldn't find your car</h1>

		<div class="box__content">
			<p class="u-text-red u-text-center">Please try entering your numberplate again</p>
			<br>
			<form action="{{ route('apiVrmInit') }}" method="GET" class="numberplate numberplate--full js-numberplate-form">
				<input type="text" class="numberplate__input" id="vrm" name="vrm" placeholder="Enter Reg" value="{{$vrm}}" required maxlength="8">
				<button><i class="fa fa-angle-right"></i></button>
			</form>
			<br>
		</div>
		{{--<div class="divider"><span>or</span></div>--}}
		{{--<h1 class="box__title box__title--centered box__title--no-border">--}}
			{{--Enter your car details manually--}}
		{{--</h1>--}}

        {{--<enter-car></enter-car>--}}

        <div class="clear"></div>
	</div>
</section>
@stop
