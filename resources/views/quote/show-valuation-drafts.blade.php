<?php /** @var JamJar\Vehicle $vehicle */
    $prioritiseShowcase = (bool) \JamJar\Setting::get('valuation_page_prioritise_showcase');
?>

@extends('newlayouts.app')

@section('content')
    <main role="main" xmlns="http://www.w3.org/1999/html">
        <section class="page-section page-section--shaded">
            <div class="page-section__content page-section__content--valuation-progress">
                <table>
                    <tr>
                        <td>
                            <img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}" width="120">
                        </td>
                        <td class="padding-left--20">
                            <h3>{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</h3>
                            <p>{{ $vehicle->regNumber  }}</p>
                            <a href="{{ route('/')}}" title="Change vehicle"><small>Change vehicle</small></a> <span>•</span> <a href="{{ route('showVehicleInformation', $vehicle) }}" title="Edit details"><small>Edit details</small></a> <span>•</span> <a href="#" hidden id="show-details-button" title="Show details"><small>Show</small></a><a href="#" id="hide-details-button" title="Hide details"><small>Hide</small></a>
                        </td>
                    </tr>
                </table>
                <table style="margin-left:50px; margin-top: 20px; font-size: 10pt" class="car-details">
                    <tr>
                        <td>Derivative:</td>
                        <td>{{$vehicle->meta->derivative}}</td>
                    </tr>
                    <tr>
                        <td>Plate Year:</td>
                        <td>{{$vehicle->meta->plate_year}}</td>
                    </tr>
                    <tr>
                        <td>Registration:</td>
                        <td>{{$vehicle->numberplate}}</td>
                    </tr>
                    <tr>
                        <td>Mileage:</td>
                        <td>{{$vehicle->meta->mileage}}</td>
                    </tr>

                </table>
                <table style="margin-left:50px; margin-top: 20px; font-size: 10pt" class="car-details">
                    <tr>
                        <td>Service History:</td>
                        <td>{{$vehicle->meta->service_history}}</td>
                    </tr>
                    <tr>
                        <td>MOT:</td>
                        <td>{{$vehicle->meta->mot}}</td>
                    </tr>
                    <tr>
                        <td>Colour:</td>
                        <td>{{$vehicle->meta->colour->title}}</td>
                    </tr>
                    <tr>
                        <td>Write Off:</td>
                        <td>{{ trans('vehicles.WriteOff' . $vehicle->meta->write_off) }}</td>
                    </tr>
                    <tr>
                        <td>Non Runner:</td>
                        <td>{{ trans('vehicles.NonRunner' . $vehicle->meta->non_runner) }}</td>
                    </tr>

                </table>
                <div class="progress-wrapper margin-right--15">
                    <div class="progress-wrapper__progress-point progress-wrapper__progress-point--passed">
                        <span>Vehicle</span>
                        1
                    </div>
                    <div class="progress-wrapper__progress-waypoint"></div>
                    <div class="progress-wrapper__progress-point progress-wrapper__progress-point--active">
                        <span>Valuations</span>
                        2
                    </div>
                    <div class="progress-wrapper__progress-waypoint"></div>
                    <div class="progress-wrapper__progress-point">
                        <span></span>
                        3
                    </div>
                    <div class="progress-wrapper__progress-waypoint"></div>
                    <div class="progress-wrapper__progress-point">
                        <span></span>
                        4
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section">
            <div class="page-section__content page-section__content--valuation-step2 page-section__content--centered">
                <div class="box__content" style="{{ count($offeredValues) == 0 ? 'display:none;' : '' }}">
                    <h3 style="margin-bottom: 10px;">
                        We have found you <span class="offers-count" >{{count($offeredValues)}}</span> <span id="valuation-word"> automated @if(count($offeredValues) > 1)valuations @else valuation @endif</span>
                        <br>
                        <p style="font-size: 1.7rem; font-weight: normal;">
                            but you may get more...
                        </p>
                    </h3>
                    @if($prioritiseShowcase)
                        <p style="font-size: 1.5rem;">
                            <a href="#" id="confirm-best-showcase" style="display: block; padding: 18px 28px 18px 12px; background-color: #FFFFFF; border: 2px solid #F47D0F; border-radius: 5px; font-size: 1.6rem; font-weight: 500; line-height: 1.5em; margin-bottom: 5px; padding: 12px; color: #000000; text-decoration: none;">You can showcase your vehicle for free in our virtual showroom to maximise and add value to your vehicle sale.</a>
                            {{--                        <strong id="confirm-best-showcase" style="cursor: pointer"> You can showcase your vehicle for free in our virtual showroom to maximise and add value to your vehicle sale. </strong>--}}
                        </p>
                        <p class="small">
                            We recommend you add images, highlight its full specification and service history to another 912 professional UK buyers currently interested in your vehicle.
                        </p>
                        <br>
                        <p>
                            <strong style="cursor: pointer">Or</strong>
                        </p>
                        <br>
                        <p style="font-size: 1.5rem;">
                            <a href="#" id="confirm-best" style="display: block; padding: 18px 28px 18px 12px; background-color: #FFFFFF; border: 2px solid #F47D0F; border-radius: 5px; font-size: 1.6rem; font-weight: 500; line-height: 1.5em; margin-bottom: 12px; margin-top: 5px; padding: 12px; color: #000000; text-decoration: none;">Accept the best current instant offer now</a>
                        </p>
                    @else
                        <p style="font-size: 1.5rem;">
                            <a href="#" id="confirm-best" style="display: block; padding: 18px 28px 18px 12px; background-color: #F47D0F; border: 2px solid #F47D0F; border-radius: 5px; font-size: 1.6rem; font-weight: 500; line-height: 1.5em; margin-bottom: 12px; margin-top: 5px; padding: 12px; color: #FFFFFF; text-decoration: none;">Accept the best current instant offer now</a>
                        </p>
                        <br>
                        <p>
                            <strong style="cursor: pointer">Or</strong>
                        </p>
                        <br>
                        <p style="font-size: 1.5rem;">
                            <a href="#" id="confirm-best-showcase" style="display: block; padding: 18px 28px 18px 12px; background-color: #FFFFFF; border: 2px solid #F47D0F; border-radius: 5px; font-size: 1.6rem; font-weight: 500; line-height: 1.5em; margin-bottom: 5px; padding: 12px; color: #000000; text-decoration: none;">You can showcase your vehicle for free in our virtual showroom to maximise and add value to your vehicle sale.</a>
                            {{--                        <strong id="confirm-best-showcase" style="cursor: pointer"> You can showcase your vehicle for free in our virtual showroom to maximise and add value to your vehicle sale. </strong>--}}
                        </p>
                        <p class="small">
                            We recommend you add images, highlight its full specification and service history to another 912 professional UK buyers currently interested in your vehicle.
                        </p>
                    @endif
                    <br>
                    <br>
                </div>
                <?php /** @var \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface[] $offeredValues */ ?>
                <table style="margin: 0 auto 173px" class="offers-container">
                    <thead>
                    <tr>
                        <th style="text-align: center">Dealer</th>
                        <th style="text-align: center" colspan="2">Details</th>
                        <th style="text-align: center" class="padding-right--20">Valuation</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($offeredValues) > 0)
                        @php
                            $firstOffer = true;
                        @endphp
                        @foreach ($offeredValues as $offeredValue)
                            @include('quote.single-offered-value-table-row', ['offeredValue' => $offeredValue, 'vehicle' => $vehicle, 'firstOffer' => $firstOffer, 'prioritiseShowcase' => $prioritiseShowcase])
                            @php
                                $firstOffer = false;
                            @endphp
                        @endforeach
                    @endif
                    @if (count($offeredValues) == 0)
                        <tr class="valuation-draft-holder offers-loading-box" data-price="-1">
                            <td colspan="5">
                                <div class="loader">
                                    Loading offers...
                                </div>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @include('quote.loading-offers')
                <div style="height: 320px; padding: 60px 40px; font-size: 18px; display:none;" class="partner-add-to-marketplace">
                    Our partners haven't provided any offers for your vehicle based on the information provided so far, however with some more information we could get you some offers via our Marketplace.
                    <br><br>
                    We recommend you add images, highlight its full specification and service history to another 912 professional UK buyers currently interested in your vehicle.
                    <div style="margin-top: 20px; text-align: center">
                        <a style="font-size: 20px;" href="{{ route('showVehicleDetails', [$vehicle]) }}" class="button button--orange-bg button--small">Add to Jamjar marketplace</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section mob-hide">
            <div class="page-section__content page-section__content--centered">
                <h3>
                    As featured in
                </h3>
                <section class="page-section__content__media-logos">
                    <div class="page-section__content__media-logos__carousel-tape">
                        <img src="/images/ux/kb_media_logo_1.e77e5338.png" alt="The Guardian" width="154">
                        <img src="/images/ux/kb_media_logo_2.d4f98b37.png" alt="BBC" width="169">
                        <img src="/images/ux/kb_media_logo_3.12be4287.png" alt="Top Gear" width="224">
                        <img src="/images/ux/kb_media_logo_4.c8c95f5f.png" alt="The Telegraph" width="297">
                    </div>
                </section>
                <h3>
                    Sell any car fast
                </h3>
                <section class="page-section__content__auto-logos">
                    <div class="page-section__content__auto-logos__carousel-tape">
                        <img src="/images/ux/kb_auto_logo_1.e7ab6737.png" alt="BMW" width="120">
                        <img src="/images/ux/kb_auto_logo_2.c1b4defc.png" alt="Audi" width="120">
                        <img src="/images/ux/kb_auto_logo_3.0cb5b59c.png" alt="Mercedes" width="120">
                        <img src="/images/ux/kb_auto_logo_4.21c602f3.png" alt="Ford" width="120">
                        <img src="/images/ux/kb_auto_logo_5.f20e53f2.png" alt="Volkswagen" width="120">
                    </div>
                </section>
            </div>
        </section><!-- Media features -->
        <section class="page-section page-section--shaded mob-hide">
            <div class="page-section__content page-section__content--video-hero">
                <h3>
                    The original car buying comparison service
                </h3>
                <div class="page-section__content__video-hero paused">
                    <article class="video-hero__badge">
                        <p><strong>Watch the latest TV ad to see how it works</strong></p>
                    </article>
                    <video width="556" height="313" poster="/images/ux/kb_video_placeholder.ce2fac56.png" onclick="this.paused ? this.play() : this.pause();" playsinline="">
                        <source src="/images/ux/kb_ad_1.06f2fe74.mp4" type="video/mp4">
                        <source src="/images/ux/kb_ad_1.8abb1b5f.ogv" type="video/ogg">
                    </video>
                </div>
                <article>
                    <p><strong>Hundreds of UK car buyers</strong></p>
                    <p class="small">
                        See offers from the hundreds of expert car buying businesses that work with Jamjar.
                    </p>
                </article>
                <article>
                    <p><strong>Fully transparent, always</strong></p>
                    <p class="small">
                        Get all the details you need right here to make the best decision for your vehicle.
                    </p>
                </article>
                <article>
                    <p><strong>Cars, vans and scrap</strong></p>
                    <p class="small">
                        We&#39;ll help you find the right deal, whether it&#39;s a car, a van or destined for the scrapyard.
                    </p>
                </article>
                <article>
                    <a href="#" title="More about Jamjar">More about Jamjar</a>
                </article>
            </div>
        </section><!-- Media features -->
    </main><!-- Main page content -->
    @push('scripts-after')
        <script>
            window.addEventListener( "pageshow", function ( event ) {
                var historyTraversal = event.persisted ||
                    ( typeof window.performance != "undefined" &&
                        window.performance.navigation.type === 2 );
                if ( historyTraversal ) {
                    // Handle page restore.
                    window.location.reload();
                }
            });
            $("#show-details-button").on("click", function(event) {
                $(".car-details").show();
                $(".progress-wrapper").addClass("margin-right--15");
                $("#show-details-button").hide();
                $("#hide-details-button").show();
            });

            $("#hide-details-button").on("click", function(event) {
                $(".car-details").hide();
                $(".progress-wrapper").removeClass("margin-right--15");
                $("#hide-details-button").hide();
                $("#show-details-button").show();
            });

            $("#confirm-best").on("click", function() {
                const href = $(".offers-container tr.valuation-draft-holder:nth-of-type(1) td:nth-of-type(4) a:nth-of-type(2)").attr('href');
                console.log(href);
                if(href) {
                    window.location = href;
                }
            });

            $("#confirm-best-showcase").on("click", function() {
                const href = $(".offers-container tr.valuation-draft-holder:nth-of-type(1) td:nth-of-type(4) a:nth-of-type(2)").attr('href');
                if(href) {
                    window.location = href + '/showcase';
                }
            })
        </script>
    @endpush
@endsection
