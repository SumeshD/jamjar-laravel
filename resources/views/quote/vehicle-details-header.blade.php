<div class="valuations__vehicle-details" style="width: 100%; @if(!auth()->user()) display: none @else @if(!auth()->user()->isPartner()) display: none; @endif @endif ">
    <ul>
        <li>
            <span>Derivative:</span> <span>{{$vehicle->meta->derivative}}</span>
        </li>
        <li>
            <span>Plate Year:</span> <span>{{$vehicle->meta->plate_year}}</span>
        </li>
        <li>
            <span>Registration:</span> <span>{{$vehicle->numberplate}}</span>
        </li>
        <li>
            <span>Mileage:</span> <span>{{$vehicle->meta->mileage}}</span>
        </li>
    </ul>
    <ul>
        <li>
            <span>Service History:</span> <span>{{$vehicle->meta->service_history}}</span>
        </li>
        <li>
            <span>MOT:</span> <span>{{$vehicle->meta->mot}}</span>
        </li>
        <li>
            <span>Colour:</span> <span>{{$vehicle->meta->colour->title}}</span>
        </li>
        <li>
            <span>Write Off:</span> <span>{{ trans('vehicles.WriteOff' . $vehicle->meta->write_off) }}</span>
        </li>
        <li>
            <span>Non Runner:</span> <span>{{ trans('vehicles.NonRunner' . $vehicle->meta->non_runner) }}</span>
        </li>
    </ul>
    <div class="valuations__buttons desktop-hide">
        <a href="{{ route('showVehicleInformation', $vehicle) }}" class="edit-vehicle-button button button--grey-bg" >
            Edit Details
        </a>
    </div>
    <div class="divider"></div>
    @include('matrix.vehicles.cap-values')
</div>

@push('scripts-after')
    <script>
        if (window.matchMedia('(max-width: 600px)').matches) {
            //$('.js-show-vehicle-details').html('Show Details <i class="fa fa-chevron-down"></i>');
            //$('.js-show-vehicle-details').toggleClass('active');
        }
        $('.js-show-vehicle-details').on('click', function(e) {
            e.preventDefault();
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $('.valuations__vehicle-details').slideDown();
                $(this).html('Hide Details <i class="fa fa-chevron-up"></i>');
                $(this).stop();
            } else {
                $('.valuations__vehicle-details').slideUp();
                $(this).html('Show Details <i class="fa fa-chevron-down"></i>');
                $(this).stop();
            }
        });
    </script>
@endpush