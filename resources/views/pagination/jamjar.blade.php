<div class="paginator">
    <div class="paginator__container">
@if ($paginator->hasPages())
        <ul class="paginator__list">
            {{-- Previous Page Link --}}
            @if (!$paginator->onFirstPage())
                <li class="paginator__item">
                    <a href="{{ $paginator->previousPageUrl() }}" class="paginator__link paginator__link--next" rel="prev">
                        Previous
                    </a>
                </li>
            @endif
            {{-- End Previous Page Link --}}

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="paginator__item disabled">
                    <span class="paginator__link">{{ $element }}</span>
                </li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="paginator__item">
                            <a class="paginator__link paginator__link--active">{{ $page }}</a>
                        </li>
                    @else
                        <li class="paginator__item">
                            <a href="{{ $url }}" class="paginator__link">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

       {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="paginator__item">
                <a href="{{ $paginator->nextPageUrl() }}" class="paginator__link paginator__link--next" rel="prev">
                    Next
                </a>
            </li>
        @endif
        {{-- End Next Page Link --}}
    </ul>
@endif
