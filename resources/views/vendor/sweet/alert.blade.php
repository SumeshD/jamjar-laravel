@if (Session::has('sweet_alert.alert'))
    <?php
        $swalSettingText = Session::pull('sweet_alert.alert');
        try {
            $swalSetting =  json_decode($swalSettingText, true);
            $swalSetting['icon'] = $swalSetting['type'];
            $swalSettingText = json_encode($swalSetting);
        } catch (\Exception $e) {
        }
    ?>
    <script>
        $(function() {
            swal({!! $swalSettingText !!});
        });
    </script>
@endif