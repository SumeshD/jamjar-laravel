@extends(config('mail-tracker.admin-template.name'))

@section('content_header')
	<div class="row pull-right">
        <div class="col-sm-12">
            <form action="{{ route('mailTracker_Search') }}" method="post" class="form-inline">
                {!! csrf_field() !!}
                <div class="form-group">
                    <input type="text" name="search" class="form-control" id="search" placeholder="Search" value="{{ session('mail-tracker-index-search') }}">
                </div>
                <button type="submit" class="btn btn-default">
                    Search
                </button>
            </form>
            <hr>
        </div>
    </div>
    <h1>Email Tracker<small>Here you can track every email sent out by the application</small></h1>
@endsection

@section(config('mail-tracker.admin-template.section'))        
<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Sent Email</h3>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <tr>
                    <th>SMTP</th>
                    <th>Recipient</th>
                    <th>Subject</th>
                    <th>Opens</th>
                    <th>Clicks</th>
                    <th>Sent At</th>
                    <th>View Email</th>
                    <th>Clicks</th>
                </tr>
            @foreach($emails as $email)
                <tr class="{{ $email->report_class }}">
                  <td>
                    <a href="{{route('mailTracker_SmtpDetail',$email->id)}}" target="_blank">
                      {{ str_limit($email->smtp_info, 20) }}
                    </a>
                  </td>
                  <td>{{$email->recipient}}</td>
                  <td>{{$email->subject}}</td>
                  <td>{{$email->opens}}</td>
                  <td>{{$email->clicks}}</td>
                  <td>{{$email->created_at->format(config('mail-tracker.date-format'))}}</td>
                  <td>
                      <a href="{{route('mailTracker_ShowEmail',$email->id)}}" target="_blank">
                        View
                      </a>
                  </td>
                  <td>
                      @if($email->clicks > 0)
                          <a href="{{route('mailTracker_UrlDetail',$email->id)}}">Url Report</a>
                      @else
                          No Clicks
                      @endif
                  </td>
                </tr>
            @endforeach
            </table>
		</div>
	</div>
</div>

<div class="col-sm-12 text-center">
    {!! $emails->render() !!}
</div>
@endsection
