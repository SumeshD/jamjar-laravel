@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'My Invoices'])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'credits'])
        </div>
        <div class="dashboard__col">

            <div class="box box--with-header box--no-margin">
                <div class="box__header box__header_dashboard">My Invoices</div>
                <div class="box__container">
                    <div class="box__content">
                        <table class="valuations__table jj_responsive_table invoices_table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Invoice Number</th>
                                    <th>Amount (Net)</th>
                                    <th>VAT</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @unless ($orders->isEmpty())
                                @foreach ($orders as $order)
                                <tr>
                                    <?php $price = $order->getOriginal('product_price') / 100; ?>
                                    <?php $netPrice = round($price / 1.20, 2) ?>
                                    <td>{{ $order->created_at->format('d/m/y H:i') }}</td>
                                    <td>{{$order->pp_payment_id}}</td>
                                    <td>&pound;{{ number_format($netPrice, 2) }}</td>
                                    <td>&pound;{{ number_format($price - $netPrice, 2) }}</td>
                                    <td>&pound;{{ $order->product_price }}</td>
                                    <td class="u-text-right"><a class="button button--orange-bg" href="{{ route('partnerInvoice', $order->pp_payment_id) }}">view invoice</a></td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">You haven't added funds to your account yet.</td>
                                    <td colspan="1" class="u-text-right">
                                        <a class="button button--orange-bg" data-toggle="buyCredits">
                                            Add Funds Now
                                        </a>
                                    </td>
                                </tr>
                            @endunless
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


@push('scripts-after')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('.valuations__table').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     false,
        "lengthChange": false,
        "pageLength": 100,
        "order": [[ 0, "desc" ]],
        "oLanguage": {
          "sSearch": "",
          "sSearchPlaceholder" : "Search"
        }
    });
} );
</script>
@endpush
@endsection
