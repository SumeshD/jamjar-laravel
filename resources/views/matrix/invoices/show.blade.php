<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
                window.Laravel = <?php echo json_encode([
                    'csrfToken' => csrf_token(),
                ]); ?>
        </script>

        <title>View Invoice: {{ $order->pp_payment_id }}</title>

        <!-- Styles -->
        <link href="{{ secure_asset('css/vendor.css') }}" rel="stylesheet">
        <link href="{{ secure_asset('css/style.css') }}" rel="stylesheet">
    </head>

    <body>
        <section class="jamjar__container--small">
            <div class="box box--narrow box--with-header">
                <div class="box__header">View Invoice: {{ $order->pp_payment_id }}</div>
                <div class="box__container">
                    <div class="invoice">
                        <div class="invoice__col">
                            <a href="javascript:window.print()" class="button button--orange-bg">Print</a>

                            <div class="invoice__address">
                                <p><strong>FROM</strong></p>
                                <p><strong class="u-text-muted">JAMJAR.COM</strong></p>
                                <p><strong class="u-text-muted">{{ \JamJar\Setting::get('jamjar_address_line_one') ?? 'Address' }}</strong></p>
                                <p><strong class="u-text-muted">{{ \JamJar\Setting::get('jamjar_address_line_two') ?? 'Address' }}</strong></p>
                                <p><strong class="u-text-muted">{{ \JamJar\Setting::get('jamjar_address_town') ?? 'Address' }}</strong></p>
                                <p><strong class="u-text-muted">{{ \JamJar\Setting::get('jamjar_address_city') ?? 'Address' }}</strong></p>
                                <p><strong class="u-text-muted">{{ \JamJar\Setting::get('jamjar_address_county') ?? 'Address' }}</strong></p>
                                <p><strong class="u-text-muted">{{ \JamJar\Setting::get('jamjar_address_postcode') ?? 'Address' }}</strong></p>
                            </div>

                            <div class="invoice__address">
                                <p><strong>TO</strong></p>
                                @if ($order->user->company)
                                    <p><strong class="u-text-muted">{{ $order->user->company->name }} c/o {{ $order->user->name }}</strong></p>
                                    <p><strong class="u-text-muted">{{ $order->user->company->address_line_one }}</strong></p>
                                    <p><strong class="u-text-muted">{{ $order->user->company->address_line_two }}</strong></p>
                                    <p><strong class="u-text-muted">{{ $order->user->company->city }}</strong></p>
                                    <p><strong class="u-text-muted">{{ $order->user->company->county }}</strong></p>
                                    <p><strong class="u-text-muted">{{ $order->user->company->postcode }}</strong></p>
                                @else 
                                    <p><strong class="u-text-muted">{{ $order->user->name }}</strong></p>
                                @endif
                            </div>
                        </div>
                        <div class="invoice__col">
                            <h1 class="invoice__title">INVOICE</h1>
                            <div class="invoice__details">
                                <p><strong>INVOICE REFERENCE</strong> <strong class="u-text-muted">{{ $order->pp_payment_id }}</strong></p>
                                <p><strong>INVOICE DATE</strong> <strong class="u-text-muted">{{ $order->created_at->format('d/m/Y') }}</strong></p>
                                <p><strong>STATUS</strong> <strong class="u-text-green">RECEIVED</strong></p>
                            </div>
                        </div>
                        <div class="invoice__table">
                            <table>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Amount (Net)</th>
                                    <th>VAT</th>
                                    <th>Total</th>
                                </tr>
                                <tr>
                                    <?php $price = $order->getOriginal('product_price') / 100; ?>
                                    <?php $netPrice = round($price / 1.20, 2) ?>
                                    <td>Jamjar partner top-up</td>
                                    <td>&pound;{{ number_format($netPrice, 2) }}</td>
                                    <td>&pound;{{ number_format($price - $netPrice, 2) }}</td>
                                    <td>&pound;{{ $order->product_price }}</td>
                                </tr>
                            </table>
                        </div>

                        <div class="invoice__footer">
                            <img src="{{ secure_asset('images/logo.png') }}" alt="jamjar.com">
                        </div>

                        <div class="invoice__footer">
                            <p><strong class="u-text-muted">VAT No: 981893464</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
