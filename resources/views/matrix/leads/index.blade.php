@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Marketplace'])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'leads'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header  box--no-margin">
                <div class="box__header header-theme-bidding">Find Vehicles</div>
                <div class="box__container">
                    <div class="box__content box__with_filtering">
                        <form action="" method="GET" style="margin-bottom: 2rem;" class="form" id="filters-form">

                                @include('matrix.leads.filters-container')

                        </form>
                        <table class="valuations__table jj_responsive_table good_news_leads_table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Car</th>
                                    <th>Area</th>
                                    <th>Owners</th>
                                    <th>Write-Off</th>
                                    <th>Non-Runner</th>
                                    <th>Year/Reg</th>
                                    <th>Mileage</th>
                                    <th>Top Value</th>
                                    <th>Fee (+VAT)</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($leads) > 0)
                                    @foreach ($leads as $lead)
                                        <?php /** @var \JamJar\Vehicle $vehicle */ ?>
                                        @php ($vehicle = $lead->vehicle)
                                        @php ($valuation = $lead->highestValuation)
                                        @php ($expiredValuation = $lead->highestExpiredValuation)
                                        <tr id="{{$valuation ? $valuation->id : $vehicle->id}}">
                                            <td>
                                                {{ $valuation ? $valuation->created_at->format('d/m/y') : $vehicle->added_to_marketplace_at->format('d/m/y') }}<br />
                                                <div class="additional-info-icons">
                                                    <img src="/images/icons/vehicle-additional-info/jj.png" @if (($vehicle->owner->user) && $vehicle->owner->user->matrix_partner != 1) class="inactive" @else class="tooltip-subject" title="Vehicle offered by a Jamjar associate" @endif>
                                                    <img src="/images/icons/vehicle-additional-info/photo-camera.png" @if (count($vehicle->getImages()) == 0) class="inactive" @else class="tooltip-subject" title="The seller has added vehicle photos" @endif>
                                                    <img src="/images/icons/vehicle-additional-info/great-britain-pound.png" @if ($vehicle->getAskingPriceInPence() == null) class="inactive" @else class="tooltip-subject" title="This vehicle has a set asking price" @endif>
                                                    <img src="/images/icons/vehicle-additional-info/info.png" @if (!$vehicle->getServiceHistoryInformation() and !$vehicle->getAdditionalSpecificationInformation()) class="inactive" @else class="tooltip-subject" title="The seller has added additional service history or specification details" @endif>
                                                </div>
                                            </td>
                                            <td style="max-width: 200px;">
                                                {{ $vehicle->meta->formatted_name }}
                                            </td>
                                            <td>
                                                @if ($vehicle->owner)
                                                {{ strtoupper($vehicle->owner->getAreaByPostcode()) }}
                                                @endif
                                            </td>
                                            <td>
                                                {{trans('vehicles.owners_count_short.owners_'.($vehicle->meta->number_of_owners <= 5 ? $vehicle->meta->number_of_owners : 6)) }}
                                            </td>
                                            <td>
                                                {{ $vehicle->meta->write_off ? 'Yes' : 'No' }}
                                            </td>
                                            <td>
                                                {{ $vehicle->meta->non_runner ? 'Yes' : 'No' }}
                                            </td>
                                            <td>
                                                {{ str_replace_first(' ', '/', $vehicle->meta->plate_year) }}
                                            </td>
                                            <td>
                                                {{ $vehicle->meta->mileage }}
                                            </td>

                                            <?php $highestValue = $vehicle->getHighestValue(); ?>
                                            @if ($highestValue)
                                                <td class="{{ ($expiredValuation and $valuation->id == $expiredValuation->id) ? 'expired-valuation' : '' }}">
                                                    &pound;{{ number_format($highestValue) }}
                                                </td>
                                            @else
                                                <td>-</td>
                                            @endif
                                            <td>
                                                @if (auth()->user()->profile->use_flat_fee)
                                                    <strong>&pound;{{ number_format(auth()->user()->profile->flat_fee+auth()->user()->profile->flat_fee*(env('VAT_FEE_IN_PERCENT')/100), 2) }}</strong>
                                                @else
                                                    <strong>&pound;{{ number_format($lead->fee, 2) }}</strong>
                                                @endif
                                            </td>
                                            <td class="u-text-right">
                                                @if ($valuation)
                                                    <a href="{{ route('partnerShowLead', $valuation) }}" class="button button--green button--small">view lead</a>
                                                @else
                                                    <a href="{{ route('partnerAddNewLeadForm', $vehicle->id) }}" class="button button--green button--small">add valuation</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <td colspan="11" style="text-align: center;">No results</td>
                                @endif
                            </tbody>
                        </table>
                        <div class="pagination">
                            <?php
                                $paginator = new \Illuminate\Pagination\LengthAwarePaginator($leads, $resultsCount, $perPage, $page);
                                $paginator = $paginator->withPath(request()->fullUrlWithQuery([]));
                            ?>

                            {{ $paginator->links('vendor.pagination.matrix-default') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('scripts-after')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endpush
@endsection
