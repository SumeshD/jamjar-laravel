@if ($errors->any())
    <div class="box__content">
        <div class="alert alert-danger col-12">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    </div>
@endif
<div class="box__content">
    <div class="@if ($highestValuation and $company->id === $highestValuation->company_id) col-4 @else col-3 @endif">
        <strong>Status</strong>
        @if ($highestValuation and $company->id === $highestValuation->company_id)
            <h3 class="u-text-green" style="font-size: 2.5rem;">Paid</h3>
        @else
            <h3 class="u-text-muted" style="margin-bottom: 0;">Available</h3>
            <div style="display:block;margin-top: 2rem;">
                <strong>Fee</strong>
                <h3 class="u-text-green" style="margin-bottom: 0;"><span id="fee-value">&pound;{{$fee}}</span></h3>
            </div>
        @endif
    </div>
    <div class="@if ($highestValuation and $company->id === $highestValuation->company_id) col-4 @else col-3 @endif">
        @if ($highestValuation and $company->id === $highestValuation->company_id)
            <strong>Your Valuation</strong>
        @else
            <strong>Highest Valuation</strong>
        @endif
        <h3 class="u-text-muted">
            {{ $vehicle->getHighestValue() ? '&pound;' . number_format($vehicle->getHighestValue()) : 'NO OFFERS' }}

            @if (
                    $highestValuation and
                    $highestExpiredValuation and
                    $highestExpiredValuation->id == $highestValuation->id
                )
                <span class="expired-valuation-date">(exp. {{ $highestValuation->created_at->format('d/m/Y') }})</span>
            @endif
        </h3>
    </div>
    <div class="@if ($highestValuation and $company->id === $highestValuation->company_id) col-4 @else col-6 @endif">
        @if ($highestValuation and $company->id === $highestValuation->company_id)
            <div class="lead-icons">
                <i class="fa fa-check-circle-o fa-3x u-text-green" aria-hidden="true"></i>
                <h3 class="u-text-green">Email sent to customer</h3>
            </div>
            <div class="lead-icons">
                <i class="fa fa-check-circle-o fa-3x u-text-green" aria-hidden="true"></i>
                <h3 class="u-text-green">Text sent to customer</h3>
            </div>
        @else
            <strong>Your Offer (&pound; - GBP)</strong>

            @if (isset($formUrl))
                <form action="{{ $formUrl }}" method="post" class="lead-form">
            @else
                <form action="{{ route('partnerUpdateLead', $highestValuation) }}" method="post" class="lead-form">
            @endif

            {{csrf_field()}}
            <div class="lead-form__col">
                <input type="number" value="{{ $vehicle->getHighestValue() ? $vehicle->getHighestValue()+50 : 50 }}" min="{{ $vehicle->getHighestValue() ? $vehicle->getHighestValue() : 0 }}" max="14000000" id="value" name="valueGreat" class="form__input form__input--small" placeholder="enter your valuation" required style="margin-bottom: 2.5rem;">

                <div class="col-6">
                    <input type="radio" name="delivery_method" value="collection" id="collection" checked="checked">
                    <label for="collection" class="form__label form__label--inline">Collection</label>
                </div>
                <div class="col-6">
                    <input type="radio" name="delivery_method" id="dropoff" value="dropoff">
                    <label for="dropoff" class="form__label form__label--inline">Drop Off</label>
                </div>

            </div>
            <div class="lead-form__col">
                <button type="submit" class="button button--green-bg">Purchase Lead</button>
            </div>
        </form>
        @endif
    </div>
    <div class="clear"></div>
    <div class="divider"></div>

    @include('matrix.vehicles.cap-values')
    <div class="divider"></div>
</div>
