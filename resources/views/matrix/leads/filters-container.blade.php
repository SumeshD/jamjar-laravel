<?php

$milesRanges = \JamJar\Services\VehicleFilter\VehicleFilterService::getAvailableMileagesRanges();
$minValues = \JamJar\Services\VehicleFilter\VehicleFilterService::getMinAvailableValues();
$maxValues = \JamJar\Services\VehicleFilter\VehicleFilterService::getMaxAvailableValues();
$fuelTypes = \JamJar\Services\VehicleFilter\VehicleFilterService::getAvailableFuelTypes();
$plateYears = \JamJar\Services\VehicleFilter\VehicleFilterService::getAvailablePlateYears();
$datesRanges = \JamJar\Services\VehicleFilter\VehicleFilterService::getAvailableDatesRanges();
$showDates = isset($showDates) ? $showDates : true;
$showHeaderMenu = isset($showHeaderMenu) ? $showHeaderMenu : true;
$showApplyFiltersButton = isset($showApplyFiltersButton) ? $showApplyFiltersButton : true;
$showAlertButton = isset($showAlertButton) ? $showAlertButton : true;
$banner = (isset($_GET['banner']) && $_GET['banner'] == true) ? true : false;
$lastAlert = isset($lastAlert) ? $lastAlert : false;
?>

@push('scripts-after')
    <style type="text/css">
        .filters-container .selects-section [class*="col-"] {
            margin: 0;
            padding: 0;
        }
    </style>
@endpush

<div class="filters-container {{ (($vehicleFilters->isAnyFilterSelected() and !isset($_GET['banner'])) or !$showHeaderMenu) ? 'expanded' : '' }}">
    <div class="filters-header">
        @if ($showHeaderMenu)
            <div style="max-width: 350px;">
                <span class="toggle-filters-area">
                    <img src="/images/icons/expand-mark-expand.png" class="expand-icon" style="vertical-align: baseline; width: 15px;">
                    <img src="/images/icons/expand-mark-hide.png" class="hide-icon" style="vertical-align: baseline; width: 15px;">
                    <span style="margin-left: 10px;">FILTERS</span>
                </span>
                <a href="{{ route('partnerGoodNewsLeads') }}" class="clear-filters-button button button--small button--orange">CLEAR FILTERS <span style="font-size: 15px; margin-left: 15px;">✕</span></a>
            </div>
            <div class="clear"></div>
        @else
            <span>Vehicle filters</span>
        @endif
    </div>
    <div class="filters-section selects-section col-12">
        <div class="col-2">CAR CONDITION</div>
        <div class="col-2">
            <input type="radio" class="form__input" name="car-condition" value="all" @if (!$vehicleFilters->carCondition) checked @endif  @if ($lastAlert && $lastAlert->car_condition==null) checked @endif> <strong>Show All</strong>
        </div>
        <div class="col-4">
            <input type="radio" class="form__input" name="car-condition" value="non-runners" @if ($vehicleFilters->carCondition == 'non-runners') checked @endif @if ($lastAlert && $lastAlert->car_condition=='non-runners') checked @endif> <strong>Show Non Runners / Write Offs Only</strong>
        </div>
        <div class="col-3">
            <input type="radio" class="form__input" name="car-condition" value="runners" @if ($vehicleFilters->carCondition == 'runners') checked @endif @if ($lastAlert && $lastAlert->car_condition=='runners') checked @endif> <strong>Show Runners Only</strong>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="filters-section col-12 selects-section">
        <div class="col-2">SELLER TYPE</div>
        <div class="col-2">
            <input type="radio" class="form__input" name="seller-type" value="all" @if (!$vehicleFilters->sellerType) checked @endif @if ($lastAlert && $lastAlert->seller_type==null) checked @endif> <strong>Show All</strong>
        </div>
        <div class="col-4">
            <input type="radio" class="form__input" name="seller-type" value="customers-only" @if ($vehicleFilters->sellerType == 'customers-only') checked @endif @if ($lastAlert && $lastAlert->seller_type=='customers-only') checked @endif> <strong>Show Customer Vehicles only</strong>
        </div>
        <div class="col-3">
            <input type="radio" class="form__input" name="seller-type" value="partners-only" @if ($vehicleFilters->sellerType == 'partners-only') checked @endif @if ($lastAlert && $lastAlert->seller_type=='partners-only') checked @endif> <strong>Show Partner Vehicles only</strong>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="filters-section col-12 selects-section vehicle-section">
        <div class="col-2">VEHICLE TYPE</div>
        <div class="col-2">
            <input type="radio" class="form__input" name="vehicle-type" value="all" @if (!$vehicleFilters->vehicleType) checked @endif @if ($lastAlert && $lastAlert->vehicle_type==null) checked @endif> <strong>Show All</strong>
        </div>
        <div class="col-4">
            <input type="radio" class="form__input" name="vehicle-type" value="car" @if ($vehicleFilters->vehicleType == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::VEHICLE_TYPE_CAR) checked @endif @if ($lastAlert && $lastAlert->vehicle_type==\JamJar\Model\MarketplaceAlerts\MarketplaceAlert::VEHICLE_TYPE_CAR) checked @endif> <strong>Car</strong>
        </div>
        <div class="col-3">
            <input type="radio" class="form__input" name="vehicle-type" value="van" @if ($vehicleFilters->vehicleType == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::VEHICLE_TYPE_VAN) checked @endif @if ($lastAlert && $lastAlert->vehicle_type==\JamJar\Model\MarketplaceAlerts\MarketplaceAlert::VEHICLE_TYPE_VAN) checked @endif> <strong>Van</strong>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="filters-section col-12 selects-section">
        <div class="col-2">VEHICLE LOCATION</div>
        <div class="col-2">
            <input type="radio" class="form__input" name="vehicle-location" value="all" @if (!$vehicleFilters->vehicleLocation) checked @endif @if ($lastAlert && $lastAlert->vehicle_location==null) checked @endif> <strong>Show All</strong>
        </div>
        <div class="col-4">
            <input type="radio" class="form__input" name="vehicle-location" value="postcode" @if ($vehicleFilters->vehicleLocation == 'postcode') checked @endif @if ($lastAlert && $lastAlert->vehicle_location=='postcode') checked @endif> <strong>Use Associate Settings</strong>
        </div>

    </div>

    <div class="clearfix"></div>

    <div class="filters-section">
        <div class="col-2">CAR SPECIFICATION</div>
        <div class="col-10">
            <div class="form-group col-4">
                <div class="search-manufacturers ">
                    <input type="hidden" name="manufacturersIds" value="{{ old('manufacturersIds') ?? '' }}" />
                    <div class="">
                        <div class="selected-manufacturers tooltip-subject nothing" data-toggle="modal" data-target="#myModal"><div style="text-align: center; height: 100%;"><img style="height: 90%;" src="/images/spinners/circle.png"></div></div>


                        <div class="models-and-manufacturers-holder modal-custom" id="myModal" role="dialog" style="top: 0 !important; margin-top: 10px !important;">
                            <div class="">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
                                    <h4 class="text-center modal-title">Select manufacturers below</h4>
                                </div>
                                <div class="search-section">

                                    <div class="row nopadding">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding">
                                            <p class="modal-label">Search for a manufacturer or model</p>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 nopadding">
                                            <div class="flexer">

                                                <input style="width: auto;" type="text" class="vehicle-manufacturers-and-models-search" placeholder="Search...">
                                                <button class="clear-filters modal-filter-btn button--orange-bg" style="margin-right:5px;">Clear filters <i class="fa fa-close"></i></button>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <div class="model-section">
                                    @push('scripts-after')
                                        <script type="text/javascript">
                                            function changeSelectedManufacturersBoxSize() {
                                                var $box = $('.manufacturers-list');

                                                if (!$box) {
                                                    return;
                                                }

                                                var labelHeight = 150;
                                                var labelFooter = 80;
                                                var modalHeight = (window.innerHeight - (labelHeight + labelFooter + 10));
                                                modalHeight = modalHeight > 100 ? modalHeight : 100;
                                                $box.css('max-height', modalHeight + 'px');
                                            }

                                            $(function() {
                                                $(window).resize(function() {
                                                    changeSelectedManufacturersBoxSize();
                                                });

                                                $('.selected-manufacturers').on('click', function() {
                                                    changeSelectedManufacturersBoxSize();
                                                });
                                            });
                                        </script>
                                    @endpush
                                    <div class="manufacturers-list">
                                        <?php /** @var \JamJar\Services\VehicleFilter\VehicleFilters $vehicleFilters */ ?>
                                        <span class="previousVehicleFilters" data-previous-filters="{{ json_encode($vehicleFilters->toArray()) }}"></span>
                                    </div>
                                    <div class="bottom-buttons" style="text-align: center;padding:15px">
                                        <button class="show-all-manufacturers-button active modal-filter-btn button--orange-bg" style="width:240px; margin-right: 20px;">SHOW ALL MANUFACTURERS</button>
                                        <button class="save-filters modal-filter-btn button--green-bg" style="width:240px">SAVE FILTERS <i class="fa fa-check"></i></button>
                                    </div>


                                </div>

                            </div>




                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-4">
                <select name="min-plate-year" class="form__input ">
                    <option value="">Year From (any) to now</option>
                    @foreach ($plateYears as $plateYear)
                        <option {{ $vehicleFilters->minPlateYear == $plateYear || ($lastAlert && $lastAlert->min_plate_year==$plateYear) ? 'selected' : '' }} value="{{ $plateYear }}">{{ $plateYear }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-4">
                <select name="number-of-owners" class="form__input ">
                    <option value="">Previous owners (any)</option>
                    @for ($i = 1; $i <= 5; $i++)
                        <option {{ $vehicleFilters->numberOfOwners == $i || ($lastAlert && $lastAlert->number_of_owners==$i)  ? 'selected' : '' }} value="{{ $i }}">{{ trans('vehicles.owners_count_external.owners_' . $i) }}</option>
                    @endfor
                </select>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-4">
                <select name="fuel-type" class="form__input ">
                    <option value="">Fuel type (any)</option>
                    @foreach ($fuelTypes as $fuelType)
                        <option {{ $vehicleFilters->fuelType == $fuelType ? 'selected' : '' }} value="{{ $fuelType }}">{{ $fuelType=='Petrol' ? 'Petrol/Hybrid/Electric' : 'Diesel' }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-4">
                <select name="mileage-range" class="form__input ">
                    <option value="">Maximum mileage (any)</option>
                    @foreach ($milesRanges as $description => $milesRange)
                        <option {{ json_encode([$vehicleFilters->minMileage, $vehicleFilters->maxMileage]) == $milesRange || ($lastAlert && json_encode([$lastAlert->min_mileage,$lastAlert->max_mileage])==$milesRange)  ? 'selected' : '' }} value="{{ $milesRange }}">{{ $description }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-4">
                @if ($showDates)
                    <select name="dates-range" class="form__input">
                        <option value="">Date added (any)</option>
                        @foreach ($datesRanges as $description => $datesRange)
                            <option data-request="{{ request()->get('dates-range') }}" data-ranges="{{ $datesRange }}" {{ request()->get('dates-range') == $datesRange ? 'selected' : '' }} value="{{ $datesRange }}">{{ $description }}</option>
                        @endforeach
                    </select>
                @endif
            </div>

            <div class="clearfix"></div>
            <div class="form-group col-4">
                <select class="form__input " name="min-value">
                    <option value="">Minimum valuation (any)</option>
                    @foreach ($minValues as $description => $minValue)
                        <option {{ $vehicleFilters->minValue == $minValue || ($lastAlert && $lastAlert->min_value==$minValue) ? 'selected' : '' }} value="{{ $minValue }}">{{ $description }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-4">
                <select class="form__input " name="max-value">
                    <option value="">Maximum valuation (any)</option>
                    @foreach ($maxValues as $description => $maxValue)
                        <option {{ $vehicleFilters->maxValue == $maxValue || ($lastAlert && $lastAlert->max_value==$maxValue) ? 'selected' : '' }} value="{{ $maxValue }}">{{ $description }}</option>
                    @endforeach
                </select>
            </div>
            @if ($showApplyFiltersButton)
                <div class="col-4">
                    <input type="submit" value="APPLY FILTERS" class="button button--orange-bg button--small" style="margin: 0;">
                </div>
            @endif

            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

@if ($vehicleFilters->isAnyFilterSelected() and $showAlertButton and $banner)
    <div class="add-alert-container">
        <div class="col-9 col-sm-12">
            <div style="font-weight: bold; font-size: 17px; margin-bottom: 10px;">Want to find out if a car like this becomes available? Use our alert feature!</div>
            <div style="font-size: 15px">If you like, we will alert you next time a car matching this search is added to the marketplace.</div>
        </div>
        <div class="col-3 col-sm-12">
            <a href="#" style="margin: 11px 0 0 0; width: 100%;" class="save-alert-button button button--orange-bg button--small">YES, SET THIS ALERT NOW</a>
        </div>
        <div class="clear"></div>
    </div>
    <div class="loader" style="display: none;">
        <div class="loader__box" style="height: auto; max-width: 600px; overflow-y: auto; max-height: 90%; overflow-x: hidden; width: auto; min-width: 350px;">
            <div class="loader__content" style="height: auto; width: 100%;">
                <div class="add-alert-popup">
                    <div class="add-alert-header" style="position:relative;">
                        <span class="close-button">x</span>
                        Get alerted next time a similar car is added to the marketplace
                    </div>
                    <div class="add-alert-body">
                        <div style="padding-top: 30px;"><img src="/images/searching_for.png"></div>
                        <div style="margin: 30px; font-weight: bold; font-size: 17px;">Confirm your Marketplace Alert</div>
                        <div style="font-size: 14px; margin: 0 20px 25px 20px;">Don't miss out next time a vehicle is added to our marketplace that you want to see in your stock. Decide whether you want real time alerts for every vehicle or a daily summary and then give your alert a unique name to identify it in future.</div>
                        <div>
                            <div style="margin: 10px; font-weight: bold; font-size: 15px;">Alert name</div>
                            <div style="width: 50%; margin: 0 auto;"><input type="text" name="alert-name" class="form__input"></div>
                            <div class="clear"></div>
                            <div class="alert-name-error-box" style="color: red; font-size: 12px;"></div>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <div style="font-weight: bold; font-size: 15px; margin: 10px 0;">Alert frequency</div>
                            <div>
                                <span class="frequency-field-daily-holder">
                                    <input type="radio" name="alert-frequency" value="daily" class="form__radio frequency-field-daily" checked> Daily summary
                                </span>
                                <span class="frequency-field-instant-holder">
                                    <input type="radio" name="alert-frequency" value="instant" class="form__radio frequency-field-instant"> Instant alerts
                                </span>
                            </div>
                        </div>
                        <div style="margin-top: 20px;">
                            <input type="button" class="button button--orange-bg create-alert" value="ALERT ME">
                            <div class="create-alert-loader" style="display: none;"><img src="/images/spinners/circle.png"></div>
                            <div class="clear">&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@push('scripts-after')
    <style type="text/css">

    </style>
@endpush