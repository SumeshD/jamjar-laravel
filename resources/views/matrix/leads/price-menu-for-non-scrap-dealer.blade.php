<?php
$isMatrixPartner = false;
if(auth()->user() !== null){
    $isMatrixPartner = auth()->user()->isMatrixPartner();
}
?>
@if ($errors->any())
    <div class="box__content">
        <div class="alert alert-danger col-12">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    </div>
@endif
<div class="box__content">
    <div class="col-4">
        <strong>Status</strong>
        @if ($highestValuation and $company->id === $highestValuation->company_id)
            <h3 class="u-text-green" style="font-size: 2.5rem;">Paid</h3>
        @else
            <h3 class="u-text-muted" style="margin-bottom: 0;">Available</h3>
        @endif
    </div>
    <div class="col-4">
        @if ($highestValuation and $company->id === $highestValuation->company_id)
            <strong>Your Valuation</strong>
        @else
            <strong>Highest Valuation</strong>
        @endif
        <h3 id="highest-valuation" class="u-text-muted">
            {{ $vehicle->getHighestValue() ? '&pound;' . number_format($vehicle->getHighestValue()) : 'NO OFFERS' }}

            @if ($highestValuation and $highestExpiredValuation and $highestExpiredValuation->id == $highestValuation->id)
                <span class="expired-valuation-date">(exp. {{ $highestValuation->created_at->format('d/m/Y') }})</span>
            @endif
        </h3>
    </div>

    @if (!$highestValuation or $company->id !== $highestValuation->company_id)
        <div class="col-4">
            <div style="display:block;margin-top: 0rem;">
                <strong>Fee (+VAT)</strong>
                <h3 class="u-text-green" style="margin-bottom: 0;"><span id="fee-value">&pound;{{number_format($fee, 2)}}</span></h3>
            </div>
        </div>
    @else
        <div class="col-4">
            <div class="lead-icons">
                <i class="fa fa-check-circle-o fa-3x u-text-green" aria-hidden="true"></i>
                <h3 class="u-text-green">Email sent to customer</h3>
            </div>
            <div class="lead-icons">
                <i class="fa fa-check-circle-o fa-3x u-text-green" aria-hidden="true"></i>
                <h3 class="u-text-green">Text sent to customer</h3>
            </div>
        </div>
    @endif

    <div class="clear"></div>
    <div class="divider"></div>

    @include('matrix.vehicles.cap-values')
    @if($isMatrixPartner)<div class="divider"></div>@endif
    <div>
        @if (!$highestValuation or $company->id !== $highestValuation->company_id)
            <form action="{{ isset($formUrl) ? $formUrl : route('partnerUpdateLead', $highestValuation) }}" method="post" class="lead-form">
                @include('matrix.leads.sliders')
            </form>
        @endif
    </div>
    <div class="clear"></div>
</div>
