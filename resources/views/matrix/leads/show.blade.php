@extends('layouts.app')
@section('content')

@include('matrix.partials.header', ['title' => 'Marketplace', 'buttons' =>
    '<a class="button button--small button--orange-bg u-float-right" onclick="window.print()">Print Page</a>'
])

<?php $company = auth()->user()->company; ?>

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'leads'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header" style="margin-top: 0;">
                <div class="box__header header-theme-bidding">Marketplace Lead Details</div>
                <div class="box__container">
                    <div class="box__content">
                        @if ($vehicle->meta->write_off==0 && $vehicle->meta->non_runner==0)

                            @include('matrix.leads.price-menu-for-non-scrap-dealer')
                        @else
                            @include('matrix.leads.price-menu-for-scrap-dealer')
                        @endif
                        @include('matrix.vehicles.information')
                        @if ($vehicle->additionalInformationHasBeenProvided())
                            @include('matrix.vehicles.additional-information')
                        @endif
                        @include('matrix.vehicles.images')
                        <div class="divider"></div>
                        @include('matrix.vehicles.owner-information')

                    </div>

                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="vehicle_id", value="{{$vehicle->id}}">
</section>
@endsection
