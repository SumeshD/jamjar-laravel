@php ($defaultConditionGood = auth()->user()->profile->getVehicleConditionDefaultPercentageGood())
@php ($defaultConditionFair = auth()->user()->profile->getVehicleConditionDefaultPercentageFair())
@php ($defaultConditionPoor = auth()->user()->profile->getVehicleConditionDefaultPercentagePoor())

@php ($currentConditionGood = isset($highestMatrixValuation) ? round($highestMatrixValuation->getVehicleConditionValueGood() / $highestMatrixValuation->value_int * 100) : $defaultConditionGood)
@php ($currentConditionFair = isset($highestMatrixValuation) ? round($highestMatrixValuation->getVehicleConditionValueFair() / $highestMatrixValuation->value_int * 100) : $defaultConditionFair)
@php ($currentConditionPoor = isset($highestMatrixValuation) ? round($highestMatrixValuation->getVehicleConditionValuePoor() / $highestMatrixValuation->value_int * 100) : $defaultConditionPoor)

@php ($showPurchaseButtons = isset($showPurchaseButtons) ? $showPurchaseButtons : true)
@php ($showSliderToggleButton = isset($showSliderToggleButton) ? $showSliderToggleButton : true)

@php ($greatConditionText = isset($greatConditionText) ? $greatConditionText : '')
@php ($alwaysShowSliders = isset($alwaysShowSliders) ? $alwaysShowSliders : false)

@php ($showUpdateButton = isset($showUpdateButton) ? $showUpdateButton : false)

@push('scripts-after')
    <script>
        $(function () {
            //work out high
            var HighVal = $('#highest-valuation').text();
            HighVal = HighVal.replace(/\s/g, '');
            HighVal = HighVal.replace('£', '');
            HighVal = HighVal.replace(',', '');
            HighVal = parseInt(HighVal) ? parseInt(HighVal) : 0;
            var $newOffer = $('#new-offer-value');
            $newOffer.text('£' + (HighVal + 50).toLocaleString('EN'));

            $("#amount-great, #amount-good, #amount-fair, #amount-poor").attr({
                "min": HighVal,
                "value": HighVal + 50
            });

            $("#amount-good, #amount-fair, #amount-poor").attr({
                "max": HighVal + 50,
                "min": 0
            });

            $("#amount-great").change(function () {

                var GreatValue = parseInt($(this).val());
                var MinVal = parseInt($(this).attr('min'));

                //makesure it is not set to 0

                if (GreatValue < MinVal) {

                    $("#amount-great").val(HighVal + 1);

                    //make sure rest dont resort to 0
                    GreatValue = HighVal + 1;
                }
                $newOffer.text('£' + GreatValue.toLocaleString('EN'));
                //CHANGE ALL INPUTS & SLIDERS

                $("#amount-good, #amount-fair, #amount-poor").attr({
                    "max": GreatValue
                });

                //good change
                var thePercentGood = $("#slider-percent-good p span").html();
                thePercentGood = parseInt(thePercentGood);
                $('#slider-range-good').slider("option", "max", GreatValue);
                $('#slider-range-good').find('.ui-slider-handle').css("left", thePercentGood + '%');
                $('#slider-range-good').find('.ui-slider-range').css("width", thePercentGood + '%');
                $("#amount-good").val((GreatValue / 100 * thePercentGood).toFixed());

                //fair change
                var thePercentFair = $("#slider-percent-fair p span").html();
                thePercentFair = parseInt(thePercentFair);
                $('#slider-range-fair').slider("option", "max", GreatValue);
                $('#slider-range-fair').find('.ui-slider-handle').css("left", thePercentFair + '%');
                $('#slider-range-fair').find('.ui-slider-range').css("width", thePercentFair + '%');
                $("#amount-fair").val((GreatValue / 100 * thePercentFair).toFixed());

                //poor change
                var thePercentPoor = $("#slider-percent-poor p span").html();
                thePercentPoor = parseInt(thePercentPoor);
                $('#slider-range-poor').slider("option", "max", GreatValue);
                $('#slider-range-poor').find('.ui-slider-handle').css("left", thePercentPoor + '%');
                $('#slider-range-poor').find('.ui-slider-range').css("width", thePercentPoor + '%');
                $("#amount-poor").val((GreatValue / 100 * thePercentPoor).toFixed());

            });

            //work out 80%
            var GreatValue = $("#amount-great").val();
            var ConGood = GreatValue / 100 * parseInt($('[name=vehicleConditionPercentageGood]').val());
            var ConFair = GreatValue / 100 * parseInt($('[name=vehicleConditionPercentageFair]').val());
            var ConPoor = GreatValue / 100 * parseInt($('[name=vehicleConditionPercentagePoor]').val());

            $("#slider-range-good").slider({
                range: "min",
                value: ConGood,
                min: 0,
                max: GreatValue,
                slide: function (event, ui) {

                    var GreatValue = $("#amount-great").val();
                    var theP = ui.value / GreatValue * 100;
                    var percentageValue = (ui.value / GreatValue * 100).toFixed();


                    $("#amount-good").val(ui.value);
                    $("#slider-percent-good p span").html(percentageValue);
                    $("#slider-percent-good").css("left", theP + '%');
                    $('[name=vehicleConditionPercentageGood]').val(percentageValue);
                }
            });

            $("#amount-good").val($("#slider-range-good").slider("value"));

            $("#slider-range-fair").slider({
                range: "min",
                value: ConFair,
                min: 0,
                max: GreatValue,
                slide: function (event, ui) {

                    var GreatValue = $("#amount-great").val();
                    var theP = ui.value / GreatValue * 100;
                    var percentageValue = (ui.value / GreatValue * 100).toFixed();

                    $("#amount-fair").val(ui.value);
                    $("#slider-percent-fair p span").html(percentageValue);
                    $("#slider-percent-fair").css("left", theP + '%');
                    $('[name=vehicleConditionPercentageFair]').val(percentageValue);
                }
            });

            $("#amount-fair").val($("#slider-range-fair").slider("value"));

            $("#slider-range-poor").slider({
                range: "min",
                value: ConPoor,
                min: 0,
                max: GreatValue,
                slide: function (event, ui) {

                    var GreatValue = $("#amount-great").val();
                    var theP = ui.value / GreatValue * 100;
                    var percentageValue = (ui.value / GreatValue * 100).toFixed();

                    $("#amount-poor").val(ui.value);
                    $("#slider-percent-poor p span").html(percentageValue);
                    $("#slider-percent-poor").css("left", theP + '%');
                    $('[name=vehicleConditionPercentagePoor]').val(percentageValue);
                }
            });

            $("#amount-poor").val($("#slider-range-poor").slider("value"));


            //change

            $("#amount-good").change(function () {
                var value = parseInt($(this).val());
                var GreatValue = parseInt($('#amount-great').val());
                var values = GreatValue - value;
                console.log('good change');

                if (values > 0) {
                } else {
                    value = GreatValue;
                    $("#amount-good").val(value);
                }
                var percentageValue = (value / GreatValue * 100).toFixed();

                $("#slider-range-good").slider("value", parseInt(value));
                $("#slider-percent-good p span").html(percentageValue);
                $("#slider-percent-good").css("left", value / GreatValue * 100 + '%');

                $('[name=vehicleConditionPercentageGood]').val(percentageValue);

            });

            $("#amount-fair").change(function () {
                var value = parseInt($(this).val());
                var GreatValue = parseInt($('#amount-great').val());


                var values = GreatValue - value;

                console.log('fair change');

                if (values > 0) {
                } else {
                    value = GreatValue;
                    $("#amount-fair").val(value);
                }
                var percentageValue = (value / GreatValue * 100).toFixed();
                $("#slider-range-fair").slider("value", parseInt(value));
                $("#slider-percent-fair p span").html(percentageValue);
                $("#slider-percent-fair").css("left", value / GreatValue * 100 + '%');

                $('[name=vehicleConditionPercentageFair]').val(percentageValue);
            });

            $("#amount-poor").change(function () {
                var value = parseInt($(this).val());
                var GreatValue = parseInt($('#amount-great').val());

                var values = GreatValue - value;

                console.log('poor change');

                if (values > 0) {
                } else {
                    value = GreatValue;
                    $("#amount-poor").val(value);
                }

                var percentageValue = (value / GreatValue * 100).toFixed();

                $("#slider-range-poor").slider("value", parseInt(value));
                $("#slider-percent-poor p span").html(percentageValue);
                $("#slider-percent-poor").css("left", value / GreatValue * 100 + '%');

                $('[name=vehicleConditionPercentagePoor]').val(percentageValue);
            });

            $('#toggle-sliders').on('click', function (e) {
                $('.lead-form__slider').toggleClass('slider-toggle');
                var isShowed = !$('.lead-form__slider').hasClass('slider-toggle');

                $('#toggle-sliders').html(isShowed ? 'Hide sliders' : 'Show sliders');
                $('[name=valuationVehicleConditionShow]').val(isShowed ? 1 : 0);
            });
        });
    </script>
@endpush

<div class="col-12" style="margin-bottom: 1rem;">
    <h6 style="margin-bottom: 1rem;">Increase your offers:</h6>
</div>

<div class="{{ $showPurchaseButtons ? 'col-8' : 'col-12' }} lead-form__slider {{ $alwaysShowSliders == true ? '' : 'slider-toggle' }}">

    <div class="col-12 slider-block">
        <div class="col-3 lead-form__slider__left">
            <strong>
                <div class="lead-form__slider__info">
                    <i class="fa fa-info-circle"
                       aria-hidden="true"></i>

                    <div class="lead-form__slider__info__text">
                        <p>Great condition - No damage, showroom
                            condition.</p>
                    </div>

                </div>
                Great condition
            </strong>
        </div>
        <div class="col-6">
            <div class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                <div id="slider-percent-great"
                     class="slider-percentage" style="left: 98.5%;">
                    <p><span>100</span>%</p></div>
                <div class="ui-slider-range ui-corner-all ui-widget-header ui-slider-range-good"
                     style="width: 100%;"></div>
            </div>
        </div>
        <div class="col-3">
            <input class="lead-form__slider__input update-fee" type="number" max="" name="valueGreat" id="amount-great">
        </div>
    </div>

    <div class="col-12 slider-block">
        <div class="col-3 lead-form__slider__left">
            <strong>
                <div class="lead-form__slider__info">
                    <i class="fa fa-info-circle"
                       aria-hidden="true"></i>
                    <div class="lead-form__slider__info__text">
                        <p>Just a couple of slight defects - one or
                            two ding dents or scuffed alloy wheels,
                            or a light scratch.</p>
                    </div>
                </div>
                Good condition
            </strong>
        </div>
        <div class="col-6">
            <div id="slider-range-good">
                <div style="left:{{ $currentConditionGood }}%;"
                     class="slider-percentage"
                     id="slider-percent-good"><p>
                        <span>{{ $currentConditionGood }}</span>%
                    </p></div>
            </div>
        </div>
        <div class="col-3">
            <input class="lead-form__slider__input" type="number"
                   max="" name="valueGood" id="amount-good">
        </div>
    </div>

    <div class="col-12 slider-block">
        <div class="col-3 lead-form__slider__left">
            <strong>
                <div class="lead-form__slider__info">
                    <i class="fa fa-info-circle"
                       aria-hidden="true"></i>
                    <div class="lead-form__slider__info__text">
                        <p>2 or 3 heavier scratches, couple of tyres
                            below legal limit, small dents.</p>
                    </div>
                </div>
                Fair condition
            </strong>
        </div>
        <div class="col-6">
            <div id="slider-range-fair">
                <div style="left:{{ $currentConditionFair }}%;"
                     class="slider-percentage"
                     id="slider-percent-fair"><p>
                        <span>{{ $currentConditionFair }}</span>%
                    </p></div>
            </div>
        </div>
        <div class="col-3">
            <input class="lead-form__slider__input" type="number"
                   max="" name="valueFair" id="amount-fair">
        </div>
    </div>

    <div class="col-12 slider-block" style="margin-bottom: 2rem;">
        <div class="col-3 lead-form__slider__left">
            <strong>
                <div class="lead-form__slider__info">
                    <i class="fa fa-info-circle"
                       aria-hidden="true"></i>
                    <div class="lead-form__slider__info__text">
                        <p>Heavily dented, heavy scratches, illegal
                            tyres, poor interior condition.</p>
                    </div>
                </div>
                Poor condition
            </strong>
        </div>
        <div class="col-6">
            <div id="slider-range-poor">
                <div style="left:{{ $currentConditionPoor }}%;"
                     class="slider-percentage"
                     id="slider-percent-poor"><p>
                        <span>{{ $currentConditionPoor }}</span>%
                    </p></div>
            </div>
        </div>
        <div class="col-3">
            <input class="lead-form__slider__input" type="number"
                   max="" name="valuePoor" id="amount-poor">
        </div>
        <div class="col-12 percentage-save" style="margin-top:2em;">
            <div>
                <input type="checkbox" name="saveConditionsAsDefault" value="1"/>
                <span>Save percentages as default settings for future leads</span>
            </div>
        </div>
    </div>
        <div>
            <button style="float:left" type="button" id="toggle-sliders" class="button button--orange-bg">
                Show Sliders
            </button>
        </div>
    @if($showUpdateButton)
        <div style="float: right; margin-right:30px;">
            <input type="submit" value="UPDATE" class="button button--orange-bg">
        </div>
    @endif
</div>

<input type="hidden" name="vehicleConditionPercentageGood" value="{{ $currentConditionGood }}">
<input type="hidden" name="vehicleConditionPercentageFair" value="{{ $currentConditionFair  }}">
<input type="hidden" name="vehicleConditionPercentagePoor" value="{{ $currentConditionPoor  }}">
<input type="hidden" name="valuationVehicleConditionShow" value="{{ auth()->user()->profile->valuation_vehicle_condition_show ? '1' : '0' }}">
{{csrf_field()}}

@if ($showPurchaseButtons )
    <div class="col-4 lead-form__purchase">
        <strong style="margin-bottom:2rem; width:100%; float:left;">Vehicle collection</strong>
        <div class="lead-form__col" style="width:100%; margin-bottom:2rem;">
            <div class="col-6">
                <input type="radio" name="delivery_method" value="collection" id="collection" checked="checked">
                <label for="collection" class="form__label form__label--inline">Collection</label>
            </div>
            <div class="col-6">
                <input type="radio" name="delivery_method" id="dropoff" value="dropoff">
                <label for="dropoff" class="form__label form__label--inline">Drop Off</label>
            </div>
        </div>
        <div class="lead-form__col" style="width:100%;">
            <button type="submit" class="button button--green-bg">
                Purchase Lead
            </button>
        </div>
    </div>
@endif