<div class="box box--with-header box--no-margin" style="margin-bottom: 30px;">
    <div class="box__container">
        <div class="box__content dashboard-container">
            <div class="dashboard-header">Getting started</div>
            <span>Complete your account setup steps to get full access to buying or selling vehicles.</span>
            <div class="clear"></div>
            <div>
                <a href="{{ route('partnerAccount') }}">
                    <div class="dashboard-started-box {{ auth()->user()->isAccountDetailsComplete() ? 'completed' : 'uncompleted' }}">
                        <div class="dashboard-inner-box">
                            <div class="tick-mark"></div>
                            <div class="completing-box">NEEDS COMPLETING</div>
                            <img src="/images/associates/account.png">
                            <div>
                                <div class="inner-box-title">Account Details</div>
                                <div class="inner-box-description">These are the details you provided on your application form. Additional locations can be added here.</div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('partnerSettingsForm') }}">
                    <div class="dashboard-started-box {{ auth()->user()->isAssociateSettingsComplete() ? 'completed' : 'uncompleted' }}">
                        <div class="dashboard-inner-box even">
                            <div class="tick-mark"></div>
                            <div class="completing-box">NEEDS COMPLETING</div>
                            <img src="/images/associates/sliders.png">
                            <div>
                                <div class="inner-box-title">Associate Settings</div>
                                <div class="inner-box-description">Set postcode areas to purchase from, whether you offer collection or allow drop-off and more.</div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('partnerWebsiteTheme') }}">
                    <div class="dashboard-started-box {{ auth()->user()->isWebsiteThemeComplete() ? 'completed' : 'uncompleted' }}">
                        <div class="dashboard-inner-box">
                            <div class="tick-mark"></div>
                            <div class="completing-box">NEEDS COMPLETING</div>
                            <img src="/images/associates/color-wheel.png">
                            <div>
                                <div class="inner-box-title">Website Theme</div>
                                <div class="inner-box-description">Configure the appearance and content for the pages customers will see when viewing your offers.</div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('partnerCredits') }}">
                    <div class="dashboard-started-box last-box {{ auth()->user()->isFoundAndBillingsComplete() ? 'completed' : 'uncompleted' }}">
                        <div class="dashboard-inner-box even">
                            <div class="tick-mark"></div>
                            <div class="completing-box">NEEDS COMPLETING</div>
                            <img src="/images/associates/coins.png">
                            <div>
                                <div class="inner-box-title">Funds & Billing</div>
                                <div class="inner-box-description">Add funds to your account to get started in buying or selling vehicles.</div>
                            </div>
                        </div>
                    </div>
                </a>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>