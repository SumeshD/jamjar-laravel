@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Dashboard', 'buttons' =>
    '<div class="dashboard-funds-box">
    <div class="dashboard-funds-box-title">FUNDS</div>
    <div class="dashboard-funds-box-amount">&pound;'. auth()->user()->funds->funds .'</div>
    <div class="dashboard-funds-box-button-holder">
        <a class="button button--small button--green-bg" href="'. route('partnerCredits') .'">ADD FUNDS <span class="dashboard-funds-box-button-plus-icon"></span></a>
    </div>
</div>'
])

@push('scripts-after')
    <style type="text/css">

    </style>
@endpush

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'dashboard'])
        </div>
        <div class="dashboard__col">
            @if (!auth()->user()->isSetupComplete())
                @include('matrix.dashboard.get-started-partial')
            @endif

            <div class="dashboard-summary-box">
                <div class="dashboard-sections-separator">Buying</div>
                <div class="dashboard-summary-inner-box first">
                    <div class="summary-title">Latest Agreed Purchases</div>

                    <?php $baseRowHeightInPx = 45; ?>
                    <?php $agreedPurchasesCount = count($agreedPurchases); ?>
                    <?php $firstSectionHeight = max(($agreedPurchasesCount > 6 ? 6 : $agreedPurchasesCount), 5);  ?>

                    <div style="min-height: {{ $firstSectionHeight * $baseRowHeightInPx }}px">
                        @if ($agreedPurchasesCount > 0)
                            <?php $displayedAgreedPurchasesCounter = 1; ?>
                            @foreach ($agreedPurchases as $agreedPurchase)
                                <?php /** @var \JamJar\Sale $agreedPurchase */ ?>
                                <div class="dashboard-summary-row">
                                  <a href="{{ route('partnerUpdateSale', $agreedPurchase) }}">
                                    <div class="primary-info">{{ $agreedPurchase->getVehicle()->getNumberPlate() }}</div>
                                    <div class="additional-info tooltip-subject" title="{{ $agreedPurchase->getVehicle()->getFormattedName() }}">{{ $agreedPurchase->getVehicle()->getFormattedName() }}</div>
                                    <div class="price-info">&pound; {{ number_format($agreedPurchase->getPriceInPence() / 100) }}</div>
                                    <div class="follow-box"> > </div>
                                  </a>
                                </div>
                                @if (++$displayedAgreedPurchasesCounter > 5)
                                    @break
                                @endif
                            @endforeach
                            @if (count($agreedPurchases) > 5)
                                <div class="dashboard-summary-row link-row">
                                    <div class="link-text"><a href="{{ route('matrixAgreedPurchasesIndex') }}">+ {{ (count($agreedPurchases) - 5) }} MORE</a></div>
                                    <div class="follow-box"> <a href="{{ route('matrixAgreedPurchasesIndex') }}"> > </a> </div>
                                </div>
                            @else
                                @for ($i = count($agreedPurchases); $i < 5; $i++)
                                    <div class="dashboard-summary-row"></div>
                                @endfor
                            @endif
                        @else
                            <div class="dashboard-summary-row" style="min-height: {{ $firstSectionHeight * $baseRowHeightInPx }}px; background-color: #f9fbfd; height: 100%; padding-top: {{ $firstSectionHeight * $baseRowHeightInPx / 2.7 }}px;">
                                <div class="empty-info">You have no outstanding agreed purchases</div>
                            </div>
                        @endif
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="dashboard-summary-inner-box">
                    <div class="summary-title">Paid Leads</div>
                    <div style="height: {{ 5 * $baseRowHeightInPx }}px">
                        <div class="dashboard-summary-row">
                            <div class="big-description-text">Automated bids</div>
                            <div class="count-box">{{ $automatedBidsCount == 1 ? ($automatedBidsCount . ' vehicle') : ($automatedBidsCount . ' vehicles') }}</div>
                            <div class="follow-box"> <a href="{{ route('partnerValuations',[
                                    'offer_status'=>'all',
                                    'lead_source'=>'automated_bids',
                                    'outbid'=>false,
                                    'vrm'=>''
                                ]) }}"> > </a> </div>
                        </div>
                        <div class="dashboard-summary-row">
                            <div class="big-description-text">Currently outbid on</div>
                            <div class="count-box">{{ $outbidOnCount == 1 ? ($outbidOnCount . ' vehicle') : ($outbidOnCount . ' vehicles') }}</div>
                            <div class="follow-box"> <a href="{{ route('partnerValuations',[
                                    'offer_status'=>'all',
                                    'lead_source'=>'all',
                                    'outbid'=>true,
                                    'vrm'=>''
                                ]) }}"> > </a> </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>

                <?php
                    $secondSectionHeight = max(
                        (count($outstandingValues) > 6 ? 6 : count($outstandingValues)),
                        (count($marketplaceAlerts) > 6 ? 6 : count($marketplaceAlerts))
                    );
                ?>

                @if(!auth()->user()->profile->is_scrap_dealer)
                <div class="dashboard-summary-inner-box first">
                    <div class="summary-title">Outstanding Suggested Values</div>
                    <div style="min-height: {{ $secondSectionHeight * $baseRowHeightInPx }}px">
                        @if (count($outstandingValues) > 0)
                            <?php $printedOutstandingValuesCounter = 1; ?>
                            @foreach ($outstandingValues as $outstandingValue)
                                <div class="dashboard-summary-row">
                                    <div class="primary-info tooltip-subject" title="{{ $outstandingValue->offer_name }}">{{ $outstandingValue->offer_name }}</div>
                                    <?php
                                        /** @var \JamJar\OfferDerivative $outstandingValue */
                                        $fullName = \JamJar\VehicleDerivative::getFullNameByOfferDerivativeId($outstandingValue->offer_derivative_id)
                                    ?>
                                    <div class="additional-info tooltip-subject" title="{{ $fullName }}">{{ $fullName }}</div>
                                    <div class="price-info">+ {{ $outstandingValue->suggested_value }}%</div>
                                    <div class="follow-box"> <a href="{{ route('suggestedValues', [$outstandingValue->offer_uuid]) }}"> > </a> </div>
                                </div>
                                @if (++$printedOutstandingValuesCounter > 5)
                                    @break
                                @endif
                            @endforeach

                        @else
                            <div class="dashboard-summary-row" style="min-height: {{ $secondSectionHeight * $baseRowHeightInPx }}px; background-color: #f9fbfd; height: 100%; padding-top: {{ $secondSectionHeight * $baseRowHeightInPx / 2.7 }}px;">
                                <div class="empty-info">You have no outstanding suggested values</div>
                            </div>
                        @endif
                    </div>

                    <div class="clear"></div>
                </div>
                @endif

                <div class="dashboard-summary-inner-box">
                    <div class="summary-title">Marketplace Alerts</div>
                    <div style="min-height: {{ $secondSectionHeight * $baseRowHeightInPx }}px">
                        @if (count($marketplaceAlerts) > 0)
                            <?php $displayedAlertsCounter = 1; ?>
                            @foreach ($marketplaceAlerts as $alertArrayElement)
                                <div class="dashboard-summary-row">
                                    <div class="average-description-text">{{ $alertArrayElement['alert']->getName() }}</div>
                                    <div class="price-info">{{ $alertArrayElement['vehicleCount'] }}</div>
                                    <div class="follow-box"> <a href="{{ route('partnerGoodNewsLeads', $alertArrayElement['alert']->getAsRequestParameters(false)) }}"> > </a> </div>
                                </div>
                                @if (++$displayedAlertsCounter > 5)
                                    @break
                                @endif
                            @endforeach

                            @if (count($marketplaceAlerts) > 5)
                                <div class="dashboard-summary-row link-row">
                                    <div class="link-text"><a href="{{ route('marketplaceEnabledAlertsList') }}">+ {{ (count($marketplaceAlerts) - 5) }} MORE</a></div>
                                    <div class="follow-box"> <a href="{{ route('marketplaceEnabledAlertsList') }}"> > </a> </div>
                                </div>
                            @endif
                        @else
                            <div class="dashboard-summary-row" style="min-height: {{ $secondSectionHeight * $baseRowHeightInPx }}px; background-color: #f9fbfd; height: 100%; padding-top: {{ $secondSectionHeight * $baseRowHeightInPx / 2.7 }}px;">
                                <div class="empty-info">You have not created any marketplace alerts</div>
                            </div>
                        @endif
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <?php $thirdSectionHeight = max((count($lastReceivedBids) > 6 ? 6 : count($lastReceivedBids)), 5);?>

            <div class="dashboard-summary-box">
                <div class="dashboard-sections-separator">Selling</div>
                <div class="dashboard-summary-inner-box first">
                    <div class="summary-title">Latest received bids</div>
                    <div style="min-height: {{ $thirdSectionHeight * $baseRowHeightInPx }}px">
                        @if (count($lastReceivedBids) > 0)
                            <?php $displayedLastReceivedBidsCounter = 1; ?>
                            @foreach ($lastReceivedBids as $lastReceivedBid)
                                <?php /** @var \JamJar\Valuation $lastReceivedBid */ ?>
                                <div class="dashboard-summary-row">
                                    <div class="primary-info">{{ $lastReceivedBid->getVehicle()->getNumberPlate() }}</div>
                                    <div class="additional-info tooltip-subject" title="{{ $lastReceivedBid->getVehicle()->getFormattedName() }}">{{ $lastReceivedBid->getVehicle()->getFormattedName() }}</div>
                                    <div class="price-info">&pound; {{ number_format($lastReceivedBid->getPriceInPence() / 100) }}</div>
                                    <div class="follow-box"> <a href="{{ route('matrixValuationDraftsShow', $lastReceivedBid->getVehicleId()) }}"> > </a> </div>
                                </div>
                                @if (++$displayedLastReceivedBidsCounter > 5)
                                    @break
                                @endif
                            @endforeach
                            @if (count($lastReceivedBids) > 5)
                                <div class="dashboard-summary-row link-row">
                                    <div class="link-text"><a href="{{ route('matrixValuationDraftsListedVehicles') }}">+ {{ (count($lastReceivedBids) - 5) }} MORE</a></div>
                                    <div class="follow-box"> <a href="{{ route('matrixValuationDraftsListedVehicles') }}"> > </a> </div>
                                </div>
                            @endif
                        @else
                            <div class="dashboard-summary-row" style="min-height: {{ $thirdSectionHeight * $baseRowHeightInPx }}px; background-color: #f9fbfd; height: 100%; padding-top: {{ $thirdSectionHeight * $baseRowHeightInPx / 2.7 }}px;">
                                <div class="empty-info">You have no bids to review</div>
                            </div>
                        @endif
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="dashboard-summary-inner-box">
                    <div class="summary-title">All vehicles</div>
                    <div class="dashboard-summary-row">
                        <div class="big-description-text">Listed vehicles</div>
                        <?php $listedVehiclesCount = count($listedVehicles) ?>
                        <?php $vehiclesWithAgreedSaleCount = count($vehiclesWithAgreedSale) ?>
                        <div class="count-box">{{ $listedVehiclesCount == 1 ? ($listedVehiclesCount . ' vehicle') : ($listedVehiclesCount . ' vehicles') }}</div>
                        <div class="follow-box"> <a href="{{ route('matrixValuationDraftsListedVehicles') }}"> > </a> </div>
                    </div>
                    <div class="dashboard-summary-row">
                        <div class="big-description-text">Agreed sales</div>
                        <div class="count-box">{{ $vehiclesWithAgreedSaleCount == 1 ? ($vehiclesWithAgreedSaleCount . ' vehicle') : ($vehiclesWithAgreedSaleCount . ' vehicles') }}</div>
                        <div class="follow-box"> <a href="{{ route('matrixAgreedSalesIndex') }}?active-only=true"> > </a> </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

    </div>
</section>


@endsection


@push('scripts-after')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script type="text/javascript">
        async function triggerAlert(alert){
            const { value: accept } = await Swal.fire({
                title: alert.subject,
                text: alert.text,
                input: 'checkbox',
                inputValue: 0,
                inputPlaceholder: 'Don\'t show this again',
                confirmButtonText: 'Continue',
                width: 800,
                heightAuto: false,
                allowOutsideClick: false,
            })

            if (accept) {
                $.ajax('/associates/'+alert.id+'/accept-alert');
            }
        };

        function getAjaxData() {
            return {
                'url' : '/associates/get-alert',
                'success' : function(alert) {
                    if(alert.status) {
                        triggerAlert(alert)
                    }
                }
            }
        }

        $(document).ready(function(){
            let ajaxData = getAjaxData();
            $.ajax(ajaxData);
        })
    </script>
    <style type="text/css">

    </style>
@endpush
