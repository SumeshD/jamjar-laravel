@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Sales Agreed Details', 'buttons' =>
    '<a class="button button--small button--orange-bg u-float-right" onclick="window.print()">Print Page</a>'
])

<?php $vehicle = $sale->vehicle; ?>
<?php $valuation = $sale->valuation; ?>

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'dashboard'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header" style="margin-top: 0;">
                <div class="box__header">Sales Agreed Details</div>
                <div class="box__container">
                    <div class="box__content u-no-columns">
                        <div>
                            <div class="col-3">
                                <strong>Status</strong>
                                @if ($sale->status == 'pending')
                                    <h3 class="u-text-orange">Awaiting Action</h3>
                                @elseif ($sale->status == 'rejected')
                                    <h3 class="u-text-red">Sale Rejected</h3>
                                @else
                                    <h3 class="u-text-green">Sale Agreed</h3>
                                @endif
                            </div>
                            <div class="col-3">
                                <strong>Valuation</strong>
                                <h3 class="u-text-green">&pound;{{ number_format($sale->price) }}</h3>
                            </div>
                            <div class="col-3">
                                <strong>Final Agreed Price</strong>
                                @if ($sale->status != 'rejected')
                                    @if ($sale->final_price)
                                        <h3 class="u-text-green">&pound;{{ number_format($sale->final_price) }}</h3>
                                    @else
                                        <h3 class="u-text-orange">Sale incomplete</h3>
                                    @endif
                                @else
                                    <h3 class="u-text-red">Sale refused</h3>
                                @endif
                            </div>
                            <div class="col-3">
                                <strong>Service</strong>
                                <h3 class="u-text-green">You Collect</h3>
                            </div>
                        </div>

                        <div class="divider"></div>
                        @include('matrix.vehicles.cap-values')

                        @if (($sale->status == 'pending') || ($sale->status == 'rejected'))
                            <div class="col-12">
                                <h2>PURCHASE STATUS</h2>
                                <form action="{{ route('partnerUpdateSale', $sale) }}" class="form" method="POST">
                                    {{csrf_field()}}
                                    {{method_field('PATCH')}}
                                    <div class="col-6">

                                        <p>Once your sale is complete, please confirm agreed sale price (if complete) and sale status.</p>

                                        <div class="form__group">
                                            <label for="final_price" class="form__label">Final Agreed Price</label>
                                            <input type="number" id="final_price" name="final_price" class="form__input form__input--small" placeholder="Original Valuation: &pound;{{number_format($sale->price)}}">
                                        </div>

                                        <div class="form__group">
                                            <label for="dealer_notes" class="form__label">Your Notes</label>
                                            <textarea name="dealer_notes" id="dealer_notes" class="form__input" rows="3" placeholder="Enter any notes about the sale you may have.">{{$sale->dealer_notes}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-6" style="padding-left: 30px;">

                                        <div class="form__group">
                                            <label for="status" class="form__label">Status</label>
                                        </div>
                                        <div class="form__group">
                                            <label for="status">
                                                <input type="radio" name="status" value="pending" {{ $sale->status == 'pending' ? 'checked="checked"' : '' }}> Awaiting Action
                                            </label>
                                        </div>
                                        <div class="form__group">
                                            <label for="status">
                                                <input type="radio" name="status" value="rejected" {{ $sale->status == 'rejected' ? 'checked="checked"' : '' }}> Rejected
                                            </label>
                                        </div>
                                        <div class="form__group">
                                            <label for="status">
                                                <input type="radio" name="status" value="complete"> Completed
                                            </label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div style="text-align: center; margin-top: 25px;">
                                            <input type="submit" value="Update Sale Record" class="button button--orange-bg">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif

                        <div class="divider"></div>

                        <div class="u-no-columns">
                            @include('matrix.vehicles.information',['sale'=>$sale])
                            @if ($vehicle->additionalInformationHasBeenProvided())
                                @include('matrix.vehicles.additional-information')
                            @endif
                            @include('matrix.vehicles.images')
                            <div class="divider"></div>
                            @include('matrix.vehicles.owner-information')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
