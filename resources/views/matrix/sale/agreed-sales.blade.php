@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Marketplace'])

<section class="dashboard @if (!auth()->user()->isPartner()) dashboard--faded @endif dashboard--two-col">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'agreed-sales'])
		</div>
		<div class="dashboard__col">
			<div class="box box--with-header box--no-margin">
				<div class="box__header box__header_dashboard header-theme-bidding">Agreed Sales</div>
				<div class="box__container">
					<div class="box__content">
						<div class="box__subheader box__subheader_dashboard box__content">
							<div class="col-2"><span>Date</span></div>
							<div class="col-2"><span>Registration</span></div>
							<div class="col-3"><span>Car</span></div>
							<div class="col-2"><span>Buyer price</span></div>
							<div class="col-1"><span>Status</span></div>
							<div class="col-2"></div>
						</div>

						<div class="clear"></div>
                            @unless ($sales->isEmpty())
                                @foreach ($sales as $sale)
                                    @if ($sale->valuation->vehicle)
                                    <div class="box__item row box__item_dashboard box__content">
                                        <div class="col-2">
                                            <span class="box__item_dashboard-title">Date: </span>
                                            <span class="box__item_dashboard-content">{{ $sale->created_at->format('d/m/Y') }} <br />{{ $sale->created_at->format('H:i:s') }}</span>
                                        </div>
                                        <div class="col-2">
                                            <span class="box__item_dashboard-title">Registration: </span>
                                            <span class="box__item_dashboard-content">{{ strtoupper($sale->valuation->vehicle->numberplate) }}</span>
                                        </div>
                                        <div class="col-3">
                                            <span class="box__item_dashboard-title">Car: </span>
                                            <span class="box__item_dashboard-content">{{ $sale->valuation->vehicle->meta->manufacturer . ", " .$sale->valuation->vehicle->meta->model . ", " . $sale->valuation->vehicle->meta->derivative }}</span>
                                        </div>
                                        <div class="col-2">
                                            <span class="box__item_dashboard-title">Buyer price: </span>
                                            <span class="box__item_dashboard-content">&pound;{{ number_format($sale->price) }}</span>
                                        </div>
                                        <div class="col-1">
                                            <span class="box__item_dashboard-title">Status: </span>
                                            <span class="box__item_dashboard-content">
                                                @if ($sale->status == 'pending')
                                                    <strong class="u-text-orange">Awaiting Action</strong>
                                                @elseif($sale->status == 'rejected')
                                                    <strong class="u-text-red">Offer Retracted</strong>
                                                @else
                                                    <strong class="u-text-green">Sale Agreed</strong>
                                                @endif
                                            </span>
                                        </div>
                                        <div class="col-2">
	                                      	<span class="box__item_dashboard-title">&nbsp;</span>
	                                      	<span class="box__item_dashboard-content">
	                                      		<a class="button button--small button--orange-bg" href="{{ route('matrixValuationDraftsShow', [$sale->vehicle->id]) }}">view details</a>
                                            </span>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                                <div class="pagination">
                                    {{ $sales->appends(request()->query())->links('vendor.pagination.matrix-default') }}
                                </div>
                        @else
                            <div class="col-12">
                                <div class="box__content">
                                    <p>No sales to display.</p>
                                </div>
                            </div>
                        @endunless
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
