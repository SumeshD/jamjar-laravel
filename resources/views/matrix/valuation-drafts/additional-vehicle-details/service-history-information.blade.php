<div class="form__group form__group--half form__group--improve-offer @if ($errors->has('service-history-information')) form--errors @endif">
    <label for="service-history-information" class="form__label">Additional Service History Information</label>
    <textarea style="height: 120px;" class="form__input" name="service-history-information" placeholder="E.g. Full franchise history, all invoices, etc.">{{ old('service-history-information') ?? $vehicle->getServiceHistoryInformation() }}</textarea>
</div>