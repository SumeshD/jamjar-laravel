
<div class="form__group form__group--half form__group--improve-offer @if ($errors->has('is-log-book')) form--errors @endif">
    <label for="is-log-book" class="form__label">Do you have the log book (V5) present? *</label>
    <div class="form__radioinput form__radio" style="width: 100%; text-align: left;">
        <div class="form__group form__group--half">
            <span><input type="radio" name="is-log-book" value="1" @if(old('is-log-book') === "1"  or $vehicle->getIsLogBook() === true or old('is-log-book') === null) checked @endif> <span data-id="yes">Yes</span></span>
            <span style="margin-left: 10px;"><input type="radio" name="is-log-book" value="0" @if(old('is-log-book') === "0"  or $vehicle->getIsLogBook() === false) checked @endif> <span data-id="no">No</span></span>
        </div>
    </div>
</div>