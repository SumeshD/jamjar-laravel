@push('scripts-after')
    <script src="{{ secure_asset('js/jquery.dm-uploader.min.js') }}"></script>
@endpush

<div class="box__content" id="drop-area" data-add-vehicle-image-url="{{ route('addVehicleImage', [$vehicle->id]) }}">
    <p style="font-weight: bold; font-size: 15px;">Add some great images (recommended).</p>
    <div class="mobile-hide" style="border: 2px dashed #b5b5b5; background-color: #ebebeb; height: 80px; margin-bottom: 18px; font-weight: bold; padding: 12px;">
        <div style="float: left; color: #979797; font-size: 15px; padding: 15px 0;">Maximum 8 images, Accepted file types: jpeg, png, gif.</div>
        <div style="float: right;">
            <form>
                <input style="width: 0.1px; height: 0.1px; opacity: 0; overflow: hidden; position: absolute; z-index: -1;" class="button button--green-bg button--small" type="file" id="vehicle-image-file" title="UPLOAD IMAGES">
                <label style="width: 200px;" class="button button--green-bg button--small" for="vehicle-image-file">UPLOAD IMAGES</label>
            </form>
        </div>
    </div>
    <div class="show-mobile" >
        <div style="margin-top: 15px; border: 2px dashed #b5b5b5; background-color: #ebebeb; min-height: 80px; margin-bottom: 18px; font-weight: bold; padding: 12px;">
            <div style="float: left; color: #979797; font-size: 16px; padding: 15px 0; text-align: center;">Maximum 8 images, Accepted file types: jpeg, png, gif.</div>
            <div class="clear"></div>
        </div>
        <div>
            <form>
                <input style="width: 0.1px; height: 0.1px; opacity: 0; overflow: hidden; position: absolute; z-index: -1;" class="button button--green-bg button--small" type="file" id="vehicle-image-file" title="UPLOAD IMAGES">
                <label style="width: 100%; margin-bottom: 25px;" class="button button--green-bg button--small" for="vehicle-image-file">UPLOAD IMAGES</label>
            </form>
        </div>
    </div>

    <div class="thumbnails-holder {{ count($vehicle->getImages()) == 0 ? 'mobile-hide' : '' }}" style="" id="images-holder">
        <div id="images-upload-progress" style="display: none; position:absolute; background-color: #70ce47; width: 100%; height: 3px; top: -1px; left: 0;"></div>
        @foreach($vehicle->getImages() as $image)
            @include('matrix.valuation-drafts.single-vehicle-image', ['image' => $image])
        @endforeach
        <div class="upload-images-info" style="margin: 50px auto 0; font-weight: bold; font-size: 20px; text-align: center; color: gray; position: absolute; width: 95%; {{ count($vehicle->getImages()) > 0 ? 'display: none;' : '' }}">
            Drag and drop your files here to upload
        </div>
        <div id="uploaded-image-preloader" style="display: none; width: 120px; height: 120px; margin: 7px 7px; float: left; position: relative; border: 1px solid #ec8807;">
            <img src="/images/spinners/circle.png" style="padding: 40px;">
        </div>
    </div>
</div>