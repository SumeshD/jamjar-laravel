<?php /** @var \JamJar\Vehicle $vehicle */ ?>
<?php $isInStock = old('is-in-stock') === "1"  or $vehicle->getIsInStock() === true; ?>
<div id="is-in-stock-box">
    <label for="is-in-stock" class="form__label">In stock? *</label>
    <div class="form__radioinput form__radio" style="width: 100%; text-align: left;">
        <div class="form__group form__group--half">
            <span><input type="radio" name="is-in-stock" value="1" @if($isInStock) checked @endif> <span data-id="yes">Yes</span></span>
            <span style="margin-left: 10px;"><input type="radio" name="is-in-stock" value="0" @if(!$isInStock) checked @endif> <span data-id="no">No</span></span>
        </div>
        <div class="form__group form__group--half" id="stock-availability-at-box-wrapper">
            <div id="stock-availability-at-box" style="@if($isInStock) display: none; @endif">
                <label for="stock-availability-at" style="width: 100%; font-size: 0.8em;">
                    <span style="padding: 0;">When is it due in stock?</span>
                </label>
                <input {{ !$isInStock ? 'required="required"' : '' }} name="stock-availability-at" type="date" class="form__input" value="{{ old('stock-availability-at') ?? ($vehicle->getStockAvailabilityAt() ? $vehicle->getStockAvailabilityAt()->format('Y-m-d') : null) }}">
            </div>
        </div>
    </div>
</div>

@push('scripts-after')
    <script>
        $(function() {
            $("#is-in-stock-box .iCheck-helper").click(function() {
                var selectedValue = $("input[name='is-in-stock']:checked").val();
                var $availableInDaysBox = $('#stock-availability-at-box');
                var $availabilityAtInput = $('[name="stock-availability-at"]');
                if (selectedValue == 1) {
                    $availableInDaysBox.fadeOut();
                    $availabilityAtInput.val('');
                    $availabilityAtInput.removeAttr('required');

                    return;
                } else {
                    $availableInDaysBox.fadeIn();
                    $availabilityAtInput.attr('required', 'required');
                }
            });
        });
    </script>
@endpush
