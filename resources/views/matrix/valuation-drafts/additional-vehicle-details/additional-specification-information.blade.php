<div class="form__group form__group--half form__group--improve-offer @if ($errors->has('additional-specification-information')) form--errors @endif">
    <label for="additional-specification-information" class="form__label">Additional Specification Information</label>
    <textarea style="height: 120px;" class="form__input" name="additional-specification-information" placeholder="E.g. Alloy wheels, Sat Nav, Sunroof, Air Con, etc.">{{ old('additional-specification-information') ?? $vehicle->getAdditionalSpecificationInformation() }}</textarea>
</div>