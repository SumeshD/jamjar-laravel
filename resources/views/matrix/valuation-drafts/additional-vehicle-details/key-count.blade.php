<?php
$selected = 0;
    if(old('keys-count') == 1 or $vehicle->getKeysCount() == 1) {
        $selected = 1;
    } elseif (old('keys-count') == 2 or $vehicle->getKeysCount() == 2) {
        $selected = 2;
    } elseif (old('keys-count') == 3 or $vehicle->getKeysCount() == 3) {
        $selected = 3;
    }
?>
<div class="form__group form__group--half form__group--improve-offer @if ($errors->has('keys-count')) form--errors @endif">
    <label for="keys-count" class="form__label">How many keys do you have for the vehicle? *</label>
    <select name="keys-count" id="keys-count" class="form__input" style="text-transform: none;">
        <option selected disabled>Please Select</option>
        <option {{ ($selected == 1) ? 'selected' : '' }} value="1">1</option>
        <option {{ ($selected == 2 || $selected == 0) ? 'selected' : '' }} value="2">2</option>
        <option {{ ($selected == 3) ? 'selected' : '' }} value="3">3+</option>
    </select>
</div>

