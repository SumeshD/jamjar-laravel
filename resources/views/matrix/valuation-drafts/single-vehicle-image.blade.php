<?php /** @var \JamJar\Model\VehicleImage $image */ ?>
<div class="single-vehicle-image vehicle-image">
    <span style="width: 13px; height: 13px; border-radius: 10px; background-color: #ff0101; z-index: 2; color: white; position: absolute; right: -6px; top: -6px; cursor: pointer; font-size: 8px; text-align: center;" data-vehicle-image-id="{{ $image->getId() }}" class="remove-vehicle-image-button">x</span>
    <div class="vehicle-image__container">
      <img src="{{ route('thumbnail', [$image->getImageId(), $image->getUploadedImage()->originalName()]) }}">
      <form id="remove-vehicle-image-{{ $image->getId() }}" method="POST" action="{{ route('removeVehicleImage', $image->getId()) }}">
          {{ csrf_field() }}
      </form>
    </div>
</div>
