<?php /** @var JamJar\Vehicle $vehicle */ ?>

@extends('layouts.app')

@section('content')

    <?php $onclickAction = ($vehicle->isVisibleOnMarketplace() or $vehicle->hasPendingSale()) ?
        "event.preventDefault(); confirmAction(function() { window.location=$('.edit-vehicle-button').attr('href') }, 'Editing your vehicle will result in a new vehicle lookup and any fees that you have paid will not be refunded.');" :
        ""; ?>

    @if ($vehicle->hasCompleteSale())
        <?php $buttons = '<div class="valuations__buttons" style="float: right; width: 120px;">
            <a href="#" onclick="swal(' . "'This vehicle has been sold and you are no longer able to edit it.'" . ')" class="edit-vehicle-button button button--grey-bg " >
                Edit Details
            </a>
        </div>'; ?>
    @elseif ($vehicle->hasExpiredOffer())
        <?php $buttons = '<p style="margin-top: 3rem; float: right;">
            <strong>
                Some of your comparison results have expired.<br />
                <a href="' . route('matrixValuationDraftsShowEditVehicleForm', $vehicle->id) . '" class="edit-vehicle-button" onclick="' . $onclickAction . '">
                    Get Fresh Results
                </a>
            </strong>
        </p>'; ?>
    @else
        <?php $buttons = '<div class="valuations__buttons mobile-hide" style="float: right; width: 120px;">
                <a href="' . route('matrixValuationDraftsShowEditVehicleForm', $vehicle->id) . '" class="edit-vehicle-button button button--grey-bg " onclick="' . $onclickAction .'">
                    Edit Details
                </a>
            </div>'; ?>
    @endif


    <header class="subheader">
        <div class="subheader__container">
            <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
                <h1 class="subheader__title" style="width: 100%;">Partner Lookups</h1>
            </div>
            <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12"></div>
            <div class="col-lg-6 col-sm-12 col-md-12 col-xs-12">
                @if (isset($buttons))
                    {!! $buttons !!}
                @endif
            </div>
        </div>
    </header>

<?php /** @var \JamJar\Vehicle $vehicle */ ?>
<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'drafts'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header box--no-margin">
                <div class="box__header">Valuations from other partners</div>
                <div style="margin: 0 30px; ">
                    @include('quote.vehicle-details-header-button', ['showLabel' => false])
                    @include('quote.vehicle-details-header')
                </div>
                @if (count($offeredValues) > 0)
                    <div class="box__container">
                        <div class="box__content">
                            <table style="width:100%;" class="valuation-table-header-only">
                                <thead>
                                <tr>
                                    <th width="25%">Dealer</th>
                                    <th width="35%">Details</th>
                                    <th width="40%">Valuation</th>
                                </tr>
                                </thead>
                            </table>

                            @foreach ($offeredValues as $offeredValue)
                                @include('matrix.valuation-drafts.single-offered-value', ['offeredValue' => $offeredValue, 'vehicle' => $vehicle])
                            @endforeach
                        </div>
                        <div class="clearfix"></div>
                        @if (!$vehicle->isVisibleOnMarketplace() and !$vehicle->hasPendingSale() and !$vehicle->hasCompleteSale() and count($offeredValues) > 7)
                            @include('matrix.valuation-drafts.add-to-marketplace-area', ['vehicle' => $vehicle])
                            <div style="margin-bottom: 30px;" class="clearfix"></div>
                        @endif
                    </div>
                @else
                    <div class="divider"></div>
                    <div style="height: 320px; padding: 60px 40px; font-size: 18px; display:none;" class="partner-add-to-marketplace">
                        Our partners haven't provided any offers for your vehicle based on the information provided so far, however with some more information we could get you some offers via our Marketplace.
                        <br><br>
                        We recommend you add images, highlight its full specification and service history to another 912 professional UK buyers currently interested in your vehicle.
                        <div style="margin-top: 20px; text-align: center">
                            <a style="font-size: 20px;" href="{{ route('showVehicleDetails', [$vehicle]) }}" class="button button--orange-bg button--small">Add to Jamjar marketplace</a>
                        </div>
                    </div>
                @endif
                <div class="box__container" style="margin-bottom: 30px;">
                    @if (!$vehicle->isVisibleOnMarketplace() and !$vehicle->hasPendingSale() and !$vehicle->hasCompleteSale())
                        <div class="partner-add-to-marketplace" style="background-repeat: no-repeat; background-position: center bottom;">
                            <div class="add-to-marketplace__left" style="float:left">
                                <div style="font-weight: bold; font-size: 23px;">
                                    Don't see a suitable offer? Add your vehicle to the Jamjar Marketplace
                                </div>
                                <div style="margin-top: 6px;">
                                    Offering your vehicle to the rest of our partner network is a great way to get improved offers!
                                </div>
                            </div>
                            <div class="add-to-marketplace__right" style="float:right">
                                <a href="{{ route('matrixValuationDraftsShowAddToMarketplaceForm', $vehicle->id) }}" class="button button--orange-bg">ADD TO MARKETPLACE</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="valuations__buttons show-mobile center-block" style="margin-bottom: 0; margin-top: 10px;">
                <a href="{{ route('matrixValuationDraftsShowEditVehicleForm', $vehicle->id) }}" class="edit-vehicle-button button button--grey-bg ">
                    Edit Details
                </a>
            </div>
        </div>
    </div>
</section>
@endsection
