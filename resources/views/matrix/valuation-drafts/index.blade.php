<?php

$page = isset($page) ? $page : 'drafts';
$showSearchBox = isset($showSearchBox) ? $showSearchBox : true;
$pageTitle = isset($pageTitle) ? $pageTitle : 'Partner Lookups';
$pageDescription = isset($pageDescription) ? $pageDescription : 'Saved searches';
$highlightVehiclesWithValuations = isset($highlightVehiclesWithValuations) ? $highlightVehiclesWithValuations : false;
$dateLabel = isset($dateLabel) ? $dateLabel : 'Listed Date';
$viewButtonText = isset($viewButtonText) ? $viewButtonText : 'VIEW DETAILS';
$showOfferTypes = isset($showOfferTypes) ? $showOfferTypes : false;
$offersType = isset($offersType) ? $offersType : null;

?>

@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => $pageTitle, 'showLookupForm' => !$showSearchBox])

@push('scripts-after')
    <script type="text/javascript">
        $(function() {
            $(".offers-types .iCheck-helper").click(function() {
                $('.search-form').submit();
            });
        });

        $(document).ready(function(){
            $('.remove').click(function(e){
                e.preventDefault();
                swal({
                    title: 'Remove vehicle from Marketplace?',
                    text: 'You are about to remove the vehicle from Jamjar Marketplace. Your vehicle will no longer be visible to the rest of our partner network' +
                        'and your marketplace seller"s fee will not be refunded.',
                    type: 'warning',
                    buttons:true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = $(this).attr("href");
                    }
                })
            });
        });


    </script>
@endpush

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => $page])
        </div>
        <div class="dashboard__col">
            @if ($showSearchBox)
                <div class="box box--with-header box--no-margin">
                    <div class="box__header ">Enter registration details</div>
                    <div class="box__container">
                        <div class="box__content" style="display: flex; margin: 50px auto;">
                            <form style="" class="numberplate js-numberplate-form" method="post" action="{{ route('matrixValuationDraftsCreateVehicle') }}">
                                {{csrf_field()}}
                                <label style="color: #555555; font: 400 16px Poppins, sans-serif; font-weight: bold; text-transform: none; text-align: center;" for="vrm" class="form__label">Enter the registration of the vehicle you want to view valuations for</label>
                                <div style="text-align: center; position:relative;">
                                    <input class="numberplate__input" type="text" id="vrm" name="numberplate" placeholder="ENTER REG" value="{{ request('numberplate') }}">
                                    <button style="display: block;"><i class="fa fa-angle-right"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif

            <div class="box box--with-header {{ !$showSearchBox ? 'box--no-margin' : '' }}">
                <div class="box__header header-theme-bidding">{{ $pageDescription }}</div>
                <div class="box__container">
                    <div class="box__content box__with_filtering">
                        <div>
                            <div style="font-weight: bold;">Search by registration</div>
                            <form method="get" style="margin: 3px 0 15px 0;" class="search-form">
                                <div class="col-4">
                                    <input class="form__input" type="text" name="searched-numberplate" placeholder="Enter registration number..." style="width: 250px; margin-right: 12px;" value="{{ request('searched-numberplate') }}">
                                    <input class="button button--orange-bg" type="submit" value="Search" style="margin-top: 0; width: 110px;">
                                </div>
                                @if ($showOfferTypes)
                                    <div class="col-6 offers-types" style="margin: 10px 10px 20px 0;">
                                        <span style="margin-right: 15px;">
                                            <input type="radio" name="offers-type" value="open" {{ $offersType == 'open' ? 'checked="checked"' : '' }}> Open offers
                                        </span>
                                        <span style="margin-right: 15px;">
                                            <input type="radio" name="offers-type" value="expired" {{ $offersType == 'expired' ? 'checked="checked"' : '' }}> Expired offers
                                        </span>
                                    </div>
                                @endif
                            </form>
                        </div>

                        <table class="lookups__table jj_responsive_table partner_lookups_table">
                            <thead>
                            <tr>
                                <th>{{ $dateLabel }}</th>
                                <th>Registration</th>
                                <th>Car</th>
                                <th>Highest valuation</th>
                                <th style="min-width: 160px;"></th>
                            </tr>
                            </thead>
                            @foreach ($vehicles as $vehicle)
                                <?php /** @var JamJar\Vehicle $vehicle */ ?>
                                <?php $highlightVehicle = ($highlightVehiclesWithValuations and $vehicle->hasActiveValuations()); ?>
                                <tr class="{{ $highlightVehicle ? 'highlighted' : '' }}">
                                    <td>{{ $vehicle->getAddedToMarketplaceAt() ? $vehicle->getAddedToMarketplaceAt()->format('d/m/Y H:i:s') : $vehicle->getCreatedAt()->format('d/m/Y H:i:s') }}</td>
                                    <td>{{ $vehicle->getNumberPlate() }}</td>
                                    <td>{{ $vehicle->getFormattedName() }}</td>
                                    <td class="desktop-centered">
                                        @if ($highlightVehicle)
                                            <div class="highlighted-info-container">
                                                <span class="highlighted-info"><b>NEW OFFER!</b></span>
                                            </div>
                                        @endif
                                        &pound;{{ number_format($vehicle->getHighestValue()) }}
                                    </td>
                                    <td style="min-width: 160px;">
                                        <a class="button button--orange-bg button--small lookups-buttons" href="{{ route('matrixValuationDraftsShow', [$vehicle->id]) }}">{{ $viewButtonText }}</a>
                                        @if($vehicle->is_visible_on_marketplace)
                                            <a class="remove button button--red-bg button--small lookups-buttons" href="{{ route('removeFromMarketplace', $vehicle->id) }}">REMOVE FROM MARKETPLACE</a>
                                        @else
                                            <a class="button button--green-bg button--small lookups-buttons" href="{{ route('matrixValuationDraftsShowAddToMarketplaceForm', $vehicle->id) }}">ADD TO MARKETPLACE</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="pagination" style="display: block; text-align: center;">
                {{ $vehicles->appends(request()->query())->links('vendor.pagination.matrix-default') }}
            </div>
        </div>
    </div>
</section>
@endsection

