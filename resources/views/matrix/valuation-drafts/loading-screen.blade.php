@push('scripts-after')
    <script>
        $('#drafts-form').on('submit', function(e) {
            $('.loader').show();
        });
    </script>
@endpush
<div class="loader" style="display: none;">
    <div class="loader__box">
        <div class="loader__content">
            <i class="fa fa-car"></i>
            <h3>Just a moment, we're collecting your offers</h3>
        </div>
    </div>
</div>