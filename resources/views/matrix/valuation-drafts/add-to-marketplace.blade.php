@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Add to marketplace', 'showLookupForm' => false])

@if ($errors->any())
    <div class="jamjar__container--small">
        <div class="alert alert--danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

<?php /** @var \JamJar\Vehicle $vehicle */ ?>
<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'drafts'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header  box--no-margin">
                <div class="box__container">
                    <br />
                    <p style="font-weight: bold; font-size: 23px;">
                        Improving your vehicles offers
                    </p>
                    <br />

                    @include('matrix.valuation-drafts.additional-vehicle-details.images')

                    <div class="box__content">
                        <form class="form" method="post" id="drafts-form" action="{{ route('matrixValuationDraftsAddToMarketplace', [$vehicle->id]) }}">
                            <div class="form__group form__group--half form__group--improve-offer @if ($errors->has('asking-price-in-pounds')) form--errors @endif">
                                <label for="asking-price-in-pounds" class="form__label">Asking price (&pound;)*</label>
                                <input min="1" data-fee-calculator-url="{{ route('matrixCalculateFeeForSeller', [$vehicle->id]) }}" data-type="separator-format" name="asking-price-in-pounds" type="text" class="form__input update-fee" value="{{ old('asking-price-in-pounds') ?? $vehicle->getAskingPriceInPence() / 100 }}">
                            </div>

                            @include('matrix.valuation-drafts.additional-vehicle-details.key-count')
                            @include('matrix.valuation-drafts.additional-vehicle-details.service-history-information')
                            @include('matrix.valuation-drafts.additional-vehicle-details.additional-specification-information')
                            @include('matrix.valuation-drafts.additional-vehicle-details.is-log-book')

                            <div class="form__group form__group--half form__group--improve-offer @if ($errors->has('is-in-stock')) form--errors @endif">
                                @include('matrix.valuation-drafts.additional-vehicle-details.is-in-stock')

                                <div class="clearfix"></div>
                                @if (!$vehicle->hasSale())
                                    <div style="font-weight: bold; margin-top: 20px;">
                                        A fee of <span id="fee-value" style="color: orange">&pound;{{ number_format($sellerFeeInPence / 100, 2) }}</span> will be charged for listing this vehicle to the marketplace and will be debited after continuing below.
                                    </div>
                                @endif
                                <div>
                                    <input style="display: none;" id="submit-form" type="submit" class="button button--green-bg button--small" value="LIST ON THE MARKETPLACE">
                                    <label for="submit-form" class="button button--green-bg" style="width: 100%; height: 50px;">LIST ON THE MARKETPLACE</label>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('matrix.valuation-drafts.loading-screen')

@endsection
