<?php /** @var \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface $offeredValue */ ?>
@if ($offeredValue->hasBuyerEnoughFunds())
    @if ($offeredValue->isExpired())
        <a class="button button--grey-bg button--block" href="#" onclick="event.preventDefault();">OFFER EXPIRED</a>
    @else
        @if ($isOfferAcceptable)
            @if ($offeredValue instanceof \JamJar\Model\ValuationDraft)
                <a class="button button--orange-bg button--block" href="{{ route('matrixValuationDraftsShowDraftAcceptForm', [$offeredValue->getId()]) }}">ACCEPT OFFER</a>
            @elseif ($offeredValue instanceof \JamJar\Valuation)
                <a class="button button--orange-bg button--block" href="{{ route('matrixValuationDraftsShowValuationAcceptForm', [$offeredValue->getId()]) }}">ACCEPT OFFER</a>
            @endif
        @else
            <a class="button button--grey-bg button--block" href="#" onclick="event.preventDefault();">ACCEPT OFFER</a>
        @endif
    @endif
@else
    This valuation is for reference only and is not available for acceptance.
@endif