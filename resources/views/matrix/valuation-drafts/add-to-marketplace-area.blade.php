<?php

$title = isset($title) ? $title : 'Don\'t see a suitable offer? Why not add your car to our marketplace';
$description = isset($description) ? $description : 'Increase your selling potential by listing this vehicle on the marketplace!';
$buttonText = isset($buttonText) ? $buttonText : 'ADD TO MARKETPLACE';
$repeatBackground = isset($repeatBackground) ? $repeatBackground : true;
$route = isset($route) ? $route : route('matrixValuationDraftsShowAddToMarketplaceForm', $vehicle->id);

?>

<div class="partner-add-to-marketplace" style="{{ !$repeatBackground ? 'background-repeat: no-repeat; background-position: center bottom;' : '' }}">
  <div class="add-to-marketplace__left">
      <div style="font-weight: bold; font-size: 23px;">
          {{ $title }}
      </div>
      <div style="margin-top: 6px;">
          {{ $description }}
          <br>
          <br>
          {{ $descriptionLine2 }}
      </div>
  </div>
  <div class="add-to-marketplace__right" style="margin-top: 20px;">
      <a style="font-size: 20px;" href="{{ $route }}" class="button button--orange-bg button--small">{{ $buttonText }}</a>

  </div>
    @if(isset($customerRemoveMarketplace) && $customerRemoveMarketplace && $vehicle->is_visible_on_marketplace==1)
        <a style="font-size: 9px;" href="{{route('removeFromMarketplaceCustomer',$vehicle->id)}}" class="remove">Remove from Marketplace</a>
    @endif

</div>
