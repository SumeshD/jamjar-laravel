<?php /** @var \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface $offeredValue */ ?>
<?php /** @var \JamJar\Vehicle $vehicle */ ?>
<div class="valuation row partner-add-to-marketplace-row">
    <div class="valuation__image" style="position:relative;">
        @if ($vehicle->isVisibleOnMarketplace() || $vehicle->hasSale())
            @if ($offeredValue->getSource() == \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VALUE_SOURCE_EXTERNAL)
                <img src="{{ secure_asset('/images/api-partners/'.strtolower($offeredValue->getBuyerCompany()->name).'/logo.jpg') }}" alt="">
            @elseif ($offeredValue->getBuyerCompany()->website)
                <img src="{{ substr($offeredValue->getBuyerCompany()->website->logo_image, 0, 4) == 'http' ? '' : '/' }}{{ $offeredValue->getBuyerCompany()->website->logo_image }}" alt="">
            @endif
        @else
            <div class="offered-value-hidden-company-logo" id="company-logo-{{ $offeredValue->getId() }}">
                <img src="/images/blurred_logo.png" alt="">
                <div class="lock-information" style="position: absolute; top: 0px;">
                    <div style="position: absolute; top: 19px; left: 2px;">
                        <img src="/images/icons/padlock-blue.png" />
                    </div>
                    <div style="margin-left: 30px;">
                        Valuations from other partners will remain anonymous until you accept one of their offers.
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="valuation__button">
        @if ($offeredValue->getSource() != \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VALUE_SOURCE_EXTERNAL and $offeredValue->getBuyerCompany()->buy_vehicles_from_other_partners)
            @if ($offeredValue->hasCompleteSale())
                <a class="button button--green-bg button--block" href="#" onclick="event.preventDefault();">OFFER ACCEPTED</a>
            @elseif ($offeredValue->hasRejectedSale())
                <a class="button button--grey-bg button--block" href="#" onclick="event.preventDefault();">CANCELLED</a>
            @elseif ($offeredValue->hasPendingSale())
                <form id="reject-sale-id-{{ $offeredValue->getSale()->id }}" method="post" action="{{ route('partnerRejectSale', $offeredValue->getSale()->id) }}">
                    {{csrf_field()}}
                </form>
                <a style="background-color: #f89a35; border: 1px solid #f89a35" class="button button--orange-bg button--block" href="#" onclick="event.preventDefault(); confirmAction(
                    function() { document.getElementById('reject-sale-id-{{$offeredValue->getSale()->id}}').submit(); },
                    'Any fees that you have paid will not be refunded if you proceed to cancel this offer.',
                    ['Back', 'OK']);
                ">CANCEL OFFER</a>
            @elseif ($vehicle->hasCompleteSale() or $vehicle->hasPendingSale())
                @include('matrix.valuation-drafts.accept-button', ['draft' => $offeredValue, 'isOfferAcceptable' => false])
            @else
                @include('matrix.valuation-drafts.accept-button', ['draft' => $offeredValue, 'isOfferAcceptable' => true])
            @endif
        @else
            <div class="not-acceptable-offer" style="position: relative;">
                <div style="position: absolute; top: 16px;">
                    <img src="/images/icons/exclamation-triangle.png" />
                </div>
                <div style="margin-left: 20px;">
                    This dealer purchases vehicles from private owners only.
                </div>
            </div>
        @endif
    </div>
    <div class="valuation__prices">

        @if ($offeredValue->scrap and $offeredValue->isCollectionAvailable() and $offeredValue->isDropOffAvailable())

            @if ($offeredValue->collection_part)
                <div class="valuation__prices-item valuation__collection">
                    <small>They Collect</small>
                    <span>&pound;{{number_format($offeredValue->collection_new_price)}}</span>
                </div>
            @else
                <div class="valuation__prices-item valuation__collection">
                    <small>You Deliver</small>
                    <span>&pound;{{number_format($offeredValue->dropoff_new_price)}}</span>
                </div>
            @endif

        @else

            @if ($offeredValue->hasCompleteSale())
                <?php /** @var \JamJar\Sale $sale */ $sale = $offeredValue->getSale(); ?>
                <div class="valuation__prices-item valuation__collection">
                    <small>{{ $sale->delivery_method == 'collection' ? 'They Collect' : 'You Deliver' }}</small>
                    <span>&pound;{{ number_format(($sale->price)) }}</span>
                </div>
            @elseif ($offeredValue->isCollectionAvailable() and $offeredValue->isDropOffAvailable())
                <div class="valuation__prices-item valuation__collection">
                    <small>Collection or delivery</small>
                    <span>&pound;{{ number_format(($offeredValue->getPriceInPence() / 100)) }}</span>
                </div>
            @else
                @if ($offeredValue->isCollectionAvailable())
                    <div class="valuation__prices-item valuation__collection">
                        <small>They Collect</small>
                        <span>&pound;{{ number_format(($offeredValue->getPriceInPence() / 100)) }}</span>
                    </div>
                @else
                    <div class="valuation__prices-item valuation__delivery valuation__prices-item">
                        <small>You Deliver</small>
                        <span>&pound;{{ number_format(($offeredValue->getPriceInPence() / 100)) }}</span>
                    </div>
                @endif
            @endif

        @endif


    </div>
    <div class="valuation__list">
        <p class="show-more-info">More Info <i class="fa fa-chevron-down"></i></p>
        <p class="valuation__location">

            @if(!$offeredValue->scrap)
                @if ($offeredValue->shouldBeCollectedFromAnyLocation())
                    Collection: Mainland UK
                @elseif ($offeredValue->shouldBeCollectedFromClientLocation())
                    Collection: From your address
                @elseif ($offeredValue->shouldBeDroppedAtDistance())
                    Distance: <strong>{{$offeredValue->getDistanceToSellerInMiles()}} miles</strong>
                @elseif ($offeredValue->shouldBeDroppedAtLocation())
                    Location: <strong>{{ $offeredValue->getLocation() ? $offeredValue->getLocation()->city : 'Unknown' }}</strong>
                @endif
            @else
                @if($offeredValue->collection_part)
                    @if ($offeredValue->shouldBeCollectedFromAnyLocationScrapPartner())
                        Collection: Mainland UK
                    @elseif ($offeredValue->shouldBeCollectedFromClientLocationScrapPartner())
                        Collection: From your address
                    @endif
                @else
                    @if ($offeredValue->shouldBeDroppedAtDistance())
                        Distance: <strong >{{$offeredValue->getDistanceToSellerInMiles()}} miles</strong>
                    @elseif ($offeredValue->shouldBeDroppedAtLocation())
                        Location: <strong>{{ $offeredValue->getLocation() ? $offeredValue->getLocation()->city : 'Unknown' }}</strong>
                    @endif
                @endif
            @endif

        </p>

        <ul class="valuation__ticklist" style="width: 100%;">
            <li class="u-text-green">
                <i class="fa fa-check"></i> {{$offeredValue->getValuationValidityInDays()}} Days Quote Guarantee
            </li>

            @if (!$offeredValue->scrap)

                @if ($offeredValue->isCollectionAvailable())
                    @if ($offeredValue->getCollectionFeeInPence() === 0)
                        <li class="u-text-green">
                            <i class="fa fa-check"></i> Free Collection
                        </li>
                    @else
                        <li class="u-text-orange">
                            <i class="fa fa-info-circle"></i> &pound;{{ number_format($offeredValue->getCollectionFeeInPence() / 100) }} Collection Fee
                        </li>
                    @endif
                @endif

                @if ($offeredValue->isDropOffAvailable())
                    @if (($offeredValue->getDropOffFeeInPence() > 0))
                        <li class="u-text-orange">
                            <i class="fa fa-info-circle"></i> &pound;{{ number_format($offeredValue->getDropOffFeeInPence() / 100) }} Dropoff Fee
                        </li>
                    @endif
                @endif

            @else

                @if($offeredValue->collection_part)

                    @if($offeredValue->collection_part)

                        @if ($offeredValue->getCollectionFeeForCTB() === 0)
                            <li class="u-text-green">
                                <i class="fa fa-check"></i> Free Collection
                            </li>
                        @else
                            <li class="u-text-orange">
                                <i class="fa fa-info-circle"></i> &pound;{{ number_format($offeredValue->getCollectionFeeForCTB()) }} Collection Fee
                            </li>
                        @endif

                    @endif

                @endif

            @endif



            @if ($offeredValue->getSource() !== \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VALUE_SOURCE_EXTERNAL)
                @if ((!$offeredValue->getBankTransferFeeInPence()))
                    <li class="u-text-green">
                        <i class="fa fa-check"></i> Free Bank Transfer
                    </li>
                @else
                    <li class="u-text-green">
                        <i class="fa fa-check"></i> Offers Bank Transfer
                    </li>
                    <li class="u-text-orange">
                        <i class="fa fa-info-circle"></i> &pound;{{ number_format($offeredValue->getBankTransferFeeInPence() / 100) }} Bank Transfer Fee
                    </li>
                @endif
            @else
                @if ($offeredValue->getPaymentMethod() == \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::PAYMENT_METHOD_BANK_TRANSFER)
                    @if ((!$offeredValue->getBankTransferFeeInPence()))
                        <li class="u-text-green">
                            <i class="fa fa-check"></i> Free Bank Transfer
                        </li>
                    @else
                        <li class="u-text-green">
                            <i class="fa fa-check"></i> Offers Bank Transfer
                        </li>
                        <li class="u-text-orange">
                            <i class="fa fa-info-circle"></i> &pound;{{ number_format($offeredValue->getBankTransferFeeInPence() / 100) }} Bank Transfer Fee
                        </li>
                    @endif
                @else
                    <li class="u-text-green">
                        <i class="fa fa-check"></i> Payment via {{ ucwords($offeredValue->getPaymentMethod()) }}
                    </li>
                @endif
            @endif
            @if ($offeredValue->getAdminFeeInPence() > 0)
                <li class="u-text-orange">
                    <i class="fa fa-info-circle"></i> &pound;{{ number_format($offeredValue->getAdminFeeInPence() / 100) }} Admin Fee
                </li>
            @else
                <li class="u-text-green">
                    <i class="fa fa-check"></i> No Admin Fee
                </li>
            @endif
        </ul>
    </div>
</div>
