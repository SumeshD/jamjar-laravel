@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Partner Lookups', 'showLookupForm' => false])

@if ($errors->any())
    <div class="jamjar__container--small">
        <div class="alert alert--danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

<?php $isEditMode = isset($isEditMode) ? $isEditMode : false; ?>

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'drafts'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header  box--no-margin">
                <div class="box__header">Enter Vehicle Details</div>
                <div class="box__container">
                    <br />
                    <h1 class="box__title box__title--mob-hide">Vehicle Details</h1>
                    <div class="box__content">
                        <div class="box__logo" style="background-image: url({{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png);">
                            {{-- <img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}"> --}}
                        </div>
                        <h2 class="box__car-title">{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</h2>
                        <ul>
                            <li>Derivative:</li>
                            <li>{{$vehicle->meta->derivative}}</li>
                            <li>Plate:</li>
                            <li>{{$vehicle->meta->plate_year}}</li>
                            <li>Registration:</li>
                            <li>{{$vehicle->numberplate}}</li>
                            <li>Previous Owners:</li>
                            <li>{{trans('vehicles.owners_count_external.owners_'.($vehicle->meta->number_of_owners <= 5 ? $vehicle->meta->number_of_owners : 6)) }}</li>
                        </ul>
                    </div>
                    <h1 class="box__title box__title--mob-hide">About This Vehicle</h1>
                    <div class="box__content">
                        <form class="form" id="drafts-form" method="post" action="{{ $isEditMode ? route('matrixValuationDraftsEditVehicle', [$vehicle->id]) : route('matrixValuationDraftsSaveVehicleInformation', [$vehicle->id]) }}">
                            <div class="form__group form__group--half @if ($errors->has('mileage')) form--errors @endif">
                                <label for="mileage" class="form__label">Mileage</label>
                                <input min="1000" data-type="separator-format" name="mileage" type="text" id="mileage" class="form__input" value="{{ old('mileage') ? old('mileage') : $vehicle->meta->getOriginal('mileage') }}">
                            </div>

                            <div class="form__group form__group--half @if ($errors->has('service-history')) form--errors @endif">
                                <label for="service-history" class="form__label">Service History</label>
                                <select name="service-history" id="service-history" class="form__input">
                                    <option selected disabled>Please Select</option>
                                    @foreach ($serviceHistories as $serviceHistory)
                                        <option {{ (old('service-history') == $serviceHistory or $vehicle->meta->getOriginal('service_history') == $serviceHistory) ? 'selected' : '' }} value="{{$serviceHistory}}">
                                            {{ trans("vehicles.service_history." . $serviceHistory) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form__group form__group--half @if ($errors->has('mot')) form--errors @endif">
                                <label for="mot" class="form__label">MOT</label>
                                <select name="mot" id="mot" class="form__input">
                                    <option disabled selected>Please Select</option>

                                    @foreach ($mots as $mot)
                                        <option {{ (old('mot') == $mot or $vehicle->meta->getOriginal('mot') == $mot) ? 'selected' : '' }} value="{{ $mot }}">
                                            {{ trans("vehicles.mot." . $mot) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form__group form__group--half @if ($errors->has('car-colour')) form--errors @endif">
                                <label for="car-colour" class="form__label">Car Colour</label>
                                <select name="car-colour" id="car-colour" class="form__input">
                                    <option value="" disabled></option>
                                    @foreach ($colors as $color)
                                        @if ($vehicle->meta->color == $color->id)
                                            <option value="{{ $color->id }}" selected>{{ $color->title }}</option>
                                        @else
                                            <option value="{{ $color->id }}">{{ $color->title }}</option>
                                        @endif
                                    @endforeach
                                    <option value="other">My colour isn't listed</option>
                                </select>
                            </div>

                            <div class="form__group @if ($errors->has('write-off')) alert alert--danger @endif">
                                <div class="form__radio">
                                    <label for="writeoff">Has your vehicle ever been subject to an insurance write-off?</label>
                                    <div class="form__radioinput">
                                        <input type="radio" name="write-off" id="writeoff" class="writeoff-no" value="0" @if((!old('write-off')) and !$vehicle->meta->write_off) checked @endif> <span data-id="no" class="js-toggle-writeoff">No</span>
                                        <input type="radio" name="write-off" id="writeoff" class="writeoff-yes" value="1" @if((old('write-off')) or $vehicle->meta->write_off) checked @endif> <span data-id="yes" class="js-toggle-writeoff">Yes</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form__group form__group--writeoff {{ ((old('write-off')) or $vehicle->meta->write_off) ? ' u-show ' : ' u-hide ' }}">
                                <div class="form__group form__group @if ($errors->has('write_off_category')) form--errors @endif">
                                    <br />
                                    <select name="write-off-category" id="write_off_category" class="form__input">
                                        <option disabled>Please Select</option>
                                        <option value="none" @if($vehicle->meta->write_off_category == "none") selected @endif>None</option>
                                        <option value="N" @if($vehicle->meta->write_off_category == "N") selected @endif>Cat N / Cat D (Can be repaired following non-structural damage)</option>
                                        <option value="S" @if($vehicle->meta->write_off_category == "S") selected @endif>Cat S / Cat C (Can be repaired following structural damage)</option>
                                        <option value="all" @if($vehicle->meta->write_off_category== "all") selected @endif>All</option>
                                    </select>
                                    <div class="divider"></div>
                                </div>
                            </div>

                            <div class="form__group @if ($errors->has('non-runner')) alert alert--danger @endif">
                                <div class="form__radio">
                                    <label for="nonrunner">Is your vehicle a non-runner?</label>
                                    <div class="form__radioinput">
                                        <input type="radio" name="non-runner" id="nonrunner" class="nonrunner-no" value="0" {{ (!old('non-runner') or !$vehicle->meta->non_runner) ? 'checked' : '' }}> <span data-id="no" class="js-toggle-nonrunner">No</span>
                                        <input type="radio" name="non-runner" id="nonrunner" class="nonrunner-yes" value="1" {{ (old('non-runner') or $vehicle->meta->non_runner) ? 'checked' : '' }}> <span data-id="yes" class="js-toggle-nonrunner">Yes</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form__group form__group--defects {{ (old('non-runner') or $vehicle->meta->non_runner) ? ' u-show ' : ' u-hide ' }}">
                                <p>Please select all mechanical defects which best describe why your vehicle is a non-runner</p>
                                @php
                                    $selectedDefects = [];
                                    if (old('non-runner-reason')) {
                                        foreach (old('non-runner-reason') as $reasonId => $flag) {
                                            if ($flag == 'on') {
                                                $selectedDefects[] = $reasonId;
                                            }
                                        }
                                    } elseif($errors->has('non-runner')) {
                                        $selectedDefects = [];
                                    } elseif ($vehicle->meta->non_runner_reason) {
                                        foreach ($vehicle->meta->non_runner_reason as $selectedDefect) {
                                            $selectedDefects[] = $selectedDefect->id;
                                        }
                                    }
                                @endphp
                                @foreach ($defects as $defect)
                                    <div class="form__checkbox">
                                        <label for="defects[{{$defect->id}}]">
                                            <input type="checkbox" id="non_runner_reason[{{$defect->id}}]" name="non-runner-reason[{{$defect->id}}]" {{in_array($defect->id, $selectedDefects) ? 'checked' : ''}}>
                                            <span>{{$defect->title}}</span>
                                        </label>
                                    </div>
                                @endforeach
                            </div>

                            <div class="form__submit">
                                <input type="submit" value="View valuations">
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('matrix.valuation-drafts.loading-screen')

@endsection
