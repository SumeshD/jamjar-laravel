<?php /** @var JamJar\Vehicle $vehicle */ ?>

@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Partner Lookups', 'showLookupForm' => false])

@if ($errors->any())
    <div class="jamjar__container--small">
        <div class="alert alert--danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

<?php /** @var \JamJar\Vehicle $vehicle */ ?>
<?php /** @var \JamJar\User $sellerUser */ ?>
<?php /** @var \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface $offeredValue */ ?>
<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container offered-value-accept">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'drafts'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header box--no-margin">
                <div class="box__header">Accept offer</div>
                <div class="box__container partial-header">
                    <p style="font-weight: bold; font-size: 23px;">
                        Your details
                    </p>
                </div>
                <div class="box__container seller-information">
                    <div class="col-6 col-lg-6">
                        <div class="user-avatar"><img style="width: 100%; height: 100%;" src="/images/associates/avatar.png" /></div>
                        <div class="entity-name">{{ $sellerUser->getFirstNameAttribute() . ' ' . $sellerUser->getLastNameAttribute() }}</div>
                        <div>
                            <?php $sellerProfile = $sellerUser->getProfile(); ?>
                            <div><span class="details-label">Address line 1:</span> {{ $sellerProfile->getAddressLineOne() }}</div>
                            <div><span class="details-label">Address line 2:</span> {{ $sellerProfile->getAddressLineTwo() }}</div>
                            <div><span class="details-label">Town or City:</span> {{ $sellerProfile->getCity() }}</div>
                            <div><span class="details-label">Postcode:</span> {{ $sellerProfile->getPostcode() }}</div>
                            <div><span class="details-label">mobile number:</span> {{ $sellerProfile->getMobileNumber() }}</div>
                            <div><span class="details-label">email:</span> {{ $sellerProfile->getEmail() }}</div>
                        </div>
                    </div>
                    <div class="col-6 col-lg-6">
                        <div class="vehicle-icon">
                            <img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}">
                        </div>
                        <div class="entity-name">{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</div>
                        <div>
                            <?php $vehicleMeta = $vehicle->getMeta(); ?>
                            <div><span class="details-label">Mileage:</span> {{ $vehicleMeta->getMileage() }}</div>
                            <div><span class="details-label">Previous owners:</span> {{trans('vehicles.owners_count_external.owners_'.($vehicleMeta->getNumberOfOwners() <= 5 ? $vehicleMeta->getNumberOfOwners() : 6)) }}</div>
                            <div><span class="details-label">Service:</span> {{ $vehicleMeta->getServiceHistory() }}</div>
                            <div><span class="details-label">MOT:</span> {{ $vehicleMeta->getMot() }}</div>
                            <div><span class="details-label">Write off:</span> {{ $vehicleMeta->isWriteOff() ? 'Yes' : 'No'}}</div>
                            <div><span class="details-label">Non runner:</span> {{ $vehicleMeta->isNonRunner() ? 'Yes' : 'No'}}</div>
                        </div>
                    </div>
                </div>
                <div class="box__container partial-header">
                    <p style="font-weight: bold; font-size: 23px;">
                        Additional details
                    </p>
                </div>
                <form class="form" method="post">
                    <div class="box__container" style="margin-top: 25px;">

                            @include('matrix.valuation-drafts.additional-vehicle-details.service-history-information')
                            @include('matrix.valuation-drafts.additional-vehicle-details.additional-specification-information')
                            @include('matrix.valuation-drafts.additional-vehicle-details.key-count')
                            <div class="form__group form__group--half @if ($errors->has('is-in-stock')) form--errors @endif">
                                @include('matrix.valuation-drafts.additional-vehicle-details.is-in-stock')
                            </div>
                            @include('matrix.valuation-drafts.additional-vehicle-details.is-log-book')
                    </div>
                    <div class="box__container partial-header" style="margin-top:20px;">
                        <p style="font-weight: bold; font-size: 23px;">
                            Final price <span style="color: #7c7c7c; font-size: 19px;">(Any admin fees have been subtracted from the price shown)</span>
                        </p>
                    </div>
                    <div class="box__container {{ ($vehicle->meta->write_off==0 && $vehicle->meta->non_runner==0) ? 'condition-select-active' : 'condition-select-inactive' }}" style="margin-top: 25px;">
                        <div class="prices-box">
                            @if ($vehicle->meta->write_off==0 && $vehicle->meta->non_runner==0)
                                <div class="condition-box">
                                    <div class="additional-fields-description">Adjust valuation based on your vehicle condition</div>
                                    <div>
                                        <div id="condition-slider">
                                            <input type="text" id="appraisalSlider" name="appraisalSlider" class="appraisalSlider">
                                        </div>
                                        <div class="u-text-center">
                                            <div class="not-accepted-condition-message alert alert-danger" style="display: none; width: 85%; margin: 16px;"></div>
                                        </div>
                                    </div>
                                    <div class="additional-fields-description">Provide details of your vehicle's <span style="text-transform: lowercase;" class="condition-name">great</span> condition</div>
                                    <div class="additional-info-condition">
                                        <textarea name="condition-information" placeholder="e.g. One or two ding dents, scuffed allow wheels or light scratch."></textarea>
                                    </div>
                                </div>
                            @endif
                            <div class="delivery-method-box {{ (!$offeredValue->isDropOffAvailable() or !$offeredValue->isDropOffAvailable()) ? 'single' : '' }}" >
                                @if ($offeredValue->isDropOffAvailable())
                                    <div class="matrix-content__col">
                                        <div class="button button--matrix-big is-active" id="dropoff">
                                            <div class="checked-mark">✔</div>
                                            You Deliver
                                            <?php $priceGreat = $offeredValue->getFinalDropOffPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_GREAT) / 100 ?>
                                            <?php $priceGood = $offeredValue->getFinalDropOffPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_GOOD) / 100 ?>
                                            <?php $priceFair = $offeredValue->getFinalDropOffPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_FAIR) / 100 ?>
                                            <?php $pricePoor = $offeredValue->getFinalDropOffPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_POOR) / 100 ?>
                                            <p class="price is-active" id="dropGreat" data-price="{{$priceGreat}}">
                                                &pound;{{number_format($priceGreat)}}
                                            </p>
                                            <p class="price u-hide" id="dropGood" data-price="{{$priceGood}}">
                                                &pound;{{number_format($priceGood)}}
                                            </p>
                                            <p class="price u-hide" id="dropFair" data-price="{{$priceFair}}">
                                                &pound;{{number_format($priceFair)}}
                                            </p>
                                            <p class="price u-hide" id="dropPoor" data-price="{{$pricePoor}}">
                                                &pound;{{number_format($pricePoor)}}
                                            </p>
                                        </div>
                                    </div>
                                @endif
                                @if ($offeredValue->isCollectionAvailable())
                                    <div class="matrix-content__col">
                                        <div class="button button--matrix-big @if (!$offeredValue->isDropOffAvailable()) is-active @endif" id="collection">
                                            <div class="checked-mark">✔</div>
                                            We Collect
                                            <?php $priceGreat = $offeredValue->getFinalCollectionPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_GREAT) / 100 ?>
                                            <?php $priceGood = $offeredValue->getFinalCollectionPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_GOOD) / 100 ?>
                                            <?php $priceFair = $offeredValue->getFinalCollectionPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_FAIR) / 100 ?>
                                            <?php $pricePoor = $offeredValue->getFinalCollectionPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_POOR) / 100 ?>
                                            <p class="price is-active" id="collGreat" data-price="{{$priceGreat}}">
                                                &pound;{{number_format($priceGreat)}}
                                            </p>
                                            <p class="price u-hide" id="collGood" data-price="{{$priceGood}}">
                                                &pound;{{number_format($priceGood)}}
                                            </p>
                                            <p class="price u-hide" id="collFair" data-price="{{$priceFair}}">
                                                &pound;{{number_format($priceFair)}}
                                            </p>
                                            <p class="price u-hide" id="collPoor" data-price="{{$pricePoor}}">
                                                &pound;{{number_format($pricePoor)}}
                                            </p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="submit-box" style="">
                            {{csrf_field()}}
                            <input type="hidden" id="condition" name="condition" value="great">
                            @if (!$offeredValue->isDropOffAvailable())
                                <input type="hidden" id="deliveryMethod" name="deliveryMethod" value="collection">
                            @else
                                <input type="hidden" id="deliveryMethod" name="deliveryMethod" value="dropoff">
                            @endif
                            <div style="margin-bottom: 10px;">
                                <input id="submit-form" type="submit" class="button button--green-bg" value="ACCEPT OFFER">
                            </div>
                            @if ((!$vehicle->isVisibleOnMarketplace() and !$vehicle->hasSale()))
                                <div style="font-weight: bold; font-size: 13px;"><img style="width: 15px; height: 15px; margin: 3px;" src="/images/icons/exclamation-circle.png" /> If you accept this offer you will be charged a seller's lead fee of <span style="color: #ec8913;">&pound;{{ number_format($feeInPence / 100, 2) }}</span></div>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@include('matrix.valuation-drafts.loading-screen')

@push('scripts-after')
    <script type="text/javascript">

    </script>
    <style type="text/css">
        .offered-value-accept .prices-box {
            background-color: #f7fbff;
            min-height: 300px;
            float: left;
            width: 100%;
            padding:20px;
        }

        .offered-value-accept .submit-box {
            width: 50%;
            float: right;
            text-align: right;
            padding-bottom: 30px;
            margin-top: 30px;
            text-align: right;
        }

        @media screen and (max-width: 600px) {
          .offered-value-accept .submit-box {
              width: 100% !important;
              text-align: center !important;
          }
        }

        @media screen and (max-width: 600px) {
          .offered-value-accept .submit-box #submit-form{
            min-width: 300px;  
          }
        }

        @media screen and (max-width: 600px) {
          .offered-value-accept .condition-select-inactive .prices-box {
              padding-bottom: 5% !important;
          }
        }


        .offered-value-accept .condition-select-inactive .prices-box {
            min-height: 240px;
            height: auto;
            float: left;
            width: 100%;
            padding-bottom: 0px;
            margin-top: 0px;
        }

        @media screen and (max-width: 600px) {
          #stock-availability-at-box-wrapper{
            margin-top: 20px !important;
          }
        }

        #stock-availability-at-box-wrapper{
          margin-top: 0px;
        }

        .offered-value-accept .condition-select-inactive .delivery-method-box {
            width: 100%;
        }

        @media screen and (max-width: 600px) {
          .offered-value-accept .delivery-method-box {
              width: 100% !important;
          }
        }

        .offered-value-accept .delivery-method-box {
            float: right;
            width: 28%;
            margin-bottom: 2%;
        }

        @media screen and (max-width: 600px) {
          .offered-value-accept .condition-box {
              width: 100% !important;
          }
        }

        .offered-value-accept .condition-box {
            float: left;
            width: 70%;
        }

        .offered-value-accept .entity-name {
            margin: 10px 0;
            font-weight: bold;
            font-size: 19px;
        }

        .offered-value-accept .matrix-content__col {
            position: relative;
            width: 100%;
        }

        .offered-value-accept .condition-select-inactive .single .matrix-content__col {
            width: 40%;
            margin: 20px auto;
            float: none;
        }

        @media screen and (max-width: 600px) {
          .offered-value-accept .condition-select-inactive .matrix-content__col {
              width: 90% !important;
              margin: 5% 5% 0% 5% !important;
              float: left;
          }
        }

        .offered-value-accept .condition-select-inactive .matrix-content__col {
            width: 40%;
            margin: 5%;
            float: left;
        }

        .offered-value-accept .condition-select-inactive .button--matrix-big {
            padding: 40px 0 ;
        }

        .offered-value-accept .checked-mark {
            font-size: 25px;
            background-color: white;
            color: #2ac229;
            border-radius: 15px;
            width: 30px;
            height: 30px;
            position: absolute;
            top: 40%;
            left: -13px;
            font-family: 'FontAwesome', sans-serif;
            text-transform: none;
            font-weight: 400;
            display: none;
        }

        .offered-value-accept .button--matrix-big.is-active .checked-mark {
            display: block;
        }

        .offered-value-accept .button--matrix-big::after {
            display: none;
        }

        .offered-value-accept .button--matrix-big.is-active {
            background-color: #c9ebc6;
            color: #79d677;
            border: 1px solid #d3e7d5;
        }

        .offered-value-accept .button--matrix-big {
            background-color: white;
            color: #b4b4b4;
            padding: 15px 0;
            border: 1px solid white;
        }

        .offered-value-accept .button--matrix-big.is-active .price {
            color: #2ac229;
        }

        .offered-value-accept .button--matrix-big .price {
            color: #696969;
        }

        @media screen and (max-width: 600px) {
          .offered-value-accept .additional-fields-description {
              font-weight: bold !important;
              margin: 20px 0px 0px 0px !important;
              text-align: center !important;
          }
        }

        .offered-value-accept .additional-fields-description {
            font-weight: bold;
            margin: 20px 20px 0px 20px;
            text-align: left;
        }

        @media screen and (max-width: 600px) {
          .offered-value-accept .additional-info-condition {
              margin: 20px 0px 20px 0px !important;
          }
        }

        .offered-value-accept .additional-info-condition {
            margin: 13px 25px 0px 22px;
        }

        @media screen and (max-width: 600px) {
          .offered-value-accept .additional-info-condition textarea {
              width: 100% !important;
          }
        }

        .offered-value-accept .additional-info-condition textarea {
            background-color: #eaeaea;
            color: #747474;
            border: 0;
            border-radius: 5px;
            width: 90%;
            padding: 13px;
            font-weight: bold;
        }

        .offered-value-accept .user-avatar {
            width: 100px;
            height: 90px;
            background-color: #f4f4f4;
        }
        .offered-value-accept .vehicle-icon {
            width: 100px;
            height: 90px;
            background-color: #f4f4f4;
        }

        .offered-value-accept .seller-information {
            width: 100%;
            margin: 20px 10px;
            text-transform: uppercase;
        }

        .offered-value-accept .seller-information .details-label {
            font-weight: bold;
        }

        .offered-value-accept .partial-header {
            background-color: #f4f4f4;
            width: 100%;
            padding: 15px 2%;
        }

        #condition-slider .irs {
            margin: 0 20px;
        }

        #condition-slider .irs-bar, #condition-slider .irs-bar-edge {
            background: linear-gradient(to top, #eb8710 0%, #eb8710 100%);
            border: 1px solid #eb8710;
            height: 16px;
        }

        #condition-slider .irs-bar-edge {
            margin: 0 4px;
        }

        #condition-slider .irs-slider {
            background: #fff;
            margin: 3px -1px;
        }

        #condition-slider .irs-grid-pol {
            display: none;
        }

        #condition-slider .irs-grid-text {
            text-transform: uppercase;
            font-size: 14px;
            font-weight: bold;
            margin: 26px;
        }

        #condition-slider .irs-line {
            background: #e6e6e6;
            height: 16px;
            border: 0;
        }
    </style>
@endpush
