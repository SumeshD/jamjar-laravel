@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Edit Location: ' . $location->name , 'buttons' => '<a href="'.route('partnerAccount').'" class="button button--orange-bg u-pull-right"><i class="fa fa-chevron-left"></i> Back to Locations</a>'])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'locations'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header box--no-margin">
                <h1 class="box__header">Locations</h1>
                @if ($errors->any())
                    <div class="box__content">
                        <div class="alert alert-danger col-12">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="box__container">
                    <form action="{{ route('partnerLocationsUpdate', $location) }}" method="POST" class="form">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="box__content">
                        <div class="form__group form__group--half @if ($errors->has('name')) form--errors @endif">
                            <label for="name" class="form__label">Location Name <span class="form__required">*</span></label>
                            <input type="text" class="form__input" id="name" name="name" required value="{{ old('name') ?? $location->name }}">
                        </div>

                        <div class="form__group form__group--half @if ($errors->has('telephone')) form--errors @endif">
                            <label for="telephone" class="form__label">Telephone Number <span class="form__required">*</span></label>
                            <input type="tel" class="form__input" id="telephone" name="telephone" required value="{{ old('telephone') ?? $location->telephone }}">
                        </div>

                        <div class="form__group form__group--half @if ($errors->has('address_line_one')) form--errors @endif">
                            <label for="address_line_one" class="form__label">Address Line One <span class="form__required">*</span></label>
                            <input type="text" class="form__input" id="address_line_one" name="address_line_one" required value="{{ old('address_line_one') ?? $location->address_line_one }}">
                        </div>

                        <div class="form__group form__group--half @if ($errors->has('address_line_two')) form--errors @endif">
                            <label for="address_line_two" class="form__label">Address Line Two</label>
                            <input type="text" class="form__input" id="address_line_two" name="address_line_two" value="{{ old('address_line_two') ?? $location->address_line_two }}">
                        </div>

{{--                        <div class="form__group form__group--half @if ($errors->has('town')) form--errors @endif">--}}
{{--                            <label for="town" class="form__label">Town <span class="form__required">*</span></label>--}}
{{--                            <input type="text" class="form__input" id="town" name="town" required value="{{ old('town') ?? $location->town }}">--}}
{{--                        </div>--}}

                        <div class="form__group form__group--half @if ($errors->has('city')) form--errors @endif">
                            <label for="city" class="form__label">Town or City <span class="form__required">*</span></label>
                            <input type="text" class="form__input" id="city" name="city" required value="{{ old('city') ?? $location->city }}">
                        </div>

                        <div class="form__group form__group--half @if ($errors->has('county')) form--errors @endif">
                            <label for="county" class="form__label">County</label>
                            <input type="text" class="form__input" id="county" name="county" value="{{ old('county') ?? $location->county }}">
                        </div>

                        <div class="form__group form__group--half @if ($errors->has('postcode')) form--errors @endif">
                            <label for="postcode" class="form__label">Postcode <span class="form__required">*</span></label>
                            <input type="text" class="form__input" id="postcode" name="postcode" required value="{{ old('postcode') ?? $location->postcode }}">
                        </div>

                        <div class="form__group">
                            <div class="form__checkbox" style="width: 100%;">
                                <label for="primary_location" class="form__label">
                                    <input type="checkbox" id="primary_location" name="primary_location"  {{ ($location->primary == 1) ? ' checked' : '' }}> <strong>Make Primary Location?</strong>
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="box__footer">
                        <button type="submit" class="button button--big button--orange-bg">Save</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
