+@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'All Locations', 'buttons' => '<a href="'.route('partnerAccount').'" class="button button--orange-bg u-pull-right"><i class="fa fa-plus"></i> Add Location</a>'])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'locations'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header box--no-margin">
                <div class="box__header">Locations</div>
            </div>

            @forelse ($locations as $location)
            <div class="box">
                <article class="location">
                    <h2 class="location__title">{{ $location->name }}</h2>
                    <p>{{$location->plain_address}}</p>
                    @if ($location->primary == 1)
                    <span class="location__primary">This is my main location</span>
                    @endif
                    <p class="location__buttons">
                        <a href="{{ route('partnerLocationsEdit', $location) }}" class="location__button location__button--edit">edit</a>
                        @if (count($locations) > 1)
                            <a
                                onclick="
                                    event.preventDefault();
                                    confirmAction(function() { document.getElementById('delete-location-form-{{$location->id}}').submit(); });
                                "
                                class="location__button location__button--delete"
                            >
                            delete
                            </a>
                        @else
                            <a
                                onclick="
                                        event.preventDefault();
                                        swal({buttons: {'button': {text: 'OK', className: 'button--orange-bg'}}, dangerMode: true, icon: 'warning', title: 'Action not permitted', text: 'You must always have at least one location. To remove this location you must first add another location.'});
                                        "
                                class="location__button location__button--delete"
                            >
                                delete
                            </a>
                        @endif

                        <form action="{{ route('partnerLocationsDelete', $location) }}" method="POST" id="delete-location-form-{{$location->id}}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                    </p>
                </article>  
            </div>
            @empty
            <div class="box">
                <p class="u-text-muted">You haven't added any locations yet.</p>
                <p class="u-text-center">
                    <a href="{{ route('partnerLocationsCreate') }}" class="button button--orange-bg ">
                        <i class="fa fa-plus"></i> Add Location
                    </a>
                </p>
            </div>
            @endforelse
        </div>
    </div>
</section>
@endsection
