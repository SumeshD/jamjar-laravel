@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Statement'])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'credits'])
        </div>
        <div class="dashboard__col">

            <div class="box box--with-header box--no-margin">
                <div class="box__header box__header_dashboard">Statement - All Activity</div>
                <div class="box__container">
                    <div class="box__content box__with_filtering">

                      {{-- <input class="valuations__search" type="text" name="paidLeadsSearch" placeholder="Search"> --}}
                            <table class="valuations__table jj_responsive_table statement_table">
                                <thead>
                                    <tr>
                                        <th width="15%">Date/Time</th>
                                        <th width="50%">Activity</th>
                                        <th width="10%">Your Valuation</th>
                                        <th width="12%">Listing Position/Sale</th>
                                        <th width="13%">Funds Activity (inc. VAT)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($statements as $item)
                                    <tr>
                                        <td>{{ $item->created_at->format('d/m/Y H:i:s') }}</td>
                                        @if ($item['type'] == 'withdrawal')
                                            <td>{{ $item->vehicle['manufacturer'] }} {{ $item->vehicle['model'] }} {{ $item->vehicle['derivative'] }}</td>
                                        @elseif ($item['type'] =='deposit')
                                            <td>Funds Added</td>
                                        @endif
                                        @if ($item['type'] == 'withdrawal')
                                            @if (isset($item['valuation']['dropoff']))
                                                <td>&pound;{{ number_format($item['valuation']['dropoff']) }}</td>
                                            @else
                                                <td>Marketplace</td>
                                            @endif
                                            <td>{{ $item->position }}</td>
                                        @else
                                            <td></td>
                                            <td></td>
                                        @endif
                                        @if ($item['type'] == 'withdrawal')
                                            <td><strong>&minus; &pound;{{ $item->amount }}</strong></td>
                                        @elseif ($item['type'] == 'deposit')
                                            <td><strong class="u-text-green">&plus; &pound;{{ $item->amount }}</strong></td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


@push('scripts-after')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('.valuations__table').DataTable({
        "paging":   true,
        "ordering": false,
        "info":     false,
        "lengthChange": false,
        "pageLength": 50,
        "order": [[ 0, "desc" ]],
        "oLanguage": {
          "sSearch": "",
          "sSearchPlaceholder" : "Search"
        }
    });
} );
</script>
@endpush
@endsection
