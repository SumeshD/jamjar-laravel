@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Dashboard'])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'account'])
        </div>
        <div class="dashboard__col">
            <form method="POST" action="{{ route('partnerAccount') }}" class="form">
                <div class="box box--with-header box--no-margin" style="margin-bottom: 25px;">
                    <div class="box__header">Your Details</div>
                    <div class="box__container">
                        <div class="box__content">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form__group form__group--half {{ $errors->has('name') ? ' form--error' : '' }}">
                                <label for="name" class="form__label">Your Name <span class="form__required">*</span></label>
                                <input
                                        id="name"
                                        type="text"
                                        class="form__input"
                                        name="name"
                                        value="{{ old('name', auth()->user()->profile->name) }}"
                                        placeholder="Enter Your Name"
                                        required
                                >
                                @if ($errors->has('name'))
                                    <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                @endif
                            </div>

                            <div class="form__group form__group--half {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                <label for="mobile_number" class="form__label">Your Mobile Number <span class="form__required">*</span></label>
                                <input
                                        id="mobile_number"
                                        type="tel"
                                        class="form__input"
                                        name="mobile_number"
                                        value="{{ old('mobile_number') ?? $user->profile->mobile_number }}"
                                        placeholder="We only use this to send you text alerts."
                                        required
                                        autofocus
                                >
                                @if ($errors->has('mobile_number'))
                                    <span class="help-block"><strong>{{ $errors->first('mobile_number') }}</strong></span>
                                @endif
                            </div>

                            <div class="form__group form__group--half {{ $errors->has('email') ? ' form--error' : '' }}">
                                <label for="email" class="form__label">E-Mail Address <span class="form__required">*</span></label>
                                <input
                                        id="email"
                                        type="email"
                                        class="form__input"
                                        name="email"
                                        value="{{ old('email', auth()->user()->profile->email) }}"
                                        placeholder="Enter your E-Mail address"
                                        required
                                        autofocus
                                >
                                @if ($errors->has('email'))
                                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box--with-header box--no-margin" style="margin-bottom: 25px;">
                    <div class="box__header">Company Details</div>
                    <div class="box__container">
                        <div class="box__content">
                            <div class="form__group form__group--half {{ $errors->has('company_name') ? ' has-error' : '' }}">
                                <label for="company_name" class="form__label">Company Name</label>
                                <input
                                    id="company_name"
                                    type="text"
                                    class="form__input"
                                    name="company_name"
                                    value="{{ old('company_name') ?? $user->company->name }}"
                                    placeholder="Enter your Company Name"
                                    required
                                    autofocus
                                >
                                @if ($errors->has('company_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form__group form__group--half {{ $errors->has('company_telephone') ? ' has-error' : '' }}">
                                <label for="company_telephone" class="form__label">Company Telephone</label>
                                <input
                                    id="company_telephone"
                                    type="tel"
                                    class="form__input"
                                    name="company_telephone"
                                    value="{{ old('company_telephone') ?? $user->company->telephone }}"
                                    placeholder="Enter your Company Contact Number"
                                    required
                                    autofocus
                                >
                                @if ($errors->has('company_telephone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_telephone') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form__group form__group--half {{ $errors->has('vat_number') ? ' has-error' : '' }}">
                                <label for="vat_number" class="form__label">VAT Number</label>
                                <input
                                    id="vat_number"
                                    type="text"
                                    class="form__input"
                                    name="vat_number"
                                    value="{{ old('vat_number') ?? $user->company->vat_number }}"
                                    placeholder="Enter your VAT Number"
                                    required
                                    autofocus
                                >
                                @if ($errors->has('vat_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('vat_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box--with-header box--no-margin" style="margin-bottom: 25px;">
                    <div class="box__header">Locations</div>
                    <div class="box__container">
                        @forelse ($locations as $location)
                            <div class="box" style="border: 1px solid #e2e2e2; margin: 1em 0; {{ $location->primary == 1 ? 'background-color: #e2e2e2;' : '' }}">
                                <article class="location">
                                    <h2 class="location__title">{{ $location->name }}</h2>
                                    <p>{{$location->plain_address}}</p>
                                    @if ($location->primary == 1)
                                        <span class="location__primary">This is my main location</span>
                                    @else
                                        <span class="location__primary"><a href="#" class="button button--green" onclick="event.preventDefault(); document.getElementById('primary-location-form-{{$location->id}}').submit();">Make main location</a></span>
                                    @endif
                                    <p class="location__buttons">
                                        <a href="{{ route('partnerLocationsEdit', $location) }}" class="location__button location__button--edit">edit</a>
                                        @if (count($locations) > 1)
                                            <a onclick="event.preventDefault();confirmAction(function() { document.getElementById('delete-location-form-{{$location->id}}').submit(); });" class="location__button location__button--delete">
                                                delete
                                            </a>
                                        @else
                                            <a onclick="event.preventDefault(); swal({buttons: {'button': {text: 'OK', className: 'button--orange-bg'}}, dangerMode: true, icon: 'warning', title: 'Action not permitted', text: 'You must always have at least one location. To remove this location you must first add another location.'});" class="location__button location__button--delete">
                                                delete
                                            </a>
                                    @endif
                                    </p>
                                </article>
                            </div>
                        @empty
                            <div class="box">
                                <p class="u-text-muted">You haven't added any locations yet.</p>
                            </div>
                        @endforelse

                        <p class="u-text-right">
                            <a href="{{ route('partnerLocationsCreate') }}" class="button button--orange-bg button--small">
                                <i class="fa fa-plus"></i> Add Location
                            </a>
                        </p>

                    </div>
                </div>

                <p class="form__submit u-text-right" style="border: 0px;">
                    <input value="Save Details" type="submit">
                </p>
            </form>
            @foreach ($locations as $location)
                <form action="{{ route('partnerLocationsDelete', $location) }}" method="POST" id="delete-location-form-{{$location->id}}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>

                <form action="{{ route('partnerLocationsMakePrimary', $location) }}" method="POST" id="primary-location-form-{{$location->id}}">
                    {{ csrf_field() }}
                </form>
            @endforeach
        </div>
    </div>
</section>
@endsection
