@extends('layouts.app')

@section('content')

@include('matrix.partials.header', [
    'title' => 'Edit Rule Details '. ($offer->name ? ' &mdash; <strong>' . $offer->name . '</strong>' : ''),
    'buttons' => '
        <a href="'.route('partnerOffers').'" class="button button--orange-bg u-pull-right" style="margin-left:0.75rem;"> <i class="fa fa-chevron-left"></i> Back to Offers</a>
        <a href="'.route('partnerOfferEditVehicles', $offer).'" class="button button--orange-bg u-pull-right" style="margin-left:0.75rem;"> <i class="fa fa-car"></i> Edit Vehicles</a>
        '
])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'offers'])
        </div>
        <div class="dashboard__col">

          <form action="{{route('partnerOfferUpdate', $offer)}}" id="edit-offer-form" method="POST">
            {{csrf_field()}}
            {{method_field('PATCH')}}

            <div class="box box--with-header box--no-margin">
              <div class="box__header header-theme-marketplace">@if ($offer->name){{ $offer->name }}@else{{ e('Edit Offer Details') }}@endif</div>
              <div class="box__content box__content--with-padding">
                  @include('matrix.offers.partials.offer-edit', $offer)
                  <div class="u-pull-right">
{{--                     <a 
                        href="{{ route('partnerOfferEditVehicles', $offer) }}" 
                        class="button button--small button--grey-bg"
                    >
                        Edit Vehicles
                    </a> --}}
                  </div>
                  <show-hide-vehicles slug="{{$offer->slug}}"></show-hide-vehicles>
              </div>
            </div>

            <div class="offer__section offer__section--centered">
                <p>
                    <button type="submit" name="editVehicles" class="button button--xl button--grey-bg" style="margin-right:2rem;">Add or Remove Vehicles</button>

                    <button type="submit" name="save" class="button button--xl button--orange-transparent-bg" style="margin-left:2rem;">Save Offer</button> 
                </p>
            </div>

          </form>



        </div>
    </div>
</section>

@push('scripts-after')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    var form = $( "#edit-offer-form" );
    form.validate();
    console.log(form.valid());
</script>
@endpush

@endsection
