<div class="dashboard__breadcrumbs">
    <div class="dashboard__crumb @if ($active == 'step1') dashboard__crumb--current @endif ">
        <a href="{{ route('partnerOfferCreate') }}">
            <div class="circle">
                <span>1</span>
            </div>
            <strong class="dashboard__crumb-title">Set Offer Rules</strong>
        </a>
    </div>
    <div class="dashboard__crumb @if ($active == 'step2') dashboard__crumb--current @endif">
        <a href="@if ($active == 'step2' || $active == 'step3') {{ route('partnerOfferEdit', $offer) }} @else # @endif">
            <div class="circle">
                <span>2</span>
            </div>
            <strong class="dashboard__crumb-title">Add Vehicles</strong>
        </a>
    </div>
    <div class="dashboard__crumb @if ($active == 'step3') dashboard__crumb--current @endif">
        <a href="@if ($active == 'step3' || $active == 'step2') {{ route('partnerOfferShow', $offer) }} @else # @endif">
            <div class="circle">
                <span>3</span>
            </div>
            <strong class="dashboard__crumb-title">Review</strong>
        </a>
    </div>
</div>
