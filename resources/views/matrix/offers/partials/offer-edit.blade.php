@foreach ($errors->all() as $error)
    <div class="alert alert-error">
        {!!$error!!}
    </div>
@endforeach
<div class="offer">
    <!-- .offer__header -->
    <div class="offer__header">
        <div class="offer__title">
            <h3>{{ $offer->manufacturer->name }}</h3>
            <span>{{$offer->fuel_type=='Petrol' ? 'Petrol/Hybrid/Electric' : 'Diesel'}} </span>
            <p class="offer__edit">
                <input class="u-hide offer__edit__show_on_edit" type="text"  name="name" id="name" value="{{$offer->name}}" required>
                <span class="offer__edit__hide_on_edit offer__edit_static_fill" style="font-size: unset; color: unset;">@if ($offer->name){{ $offer->name }}@else<em>No nickname provided</em>@endif</span>
                <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
            </p>
        </div>
    </div>
    <!-- / .offer__header -->
    <!-- .offer__list -->
    <div class="offer__list">
        <div class="col-6">
            <ul>
                <!-- Year From -->
                <li>Year From:</li>
                <li class="offer__edit">
                    <select name="minimum_age" id="minimum_age" class="u-hide offer__edit__show_on_edit">
                        <option disabled>Please Select</option>
                        @foreach($plateYears as $plate)
                        <option value="{{$plate->plate_year}}-{{$plate->plate_number}}" @if($plate->plate_year == $offer->minimum_age) selected @endif>
                            {{$plate->plate_year}} {{$plate->plate_number}}
                        </option>
                        @endforeach
                    </select>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->minimum_age}}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                <!-- / Year From -->
                <!-- Minimum Mileage -->
                <li>Minimum Mileage:</li>
                <li class="offer__edit">
                    <input class="u-hide offer__edit__show_on_edit" type="number" max="999999" min="0" name="minimum_mileage" id="minimum_mileage" value="{{str_replace(",", "", $offer->minimum_mileage)}}" required>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->minimum_mileage}}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                <!-- / Minimum Mileage -->
                <!-- Minimum Value -->
                <li>Minimum Value:</li>
                <li class="offer__edit">
                    <input class="u-hide offer__edit__show_on_edit" type="number" max="999999" min="0" name="minimum_value" id="minimum_value" value="{{str_replace(",", "", $offer->minimum_value)}}" required>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->minimum_value}}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                <!-- / Minimum Value -->
                <!-- Minmum MOT -->
                <li>Minimum MOT:</li>
                <li class="offer__edit">
                    <select class="u-hide offer__edit__show_on_edit" name="minimum_mot" id="minimum_mot">
                        <option disabled>Please Select</option>
                        <option value="SixPlus" @if($offer->minimum_mot == "SixPlus") selected @endif>6 Months+</option>
                        <option value="ThreetoSix" @if($offer->minimum_mot == "ThreetoSix") selected @endif>3-6 Months</option>
                        <option value="OneToThree" @if($offer->minimum_mot == "OneToThree") selected @endif>1-3 Months</option>
                        <option value="LessThanOne" @if($offer->minimum_mot == "LessThanOne") selected @endif>Less Than 1 Month</option>
                        <option value="Expired" @if($offer->minimum_mot == "Expired") selected @endif>Expired</option>
                    </select>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{ trans('vehicles.' . $offer->minimum_mot) }}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                <!-- / Minimum MOT -->
                <!-- Accept Write Offs -->
                <li>Accept Write Offs?</li>
                <li class="offer__edit">
                    <select class="u-hide offer__edit__show_on_edit" name="accept_write_offs" id="accept_write_offs">
                        <option disabled>Please Select</option>
                        <option value="1" @if($offer->accept_write_offs) selected @endif>Yes</option>
                        <option value="0" @if(!$offer->accept_write_offs) selected @endif>No</option>
                    </select>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{ trans('vehicles.AcceptWriteOffs' . $offer->accept_write_offs) }}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                <!-- / Accept Write Offs -->
            </ul>
        </div>
        <div class="col-6">
            <ul>
                <!-- Year To -->
                <li>Year To:</li>
                <li class="offer__edit">
                    <select name="maximum_age" id="maximum_age" class="u-hide offer__edit__show_on_edit">
                        <option disabled>Please Select</option>
                        @foreach($plateYears as $plate)
                        <option value="{{$plate->plate_year}}-{{$plate->plate_number}}" @if($plate->plate_year .'-'. $plate->plate_number == $offer->maximum_age) selected @endif>
                            {{$plate->plate_year}} {{$plate->plate_number}}
                        </option>
                        @endforeach
                    </select>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->maximum_age}}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                <!-- / Year To -->
                <!-- Maximum Mileage -->
                <li>Maximum Mileage:</li>
                <li class="offer__edit">
                    <input class="u-hide offer__edit__show_on_edit" type="number" max="999999" min="0" name="maximum_mileage" id="maximum_mileage" value="{{str_replace(",", "", $offer->maximum_mileage)}}" required>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->maximum_mileage}}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                <!-- Maximum Mileage -->
                <!-- Maximum Value -->
                <li>Maximum Value:</li>
                <li class="offer__edit">
                    <input class="u-hide offer__edit__show_on_edit" type="number" max="999999" min="0" name="maximum_value" id="maximum_value" value="{{str_replace(",", "", $offer->maximum_value)}}" required>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->maximum_value}}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                <!-- / Maximum Value -->
                <li>Minimum Service History:</li>
                <li class="offer__edit">
                    <select class="u-hide offer__edit__show_on_edit" name="minimum_service_history" id="minimum_service_history">
                        <option disabled>Please Select</option>
                        <option value="FullFranchise" @if($offer->minimum_service_history == "FullFranchise") selected @endif>Full Franchise History</option>
                        <option value="PartFranchise" @if($offer->minimum_service_history == "PartFranchise") selected @endif>Part Franchise History</option>
                        <option value="PartHistory" @if($offer->minimum_service_history == "PartHistory") selected @endif>Part History</option>
                        <option value="MinimalHistory" @if($offer->minimum_service_history == "MinimalHistory") selected @endif>Minimal History</option>
                        <option value="None" @if($offer->minimum_service_history == "None") selected @endif>None</option>
                    </select>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{ trans('vehicles.' . $offer->minimum_service_history) }}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>

                <!-- Maximum Number of Previous Owners -->
                <li>Maximum No. of Previous Owners:</li>
                <li class="offer__edit">
                    <select class="u-hide offer__edit__show_on_edit" name="maximum_previous_owners" id="maximum_previous_owners" style="max-width: calc(100% - 30px);">
                        <option disabled>Please Select</option>
                        <option value="1" @if($offer->maximum_previous_owners == "1") selected @endif>First Owner</option>
                        <option value="2" @if($offer->maximum_previous_owners == "2") selected @endif>1 Previous Owner</option>
                        <option value="3" @if($offer->maximum_previous_owners == "3") selected @endif>2 Previous Owners</option>
                        <option value="4" @if($offer->maximum_previous_owners == "4") selected @endif>3 Previous Owners</option>
                        <option value="5" @if($offer->maximum_previous_owners == "5") selected @endif>4 Previous Owners</option>
                        <option value="6" @if($offer->maximum_previous_owners == "6") selected @endif>More than 5 previous owners</option>
                    </select>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill"> {{trans('vehicles.owners_count_external.owners_'.($offer->maximum_previous_owners <= 5 ? $offer->maximum_previous_owners : 6)) }}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                <!-- / Maximum Number of Previous Owners -->
            </ul>
        </div>
        <div class="clear"></div>
        <div class="divider"></div>
        <div class="clear"></div>
        <div class="col-12">
            <ul>
                {{-- @if ($offer->accept_write_offs) --}}
                <div class="js-write-off-category @if (!$offer->accept_write_offs) u-hide @endif">
                <li>Write Off Category:</li>
                    <li class="offer__edit">
                        <select class="u-hide offer__edit__show_on_edit" name="maximum_write_off_category" id="maximum_write_off_category" style="width:100%;">
                            <option disabled>Please Select</option>
                            <option value="none" @if($offer->maximum_write_off_category == "none") selected @endif>None</option>
                            <option value="N" @if($offer->maximum_write_off_category == "N") selected @endif>Cat N / Cat D (Can be repaired following non-structural damage)</option>
                            <option value="S" @if($offer->maximum_write_off_category == "S") selected @endif>Cat S / Cat C (Can be repaired following structural damage)</option>
                            <option value="all" @if($offer->maximum_write_off_category == "all") selected @endif>All</option>
                        </select>
                        <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->maximum_write_off_category }}</span>
                        <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                        <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                    </li>

                </div>
                {{-- @else
                <li>Write Off Category:</li>
                <li class="offer__edit" style="width: 100%;">
                    <p class="col-3">
                        <label for="all" class="u-hide offer__edit__show_on_edit">
                            <span>All</span>
                            <input name="maximum_write_off_category[]" type="checkbox" id="all" value="all">
                        </label>
                    </p>
                    <p class="col-3">
                        <label for="A" class="u-hide offer__edit__show_on_edit">
                            <span>Cat A</span>
                            <input name="maximum_write_off_category[]" type="checkbox" id="A" value="A">
                        </label>
                    </p>
                    <p class="col-3">
                        <label for="B" class="u-hide offer__edit__show_on_edit">
                            <span>Cat B</span>
                            <input name="maximum_write_off_category[]" type="checkbox" id="B" value="B">
                        </label>
                    </p>
                    <p class="col-3">
                        <label for="C" class="u-hide offer__edit__show_on_edit">
                            <span>Cat C</span>
                            <input name="maximum_write_off_category[]" type="checkbox" id="C" value="C">
                        </label>
                    </p>
                    <p class="col-3">
                        <label for="D" class="u-hide offer__edit__show_on_edit">
                            <span>Cat D</span>
                            <input name="maximum_write_off_category[]" type="checkbox" id="D" value="D">
                        </label>
                    </p>
                    <p class="col-3">
                        <label for="N" class="u-hide offer__edit__show_on_edit">
                            <span>Cat N</span>
                            <input name="maximum_write_off_category[]" type="checkbox" id="N" value="N">
                        </label>
                    </p>
                    <p class="col-3">
                        <label for="S" class="u-hide offer__edit__show_on_edit">
                            <span>Cat S</span>
                            <input name="maximum_write_off_category[]" type="checkbox" id="S" value="S">
                        </label>
                    </p>
                    <p class="col-3">
                        <label for="none" class="u-hide offer__edit__show_on_edit">
                            <span>None</span>
                            <input name="maximum_write_off_category[]" type="checkbox" id="none" value="none">
                        </label>
                    </p>
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{ ucwords($offer->maximum_write_off_category) }}</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                @endif --}}
                <!-- Acceptable Mechanical Defects -->
                @if ($offer->acceptable_defects)
                <li>Acceptable Mechanical Defects:</li>
                <li class="offer__edit" style="width: 100%;float:left;">
                    <p class="col-3">
                        <label for="selectAllDefects" class="u-hide offer__edit__show_on_edit js-selectAllDefects">
                            <strong>Select All</strong>
                            <input type="checkbox" id="selectAllDefects">
                        </label>
                    </p>
                    @foreach ($defects as $defect)
                        <p class="col-3">
                            <label for="{{ $defect->title }}" class="u-hide offer__edit__show_on_edit">
                                <span>{{ $defect->title }}</span>
                                <input name="acceptable_defects[]" type="checkbox" id="{{ $defect->title }}" value="{{ $defect->id }}" @if(in_array($defect->id, $defectIds->toArray())) checked @endif>
                            </label>
                        </p>
                    @endforeach
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">
                        @foreach ($offer->acceptable_defects as $defect)
                        {{$defect->title}}@if (!$loop->last), @endif
                        @endforeach
                    </span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                @else
                <li>Acceptable Mechanical Defects:</li>
                <li class="offer__edit" style="width: 100%; float: left;">
                    <p class="col-3">
                        <label for="selectAllDefects" class="u-hide offer__edit__show_on_edit js-selectAllDefects">
                            <strong>Select All</strong>
                            <input type="checkbox" id="selectAllDefects">
                        </label>
                    </p>
                    @foreach ($defects as $defect)
                    <p class="col-3">
                        <label for="{{ $defect->title }}" class="u-hide offer__edit__show_on_edit">
                            <span>{{ $defect->title }}</span>
                            <input name="acceptable_defects[]" type="checkbox" id="{{ $defect->title }}" value="{{ $defect->id }}">
                        </label>
                    </p>
                    @endforeach
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">None</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                @endif
                <!-- / Acceptable Defects -->
                
                <!-- Acceptable Colours -->
                @if ($offer->acceptable_colors)
                <li>Acceptable Colours:</li>
                <li class="offer__edit">
                    @foreach ($colours as $color)
                    <p>
                        <label for="{{ $color->title }}" class="u-hide offer__edit__show_on_edit">
                            <span>{{ $color->title }}</span>
                            <input name="colours[]" type="checkbox" id="{{ $color->title }}" value="{{ $color->id }}" @if(in_array($color->id, $colorIds->toArray())) checked @endif">
                        </label>
                    </p>
                    @endforeach
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">
                        @foreach ($offer->acceptable_colors as $color)
                        {{$color->title}}@if (!$loop->last), @endif
                        @endforeach
                    </span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                @else
                <li>Acceptable Colours:</li>
                <li class="offer__edit" style="width: 100%; float: left;"> 
                    @foreach ($colours as $color)
                    <p class="col-3">
                        <label for="{{ $color->title }}" class="u-hide offer__edit__show_on_edit">
                            <span>{{ $color->title }}</span>
                            <input name="colours[]" type="checkbox" id="{{ $color->title }}" value="{{ $color->id }}" checked>
                        </label>
                    </p>
                    @endforeach
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">All Colours Accepted</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
                @endif
                <!-- / Acceptable Colours -->
            </ul>
        </div>
        <div class="divider divider--light"></div>
        <div class="col-6">
            <ul>
                <li>Great Condition Percentage Modifier</li>
                <li class="offer__edit">
                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->great_condition_percentage_modifier}}%</span>

                    <i class="fa fa-ban"></i>
                </li>

                <li>Average Condition Percentage Modifier</li>
                <li class="offer__edit">
                    <input class="u-hide offer__edit__show_on_edit" type="number" name="avg_condition_percentage_modifier" id="avg_condition_percentage_modifier" value="{{ $offer->avg_condition_percentage_modifier }}" required>

                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->avg_condition_percentage_modifier}}%</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
            </ul>
        </div>
        <div class="col-6">
            <ul>
                <li>Good Condition Percentage Modifier</li>
                <li class="offer__edit">
                    <input class="u-hide offer__edit__show_on_edit" type="number" name="good_condition_percentage_modifier" id="good_condition_percentage_modifier" value="{{ $offer->good_condition_percentage_modifier }}" required>

                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->good_condition_percentage_modifier}}%</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>

                <li>Poor Condition Percentage Modifier</li>
                <li class="offer__edit">
                    <input class="u-hide offer__edit__show_on_edit" type="number" name="poor_condition_percentage_modifier" id="poor_condition_percentage_modifier" value="{{ $offer->poor_condition_percentage_modifier }}" required>

                    <span class="offer__edit__hide_on_edit offer__edit_static_fill">{{$offer->poor_condition_percentage_modifier}}%</span>
                    <i class="fa fa-pencil u-cursor-pointer offer__edit__hide_on_edit"></i>
                    <i class="fa fa-check u-cursor-pointer offer__edit__show_on_edit" style="display: none;"></i>
                </li>
            </ul>
        </div>
    </div>
    <!-- / .offer__list -->
{{--     @if ($offer->active == 1)
        @foreach ($offer->models as $model)
            @if ($model)
                <offer-model :model="{{$model}}" :selected="{{json_encode($selectedDerivatives)}}" :details="{{$model->model->load('manufacturer')}}"></offer-model>
            @endif
        @endforeach
    @endif --}}
</div>
@push('scripts-after')
<script>
// Don't submit the form on enter key
$(function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});

$(function() {
    $('#selectAllDefects').on('ifChecked', function(event){
        $('input[name="acceptable_defects[]"]').each(function(index, el) {
            $(el).iCheck('check')
        });
    });
    $('#selectAllDefects').on('ifUnchecked', function(event){
        $('input[name="acceptable_defects[]"]').each(function(index, el) {
            $(el).iCheck('uncheck')
        });
    });
    $('#accept_write_offs').on('change', function(e) {
        var val = $(this).val();
        console.log(val);
        if (val === '1') {
            console.log('enabling maximum write off category');
            $('.js-write-off-category').removeClass('u-hide');
        } else {
            console.log('disabling maximum write off category');
            $('.js-write-off-category').addClass('u-hide');
        }
    });
    // $('maximum_write_off_category')
});


</script>
@endpush
