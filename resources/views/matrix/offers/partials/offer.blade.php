<div class="offer">
    <!-- .offer__header -->
    <div class="offer__header">
        <div class="offer__title">

            <h3>{{ $vehicleManufacturer ? $vehicleManufacturer->name : 'Unknown manufacturer' }}</h3>
            <span>{{ $offer->fuel_type }}</span>
            <p>{{ $offer->name }}</p>
        </div>
        <div class="offer__buttons">
            <a href="{{ route('partnerOfferEditVehicles', $offer) }}" class="button button--narrow button--grey-bg">add / remove vehicles</a>
            <a href="{{ route('partnerOfferEdit', $offer) }}" class="button button--narrow button--orange-bg">view / edit</a>

                @if ($offer->active)
                <a href="{{ route('partnerOfferPause', $offer) }}" class="button button--narrow button--red">pause</a>
                @else
                <a href="{{ route('partnerOfferComplete', $offer) }}" class="button button--narrow button--green-bg">activate</a>
                @endif


            <a class="button button--narrow button--red-bg" onclick="event.preventDefault();deleteOffer('{{$offer->id}}');">delete</a>
        </div>
    </div>
    <!-- / .offer__header -->
    @if($offer->active==0)
        <div class="row" >
            <div class="col-md-12" style="margin-top:20px;margin-bottom:10px;">
                <span class="rule-paused">RULE PAUSED</span>
            </div>

        </div>
    @endif


<!-- .offer__list -->
    <div class="offer__list">
        <div class="offer__list-wrap">
            <div class="col-6">
                <ul>
                    <li>Year From:</li>
                    <li>{{$offer->minimum_age}}</li>

                    <li>Minimum Mileage:</li>
                    <li>{{$offer->minimum_mileage}}</li>

                    <li>Minimum Value:</li>
                    <li>{{$offer->minimum_value}}</li>

                    <li>Minimum MOT:</li>
                    <li>{{trans('vehicles.'.$offer->minimum_mot)}}</li>

                    <li>Accept Non Runners?</li>
                    <li>{{ trans('vehicles.AcceptWriteOffs' . $offer->accept_write_offs) }}</li>

                    @if ($offer->accept_write_offs)
                    <li>Accept Write Offs?:</li>
                    <li>{{ucfirst($offer->maximum_write_off_category)}}</li>
                    @endif
                </ul>
            </div>
            <div class="col-6">
                <ul>
                    <li>Year To:</li>
                    <li>{{$offer->maximum_age}}</li>

                    <li>Maximum Mileage:</li>
                    <li>{{$offer->maximum_mileage}}</li>

                    <li>Maximum Value:</li>
                    <li>{{$offer->maximum_value}}</li>

                    <li>Minimum Service History:</li>
                    <li>{{trans('vehicles.'.$offer->minimum_service_history)}}</li>

                    <li>Maximum No. of Previous Owners:</li>
                    <li>
                        {{trans('vehicles.owners_count_external.owners_'.($offer->maximum_previous_owners <= 5 ? $offer->maximum_previous_owners : 6)) }}
                    </li>
                </ul>
            </div>
        </div>

        <div class="clear"></div>
        <div class="divider divider--light"></div>
        <div class="clear"></div>

        <div class="col-12">
            <ul>
                <li>Write Off Category:</li>
                @if ($offer->accept_write_offs)
                <li>{{ ucwords($offer->maximum_write_off_category) }}</li>
                @else
                <li>None</li>
                @endif

                <li>Acceptable Colours:</li>
                @if ($offer->acceptable_colors)
                <li>
                    <span>
                    @foreach ($offer->acceptable_colors as $color)
                        {{$color->title}}@if (!$loop->last), @endif
                    @endforeach
                    </span>
                </li>
                @else
                <li>All</li>
                @endif

                <li>Acceptable Mechanical Defects:</li>
                @if ($offer->acceptable_defects)
                <li>
                	<span>
	                	@foreach ($offer->acceptable_defects as $defect)
	                        {{$defect->title}}@if (!$loop->last), @endif
	                    @endforeach
                	</span>
               	</li>
               	@else
               	<li><span>None</span></li>
                @endif
            </ul>
        </div>

        <div class="divider divider--light"></div>
        <div class="offer__list-wrap">
            <div class="col-6">
                <ul>
                    <li>Great Condition Modifier</li>
                    <li>{{$offer->great_condition_percentage_modifier}}%</li>
                    <li>Average Condition Modifier</li>
                    <li>{{$offer->avg_condition_percentage_modifier}}%</li>
                </ul>
            </div>
            <div class="col-6">
                <ul>
                    <li>Good Condition Modifier</li>
                    <li>{{$offer->good_condition_percentage_modifier}}%</li>
                    <li>Poor Condition Modifier</li>
                    <li>{{$offer->poor_condition_percentage_modifier}}%</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- / .offer__list -->

    @if ($offer->active == 1)
        @unless($offer->models->isEmpty())
            @foreach ($offer->models as $model)
                @if ($model)
                    <?php $vehicleModel = $model->getVehicleModel() ?>
                    @if ($model->model->manufacturer)
                    <!-- .offer__rule -->
                    <div class="offer__rule">
                        <div class="offer__rule--title">
                            <strong>{{$vehicleModel->manufacturer->name}} {{ $vehicleModel->name }}  ({{$vehicleModel->introduced_date}} - {{ $vehicleModel->discontinued_date}})</strong>
                        </div>
                        <div class="offer__rule--derivatives">
                            <strong>{{ $model->derivatives->count() }} Derivatives</strong>
                            {{-- <strong>{{$offer->derivativesCount($model['cap_model_id'])}} Derivatives</strong> --}}
                        </div>
                        <div class="offer__rule--percentage">
                            <strong>{{ $offer->base_value }}%</strong>
                        </div>
                        <!-- .offer__rule--children -->
                        <div class="offer__rule-children">
                            @foreach ($model->derivatives as $derivative)
                                @if (($derivative->base_value != $offer->base_value) ||  ($derivative->getSuggestedValue()))
                                <div class="offer__rule--child">
                                    <div class="offer__rule--title">
                                        <p>{{$derivative->getVehicleDerivative() ? $derivative->getVehicleDerivative()->name : 'Unknown derivative' }}</p>
                                    </div>
                                    <div class="offer__rule--derivatives">
                                        &nbsp;
                                        @if ($derivative->getSuggestedValue())
                                            @if ($derivative->getSuggestedValue()->percentage)
                                                @if ($derivative->getSuggestedValue()->percentage > $derivative->base_value)
                                                <a href="{{ route('partnerOfferSuggestedValues', $offer) }}">
                                                    <span class="jj-suggested">
                                                        Jamjar Suggested Value of {{$derivative->getSuggestedValue()->percentage}}% - edit offer to update
                                                    </span>
                                                </a>
                                                @else
                                                <a href="{{ route('partnerOfferSuggestedValues', $offer) }}">
                                                    <span class="jj-suggested">
                                                        <i class="fa fa-check"></i> Using Jamjar Suggested
                                                    </span>
                                                </a>
                                                @endif
                                            @endif
                                        @endif
                                    </div>
                                    <div class="offer__rule--percentage">
                                        <p>{{ $derivative->base_value }}%</p>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        </div>
                        <!-- / .offer__rule--children -->
                    </div>
                    @endif
                    <!-- / .offer__rule -->
                @endif
            @endforeach
        @else
            <div class="row">
                <div class="col-md-12">
                    <p><strong>You have not selected any vehicles.</strong></p>
                </div>

            </div>

        @endunless
    @endif

</div>
<form action="{{ route('partnerOfferDelete', $offer) }}" method="POST" id="delete-offer-{{$offer->id}}">
    {{csrf_field()}}
    {{method_field('DELETE')}}
</form>
