@extends('layouts.app')

@section('content')

@include('matrix.partials.header', [
    'title' => 'Add New Rule',
    'buttons' => '<a href="'.route('partnerOffers').'" class="button button--orange-bg u-pull-right"><i class="fa fa-chevron-left"></i> Back to Offers</a>',
])

@if ($errors->any())
<div class="dashboard">
    <div class="dasboard__container">
        <div class="alert alert--danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'create-bids'])
        </div>
        <div class="dashboard__col">
            <div class="box box--no-padding box--with-header" style="margin-top:0px">
                <matrix-offers :scrap_dealer="{{auth()->user()->profile->is_scrap_dealer}}" :db_offer="{{$offer}}" :years="{{$plateYears}}" :colors="{{$colours}}" :defects="{{$defects}}" :lang="{{json_encode(trans('vehicles'))}}"></matrix-offers>
            </div>
        </div>
    </div>
</section>

@endsection
