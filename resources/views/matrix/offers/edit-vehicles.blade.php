@extends('layouts.app')

@section('content')

    @include('matrix.partials.header', [
        'title' => 'Edit Vehicles for Rule ' . ($offer->name ? ('&mdash; <strong>' . $offer->name . '</strong>') : ''),
        'buttons' => '
            <a href="'.route('partnerOffers').'" class="button button--orange-bg u-pull-right" style="margin-left:0.75rem;"><i class="fa fa-chevron-left"></i> Back to Offers</a>
            <a href="'.route('partnerOfferEdit', $offer).'" class="button button--orange-bg u-pull-right" style="margin-left:0.75rem;"><i class="fa fa-pencil"></i> Edit Rule</a>
        ',
    ])

    <section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
        <div class="dashboard__container">
            <div class="dashboard__col">
                @include('matrix.partials.sidebar', ['active' => 'offers'])
            </div>
            <div class="dashboard__col">
                <edit-vehicles csrf="{{ csrf_token() }}" :offer="{{$offer}}" manufacturer="{{ucfirst(strtolower($offer->manufacturer->name))}}" criteria="{{route('partnerOfferEdit', $offer)}}" form="{{route('partnerOfferUpdateVehicles', $offer)}}"></edit-vehicles>


            </div>
        </div>
    </section>

    @push('scripts-after')

        <script>

            $(document).ready(function(){
                // When the user scrolls the page, execute myFunction
                window.onscroll = function() {scrollCheck()};
                var header = document.getElementById("save-all-header");
                var offsetPosition = header.offsetTop;
                var width = header.offsetWidth;
                var height = header.offsetHeight;

                $("#save-all-header").width(width-30);
                $("#save-all-header").css("padding-right", 33);
                function scrollCheck() {
                    if (window.pageYOffset > offsetPosition) {
                        header.classList.add("floating__header");
                        $("#first-offer-section").css("margin-top", height);
                    } else {
                        header.classList.remove("floating__header");
                        $("#first-offer-section").css("margin-top", 0);
                    }
                }

                function ConvertFormToJSON(form){
                    var array = jQuery(form).serializeArray();
                    var json = {};

                    jQuery.each(array, function() {
                        json[this.name] = this.value || '';
                    });

                    return json;
                }

                $("#save--for-all").click(function(){
                    swal({
                        title: "Are you sure?",
                        text: "You are going to change values for all models",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            let data2 = $('#edit-vehicles').serializeArray();
                            var token = data2.filter(obj=>{
                                return obj.name === '_token';
                            });
                            let data = {
                                'set-all': $("[name='set-all']").val()
                            };

                            $.ajax({
                                type:'POST',
                                url:'{{route('partnerOfferUpdateValuesForAllVehicles', $offer)}}',
                                data:{_token:token[0]['value'], _method:'PATCH',data:JSON.stringify(data)},
                                error: function() {
                                    console.log('Error');
                                },
                                success: function(data) {
                                    if(data.success){
                                        window.location = '{{route('partnerOfferEditVehicles', $offer)}}';
                                    }
                                },
                            });
                        }
                    });
                })


                $('#edit-vehicles').submit(function(e) {
                    // Get all the forms elements and their values in one step
                    e.preventDefault();

                    var data2 = $(this).serializeArray();

                    var token = data2.filter(obj=>{
                        return obj.name === '_token';
                    });

                    var json_data = ConvertFormToJSON(this);

                    $.ajax({
                        type:'POST',
                        url:'{{route('partnerOfferUpdateVehicles', $offer)}}',
                        data:{_token:token[0]['value'],_method:'PATCH',data:JSON.stringify(json_data)},
                        error: function() {
                            console.log('Error');
                        },
                        success: function(data) {
                            if(data.success){

                                window.location = '{{route('partnerOffers')}}';
                            }
                        },
                    });

                });



                $('body').on('click','label.selectAll1', function(event) {

                    if($('#selectAll1').prop('checked')==true) {

                        $('input[type="checkbox"]').each(function(index, el) {

                            if($(el).attr('id')!='selectAll1'){

                                $(el).prop('checked', false);
                            }

                        });
                        $('.offer__model').removeClass('active');
                    } else {

                        $('input[type="checkbox"]').each(function(index, el) {
                            if($(el).attr('id')!='selectAll1'){

                                $(el).prop('checked', true);
                            }
                        });
                        $('.offer__model').addClass('active');
                    }


                })

                $('body').on('click','label.model__check', function(event) {


                    // get the model
                    var model = $(this).parent().parent();
                    // if the model doesnt have an active class
                    if (!model.hasClass('active')) {
                        // find the child checkboxes
                        var derivatives = $(this).parent().siblings();
                        derivatives.removeClass('u-hide');
                        // check them
                        derivatives.find('input[type="checkbox"]').each(function(index, el) {
                            $(el).prop('checked', true);
                        });
                    } else {
                        // select all unselect
                        var derivatives = $(this).parent().siblings();
                        derivatives.addClass('u-hide');
                    }
                    // add the active class so we don't do this again
                    $(this).parent().parent().addClass('active');
                });

                $('body').on('change','.spirit-check > input[type="checkbox"]', function(event) {


                    // If the checkbox is checked and moving to an unchecked state
                    if (!event.target.checked) {
                        // find the children
                        var childDerivatives = $(this).parent().siblings().children().children().find('input[type="checkbox"]');
                        // find the parent
                        var parent = $(this).parent().parent();
                        // if we have children
                        if (childDerivatives.length > 0) {
                            // loop through the children
                            for (var i = childDerivatives.length - 1; i >= 0; i--) {
                                // uncheck the children
                                childDerivatives[i].checked = false;
                            }
                            // remove the active class
                            parent.removeClass('active');
                        }
                    }
                });

                $('body').on('change','.offer__derivative input[type="checkbox"]', function(e) {


                    var model = $(this).parent().parent().parent().parent();

                    var derivatives = model.find('.offer__derivative');
                    var isChecked = false;

                    $.each(derivatives, function(index, derivative) {
                        if ($(derivative).find('input[type="checkbox"]').prop('checked') == true) {
                            isChecked = true;
                        }
                        // console.log($(derivative).prop('checked'));
                    });

                    if (isChecked === false) {
                        $(model).find('input[type="checkbox"]').prop('checked', false);
                        $(model).removeClass('active');
                        $(model).children('.offer__derivatives').addClass('u-hide');
                        // derivatives.addClass('u-hide');
                    }
                });

                $('body').on('click','label.derivative__check', function(event) {


                    // get the parent checkbox and check it
                    $(this).parent().parent().parent().siblings('.spirit-check').find('input[type="checkbox"]').each(function(index, el) {
                        $(el).prop('checked', true);
                    });
                    // add the active class to the model row
                    $(this).parent().parent().parent().parent().addClass('active');
                });

            });


        </script>
    @endpush

@endsection
