@extends('layouts.app')

@section('content')
<header class="subheader">
    <div class="subheader__container">
        <h1 class="subheader__title">@if (!empty($offer->name)){{ $offer->name }} &mdash;@endif Suggested Values</h1>
        <a href="{{ route('partnerOffers') }}" class="button button--orange-bg u-pull-right" style="margin-left:0.75rem;">
            <i class="fa fa-chevron-left"></i> Back to Bids
        </a>
        <a href="{{ route('partnerOfferEdit', $offer) }}" class="button button--orange-bg u-pull-right" style="margin-left:0.75rem;">
            <i class="fa fa-pencil"></i> Edit Rule
        </a>
        <a href="{{ route('partnerOfferEditVehicles', $offer) }}" class="button button--orange-bg u-pull-right" style="margin-left:0.75rem;">
            <i class="fa fa-car"></i> Edit Vehicles
        </a>
    </div>
</header>

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'offers'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header box--no-margin">
                <div class="box__header header-theme-marketplace">@if (!empty($offer->name)){{ $offer->name }} &mdash;@endif Suggested Values</div>
                <div class="box__content box__content--with-padding">

                    <suggested-values :rule="true" slug="{{$offer->slug}}" csrf="{{ csrf_token() }}"></suggested-values>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
