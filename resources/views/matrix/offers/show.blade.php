@extends('layouts.app')

@section('content')

@include('matrix.partials.header', [
    'title' => 'Viewing Offer ' . ($offer->name ? ('&mdash; <strong>' . $offer->name . '</strong>') : ''),
    'buttons' => '
        <a href="'.route('partnerOffers').'" class="button button--orange-bg u-pull-right"><i class="fa fa-chevron-left"></i> Back to Offers</a>
    ',
])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'offers'])
        </div>
        <div class="dashboard__col">
            @include('matrix.offers.partials.breadcrumbs', ['active' => 'step3'])

            <div class="box box--with-header">
                <div class="box__header">
                    <span>3</span> Review
                </div>
                <div class="box__container">
                    <div class="box__content">
                        <p><span class="u-text-orange">Please confirm that your offer has been setup correctly.</span></p>
                        <p><strong class="u-text-orange">To finish, please click 'Publish' and your offers will be live on Jamjar.com</strong></p>

                        <div class="divider"></div>

                        <div class="offer-rules">
                            <strong class="offer-rules__title">Offer Rules</strong>
                            <div class="offer-rules__col">
                                <p>Minimum Mileage: <span>{{ $offer->minimum_mileage }}</span></p>
                                <p>Maximum Mileage: <span>{{ $offer->maximum_mileage }}</span></p>
                                <p>Minimum Value: <span>{{ $offer->minimum_value }}</span></p>
                                <p>Maximum Value: <span>{{ $offer->maximum_value }}</span></p>
                            </div>
                            <div class="offer-rules__col">
                                <p>Minimum Acceptable MOT: <span>{{ $offer->minimum_mot }}</span></p>
                                <p>Maximum No. Of Previous Owners:
                                    <span>
                                        {{trans('vehicles.owners_count_external.owners_'.($offer->maximum_previous_owners <= 5 ? $offer->maximum_previous_owners : 6)) }}
                                    </span>
                                </p>
                                <p>Accept Write Offs?: <span>{{ $offer->accept_write_offs ? 'Yes' : 'No Write Offs'}}</span></p>
                            </div>
                            <div class="clear"></div>
                            <div class="offer-rules__col">
                                @if ($offer->acceptable_colors)
                                <p><strong>Acceptable Colours:</strong></p>
                                <ul>
                                    @foreach ($offer->acceptable_colors as $color)
                                        <li>{{ $color->title }}</li>
                                    @endforeach
                                </ul>
                                @endif
                            </div>
                            <div class="offer-rules__col">
                                @if ($offer->acceptable_defects)
                                <p><strong>Acceptable Defects:</strong></p>
                                <ul>
                                    @foreach ($offer->acceptable_defects as $defect)
                                        <li>{{ $defect->title }}</li>
                                    @endforeach
                                </ul>
                                @endif
                            </div>
                        </div>

                        <table class="offer-review">
                            <thead class="offer-review__header">
                                <tr>
                                    <th>% of CAP</th>
                                    <th>Model / Derivative Name</th>
                                    <th># of Derivatives</th>
                                </tr>
                            </thead>
                            <tbody class="offer-review__body">
                                @foreach ($offer->models() as $model)
                                    <tr class="offer-review__row">
                                        <td><strong>{{$offer->base_value}}%</strong></td>
                                        <td><strong>{{$model->manufacturer->name}} {{ $model->name }} ({{$model->introduced_date}} - {{$model->discontinued_date }})</strong></td>
                                        <td><strong>{{ $offer->derivativesCount($model->cap_model_id) }} Derivatives</strong></td>
                                        @foreach ($offer->derivatives($model->cap_model_id) as $derivative)
                                            <tr class="offer-review__child-row">
                                                <td>&nbsp;</td>
                                                <td>{{ $model->manufacturer->name }} {{ $derivative->model->name }} {{ $derivative->name }}</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                            
                        <div class="form__submit">
                            <a href="{{ route('partnerOfferEdit', $offer) }}" class="button button--grey-bg">Previous Step</a>
                            <a href="{{ route('partnerOfferComplete', $offer) }}" class="button button--orange-bg">Complete &amp; Set Live</a>
                            <br>
                            <small>Once your bid is published it will appear live on Jamjar.com. You can come back and edit your offer details at any time.</small>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
