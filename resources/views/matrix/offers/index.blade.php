@extends('layouts.app')

@section('content')

@include('matrix.partials.header', [
    'title' => 'Automatic Bidding',
    'buttons' => (count($missing)>0) ? '' : '<a href="'.route('partnerOfferCreate').'" class="button button--orange-bg u-pull-right"><i class="fa fa-plus"></i> Add New Rule</a>',
])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'offers'])
        </div>
        <automated-bids  :missing="{{json_encode($missing)}}" csrf="{{ csrf_token() }}"></automated-bids>
    </div>
</section>
@push('scripts-after')
    <script>
        $(document).ready(function() {
            // When the user scrolls the page, execute myFunction
            window.onscroll = function () {
                scrollCheck()
            };
            var header = document.getElementById("save-all-header");
            var offsetPosition = header.offsetTop;
            var width = header.offsetWidth;
            var height = header.offsetHeight;

            $("#save-all-header").width(width - 30);
            $("#save-all-header").css("padding-right", 33);

            function scrollCheck() {
                if (window.pageYOffset > offsetPosition) {
                    header.classList.add("floating__header");
                    $(".offer-box__header").css("margin-top", +90);
                } else {
                    header.classList.remove("floating__header");
                    $(".offer-box__header").css("margin-top", 0);
                }
            }
        });
    </script>
@endpush
@endsection
