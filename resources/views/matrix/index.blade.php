@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Dashboard'])

<section class="dashboard @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'dashboard'])
		</div>
		<div class="dashboard__col">
			<div class="box box--with-header box--no-margin">
				<div class="box__header box__header_dashboard">Latest Agreed Sales</div>
				<div class="box__container">
					<div class="box__content">
						<div class="box__subheader box__subheader_dashboard box__content">
							<div class="col-2"><span>Date</span></div>
							<div class="col-2"><span>Registration</span></div>
							<div class="col-3"><span>Car</span></div>
							<div class="col-2"><span>Your Valuation</span></div>
							<div class="col-1"><span>Status</span></div>
							<div class="col-2"></div>
						</div>

						<div class="clear"></div>
                            @unless ($sales->isEmpty())
                                @foreach ($sales as $sale)
                                    @if ($sale->valuation->vehicle)
                                    <div class="box__item row box__item_dashboard box__content">
                                        <div class="col-2">
                                            <span class="box__item_dashboard-title">Date: </span>
                                            <span class="box__item_dashboard-content">{{ $sale->created_at->format('d/m/Y') }} <br />{{ $sale->created_at->format('H:i:s') }}</span>
                                        </div>
                                        <div class="col-2">
                                            <span class="box__item_dashboard-title">Registration: </span>
                                            <span class="box__item_dashboard-content">{{ strtoupper($sale->valuation->vehicle->numberplate) }}</span>
                                        </div>
                                        <div class="col-3">
                                            <span class="box__item_dashboard-title">Car: </span>
                                            <span class="box__item_dashboard-content">{{ $sale->valuation->vehicle->meta->manufacturer . ", " .$sale->valuation->vehicle->meta->model . ", " . $sale->valuation->vehicle->meta->derivative }}</span>
                                        </div>
                                        <div class="col-2">
                                            <span class="box__item_dashboard-title">Your Valuation: </span>
                                            <span class="box__item_dashboard-content">&pound;{{ number_format($sale->price) }}</span>
                                        </div>
                                        <div class="col-1">
                                            <span class="box__item_dashboard-title">Status: </span>
                                            <span class="box__item_dashboard-content">
                                                @if ($sale->status == 'pending')
                                                    <strong class="u-text-orange">Awaiting Action</strong>
                                                @elseif($sale->status == 'rejected')
                                                    <strong class="u-text-red">Offer Retracted</strong>
                                                @else
                                                    <strong class="u-text-green">Sale Agreed</strong>
                                                @endif
                                            </span>
                                        </div>
                                        <div class="col-2">
	                                      	<span class="box__item_dashboard-title">&nbsp;</span>
	                                      	<span class="box__item_dashboard-content">
	                                      		<a class="button button--small button--orange-bg" href="{{ route('partnerShowSale', $sale) }}">view details</a>
                                            </span>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <div class="col-12">
                                <div class="box__content">
                                    <p>No sales agreed to display.</p>
                                </div>
                            </div>
                        @endunless
                    </div>
					{{-- <div class="box__footer box__footer--no-border">
						<a href="#partnerSalesRoute" class="button button--orange-bg">VIEW ALL SALES AGREED</a>
					</div> --}}
				</div>
			</div>
		</div>
		<div class="dashboard__col">
            <div class="box box--with-header box--no-margin">
                @if (auth()->user()->profile->auto_partner == 0)
                <div class="box__header">Remaining Funds</div>
                <div class="box__container">
                    <div class="box__content u-text-center">
						<div class="box__item">
							<p>My Remaining Funds</p>
							<h1 class="u-text-green">&pound;{{auth()->user()->funds->funds}}</h1>
						</div>

						<a href="{{ route('partnerCredits') }}" class="button button--orange-bg">ADD FUNDS</a>
                    </div>
                </div>
                @else
                <div class="box__header">Auto Partner</div>
                <div class="box__container">
                    <div class="box__content u-text-center">
                        <div class="box__item">
                            <p>You are logged in as an automatic partner. This means you are only able to see your Paid Leads and Sales Agreed.</p>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
	</div>
</section>

@endsection
