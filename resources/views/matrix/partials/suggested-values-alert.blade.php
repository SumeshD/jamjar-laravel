@if (auth()->user()->isPartner())
    @php ($suggestedValuesCount = auth()->user()->getSuggestedValuesCount())
    @if ($suggestedValuesCount > 0)
        <div class="jamjar__container">
            <div class="alert alert--info">
                <p>
                    You have {{ $suggestedValuesCount }} outstanding Jamjar Suggested Values.
                    <a href="{{route('partnerOffers')}}" class="u-pull-right">Update now</a>
                </p>
            </div>
        </div>
    @endif
@endif
