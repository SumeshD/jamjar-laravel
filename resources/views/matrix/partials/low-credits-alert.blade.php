@if (auth()->user()->isPartner())
    @if (auth()->user()->funds->getOriginal('funds') != 0)
        @if (auth()->user()->funds->getOriginal('funds') <= 5000)
        <div class="jamjar__container">
            <div class="alert alert--danger">
                <p>
                    Your remaining funds are: &pound;{{auth()->user()->funds->funds}}
                    <a href="{{route('partnerCredits')}}" class="u-pull-right">Top up now</a>
                </p>
            </div>
        </div>
        @endif
    @else
        <div class="jamjar__container">
            <div class="alert alert--info">
                <p>
                    Your offers will not be live until you have added some funds to your account.
                    <a href="{{route('partnerCredits')}}" class="u-pull-right">Top up now</a>
                </p>
            </div>
        </div>
    @endif
@endif
