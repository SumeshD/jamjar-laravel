<?php $showLookupForm = isset($showLookupForm) ? $showLookupForm : true; ?>
<header class="subheader">
    <div class="subheader__container">
        <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
            <h1 class="subheader__title" style="width: 100%;">{!! $title !!}</h1>
        </div>
        @if ($showLookupForm)
            <div class="col-lg-6 col-sm-12 col-md-12 col-xs-12">
                <div style="text-align: center;">
                    <span style="vertical-align: super;">Unlimited</span>
                    <form class="numberplate numberplate-matrix-dashboard" method="post" action="{{ route('matrixValuationDraftsCreateVehicle') }}">
                        {{csrf_field()}}
                        <input style="padding: 1rem;" class="numberplate__input" type="text" id="vrm" name="numberplate" placeholder="REG NUMBER" value="{{ request('numberplate') }}">
                        <button style="right: 0;"><i class="fa fa-angle-right"></i></button>
                    </form>
                    <span style="vertical-align: super;">Look-ups</span>
                </div>
            </div>
        @else
            <div class="col-lg-6 col-sm-12 col-md-12 col-xs-12"></div>
        @endif
        <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12" style="padding: 0; text-align: left;">
            @if (isset($buttons))
                {!! $buttons !!}
            @endif
        </div>
    </div>
</header>