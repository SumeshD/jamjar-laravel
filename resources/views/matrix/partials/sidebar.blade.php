<div class="sidebar">
	<ul class="sidebar__list">
		<li class="sidebar__toggle">
			<a class="sidebar__link">
				Menu
			</a>
		</li>
		<li class="sidebar__item">
			<a href="{{ route('partnerDashboard') }}" class="sidebar__link icon-fa-home @if ($active == 'dashboard') sidebar__link--is-active @endif">
				<span class="menu-button-title">Dashboard</span>
			</a>
		</li>
		<li class="sidebar__item sidebar-between sidebar-setup">
			<div class="sidebar__separator_big">Setup</div>
		</li>

		<?php $isSetupComplete = auth()->user()->isSetupComplete() ?>
		<li class="sidebar__item">
			<a href="{{ route('partnerAccount') }}" class="sidebar__link icon-user {{ !$isSetupComplete ? (auth()->user()->isAccountDetailsComplete() ? 'complete' : 'incomplete') : 'complete' }} @if ($active == 'account') sidebar__link--is-active @endif">
				<span class="menu-button-title" style="margin-left:13px !important">
					Account Details
				</span>
				<span class="complete-mark"></span>
				<span class="incomplete-mark"></span>
			</a>
		</li>
		<li class="sidebar__item">
			<a href="{{ route('partnerSettingsForm') }}" class="sidebar__link icon-settings {{ !$isSetupComplete ? (auth()->user()->isAssociateSettingsComplete() ? 'complete' : 'incomplete') : 'complete' }} @if ($active == 'settings') sidebar__link--is-active @endif">
				<span class="menu-button-title">Associate Settings</span>
				<span class="complete-mark"></span>
				<span class="incomplete-mark"></span>
			</a>
		</li>
		<li class="sidebar__item">
			<a href="{{ route('partnerWebsiteTheme') }}" class="sidebar__link icon-frame {{ !$isSetupComplete ? (auth()->user()->isWebsiteThemeComplete() ? 'complete' : 'incomplete') : 'complete' }} @if ($active == 'theme') sidebar__link--is-active @endif">
				<span class="menu-button-title">Website Theme</span>
				<span class="complete-mark"></span>
				<span class="incomplete-mark"></span>
			</a>
		</li>
		@if (auth()->user()->profile->auto_partner == 0)
			<li class="sidebar__item">
				<a href="{{ route('partnerCredits') }}" class="sidebar__link icon-paypal  {{ !$isSetupComplete ? (auth()->user()->isFoundAndBillingsComplete() ? 'complete' : 'incomplete') : 'complete' }} @if ($active == 'credits') sidebar__link--is-active @endif">
					<span class="menu-button-title">Funds &amp; Billing</span>
					<span class="complete-mark"></span>
					<span class="incomplete-mark"></span>
				</a>
			</li>
		@endif
		<li class="sidebar__item sidebar-between sidebar-marketplace">
			<div class="sidebar__separator_big">Marketplace</div>
		</li>
		<li class="sidebar__item">
			<a href="{{ route('matrixValuationDraftsIndex',['quick'=>true]) }}" class="sidebar__link icon-magnifier @if ($active == 'drafts') sidebar__link--is-active @endif">
				<span class="menu-button-title">My Free Lookups</span>
			</a>
		</li>
{{--		<li class="sidebar__item">--}}
{{--			<a href="{{route('matrixSellItTradeTest')}}"  class="sidebar__link icon-fa-agreement @if ($active == 'sellittrade') sidebar__link--is-active @endif" >--}}
{{--				<span class="menu-button-title">SellItTrade Integration</span>--}}
{{--			</a>--}}
{{--		</li>--}}
		<li class="sidebar__item sidebar-small-between">
			<div class="sidebar__separator">Buying</div>
		</li>
		@if (auth()->user()->profile->auto_partner == 0)
			<li class="sidebar__item">
				<a href="{{ route('partnerGoodNewsLeads') }}" class="sidebar__link icon-basket @if ($active == 'leads') sidebar__link--is-active @endif">
					<span class="menu-button-title">Find Vehicles</span>
				</a>
			</li>
			<li class="sidebar__item">
				<a href="{{ route('marketplaceEnabledAlertsList') }}" class="sidebar__link icon-bell @if ($active == 'marketplace-alerts') sidebar__link--is-active @endif">
					<span class="menu-button-title">Alerts</span>
				</a>
			</li>

		@endif
		<li class="sidebar__item">
			<a href="{{ route('partnerValuations',[
                                    'offer_status'=>'all',
                                    'lead_source'=>'marketplace',
                                    'outbid'=>false,
                                    'vrm'=>''
                                ]) }}" class="sidebar__link icon-flag @if ($active == 'valuations-marketplace') sidebar__link--is-active @endif">
				<span class="menu-button-title">Paid Leads</span>
			</a>

		</li>
		<li class="sidebar__item">
			<a href="{{ route('matrixAgreedPurchasesIndex',['marketplace'=>true]) }}" class="sidebar__link icon-fa-smile @if ($active == 'agreed-purchases-marketplace') sidebar__link--is-active @endif">
				<span class="menu-button-title">Agreed Purchases</span>
			</a>
		</li>
		<li class="sidebar__item sidebar-small-between">
			<div class="sidebar__separator">Selling</div>
		</li>
		<li class="sidebar__item">
			<a href="{{ route('matrixValuationDraftsIndex') }}" class="sidebar__link icon-plus @if ($active == 'drafts') sidebar__link--is-active @endif">
				<span class="menu-button-title">Add Vehicle</span>
			</a>
		</li>
		@if (auth()->user()->profile->auto_partner == 0)
			<li class="sidebar__item">
				<a href="{{ route('matrixValuationDraftsListedVehicles') }}" class="sidebar__link icon-list @if ($active == 'listed-vehicles') sidebar__link--is-active @endif">
					<span class="menu-button-title">Listed Vehicles</span>
				</a>
			</li>
			<li class="sidebar__item">
				<a href="{{ route('matrixValuationDraftsOffersReceived') }}" class="sidebar__link icon-flag @if ($active == 'offers-received') sidebar__link--is-active @endif">
					<span class="menu-button-title">Received Bids</span>
				</a>
			</li>
			<li class="sidebar__item">
				<a href="{{ route('matrixAgreedSalesIndex') }}" class="sidebar__link icon-like @if ($active == 'agreed-sales') sidebar__link--is-active @endif">
					<span class="menu-button-title">Agreed Sales</span>
				</a>
			</li>
        @endif

		@if (auth()->user()->profile->auto_partner == 0)
		<li class="sidebar__item sidebar-between sidebar-bidding">
			<div class="sidebar__separator_big">Automatic Bidding</div>
		</li>


			<li class="sidebar__item">
				<a href="{{ route('partnerOfferCreate') }}" class="sidebar__link icon-magic-wand @if ($active == 'create-bids') sidebar__link--is-active @endif">
					<span class="menu-button-title">Add Rule</span>
				</a>
			</li>
			<li class="sidebar__item">
				<a href="{{ route('partnerOffers') }}" class="sidebar__link icon-equalizer @if ($active == 'offers') sidebar__link--is-active @endif">
					<span class="menu-button-title">View Rules</span>
				</a>
			</li>
			<li class="sidebar__item">
				<a href="{{ route('partnerValuations',[
                                    'offer_status'=>'all',
                                    'lead_source'=>'automated_bids',
                                    'outbid'=>false,
                                    'vrm'=>''
                                ]) }}" class="sidebar__link icon-flag @if ($active == 'valuations-automated-bids') sidebar__link--is-active @endif">
					<span class="menu-button-title">Paid Leads</span>
				</a>

			</li>
			<li class="sidebar__item">
				<a href="{{ route('matrixAgreedPurchasesIndex',['automated_bids'=>true]) }}" class="sidebar__link icon-fa-smile @if ($active == 'agreed-purchases-automated-bids') sidebar__link--is-active @endif">
					<span class="menu-button-title">Agreed Purchases</span>
				</a>
			</li>

		@endif

    </ul>
</div>
