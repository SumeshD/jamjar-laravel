<?php /** @var \JamJar\Model\MarketplaceAlerts\MarketplaceAlert $alert */ ?>

@extends('layouts.app')

@section('content')

    @include('matrix.partials.header', ['title' => 'Marketplace'])

    <div class="clear"></div>

    <section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
        <div class="dashboard__container">
            <div class="dashboard__col">
                @include('matrix.partials.sidebar', ['active' => 'marketplace-alerts'])
            </div>
            <div class="dashboard__col">
                <div class="box box--with-header box--no-margin">
                    <div class="box__header header-theme-bidding">Marketplace Alerts</div>
                    <div class="box__container">
                        <div class="box__content box__with_filtering">
                            <form action="{{ route('marketplaceAlertsProcessEditForm', [$alert]) }}" method="POST" style="margin-bottom: 2rem;" class="form" id="filters-form">
                                {{ csrf_field() }}
                                <div class="col-12 add-alert-container" style="font-weight: bold;">
                                    <div class="col-2">
                                        <a style="width: 160px;" href="{{ $alert->isEnabled() ? route('marketplaceEnabledAlertsList') : route('marketplaceDisabledAlertsList') }}" class="button button--orange-bg">GO BACK</a>
                                    </div>
                                    <div class="col-10">
                                        <div style="text-transform: uppercase; font-size: 19px; margin-bottom: 10px;">YOUR ARE CURRENTLY EDITING '{{ $alert->getName() }}'</div>
                                        <div style="font-size: 12px;">Edit your details below, and click save to update alert.</div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div style="margin: 20px 0; font-size: 18px; font-weight: bold;">
                                    Edit your alert
                                </div>
                                <div class="col-4" style="font-size: 12px;">
                                    <div style="margin: 5px 0; font-weight: bold;">Alert name</div>
                                    <div style="margin-bottom: 20px;">Name your alert so you can identify it later</div>
                                    <div style="width: 90%;">
                                        <input style="background-color: #f6f6f6;" class="form__input" type="text" name="alert-name" value="{{ isset($alertName) ? $alertName : $alert->getName() }}">
                                        @if ($errors->has('alert-name'))
                                            <div style="color: red;"><strong>{{ $errors->first('alert-name') }}</strong></div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-4" style="font-size: 12px;">
                                    <div style="margin: 5px 0; font-weight: bold;">Alert frequency</div>
                                    <div style="margin-bottom: 30px;">Get an alert for each vehicle or a daily summary</div>
                                    <div>
                                        <span style="margin-right: 40px; font-size: 17px;">
                                            <input type="radio" name="alert-frequency" value="instant" {{ $alert->getFrequency() == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::FREQUENCY_INSTANT ? 'checked' : '' }}>
                                            Instant
                                        </span>
                                        <span style="font-size: 17px;">
                                            <input type="radio" name="alert-frequency" value="daily" {{ $alert->getFrequency() == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::FREQUENCY_DAILY ? 'checked' : '' }}>
                                            Daily
                                        </span>
                                        @if ($errors->has('alert-frequency'))
                                            <div style="color: red; margin-top: 20px;"><strong>{{ $errors->first('alert-frequency') }}</strong></div>
                                        @endif
                                    </div>
                                </div>
                                <div style="margin-bottom: 30px;" class="clear"></div>

                                @include('matrix.leads.filters-container', [
                                    'showDates' => false,
                                    'showHeaderMenu' => false,
                                    'showApplyFiltersButton' => false,
                                    'showAlertButton' => false,
                                ])

                                <input type="submit" style="margin-top: 20px;" value="SAVE ALERT" class="button button--green-bg button--big">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection