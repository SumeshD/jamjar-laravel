<?php /** @var \JamJar\Model\MarketplaceAlerts\MarketplaceAlert $alert */ ?>

<div class="models-and-manufacturers-modal" id="alert-{{ $alert->getId() }}-modal">
    <div class="manufacturers-and-models-modal-header">All manufacturers and models</div>
    <div class="manufacturers-and-models-modal-content">
        @foreach ($alert->getMarketplaceAlertsFilters() as $marketplaceAlertsFilter)
            <div>
                <span>{{ $marketplaceAlertsFilter->getManufacturerName() }}: </span>
                <span class="models-names">
                    @if (count($marketplaceAlertsFilter->getUniqueModels()) > 0)
                        {{ implode(', ', $marketplaceAlertsFilter->getUniqueModelsNames()) }}
                    @else
                        <span>all</span>
                    @endif
                </span>
            </div>
        @endforeach
    </div>
    <div class="manufacturers-and-models-modal-footer">
        <span class="arrow-down-border"></span>
        <span class="arrow-down"></span>
    </div>
</div>