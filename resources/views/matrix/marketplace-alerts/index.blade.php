@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Marketplace'])

@push('scripts-after')
    <style type="text/css">
        .manage-button-area {
            float: left;
            white-space:nowrap;
            height: 50px;
            margin-top: 20px;
        }
    </style>
@endpush

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'marketplace-alerts'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header box--no-margin">
                <div class="box__header header-theme-bidding">Marketplace Alerts</div>
                @include('matrix.marketplace-alerts.buttons')
                <div class="box__container">
                    <div>
                        <table class="alerts-table jj_responsive_table marketplace_alerts_table">
                            <tr>
                                <th style="width: 15%;">Name</th>
                                <th style="width: 40%;">Vehicle filters</th>
                                <th style="width: 10%;">Frequency</th>
                                <th style="width: 32%;">Actions</th>
                            </tr>
                            @foreach ($alerts as $alert)
                                <?php /** @var \JamJar\Model\MarketplaceAlerts\MarketplaceAlert $alert */ ?>
                                <tr>
                                    <td style="font-weight: bold;">{{ $alert->getName() }}</td>
                                    <td class="alert-specification">
                                        @if (count($alert->getMarketplaceAlertsFilters()) > 0)
                                            <div class="spec-area manufacturers-summary">
                                                <span class="modal-trigger-area">
                                                    @include('matrix.marketplace-alerts.models-and-manufacturers-modal', ['alert' => $alert])
                                                    <b>Manufacturers:</b>
                                                    @foreach ($alert->getMarketplaceAlertsFilters() as $modelManufacturerFilter)
                                                        <?php /** @var \JamJar\Model\MarketplaceAlerts\MarketplaceAlertsFilter $modelManufacturerFilter */ ?>
                                                        <span>
                                                            {{ $modelManufacturerFilter->getManufacturerName() }}
                                                            <?php $modelsCount = count($modelManufacturerFilter->getUniqueModels()) ?>
                                                            ({{ $modelsCount > 0 ? $modelsCount : 'all' }})
                                                        </span>
                                                    @endforeach
                                                </span>
                                            </div>
                                        @else
                                            <div class="spec-area">
                                                <b>Manufacturers:</b> All
                                            </div>
                                        @endif

                                        @if ($alert->getCarCondition())
                                            <div class="spec-area">
                                                <b>Condition: </b>
                                                @if ($alert->getCarCondition() == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::CONDITION_NON_RUNNERS_ONLY)
                                                    Non-runners only
                                                @elseif ($alert->getCarCondition() == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::CONDITION_RUNNERS_ONLY)
                                                    Runners only
                                                @endif

                                            </div>
                                        @endif

                                        @if ($alert->getVehicleType())
                                            <div class="spec-area">
                                                <b>Vehicle type: </b>
                                                @if ($alert->getVehicleType() == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::VEHICLE_TYPE_CAR)
                                                    Cars only
                                                @elseif ($alert->getVehicleType() == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::VEHICLE_TYPE_VAN)
                                                    Vans only
                                                @endif

                                            </div>
                                        @endif

                                        @if (count($alert->getCarSpecification()) > 0)
                                            <div class="spec-area">
                                                <b>Vehicle Specification:</b>
                                                <?php $printedSpecificationsCount = 0 ?>
                                                @foreach ($alert->getCarSpecification() as $specKey => $specContent)
                                                    <span>{{ $specKey }}: {{ $specContent }}</span> {{ (++$printedSpecificationsCount < count($alert->getCarSpecification())) ? '|' : '' }}
                                                @endforeach
                                            </div>
                                        @endif
                                    </td>
                                    <td style="font-weight: bold; text-transform: capitalize;">{{ $alert->getFrequency() }}</td>
                                    <td>
                                        <div class="clear"></div>
                                        <div class="matches-counter-area">
                                            <p class="matches-notification">
                                                <span class="matches-found">
                                                    <?php $matches = $alert->getActiveVehiclesCount($alert);?>
                                                    @if ($matches==1)
                                                        {{$matches}} match
                                                    @else
                                                        {{$matches}} matches
                                                    @endif

                                                </span>
                                                <span class="matches-short-text">
                                                     &nbsp;found
                                                </span>
                                                <span class="matches-full-text">
                                                     &nbsp;found with this alert
                                                </span>

                                                <a class="alerts-manage-button view-matches" href="{{ route('partnerGoodNewsLeads', $alert->getAsRequestParameters(false)) }}">
                                                    VIEW  <i class="fa fa-angle-right"></i>
                                                </a>
                                            </p>

                                        </div>
                                        <div class="actions-buttons-area">
                                            <div class="manage-button-area">
                                                <a class="alerts-manage-button edit" href="{{ route('marketplaceAlertsShowEditForm', [$alert]) }}">
                                                    EDIT<img style="vertical-align: middle; margin-left: 5px;" src="/images/icons/edit_icon.png" />
                                                </a>
                                            </div>
                                            <div class="manage-button-area">
                                                @if ($alert->isEnabled())
                                                    <form style="display: inline;" id="disable-alert-{{ $alert->getId() }}" method="post" action="{{ route('marketplaceAlertsDisable', ['alert' => $alert]) }}">
                                                        {{ csrf_field() }}
                                                        <a class="alerts-manage-button pause" href="{{ route('marketplaceAlertsShowEditForm', [$alert]) }}" onclick="event.preventDefault(); confirmAction(function() { document.getElementById('disable-alert-{{$alert->getId()}}').submit(); }, ' ');">
                                                            PAUSE<img style="vertical-align: middle; margin-left: 5px;" src="/images/icons/pause_icon.png" />
                                                        </a>
                                                    </form>
                                                @else
                                                    <form style="display: inline;" id="enable-alert-{{ $alert->getId() }}" method="post" action="{{ route('marketplaceAlertsEnable', ['alert' => $alert]) }}">
                                                        {{ csrf_field() }}
                                                        <a class="alerts-manage-button activate" href="{{ route('marketplaceAlertsShowEditForm', [$alert]) }}" onclick="event.preventDefault(); confirmAction(function() { document.getElementById('enable-alert-{{$alert->getId()}}').submit(); }, ' ');" >
                                                            ACTIVATE<img style="vertical-align: middle; margin-left: 5px;" src="/images/icons/play_icon.png" />
                                                        </a>
                                                    </form>
                                                @endif
                                            </div>
                                            <div class="manage-button-area">
                                                <form style="display: inline;" id="delete-alert-{{ $alert->getId() }}" method="post" action="{{ route('marketplaceAlertsDelete', ['alert' => $alert]) }}">
                                                    {{ csrf_field() }}
                                                    <a class="alerts-manage-button delete" href="{{ route('marketplaceAlertsShowEditForm', [$alert]) }}" onclick="event.preventDefault(); confirmAction(function() { document.getElementById('delete-alert-{{$alert->getId()}}').submit(); }, ' ');">
                                                        DELETE<img style="vertical-align: middle; margin-left: 5px;" src="/images/icons/delete_icon.png" />
                                                    </a>
                                                </form>
                                            </div>
                                        </div>


                                        <div class="clear"></div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div style="border: 0; min-height: 25px;">
                            {{ $alerts->appends(request()->query())->links('vendor.pagination.matrix-default') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
