<?php
$enabledAlertsOnly = isset($enabledAlertsOnly) ? $enabledAlertsOnly : false;
$disabledAlertsOnly = isset($disabledAlertsOnly) ? $disabledAlertsOnly : false;
?>

<div class="tab-buttons-container">
    <div class="clear"></div>
    <div class="tab-buttons-desktop">
        <a class="tab-button {{ $enabledAlertsOnly ? 'active' : '' }}" href="{{ route('marketplaceEnabledAlertsList') }}">ACTIVE ALERTS</a>
        <a class="tab-button {{ $disabledAlertsOnly ? 'active' : '' }}" href="{{ route('marketplaceDisabledAlertsList') }}">PAUSED ALERTS</a>
        <a class="tab-button green" href="{{ route('marketplaceAlertsShowCreateForm') }}">ADD ALERT +</a>
    </div>
    <div class="tab-buttons-mobile">
        <a class="tab-button {{ $enabledAlertsOnly ? 'active' : '' }}" href="{{ route('marketplaceEnabledAlertsList') }}">ACTIVE</a>
        <a class="tab-button {{ $disabledAlertsOnly ? 'active' : '' }}" href="{{ route('marketplaceDisabledAlertsList') }}">PAUSED</a>
        <a style="margin-right: 0;" class="tab-button green" href="{{ route('marketplaceAlertsShowCreateForm') }}">ADD+</a>
    </div>
    <div class="clear"></div>
</div>