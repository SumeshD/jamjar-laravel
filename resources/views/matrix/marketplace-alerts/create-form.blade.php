<?php /** @var \JamJar\Model\MarketplaceAlerts\MarketplaceAlert $alert */ ?>

@extends('layouts.app')

@section('content')

    @include('matrix.partials.header', ['title' => 'Marketplace'])

    <section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
        <div class="dashboard__container">
            <div class="dashboard__col">
                @include('matrix.partials.sidebar', ['active' => 'marketplace-alerts'])
            </div>
            <div class="dashboard__col">
                <div class="box box--with-header box--no-margin">
                    <div class="box__header header-theme-bidding">Marketplace Alerts</div>
                    @include('matrix.marketplace-alerts.buttons')
                    <div class="box__container">
                        <div class="box__content box__with_filtering">
                            <form action="{{ route('marketplaceAlertsProcessCreateForm') }}" method="POST" style="margin-bottom: 2rem;" class="form" id="filters-form">
                                {{ csrf_field() }}
                                <div style="margin: 20px 0; font-size: 18px; font-weight: bold;">
                                    Add an alert
                                </div>
                                <div class="col-4" style="font-size: 12px;">
                                    <div style="margin: 5px 0; font-weight: bold;">Alert name</div>
                                    <div style="margin-bottom: 20px;">Name your alert so you can identify it later</div>
                                    <div>
                                        <input style="width: 90%; background-color: #f6f6f6;" class="form__input" type="text" name="alert-name" value="{{ isset($alertName) ? $alertName : '' }}">
                                        @if ($errors->has('alert-name'))
                                            <div class="clear"></div>
                                            <div style="color: red;"><strong>{{ $errors->first('alert-name') }}</strong></div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-4" style="font-size: 12px;">
                                    <div style="margin: 5px 0; font-weight: bold;">Alert frequency</div>
                                    <div style="margin-bottom: 30px;">Get an alert for each vehicle or a daily summary</div>
                                    <div>
                                        <span style="margin-right: 40px; font-size: 17px;">
                                            <input type="radio" name="alert-frequency" value="instant" {{ (!old('alert-frequency') or old('alert-frequency') == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::FREQUENCY_INSTANT) ? 'checked' : (isset($lastAlert) && $lastAlert->frequency=='instant') ?  'checked' : '' }}>
                                            Instant
                                        </span>
                                        <span style="font-size: 17px;">
                                            <input type="radio" name="alert-frequency" value="daily" {{ old('alert-frequency') == \JamJar\Model\MarketplaceAlerts\MarketplaceAlert::FREQUENCY_DAILY ? 'checked' :  (isset($lastAlert) && $lastAlert->frequency=='daily') ?  'checked' : '' }}>
                                            Daily
                                        </span>
                                        @if ($errors->has('alert-frequency'))
                                            <div style="color: red; margin-top: 20px;"><strong>{{ $errors->first('alert-frequency') }}</strong></div>
                                        @endif
                                    </div>
                                </div>
                                <div style="margin-bottom: 30px;" class="clear"></div>

                                @include('matrix.leads.filters-container', [
                                    'showDates' => false,
                                    'showHeaderMenu' => false,
                                    'showApplyFiltersButton' => false,
                                    'showAlertButton' => false,
                                ])

                                <input type="submit" style="margin-top: 20px;" value="SAVE ALERT" class="button button--green-bg button--big">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection