@extends('matrix.website.index')

@section('title')
    {{$page->title}}
@endsection

@section('content')
    @if(str_contains($page->content,'Default content for Home'))
        <p><strong>The UK's professional online purchasers.</strong><br><br>
            Secure, Easy and Convenient.<br>
            Free UK collection.<br>
            Instant Bank transfer.
        </p>
    @else
        {!!$page->content!!}
    @endif
@endsection

@section('style')

    /* {{ $website->company->name }} Style Overrides */
    .navigation--matrix {
      background-color: #FFF;
      width: 100%;
      float: left;
      border-bottom: 1px solid #{{$website->bg_color}};
    }

    .navigation--matrix .navigation__link:hover, .navigation--matrix .navigation__link--is-active {
        color: #{{$website->bg_color}};
    }

    .button--matrix {
      font-weight: 500;
      background-color: #FFF;
      color: #{{$website->bg_color}};
      padding: 2rem 3rem;
    }

    .button--matrix:hover {
      color: #FFF;
      background-color: #{{$website->bg_color}};
    }

    .button--matrix-big {
      font-weight: 500;
      background-color: #FFF;
      color: #{{$website->bg_color}};
      padding: 5rem 10rem;
      text-transform: uppercase;
      text-align: center;
      font-size: 2rem;
      border: 1px solid #{{$website->bg_color}};
      width: 100%;
    }

    .button--matrix-big:hover, .button--matrix-big.is-active {
      color: #FFF;
      background-color: #{{$website->bg_color}};
    }

    .button--matrix-big:after {
      content: '\F00C    selected';
      font-family: 'FontAwesome', sans-serif;
      text-transform: none;
      color: #{{$website->brand_color}};
      font-weight: 400;
      visibility: hidden;
    }

    .button--matrix-big.is-active:after {
      content: '\F00C    selected';
      font-family: 'FontAwesome', sans-serif;
      text-transform: none;
      color: #{{$website->brand_color}};
      font-weight: 400;
      visibility: visible;
    }

    .form__submit--blue input[type="submit"] {
      background-color: #{{$website->bg_color}};
      border: 1px solid #{{$website->bg_color}};
      color: #FFF;
    }

    .form__submit--blue input[type="submit"]:hover {
      color: #{{$website->bg_color}};
    }

    .u-text-lightblue {
        color: #{{$website->bg_color}};
    }

    .box__content--split-list a {
        border-bottom: 1px solid #{{$website->bg_color}};
    }
@endsection
