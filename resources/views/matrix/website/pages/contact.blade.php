@extends('matrix.website.index')

@section('title')
    {{$page->title}}
@endsection

@section('content')
    @if(str_contains($page->content,'Default content for Contact'))
        <strong>Tel: <a href="tel:{{$website->company->telephone}}">{{$website->company->telephone}}</a></strong>
        <strong>Email: <a href="mailto:{{$website->company->user->profile->email}}">{{$website->company->user->profile->email}}</a></strong>
    @else
        <div class="matrix-content__content">
            <p>{!!$page->content!!}</p>

            <div class="matrix-content__col">
                <p><strong>Contact Details</strong></p>
                <ul class="matrix-content__list">
                    <li><strong>Telephone:</strong> <a href="tel:{{str_replace(' ', '+', $website->company->telephone)}}">{{$website->company->telephone}}</a></li>
                    <li><strong>Email:</strong> <a href="mailto:{{$website->company->user->profile->email}}">{{$website->company->user->profile->email}}</a></li>
                </ul>
                <p><strong>Address:</strong></p>
                <p>{{ $website->company->name }}</p>
                <p>{{ $website->company->address_line_one }}</p>
                <p>{{ $website->company->address_line_two }}</p>
                <p>{{ $website->company->city }}</p>
                <p>{{ $website->company->county }}</p>
                <p>{{ $website->company->postcode }}</p>
            </div>
            <div class="matrix-content__col">
                @if ($page->meta)
                    <p><strong>Opening Times</strong></p>
                    <ul class="matrix-content__list--table">
                        @foreach (unserialize($page->meta) as $day => $times)
                            <li>{{ucwords($day)}}</li>
                            @if (is_null($times['open']) OR is_null($times['close']))
                                <li>Closed</li>
                            @else
                                <li>{{ $times['open'] }} - {{ $times['close'] }}</li>
                            @endif
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="clear"></div>
        </div>
    @endif

@endsection

@section('style')

    /* {{ $website->company->name }} Style Overrides */
    .navigation--matrix {
      background-color: #FFF;
      width: 100%;
      float: left;
      border-bottom: 1px solid #{{$website->bg_color}};
    }

    .navigation--matrix .navigation__link:hover, .navigation--matrix .navigation__link--is-active {
        color: #{{$website->bg_color}};
    }

    .button--matrix {
      font-weight: 500;
      background-color: #FFF;
      color: #{{$website->bg_color}};
      padding: 2rem 3rem;
    }

    .button--matrix:hover {
      color: #FFF;
      background-color: #{{$website->bg_color}};
    }

    .button--matrix-big {
      font-weight: 500;
      background-color: #FFF;
      color: #{{$website->bg_color}};
      padding: 5rem 10rem;
      text-transform: uppercase;
      text-align: center;
      font-size: 2rem;
      border: 1px solid #{{$website->bg_color}};
      width: 100%;
    }

    .button--matrix-big:hover, .button--matrix-big.is-active {
      color: #FFF;
      background-color: #{{$website->bg_color}};
    }

    .button--matrix-big:after {
      content: '\F00C    selected';
      font-family: 'FontAwesome', sans-serif;
      text-transform: none;
      color: #{{$website->brand_color}};
      font-weight: 400;
      visibility: hidden;
    }

    .button--matrix-big.is-active:after {
      content: '\F00C    selected';
      font-family: 'FontAwesome', sans-serif;
      text-transform: none;
      color: #{{$website->brand_color}};
      font-weight: 400;
      visibility: visible;
    }

    .form__submit--blue input[type="submit"] {
      background-color: #{{$website->bg_color}};
      border: 1px solid #{{$website->bg_color}};
      color: #FFF;
    }

    .form__submit--blue input[type="submit"]:hover {
      color: #{{$website->bg_color}};
    }

    .u-text-lightblue {
        color: #{{$website->bg_color}};
    }

    .box__content--split-list a {
        border-bottom: 1px solid #{{$website->bg_color}};
    }
@endsection
