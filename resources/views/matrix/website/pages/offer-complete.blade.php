@extends('matrix.website.index')

@section('content')

    @php
        $location = $valuation->location ?? $valuation->company->location->first()
    @endphp

    <div class="matrix-intro clear offer-complete">
        <a href="javascript:window.print()" class="button button--orange-bg button--print">Print This Page</a>

        <h1 class="matrix-title">Thank you!</h1>
        <br>
        <p style="width: 100%; float: left;">You have accepted an offer of <strong>£{{$sale->price}}<sup>*</sup></strong> from <strong>{{$valuation->company->name}}</strong></h1>

    </div>

    <h2 class="matrix-sub-title">Your Details</h2>

    <div class="grid-2-col clear">

        <div class="grid-2-col__item split-list">
            <ul class="matrix-list clear">
                <li><span>Name:</span> <strong>{{ auth()->check() ? auth()->user()->name : '' }}</strong></li>
                <li><span>Email Address:</span> <strong>{{ auth()->check() ? auth()->user()->email : '' }}</strong></li>
            </ul>
        </div>
        <div class="grid-2-col__item split-list">
            <ul class="matrix-list clear">
                <li><span>Address:</span> <strong>{!! auth()->check() ? auth()->user()->profile->formatted_address : '' !!}</strong></li>
                <li><span>Contact Number:</span> <strong>{{ auth()->check() ? auth()->user()->profile->mobile_number : '' }}</strong></li>
            </ul>
        </div>

    </div>

    <h2 class="matrix-sub-title">Your Vehicle</h2>

    <div class="clear">

        <div class="matrix-content__header">
            <div class="matrix-content__car-logo" style="background-image: url({{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png);"></div>
            <h2 class="matrix-content__car-title">
                {{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}
                <div class="entity-name">{{ $vehicle->meta->derivative }}</div>
            </h2>


        </div>

        <div class="grid-2-col grid-2-col--padding clear" style="width: 80%;">
            <div class="grid-2-col__item split-list">
                <ul class="matrix-list clear">
                    <li><span class="details-label">Reg number:</span> <strong>{{ $vehicle->getNumberplate() }}</strong></li>
                    <li><span>Mileage:</span> <strong>{{ $vehicle->meta->mileage }}</strong></li>
                    <li><span>Service:</span> <strong>{{ $vehicle->meta->service_history }}</strong></li>
                    @if ($website->company->isVehicleConditionAvailable())
                        <li><span>Condition:</span><strong>{{ucfirst($valuation->sale->vehicle_condition)}}</strong></li>
                        @if($valuation->sale->dealer_notes)
                            <li><span>Condition Notes:</span><strong>{{$valuation->sale->dealer_notes}}</strong></li>
                        @endif
                    @endif
                </ul>
            </div>
            <div class="grid-2-col__item split-list">
                <ul class="matrix-list clear">
                    <li><span>MOT:</span> <strong>{{ $vehicle->meta->mot }}</strong></li>
                    <li><span>Write Off:</span> <strong>{{ $vehicle->meta->write_off == 0 ? 'No' : 'Yes'}}</strong></li>
                    <li><span>Non-Runner:</span> <strong>{{ $vehicle->meta->non_runner == 0 ? 'No' : 'Yes'}}</strong></li>
                </ul>
            </div>
        </div>

{{--         <div class="split-list">
            <ul class="matrix-list clear">
                <li>Preliminary Valuation*:</li>
                <li><strong>{{$valuation->value}}</strong></li>
            </ul>
        </div> --}}

    </div>

    <h2 class="matrix-sub-title">Additional details</h2>

    <div class="grid-2-col grid-2-col--padding clear" style="width: 80%;">
        <div class="grid-1-col__item split-list">
            <ul class="matrix-list clear">
                @if($vehicle->service_history_information)
                    <li><span>Additional service history information:</span> <strong>{{ $vehicle->service_history_information }}</strong></li>
                @endif

                @if($vehicle->additional_specification_information)
                    <li><span>Additional specification information:</span> <strong>{{ $vehicle->additional_specification_information }}</strong></li>
                @endif

                @if($vehicle->keys_count)
                    <li><span>Number of keys:</span> <strong>{{ $vehicle->keys_count }}</strong></li>
                @endif

            </ul>
        </div>

    </div>

    <h2 class="matrix-sub-title">Next Steps</h2>

    <div class="grid-2-col clear">
        <div class="grid-2-col__item">
            <ol class="matrix-numbered-list clear">
                @if ($valuation->sale->delivery_method == 'dropoff')
                    <li>
                        Please <a href="javascript:window.print()">print this page</a> and keep it with you during your drop off appointment for your own reference.
                    </li>
                @else
                    <li>
                        Please <a href="javascript:window.print()">print this page</a> and keep it with you during your collection appointment for your own reference.
                    </li>
                @endif

                @if ($valuation->sale->delivery_method == 'dropoff')
                    <li>
                        Please call {{$valuation->company->name}} on {!! $valuation->company->telephone !!}, quoting Jamjar and your vehicles registration number and they will be happy to discuss and confirm your payment details, required paperwork and all convenient drop-off time and date options.
                    </li>
                @else
                    <li>{{$valuation->company->name}} will contact you within 3 working hours and will be happy to discuss and confirm your payment details, required paperwork and also arrange your vehicles collection, time's and date's options.</li>
                @endif

                @if ($valuation->sale->delivery_method == 'dropoff')
                    <li>To the right is your most local {{$valuation->company->name}} drop off point.</li>
                @else
                    <li>{{$valuation->company->name}} currently has the following address as your preferred collection address. If this is no longer your preferred collection address, please discuss this when they contact you.</li>
                @endif
            </ol>
        </div>
        @if ($valuation->sale->delivery_method == 'dropoff')
        <div class="grid-2-col__item">
            <img class="matrix-content__map" src="http://maps.google.com/maps/api/staticmap?center={{urlencode($location->postcode)}}&zoom=14&size=400x400&maptype=roadmap&markers=color:ORANGE|label:A|{{urlencode($location->postcode)}}&sensor=false&key={{ config('googlemaps.key') }}">
        </div>
        @else
        <div class="grid-2-col__item">
            <img class="matrix-content__map" src="http://maps.google.com/maps/api/staticmap?center={{urlencode(auth()->user()->profile->postcode)}}&zoom=14&size=400x400&maptype=roadmap&markers=color:ORANGE|label:A|{{urlencode(auth()->user()->profile->postcode)}}&sensor=false&key={{ config('googlemaps.key') }}">
        </div>
        @endif


        <div class="col-12">
          <h5 class="matrix-sub-title" style="margin-top: 0px;">Dealer information</h5>
            {{-- <p><i class="fa fa-map-marker fa-2x u-text-lightblue"></i> Address</p> --}}
            <address>
                <p style="width:100%; margin-bottom:10px;"><strong>Contact: {{ $website->company->user->name }}</strong></p>
                <p><strong>{{$website->company->name}}</strong></p>
                <p>
                @if ($website->company->address_line_one)
                    {{ $website->company->address_line_one }},
                @endif
                @if ($website->company->address_line_two)
                    {{ $website->company->address_line_two }}
                @endif
                </p>
                <p>
                @if ($website->company->city)
                    {{ $website->company->city }}
                @endif
                </p>
                <p>
                @if ($website->company->county)
                    {{ $website->company->county }},
                @endif
                </p>
                <p>
                @if ($website->company->postcode)
                    {{ $website->company->postcode }}
                @endif
                </p>
            </address>
            <strong><a href="tel:{{ $website->company->telephone }}">{{ $website->company->telephone }}</a></strong>
        </div>


    </div>


    <div class="matrix-terms">
        <small>*Your final valuation is subject to change based on vehicle condition appraisal by dealer.</small>
    </div>

@endsection

@section('style')
.matrix-hero, .matrix-cta {
    display: none;
}


@page{
    margin:10px;
}

@media print {

   /* Set body padding to header and footer */
   body {
       padding: 40px 0;
   }


   /* All unecessary elements */
   .header--matrix {
        border-bottom: 0;
    }

    .navigation, .button, .matrix-footer {
        display: none;
    }
    .matrix-content__container {
        max-width: 100%;
        width: 100%;
    }

    h1,h2,h3,h4,h5,h6 {
        margin: 0.5rem 0;
    }

    .grid-2-col__item, .grid-2-col--padding .grid-2-col__item {
        width: 100%;
        float: left;
        margin: 0;
    }

    p,div,ul,li {
        display: inline-block;
        width: 100%;
    }
    .matrix-title {
        display: block;
        width: 100%;
        margin: 0;
        padding: 0;
    }
    body > section.matrix-content > div > div > div > div.matrix-intro.clear > h1 {
        display: none;
    }

    body > section.matrix-content > div > div > div > div:nth-child(5) > div.matrix-content__header > strong {
        display: block;
        width: 100%;
    }
    body > section.matrix-content > div > div > div > div:nth-child(7) > div:nth-child(2) {
        text-align:center;
        margin: 0 auto;
    }
    .matrix-content__map {
        float: none;
        width: 60%;
        margin: 0 auto;
        text-align:center;
    }

    /* Fix Header and footer class on page */
    .footer,
    .header {
        position: fixed;
        top: 0;
        left: 0;
        height: 40px;
        width: 100%;
        background-color: white;
        margin-left: 25px;
    }

    .footer{
        top: auto;
        bottom: 0;
    }

    @page
        counter-increment: page;
        counter-reset: page 1;
    }
    .footer:after {
        content: "Page " counter(page) " of " counter(pages);
        color: blue;
        float: right;
    }
}

@endsection
