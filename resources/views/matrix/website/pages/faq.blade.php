@extends('matrix.website.index')

@section('title')
    {{$page->title}}
@endsection

@section('content')
    @if(str_contains($page->content,'Default content for FAQ'))
        <p><h2>What do you do with my personal data?<br></h2><br>
            We need your email address as we send you the valuation link via email, once your purchase offer has been calculated.<br>

            We ask for your telephone number as we can send the valuation and any appointment information via SMS text messaging, so it is automatically stored on your phone.<br>

            It also makes communication much easier if we need to clarify any vehicle specification details before valuing your vehicle.<br>

            Please note, we fully adhere to the 2018 GDPR and Privacy guidelines and will not divulge any of your personal details for any other purpose other than the purchase of your car.<br><br>

        <h2>Why do you need my postcode?</h2><br>
            The correct postcode is vitally important to us. We can only accurately value your vehicle based on the postcode your vehicle needs collecting from.<br>

            It can be any mainland UK postcode but to accurately value your vehicle including our free collection service, we must know how far one of our 300 operators must travel to and from.<br>

            We try to offer the best price to all our visitors and giving us the correct postcode in the first instance will guarantee the logistics costs included in the price we have offered for your vehicle.<br>

            If the postcode is altered after initial valuation, then our valuation may also change.<br>

            Please note, we fully adhere to the 2018 GDPR and Privacy guidelines and will not divulge any of your personal details for any other purpose other than the purchase of your car.<br><br>


        <h2>How often will you contact me?</h2><br>
            If you have not accepted our valuation, we will contact you via email and SMS text messaging (if you have given us your mobile number) with details of your valuation and 1 follow up valuation reminder.<br>

            If you have agreed the sale of your vehicle, our chosen and normally our closest operator to your given postcode will contact you within 3 working hours of your confirmation of your intended sale to us, for collection arrangements and payment details directly with you.<br>

            Our operator may also speak to you during the process for additional direction details or any queries we have about vehicle paperwork or changes to the time schedules.<br>

            Please note, we fully adhere to the 1998 Data Protection Act and will not divulge any of your personal details for any other purpose other than the purchase of your car.<br><br>


        <h2>How long does the valuation remain valid?</h2><br>

            5 days.<br>

            If your valuation expires then please re-enter your registration number and we will re-value the vehicle again.<br><br>


        <h2>How do I get my Road Tax refunded?</h2><br>
            You are entitled to a road tax refund on any full remaining months left on your road tax, which will automatically be refunded to you by the DVLA.<br>

            They will send the refund to the car owner’s address on their records, so it’s important that you keep them up to date. You can also sell your car with no road tax on it.<br><br>


        <h2>Do you buy written-off and non-runner cars and vans?</h2><br>
            Yes, use this website to value your vehicle in the normal way, making sure you indicate your vehicle is an insurance write-off during the refine vehicle step of our valuation process, and you can easily sell your written off car or van to us.<br><br>



        <h2>I want to accept your offer, what do I do next?</h2><br>

            You simply press Accept at the purchase offer page and make your way through to the Handshake section which will take you 30 seconds to complete.<br>

            We do the rest.<br>

            We cannot accept your offer over the phone for you unfortunately. We have a company and privacy statement policy in place which means only the customer offering their vehicle can enter, edit and agree the value and/or sale of their vehicle.<br>

            Tell us when and where you would like us to collect the vehicle and we will do the rest including arranging payment for the vehicle.<br><br>


        <h2>What happens next?</h2><br>
            One of our 300 operators around the UK, (probably the closest one to your postcode entered) will contact you to arrange a convenient time and date for collection, normally within 3 working hours.<br>

            He or she will also confirm with you that all required paperwork is present with the vehicle including the vehicle’s V5c registration document and any valid MOT or Service History you have entered previously that the vehicle has present.<br>

            An HPi check will be carried out behind the scenes on your vehicle and our operator will arrange and confirm bank transfer details with you for your 3 minute free bank transfer once this has been completed.<br>

            *We cannot guarantee bank transfer for all scrap or end of life purchased vehicles.<br><br>


        <h2>Will you attempt to take my vehicle before I've received payment?</h2><br>
            No, we will never attempt to take the vehicle until you are happy you have been paid for the vehicle.<br><br>


        <h2>I am not the legal owner or keeper of the vehicle, can you still pay me?</h2><br>
            Unless you are acting as an agent or an executor of an estate on behalf of the owner, and have relevant paperwork we can only make payment to the legal owners and registered keeper's instructed bank account.<br>

            We can only pay monies to the legal owner of the vehicle.<br><br>


        <h2>Can you collect, evenings and weekends?</h2><br>
            Yes of course and payments can also be made at any time.<br>

            Please bear in mind that weekends are usually booked first so please be patient if you cannot get a weekend appointment.<br>

            However we will endeavour to collect and pay for the vehicle at a convenient time for you.<br><br>


        <h2>I have an outstanding finance agreement on the vehicle, can I still sell you the vehicle?</h2><br>
            This is not a problem and will not affect the purchase offer.<br>

            We will pay you for the vehicle via bank transfer in the usual way but reduce the balance given to you by the current outstanding value of the finance settlement still owed to the relevant bank, finance or leasing company.<br>

            The total outstanding finance settlement figure will be sent to the relevant bank, finance or leasing company directly then giving us full title to the vehicle.<br>

            You will need to provide us an up to date finance settlement figure at the time our operator contacts you to arrange collection and payment details with you so we can calculate how much we are paying you and how much we are paying the outstanding bank, finance or leasing company.<br>

            We do not charge any additional fees for sending an additional payment to the bank, finance or leasing company.<br><br>


        <h2>The outstanding amount remaining is more than the vehicle is worth?</h2><br>
            This will not affect the purchase offer.<br>

            We will settle the outstanding finance in full, however as this would result in us holding the negative equity we require the following; In these circumstances we require payment by you to cover this negative equity which will be needed by bank transfer before we pay the whole outstanding finance due to the relevant bank, finance or leasing company.<br>

            We will then settle the total outstanding finance settlement to the relevant bank, finance or leasing company directly and in full.<br>

            You will need to provide us an up to date finance settlement figure at the time our operator contacts you to arrange collection and payment details with you so we can calculate how much we require from you and how much we are paying the outstanding bank, finance or leasing company. We do not charge any additional fees for sending payment to the bank, finance or leasing company.<br><br>



        <h2>Who needs to gain the outstanding finance settlement figure from my Finance company?</h2><br>
            You will have to gain the outstanding finance settlement figure from your relevant bank, finance or leasing company as they are not legally allowed to give us details of your account with them.<br>

            We automatically check each vehicle for any outstanding finance agreements registered against it, using either HPi and/or Experian data checks so it will be impossible for us to purchase your vehicle until we can guarantee the outstanding finance has been settled or cleared from their registers.<br>

            A representative will contact you shortly after acceptance of the agreed sale and will talk you through the required process.<br>
        </p>
    @else
        {!!$page->content!!}
    @endif

@endsection

@section('style')

    /* {{ $website->company->name }} Style Overrides */
    .navigation--matrix {
      background-color: #FFF;
      width: 100%;
      float: left;
      border-bottom: 1px solid #{{$website->bg_color}};
    }

    .navigation--matrix .navigation__link:hover, .navigation--matrix .navigation__link--is-active {
        color: #{{$website->bg_color}};
    }

    .button--matrix {
      font-weight: 500;
      background-color: #FFF;
      color: #{{$website->bg_color}};
      padding: 2rem 3rem;
    }

    .button--matrix:hover {
      color: #FFF;
      background-color: #{{$website->bg_color}};
    }

    .button--matrix-big {
      font-weight: 500;
      background-color: #FFF;
      color: #{{$website->bg_color}};
      padding: 5rem 10rem;
      text-transform: uppercase;
      text-align: center;
      font-size: 2rem;
      border: 1px solid #{{$website->bg_color}};
      width: 100%;
    }

    .button--matrix-big:hover, .button--matrix-big.is-active {
      color: #FFF;
      background-color: #{{$website->bg_color}};
    }

    .button--matrix-big:after {
      content: '\F00C    selected';
      font-family: 'FontAwesome', sans-serif;
      text-transform: none;
      color: #{{$website->brand_color}};
      font-weight: 400;
      visibility: hidden;
    }

    .button--matrix-big.is-active:after {
      content: '\F00C    selected';
      font-family: 'FontAwesome', sans-serif;
      text-transform: none;
      color: #{{$website->brand_color}};
      font-weight: 400;
      visibility: visible;
    }

    .form__submit--blue input[type="submit"] {
      background-color: #{{$website->bg_color}};
      border: 1px solid #{{$website->bg_color}};
      color: #FFF;
    }

    .form__submit--blue input[type="submit"]:hover {
      color: #{{$website->bg_color}};
    }

    .u-text-lightblue {
        color: #{{$website->bg_color}};
    }

    .box__content--split-list a {
        border-bottom: 1px solid #{{$website->bg_color}};
    }
@endsection
