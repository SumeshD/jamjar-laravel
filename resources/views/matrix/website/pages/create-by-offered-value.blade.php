@extends('matrix.website.index')

@section('title')
    {{$page->title}}
@endsection

@section('contenttitle')
    Confirm Your Valuation Options
@endsection

@section('content')
    <?php /** @var \JamJar\Services\Valuations\OfferedValues\OfferedValueInterface $offeredValue */ ?>

    @if ($offeredValue->isDropOffAvailable())
        <div class="matrix-content__col"  @if (!$offeredValue->isCollectionAvailable()) style="width: 100%" @endif>
            <div class="button button--matrix-big is-active" id="dropoff">
                You Deliver *
                <?php $priceGreat = $offeredValue->getFinalDropOffPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_GREAT) / 100 ?>
                <?php $priceGood = $offeredValue->getFinalDropOffPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_GOOD) / 100 ?>
                <?php $priceFair = $offeredValue->getFinalDropOffPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_FAIR) / 100 ?>
                <?php $pricePoor = $offeredValue->getFinalDropOffPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_POOR) / 100 ?>
                <p class="price is-active" id="dropGreat" data-price="{{$priceGreat}}">
                    &pound;{{number_format($priceGreat)}}
                </p>
                <p class="price u-hide" id="dropGood" data-price="{{$priceGood}}">
                    &pound;{{number_format($priceGood)}}
                </p>
                <p class="price u-hide" id="dropFair" data-price="{{$priceFair}}">
                    &pound;{{number_format($priceFair)}}
                </p>
                <p class="price u-hide" id="dropPoor" data-price="{{$pricePoor}}">
                    &pound;{{number_format($pricePoor)}}
                </p>
            </div>
        </div>
    @endif
    @if ($offeredValue->isCollectionAvailable())
        <div class="matrix-content__col" @if (!$offeredValue->isDropOffAvailable()) style="width: 100%" @endif>
            <div class="button button--matrix-big @if (!$offeredValue->isDropOffAvailable()) is-active @endif" id="collection">
                We Collect *
                <?php $priceGreat = $offeredValue->getFinalCollectionPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_GREAT) / 100 ?>
                <?php $priceGood = $offeredValue->getFinalCollectionPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_GOOD) / 100 ?>
                <?php $priceFair = $offeredValue->getFinalCollectionPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_FAIR) / 100 ?>
                <?php $pricePoor = $offeredValue->getFinalCollectionPriceInPence(\JamJar\Services\Valuations\OfferedValues\OfferedValueInterface::VEHICLE_CONDITION_POOR) / 100 ?>
                <p class="price is-active" id="collGreat" data-price="{{$priceGreat}}">
                    &pound;{{number_format($priceGreat)}}
                </p>
                <p class="price u-hide" id="collGood" data-price="{{$priceGood}}">
                    &pound;{{number_format($priceGood)}}
                </p>
                <p class="price u-hide" id="collFair" data-price="{{$priceFair}}">
                    &pound;{{number_format($priceFair)}}
                </p>
                <p class="price u-hide" id="collPoor" data-price="{{$pricePoor}}">
                    &pound;{{number_format($pricePoor)}}
                </p>
            </div>
        </div>
    @endif

    @if (($website->company->isVehicleConditionAvailable()))
        <div class="clear"></div>
        <div class="divider"></div>

        <h4 class="u-text-center">Adjust valuation based on your vehicles condition.</h4>

        <input type="text" id="appraisalSlider" name="appraisalSlider" class="appraisalSlider">

        <div class="u-text-center">
            <p class="u-hide appraisal-text js-appraisal"  id="js-appraisal-Poor">
                Poor condition - i.e. heavily dented, heavy scratches, illegal tyres, poor interior condition.
            </p>
            <p class="u-hide appraisal-text js-appraisal"  id="js-appraisal-Fair">
                Fair condition - i.e 2 or 3 heavier scratches, couple of tyres below legal limit, small dents.
            </p>
            <p class="u-hide appraisal-text js-appraisal"  id="js-appraisal-Good">
                Good condition - Just a couple of slight defects - i.e. one or two ding dents or scuffed alloy wheels, or a light scratch.
            </p>
            <p class="appraisal-text js-appraisal"  id="js-appraisal-Great">
                Great condition - No damage, showroom condition.
            </p>

            <div class="not-accepted-condition-message alert alert-danger" style="display: none;"></div>
        </div>
    @endif
    <div class="divider"></div>

    <div class="row">
        <p>* the price includes all fees below</p>
        <div class="col-12 fifty-fifty">
            <p class="u-font-bold" style="font-size: 2rem; width:50%; float:left; padding-left:5px;">
                <i class="fa fa-check"></i> {{$website->company->valuation_validity}} Quote Guarantee
            </p>

            <p class="u-font-bold" style="font-size: 2rem; width:50%; float:left; padding-left:5px;">
                @if ($offeredValue->getBankTransferFeeInPence())
                    <i class="fa fa-check"></i> &pound;{{number_format($offeredValue->getBankTransferFeeInPence() / 100, 2)}} Bank Transfer Fee
                @else
                    <i class="fa fa-check"></i> Free Bank Transfer
                @endif
            </p>

            @if ($offeredValue->getAdminFeeInPence())
                <p class="u-font-bold" style="font-size: 2rem; width:50%; float:left; padding-left:5px;">
                    <i class="fa fa-check"></i> &pound;{{number_format($offeredValue->getAdminFeeInPence() / 100, 2)}} Admin Fee
                </p>
            @endif

            @if ($offeredValue->isCollectionAvailable())
                <p class="u-font-bold" style="font-size: 2rem; width:50%; float:left; padding-left:5px;">
                    @if ($offeredValue->getCollectionFeeInPence())
                        <i class="fa fa-check"></i> &pound;{{number_format($offeredValue->getCollectionFeeInPence() / 100, 2)}} Collection Fee
                    @else
                        <i class="fa fa-check"></i> Free Collection
                    @endif
                </p>
            @endif

            @if ($offeredValue->isDropOffAvailable())
                <p class="u-font-bold" style="font-size: 2rem; width:50%; float:left; padding-left:5px;">
                    @if ($offeredValue->getDropOffFeeInPence())
                        <i class="fa fa-check"></i> &pound;{{number_format($offeredValue->getDropOffFeeInPence() / 100, 2)}} Drop Off Fee
                    @else
                        <i class="fa fa-check"></i> Free Drop Off
                    @endif
                </p>
            @endif
        </div>
    </div>
</section>

<section class="matrix-content" style="background-color: #{{ $website->bg_color }};">
    <div class="matrix-content__container">
        <div class="box">
            <h1 class="matrix-content__title">Confirm Your Details</h1>
            <div class="box__content">

                <div>
                    <div class="matrix-content__car-logo" style="background-image: url({{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png);"></div>
                    <h2 class="matrix-content__car-title">
                        {{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}
                    </h2>
                </div>

                <div class="grid-2-col clear">
                    <div class="grid-2-col__item">
                        <ul class="split-list" style="width:100%;">
                            <li><strong>Mileage:</strong></li>
                            <li>{{ $vehicle->meta->mileage }}</li>
                            <li><strong>Previous Owners:</strong></li>
                            <li>{{trans('vehicles.owners_count.owners_'.($vehicle->meta->number_of_owners <= 5 ? $vehicle->meta->number_of_owners : 6)) }}</li>
                            <li><strong>Service:</strong></li>
                            <li>{{ $vehicle->meta->service_history }}</li>
                        </ul>
                    </div>

                    <div class="grid-2-col__item">
                        <ul class="split-list" style="width:100%;">
                            <li><strong>MOT:</strong></li>
                            <li>{{ $vehicle->meta->mot }}</li>
                            <li><strong>Write Off:</strong></li>
                            <li>{{ $vehicle->meta->write_off == 0 ? 'No' : 'Yes'}}</li>
                            <li><strong>Non Runner:</strong></li>
                            <li>{{ $vehicle->meta->non_runner == 0 ? 'No' : 'Yes'}}</li>
                        </ul>
                    </div>
                </div>

            </div>

            <div class="box__content">
                <?php $action = $offeredValue instanceof \JamJar\Model\ValuationDraft ? 'matrixValuationDraftsSubmitDraftAcceptForm' : 'matrixValuationDraftsSubmitValuationAcceptForm' ?>
                <form action="{{ route($action, [$offeredValue->id]) }}" method="POST" class="form" onsubmit="$(this).find('[type=submit]').val('Processing...').prop('disabled', true);">
                    {{csrf_field()}}
                    <input type="hidden" id="vid" name="vid" value="{{ encrypt($offeredValue->id) }}">
                    <input type="hidden" id="price" name="price" value="{{ $offeredValue->price_in_pence / 100 }}">

                    @if (!$offeredValue->isDropOffAvailable())
                        <input type="hidden" id="deliveryMethod" name="deliveryMethod" value="collection">
                    @else
                        <input type="hidden" id="deliveryMethod" name="deliveryMethod" value="dropoff">
                    @endif
                    <input type="hidden" id="condition" name="condition" value="great">

                    <div class="form__group form__group--half">
                        <label for="firstname" class="form__label">First Name</label>
                        <input type="text" name="firstname" id="firstname" class="form__input" value="{{ auth()->user()->first_name ?? old('firstname') }}" required>
                    </div>
                    <div class="form__group form__group--half">
                        <label for="lastname" class="form__label">Last Name</label>
                        <input type="text" name="lastname" id="lastname" class="form__input" value="{{ auth()->user()->last_name ?? old('lastname') }}" required>
                    </div>
                    <div class="form__group form__group--half">
                        <label for="address_line_one" class="form__label">Address Line 1</label>
                        <input type="text" name="address_line_one" id="address_line_one" class="form__input" value="{{ auth()->user()->profile->address_line_one ?? old('address_line_one') }}" required>
                    </div>
                    <div class="form__group form__group--half">
                        <label for="address_line_two" class="form__label">Address Line 2</label>
                        <input type="text" name="address_line_two" id="address_line_two" class="form__input" value="{{ auth()->user()->profile->address_line_two ?? old('address_line_two') }}">
                    </div>
                    <div class="form__group form__group--half">
                        <label for="town" class="form__label">Town</label>
                        <input type="text" name="town" id="town" class="form__input" value="{{ auth()->user()->profile->town ?? old('town') }}">
                    </div>
                    <div class="form__group form__group--half">
                        <label for="postcode" class="form__label">Postcode</label>
                        <input type="text" name="postcode" id="postcode" class="form__input" value="{{ auth()->user()->profile->postcode ?? old('postcode') }}" required>
                    </div>
                    <div class="form__group form__group--half">
                        <label for="telephone_number" class="form__label">Mobile Number</label>
                        <input type="tel" name="telephone_number" id="telephone_number" class="form__input" value="{{ auth()->user()->profile->mobile_number ?? old('telephone_number') }}" required>
                    </div>
                    <div class="form__group form__group--half">
                        <label for="email" class="form__label">Email Address</label>
                        <input type="email" name="email" id="email" class="form__input" value="{{  auth()->user()->email ?? old('email') }}" required>
                    </div>

                    <div class="form__submit form__submit--blue">
                        <input type="submit" class="button button--blue button-ac" value="Complete & Accept">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('style')

    /* {{ $website->company->name }} Style Overrides */

	* {
		font-family: "Open Sans", sans-serif;
	}


    .navigation--matrix {
      border-bottom: 1px solid #{{$website->bg_color}};

      background-color: #FFF;
      width: 100%;
      float: left;
    }

    .navigation--matrix
    .navigation__link:hover,
    .navigation--matrix
    .navigation__link--is-active {
        color: #{{$website->bg_color}};
    }

    .button--matrix {
      color: #{{$website->bg_color}};

      font-weight: 500;
      background-color: #FFF;
      padding: 2rem 3rem;
    }

    .button--matrix:hover {
      background-color: #{{$website->bg_color}};

      color: #FFF;
    }

    .button--matrix-big {
      color: #{{$website->bg_color}};
      border: 1px solid #{{$website->bg_color}};

 {{--      font-weight: 500;
      background-color: #FFF;
      text-transform: uppercase;
      text-align: center;
      font-size: 2rem;
      width: 100%; --}}
    }

    .button--matrix-big:hover,
    .button--matrix-big.is-active {
      background-color: #{{$website->bg_color}};

      color: #FFF;
    }

    .button--matrix-big:after {
      color: #{{$website->brand_color}};

      content: '\F00C    selected';
      font-family: 'FontAwesome', sans-serif;
      text-transform: none;
      font-weight: 400;
      visibility: hidden;
    }

    .button--matrix-big.is-active:after {
      color: #{{$website->brand_color}};

      content: '\F00C    selected';
      font-family: 'FontAwesome', sans-serif;
      text-transform: none;
      font-weight: 400;
      visibility: visible;
    }

    .form__submit--blue input[type="submit"] {
      background-color: #{{$website->bg_color}};
      border: 1px solid #{{$website->bg_color}};

      color: #FFF;
    }

    .form__submit--blue input[type="submit"]:hover {
      color: #{{$website->bg_color}};
    }

    .u-text-lightblue {
        color: #{{$website->bg_color}};
    }

    .box__content--split-list a {
        border-bottom: 1px solid #{{$website->bg_color}};
    }

    .matrix-hero {
        display: none;
    }

    .irs-line {
        border: none;
        background: #e1e1e1;
    }
    .irs-bar {
        border-top: none;
        border-bottom: none;
        background: #00aeef;
    }
    .irs-bar-edge {
        border: none;
        background: #00aeef;

    }
    .irs-from,
    .irs-to,
    .irs-single {
        background: #00aeef;
    }
    .irs-slider {
        background: #00aeef;
        border: none;
    }
    .irs-grid-pol.small {
        display: none;
    }
@endsection
