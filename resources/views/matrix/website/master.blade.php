<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
                window.Laravel = <?php echo json_encode([
                    'csrfToken' => csrf_token(),
                ]); ?>
        </script>

        <title>{{ $title }}</title>

        <!-- Styles -->
        <link href="{{ secure_asset('css/vendor.css') }}" rel="stylesheet">
        <link href="{{ secure_asset('css/style.css') }}" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">

        <style>
            {{$style}}
        </style>
    </head>

    <body>

        <header class="header header--matrix">
            <div class="header__container">
                <div class="header__logo">
                    <a href="{{ $homeLink }}">
                        <img src="{{ $logo }}?ts={{ time() }}" alt="{{ $title }}">
                    </a>
                </div>
            </div>
        </header>
        
        <div class="blackout"></div>

        <nav class="navigation navigation--matrix">
            <div class="navigation__container">
                {{ $nav }}
            </div>
        </nav>
        
        <section class="matrix-hero-new" style="background-image:url({{$hero}}?ts={{ time() }});"></section>

        <section class="matrix-content" style="background-color: {{ $bgcolor }};">
            <div class="matrix-content__container">
                <div class="matrix-content__box">
                    {{$slot}}
                </div>
            </div>
        </section>

        @if (session('valuation') && (!$yourOfferPage))
        <section class="matrix-cta" style="background-color: {{ $brandcolor }};">
            <div class="matrix-cta__container">
                <div class="matrix-cta__col">
                    <p>Your Offer - Up To</p>
                    @if ( session('valuation')['dropoff_value'] == 0)
                    <p class="footer-price">&pound;{{ number_format(session('valuation')['collection_value']) }}</p>
                    @else
                    <p class="footer-price">&pound;{{ number_format(session('valuation')['dropoff_value']) }}</p>
                    @endif
                </div>
                <div class="matrix-cta__col">
                    <a href="your-offer" class="button button--matrix">View Offer</a>
                </div>
            </div>
        </section>
        @endif

        <footer class="matrix-footer">
            <div class="matrix-footer__container">
                <div class="matrix-footer__col">
                    <img src="{{ $logo }}?ts={{ time() }}" alt="{{ $title }}">
                </div>
                <div class="matrix-footer__col">
                    <p>For more details read our <a href="terms-and-conditions">terms and conditions</a></p>
                </div>
            </div>
        </footer>

        <!-- Scripts -->
        <script src="{{ secure_asset('js/vendor.js') }}"></script>
        <script src="{{ secure_asset('js/app.js') }}"></script>
        @yield('scripts')
        
        @include('sweet::alert')
    </body>
</html>
