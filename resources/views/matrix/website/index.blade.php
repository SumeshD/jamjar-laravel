<?php $yourOfferPage = isset($yourOfferPage) ?>
@component('matrix.website.master', ['yourOfferPage' => $yourOfferPage])
    @slot('style')
        @yield('style')
    @endslot

    @slot('title')
        {{ $website->company->name }}
    @endslot

    @slot('logo')
        @if($website->logo_image && $website->logo_image!='' && $website->logo_image!='partner-websites/default/jamjar_partner.jpg')
            {{ $website->logo_image }}
        @else
            {{Request::root()}}/images/jamjar_partner2.jpg
        @endif
    @endslot

    @slot('homeLink')
        {{ route('partnerWebsiteIndex', $website) }}
    @endslot

    @slot('nav')
        <ul class="navigation__items">
            @foreach($website->pages as $page)
                @if ($page->active == 1)
                    @if (in_array($page->slug, ['terms-and-conditions','offer-complete']))
                        @continue
                    @endif

                    @if (($page->slug === 'your-offer') && (!session('valuation')))
                        @continue
                    @endif

                <li class="navigation__item">
                    <a
                        href="{{ route('partnerWebsitePage', [$website, $page]) }}"
                        class="
                            navigation__link
                            {{ strpos(Request::path(), $page->slug) !== false ? 'navigation__link--is-active' : '' }}
                        "
                    >
                        {{ $page->title }}
                    </a>
                </li>
                @endif
            @endforeach
        </ul>
    @endslot

    @slot('hero')
        @if($website->hero_image && $website->hero_image!='' && $website->hero_image!='partner-websites/default/hero.jpg')
            {{ $website->hero_image }}
        @else
            {{Request::root()}}/images/default.jpg
        @endif

    @endslot

    @slot('bgcolor')
        #{{ $website->bg_color }}
    @endslot

    @slot('brandcolor')
        #{{ $website->brand_color }}
    @endslot

    {{-- below is the $slot default --}}
    {{-- <h2 class="matrix-content__title">@yield('contenttitle', '')</h2> --}}
    <div class="matrix-content__content">
        @yield('content')
    </div>
@endcomponent
