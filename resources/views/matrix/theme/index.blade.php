@extends('layouts.app')

@section('content')

@include('matrix.partials.header', [
    'title' => 'Valuation Template',
    'buttons' => '
        <a href="'.route('partnerWebsiteIndex', auth()->user()->website).'" target="_blank" class="button button--green-bg u-pull-right"><i class="fa fa-eye"></i> View Website</a>
    ',
])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'theme'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header box--no-margin">
                <div class="box__header">Edit Template</div>
                    <form action="{{route('partnerWebsiteSave')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box__container">
                            <div class="box__item">
                                <!-- Form Group -->
                                <h3 class="box__title box__title--no-border">Upload Your Company Logo</h3>
                                <div class="form__group">
                                    <label for="logo_image" class="form__label">Choose File</label>
                                    <input type="file" class="form__input" name="logo_image" id="logo_image">
                                </div>
                                @if (auth()->user()->website->logo_image != null)
                                @if (auth()->user()->website->logo_image)
                                <div class="uploadedfile uploadedfile--logo">
                                    <div class="uploadedfile__image" style="background-image: url({{ auth()->user()->website->logo_image }}?ts={{time()}}); height: 100px;"></div>
                                    <div class="uploadedfile__link" style="height: 100px;">
                                        <a href="{{ auth()->user()->website->logo_image }}" class="button button--orange">View Full</a>

                                        <a class="u-text-red u-pull-right js-remove-logo">remove file</a>
                                    </div>
                                </div>
                                @endif
                                @endif
                                <div class="divider"></div>
                                <!-- End Form Group -->

                                <!-- Form Group -->
                                <h3 class="box__title box__title--no-border">Upload Your Header Image</h3>
                                <div class="form__group">
                                    <label for="hero_image" class="form__label">Choose File</label>
                                    (For best outcome please use 1920x500 image resolution)
                                    <input type="file" class="form__input" name="hero_image" id="hero_image">
                                </div>
                                @if (auth()->user()->website->hero_image != null)
                                @if (auth()->user()->website->hero_image)
                                <div class="uploadedfile uploadedfile--hero">
                                    <div class="uploadedfile__image" style="background-image: url({{ auth()->user()->website->hero_image }}?ts={{time()}}); height: 100px;"></div>
                                    <div class="uploadedfile__link" style="height: 100px;">
                                        <a href="{{ auth()->user()->website->hero_image }}" class="button button--orange">View Full</a>

                                        <a class="u-text-red u-pull-right js-remove-hero">remove file</a>

                                    </div>
                                </div>
                                @endif
                                @endif
                                <div class="divider"></div>
                                <!-- End Form Group -->

                                <!-- Form Group -->
                                <h3 class="box__title box__title--no-border">Choose Theme Colours</h3>
                                <div class="box__content">
                                    <div class="color-block">
                                        <p class="color-block__title">Main Brand Colour</p>
                                        <input id="brand_color" name="brand_color" class="color-block__item jscolor form__input"  value="{{ auth()->user()->website->brand_color }}">
                                    </div>
                                    
                                    <div class="color-block">
                                        <p class="color-block__title">Background Colour</p>
                                        <input id="bg_color" name="bg_color" class="color-block__item jscolor form__input" value="{{ auth()->user()->website->bg_color }}">
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <!-- End Form Group -->
                            </div>
                            <div class="box__footer box__footer--no-border">
                                <button type="submit" class="button button--big button--orange-bg">Save Template</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="box box--with-header">
                    <div class="box__header">Website Pages</div>
                    <ul class="box__tabs">
                        <li class="box__tab box__tab--active">
                            <a href="#toggleTab" class="box__tab-link" data-tab="home">
                                Home
                            </a>
                        </li>
                        <li class="box__tab">
                            <a href="#toggleTab" class="box__tab-link" data-tab="faq">
                                FAQ
                            </a>
                        </li>
                        <li class="box__tab">
                            <a href="#toggleTab" class="box__tab-link" data-tab="contact">
                                Contact
                            </a>
                        </li>
                        <li class="box__tab">
                            <a href="#toggleTab" class="box__tab-link" data-tab="terms">
                                Terms &amp; Conditions
                            </a>
                        </li>
{{--                         
                        <li class="box__tab">
                            <a href="#toggleTab" class="box__tab-link" data-tab="salesagreed">
                                Sales Agreed Email
                            </a>
                        </li> 
--}}
                    </ul>

                    <!-- Tab Group -->
                    <div class="box__content js-tabcontent-home">
                        <div class="box__container">
                            <form action="{{ route('partnerWebsitePageSave', [auth()->user()->website, 'home']) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="onoffswitch" value="true">

                                <!-- Form Group -->
                                <h3 class="box__title box__title--no-border">Homepage</h3>
                                <div class="form__group">
                                    <label for="content" class="form__label">Information</label>
                                    <textarea rows="5" class="form__input js-editor" name="content" id="content">{{auth()->user()->website->getPage('home')->content }}</textarea>
                                </div>
                                <div class="divider"></div>
                                <div class="box__footer box__footer--no-border">
                                    <button type="submit" class="button button--big button--orange-bg">Save Page</button>
                                </div>
                                <!-- End Form Group -->
                            </form>
                        </div>
                    </div>
                    <!-- End Tab Group -->

                    <!-- Tab Group -->
                    <div class="box__content u-hide js-tabcontent-faq">
                        <div class="box__container">
                            <form action="{{ route('partnerWebsitePageSave', [auth()->user()->website, 'faq']) }}" method="POST">
                                {{ csrf_field() }}

                                <!-- Form Group -->
                                <h3 class="box__title box__title--no-border">FAQ</h3>

                                <div class="onoff">
                                    <div class="onoff__col">
                                        <span class="onoff__label">Display?</span>
                                    </div>
                                    <div class="onoff__col">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="displaySection" checked>
                                            <label class="onoffswitch-label" for="displaySection">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form__group">
                                    <label for="content" class="form__label">
                                        Information
                                    </label>

                                    <textarea rows="5" class="form__input js-editor" name="content" id="content">{{auth()->user()->website->getPage('faq')->content }}</textarea>
                                </div>
                                <div class="divider"></div>
                                <div class="box__footer box__footer--no-border">
                                    <button type="submit" class="button button--big button--orange-bg">Save Page</button>
                                </div>
                                <!-- End Form Group -->
                            </form>
                        </div>
                    </div>
                    <!-- End Tab Group -->

                    <!-- Tab Group -->
                    <div class="box__content u-hide js-tabcontent-contact">
                        <div class="box__container">
                            <!-- Form Group -->
                            <form action="{{ route('partnerWebsitePageSave', [auth()->user()->website, 'contact']) }}" method="POST" class="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="onoffswitch" value="true">

                                <h3 class="box__title box__title--no-border">Contact</h3>
                                <div class="form__group">
                                    <label for="content" class="form__label">
                                        Contact Details
                                    </label>
                                    <textarea rows="5" class="form__input js-editor" name="content" id="content">{{auth()->user()->website->getPage('contact')->content }}</textarea>
                                </div>

                                <div class="col-2"><strong>Day</strong></div>
                                <div class="col-5"><strong>Opening Time</strong></div>
                                <div class="col-4"><strong>Closing Time</strong></div>

                                <div class="divider"></div>

                                @foreach(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] as $day)
                                <div class="col-2">
                                    <strong>{{$day}}</strong>
                                </div>
                                <div class="col-5" style="padding: 0 1rem;">
                                    <div class="form__group">
                                        <input 
                                            type="text" 
                                            name="openingtimes[{{strtolower($day)}}][open]" 
                                            id="openingtimes[{{strtolower($day)}}][open]" 
                                            class="form__input" 
                                            placeholder="09:00" 
                                            value="{{ array_get($openingtimes, strtolower($day).'.open') }}"
                                        >
                                    </div>
                                </div>
                                <div class="col-5" style="padding: 0 1rem;">
                                    <div class="form__group">
                                        <input 
                                            type="text" 
                                            name="openingtimes[{{strtolower($day)}}][close]" 
                                            id="openingtimes[{{strtolower($day)}}][close]" 
                                            class="form__input" 
                                            placeholder="17:00" 
                                            value="{{ array_get($openingtimes, strtolower($day).'.close') }}"
                                        >
                                    </div>
                                </div>
                                    
                                @endforeach

                                <div class="divider"></div>
                                <div class="box__footer box__footer--no-border">
                                    <button type="submit" class="button button--big button--orange-bg">Save Page</button>
                                </div>
                            </form>
                            <!-- End Form Group -->
                        </div>
                    </div>
                    <!-- End Tab Group -->

                    <!-- Tab Group -->
                    <div class="box__content u-hide js-tabcontent-terms">
                        <div class="box__container">
                            <!-- Form Group -->
                            <form action="{{ route('partnerWebsitePageSave', [auth()->user()->website, 'terms-and-conditions']) }}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="onoffswitch" value="false">
                                <h3 class="box__title box__title--no-border">Terms and Conditions</h3>
                                <div class="form__group">
                                    <label for="termscontent" class="form__label">Information</label>
                                    <textarea rows="5" class="form__input js-editor" name="content" id="termscontent">{{ auth()->user()->website->getPage('terms-and-conditions')->content }}</textarea>
                                </div>
                                <div class="divider"></div>
                                <div class="box__footer box__footer--no-border">
                                    <button type="submit" class="button button--big button--orange-bg">Save Page</button>
                                </div>
                            </form>
                            <!-- End Form Group -->
                        </div>
                    </div>
                    <!-- End Tab Group -->

                    <!-- Tab Group -->
{{--                     
                    <div class="box__content u-hide js-tabcontent-salesagreed">
                        <div class="box__container">
                            <!-- Form Group -->
                            <form action="" method="POST">
                                <input type="hidden" name="onoffswitch" value="true">
                                <h3 class="box__title box__title--no-border">Sale Agreed Email</h3>
                                <div class="form__group">
                                    <label for="emailsalesagreed" class="form__label">Email Copy to Accepted Sales</label>
                                    <textarea rows="5" class="form__input js-editor" name="emailsalesagreed" id="emailsalesagreed">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur fugiat molestiae corrupti quae distinctio quasi ipsum ipsam aliquam quam itaque culpa at deleniti quos iste architecto omnis, debitis, autem reiciendis.</textarea>
                                </div>
                                <div class="divider"></div>
                                <div class="box__footer box__footer--no-border">
                                    <button type="submit" class="button button--big button--orange-bg">Save Page</button>
                                </div>
                            </form>
                            <!-- End Form Group -->
                        </div>
                    </div> 
--}}
                    <!-- End Tab Group -->

                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script>
    $('.js-remove-logo').on('click', function(e) {
        e.preventDefault;
        axios.delete('/api/websites/{{auth()->user()->website->slug}}/deleteLogo')
            .then(response => {
                $('.uploadedfile--logo').hide();
                console.log(response.data);
            });
    });

    $('.js-remove-hero').on('click', function(e) {
        e.preventDefault;
        axios.delete('/api/websites/{{auth()->user()->website->slug}}/deleteHero')
            .then(response => {
                $('.uploadedfile--hero').hide();

            });
    });
</script>
@endsection
