<div class="offer__rule">
        <div class="model-head">
            <div class="offer__rule--title ">
                <strong>{{$manufacturerName}} {{$vehicleModel->name}} ({{$vehicleModel->introduced_date}} - {{$vehicleModel->getDiscontinuedDateAttribute($vehicleModel->discontinued_date)}})</strong>
            </div>
            <div class="offer__rule--derivatives ">
                <strong>{{$chosenDerivativesCount}} Derivatives</strong>
            </div>

        </div>
        @foreach( $suggestedValues as $suggestedValue )
        <div class="offer__rule-children">

                <div class="offer__rule--child">
                    <div class="offer__rule--title">
                        <p>{{$suggestedValue->derivative_name}}</p>
                    </div>
                    <div class="offer__rule--derivatives">

                        <form method="POST" action="/associates/update-suggested-value-manual/{{$offerSlug}}/{{$suggestedValue->derivative_id}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="redirect_rule" value="1">
                            <p class="inside-p manual"> <span class="mobile-show" style="display:none">OR UPDATE VALUE MANUALLY</span> <input class="suggested-values-input" type="text" name="value" value="{{$suggestedValue->base_value}}"><strong>%</strong></p>
                            <button type="submit" class="button button--orange-bg magic-btn " style="margin-right:6px">UPDATE</button>
                        </form>

                        <div>
                            <p class="inside-p topper move-right">JAMJAR SUGGESTED VALUE IS <strong><input disabled class="suggested-values-input" type="text" value="{{$suggestedValue->suggested_value_percentage}}">%</strong>
                                <a href="/associates/update-suggested-value/{{$offerSlug}}/{{$suggestedValue->derivative_id}}/{{$suggestedValue->suggested_value_percentage}}?redirect_rule=1" class="button button--green-bg magic-btn">ACCEPT</a></p>
                        </div>


                    </div>


                </div>
        </div>
        @endforeach
</div>
