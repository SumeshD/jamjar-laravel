@extends('layouts.app')

@section('content')

    @include('matrix.partials.header', [
        'title' => 'Suggested Values',

    ])

    <section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
        <div class="dashboard__container">
            <div class="dashboard__col">
                @include('matrix.partials.sidebar', ['active' => 'suggested-values'])
            </div>
            <div class="dashboard__col">
                <div class="box box--with-header box--no-margin">
                    <div class="box__header header-theme-marketplace">Suggested Values</div>

                    <div class="box__content box__content--with-padding" >
                        @if($offerCount > 0)
                            <div class="suggested-values partner-add-to-marketplace" style="">
                                <div class="add-to-marketplace__left" style="float:left">
                                    <div style="font-weight: bold; font-size: 23px;">
                                        You have {{$offerCount}} suggestions, why not update them all at once?
                                    </div>
                                    <div style="margin-top: 6px;">
                                        If you like, we can keep on top of your suggestions by updating them all.
                                    </div>
                                </div>
                                <div class="add-to-marketplace__right" style="float:left;margin-left:40px">
                                    <a href="/associates/update-suggested-values-model/{{$offerSlug}}"  class="button button--orange-bg">UPDATE ALL</a>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="box__content box__content--with-padding" >
                        <div class="main-offer" style="margin-bottom: 20px;">
                            <div class="offer__header">
                                <div class="offer__title" data-models="{{ $modelList }}">
                                    <h3>{{$manufacturerName}}</h3>
                                    <span>{{$fuelType}}</span>
                                    <p>{{$offerName}}</p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    @push('scripts-after')
        <script src="{{ asset('js/suggested-values.js')}}"></script>
    @endpush

@endsection
