@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Associate Settings'])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'settings'])
        </div>
        <div class="dashboard__col">
            <form method="POST" action="{{ route('partnerSettingsUpdate') }}" class="form">
                <div class="box box--with-header box--no-margin" style="margin-bottom: 25px;">
                    <div class="box__header">Associate Settings</div>

                    @if ($errors->any())
                        <div class="alert alert--danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="box__container">
                        <div class="box__content">
                            <h5>Search settings</h5>
                            <br />

                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form__group">
                                <div class="form__group form__group--half  @if ($errors->has('valuation_validity')) form--errors @endif">
                                    <label for="valuation_validity" class="form__label">Valuation Validity (days)</label>
                                    <input type="number" class="form__input" id="valuation_validity" name="valuation_validity" placeholder="in days" min="1" value="{{ (int) $user->company->valuation_validity }}">
                                </div>
                                <div class="form__group form__group--half  @if ($errors->has('bank_transfer_fee')) form--errors @endif">
                                    <label for="bank_transfer_fee" class="form__label">Bank Transfer Fee (GBP)</label>
                                    <input type="number" class="form__input" id="bank_transfer_fee" name="bank_transfer_fee" placeholder="in pounds" min="0" value="{{$user->company->getOriginal('bank_transfer_fee') / 100}}">                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__group form__group--half  @if ($errors->has('admin_fee')) form--errors @endif">
                                    <label for="admin_fee" class="form__label">Admin Fee (GBP)</label>
                                    <input type="number" class="form__input" id="admin_fee" name="admin_fee" placeholder="in pounds" min="0" value="{{$user->company->getOriginal('admin_fee') / 100}}">
                                </div>
                                <div class="form__group form__group--half  @if ($errors->has('buy_vehicles_from_other_partners')) form--errors @endif">
                                    <label for="buy_vehicles_from_other_partners" class="form__label"></label>
                                    <div class="form__label">
                                        <input type="checkbox" class="form__input" id="buy_vehicles_from_other_partners" name="buy_vehicles_from_other_partners" {{ old('buy_vehicles_from_other_partners',$user->company->buy_vehicles_from_other_partners) ? 'checked=checked' : '' }}>
                                        <span style="text-transform: uppercase; padding-left: 5px;">Are you interested in purchasing vehicles from other partners?</span>
                                    </div>

                                </div>
                            </div>

                            <div class="divider"></div>

                            <h5>Fees</h5>
                            <br />

                            <div class="form__group">
                                <div class="form__group form__group--half @if ($errors->has('collection_fee')) form--errors @endif">
                                    <label for="collection_fee" class="form__label">Collection Fee (GBP)</label>
                                    <input type="number" class="form__input" id="collection_fee" min="0" name="collection_fee" value="{{ old('collection_fee') ?? $location->collection_fee }}">
                                    <div class="form__checkbox" style="width: 100%;">
                                        <label>
                                            <input type="checkbox" class="form__input" id="enable_collection" name="enable_collection" {{ ($location->allow_collection == 1) ? ' checked' : '' }}> <strong>Offer Collection?</strong>
                                        </label>
                                    </div>
                                </div>

                                <div class="form__group form__group--half @if ($errors->has('dropoff_fee')) form--errors @endif">
                                    <label for="dropoff_fee" class="form__label">Drop Off Fee (GBP)</label>
                                    <input type="number" class="form__input" id="dropoff_fee" min="0" name="dropoff_fee" value="{{ old('dropoff_fee') ?? $location->dropoff_fee }}">
                                    <div class="form__checkbox" style="width: 100%;">
                                        <label>
                                            <input type="checkbox" class="form__input" id="enable_delivery" name="enable_delivery"  {{ ($location->allow_dropoff == 1) ? ' checked' : '' }}> <strong>Allow Drop Off?</strong>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="divider"></div>

                            <h5>Notifications</h5>
                            <br />

                            <div class="form__group">
                                <div class="form__group form__group--half @if ($errors->has('send_additional_vehicle_information_emails')) form--errors @endif">
                                    <div class="form__checkbox" style="width: 100%;">
                                        <label>
                                            <input type="checkbox" class="form__input" id="send_additional_vehicle_information_emails" name="send_additional_vehicle_information_emails" {{ ($user->send_additional_vehicle_information_emails == 1) ? ' checked' : '' }}> <strong>Notify me when additional information is provided for vehicles I have valued</strong>
                                        </label>
                                    </div>
                                </div>

                                <div class="form__group form__group--half"></div>
                            </div>

                            <div class="divider"></div>

                            <h5>Postcode areas to purchase from</h5>
                            <br />

                            <div class="form__group">
                                @foreach ($postcodes as $key => $values)

                                    <div>
                                        <div style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);width:100%;float:left;padding: 1rem 0;margin-bottom: 1rem;">
                                        @if (count($values) >= 5)
                                            <div class="form__checkbox u-pull-right" style="margin-bottom: 0">
                                                <label for="select_all[{{$key}}]">
                                                    <input type="checkbox" id="select_all_{{$key}}" name="select_all[{{$key}}]">
                                                    <span>Select All</span>
                                                </label>
                                            </div>
                                            @push('scripts-after')
                                                <script>
                                                    $("#select_all_{{$key}}").on('ifChecked', function() {
                                                        $('[data-country={{$key}}]').each(function() {
                                                            $(this).iCheck('check');
                                                        });
                                                    });
                                                    $("#select_all_{{$key}}").on('ifUnchecked', function() {
                                                        $('[data-country={{$key}}]').each(function() {
                                                            $(this).iCheck('uncheck');
                                                        });
                                                    });
                                                </script>
                                            @endpush
                                            <strong class="title" style="border-bottom:none;padding:0;margin-bottom:0">{{$key}}</strong>
                                        @else
                                            <strong class="title" style="border-bottom:none;padding:0;margin-bottom:0" >{{$key}}</strong>
                                        @endif
                                        </div>

                                            <div style="padding-bottom:2rem;">
                                                @foreach ($values as $postcode)
                                                    <div class="form__checkbox" style="width:25%;">
                                                        <label for="postcodes[{{$postcode->postcode}}]">
                                                            <input type="checkbox" id="postcodes[{{$postcode->postcode}}]" name="postcodes[{{$postcode->postcode}}]" data-country="{{$postcode->country}}"
                                                                   @if (in_array($postcode->postcode, $user->company->postcodes))
                                                                   checked
                                                                    @endif
                                                            >
                                                            <span>{{str_limit($postcode->county, 15)}} <strong>({{$postcode->postcode}})</strong></span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                    </div>

                                @endforeach
                            </div>
                        </div>

                        <div class="form__submit">
                            <input value="Save Details" type="submit">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
