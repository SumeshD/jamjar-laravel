<?php /** @var \JamJar\Vehicle $vehicle */ ?>

<div class="col-12">
    <h6 style="margin: 0 0 3rem; font-weight: bold; text-transform: uppercase; font-size: 28px; color: black;">Vehicle Information Given</h6>
    <div class="col-6">
        <p>Type: <strong>{{ $vehicle->type }}</strong></p>
        <p>Manufacturer: <strong>{{ $vehicle->meta->manufacturer }}</strong>
        </p>
        <p>Registration No: <strong>{{ $vehicle->numberplate }}</strong></p>
        <p>VIN No: <strong>{{ $vehicle->meta->vin_number }}</strong></p>
        <p>Model: <strong>{{ $vehicle->meta->model }}</strong></p>
        <p>Derivative: <strong>{{ $vehicle->meta->derivative }}</strong></p>
        <p>Fuel Type: <strong>{{ $vehicle->meta->fuel_type }}</strong></p>
        <p>Colour:
            <strong>{{ strtoupper($vehicle->meta->colour->title) }}</strong>
        </p>
        <p>Mileage: <strong>{{ $vehicle->meta->mileage }}</strong></p>
        <p>Write Off Category:
            <strong>{{ucwords($vehicle->meta->write_off_category)}}</strong>
        </p>

        @if(isset($sale))
            @if($sale->vehicle_condition)
                <p>Condition: <strong>{{ ucfirst($sale->vehicle_condition) }}</strong></p>
            @endif

            @if($sale->dealer_notes)
                <p>Condition notes: <strong>{{ $sale->dealer_notes }}</strong></p>
            @endif
        @endif




    </div>
    <div class="col-6">
        <p>Transmission: <strong>{{ $vehicle->meta->transmission }}</strong>
        </p>
        <p>Engine: <strong>{{ $vehicle->meta->engine_capacity }}</strong>
        </p>
        <p>Introduced:
            <strong>{{ $vehicle->meta->registration_date }}</strong></p>
        <p>Year: <strong>{{ $vehicle->meta->plate_year }}</strong></p>
        <p>Previous Owners: <strong>{{trans('vehicles.owners_count_external.owners_'.($vehicle->meta->number_of_owners <= 5 ? $vehicle->meta->number_of_owners : 6)) }}</strong>
        </p>
        <p>Service History:
            <strong>{{ $vehicle->meta->service_history }}</strong></p>
        <p>MOT: <strong>{{ $vehicle->meta->mot }}</strong></p>
        <p>Non-Runner:
            <strong>{{ $vehicle->meta->non_runner ? 'Yes' : 'No' }}</strong>
        </p>
        @if ($vehicle->meta->non_runner_reason)
            <p>Reported Vehicle Issues: </p>
            <ul>
                @foreach ($vehicle->meta->non_runner_reason as $defect)
                    <li><strong>{{$defect->title}}</strong></li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
