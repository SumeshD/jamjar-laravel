<?php /** @var \JamJar\Vehicle $vehicle */ ?>
<?php $company = isset($company) ? $company : auth()->user()->company; ?>

<div class="col-12">
    <h6 style="margin: 3rem 0; font-weight: bold; text-transform: uppercase; font-size: 22px; color: black;">User Information</h6>
    <div class="col-6">
        <p>Date / Time Valued:
            <strong>{{isset($highestValuation) ? $highestValuation->created_at : (isset($valuation) ? $valuation->created_at : 'no valuation')}}</strong>
        </p>
        <p>Area:
            <strong>{{ strtoupper($vehicle->owner->getAreaByPostcode()) }}</strong>
        </p>
    </div>
    @if ((isset($highestValuation) and $highestValuation and $company->id === $highestValuation->company_id) or (isset($valuation) and $company->id === $valuation->company_id))
        <div class="col-6">
            <p>Name: <strong>{{ $vehicle->owner->user->name }}</strong></p>
            <p>Email: <strong>{{ $vehicle->owner->email }}</strong></p>
            <p>Phone: <strong>{{ $vehicle->owner->telephone }}</strong></p>
            <p>Address: <strong>{{ $vehicle->owner->postcode }}</strong></p>
        </div>

    @elseif(!$vehicle->owner->user->matrix_partner)
        <div class="col-6">
            <p>Name: <strong>{{ $vehicle->owner->user->name }}</strong></p>
        </div>
    @endif
</div>
