<?php /** @var \JamJar\Vehicle $vehicle */ ?>

<div class="col-12">
    <h6 style="margin: 3rem 0; font-weight: bold; text-transform: uppercase; font-size: 22px; color: black;">Additional information</h6>
    <div class="col-6">
        <p>
            Service history information: <br />
            <b>{{ $vehicle->getServiceHistoryInformation() }}</b>
        </p>
        <p>No. of keys: <b>{{ trans('vehicles.additional_information.keys_count.' . $vehicle->getKeysCount()) }}</b></p>
        @if ($vehicle->getIsLogBook() !== null)
            <p>V5 present?: <b>{{ $vehicle->getIsLogBook() ? 'Yes' : 'No' }}</b></p>
        @endif
    </div>
    <div class="col-6">
        <p>
            Specification information: <br />
            <b>{{ $vehicle->getAdditionalSpecificationInformation() }}</b>
        </p>

        @if ($vehicle->getIsInStock() !== null or $vehicle->getStockAvailabilityAt() !== null)
            <p>In stock?: <b>{{ ($vehicle->isAvailable()) ? 'Yes' : 'No' }}</b></p>
        @endif

        @if (!$vehicle->isAvailable())
            <p>Available at: <b>{{ $vehicle->getStockAvailabilityAt()->format('Y/m/d') }}</b></p>
        @endif
        @if ($vehicle->getAskingPriceInPence())
            <p>Asking price: <b>&pound;{{ number_format($vehicle->getAskingPriceInPence() / 100, 0) }}</b></p>
        @endif
    </div>
</div>
