<?php /** @var \JamJar\Vehicle $vehicle */ ?>
<?php /** @var \JamJar\Valuation $highestValuation */ ?>
<?php
    $capValueInPounds = $vehicle->getCapValueInPence() !== null ?
        ($vehicle->getCapValueInPence() / 100) :
        (isset($highestValuation) ? (int) $highestValuation->cap_value : 0);

    $capValueBelowInPounds = $vehicle->getCapValueBelowInPence() !== null ?
        ($vehicle->getCapValueBelowInPence() / 100) :
        (isset($highestValuation) ? (int) $highestValuation->cap_value_below : 0);

    $capValueCleanInPounds = $vehicle->getCapValueCleanInPence() !== null ?
        ($vehicle->getCapValueCleanInPence() / 100) :
        (isset($highestValuation) ? (int) $highestValuation->cap_value_clean : 0);

    $capValueRetailInPounds = $vehicle->getCapValueRetailInPence() !== null ?
        ($vehicle->getCapValueRetailInPence() / 100) :
        (isset($highestValuation) ? (int) $highestValuation->cap_value_retail : 0);
    $isMatrixPartner = false;
    if(auth()->user() !== null){
        $isMatrixPartner = auth()->user()->isMatrixPartner();
    }
?>
@if ($isMatrixPartner)
<div style="margin: 10px 0;">
    <div class="col-jjvalues-3">
        <strong>JJ Retail</strong>
        <h4 class="u-text-muted" style="font-size: 2rem; color: #65baff !important;">
            @if($capValueRetailInPounds == 0 || $capValueRetailInPounds == '')
                Zero data
            @else
                £{{ number_format($capValueRetailInPounds + 15) }}
            @endif
        </h4>
    </div>
    <div class="col-jjvalues-3">
        <strong>JJTV Clean</strong>
        <h4 class="u-text-muted" style="font-size: 2rem;">
            @if($capValueCleanInPounds == 0 || $capValueCleanInPounds == '')
                Zero data
            @else
                £{{ number_format($capValueCleanInPounds + 13) }}
            @endif
        </h4>
    </div>
    <div class="col-jjvalues-3">
        <strong>JJTV Average</strong>
        <h4 class="u-text-muted" style="font-size: 2rem;">
            @if($capValueInPounds == 0 || $capValueInPounds == '')
                Zero data
            @else
                £{{ number_format($capValueInPounds + 9) }}
            @endif
        </h4>
    </div>
    <div class="col-jjvalues-3">
        <strong>JJTV Below</strong>
        <h4 class="u-text-muted" style="font-size: 2rem;">
            @if($capValueBelowInPounds == 0 || $capValueBelowInPounds == '')
                Zero data
            @else
                £{{ number_format($capValueBelowInPounds + 3) }}
            @endif
        </h4>
    </div>
</div>
@endif