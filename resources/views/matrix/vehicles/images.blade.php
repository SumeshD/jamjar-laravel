<?php /** @var \JamJar\Vehicle $vehicle */ ?>
@if (count($vehicle->getImages()) > 0)
    <div class="col-12">
        <h6 style="margin: 3rem 0; font-weight: bold; text-transform: uppercase; font-size: 22px; color: black;">Car Photos</h6>
        <div>
            @foreach($vehicle->getImages() as $vehicleImage)
                <a target="_blank" href="{{ route('image', [$vehicleImage->getUploadedImage()->id(), $vehicleImage->getUploadedImage()->originalName()]) }}" class="image-preview">
                    <img class="vehicle-image" src="{{ route('thumbnail', [$vehicleImage->getUploadedImage()->id(), $vehicleImage->getUploadedImage()->originalName()]) }}" />
                </a>
            @endforeach
        </div>
    </div>
@endif

@push('scripts-after')
    <script>
        $(function() {
            $('.image-preview').viewbox({
                // template
                template: '<div class="viewbox-container"><div class="viewbox-body"><div class="viewbox-header"></div><div class="viewbox-content"></div><div class="viewbox-footer"></div></div></div>',
                // loading spinner
                loader: '<div class="image-loader"><div class="spinner"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>',
                setTitle: true,
                margin: 20,
                resizeDuration: 300,
                openDuration: 200,
                closeDuration: 200,
                closeButton: true,
                navButtons: true,
                closeOnSideClick: true,
                nextOnContentClick: true,
                useGestures: true
            });

        });
    </script>
@endpush
