@extends('layouts.app')

@section('content')

    <section class="associate-banner content">
        <div class="content__container">
            <div class="content__col">
                <h2 class="content__title"><span>Get unlimited free lookups with the car buying comparison site.</span>
                </h2>
                <p><span>Apply to become an associate today and get real-world valuation comparisons with our unlimited free vehicle lookups.</span>
                </p>
                <a href="#apply-form" class="button button--orange">APPLY NOW</a>
            </div>
            <div class="content__col associate-banner__image">
                <img src="{{ secure_asset('images/associates/banner.jpg') }}" alt="Associate dashboard">
            </div>
        </div>
    </section>

    <section class="associate-how content">
        <div class="content__container">
            <h2 class="content__title title-underline center">
                <span>Find the best vehicles from 1000s of customers</span></h2>

            <div class="content__col">
                <div class="content__col__arrow"></div>
                <div class="content__col__block">
                    <img src="{{ secure_asset('images/associates/clipboard.svg') }}" alt="Sign up for free">
                    <h4>SIGN UP FOR FREE</h4>
                    <span>There’s no sign up fee, just complete the application form and join over 50 trusted partners</span>
                </div>
            </div>

            <div class="content__col">
                <div class="content__col__arrow"></div>
                <div class="content__col__block">
                    <img src="{{ secure_asset('images/associates/magnify.svg') }}" alt="Find 1000s of cars">
                    <h4>Find 1000s of cars</h4>
                    <span>Pick the best vehicles for you from 1000s of users looking for your valuations</span>
                </div>
            </div>

            <div class="content__col">
                <div class="content__col__block">
                    <img src="{{ secure_asset('images/associates/alarm.svg') }}" alt="Make a bid to alert the owner!">
                    <h4>Make a bid to alert the owner!</h4>
                    <span>Track your offers and valuations through our online portal</span>
                </div>
            </div>

            <!--
                <div class="content__col middle">
                    <a href="#associate-benefits" class="button button--orange">SIGN UP TODAY</a>
                </div>
            -->

        </div>
    </section>

    <section id="apply-form"
             class="associate-content content @if (request()->getQueryString() == 'applied=1') content--faded @endif">
        <div class="content__container">
            <div class="content__col left">
                <h2 class="content__title title-underline"><span>Apply Online</span></h2>
                <h3>As a Jamjar Associate, you’ll have access to a range of features and benefits to help you find the
                    vehicles you’re looking for at the prices that work for you.</h3>
                <h3 class="title-underline">Associate benefits</h3>
                <ul>
                    <li>‘Cherry pick’ the best cars for your stock from our new leads feature with full CAP valuation
                        data.
                    </li>
                    <li>Set live alerts to notify you when a specific car is added to the market by customers.</li>
                    <li>Have access to our matrix rules system to provide automated bids to help beat the competition.
                    </li>
                    <li>Set up your own micro site without charge.</li>
                </ul>

                <div class="associate-content__unlimited-reg">

                    <h4>Unlimited registration number lookups for associates</h4>
                    <p>Only available as a Jamjar associate, you'll have unlimited free registration number lookups to
                        compare how other partners value the vehicles you're buying and selling.</p>
                    <a id="openModal">Find out more</a>

                    <div class="associate-content__unlimited-reg__icon">
                        <img src="{{ secure_asset('images/icons/unlimited.svg') }}" alt="Unlimited registration number">
                    </div>

                </div>

            </div>
            <div class="content__col right">
                <div class="box box--narrow clear">

                    <div class="box__header">
                        <p>Fill in your details and we'll get in touch with you about becoming a jamjar.com partner.</p>
                    </div>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert--danger">
                            {!!$error!!}
                        </div>
                    @endforeach
                    <form action="{{ route('savePartner') }}" method="POST" class="form radio-button-form">
                        {{ csrf_field() }}

                        <div class="form__container">

                            <h3>Company Details</h3>

                            <div class="form__group">

                                <div class="form__group form__group--half @if ($errors->has('company_name')) form--errors @endif">
                                    <label for="company_name" class="form__label">Company Name <span
                                                class="form__required">*</span></label>
                                    <input
                                            type="text"
                                            name="company_name"
                                            id="company_name"
                                            class="form__input"
                                            value="{{ old('company_name') }}"
                                            required
                                    >
                                </div>

                                <div class="form__group form__group--half @if ($errors->has('telephone_number')) form--errors @endif">
                                    <label for="company_number" class="form__label">Company Telephone <span
                                                class="form__required">*</span></label>
                                    <input
                                            type="text"
                                            name="company_number"
                                            id="company_number"
                                            class="form__input"
                                            value="{{ old('company_number') }}"
                                            required
                                    >
                                </div>

                                @include('address.full-loquate-address')

                                <div class="clearfix"></div>

                                <div class="form__group form__group__checkbox form__group--half @if ($errors->has('partner_buy')) form--errors @endif">
                                    <div class="form__label">I'm interested in purchasing vehicles from
                                        other Jamjar partners as well as Private owners.
                                    </div>

                                    <label class="container">
                                        <input
                                                type="checkbox"
                                                name="buy_vehicles_from_other_partners"
                                                id="buy_vehicles_from_other_partners"
                                                class="form__input"
                                                {{ old('buy_vehicles_from_other_partners') ? 'checked=checked' : '' }}
                                        >
                                        <span class="checkmark"></span> Yes
                                    </label>
                                </div>

                                <div class="form__group form__group__checkbox form__group--half @if ($errors->has('is_scrap_dealer')) form--errors @endif">
                                    <div for="is_scrap_dealer" class="form__label">Are you a scrap dealer?</div>

                                    <label class="container">
                                        <input
                                                type="checkbox"
                                                name="is_scrap_dealer"
                                                id="is_scrap_dealer"
                                                class="form__input"
                                                {{ old('is_scrap_dealer') ? 'checked=checked' : '' }}
                                        >
                                        <span class="checkmark"></span> Yes
                                    </label>

                                </div>

                            </div>

                            <div class="divider"></div>

                            <h3>Your Details</h3>

                            <div class="form__group">

                                <div class="form__group form__group--half @if ($errors->has('name')) form--errors @endif">
                                    <label for="name" class="form__label">Your Name <span
                                                class="form__required">*</span></label>
                                    <input
                                            type="text"
                                            name="name"
                                            id="name"
                                            class="form__input"
                                            value="{{ old('name') }}"
                                            required
                                    >
                                </div>

                                <div class="form__group form__group--half @if ($errors->has('telephone_number')) form--errors @endif">
                                    <label for="telephone_number" class="form__label">Mobile Number <span
                                                class="form__required">*</span></label>
                                    <input
                                            type="text"
                                            name="telephone_number"
                                            id="telephone_number"
                                            class="form__input"
                                            value="{{ old('telephone_number') }}"
                                            required
                                    >
                                </div>

                                @if (!auth()->check())
                                    <div class="form__group form__group--half @if ($errors->has('email')) form--errors @endif">
                                        <label for="email" class="form__label">Email Address <span
                                                    class="form__required">*</span></label>
                                        <input
                                                type="email"
                                                name="email"
                                                id="email"
                                                class="form__input"
                                                value="{{ old('email') }}"
                                                required
                                        >
                                    </div>

                                    <div class="form__group form__group--half @if ($errors->has('password')) form--errors @endif">
                                        <label for="password" class="form__label">Choose Password <span
                                                    class="form__required">*</span></label>
                                        <input
                                                type="password"
                                                name="password"
                                                id="password"
                                                class="form__input"
                                                value="{{ old('password') }}"
                                                required
                                        >
                                    </div>
                                @else
                                    <div class="form__group form__group--half @if ($errors->has('email')) form--errors @endif">
                                        <input
                                                type="hidden"
                                                name="email"
                                                id="email"
                                                class="form__input"
                                                value="{{ auth()->user()->email }}"
                                                required
                                        >
                                    </div>
                                @endif

                            </div>

                        </div>

                        <div class="form__container white">

                            <h5 class="box__title box__title--mob-hide" style="margin-top: 10px;">Keep in touch with
                                Jamjar</h5>
                            <div class="form__group @if ($errors->has('gdpr')) form--errors @endif">
                                <label for="gdpr">
                                    <p>We'll automatically contact you with alerts about your offers and valuations.
                                        We’d like you to be the first to know about latest offers, news, products and
                                        promotions. You can let us know at any time if you change your mind. Further
                                        information can be found in our <a target="_blank"
                                                                           href="https://www.jamjar.com/associate-privacy/">privacy
                                            policy.</a></p>
                                    <br/>
                                    <p>
                                        Would you like to receive these updates from us?
                                    </p>
                                    <br/>
                                </label>
                            </div>
                            <br/>

                            <p class="radio-button-error">
                                Please select an option below
                            </p>

                            <div style="width: 100%; float:left; display:inline-block;">

                                <label for="marketing">
                                    <span data-value="1" class="radio-button radio-button-circle inactive">YES</span>
                                    <span data-value="0" class="radio-button radio-button-circle inactive">NO</span>
                                    <input type="hidden" name="marketing" id="marketing" value="0">
                                </label>

                                <div class="form__submit form__submit--no-border">
                                    <input
                                            class="loquate-data-submit-button"
                                            type="submit"
                                            value="Send"
                                            style="width: 230px;"
                                    >
                                </div>

                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="associate-benefits" class="associate-benefits content">
        <div class="content__container">
            <div class="content__col">
                <img src="{{ secure_asset('images/icons/freereg.svg') }}" alt="Unlimited Registration  number lookups">
                <span>Unlimited Registration  number lookups</span>
            </div>
            <div class="content__col">
                <img src="{{ secure_asset('images/icons/unique-visitors.svg') }}" alt="1000s of unique visitors">
                <span>1000s of unique users loooking for your valuations</span>
            </div>
            <div class="content__col">
                <img src="{{ secure_asset('images/icons/trusted-partner.svg') }}" alt="Trusted Partners">
                <span>Join a group of over 50 trusted partners on jamjar.com</span>
            </div>
            <div class="content__col">
                <img src="{{ secure_asset('images/icons/track-offers.svg') }}" alt="Track Offers">
                <span>Track your offers and valuations through our online portal</span>
            </div>
        </div>
    </section>
    <!--
    <section class="associate-how blue content">
        <div class="content__container">

            <div class="associate-how__testimonial">
                <h3>“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. ”</h3>
                <p><span>Happy Associate</span><br/>Director of Car company</p>
            </div>

        </div>
    </section>
    -->
    <section class="associate-help content">
        <div class="content__container">

            <img class="associate-help__image-title" src="{{ secure_asset('images/icons/help.svg') }}" alt="Need help?">

            <h2 class="title-underline center">Need Help?</h2>

            <p>We’re always happy to help you with any questions, problems or information.</p>

            <a href="mailto:info@jamjar.com?Subject=Support">info@jamjar.com</a>

        </div>
    </section>

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>

            <div class="modal-content__header">
                <h3>Unlimited registration number lookups for associates</h3>

                <div class="modal-content__header__icon">
                    <img src="{{ secure_asset('images/icons/unlimited.svg') }}" alt="Unlimited registration number">
                </div>

            </div>

            <div class="modal-content__text">
                <p>
                    Look up vehicle registration numbers in your associate area and we'll provide a comparison of the
                    valuations provided by other partners for that vehicle.
                </p>

                <p>
                    You can use these valuations for reference, or even accept a valuation as an offer if the partner
                    has chosen to purchase from other partners.
                </p>

                <img src="{{ secure_asset('images/associates/unlimited_lookups_example.png') }}"
                     alt="Unlimited lookups">


            </div>

        </div>

    </div>


@endsection
