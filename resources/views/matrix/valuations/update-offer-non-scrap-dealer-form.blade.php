@php ($headerText = isset($headerText) ? $headerText : 'Someone else has submitted a higher offer for this vehicle!')
@php ($yourOfferText = isset($yourOfferText) ? $yourOfferText : 'Your offer')
@php ($showHigherBid = isset($showHigherBid) ? $showHigherBid : true)
@php ($maxValue = max($paidLead->highestValuation->getOriginal('collection_value'), $paidLead->highestValuation->getOriginal('dropoff_value')))
@php ($showUpdateButton = isset($showUpdateButton) ? true : false)
<?php
        /*
    if($showHigherBid) {

        $oldFeeHidden = $valuation->getPaidFee();
        $oldFeeShow = number_format($oldFeeHidden / 10000, 2);
    } else {

        $oldFeeHidden = $oldFee;
        $oldFeeShow = number_format($oldFeeHidden / 100, 2);
    }
        */
?>


@if ($errors->any())
    <div>
        <div class="alert alert-danger col-12">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    </div>
@endif
<form action="{{ route('partnersubmitHigherValueForValuation', [$valuation->id]) }}" method="post">
    <div>
        <div class="box__content">
            <div class="col-12">
                <h4 style="color: black; text-align: center; margin-bottom:20px;">
                    <b>{{ $headerText }}</b>
                </h4>
            </div>
            <br /><br />

            <div class="paid-leads-outbid-background__container">
                @if ($showHigherBid)
                    <div class="col-6" style="width: 33%;">
                        <div class="col-6">
                            <div style="text-align: center; font-size: 14px;">
                                <b>Latest Offer</b>
                            </div>
                            <div style="text-align: center;">
                                <span class="paid-leads-labels new-price" style="color: #65baff !important;">
                                    <b id="highest-valuation">{{ $paidLead->highestValuation->value }}</b>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-3" style="width: 33%;">
                        <div style="text-align: center; font-size: 14px; margin-bottom:10px;">
                            <b>Additional Fee (+VAT)</b>
                            <div>
                                <span class="paid-leads-labels additional-fee">
                                    <b><span id="fee-value" data-old-fee="{{ $oldFee }}">£{{ number_format($additionalFee / 100, 2) }}</span></b>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-3" style="width: 33%;">
                        <div style="text-align: center; font-size: 14px; margin-bottom:10px;">
                            <b>New Offer</b>
                            <div>
                                    <span class="paid-leads-labels additional-fee">
                                        <b><span id="new-offer-value">£{{ $paidLead->highestValuation->value }}</span></b>
                                    </span>
                            </div>
                        </div>
                    </div>

                @else

                    <div class="col-3">
                        <div style="text-align: center; font-size: 14px;">
                            <b>{{ $yourOfferText }}</b>
                        </div>
                        <div style="text-align: center;">
                            <span class="paid-leads-labels old-price-current">
                                <b id="highest-valuation">{{ $valuation->value }}</b>
                            </span>
                        </div>
                    </div>

                    <div class="col-3">
                        <div style="text-align: center; font-size: 14px; margin-bottom:10px;">
                            <b>Additional Fee (+VAT)</b>
                            <div>
                                <span class="paid-leads-labels additional-fee">
                                    <b><span id="fee-value" data-old-fee="{{ $oldFee }}">£{{ number_format($additionalFee / 100, 2) }}</span></b>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div style="text-align: center; font-size: 14px; margin-bottom:10px;">
                            <b>New Offer</b>
                            <div>
                                    <span class="paid-leads-labels additional-fee">
                                        <b><span id="new-offer-value">{{ $valuation->value }}</span></b>
                                    </span>
                            </div>
                        </div>
                    </div>

                @endif
            </div>
            <div class="divider"></div>
            @include('matrix.vehicles.cap-values')
            <div class="divider"></div>
            <div>
                @include('matrix.leads.sliders', [
                    'showPurchaseButtons' => false,
                    'greatConditionText' => 'Your new offer',
                    'showSliderToggleButton' => false,
                    'alwaysShowSliders' => false,
                    'showUpdateButton' => $showUpdateButton
                ])
            </div>
        </div>
    </div>
</form>