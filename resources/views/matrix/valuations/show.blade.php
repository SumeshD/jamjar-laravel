@extends('layouts.app')
@section('content')

<?php /** @var \JamJar\Valuation $valuation */ ?>
<?php /** @var \JamJar\Vehicle $vehicle */ ?>
<?php $vehicle = $valuation->getVehicle(); ?>

@include('matrix.partials.header', ['title' => 'Paid Leads', 'buttons' =>
    '<a class="button button--small button--orange-bg u-float-right" onclick="window.print()">Print Page</a>'
])

@php ($anotherPartnerPlacedBetterValue = (auth()->user()->id === $valuation->company->user_id and $valuation->id != $paidLead->highestValuation->id))

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'valuations'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-header" style="margin-top: 0;">
                <div class="box__header ">Lead Details</div>
                <div class="box__container">
                    <div class="box__content">
                        @if ($anotherPartnerPlacedBetterValue)
                            @if ($vehicle->meta->write_off==0 && $vehicle->meta->non_runner==0)
                                @include('matrix.valuations.update-offer-non-scrap-dealer-form')
                            @else
                                @include('matrix.valuations.update-offer-scrap-dealer-form')
                            @endif
                        @else
                            @if ($vehicle->meta->write_off==0 && $vehicle->meta->non_runner==0)
                                @include('matrix.valuations.update-offer-non-scrap-dealer-form', [
                                    'headerText' => 'You are the highest bidder, would you like to increase your offer?',
                                    'showHigherBid' => false,
                                    'yourOfferText' => 'Your current offer',
                                ])
                            @else
                                @include('matrix.valuations.update-offer-scrap-dealer-form', [
                                    'headerText' => 'You are the highest bidder, would you like to increase your offer?',
                                    'showHigherBid' => false,
                                    'yourOfferText' => 'Your current offer',
                                ])
                            @endif
                        @endif

                        <div class="divider"></div>
                        <div class="u-no-columns">
                            @include('matrix.vehicles.information')
                            @if ($vehicle->additionalInformationHasBeenProvided())
                                @include('matrix.vehicles.additional-information')
                            @endif
                            @include('matrix.vehicles.images')
                            <div class="divider"></div>
                            @include('matrix.vehicles.owner-information')
                            <div class="divider"></div>
                            @include('matrix.valuations.owner-note')
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@push('styles-after')
<style>
    .u-no-columns ul {
        width: 100% !important;
        float: left;
    }
    .u-no-columns ul li {
        width: 100% !important;
    }
</style>
@endpush
@endsection
