@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Paid Leads'])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @php


               if(isset(request()->lead_source)){


                   if(request()->lead_source=='marketplace'){
                       $activated = 'valuations-marketplace';
                   }

                    if(request()->lead_source=='automated_bids'){
                       $activated = 'valuations-automated-bids';

                    }

                    if(request()->lead_source=='all'){
                       $activated = 'valuations-marketplace';
                    }

               }else{

                   $activated = 'valuations-marketplace';
               }



            @endphp
            @include('matrix.partials.sidebar', ['active' => $activated])
        </div>
        <div class="dashboard__col">

            <div class="box box--with-header box--no-margin">
                <div class="box__header box__header_dashboard {{request()->lead_source=='marketplace' ? 'header-theme-bidding' : 'header-theme-marketplace'}}">Paid Leads</div>
                <div class="box__container">
                    <div class="box__content box__with_filtering" >

                      {{-- <input class="valuations__search" type="text" name="paidLeadsSearch" placeholder="Search"> --}}

                        <div class="row" style="margin-bottom:30px">

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">

                                <p class="leads-filter-titles">Offer Status</p>

                                <a href="{{ route('partnerValuations',
                                [
                                    'offer_status'=>'all',
                                    'lead_source'=>(isset(request()->lead_source)) ? request()->lead_source : 'all',
                                    'outbid'=>(isset(request()->outbid)) ? request()->outbid : false,
                                    'vrm'=>(isset(request()->vrm)) ? request()->vrm : ''
                                ]) }}" class="filter-button {{request()->offer_status=='all' ? 'orange-filter-button-active' : 'orange-filter-button-inactive'}}">ALL</a>
                                <a href="{{ route('partnerValuations',[
                                    'offer_status'=>'active',
                                    'lead_source'=>(isset(request()->lead_source)) ? request()->lead_source : 'all',
                                    'outbid'=>(isset(request()->outbid)) ? request()->outbid : false,
                                    'vrm'=>(isset(request()->vrm)) ? request()->vrm : ''
                                ]) }}" class="filter-button {{request()->offer_status=='active' ? 'orange-filter-button-active' : 'orange-filter-button-inactive'}}">ACTIVE</a>
                                <a href="{{ route('partnerValuations',[
                                    'offer_status'=>'expired',
                                    'lead_source'=>(isset(request()->lead_source)) ? request()->lead_source : 'all',
                                    'outbid'=>(isset(request()->outbid)) ? request()->outbid : false,
                                    'vrm'=>(isset(request()->vrm)) ? request()->vrm : ''
                                ]) }}" class="filter-button {{request()->offer_status=='expired' ? 'orange-filter-button-active' : 'orange-filter-button-inactive'}}">EXPIRED</a>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

                                <p class="leads-filter-titles">Lead Source</p>

                                <a href="{{ route('partnerValuations',[
                                    'offer_status'=>(isset(request()->offer_status)) ? request()->offer_status : 'all',
                                    'lead_source'=>'all',
                                    'outbid'=>(isset(request()->outbid)) ? request()->outbid : false,
                                    'vrm'=>(isset(request()->vrm)) ? request()->vrm : ''
                                ]) }}" class="filter-button {{request()->lead_source=='all' ? 'orange-filter-button-active' : 'orange-filter-button-inactive'}}">ALL</a>
                                <a href="{{ route('partnerValuations',[
                                    'offer_status'=>(isset(request()->offer_status)) ? request()->offer_status : 'all',
                                    'lead_source'=>'marketplace',
                                    'outbid'=>(isset(request()->outbid)) ? request()->outbid : false,
                                    'vrm'=>(isset(request()->vrm)) ? request()->vrm : ''
                                ]) }}" class="filter-button {{request()->lead_source=='marketplace' ? 'orange-filter-button-active' : 'orange-filter-button-inactive'}}">MARKETPLACE</a>
                                <a href="{{ route('partnerValuations',[
                                    'offer_status'=>(isset(request()->offer_status)) ? request()->offer_status : 'all',
                                    'lead_source'=>'automated_bids',
                                    'outbid'=>(isset(request()->outbid)) ? request()->outbid : false,
                                    'vrm'=>(isset(request()->vrm)) ? request()->vrm : ''
                                ]) }}" class="filter-button {{request()->lead_source=='automated_bids' ? 'orange-filter-button-active' : 'orange-filter-button-inactive'}}">AUTO BIDDING</a>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-1">

                                <p class="leads-filter-titles">Outbid</p>

                                <div class="onoff" style="float:left;margin-top:1rem">

                                    <div class="onoff__col">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="displaySection" {{(isset(request()->outbid) && request()->outbid) ? 'checked' : ''}}>
                                            <label class="onoffswitch-label" for="displaySection">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

                                <p class="leads-filter-titles">Search by Registration</p>

                                <div class="row">
                                    <div class="col-md-7">
                                        <input value="{{(isset(request()->vrm)) ? request()->vrm : ''}}" class="search-filter-input form__input offset-top" type="text" name="vrm" id="vrm-text" placeholder="ENTER VRM...">

                                    </div>
                                    <div class="col-md-5">
                                        <a href="{{ route('partnerValuations',[
                                    'offer_status'=>(isset(request()->offer_status)) ? request()->offer_status : 'all',
                                    'lead_source'=>(isset(request()->lead_source)) ? request()->lead_source : 'all',
                                    'outbid'=>(isset(request()->outbid)) ? request()->outbid : false,
                                    'vrm'=>(isset(request()->vrm)) ? request()->vrm : ''
                                ]) }}" id="search-vrm" class="offset-top filter-button orange-filter-button-active search-filter-btn">Search</a>

                                    </div>
                                </div>


                            </div>

                        </div>


                        <table class="valuations__table jj_responsive_table paid_leads_table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Car</th>
                                    <th>Area</th>
                                    <th>Owners</th>
                                    <th>Write-Off</th>
                                    <th>Non-Runner</th>
                                    <th>Year/Reg</th>
                                    <th>Mileage</th>
                                    <th>Valuation</th>
                                    <th>Position</th>
                                    <th>Paid (+VAT)</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


@push('scripts-after')
    <style>
        .max-width{
            max-width: 200px;
        }

        .loader__box{

            background-color: white;
        }

        .box__content h1, .box__content h2, .box__content h3{

            font-weight: 400;
            color:#6e6e6e;
            text-transform: none;
        }

    </style>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('.valuations__table').DataTable({
        "paging":   true,
        "ordering": false,
        "info":     false,
        "searching": false,
        "serverSide": true,
        "processing": true,
        "ajax":{
            "url": "/api/paid-leads/get-data",
            "dataType": "json",
            "type": "GET",
            "data":{ _token: "{{csrf_token()}}",
                offer_status:"{{$_GET['offer_status']}}",
                lead_source:"{{$_GET['lead_source']}}",
                outbid:"{{$_GET['outbid']}}",
                vrm:"{{$_GET['vrm']}}",
            }
        },
        "lengthChange": false,
        "pageLength": 10,
        "oLanguage": {
          "sSearch": "",
          "sSearchPlaceholder" : "Search",
            "sProcessing": "<div class=\"loader__box\"><div class=\"loader__content\"><i class=\"fa fa-car\"></i> <h3>Please wait, this may take a couple of seconds...</h3></div></div>"
        },
        "columns": [
            { "data": "created_at" },
            { "className":"max-width", "data": "name" },
            { "data": "postcode" },
            { "data": "num_owners" },
            { "data": "write_off" },
            { "data": "non_runner" },
            { "data": "plate_year" },
            { "data": "mileage" },
            { "className":"desktop-centered","data": "outbid" },
            { "data": "position" },
            { "data": "fee" },
            { "className":"u-text-right","data": "details" }
        ],
        createdRow: function( row, data, dataIndex ) {
            if(data.highest_valuation_id!=data.valuation_id) {

                $(row).addClass('outbid');
            }
        }
    });

    $('#displaySection').change(function(){

        if(this.checked) {

            var href = "{{ route('partnerValuations',[
                                    'offer_status'=>(isset(request()->offer_status)) ? request()->offer_status : 'all',
                                    'lead_source'=>(isset(request()->lead_source)) ? request()->lead_source : 'all',
                                    'outbid'=>true,
                                    'vrm'=>(isset(request()->vrm)) ? request()->vrm : ''
                                ]) }}";
        }else{
            var href = "{{ route('partnerValuations',[
                                    'offer_status'=>(isset(request()->offer_status)) ? request()->offer_status : 'all',
                                    'lead_source'=>(isset(request()->lead_source)) ? request()->lead_source : 'all',
                                    'outbid'=>false,
                                    'vrm'=>(isset(request()->vrm)) ? request()->vrm : ''
                                ]) }}";
        }

        href = href.replace(/amp;/g,'&');
        window.location.href = href;

    });

    $('#search-vrm').click(function(e){

        e.preventDefault();

        var vrm = $('#vrm-text').val();
        var href = $(this).attr('href');

        var split = href.split('vrm=');
        var final_href = split[0]+'vrm='+vrm;
        $('#search-vrm').attr('href','valuations?offer_status=all&lead_source=all&outbid=0&vrm='+vrm);

        window.location.href = 'valuations?offer_status=all&lead_source=all&outbid=0&vrm='+vrm;

    });

    $("#vrm-text").on('keyup', function (e) {
        if (e.keyCode === 13) {

            $('#search-vrm').trigger('click');

        }
    });

} );
</script>
@endpush
@endsection
