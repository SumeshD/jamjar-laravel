<?php /** @var \JamJar\Valuation $valuation */ ?>
<form action="{{ route('saveNoteForValuation', [$valuation->id]) }}" method="post">
    <div class="col-12">
        <h6 class style="margin: 3rem 0; font-weight: bold; text-transform: uppercase; font-size: 22px; color: black;">YOUR NOTES</h6>
        <textarea name="valuation-note" style="height: 120px;" class="form__input">{{$valuation->getNote()}}</textarea>
        <div class="col-3">
            <div>
                <input style="width: 140px;" type="submit" class="button button--green-bg" value="SAVE" />
            </div>
        </div>
    </div>
    {{csrf_field()}}
</form>
