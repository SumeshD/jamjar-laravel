@php ($headerText = isset($headerText) ? $headerText : 'Someone else has submitted a higher offer for this vehicle!')
@php ($yourOfferText = isset($yourOfferText) ? $yourOfferText : 'Your offer')
@php ($showHigherBid = isset($showHigherBid) ? $showHigherBid : true)

@if ($errors->any())
    <div>
        <div class="alert alert-danger col-12">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    </div>
@endif
<div>
    <div class="box__content paid-leads-outbid-background">
        <div class="col-12">
            <h4 style="color: black; text-align: center; margin-bottom:20px;">
                <b>{{ $headerText }}</b>
            </h4>
        </div>
        <br /><br />
        <div class="paid-leads-outbid-background__container">
            @if ($showHigherBid)
                <div class="col-6">
                    <div class="col-6">
                        <div style="text-align: center; font-size: 14px;">
                            <b>Your offer</b>
                        </div>
                        <div style="text-align: center;">
                                                    <span class="paid-leads-labels old-price-warning">
                                                        <b>{{ $valuation->value }}</b>
                                                    </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div style="text-align: center; font-size: 14px;">
                            <b>Latest Offer</b>
                        </div>
                        <div style="text-align: center;">
                                                    <span class="paid-leads-labels new-price">
                                                        <b>{{ $paidLead->highestValuation->value }}</b>
                                                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="col-12">
                        <div style="text-align: center; font-size: 14px; margin-bottom:10px;">
                            <b>Would you like to increase your offer?</b>
                        </div>
                        <div style="text-align: center;">
                            @php ($maxValue = max($paidLead->highestValuation->getOriginal('collection_value'), $paidLead->highestValuation->getOriginal('dropoff_value')))

                            <form action="{{ route('partnersubmitHigherValueForValuation', [$valuation->id]) }}" method="post">
                                {{ csrf_field() }}
                                <input name="valueGreat" id="value" class="paid-leads-labels-input" min="{{ $maxValue + 1 }}" type="number" value="{{ $proposedPrice }}" />
                                <input style="width: 140px;" type="submit" class="button button--orange-bg" value="UPDATE" />
                            </form>
                        </div>
                        <div class="col-12" style="text-align: center;">
                            <b class="u-text-green">Additional fee (+VAT): <span id="fee-value" data-old-fee="{{ $oldFee }}">£{{ number_format($additionalFee / 100, 2) }}</span></b>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-2"></div>
                <div class="col-3">
                    <div class="col-6">
                        <div style="text-align: center; font-size: 14px;">
                            <b>Your offer</b>
                        </div>
                        <div style="text-align: center;">
                                                    <span class="paid-leads-labels old-price-current">
                                                        <b>{{ $valuation->value }}</b>
                                                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="col-12">
                        <div style="text-align: center; font-size: 14px; margin-bottom:10px;">
                            <b>Would you like to increase your offer?</b>
                        </div>
                        <div style="text-align: center;">
                            @php ($maxValue = max($paidLead->highestValuation->getOriginal('collection_value'), $paidLead->highestValuation->getOriginal('dropoff_value')))

                            <form action="{{ route('partnersubmitHigherValueForValuation', [$valuation->id]) }}" method="post">
                                {{ csrf_field() }}
                                <input name="valueGreat" id="value" class="paid-leads-labels-input" min="{{ $maxValue + 1 }}" type="number" value="{{ $proposedPrice }}" />
                                <input style="width: 140px;" type="submit" class="button button--orange-bg" value="UPDATE" />
                            </form>
                        </div>
                        <div class="col-12" style="text-align: center;">
                            <b class="u-text-green">Additional fee (+VAT): <span id="fee-value" data-old-fee="{{ $oldFee }}">£{{ number_format($additionalFee / 100, 2) }}</span></b>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
    <div class="divider"></div>
    @include('matrix.vehicles.cap-values')
</div>