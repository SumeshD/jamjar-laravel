<script>
let laravelCsrfToken = window.Laravel.csrfToken;
paypal.Button.render({
    env: '{{getenv('PAYPAL_ENV')}}',

    commit: true,

    payment: function(resolve, reject) {

        setTimeout(function() {
            $('.blackout').addClass('active');

            swal("We are just talking with PayPal. This process can take up to 30 seconds", {
              button: false,
            });

        }, 3000);

        let paymentUri = '{{route('partnerPayments')}}';

        paypal.request({
            method: 'post',
            url: paymentUri,
            headers: {
                'x-csrf-token': laravelCsrfToken
            },
            data: {
                productId: '{{ isset($amount->id) ? encrypt($amount->id) : 0 }}'
            },
        })
        .then(function(data) {
            resolve(data.id);
        })
        .catch(function(err) {
            console.log(err);
            reject(err);
        });
    },

    onAuthorize: function(data, actions) {
        let acceptPaymentUri = '{{route('partnerCompletePayment')}}';

        paypal.request({
            method: 'post',
            url: acceptPaymentUri,
            data: {
                paymentID: data.paymentID,
                payerID:   data.payerID,
                productId: '{{ isset($amount->id) ? encrypt($amount->id) : 0 }}'
            },
            headers: { 'x-csrf-token': laravelCsrfToken },
        })
        .then(function(data) {
            console.info('Payment Successful');
            window.location.href = '{{ route('partnerCredits') }}';
        })
        .catch(function(err) {
            console.log(err);
        });
   }
}, '#paypal-button');
</script>
