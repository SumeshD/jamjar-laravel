@extends('layouts.app')

@section('content')

@include('matrix.partials.header', ['title' => 'Funding &amp; Billing'])

<div class="modal credits" data-modal="buyCredits">
    <h2 class="modal__title">Add Funds</h2>
    <a class="modal__close"><i class="fa fa-times"></i></a>
    <form action="{{ route('partnerCheckout') }}" class="form" method="POST">
        <div class="modal__body">
            {{ csrf_field() }}

            <div class="form__group">
                <select name="amount" id="amount" class="form__input">
                    <option selected disabled>Choose your funds amount</option>
                    @foreach ($amounts as $amount)
                        <option value="{{$amount->id}}">&pound;{{$amount->price}} {{isset($amount->description) ? $amount->description : ''}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="modal__footer">
            <button type="submit" class="button button--green-bg"><i class="fa fa-paypal"></i> Continue to Checkout</button>
        </div>
    </form>
</div>

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'credits'])
        </div>
        <div class="dashboard__col">
        <!-- My Funds - Choose Your Leads -->
            <div>
                <div class="col-4 matchheight funds-left-box">
                    <div class="box box--with-header box--no-margin matchheight billing_remaining_funds" style="min-height: 450px;">
                        <div class="box__header">My Funds</div>
                        <div class="box__container">
                            <div class="box__content u-text-center">
        						<div class="box__item">
        							<p class="billing_remaining_funds__title">My Remaining Funds</p>
                                    {{-- @if ((str_contains(auth()->user()->email, 'sellyourjamjar.co.uk')))
                                        <h1 class="u-text-green billing_remaining_funds__amount">&pound;&infin;</h1>
                                    @else --}}
                                        <h1 class="u-text-green billing_remaining_funds__amount">&pound;{{auth()->user()->funds->funds}}</h1>
                                    {{-- @endif --}}
        						</div><!-- /.box__item -->

        						<a
                                    href="{{ route('partnerCredits') }}"
                                    data-toggle="buyCredits"
                                    class="button button--orange-bg button__full  billing_remaining_funds__button"
                                >
                                    ADD FUNDS
                                </a>
                            </div><!-- /.box__content -->
                        </div><!-- /.box__container -->
                    </div><!-- /.box -->
                </div><!-- col -->

                <div class="col-8 matchheight funds-right-box" style="width: calc(66.66666667% - 25px); float: right;">
                    <div class="box box--with-header box--no-margin matchheight">
                        <div class="box__header">Choose your leads</div>
                        <div class="box__container">
                            <div class="box__content">
                                @if (auth()->user()->profile->use_flat_fee == 1)
                                    <p>You are currently on a Flat Fee Per Lead system.</p>
                                    <br>
                                    <p>You currently pay <strong>&pound;{{ auth()->user()->profile->flat_fee ?? '0.00' }} (+VAT)</strong> per lead.</p>
                                    <br>
                                    <p>Please select which lead position you'd like to be listed under below.</p>
                                    <br>
                                    <p>Your selected lead position will appear in the '<a href="{{route('partnerValuations')}}">Paid Leads</a>' section of your Associate Area.</p>
                                    <br>
                                    <p>You will only be charged when you appear in your chosen minimum listing position or higher.</p>
                                    <br>
                                    <p>If you don't appear within your minimum listing position you will not be charged.</p>
                                    <br>
                                    <br>
                                    <p><strong>MINIMUM LISTING POSITION</strong></p>
                                    <matrix-position position="{{auth()->user()->company->position->position}}"></matrix-position>
                                @else
                                    <p>Please select which lead position you'd like to be listed under below.</p>
                                    <br>
                                    <p>Your selected lead position will appear in the '<a href="{{route('partnerValuations')}}">Paid Leads</a>' section of your Associate Area.</p>
                                    <br>
                                    <p>You will only be charged when you appear in your chosen minimum listing position or higher.</p>
                                    <br>
                                    <p>If you don't appear within your minimum listing position you will not be charged.</p>
                                    <br>
                                    <br>
                                    <p><strong>MINIMUM LISTING POSITION</strong></p>
                                    <matrix-position position="{{auth()->user()->company->position->position}}"></matrix-position>
                                @endif
                            </div><!-- /.box__content -->
                        </div><!-- /.box__container -->
                    </div><!-- /.box -->
                </div><!-- col -->
            </div><!-- /.row -->
            <!-- / My Funds - Choose Your Leads -->

            <!-- Calculator - How our fees work -->
            <div>
                <div class="box box--with-header">
                    <div class="box__header">Calculator - How our fees work</div>
                    <div class="box__container">
                        <div class="box__content">
                            @if (auth()->user()->profile->use_flat_fee == 1)
                            <div class="alert">
                                Please note, you are on our flat fee system, and as such the results from the calculator do not apply to you.
                            </div>
                            @endif
                            <p>Our fees are based on the value you give for a vehicle and the position you appear in the listings.</p>
                            <p>The position you select above will set which leads you recieve and are charged for.</p>
                            <p>Give our calculator a try to see exactly what you'll be charged.</p>
                            <br>
                            <matrix-cost-calculator></matrix-cost-calculator>

                        </div><!-- /.box__content -->
                    </div><!-- /.box__container -->
                </div><!-- /.box -->
            </div><!-- /.row -->
            <!-- / Calculator -->

            <!-- Invoices -->
            <div class="row">
                <div class="box box--with-header box--no-margin">
                    <div class="box__header">Invoices</div>
                    <div class="box__container">
                        <div class="box__content">
                            <p>
                                <strong>SHOWING THE LATEST 3 RESULTS</strong>
                                <a class="button button--orange-bg u-float-right" href="{{route('partnerInvoices')}}">SHOW ALL</a>
                            </p>
                            <table class="valuations__table jj_responsive_table invoices_table">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Invoice Number</th>
                                        <th>Amount (Net)</th>
                                        <th>VAT</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @unless (auth()->user()->orders->isEmpty())
                                    @foreach (auth()->user()->orders->take(3) as $order)
                                    <tr>
                                        <?php $price = $order->getOriginal('product_price') / 100; ?>
                                        <?php $netPrice = round($price / 1.20, 2) ?>
                                        <td>{{ $order->created_at->format('d/m/y H:i') }}</td>
                                        <td>{{$order->pp_payment_id}}</td>
                                        <td>&pound;{{ number_format($netPrice, 2) }}</td>
                                        <td>&pound;{{ number_format($price - $netPrice, 2) }}</td>
                                        <td>&pound;{{ $order->product_price }}</td>
                                        <td class="u-text-right"><a class="button button--orange-bg" href="{{ route('partnerInvoice', $order->pp_payment_id) }}" target="_blank">view invoice</a></td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">You haven't added funds to your account yet.</td>
                                        <td colspan="1" class="u-text-right">
                                            <a class="button button--orange-bg" data-toggle="buyCredits">
                                                Add Funds Now
                                            </a>
                                        </td>
                                    </tr>
                                @endunless
                                </tbody>
                            </table>
                        </div><!-- /.box__content -->
                    </div><!-- /.box__container -->
                </div><!-- /.box -->
            </div><!-- /.row -->
            <!-- / Invoices -->

            <!-- Statement - Latest Activity -->
            <div class="row">
                <div class="box box--with-header">
                    <div class="box__header">Statement - Latest Activity</div>
                    <div class="box__container">
                        <div class="box__content">
                            <p>
                                <strong>SHOWING THE LATEST 20 RESULTS</strong>
                                <a class="button button--orange-bg u-float-right" href="{{route('partnerStatements')}}">VIEW MORE</a>
                            </p>
                            <table class="valuations__table jj_responsive_table statement_table">
                                <thead>
                                    <tr>
                                        <th width="15%">Date/Time</th>
                                        <th width="50%">Activity</th>
                                        <th width="10%">Your Valuation</th>
                                        <th width="12%">Listing Position/Sale</th>
                                        <th width="13%">Funds Activity (inc. VAT)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($statements as $item)
                                <tr>
                                    <?php $fundsStatement = \JamJar\Primitives\FundsStatement::createFromRawData($item); ?>
                                    <td>{{ $fundsStatement->createdAt->format('d/m/Y H:i:s') }}</td>
                                    <td>{{ $fundsStatement->activity }}</td>
                                    <td>
                                        @if ($fundsStatement->valueInPence !== null)
                                            &pound;{{ number_format($fundsStatement->valueInPence / 100) }}
                                        @endif
                                    </td>
                                    <td>{{ $fundsStatement->listingPosition }}</td>
                                    <td>
                                        @if ($fundsStatement->fundsAmountInPence !== null)
                                            <strong class="statement__amount {{ $fundsStatement->fundsAmountInPence >= 0 ? 'u-text-green' : '' }}">
                                                {!! $fundsStatement->fundsAmountInPence >= 0 ? "&plus;" : "&minus;" !!}
                                                &pound;{{ number_format(abs($fundsStatement->fundsAmountInPence) / 100, 2) }}
                                            </strong>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p>
                                <a class="button button--orange-bg u-float-right" href="{{route('partnerStatements')}}">VIEW MORE</a>
                            </p>
                        </div><!-- /.box__content -->
                    </div><!-- /.box__container -->
                </div><!-- /.box -->
            </div><!-- /.row -->
            <!-- / Statement - Latest Activity -->
        </div><!-- /.dashboard__col -->
    </div>
</section><!-- /.dashboard__container -->
@endsection
