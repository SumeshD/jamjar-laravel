@extends('layouts.app')

@section('scripts')
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    @include('matrix.credits.partials.paypal-js')
@endsection

@section('content')

@include('matrix.partials.header', ['title' => 'Confirm Purchase'])

<section class="dashboard dashboard--two-col @if (!auth()->user()->isPartner()) dashboard--faded @endif">
    <div class="dashboard__container">
        <div class="dashboard__col">
            @include('matrix.partials.sidebar', ['active' => 'credits'])
        </div>
        <div class="dashboard__col">
            <div class="box box--with-table box--with-header box--no-margin">
                <div class="box__header">Confirm Purchase</div>
                <div class="box__container">
                    <div class="box__content">
                    @if ($amount)
                        <table>
                            <thead>
                                <tr>
                                    <th>Purchasing</th>
                                    <th>Amount (Net)</th>
                                    <th>VAT</th>
                                    <th>Total</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <?php $price = (float) $amount->price; ?>
                                    <?php $netPrice = round($price / 1.20, 2) ?>
                                    <td>Adding Funds</td>
                                    <td>&pound;{{ number_format($netPrice, 2) }}</td>
                                    <td>&pound;{{ number_format($price - $netPrice, 2) }}</td>
                                    <td>&pound;{{ $amount->price }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <p class="u-text-muted">
                            <small>Please review the products you have selected.</small>
                        </p>
                        <p class="u-text-muted">
                            <small>Everything looks good? Click the PayPal Button to Pay.</small>
                        </p>
                    </div>
                    <div class="box__footer box__footer--no-border">
                        <div id="paypal-button"></div>
                    </div>
                    @else
                        <p>You haven't added any credits to your cart yet.</p>
                        <p><a href="{{route('partnerCredits')}}" class="button button--orange">Go Back?</a></p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
