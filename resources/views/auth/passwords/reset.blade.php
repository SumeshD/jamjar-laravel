@extends('layouts.app')

@section('content')
<section class="jamjar__container">
	<div class="box box--narrow box--auth">
		<div class="box__container">
			<h1 class="box__title box__title--centered">Reset your jamjar password</h1>
			<div class="box__content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.request') }}">
                	{{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

					<div class="form__group {{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="form__label">E-Mail Address</label>
						<input type="email" name="email" id="email" class="form__input" value="{{ $email or old('email') }}" placeholder="Enter your E-Mail address" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
					</div>

					<div class="form__group {{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="form__label">Password</label>
						<input type="password" name="password" id="password" class="form__input" placeholder="Enter your Password" required>
						@if ($errors->has('password'))
						    <span class="help-block">
						        <strong>{{ $errors->first('password') }}</strong>
						    </span>
						@endif
					</div>

					<div class="form__group {{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password-confirm" class="form__label">Confirm Password</label>
						<input type="password" name="password_confirmation" id="password-confirm" class="form__input" placeholder="Please confirm your password" required>
					</div>

                	<div class="form__submit">
                        <input type="submit" value="Let me in!">
                	</div>
                </form>
			</div>
		</div>
	</div>
</section>
@endsection
