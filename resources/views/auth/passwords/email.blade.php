@extends('layouts.app')

@section('content')
<section class="jamjar__container">
	<div class="box box--narrow box--auth">
		<div class="box__container">
			<h1 class="box__title box__title--centered">Reset your jamjar password</h1>
			<div class="box__content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}" class="form">
                	{{ csrf_field() }}
                	<div class="form__group">
                        <label for="email" class="form__label">E-Mail Address</label>
                        <input id="email" type="email" class="form__input" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                	</div>
                	<div class="form__submit">
                        <input type="submit" value="Send Password Reset">
                	</div>
                </form>
			</div>
		</div>
	</div>
</section>
@endsection
