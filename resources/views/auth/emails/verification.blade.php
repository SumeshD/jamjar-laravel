@extends('layouts.app')

@section('content')
<section class="jamjar__container">
	<div class="box box--with-header">
		<h1 class="box__header">Just one more thing</h1>
		<div class="box__container">
			<div class="box__content">
				<p>Thanks for registering with jamjar. We've just sent you an email with a link which you need to click in order to activate your account.</p>
			</div>
		</div>
	</div>
</section>
@endsection
