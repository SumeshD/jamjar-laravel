@extends('layouts.app')

@section('content')
<section class="jamjar__container">
	<div class="box box--with-header">
		<h1 class="box__header">Email Verified</h1>
		<div class="box__container">
			<div class="box__content">
				<p>Thanks for confirming your email address. You are now logged in.</p>
				<p><a href="{{ route('home') }}" class="button button--orange">Go to Dashboard</a></p>
			</div>
		</div>
	</div>
</section>
@endsection