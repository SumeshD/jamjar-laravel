@extends('newlayouts.app')

@section('content')
	<main role="main">
		<section class="page-section">
			<div class="page-section__content page-section__content--registration page-section__content--centered">
				<h2>
					Sign in to your account
				</h2>
				<p class="small">
					Enter the email address registered with your JamJar account to retrieve your password.
				</p>
				<form action="{{ route('login') }}" method="POST">
					{{ csrf_field() }}
					<label for="email">
						Your email address *
						<input type="email" name="email" id="email" value="{{ old('email') }}">
					</label>
					@if ($errors->has('email'))
						<span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
					@endif
					<label for="password">
						Your password *
						<input type="password" name="password" id="password">
					</label>
					@if ($errors->has('password'))
						<span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
					@endif
					<button type="submit">Sign in to your account</button>
					<p class="small"><a href="{{ route('password.request') }}" title="Forgotten password"><small>Forgotten password</small></a></p>
				</form>
			</div>
		</section>
	</main><!-- Main page content -->




{{--<section class="jamjar__container">--}}
{{--	<div class="box box--narrow box--auth">--}}
{{--		<div class="box__container">--}}
{{--			<h1 class="box__title box__title--centered">Welcome Back</h1>--}}
{{--			<div class="box__content">--}}
{{--				<form method="POST" action="{{ route('login') }}" class="form">--}}

{{--                    {{ csrf_field() }}--}}

{{--					<div class="form__group {{ $errors->has('email') ? ' has-error' : '' }}">--}}
{{--						<label for="email" class="form__label">E-Mail Address</label>--}}
{{--                        <input id="email" type="email" class="form__input" name="email" value="{{ old('email') }}" placeholder="Enter your E-Mail address" required autofocus>--}}
{{--                        @if ($errors->has('email'))--}}
{{--                            <span class="help-block">--}}
{{--                                <strong>{{ $errors->first('email') }}</strong>--}}
{{--                            </span>--}}
{{--                        @endif--}}
{{--					</div>--}}

{{--					<div class="form__group {{ $errors->has('password') ? ' has-error' : '' }}">--}}
{{--						<label for="password" class="form__label">Password</label>--}}
{{--                        <input id="password" type="password" class="form__input" name="password" placeholder="Enter your password" required>--}}
{{--                        @if ($errors->has('password'))--}}
{{--                            <span class="help-block">--}}
{{--                                <strong>{{ $errors->first('password') }}</strong>--}}
{{--                            </span>--}}
{{--                        @endif--}}
{{--					</div>--}}

{{--					<div class="form__group">--}}
{{--						<div class="form__checkbox" style="width: 100%; padding: 0;">--}}
{{--							<label>--}}
{{--								<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
{{--							</label>--}}
{{--						</div>--}}
{{--					</div>--}}

{{--					<div class="form__submit form__submit--no-border">--}}
{{--						<input type="submit" class="form__input" value="Login">--}}
{{--					</div>--}}
{{--				</form>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div class="u-text-center">--}}
{{--			<p><a href="{{ route('password.request') }}" class="u-text-grey">Forgotten Your Password?</a></p>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--</section>--}}
@endsection
