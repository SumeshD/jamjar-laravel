@extends('layouts.app')

@section('content')
<section class="jamjar__container">
    <div class="box box--narrow box--auth">
        <div class="box__container">
            <h1 class="box__title box__title--centered">Sorry about that...</h1>
            <div class="box__content">
                <p>The server was unable to complete your request.</p>
                <p style="margin-bottom: 0.5em;"><strong>Why did this happen?</strong></p>
                <ul class="u-full-list">
                    <li><i class="fa fa-chevron-right"></i> Our servers are just too busy.</li>
                    <li><i class="fa fa-chevron-right"></i> The page took too long to send a reply.</li>
                    <li><i class="fa fa-chevron-right"></i> We had a little hiccup, try reloading the page.</li>
                </ul>
                <p>Just for good measure, Jamjar's technical team have been notified about this incident.</p>
                <p>Have you been seeing this for more than a few minutes? Please <a href="https://www.jamjar.com/contact-us/">get in touch.</a></p>
            </div>
        </div>
    </div>
</section>
@endsection
