@extends('errors::layout')

@section('title', 'Service Unavailable')

@section('message')
    <div style="width: 80%; margin: 0 auto;">
        <img src="https://www.jamjar.com/assets/images/jamjar-logo.png">
        <h3>Jamjar is currently down for some scheduled maintenance. We'll be back soon, thank you for your patience.</h3>
    </div>
@endsection