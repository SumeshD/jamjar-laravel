@extends('layouts.app')

@section('content')
<main class="jamjar__container">
    <div class="box box--with-header">
        <div class="box__header">Cost Calculator</div>
        <div class="box__container">
            <div class="box__content">
                <matrix-cost-calculator></matrix-cost-calculator>
            </div><!-- /.box__content -->
        </div><!-- /.box__container -->
    </div><!-- /.box -->
</main>
@endsection
