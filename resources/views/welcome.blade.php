@extends('layouts.app')

@section('content')
<section class="jamjar__container">
	
	<div class="box">
		<div class="box__container">
			<div class="box__content">
				<form action="{{ route('apiVrmInit') }}" method="GET" class="form">
					<label for="" class="form__label">Numberplate Search</label>
					<input type="text" class="form__input" id="vrm" name="vrm">
				</form>
			</div>
		</div>
	</div>

</section>

{{-- http://webservices.capnetwork.co.uk/capdvla_webservice/capdvla.asmx/DVLALookupVRM --}}
@endsection