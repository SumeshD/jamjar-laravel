@extends('mail.master')

@section('title')
Claim your jamjar account
@endsection

@section('button')
<p><a href="{{ url('/login') }}" class="button button--orange">Click here to login</a></p>
@endsection

@section('content')
	<p>Thanks for using jamjar.com.</p>
	<p>You recently received a valuation on jamjar.com</p>
	<p>In order to save your valuations we have created you an account with a temporary password.</p>
	<p>Your temporary password is <pre>{{ $password }}</pre></p>
	<p>Please click the button below to log in.</p>
@endsection
