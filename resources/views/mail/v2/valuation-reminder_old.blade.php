@extends('mail.v2.master')

@section('title', 'JamJar - Your reminder')

@section('styles')

    <style>

        @font-face {
            font-family: SourceSansBold;
            src: url(https://jamjar-prod.s3.eu-west-2.amazonaws.com/fonts/SourceSansPro-Bold.ttf);
        }

        @font-face {
            font-family: SourceSansRegular;
            src: url(https://jamjar-prod.s3.eu-west-2.amazonaws.com/fonts/SourceSansPro-Regular.ttf);
        }

        .jumbo{
            background-color:#000000;
            height:461px;
            background-image: url(https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/automobiles-automotives-black-and-white-709121.png);
            background-position: right;
            background-repeat: no-repeat;
        }

        .jumbo-main-text{
            margin-top:60px;
            font-family: SourceSansBold;
            font-size: 60px;
            font-weight: 900;
            font-style: normal;
            font-stretch: normal;
            line-height: 1;
            letter-spacing: -0.72px;
            color: #f89c20;
        }

        .jumbo-small-text{

            font-family: SourceSansRegular;
            font-size: 26px;
            font-weight: 300;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: -0.31px;
            color: #ffffff;
        }

        .now{
            font-weight: 900;
        }

        .line{
            height: 1px;
            background-color: #6d6f71;
            Margin-bottom: 15px;
        }

        h2,h3,p{
            margin-bottom:15px !important;
        }

        .money-text{
            font-family: Arial;
            font-size: 60px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1;
            letter-spacing: -0.72px;
            color: #ffffff;
        }

        .accept-valuation{

            height: 56px;
            border-radius: 4px;
            box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.24);
            background-color: #f89c20;
            font-family: Arial;
            font-size: 16px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: 3.5;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            display:block;
        }

        .accept-valuation:visited{
            color: #ffffff;
        }

        .accept-valuation:hover{
            color: #ffffff;
        }

        .valuation-box{

            height: 86px;
            border-radius: 2px;
            border-top: solid 1px #e2e2e2;
            border-bottom: solid 3px #f89c20;
            padding-bottom:0px !important;
        }

        .vb-left{

            border-left: solid 1px #e2e2e2;
        }

        .vb-right{

            border-right: solid 1px #e2e2e2;
        }

        .box-text{

            margin:0px;

        }

        .main-text{

            font-family: Arial;
            font-size: 16px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.63;
            letter-spacing: -0.4px;
            color: #372e2c;
        }

        .val-price{

            font-family: Arial;
            font-size: 18px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: 3.33;
            letter-spacing: -0.22px;
            color: #372e2c;
        }

        .link{
            color: #f89c20 !important;
        }

        .val-title{

            ont-family: Arial;
            font-size: 18px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.11;
            letter-spacing: normal;
            color: #4a4a4a;
        }

        .gray-box{

            background-color: #f4f4f4;
            padding:30px 45px 20px 45px;
            position: relative;
            top:-30px;
        }

        .quote{


            font-family: Arial;
            font-size: 22px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.45;
            letter-spacing: -0.5px;
            text-align: justify;
            color: #000000;
        }

        .author{

            font-family: Arial;
            font-size: 16px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.44;
            letter-spacing: -0.21px;
            color: #372e2c;
            text-align:right
        }

        .bottom-links a{

            font-family: Arial;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.69;
            letter-spacing: normal;
            color: #f89c20 !important;
        }

        .tel-link{
            font-size: 22px;
        }

        .mail-link{

            font-size: 18px;
        }

        .custom-padding{

            padding:0px 30px;
        }

        .socials{

            display: flex;
            justify-content: center;
            height:50px;
        }

        .social-image{

            margin-left:15px;
            width:43px;
        }

        @media (max-width: 596px) {

            .valuation-box{

                height:50px;
                border: none !important;
            }



            .val-price{

                text-align:left !important;
                border-bottom:0px !important;
            }

            .box-text{

                border-bottom:0px !important;
            }

            .custom-padding{

                padding:0 53px 0 23px  !important;
            }

            .vb-left{

                padding:15px 15px 0px 15px !important;
                border: solid 1px #e2e2e2 !important;
                border-bottom: none !important;
            }

            .vb-right{

                padding:0px 15px 15px 15px !important;
                border: solid 1px #e2e2e2 !important;
                border-bottom: solid 3px #f89c20 !important;
                border-top:none !important;
            }

            .mobile-img-padding{

                padding-left:60px;
            }

            .gray-box{

                padding: 30px 30px 20px 30px;
            }

            .gray-box p{
                text-align: left;
            }


        }

    </style>

@endsection

@section('content')

    <table class="container">
        <tr>
            <td class="main-padding jumbo">
                <table class="row">
                    <tr >
                        <table class="row " >
                            <tr>
                                <th class="small-12 large-6 first columns">

                                    <table align="left" class=" text-center float-left">
                                        <tr>
                                            <td class="mobile-right-padding">
                                                <h2 class="jumbo-main-text">Don't<br>miss out</h2>
                                                <p class="jumbo-small-text">Sell your car <span class="now">now</span> for...</p>
                                                <div class="line"></div>
                                                <p class="money-text text-center">{{$valuation->value}}</p>
                                                <a href="{{route('showSavedValuation', $valuation->uuid)}}" class="accept-valuation">Accept your valuation</a>
                                            </td>
                                        </tr>
                                    </table>

                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-12 first columns">

                                    <table align="left" class="text-center float-left">
                                        <tr>
                                            <td class="mobile-right-padding">
                                                <p class="main-text" style="margin-top:50px">Hello {{$user->first_name}}<br>
                                                    Thank you for choosing <a href="/" target="_blank" class="link">jamjar.com</a> to sell your car.<br>
                                                    We're pleased to provide your current vehicle valuation:</p>
                                            </td>
                                        </tr>
                                    </table>

                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="custom-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>

                                <th class="small-12 large-7 first columns valuation-box vb-left " valign="middle">
                                    <p class="text-left box-text" valign="middle"> {{$vehicle->meta->manufacturer}} {{$vehicle->meta->model}} {{$vehicle->meta->derivative}}
                                    </p>
                                </th>
                                <th class="small-12 large-5 last columns valuation-box vb-right " valign="middle">
                                    <p class="text-right box-text val-price" valign="middle">{{$valuation->value}}
                                    </p>
                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding ">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>

                                <th class="small-12 large-12 first columns " valign="middle">
                                    <h3 class="text-left val-title" valign="middle" style="margin-top:40px">Improve your valuations
                                    </h3>
                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-12 first columns">

                                    <table align="left" class="text-center float-left">
                                        <tr>
                                            <td class="mobile-right-padding">
                                                <p class="main-text">Not received the valuation you were expecting for your car? Our current valuation is based on the information
                                                you gave us, but your car could hold more value. Simply enter more details about your car to help build a complete
                                                picture to receive updated offers.</p>
                                            </td>
                                        </tr>
                                    </table>

                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-6 first columns">

                                    <table align="left" class="text-center float-left">
                                        <tr>
                                            <td class="mobile-right-padding">
                                                <a href="{{route('showAdditionalInformationForm', $vehicle->id)}}" class="accept-valuation">Tell us more about your car</a>
                                            </td>
                                        </tr>
                                    </table>

                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td>
                <table class="row">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-12 first columns" style="padding:0px;">

                                    <table align="right" class="text-center float-right">
                                        <tr>
                                            <td class="mobile-img-padding">
                                                <img class="float-right" alt="Sell Your Car" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/carpeople.png" style="margin-top:40px"/>
                                            </td>
                                        </tr>
                                    </table>

                                </th>

                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-12 first columns" >

                                    <table align="right" class="text-center float-right">
                                        <tr>
                                            <td class="mobile-right-padding">
                                                <div class="gray-box">
                                                    <p class="quote">"I needed a quick and easy way to sell my car. Jamjar provided an excellent service."</p>
                                                    <p class="author">Dean Chatterton</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>

                                <th class="small-12 large-12 first columns " valign="middle">
                                    <h3 class="text-left val-title" valign="middle" style="margin-top:20px">About your valuations
                                    </h3>
                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-12 first columns">

                                    <table align="left" class="text-center float-left">
                                        <tr>
                                            <td class="mobile-right-padding">
                                                <p class="main-text">Selling your car has never been easier! Simply click the button below to view your cars
                                                valuations confirm the deal and initiate contact with the buyer online, making the whole process as smooth as possible.</p>
                                            </td>
                                        </tr>
                                    </table>

                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-6 first columns">

                                    <table align="left" class="text-center float-left">
                                        <tr>
                                            <td class="mobile-right-padding">
                                                <a href="{{route('showSavedValuation', $valuation->uuid)}}" class="accept-valuation">View your latest valuations</a>
                                            </td>
                                        </tr>
                                    </table>

                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td>
                <table class="row">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-12 first columns" style="padding:0px;">

                                    <table align="right" class="text-center float-right">
                                        <tr>
                                            <td class="mobile-img-padding">
                                                <img class="float-right" alt="Car Valuations" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/peopleinsidecar.png" style="margin-top:40px"/>
                                            </td>
                                        </tr>
                                    </table>

                                </th>

                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-12 first columns" >

                                    <table align="right" class="text-center float-right">
                                        <tr>
                                            <td class="mobile-right-padding">
                                                <div class="gray-box">

                                                    <div class="socials">
                                                        <div class="social-image">
                                                           <a href="https://twitter.com/jamjarcompare"><img alt="Twitter" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/twitter-circle.png"/></a>
                                                        </div>
                                                        <div class="social-image">
                                                            <a href="https://www.facebook.com/Jamjarcompare/"><img  alt="Facebook" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/facebook-circle.png"/></a>
                                                        </div>
                                                        <div class="social-image">
                                                            <a href="https://www.instagram.com/jamjarcompare/"><img  alt="Instagram" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/instagram-circle.png"/></a>
                                                        </div>



                                                    </div>

                                                    <p class="author">Be the first to know about our latest news, offers, and car tips. Like us on
                                                    Facebook and follow us on Twitter and Instagram.</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>

                                <th class="small-12 large-12 first columns " valign="middle">
                                    <h3 class="text-left val-title" valign="middle" style="margin-top:20px">We're here to help.
                                    </h3>
                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>
                                <th class="small-12 large-12 first columns">

                                    <table align="left" class="text-center float-left">
                                        <tr>
                                            <td class="mobile-right-padding">
                                                <p class="main-text">Our award-winning Customer Care Team is available 24/7. <br>If you have any questions, please contact support team.</p>
                                            </td>
                                        </tr>
                                    </table>

                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="container">
        <tr>
            <td class="main-padding">
                <table class="row main-text">
                    <tr >
                        <table class="row" >
                            <tr>

                                <th class="small-12 large-12 first columns " valign="middle">
                                    <h3 class="text-left val-title bottom-links tel-link" valign="middle" style="margin-bottom:5px !important"><a href="tel:0115 870 9903">0115 870 9903</a>
                                    </h3>
                                    <h3 class="text-left val-title bottom-links mail-link" valign="middle"><a href="mailto:customer.care@jamjar.com">customer.care@jamjar.com</a>
                                    </h3>
                                </th>
                                <th class="expander"></th>
                            </tr>
                        </table>
                    </tr>
                </table>

            </td>
        </tr>
    </table>


@endsection