<?php $unsubscribeRoute = isset($unsubscribeRoute) ? $unsubscribeRoute : 'notificationUnsubscribe' ?>
@php ($link = Autologin::to($valuation->user, route($unsubscribeRoute)))

<div  style="background:#f4f4f4;background-color:#f4f4f4;margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f4f4f4;background-color:#f4f4f4;width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="border-bottom:3px solid #f89c20;direction:ltr;font-size:0px;padding:20px 0px 0px 0px;text-align:center;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                class="" style=""
                        >

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                            >
                                <tr>
                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <![endif]-->


                <div  style="margin:0px auto;max-width:600px;">

                    <table
                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                    >
                        <tbody>
                        <tr>
                            <td
                                    style="direction:ltr;font-size:0px;padding:20px 0;padding-left:55px;padding-right:55px;text-align:center;"
                            >
                                <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                        <td
                                                class="" width="600px"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:490px;" width="490"
                                            >
                                                <tr>
                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]-->


                                <div  style="margin:0px auto;max-width:490px;">

                                    <table
                                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                                    >
                                        <tbody>
                                        <tr>
                                            <td
                                                    style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                                            >
                                                <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                    <tr>

                                                        <td
                                                                class="" style="vertical-align:top;width:490px;"
                                                        >
                                                <![endif]-->

                                                <div
                                                        class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                                >

                                                    <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                                    >

                                                        <tr>
                                                            <td
                                                                    align="left" style="font-size:0px;padding:0px;word-break:break-word;"
                                                            >

                                                                <table
                                                                        border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                                                >
                                                                    <tbody>
                                                                    <tr>
                                                                        <td  style="width:157px;">

                                                                            <a
                                                                                    href="/" target="_blank"
                                                                            >

                                                                                <img
                                                                                        alt="Jamjar logo" height="auto" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/jamjar-logo.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="157"
                                                                                />

                                                                            </a>

                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>

                                                <!--[if mso | IE]>
                                                </td>

                                                </tr>

                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>


                                <!--[if mso | IE]>
                                </td>
                                </tr>
                                </table>

                                </td>
                                </tr>

                                <tr>
                                    <td
                                            class="" width="600px"
                                    >

                                        <table
                                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:490px;" width="490"
                                        >
                                            <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]-->


                                <div  style="margin:0px auto;max-width:490px;">

                                    <table
                                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                                    >
                                        <tbody>
                                        <tr>
                                            <td
                                                    style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                                            >
                                                <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                    <tr>

                                                        <td
                                                                class="" style="vertical-align:top;width:490px;"
                                                        >
                                                <![endif]-->

                                                <div
                                                        class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                                >

                                                    <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                                    >

                                                        <tr>
                                                            <td
                                                                    align="left" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;"
                                                            >

                                                                <div
                                                                        style="font-family:Lato, Arial;font-size:11px;font-weight:400;line-height:18px;text-align:left;color:#9b9b9b;"
                                                                >Registered address: <a href="/" style="color:#f89c20;text-decoration:none">Jamjar.com</a>, 74-76 Grantham Road, Nottingham, NG12 2HY, United Kingdom.<br>
                                                                    To unsubscribe from these emails <a href="{{ $link }}" style="color:#f89c20;text-decoration:none">click here</a>.<br>
                                                                    Use of the service and website is subject to our <a href="/terms-conditions" style="color:#f89c20;text-decoration:none">Terms of Use</a> and <a href="/privacy" style="color:#f89c20;text-decoration:none">Privacy Statement</a>.<br><br>
                                                                    @ Copyright <a href="/" style="color:#f89c20;text-decoration:none">jamjar.com</a> 1997 - {{date('Y')}}</div>

                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>

                                                <!--[if mso | IE]>
                                                </td>

                                                </tr>

                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>


                                <!--[if mso | IE]>
                                </td>
                                </tr>
                                </table>

                                </td>
                                </tr>

                                </table>
                                <![endif]-->
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>


                <!--[if mso | IE]>
                </td>
                </tr>
                </table>

                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>