<?php $unsubscribeRoute = isset($unsubscribeRoute) ? $unsubscribeRoute : 'notificationUnsubscribe' ?>
@php ($link = Autologin::to($valuation->user, route($unsubscribeRoute)))

<table class="row">
    <tr >
        <table class="row header" >
            <tr>
                <th class="small-12 large-12 first columns">
                    <a href="/" >
                            <img class="float-left" alt="JamJar" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/jamjar-logo.png" style="margin-top:40px"/>
                    </a>

                </th>
                <th class="expander"></th>
            </tr>
        </table>
        <table class="row header" >
            <tr>
                <th class="small-12 large-12 first columns">
                    <p class="footer-text mobile-right-padding">Registered address: Jamjar.com, 74-76 Grantham Road, Nottingham, NG12 2HY, United Kingdom.<br>
                        To update your emails preferences <a href="{{ $link }}">click here</a>.<br>
                        Use of the service and website is subject to our <a href="/terms-conditions">Terms of Use</a> and <a href="/privacy">Privacy Statement</a>.<br><br>
                        @ Copyright <a href="/">jamjar.com</a> 1997 - {{date('Y')}}
                    </p>
                </th>
                <th class="expander"></th>
            </tr>
        </table>
    </tr>
</table>
