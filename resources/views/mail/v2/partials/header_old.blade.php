<table class="row">
    <tr >
        <table class="row header" >
            <tr>
                <th class="small-12 large-5 first " valign="middle">
                    <a href="/" target="_blank">

                        <img class="header-logo" alt="JamJar" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/jamjar-logo.png" />

                    </a>
                </th>
                <th class="small-12 large-7 last " valign="middle">
                    <table align="center" class="menu text-center float-center">
                        <tr>
                            <td class="menu">
                                <table>
                                    <tr>
                                        <th class="menu-item float-center header-menu"><a href="/sell-my-car" target="_blank">Sell my car</a></th>
                                        <th class="menu-item float-center header-menu"><a href="/about" target="_blank">About us</a></th>
                                        <th class="menu-item float-center header-menu"><a href="/contact-us" target="_blank">Contact us</a></th>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </th>
                <th class="expander"></th>
            </tr>
        </table>
    </tr>
</table>