<div  style="margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="direction:ltr;font-size:0px;padding:20px 0;padding-left:30px;padding-right:30px;text-align:center;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                class="" style="vertical-align:top;width:216px;"
                        >
                <![endif]-->

                <div
                        class="mj-column-per-40 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                    >

                        <tr>
                            <td
                                    align="center" style="font-size:0px;padding:10px;word-break:break-word;"
                            >

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                >
                                    <tbody>
                                    <tr>
                                        <td  style="width:157px;">

                                            <a
                                                    href="/" target="_blank"
                                            >

                                                <img
                                                        alt="Jamjar logo" height="auto" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/jamjar-logo.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="157"
                                                />

                                            </a>

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>

                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                <td
                        class="" style="vertical-align:top;width:324px;"
                >
                <![endif]-->

                <div
                        class="mj-column-per-60 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                    >

                        <tr>
                            <td
                                    align="center" style="font-size:0px;padding-top:5px;word-break:break-word;"
                            >


                                <div
                                        class="mj-inline-links" style=""
                                >

                                    <!--[if mso | IE]>

                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr>



                                            <td
                                                    style="padding:15px 10px;" class=""
                                            >

                                    <![endif]-->


                                    <a
                                            class="mj-link" href="/sell-my-car" target="_blank" style="display:inline-block;color:#372e2c;font-family:Arial;font-size:14px;font-weight:400;line-height:1.86;text-decoration:underline;text-transform:none;padding:15px 10px;"
                                    >
                                        Sell my car
                                    </a>


                                    <!--[if mso | IE]>

                                    </td>



                                    <td
                                            style="padding:15px 10px;" class=""
                                    >

                                    <![endif]-->


                                    <a
                                            class="mj-link" href="/about" target="_blank" style="display:inline-block;color:#372e2c;font-family:Arial;font-size:14px;font-weight:400;line-height:1.86;text-decoration:underline;text-transform:none;padding:15px 10px;"
                                    >
                                        About us
                                    </a>


                                    <!--[if mso | IE]>

                                    </td>



                                    <td
                                            style="padding:15px 10px;" class=""
                                    >

                                    <![endif]-->


                                    <a
                                            class="mj-link" href="/contact-us" target="_blank" style="display:inline-block;color:#372e2c;font-family:Arial;font-size:14px;font-weight:400;line-height:1.86;text-decoration:underline;text-transform:none;padding:15px 10px;"
                                    >
                                        Contact us
                                    </a>


                                    <!--[if mso | IE]>

                                    </td>



                                    </tr></table>

                                    <![endif]-->

                                </div>

                            </td>
                        </tr>

                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>

