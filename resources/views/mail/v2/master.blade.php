<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title>@yield('title')</title>
    <!--[if !mso]><!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a { padding:0; }
        body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
        table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
        img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
        p { display:block;margin:13px 0; }
    </style>
    <!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
        .outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->

    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Lato:300,400,500,700');
    </style>
    <!--<![endif]-->



    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-40 { width:40% !important; max-width: 40%; }
            .mj-column-per-60 { width:60% !important; max-width: 60%; }
            .mj-column-per-70 { width:70% !important; max-width: 70%; }
            .mj-column-per-30 { width:30% !important; max-width: 30%; }
            .mj-column-per-100 { width:100% !important; max-width: 100%; }
            .mj-column-per-50 { width:50% !important; max-width: 50%; }
        }
    </style>

    <style type="text/css">
        @media (max-width:410px) {
            .box-padding { padding-left:20px !important;padding-right:20px !important;}
        }
    </style>


    <style type="text/css">



        @media only screen and (max-width:480px) {
            table.full-width-mobile { width: 100% !important; }
            td.full-width-mobile { width: auto !important; }
        }


        noinput.mj-menu-checkbox { display:block!important; max-height:none!important; visibility:visible!important; }

        @media only screen and (max-width:480px) {
            .mj-menu-checkbox[type="checkbox"] ~ .mj-inline-links { display:none!important; }
            .mj-menu-checkbox[type="checkbox"]:checked ~ .mj-inline-links,
            .mj-menu-checkbox[type="checkbox"] ~ .mj-menu-trigger { display:block!important; max-width:none!important; max-height:none!important; font-size:inherit!important; }
            .mj-menu-checkbox[type="checkbox"] ~ .mj-inline-links > a { display:block!important; }
            .mj-menu-checkbox[type="checkbox"]:checked ~ .mj-menu-trigger .mj-menu-icon-close { display:block!important; }
            .mj-menu-checkbox[type="checkbox"]:checked ~ .mj-menu-trigger .mj-menu-icon-open { display:none!important; }
        }

    </style>
    <style type="text/css">.bg-wrapper, .bg-wrapper > div > table { background-position: right center !important; }
        .rel-position{position:relative;bottom:20px}
        @media (max-width: 420px) {

            .hero-main-text div{
                font-size:45px !important;
            }

            .val-price div{
                text-align:left !important;
            }

            .button-width,.button-width table{
                width:100% !important;
            }
        }</style>

</head>
<body style="background-color:#ffffff;">
<div style="display:none">{{$preheaderText}}</div>

<div
        style="background-color:#ffffff;"
>

    <!--[if mso | IE]>
    <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->

    @include('mail.v2.partials.header')


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    @yield('content')


    <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    @include('mail.v2.partials.footer')


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->


</div>

</body>
</html>
  