@extends('mail.v2.master')

@section('title', 'JamJar - Your reminder')

@section('content')

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="bg-wrapper-outlook" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">

                <v:rect  style="width:600px;" xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false">
                    <v:fill  origin="0.5, 0" position="0.5, 0" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/automobiles-automotives-black-and-white-709121.png" color="#000000" type="tile" />
                    <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                        <![endif]-->

                        <div  class="bg-wrapper" style="background:#000000 url(https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/automobiles-automotives-black-and-white-709121.png) top center / contain no-repeat;margin:0px auto;max-width:600px;">
                            <div  style="line-height:0;font-size:0;">
                                <table
                                        align="center" background="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/automobiles-automotives-black-and-white-709121.png" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#000000 url(https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/automobiles-automotives-black-and-white-709121.png) top center / contain no-repeat;width:100%;"
                                >
                                    <tbody>
                                    <tr>
                                        <td
                                                style="direction:ltr;font-size:0px;padding:20px 0;padding-left:30px;padding-right:30px;padding-top:50px;text-align:center;"
                                        >
                                            <!--[if mso | IE]>
                                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                <tr>

                                                    <td
                                                            class="" style="vertical-align:top;width:378px;"
                                                    >
                                            <![endif]-->

                                            <div
                                                    class="mj-column-per-70 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                            >

                                                <table
                                                        border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                                >



                                                    <tr>
                                                        <td
                                                                align="left" class="hero-main-text" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                        >

                                                            <div
                                                                    style="font-family:Raleway, Arial;font-size:60px;font-weight:900;line-height:60px;text-align:left;color:#f89c20;"
                                                            >Accept your best offer</div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td
                                                                align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                        >

                                                            <div
                                                                    style="font-family:Raleway, Arial;font-size:26px;font-weight:300;line-height:26px;text-align:left;color:#ffffff;"
                                                            >Sell your {{$vehicleType}} <strong>now</strong> for...</div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td
                                                                style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                        >

                                                            <p
                                                                    style="border-top:solid 1px #6d6f71;font-size:1;margin:0px auto;width:100%;"
                                                            >
                                                            </p>

                                                            <!--[if mso | IE]>
                                                            <table
                                                                    align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #6d6f71;font-size:1;margin:0px auto;width:328px;" role="presentation" width="328px"
                                                            >
                                                                <tr>
                                                                    <td style="height:0;line-height:0;">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <![endif]-->


                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td
                                                                align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                        >

                                                            <div
                                                                    style="font-family:Lato, Roboto, Arial;font-size:60px;font-weight:400;line-height:1;text-align:center;color:#ffffff;"
                                                            >{{$valuation->value}}</div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td
                                                                align="center" vertical-align="middle" class="button-width" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                        >

                                                            <table
                                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:100%;line-height:100%;"
                                                            >
                                                                <tr>
                                                                    <td
                                                                            align="center" bgcolor="#f89c20" role="presentation" style="border:none;border-radius:3px;cursor:auto;height:56px;mso-padding-alt:10px 25px;background:#f89c20;" valign="middle"
                                                                    >
                                                                        <a
                                                                                href="{{route('showSavedValuation', $valuation->uuid)}}"   style="display:inline-block;background:#f89c20;color:#ffffff;font-family:Lato, Roboto, Arial;font-size:16px;font-weight:900;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;"
                                                                        >
                                                                            Accept your valuation
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>

                                                </table>

                                            </div>

                                            <!--[if mso | IE]>
                                            </td>

                                            <td
                                                    class="" style="vertical-align:top;width:162px;"
                                            >
                                            <![endif]-->

                                            <div
                                                    class="mj-column-per-30 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                            >

                                                <table
                                                        border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                                >

                                                </table>

                                            </div>

                                            <!--[if mso | IE]>
                                            </td>

                                            </tr>

                                            </table>
                                            <![endif]-->
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--[if mso | IE]>
                        </v:textbox>
                        </v:rect>

                        </td>
                        </tr>
                        </table>



                        <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                        >
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->


                        <div  style="margin:0px auto;max-width:600px;">

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                            >
                                <tbody>
                                <tr>
                                    <td
                                            style="direction:ltr;font-size:0px;padding:20px 0;padding-left:30px;padding-right:30px;text-align:center;"
                                    >
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                            <tr>

                                                <td
                                                        class="" style="vertical-align:top;width:540px;"
                                                >
                                        <![endif]-->

                                        <div
                                                class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                        >

                                            <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                            >

                                                <tr>
                                                    <td
                                                            align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                    >

                                                        <div
                                                                style="font-family:Lato, Roboto, Arial;font-size:16px;font-weight:400;line-height:26px;text-align:left;color:#372e2c;"
                                                        >Hello {{$user->first_name}}<br>
                                                            Thanks for using <a href="/" style="color:#f89c20">jamjar.com</a> comparison to help sell your {{$vehicleType}}.<br>
                                                            We're pleased to confirm your current highest valuation:</div>

                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>


                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>


                        <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                        >
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->


                        <div  style="margin:0px auto;max-width:600px;">

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                            >
                                <tbody>
                                <tr>
                                    <td
                                            style="direction:ltr;font-size:0px;padding:20px 0;padding-left:55px;padding-right:55px;text-align:center;"
                                    >
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                            <tr>

                                                <td
                                                        class="" style=""
                                                >

                                                    <table
                                                            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:490px;" width="490"
                                                    >
                                                        <tr>
                                                            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                        <![endif]-->


                                        <div  style="margin:0px auto;border-radius:2px;max-width:490px;">

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;border-radius:2px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td
                                                            style="border:1px solid #e2e2e2;border-bottom:3px solid #f89c20;direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                                                    >
                                                        <!--[if mso | IE]>
                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                            <tr>
                                                                <td
                                                                        class="" width="490px"
                                                                >
                                                        <![endif]-->

                                                        <div
                                                                class="mj-column-per-50 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                                        >

                                                            <table
                                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                                            >

                                                                <tr>
                                                                    <td
                                                                            align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                                    >

                                                                        <div
                                                                                style="font-family:Lato, Roboto, Arial;font-size:16px;font-weight:400;line-height:26px;text-align:left;color:#372e2c;"
                                                                        >{{$vehicle->meta->manufacturer}} {{$vehicle->meta->model}} {{$vehicle->meta->derivative}}</div>

                                                                    </td>
                                                                </tr>

                                                            </table>

                                                        </div>

                                                        <!--[if mso | IE]>
                                                        </td>
                                                        </tr>

                                                        <tr>
                                                            <td
                                                                    class="" width="490px"
                                                            >
                                                        <![endif]-->

                                                        <div
                                                                class="mj-column-per-50 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                                        >

                                                            <table
                                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                                            >

                                                                <tr>
                                                                    <td
                                                                            align="right" class="val-price" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                                    >

                                                                        <div
                                                                                style="font-family:Lato, Roboto, Arial;font-size:18px;font-weight:900;line-height:26px;text-align:right;color:#372e2c;"
                                                                        >{{$valuation->value}}</div>

                                                                    </td>
                                                                </tr>

                                                            </table>

                                                        </div>

                                                        <!--[if mso | IE]>
                                                        </td>
                                                        </tr>

                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>


                                        <!--[if mso | IE]>
                                        </td>
                                        </tr>
                                        </table>

                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>


                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>



                        <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                        >
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->


                        <div  style="margin:0px auto;max-width:600px;">

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                            >
                                <tbody>
                                <tr>
                                    <td
                                            style="direction:ltr;font-size:0px;padding:20px 0;padding-left:30px;padding-right:30px;text-align:center;"
                                    >
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                            <tr>

                                                <td
                                                        class="" style="vertical-align:top;width:540px;"
                                                >
                                        <![endif]-->

                                        <div
                                                class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                        >

                                            <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                            >

                                                <tr>
                                                    <td
                                                            align="left" style="font-size:0px;padding:10px 25px;padding-bottom:20px;word-break:break-word;"
                                                    >

                                                        <div
                                                                style="font-family:Lato, Roboto, Arial;font-size:18px;font-weight:900;line-height:20px;text-align:left;color:#4a4a4a;"
                                                        >Marketplace</div>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td
                                                            align="left" style="font-size:0px;padding:10px 25px;padding-bottom:20px;word-break:break-word;"
                                                    >

                                                        <div
                                                                style="font-family:Lato, Roboto, Arial;font-size:16px;font-weight:400;line-height:26px;text-align:left;color:#4a4a4a;"
                                                        >
                                                            Think your {{$vehicleType}} is worth more? Why not enter more vehicle information through our Premium Marketplace completely free of charge? We can share the news and your additional details to our UK Partner Network to help get you what you need.
                                                        </div>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td
                                                            align="left" vertical-align="middle" class="button-width" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                    >

                                                        <table
                                                                border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:70%;line-height:100%;"
                                                        >
                                                            <tr>
                                                                <td
                                                                        align="center" bgcolor="#f89c20" role="presentation" style="border:none;border-radius:3px;cursor:auto;height:56px;mso-padding-alt:10px 25px;background:#f89c20;" valign="middle"
                                                                >
                                                                    <a
                                                                            href="{{route('showAdditionalInformationForm', $vehicle->id)}}"   style="display:inline-block;background:#f89c20;color:#ffffff;font-family:Lato, Roboto, Arial;font-size:16px;font-weight:900;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;"
                                                                    >
                                                                        Enter more information
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>


                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>


                        <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                        >
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->


                        <div  style="margin:0px auto;max-width:600px;">

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                            >
                                <tbody>
                                <tr>
                                    <td
                                            style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-left:55px;padding-right:0px;text-align:center;"
                                    >
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                            <tr>

                                                <td
                                                        class="" style="vertical-align:top;width:545px;"
                                                >
                                        <![endif]-->

                                        <div
                                                class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                        >

                                            <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                            >

                                                <tr>
                                                    <td
                                                            align="center" style="font-size:0px;padding:10px 25px;padding-right:0px;padding-bottom:0px;padding-left:20%;word-break:break-word;"
                                                    >

                                                        <table
                                                                border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                                        >
                                                            <tbody>
                                                            <tr>
                                                                <td  style="width:415px;">

                                                                    <img
                                                                            height="auto" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/carpeople.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="415"
                                                                    />

                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>


                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>

                        <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="rel-position-outlook" style="width:600px;" width="600"
                        >
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->


                        <div  class="rel-position" style="margin:0px auto;max-width:600px;">

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                            >
                                <tbody>
                                <tr>
                                    <td
                                            style="direction:ltr;font-size:0px;padding:20px 0;padding-left:55px;padding-right:55px;padding-top:0px;text-align:center;"
                                    >
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                            <tr>

                                                <td
                                                        class="" style=""
                                                >

                                                    <table
                                                            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:490px;" width="490"
                                                    >
                                                        <tr>
                                                            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                        <![endif]-->


                                        <div  style="background:#f4f4f4;background-color:#f4f4f4;margin:0px auto;max-width:490px;">

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f4f4f4;background-color:#f4f4f4;width:100%;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td
                                                            style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                                                    >
                                                        <!--[if mso | IE]>
                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                            <tr>
                                                                <td
                                                                        class="" width="490px"
                                                                >
                                                        <![endif]-->

                                                        <div
                                                                class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                                        >

                                                            <table
                                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                                            >

                                                                <tr>
                                                                    <td
                                                                         class="box-padding"   align="center" style="font-size:0px;padding:20px 50px 10px 50px;word-break:break-word;"
                                                                    >

                                                                        <div
                                                                                style="font-family:Lato, Roboto, Arial;font-size:22px;font-weight:400;line-height:32px;text-align:center;color:#000000;"
                                                                        >“I needed a quick and easy way to sell my car. Jamjar provided an excellent service.”</div>

                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td
                                                                              align="right" style="font-size:0px;padding:10px 50px 20px 50px;word-break:break-word;"
                                                                    >

                                                                        <div
                                                                                style="font-family:Lato, Roboto, Arial;font-size:16px;font-weight:400;line-height:20px;text-align:right;color:#000000;"
                                                                        >Dean Chatterton</div>

                                                                    </td>
                                                                </tr>

                                                            </table>

                                                        </div>

                                                        <!--[if mso | IE]>
                                                        </td>
                                                        </tr>

                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>


                                        <!--[if mso | IE]>
                                        </td>
                                        </tr>
                                        </table>

                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>


                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>


                        <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                        >
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->


                        <div  style="margin:0px auto;max-width:600px;">

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                            >
                                <tbody>
                                <tr>
                                    <td
                                            style="direction:ltr;font-size:0px;padding:20px 0;padding-left:30px;padding-right:30px;text-align:center;"
                                    >
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                            <tr>

                                                <td
                                                        class="" style="vertical-align:top;width:540px;"
                                                >
                                        <![endif]-->

                                        <div
                                                class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                        >

                                            <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                            >

                                                <tr>
                                                    <td
                                                            align="left" style="font-size:0px;padding:10px 25px;padding-bottom:20px;word-break:break-word;"
                                                    >

                                                        <div
                                                                style="font-family:Lato, Roboto, Arial;font-size:18px;font-weight:900;line-height:20px;text-align:left;color:#4a4a4a;"
                                                        >About your valuations</div>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td
                                                            align="left" style="font-size:0px;padding:10px 25px;padding-bottom:20px;word-break:break-word;"
                                                    >

                                                        <div
                                                                style="font-family:Lato, Roboto, Arial;font-size:16px;font-weight:400;line-height:26px;text-align:left;color:#4a4a4a;"
                                                        > Selling your car has never been easier! Simply click the button below to view your vehicle's valuations, confirm the deal and initiate contact with the buyer online, making the whole process as smooth as possible.</div>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td
                                                            align="left" vertical-align="middle" class="button-width" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                    >

                                                        <table
                                                                border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:70%;line-height:100%;"
                                                        >
                                                            <tr>
                                                                <td
                                                                        align="center" bgcolor="#f89c20" role="presentation" style="border:none;border-radius:3px;cursor:auto;height:56px;mso-padding-alt:10px 25px;background:#f89c20;" valign="middle"
                                                                >
                                                                    <a
                                                                            href="{{route('showSavedValuation', $valuation->uuid)}}"
                                                                            style="display:inline-block;background:#f89c20;color:#ffffff;font-family:Lato, Roboto, Arial;font-size:16px;font-weight:900;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;"
                                                                    >
                                                                        View your latest valuations
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>


                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>


                        <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                        >
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->


                        <div  style="margin:0px auto;max-width:600px;">

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                            >
                                <tbody>
                                <tr>
                                    <td
                                            style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-left:55px;padding-right:0px;text-align:center;"
                                    >
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                            <tr>

                                                <td
                                                        class="" style="vertical-align:top;width:545px;"
                                                >
                                        <![endif]-->

                                        <div
                                                class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                        >

                                            <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                            >

                                                <tr>
                                                    <td
                                                            align="center" style="font-size:0px;padding:10px 25px;padding-right:0px;padding-bottom:0px;padding-left:20%;word-break:break-word;"
                                                    >

                                                        <table
                                                                border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                                        >
                                                            <tbody>
                                                            <tr>
                                                                <td  style="width:415px;">

                                                                    <img
                                                                            height="auto" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/peopleinsidecar.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="415"
                                                                    />

                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>


                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>


                        <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="rel-position-outlook" style="width:600px;" width="600"
                        >
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->


                        <div  class="rel-position" style="margin:0px auto;max-width:600px;">

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                            >
                                <tbody>
                                <tr>
                                    <td
                                            style="direction:ltr;font-size:0px;padding:20px 0;padding-left:55px;padding-right:55px;padding-top:0px;text-align:center;"
                                    >
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                            <tr>

                                                <td
                                                        class="" style=""
                                                >

                                                    <table
                                                            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:490px;" width="490"
                                                    >
                                                        <tr>
                                                            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                        <![endif]-->


                                        <div  style="background:#f4f4f4;background-color:#f4f4f4;margin:0px auto;max-width:490px;">

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f4f4f4;background-color:#f4f4f4;width:100%;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td
                                                            style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                                                    >
                                                        <!--[if mso | IE]>
                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                            <tr>
                                                                <td
                                                                        class="" width="490px"
                                                                >
                                                        <![endif]-->

                                                        <div
                                                                class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                                        >

                                                            <table
                                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                                            >

                                                                <tr>
                                                                    <td
                                                                            align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                                    >


                                                                        <!--[if mso | IE]>
                                                                        <table
                                                                                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                                        >
                                                                            <tr>

                                                                                <td>
                                                                        <![endif]-->
                                                                        <table
                                                                                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"
                                                                        >

                                                                            <tr

                                                                            >
                                                                                <td  style="padding:4px;">
                                                                                    <table
                                                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f4f4f4;border-radius:3px;width:43px;"
                                                                                    >
                                                                                        <tr>
                                                                                            <td  style="font-size:0;height:43px;vertical-align:middle;width:43px;">
                                                                                                <a  href="https://twitter.com/jamjarcompare" target="_blank">
                                                                                                    <img
                                                                                                            height="43" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/twitter-circle.png" style="border-radius:3px;display:block;" width="43"
                                                                                                    />
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>

                                                                            </tr>

                                                                        </table>
                                                                        <!--[if mso | IE]>
                                                                        </td>

                                                                        <td>
                                                                        <![endif]-->
                                                                        <table
                                                                                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"
                                                                        >

                                                                            <tr

                                                                            >
                                                                                <td  style="padding:4px;">
                                                                                    <table
                                                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f4f4f4;border-radius:3px;width:43px;"
                                                                                    >
                                                                                        <tr>
                                                                                            <td  style="font-size:0;height:43px;vertical-align:middle;width:43px;">
                                                                                                <a  href="https://www.facebook.com/Jamjarcompare/" target="_blank">
                                                                                                    <img
                                                                                                            height="43" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/facebook-circle.png" style="border-radius:3px;display:block;" width="43"
                                                                                                    />
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>

                                                                            </tr>

                                                                        </table>
                                                                        <!--[if mso | IE]>
                                                                        </td>

                                                                        <td>
                                                                        <![endif]-->
                                                                        <table
                                                                                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"
                                                                        >

                                                                            <tr

                                                                            >
                                                                                <td  style="padding:4px;">
                                                                                    <table
                                                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f4f4f4;border-radius:3px;width:43px;"
                                                                                    >
                                                                                        <tr>
                                                                                            <td  style="font-size:0;height:43px;vertical-align:middle;width:43px;">
                                                                                                <a  href="https://www.instagram.com/jamjarcompare/" target="_blank">
                                                                                                    <img
                                                                                                            height="43" src="https://jamjar-prod.s3.eu-west-2.amazonaws.com/images/instagram-circle.png" style="border-radius:3px;display:block;" width="43"
                                                                                                    />
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>

                                                                            </tr>

                                                                        </table>
                                                                        <!--[if mso | IE]>
                                                                        </td>

                                                                        </tr>
                                                                        </table>
                                                                        <![endif]-->


                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td
                                                                            class="box-padding"    align="center" style="font-size:0px;padding:10px 50px 20px 50px;word-break:break-word;"
                                                                    >

                                                                        <div
                                                                                style="font-family:Lato, Roboto, Arial;font-size:16px;font-weight:400;line-height:22px;text-align:center;color:#372e2c;"
                                                                        >Be the first to know about our latest news, offers, and car tips.
                                                                            Like us on Facebook and follow us on Twitter and Instagram.</div>

                                                                    </td>
                                                                </tr>

                                                            </table>

                                                        </div>

                                                        <!--[if mso | IE]>
                                                        </td>
                                                        </tr>

                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>


                                        <!--[if mso | IE]>
                                        </td>
                                        </tr>
                                        </table>

                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>


                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>


                        <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                        >
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->


                        <div  style="margin:0px auto;max-width:600px;">

                            <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                            >
                                <tbody>
                                <tr>
                                    <td
                                            style="direction:ltr;font-size:0px;padding:20px 0;padding-left:30px;padding-right:30px;text-align:center;"
                                    >
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                            <tr>

                                                <td
                                                        class="" style="vertical-align:top;width:540px;"
                                                >
                                        <![endif]-->

                                        <div
                                                class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                        >

                                            <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                                            >

                                                <tr>
                                                    <td
                                                            align="left" style="font-size:0px;padding:10px 25px;padding-bottom:20px;word-break:break-word;"
                                                    >

                                                        <div
                                                                style="font-family:Lato, Roboto, Arial;font-size:18px;font-weight:900;line-height:20px;text-align:left;color:#4a4a4a;"
                                                        >We're here to help</div>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td
                                                            align="left" style="font-size:0px;padding:10px 25px;padding-bottom:20px;word-break:break-word;"
                                                    >

                                                        <div
                                                                style="font-family:Lato, Roboto, Arial;font-size:16px;font-weight:400;line-height:25px;text-align:left;color:#4a4a4a;"
                                                        >If you have any questions, please contact our Customer Care team who will be happy to help.</div>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td
                                                            align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                                    >

                                                        <div
                                                                style="font-family:Lato, Roboto, Arial;font-size:22px;font-weight:400;line-height:20px;text-align:left;color:#f89c20;"
                                                        ><a href="tel:01158709903" style="color:#f89c20;text-decoration:none">0115 870 9903</a></div>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td
                                                            align="left" style="font-size:0px;padding:10px 25px;padding-bottom:20px;word-break:break-word;"
                                                    >

                                                        <div
                                                                style="font-family:Lato, Roboto, Arial;font-size:18px;font-weight:400;line-height:20px;text-align:left;color:#f89c20;"
                                                        ><a href="mailto:info@jamjar.com" style="color:#f89c20;">info@jamjar.com</a></div>

                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>


                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>



@endsection