<!doctype html>
<html>
    <head>
        <style type="text/css">table,td,div,a{box-sizing:border-box;text-decoration:none}img{-ms-interpolation-mode:bicubic;max-width:100%}body{-webkit-font-smoothing:antialiased;font-size:16px;height:100%!important;line-height:1.6em;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:100%!important}table{border-collapse:separate!important;mso-table-lspace:0;mso-table-rspace:0;width:100%}table td{vertical-align:top}p{line-height:1.3}.ExternalClass{width:100%}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%}body{background:#eee}.main-table{width:600px;margin:0 auto}.content-table{width:600px;background:#fff}@media only screen and (max-width:690px){table[class=body] h1,table[class=body] h2,table[class=body] h3,table[class=body] h4{font-weight:600!important}table[class=body] h1{font-size:22px!important}table[class=body] h2{font-size:18px!important}table[class=body] h3{font-size:16px!important}table[class=body] .content,table[class=body] .wrapper{padding:10px!important}table[class=body] .container{padding:0!important;width:100%!important}table[class=body] .btn table,table[class=body] .btn a{width:100%!important}}body{font-family:"Verdana",sans-serif}h1,h2,h3,h4,h5,h6{line-height:1.2;font-weight:400}a{text-decoration:none;font-size:18px}p{color:#6e6e6e;font-size:18px}.button{background:#f89b1f;color:#fff;border:none;border-radius:10px;padding:1rem 1.25rem;display:inline-block;vertical-align:middle}.button--large{font-size:26px;padding:1.5rem 1.75rem}.header{background:#000;text-align:center}.sub-header{background:#f89b1f;text-align:center}.sub-header img{display:inline-block;vertical-align:middle}@media (max-width:400px){.sub-header img{display:block!important;margin:0 auto!important}}.sub-header__title{color:#fff;display:inline-block;vertical-align:middle;margin:0;font-size:1.7rem}@media (max-width:400px){.sub-header__title{font-size:20px!important;width:100%!important;display:block!important}}.sub-header__icon-wrap{width:110px}@media (max-width:400px){.sub-header__icon-wrap{width:auto!important}}.footer{background:#000;text-align:center;color:#6e6e6e}.footer a{color:#6e6e6e;text-decoration:none}.main-content{text-align:center}.text-big{font-weight:600;font-size:30px;display:block;color:#000}.text-exsmall{font-size:14px}.dealer-table{text-align:left}.dealer-table__offer{font-size:30px;color:#000;font-weight:600;margin:0}@media (max-width:400px){.dealer-table__offer{font-size:14px!important}}@media (max-width:400px){.dealer-table .button{width:152px!important}}.works-table img{display:block;margin:0 auto}.address{display:block}.map{max-width:100%}.left-column{width:200px}@media (max-width:400px){.left-column{width:100px!important}}.number-item{width:100px}@media (max-width:400px){.number-item{width:100px!important}}</style>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Set your header title here -->
        <title>JamJar - Thanks for joining</title>
        <!-- fonts -->
    </head>
    <body style="-webkit-font-smoothing: antialiased;font-size: 16px;line-height: 1.6em;margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #eee;font-family: Verdana,sans-serif;height: 100%!important;width: 100%!important;">
        <!-- Table Wrapper -->
        <table class="main-table" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 600px;margin: 0 auto;border-collapse: separate!important;">
            <tbody>
                    <td width="100%" valign="top" align="center" class="main-table__cell" style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                        <!-- Main Content Table -->
                        <table class="content-table" width="650" cellspacing="0" cellpadding="0" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 600px;background: #fff;border-collapse: separate!important;">
                            <tbody>
                                <tr>
                                    <td width="100%" valign="top" align="center" style="box-sizing: border-box;text-decoration: none;vertical-align: top;">

                                        <!-- Start Main  -->
                                        <table width="650" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 100%;border-collapse: separate!important;">
                                            <!-- HEADER -->
                                            <tr style="background-color:#000;">
                                                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                    <table class="header" width="650" valign="middle" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 650px;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;background: #000;text-align: center;border-collapse: separate!important;" ;>
                                                        <!--SPACER--><tr><td height="25" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                        <tr>
                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                <img src="https://www.jamjar.com/assets/images/jamjar-logo-white.png" alt="JamJar" style="-ms-interpolation-mode: bicubic;max-width: 100%;">
                                                            </td>
                                                        </tr>
                                                        <!--SPACER--><tr><td height="25" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- END HEADER -->
                                            <!-- SUB HEADER -->
                                            <tr style="background-color:#f89b1f;">
                                                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                    <table class="sub-header" width="600" valign="middle" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 600px;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;background: #f89b1f;text-align: center;border-collapse: separate!important;" ;>
                                                        <!--SPACER--><tr><td height="35" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                        <tr>
                                                            <td style="vertical-align: middle;box-sizing: border-box;text-decoration: none;">
                                                                <img src="http://s3.eu-west-2.amazonaws.com/jamjar-prod/email/jamjar/icons/happy-face.png" alt="Check Out" style="-ms-interpolation-mode: bicubic;max-width: 100%;display: inline-block;vertical-align: middle;">
                                                                <h1 class="sub-header__title" style="padding-top:20px;line-height: 1.2;font-weight: 400;color: #fff;display: inline-block;vertical-align: middle;margin: 0;font-size: 1.7rem;">You're all set up<br> and good to go.</h1>
                                                            </td>
                                                        </tr>

                                                        <!--SPACER--><tr><td height="25" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- END SUB HEADER -->
                                            <tr>
                                                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                    <table style="width: 600px;margin: 0 auto;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: separate!important;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <!-- table content -->
                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                <table class="main-content" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 100%;text-align: center;border-collapse: separate!important;">
                                                                    <tbody>
                                                                        <!--SPACER--><tr><td height="40" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                                        <tr>
                                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                                <p style="line-height: 1.3;color: #6e6e6e;font-size: 18px;">Thanks for joining Jamjar.com, The UKs leading online car buying comparison site.</p>
                                                                                <p style="line-height: 1.3;color: #6e6e6e;font-size: 18px;">Our process for valuing and selling your car couldn’t be easier.</p>
                                                                            </td>
                                                                        </tr>
                                                                        <!--SPACER--><tr><td height="40" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="background-color:#f7f7f7;">
                                                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                    <table style="width: 600px;margin: 0 auto;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: separate!important;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <!-- table content -->
                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                <table class="main-content" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 100%;text-align: center;border-collapse: separate!important;">
                                                                    <tbody>
                                                                        <!--SPACER--><tr><td height="40" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                                        <tr>
                                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                                <h2 style="color: #f89b1f;font-size: 26px;line-height: 1.2;font-weight: 400;">How it Works</h2>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                                <table class="works-table" width="600" valign="middle" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 600px;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: separate!important;" ;>
                                                                                    <tr>
                                                                                        <td style="vertical-align: top;width: 200px;box-sizing: border-box;text-decoration: none;">
                                                                                            <img src="http://s3.eu-west-2.amazonaws.com/jamjar-prod/email/jamjar/icons/free-reg.png" alt="jamjar" style="-ms-interpolation-mode: bicubic;max-width: 100%;display: block;margin: 0 auto;">
                                                                                        </td>
                                                                                        <td style="vertical-align: top;width: 200px;box-sizing: border-box;text-decoration: none;">
                                                                                            <img src="http://s3.eu-west-2.amazonaws.com/jamjar-prod/email/jamjar/icons/money-looking.png" alt="jamjar" style="-ms-interpolation-mode: bicubic;max-width: 100%;display: block;margin: 0 auto;">
                                                                                        </td>
                                                                                        <td style="vertical-align: top;width: 200px;box-sizing: border-box;text-decoration: none;">
                                                                                            <img src="http://s3.eu-west-2.amazonaws.com/jamjar-prod/email/jamjar/icons/key-calendar.png" alt="jamjar" style="-ms-interpolation-mode: bicubic;max-width: 100%;display: block;margin: 0 auto;">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="background-color:#f7f7f7;">
                                                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                    <table style="width: 600px;margin: 0 auto;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: separate!important;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <!-- table content -->
                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                <table class="main-content" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 100%;text-align: center;border-collapse: separate!important;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                                <table class="works-table" width="600" valign="middle" align="center" cellpadding="10" cellspacing="0" border="0" style="width: 600px;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: separate!important;" ;>
                                                                                    <tr>
                                                                                        <td style="vertical-align: top;width: 200px;box-sizing: border-box;text-decoration: none;">
                                                                                            <p style="margin: 0;font-size: 14px;line-height: 1.3;color: #6e6e6e;">Simply enter your registration number with a few other details.</p>
                                                                                        </td>
                                                                                        <td style="vertical-align: top;width: 200px;box-sizing: border-box;text-decoration: none;">
                                                                                            <p style="margin: 0;font-size: 14px;line-height: 1.3;color: #6e6e6e;">Sit back and watch the best offers roll in from the best used car online car buying websites in the UK.</p>
                                                                                        </td>
                                                                                        <td style="vertical-align: top;width: 200px;box-sizing: border-box;text-decoration: none;">
                                                                                            <p style="margin: 0;font-size: 14px;line-height: 1.3;color: #6e6e6e;">Select the best offer for you, enter your final details and your chosen online car buying service will do the rest.</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <!--SPACER--><tr><td height="40" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                        <tr>
                                                            <td style="text-align: center;box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                <a href="https://comparison.jamjar.com/email/verify/{{$email_token}}" class="button" style="box-sizing: border-box;text-decoration: none;font-size: 18px;background: #f89b1f;color: #fff;border: none;border-radius: 10px;padding: 1rem 1.25rem;display: inline-block;vertical-align: middle;">Compare Your Prices</a>
                                                            </td>
                                                        </tr>
                                                        <!--SPACER--><tr><td height="60" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                    <table style="width: 600px;margin: 0 auto;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: separate!important;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <!-- table content -->
                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                <table class="main-content" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 100%;text-align: center;border-collapse: separate!important;">
                                                                    <tbody>
                                                                        <!--SPACER--><tr><td height="40" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                                        <tr>
                                                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                                                                <p style="line-height: 1.3;color: #6e6e6e;font-size: 18px;">In the meantime, if you need any help selling your vehicle or have any other questions about the process, simply get in touch with us. </p>
                                                                            </td>
                                                                        </tr>
                                                                        <!--SPACER--><tr><td height="40" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            @include('mail.partials.footer')
                                            <!-- END MAIN CONTENT -->
                                        </table>
                                        <!-- End Main  -->
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- End Content Table -->
                    </td>

            </tbody>
        </table>
        <!-- End Main Table -->
    </body>
</html>

{{-- @extends('mail.master')

@section('title')
Please Verify Your Email Address
@endsection

@section('button')
<p><a href="{{'https://jamjar.com' . route('verifyEmail', $email_token, false) }}" class="button button--orange">Click here to verify your email</a></p>
<p><small>Can't click the button? Copy and paste this text into your browser window: {{ 'https://jamjar.com' . route('verifyEmail', $email_token, false) }}</small></p>
@endsection

@section('content')
	<p>Thanks for registering on jamjar.com.</p>
	<p>Before we can activate your account we need to verify your email address.</p>
	<p>Please click the button below to do so.</p>
@endsection
 --}}
