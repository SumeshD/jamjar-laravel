@php
    $matrixPostcode = $sale->valuation->location ? $sale->valuation->location->postcode : $sale->company->postcode;
    $postcode = $sale->delivery_method == 'dropoff' ? $matrixPostcode : $vehicle->owner->postcode;
@endphp

<tr>
    <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
        <table style="width: 600px;margin: 0 auto;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: separate!important;" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <!-- table content -->
                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                    <table class="main-content" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 100%;text-align: center;border-collapse: separate!important;">
                        <tbody>
                            <!--SPACER--><tr><td height="40" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                            <tr>
                                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                                    <img class="map" src="http://maps.google.com/maps/api/staticmap?center={{urlencode($postcode)}}&zoom=14&size=400x400&maptype=roadmap&markers=color:ORANGE|label:A|{{urlencode($postcode)}}&sensor=false&key={{ config('googlemaps.key') }}"  alt="JamJar" style="-ms-interpolation-mode: bicubic;max-width: 100%;">
                                </td>
                            </tr>
                            <!--SPACER--><tr><td height="40" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
