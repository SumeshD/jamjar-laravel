<!-- FOOTER -->
<tr style="background-color:#fff;">
    <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;padding-left: 10px;padding-right: 10px;">
        <table class="footer" width="650" valign="middle" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 650px;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;background: #fff;text-align: center;color: #000000;border-collapse: separate!important;" ;>
            <!--SPACER--><tr><td height="35" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
            <tr>
                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top; text-align: left;">
                    <a href="https://www.jamjar.com" style="box-sizing: border-box;text-decoration: none;font-size: 18px;color: #6e6e6e;"><img src="http://s3.eu-west-2.amazonaws.com/jamjar-prod/email/jamjar/logo.png" alt="jamjar.com"></a>
                </td>
            </tr>
            <tr>
                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top; text-align: left;line-height:18px;">
                    <span style="font-size:10px; color:black;">Copyright © jamjar.com 1997 - {{ date('Y') }}</span>
                </td>
            </tr>
            <tr>
                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top; text-align: left;line-height:18px;">
                    <span style="font-size:10px; color:black;">Registered address: 74-76 Grantham Road, Nottingham, NG12 2HY, United Kingdom</span>
                </td>
            </tr>
            <tr>
                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top; text-align: left;line-height:18px;">
                    <span style="font-size:10px; color:black;">
                        <a style="font-size: 10px; color: blue; text-decoration: underline;" href="https://www.jamjar.com/">Jamjar.com</a> |
                        <a style="font-size: 10px; color: blue; text-decoration: underline;" href="https://www.jamjar.com/terms-conditions/">Terms & Conditions</a> |
                        <a style="font-size: 10px; color: blue; text-decoration: underline;" href="https://www.jamjar.com/privacy/">Privacy Policy</a>
                    </span>
                </td>
            </tr>
            <tr>
                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top; text-align: left;line-height:18px;">
                    <span style="font-size:10px; color:black;">Jamjar.com is a trading name of Grapevine Europe Limited, registered in England and Wales, Companies House registered number 7046823</span>
                </td>
            </tr>
            <!--SPACER--><tr><td height="35" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
        </table>
    </td>
</tr>
<!-- END FOOTER -->