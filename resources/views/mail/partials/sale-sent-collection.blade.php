<tr>
    <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
        <table style="width: 600px;margin: 0 auto;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: separate!important;" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <!-- table content -->
                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                    <table class="main-content" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 100%;text-align: center;border-collapse: separate!important;">
                        <tbody>
                            <tr>
                                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;padding-left: 30px;padding-right: 30px;">
                                    <table class="dealer-table" width="600" valign="middle" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 600px;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;text-align: left;border-collapse: separate!important;" ;>
                                        <tr>
                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;"><p style="margin: 0;font-size: 14px;line-height: 1.3;color: #6e6e6e;">Next Steps</p></td>
                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td>
                                        </tr>
                                        <!--SPACER--><tr><td height="10" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                        <tr>
                                            <td style="width: 120px;">
                                                <div style="border-radius: 50%; width: 36px; height: 36px; padding: 5px; background: #666; border: 2px solid #666; color: #fff; text-align: center; font: 20px Arial, sans-serif; margin-top: 5px;">1</div>
                                            </td>
                                            <td style="vertical-align: top;border-top: 1px solid #c4c4c4;padding: 0.5rem 0;box-sizing: border-box;text-decoration: none;">
                                                <p style="margin: 0;line-height: 1.3;color: #6e6e6e;font-size: 18px;">Please print this email and keep it with you during your collection appointment for your own reference.</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="border-radius: 50%; width: 36px; height: 36px; padding: 5px; background: #666; border: 2px solid #666; color: #fff; text-align: center; font: 20px Arial, sans-serif; margin-top: 5px;">2</div>
                                            </td>
                                            <td style="padding: 0.5rem 0;vertical-align: top;box-sizing: border-box;text-decoration: none;">
                                                <p style="margin: 0;line-height: 1.3;color: #6e6e6e;font-size: 18px;">
                                                    {{$sale->company->name}} will contact you within 3 working hours and will be happy to discuss and confirm your payment details, required paperwork and also arrange your vehicles collection, time's and date's options.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="border-radius: 50%; width: 36px; height: 36px; padding: 5px; background: #666; border: 2px solid #666; color: #fff; text-align: center; font: 20px Arial, sans-serif; margin-top: 5px;">3</div>
                                            </td>
                                            <td style="padding: 0.5rem 0;vertical-align: top;box-sizing: border-box;text-decoration: none;">
                                                <p style="margin: 0;line-height: 1.3;color: #6e6e6e;font-size: 18px;">
                                                    Alternatively you can contact {{$sale->company->name}} directly on {{$sale->company->telephone}}. Please quote Jamjar and your Registration number.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="border-radius: 50%; width: 36px; height: 36px; padding: 5px; background: #666; border: 2px solid #666; color: #fff; text-align: center; font: 20px Arial, sans-serif; margin-top: 5px;">4</div>
                                            </td>
                                            <td style="padding: 0.5rem 0;vertical-align: top;box-sizing: border-box;text-decoration: none;">
                                                <p style="margin: 0;line-height: 1.3;color: #6e6e6e;font-size: 18px;">
                                                    {{$sale->company->name}} currently has the following address as your preferred collection address. If this is no longer your preferred collection address, please discuss this when they contact you.

                                                    <span class="address" style="display: block;">{{$sale->user->profile->address}}</span>
                                                </p>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
