<tr>
    <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
        <table style="width: 600px;margin: 0 auto;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: separate!important;" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <!-- table content -->
                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;">
                    <table class="main-content" style="box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;width: 100%;text-align: center;border-collapse: separate!important;">
                        <tbody>
                            <tr>
                                <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;padding-left: 30px;padding-right: 30px;">
                                    <table class="dealer-table" width="600" valign="middle" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 600px;box-sizing: border-box;text-decoration: none;mso-table-lspace: 0;mso-table-rspace: 0;text-align: left;border-collapse: separate!important;" ;>
                                        <tr>
                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;"><p style="margin: 0;font-size: 14px;line-height: 1.3;color: #6e6e6e;">Next Steps</p></td>
                                            <td style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td>
                                        </tr>
                                        <!--SPACER--><tr><td height="10" style="box-sizing: border-box;text-decoration: none;vertical-align: top;"></td></tr><!--/SPACER-->
                                        <tr>
                                            <td style="vertical-align: top;border-top: 1px solid #c4c4c4;padding: 0.5rem 0;width: 100px;box-sizing: border-box;text-decoration: none;">
                                                <img style="display: inline-block;vertical-align: top;-ms-interpolation-mode: bicubic;max-width: 100%;" src="http://s3.eu-west-2.amazonaws.com/jamjar-prod/email/jamjar/icons/1.png" alt="JamJar">
                                            </td>
                                            <td style="vertical-align: top;border-top: 1px solid #c4c4c4;padding: 0.5rem 0;box-sizing: border-box;text-decoration: none;">
                                                <p style="margin: 0;line-height: 1.3;color: #6e6e6e;font-size: 18px;">Please print this email and keep it with you during your collection appointment for your own reference.</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0.5rem 0;vertical-align: top;width: 100px;box-sizing: border-box;text-decoration: none;padding-top: 14px;">
                                                <img style="display: inline-block;vertical-align: top;-ms-interpolation-mode: bicubic;max-width: 100%;" src="http://s3.eu-west-2.amazonaws.com/jamjar-prod/email/jamjar/icons/2.png" alt="JamJar">
                                            </td>
                                            <td style="padding: 0.5rem 0;vertical-align: top;box-sizing: border-box;text-decoration: none;">
                                                <p style="margin: 0;line-height: 1.3;color: #6e6e6e;font-size: 18px;">
                                                    Please call {{$sale->company->name}} on {{$sale->company->telephone}}, quoting Jamjar and your vehicles registration number and they will be happy to discuss and confirm your payment details, required paperwork and all convenient drop-off time and date options.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0.5rem 0;vertical-align: top;width: 100px;box-sizing: border-box;text-decoration: none;">
                                                <img style="display: inline-block;vertical-align: top;-ms-interpolation-mode: bicubic;max-width: 100%;" src="http://s3.eu-west-2.amazonaws.com/jamjar-prod/email/jamjar/icons/3.png" alt="JamJar">
                                            </td>
                                            <td style="padding: 0.5rem 0;vertical-align: top;box-sizing: border-box;text-decoration: none;">
                                                <p style="margin: 0;line-height: 1.3;color: #6e6e6e;font-size: 18px;">
                                                    Below is your most local {{$sale->company->name}} drop off point:

                                                    <span class="address" style="display: block;">
                                                        @php($location = $sale->valuation->location ?? $sale->company->location->first())
                                                        {!!$location->address!!}
                                                    </span>
                                                </p>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
