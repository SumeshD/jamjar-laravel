<?php $unsubscribeRoute = isset($unsubscribeRoute) ? $unsubscribeRoute : 'notificationUnsubscribe' ?>
@php ($link = Autologin::to($autoLoginUser, route($unsubscribeRoute)))



<tr>
    <td>
        <table style="width: 660px; margin: 0 auto;">
            <tr>
                <td style="vertical-align: middle;border-top: 1px solid #c4c4c4;padding: 0.2rem 0;box-sizing: border-box;text-align: center;">
                    <a href="{{ $link }}" style="color: grey;font-size:0.9rem;text-decoration: none;">Unsubscribe from these emails</a>
                </td>
            </tr>
        </table>
    </td>
</tr>