<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
                window.Jamjar = <?php echo json_encode([
                    'csrfToken' => csrf_token(),
                ]); ?>
        </script>

        <title>Offer Accepted - Sell My Car Fast | Buy My Car Today | Money4YourMotors</title>

        <!-- Styles -->
        <link href="{{ secure_asset('css/vendor.css') }}" rel="stylesheet">
        <link href="{{ secure_asset('css/style.css') }}" rel="stylesheet">
        <style>
            .site-wrapper {
                max-width: 980px;
                background-color: #ffffff;
                margin: 0 auto;
            }
            .inner-wrapper {
                padding-left: 30px;
                padding-right: 30px;
                margin-top: 8px;
                margin-bottom: 0px;
                color: #333333;
                text-decoration: none;
                line-height: 15px;
                border-top: 1px solid #dbdbdb;
                border-bottom: 1px solid #dbdbdb;
                border-radius: 1px;
                box-shadow: 0px 1px 5px rgba(0,0,0,.1);
                font-family: Arial, sans-serif;
            }
            .matrix-content p {
                line-height: 1.7em;
            }
            input[type="submit"] {
                background-color: #95DE58 !important;
                border: 1px solid #1B600F !important;
                color: #306108 !important;
            }
            input[type="submit"]:hover {
                background-color: #5cb811 !important;
                color: #ffffff !important;
            }
            .matrix-footer__col img {
                max-width: 250px;
            }
        </style>
    </head>

    <body style="background-color: #ebebeb;">
        <div class="site-wrapper">
            <div class="inner-wrapper">
                <header class="header header--matrix" style="border-bottom: 0;">
                    <div class="header__container">
                        <div class="header__logo">
                            <a href="https://www.money4yourmotors.com/">
                                <img src="https://www.money4yourmotors.com/images/HomeLogo_2.jpg" alt="M4YM">
                            </a>
                        </div>
                    </div>
                </header>
                
                <div class="blackout"></div>


                <section class="matrix-content" style="background-color: #ffffff;">
                    <div class="inner-wrapper">
                        <div class="matrix-content__box" style="padding:0;">
                            <h1 class="matrix-title">Thank you!</h1>
                            <div class="matrix-content__content">
                                <p>Please contact us to arrange your appointment. </p>

                                <p>Call 0151 666 9980 and quote reference number {{$ref}}</p>

                                <p>Our call centre is open 8:30am - 8:30pm Monday - Friday, 9:00am - 6:00pm Saturday and  10:00am - 6:00pm on Sunday</p>

                                <p>Appointments can booked for any time between 8.30am - 8.30pm Monday - Friday and 8.30am - 6.30pm at weekends</p>

                                <p>Our call centre staff will then confirm your appointment and arrange for one of our friendly buyers to visit you and purchase your vehicle.</p>
                                <p>You choose when and where, it can be at your home or workplace, whichever is easiest for you, any day of the week including Saturday or Sunday.</p>

                                <p>Kind Regards</p>

                                <p>Money4yourMotors.com Ltd</p>

                            </div>
                        </div>
                    </div>
                </section>


                <footer class="matrix-footer">
                    <div class="matrix-footer__container">
                        <div class="matrix-footer__col">
                            <img src="https://www.money4yourmotors.com/images/HomeLogo_2.jpg" alt="M4YM">
                        </div>
                        <div class="matrix-footer__col">
                            <p>For more details read our <a href="https://www.money4yourmotors.com/terms.asp">terms and conditions</a></p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!-- Scripts -->
        <script src="{{ secure_asset('js/vendor.js') }}"></script>
        <script src="{{ secure_asset('js/app.js') }}"></script>
        @yield('scripts')
        
        @include('sweet::alert')
    </body>
</html>
