<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="{{ app()->getLocale() }}">
<head>
@if (!str_contains(Request::url(), $_SERVER['HTTP_HOST'] . '/associate'))
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WKNH5JS');</script>
        <!-- End Google Tag Manager -->
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ secure_asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ secure_asset('images/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ secure_asset('images/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ secure_asset('images/favicons/favicon-16x16.png') }}">


    @yield('styles')
    @stack('styles-after')
    <script type="text/javascript">
        window.cookieconsent_options = {"message":"Jamjar uses cookies to ensure you get the best experience.","dismiss":"Got it!","learnMore":"More info","link":"https://www.jamjar.com/info/cookie-policy","theme":"light-bottom"};
    </script>
    <script>(function(n,t,i,r){var u,f;n[i]=n[i]||{},n[i].initial={accountCode:"CMPNY18790",host:"CMPNY18790.pcapredict.com"},n[i].on=n[i].on||function(){(n[i].onq=n[i].onq||[]).push(arguments)},u=t.createElement("script"),u.async=!0,u.src=r,f=t.getElementsByTagName("script")[0],f.parentNode.insertBefore(u,f)})(window,document,"pca","//CMPNY18790.pcapredict.com/js/sensor.js")</script>
    <script src="{{ secure_asset('vendor/adminlte/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/js/jquery.viewbox.min.js"></script>
</head>
<body class="{{ Route::currentRouteName() == '/' ? 'front-page' : Route::currentRouteName() }} {{ (strpos(Route::getCurrentRoute()->uri(), 'associates') !== false and Route::currentRouteName() != 'becomeAPartner') ? 'partner-website' : '' }}">
@if (!str_contains(Request::url(), $_SERVER['HTTP_HOST'] . '/associate'))
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WKNH5JS"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
@endif
<div id="app">
    @include('newlayouts.header')

    @yield('content')

    @include('newlayouts.footer')
</div>

<!-- Scripts -->
@if (str_contains(Request::url(), $_SERVER['HTTP_HOST'] . '/associate'))
    <script>
        function deleteOffer(offerID){
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this bid",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        document.getElementById('delete-offer-' + offerID).submit();
                    }
                    // else {
                    //     swal("Your offer is safe");
                    // }
                });
        }
        $(document).ready(function(){

            var toggleMenu = false;

            $(document).on('click','.js-trigger-menu', function (e) {
                e.preventDefault();

                toggleMenu = !toggleMenu;

                if (toggleMenu) {
                    $('.blackout').addClass('active');
                    var headerHeight = $('.header').outerHeight(true);
                    $('.mega').css('top', headerHeight + 'px');
                    $('.mega').show();
                    $(this).addClass('active');
                }else{

                    $('.blackout').removeClass('active');
                    $('.mega').hide();
                    $(this).removeClass('active');
                }


                if ($(this).hasClass('active')) {
                    $(this).html('<i class="fa fa-times"></i>');
                } else {
                    $(this).html('<i class="fa fa-bars"></i>');
                }
            });
            //associate
            $('.radio-button').on('click', function() {
                var $this = $(this);

                $('.radio-button.active').removeClass('active').addClass('inactive');
                $this.removeClass('inactive').addClass('active');

                $('#marketing').val($this.attr('data-value'));
            });

            // $('#manual-address').on('click', function() {
            //     console.log('MANUAL CLICK');
            //     $('.manual-hide').toggleClass('active');
            // });

            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the button that opens the modal
            var btn = document.getElementById("openModal");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on the button, open the modal

            if (btn) {
                btn.onclick = function() {
                    modal.style.display = "block";
                }
            }

            // When the user clicks on <span> (x), close the modal
            if (span) {
                span.onclick = function() {
                    modal.style.display = "none";
                }
            }


            $(document).on('click', function (e) {

                console.log(e.target);
                if($('.js-trigger-menu').hasClass('active')){

                    if ($(e.target).closest(".blackout").length !== 0) {
                        $(".mega").hide();
                        $('.blackout').removeClass('active');
                        $('.js-trigger-menu').removeClass('active');
                        $('.js-trigger-menu').html('<i class="fa fa-bars"></i>');
                    }
                }


            });


            $('.jj-sticky-header').css('position', 'relative');

            $('.sidebar__toggle a').on('click', function(e) {
                $(".sidebar__item").slideToggle();
            });
            // function setHeaderHeight(){
            //     var headerHeight = $('.header').outerHeight(true);
            //     //set spacer same height as header
            //     $('.spacer').css('padding-bottom', headerHeight + 'px');
            // }

            // setHeaderHeight();

            // $(window).resize(function(event) {
            //     setHeaderHeight();
            // });

        });

        var valueUpdateEvent = null;

        function calculateFee(element) {
            var value = element.val();
            clearTimeout(valueUpdateEvent);
            var vehicleId = $("#vehicle_id").val();
            value = value.replace(/,+/g, '');

            if (parseInt(value) != value) {
                return;
            }
            var $fee = $('#fee-value');
            var oldFee = $fee.text();
            var paidFee = $fee.attr('data-old-fee');

            var feeCalculatorUrl = element.attr('data-fee-calculator-url') ?
                element.attr('data-fee-calculator-url') :
                '{{ route('leadFeeByValue') }}';

            valueUpdateEvent = setTimeout(function () {
                $fee.html('<img src="/images/spinners/circle.png" />');
                $.ajax({
                    type: "GET",
                    dataType: 'json',
                    accepts: {
                        'custom': 'application/json, text/plain, */*'
                    },
                    contentType: 'application/json',
                    url: feeCalculatorUrl,
                    data: {
                        'value': value,
                        'vehicle_id': vehicleId
                    },
                    success: function(data) {
                        var newFee = paidFee ? (parseFloat(data.fee) - parseInt(paidFee) / 100) : parseFloat(data.fee);
                        if (newFee >= 0) {
                            $fee.text('£' + newFee.toFixed(2));
                        } else {
                            $fee.text('£0.00');
                        }
                    },
                    error: function () {
                        $fee.text(oldFee);
                    }
                });
            }, 700);
        }

        $(function() {
            $('#value').on('input', function(event) {
                calculateFee($(this));
            });

            $('.update-fee').on('input', function(event) {
                calculateFee($(this));
            });
        });

    </script>
@endif

<script src="{{ secure_asset('js/app.js') }}"></script>
<script src="{{ secure_asset('js/helpers.js?version=cd1b5fcd-691d-4b4f-b968-a9f6acd8c74c') }}"></script>
<script src="//unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    $(function() {
        var $tooltipsters = $('.tooltip-subject');

        if ($tooltipsters.length > 0) {
            $('.tooltip-subject').tooltipster({
                theme: 'tooltipster-borderless'
            });
        }
    });
</script>
@yield('scripts')
<!-- start -->
@stack('scripts-after')
<!-- stop -->

@include('sweet::alert')

@include('chat.facebook')

<script src="{{ secure_asset('js/chosen.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ secure_asset('js/app.ce5cbd10.js') }}" type="text/javascript"></script>
</body>
</html>
