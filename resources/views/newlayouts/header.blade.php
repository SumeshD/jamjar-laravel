<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<header>
	<div class="header__content">
		<a id="jamjarLogo" class="header__content__logo" href="@if(env('APP_ENV') == 'local' || env('APP_ENV') == 'staging')/@else https://www.jamjar.com/ @endif" title="jamjar.com"></a><!-- Logo linking to main page -->
		<nav id="mobileMenu">
			<ul>
				<li>
					<a href="https://www.jamjar.com" target="_blank" title="Free valuations">Free valuations</a>
				</li>
				<li>
					<a href="https://www.jamjar.com/how-it-works/" target="_blank" title="How it works">How it works</a>
				</li>
				<li class="header__content__nav__link--folder">
					<a href="#" title="More from Jamjar">More from Jamjar</a>
					<ul>
						<li>
							<a href="https://www.jamjar.com/about-us/" target="_blank" title="About" class="header__content__nav__sub-link">About</a>
						</li>
						<li>
							<a href="https://comparison.jamjar.com/associates/apply" target="_blank" title="Become an associate" class="active header__content__nav__sub-link">Become an associate</a>
						</li>
					</ul>
				</li>
					@if (Auth::guest())
						<li>
							<a id="jamjarSignin" href="/login" title="Sign in">Sign in</a><!-- Sign in element -->
						</li>
					@else
					<li class="header__content__nav__link--folder" >
						<a href="#" >@if (auth()->user()->isPartner()) {{ auth()->user()->company->name }} @else {{ auth()->user()->name }} @endif</a>
						<ul>
							@if (auth()->user()->isAdmin())
								<li>
									<a href="{{ route('adminDashboard') }}" class="header__content__nav__sub-link">
										Administration
									</a>
								</li>
							@endif
							@if ((auth()->user()->isPartner()) || (auth()->user()->isAdmin()))
								<li>
									<a href="{{ route('partnerDashboard') }}" class="header__content__nav__sub-link">
										Associate Area
									</a>
								</li>
							@else
								<li>
									<a href="{{ route('dashboard') }}" class="header__content__nav__sub-link">
										Saved Vehicles
									</a>
								</li>
							@endif
							<li>
								<a href="{{ route('account') }}" class="header__content__nav__sub-link">
									My Account
								</a>
							</li>
							<li>
								<a href="{{ route('logout') }}"  onclick="event.preventDefault();document.getElementById('logout-form').submit();">
									Log Out
								</a>
							</li>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</ul>
					@endif
				</li>
			</ul>
		</nav><!-- Nav bar with the menu links -->
		<div id="mobileMenuButton" class="header__content__menu-button"></div><!-- Mobile menu button -->
	</div>
</header> <!-- Page header -->

@if (!str_contains(Request::url(), $_SERVER['HTTP_HOST'] . '/associate'))
<div class="spacer"></div>
@endif
<div class="blackout"></div>
