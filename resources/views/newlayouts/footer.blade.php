@if (env('APP_ENV') !== 'testing')

    <footer>
        <section class="footer__main mob-hide">
            <div class="footer__content">
                <article>
                    <a id="jamjarFooterLogo" class="footer__content__logo" href="#" title="jamjar.com"></a>
                    <p class="smaller">
                        Jamjar.com, 74-76 Grantham Road, Nottingham, NG12 2HY, United Kingdom.
                    </p>
                    <p class="smaller">
                        Jamjar.com is a trading name of Grapevine Europe Limited, registered in England and Wales, Companies House registered number 7046823
                    </p>
                    <p class="smaller">
                        *Your personal information is not required by JamJar to get your free vehicle valuations. You will need to provide personal information to create a JamJar account so we can connect you with our buying partners.
                    </p>
                </article>
                <article>
                    <p class="small">
                        <strong>Valuations</strong>
                    </p>
                    <a class="text-black" href="https://www.jamjar.com/sell-my-car/" target="_blank" title="Sell my car"><small>Sell my car</small></a><br>
                    <a class="text-black" href="https://www.jamjar.com/scrap-my-car/" target="_blank" title="Scrap my car"><small>Scrap my car</small></a><br>
                    <a class="text-black" href="https://www.jamjar.com/value-my-car/" target="_blank" title="Value my car"><small>Value my car</small></a><br>
                    <a class="text-black" href="https://www.jamjar.com/value-my-van/" target="_blank" title="Sell my van"><small>Sell my van</small></a><br>
                    <a class="text-black" href="https://www.jamjar.com/sell-my-van/" target="_blank" title="Value my van"><small>Value my van</small></a>
                </article>
                <article>
                    <p class="small">
                        <strong>More about Jamjar</strong>
                    </p>
                    <a class="text-black" href="https://www.jamjar.com/how-it-works/" target="_blank" title="How it works"><small>How it works</small></a><br>
                    <a class="text-black" href="https://www.jamjar.com/about-us/" target="_blank" title="About Jamjar"><small>About Jamjar</small></a><br>
                    <a class="text-black" href="https://www.jamjar.com/faqs/" target="_blank" title="FAQs"><small>FAQs</small></a><br>
                    <a class="text-black" href="https://comparison.jamjar.com/associates/apply" target="_blank" title="Become an associate"><small>Become an associate</small></a>
                </article>
                <article>
                    <p class="small">
                        <strong>Your Account</strong>
                    </p>
                    <a class="text-black" href="/login" title="Sing in"><small>Sign in</small></a><br>
                    <a class="text-black" href="/login" title="My valuations"><small>My valuations</small></a><br>
                    <a class="text-black" href="/login" title="My account"><small>My account</small></a><br>
                </article>
                <article>
                    <p class="small">
                        <strong>Connect</strong>
                    </p>
                    <a class="text-black" href="https://www.jamjar.com/contact-us" target="_blank" title="Contact us"><small>Contact us</small></a><br>                   
                    <a class="text-black" href="https://www.jamjar.com/blog/" target="_blank" title="Blog"><small>Blog</small></a><br>
                </article>
            </div>
        </section>
        <section class="footer__copyright">
            <div class="footer__content">
                <article class="footer__legals">
                    <a href="https://www.jamjar.com/associate-privacy-policy/" target="_blank" title="Privacy"><small>Privacy</small></a>
                    <a href="/terms" title="Terms"><small>Terms</small></a>
                    <a href="/cookies" title="Cookies"><small>Cookies</small></a>
                </article>
                <small>
                    Copyright &#169; jamjar.com 1997 - 2022
                </small>
            </div>
        </section><!--- Last ribbon at the very bottom -->
    </footer><!-- Footer content -->
{{--@if (!str_contains(Request::url(), $_SERVER['HTTP_HOST'] . '/associate'))--}}
{{--  <footer class="footer">--}}
{{--      <div class="footer__container">--}}
{{--      	<div class="footer__logo">--}}
{{--            <a href="https://www.jamjar.com/"><img src="{{ secure_asset('images/logo.png') }}" alt="jamjar.com"></a>--}}
{{--            <div class="footer__social">--}}
{{--                <a href="https://www.facebook.com/Jamjarcompare/" rel="me nofollow"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>--}}
{{--                <a href="https://www.instagram.com/jamjarcompare/" rel="me nofollow"><i class="fa fa-instagram" aria-hidden="true"></i></a>--}}
{{--                <a href="https://twitter.com/jamjarcompare" rel="me nofollow"><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
{{--                <a href="https://www.youtube.com/channel/UC2x57dWX3lwN1PrgwBI_Zog" rel="me nofollow"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--      	<div class="footer__navigation">--}}
{{--      		<ul class="menu">--}}

{{--      			<li class="menu__item">--}}
{{--      				<a href="https://www.jamjar.com/blog/" class="menu__link">Blog</a>--}}
{{--      			</li>--}}

{{--                <li class="menu__item">--}}
{{--                    <a href="https://www.jamjar.com/contact-us/" class="menu__link">Contact Us</a>--}}
{{--                </li>--}}

{{--      			<li class="menu__item">--}}
{{--      				<a href="https://www.jamjar.com/terms-conditions/" class="menu__link">Terms &amp; Conditions</a>--}}
{{--      			</li>--}}

{{--                <li class="menu__item">--}}
{{--                    <a href="https://www.jamjar.com/privacy/" class="menu__link">Privacy Policy</a>--}}
{{--                </li>--}}

{{--                <li class="menu__item">--}}
{{--                    <a href="https://www.jamjar.com/cookie-policy" class="menu__link">Cookie Policy</a>--}}
{{--                </li>--}}

{{--      		</ul>--}}
{{--      	</div>--}}
{{--      	<div class="footer__copyright" style="text-align: left;">--}}
{{--          	<p>Copyright &copy; jamjar.com 1997 - {{ date('Y') }}</p>--}}
{{--            <p>Registered address: Jamjar.com, 74-76 Grantham Road, Nottingham, NG12 2HY, United Kingdom</p>--}}
{{--            <p>Jamjar.com is a trading name of Grapevine Europe Limited, registered in England and Wales, Companies House registered number 7046823</p>--}}
{{--    </div>--}}
{{--  </div>--}}
{{--</footer>--}}
{{--@else--}}
{{--  <footer class="footer footer__partner_matrix">--}}
{{--    <div class="footer__container">--}}
{{--      <div class="footer__logo">--}}
{{--        <img src="{{ secure_asset('images/logo.png') }}" alt="jamjar.com">--}}
{{--      </div>--}}

{{--      <div class="footer__copyright" style="text-align: left;">--}}
{{--        <p>Copyright &copy; jamjar.com 1997 - {{ date('Y') }}</p>--}}
{{--        <p>Registered address: Jamjar.com, 74-76 Grantham Road, Nottingham, NG12 2HY, United Kingdom</p>--}}
{{--        <p>Jamjar.com is a trading name of Grapevine Europe Limited, registered in England and Wales, Companies House registered number 7046823</p>--}}
{{--    </div>--}}
{{--  </div>--}}
{{--</footer>--}}
{{--@endif--}}
@endif
@push('scripts-after')
<script type="text/javascript">
    (function(windowAlias, documentAlias, trackerName) {
        if (!windowAlias[trackerName]) {
            windowAlias.GlobalAdalyserNamespace = windowAlias.GlobalAdalyserNamespace
                    || [];
            windowAlias.GlobalAdalyserNamespace.push(trackerName);
            windowAlias[trackerName] = function() {
                (windowAlias[trackerName].q = windowAlias[trackerName].q || []).push(arguments)
            };
            windowAlias[trackerName].q = windowAlias[trackerName].q || [];
            var nel = documentAlias.createElement("script"),
                fel = documentAlias.getElementsByTagName("script")[0];
            nel.async = 1;
            nel.src = "//c5.adalyser.com/adalyser.js?cid=jamjar";
            fel.parentNode.insertBefore(nel, fel)
        }
    }(window, document, "adalyserTracker"));

    window.adalyserTracker("create", {
        campaignCookieTimeout: 15552000,
        conversionCookieTimeout: 604800,
        clientId: "jamjar",
        trafficSourceInternalReferrers: [
            "^(.*\\.)?jamjar.com$"
        ]
    });
    window.adalyserTracker("trackSession", "lce1", {});
</script>

<script type="text/javascript">

  if(window.location.href.indexOf("/your-car/") > -1) {
    console.log('track: about stage');
    window.adalyserTracker("trackEvent", "lce2", {},true);
  }

</script>
@endpush
