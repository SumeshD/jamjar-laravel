<?php
$errorArray = [];
if($errors) {
    $errorArray = array_unique($errors->all());
}

$selected = 0;
if(old('keys-count') == 1 or $vehicle->getKeysCount() == 1) {
    $selected = 1;
} elseif (old('keys-count') == 2 or $vehicle->getKeysCount() == 2) {
    $selected = 2;
} elseif (old('keys-count') == 3 or $vehicle->getKeysCount() == 3) {
    $selected = 3;
}
$postCode = isset($postCode) ? $postCode : '';
?>
@extends('newlayouts.app')

@section('content')

    <main role="main" style="display: none">
        <section class="page-section">
            <div class="page-section__content page-section__content--valuation-step3 page-section__content--valuation">
                <form id="showcaseForm" action="{{ route('processShowcaseForm', [$vehicle->id]) }}" method="POST" enctype="multipart/form-data">
                    @if ($errors->any())
                        <div class="page-section__content page-section__content--valuation" style="display: inline">
                            <div class="alert alert--danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    {{ csrf_field() }}
                    <div class="form-section">
                        <div class="form-section__form">
                            <div class="form-section__form">
                                <div style="float: left; margin-bottom: 24px; font-size: 2.3rem; font-weight: 500; line-height: 1.25em;">
                                    Showcase your vehicle
                                </div>
                                <div style="font-size: 1.3rem; margin-bottom: 24px; float: left">
                                    When you showcase your vehicle it will be visible to another 912 of our buying partners who can offer you a bespoke valuation based on your location and the service history, specification and images.
                                </div>
                            </div>
                            <div class="form-section__form">
                                <label for="pictures">
                                    RECOMMENDED<br>
                                    We recommend you take some great photos
                                    <div class="photo-booth">
                                        @for($i=0; $i<8; $i++)
                                            @if(isset($images[$i]))
                                                <label class="showcase-img active-label" for="image{{$i+1}}" style="background-image: url({{route('thumbnail', [$images[$i]->getImageId(), $images[$i]->getUploadedImage()->originalName()])}});">
                                                    <input type="file" accept="image/*" name="image[{{$i}}]" id="image{{$i+1}}" image-id="{{$images[$i]->id}}">
                                                </label>
                                            @else
                                                <label class="showcase-img" for="image{{$i+1}}">
                                                    <input type="file" accept="image/*" name="image[{{$i}}]" id="image{{$i+1}}">
                                                </label>
                                            @endif
                                        @endfor
                                    </div>
                                    <a href="#" title="Guide">Unsure? Take a look at our guide</a>
                                </label>
                                <label for="nrKeys">
                                    How many keys do you have for the vehicle
                                    <select name="nrKeys" id="nrKeys">
                                        <option disabled="" selected="">Please select</option>
                                        <option {{ ($selected == 1) ? 'selected' : '' }} value="1">1</option>
                                        <option {{ ($selected == 2 || $selected == 0) ? 'selected' : '' }} value="2">2</option>
                                        <option {{ ($selected == 3) ? 'selected' : '' }} value="3">3+</option>
                                    </select>
                                </label>
                                <label for="serviceInfo">
                                    Additional service information
                                    <textarea name="serviceInfo" id="serviceInfo" rows="6">{{$vehicle->service_history_information}}</textarea>
                                </label>
                                <label for="specsInfo">
                                    Additional specification information
                                    <textarea name="specsInfo" id="specsInfo" rows="6">{{$vehicle->additional_specification_information}}</textarea>
                                </label>
                                <p class="small">
                                    We will inform you of any improved valuations via text message and email.
                                </p>
                            </div>
                            <button id="submitShowcase" type="submit">Add to Jamjar showroom</button>
                            <a href="{{route("showSavedValuation",$vehicle->valuations->first()->uuid)}}" title="Valuations"><small>Skip adding more details and go back to valuations</small></a>
                        </div>
                        <aside>
                            <article style="margin-bottom: 24px; padding: 25px 20px 20px; background-color: #F8F8F8; margin-top: 70%">
                                <p class="small">
                                    <strong>Check out our guide for taking great photo&#39;s of your vehicle.</strong>
                                </p>
                                <p class="small">
                                    By taking some great quality photos of your car, you could help to convert more interest and higher offers for your vehicle.
                                </p>
                                <p class="small">
                                    <a href="#" title="Guide"><small>Take a look at our guide</small></a>
                                </p>
                            </article>
                            <article>
                                <img src="/images/ux/kb_avatar_placeholder.c7013877.png" alt="Mr. H" width="72">
                                <p>&#8220;Incredibly easy process&#8221;</p>
                                <p class="small">Best price across all selling sites. I will definately use JamJar again and reccommended to all of my friends and family.</p>
                                <p class="small">Mr H • Nottingham</p>
                            </article>
                            <img src="/images/ux/kb_trusted_site.274ddd7b.png" alt="Trusted site" width="264">
                        </aside>
                    </div>

                </form>
            </div>
        </section>
        <section class="page-section mob-hide">
            <div class="page-section__content page-section__content--centered">
                <h3>
                    As featured in
                </h3>
                <section class="page-section__content__media-logos">
                    <div class="page-section__content__media-logos__carousel-tape">
                        <img src="/images/ux/kb_media_logo_1.e77e5338.png" alt="The Guardian" width="154">
                        <img src="/images/ux/kb_media_logo_2.d4f98b37.png" alt="BBC" width="169">
                        <img src="/images/ux/kb_media_logo_3.12be4287.png" alt="Top Gear" width="224">
                        <img src="/images/ux/kb_media_logo_4.c8c95f5f.png" alt="The Telegraph" width="297">
                    </div>
                </section>
                <h3>
                    Sell any car fast
                </h3>
                <section class="page-section__content__auto-logos">
                    <div class="page-section__content__auto-logos__carousel-tape">
                        <img src="/images/ux/kb_auto_logo_1.e7ab6737.png" alt="BMW" width="120">
                        <img src="/images/ux/kb_auto_logo_2.c1b4defc.png" alt="Audi" width="120">
                        <img src="/images/ux/kb_auto_logo_3.0cb5b59c.png" alt="Mercedes" width="120">
                        <img src="/images/ux/kb_auto_logo_4.21c602f3.png" alt="Ford" width="120">
                        <img src="/images/ux/kb_auto_logo_5.f20e53f2.png" alt="Volkswagen" width="120">
                    </div>
                </section>
            </div>
        </section><!-- Media features -->
        <section class="page-section page-section--shaded mob-hide">
            <div class="page-section__content page-section__content--video-hero">
                <h3>
                    The original car buying comparison service
                </h3>
                <div class="page-section__content__video-hero paused">
                    <article class="video-hero__badge">
                        <p><strong>Watch the latest TV ad to see how it works</strong></p>
                    </article>
                    <video width="556" height="313" poster="/images/ux/kb_video_placeholder.ce2fac56.png" onclick="this.paused ? this.play() : this.pause();" playsinline="">
                        <source src="/images/ux/kb_ad_1.06f2fe74.mp4" type="video/mp4">
                        <source src="/images/ux/kb_ad_1.8abb1b5f.ogv" type="video/ogg">
                    </video>
                </div>
                <article>
                    <p><strong>Hundreds of UK car buyers</strong></p>
                    <p class="small">
                        See offers from the hundreds of expert car buying businesses that work with Jamjar.
                    </p>
                </article>
                <article>
                    <p><strong>Fully transparent, always</strong></p>
                    <p class="small">
                        Get all the details you need right here to make the best decision for your vehicle.
                    </p>
                </article>
                <article>
                    <p><strong>Cars, vans and scrap</strong></p>
                    <p class="small">
                        We&#39;ll help you find the right deal, whether it&#39;s a car, a van or destined for the scrapyard.
                    </p>
                </article>
                <article>
                    <a href="#" title="More about Jamjar">More about Jamjar</a>
                </article>
            </div>
        </section><!-- Media features -->
    </main><!-- Main page content -->
    @push('scripts-after')
        <script>
            $(document).ready(function() {
                $('input[type=file]').on('change', function(event){
                    $(this).parent().removeClass('active');
                    if(!$(this).hasClass('active-label') && !$(this).parent().hasClass('spinner')) {
                        $(this).parent().addClass('spinner')
                        const formData = new FormData();
                        formData.append("file", this.files[0]);
                        console.log(formData);
                            axios.post('{{route('addVehicleImage', $vehicle->id)}}', formData, {
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            }).then(({data}) => {
                                console.log(data);
                                $(this).parent().removeClass("spinner");
                                $(this).parent().addClass("active-label");
                                $(this).attr('image-id',data.data.vehicleImage.id);
                            });
                        }
                })

                $('.showcase-img').on('click', function(event) {
                    if($(this).hasClass('active-label')) {
                        event.preventDefault();
                        var fileInput = $(this).children("input");
                        fileInput.val('');
                        $(this).removeClass('active-label');
                        $(this).removeAttr('style');
                        console.log($(this).children("input").attr('image-id'));
                        axios.post('/api/images/vehicle-image/remove/'+fileInput.attr('image-id'),
                            {_token: '{{ csrf_field() }}'
                        }).then(({data}) => {

                        });
                    }
                });

            });

        </script>
    @endpush
