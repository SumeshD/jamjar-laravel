@extends('layouts.app')

@section('content')

@if ($errors->any())
<div class="jamjar__container--small">
    <div class="alert alert--danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<section class="jamjar__container">
	<div class="box">
		<h1 class="box__title box__title--mob-hide">Your Vehicle</h1>
		<div class="box__content">
			<div style="font-weight: bold; font-size: 18px; margin-bottom: 20px">Improve your vehicles offers.</div>
			@include('matrix.valuation-drafts.additional-vehicle-details.images')
		</div>

		<form class="form" method="POST" action="{{ route('processAdditionalInformationForm', [$vehicle->id]) }}" id="edit-vehicle-form">


			@include('matrix.valuation-drafts.additional-vehicle-details.key-count')
			<div class="form__group form__group--half form__group--improve-offer @if ($errors->has('asking-price-in-pounds')) form--errors @endif">
				<label for="asking-price-in-pounds" class="form__label"></label>
				{{--				<input min="1" data-fee-calculator-url="{{ route('matrixCalculateFeeForSeller', [$vehicle->id]) }}" data-type="separator-format" name="asking-price-in-pounds" type="text" class="form__input update-fee" value="{{ old('asking-price-in-pounds') ?? $vehicle->getAskingPriceInPence() / 100 }}">--}}
			</div>

			@include('matrix.valuation-drafts.additional-vehicle-details.service-history-information')
			@include('matrix.valuation-drafts.additional-vehicle-details.additional-specification-information')

			<div class="form__group form__group--half form__group--improve-offer">
				<label for="additional-specification-information" class="form__label"></label>
			</div>

			<div class="form__group form__group--half form__group--improve-offer">
				<input style="width: 100%;" id="submit-form" type="submit" class="button button--green-bg" value="ADD INFORMATION">
			</div>
			{{csrf_field()}}
		</form>
	</div>
</section>

@push('scripts-after')
	<script>
	</script>
@endpush

@endsection
