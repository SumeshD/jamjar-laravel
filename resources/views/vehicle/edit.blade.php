<?php $postcode = isset($postcode) ? $postcode : ''; ?>
@extends('newlayouts.app')

@section('content')

	<main role="main">
		<section class="page-section page-section--shaded">
			<div class="page-section__content page-section__content--valuation-progress">
				<table>
					<tr>
						<td>
							<img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}" width="120">
						</td>
						<td class="padding-left--20">
							<h3>{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</h3>
							<p>{{$vehicle->numberplate}}</p>
							<a href="#" id="show-details-button" hidden title="Show details"><small>Show details</small></a><a href="#" id="hide-details-button" title="Hide details"><small>Hide details</small></a>
						</td>
					</tr>
				</table>
				<table style="margin-left:50px; margin-top: 20px; font-size: 10pt" class="car-details">
					<tr>
						<td>Derivative:</td>
						<td>{{$vehicle->meta->derivative}}</td>
					</tr>
					<tr>
						<td>Plate Year:</td>
						<td>{{$vehicle->meta->plate_year}}</td>
					</tr>
					<tr>
						<td>Registration:</td>
						<td>{{$vehicle->numberplate}}</td>
					</tr>
					<tr>
						<td>Mileage:</td>
						<td>Unknown</td>
					</tr>

				</table>
				<table style="margin-left:50px; margin-top: 20px; font-size: 10pt" class="car-details">
					<tr>
						<td>Service History:</td>
						<td>Unknown</td>
					</tr>
					<tr>
						<td>MOT:</td>
						<td>Unknown</td>
					</tr>
					<tr>
						<td>Colour:</td>
						<td>{{$vehicle->meta->colour->title}}</td>
					</tr>
					<tr>
						<td>Write Off:</td>
						<td>Unknown</td>
					</tr>
					<tr>
						<td>Non Runner:</td>
						<td>Unknown</td>
					</tr>

				</table>
				<div class="progress-wrapper margin-right--15">
					<div class="progress-wrapper__progress-point progress-wrapper__progress-point--active">
						<span>Vehicle</span>
						1
					</div>
					<div class="progress-wrapper__progress-waypoint"></div>
					<div class="progress-wrapper__progress-point">
						<span></span>
						2
					</div>
					<div class="progress-wrapper__progress-waypoint"></div>
					<div class="progress-wrapper__progress-point">
						<span></span>
						3
					</div>
					<div class="progress-wrapper__progress-waypoint"></div>
					<div class="progress-wrapper__progress-point">
						<span></span>
						4
					</div>
				</div>
			</div>
		</section>
		<section class="page-section">
			<div class="page-section__content page-section__content--valuation">
				<form style="width: 100%" action="{{ route('vehicleEdit', $vehicle->id) }}" id="vehicle-form" method="POST">
					@if ($errors->any())
						<div class="page-section__content page-section__content--valuation" style="display: inline">
							<div class="alert alert--danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					@endif
					{{csrf_field()}}
					<input type="hidden" name="previous-owners" value="{{ $vehicle->meta->number_of_owners }}">
					<h4>
						Edit Your Vehicle
					</h4>
					<label for="mileage">
						Mileage
						<input min="1000" type="text" name="mileage" id="mileage" value="@if(Session::has('vehicle-details')){{  trim(Session::get('vehicle-details.mileage','')) }}@else{{ old('mileage') ?? request()->mileage }}@endif">
					</label>
					@if (!auth()->user() || empty($postcode))
						<label for="postcode">
							Collection postcode
							<input type="text" name="postcode" id="postcode" value="@if(Session::has('vehicle-details')){{  trim(Session::get('vehicle-details.postcode','')) }}@else{{ old('postcode') ?? request()->postcode }}@endif">
						</label>
					@else
						<input name="postcode" type="hidden" id="postcode" value="{{ auth()->user()->profile->postcode }}">
					@endif
					<label for="service-history">
						Service history
						<select name="service-history" id="service-history">
							<option disabled="" selected="">Please select</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.service-history') === 'FullFranchise') selected
									@else
									@if (old('service-history') == 'FullFranchise') selected @endif
									@if (request()->service_history == 'FullFranchise') selected @endif
									@endif
									value="FullFranchise"
							>
								Full Franchise History
							</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.service-history') === 'PartFranchise') selected
									@else
									@if (old('service-history') == 'PartFranchise') selected @endif
									@if (request()->service_history == 'PartFranchise') selected @endif
									@endif
									value="PartFranchise"
							>
								Part Franchise History
							</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.service-history') === 'PartHistory') selected
									@else
									@if (old('service-history') == 'PartHistory') selected @endif
									@if (request()->service_history == 'PartHistory') selected @endif
									@endif
									value="PartHistory"
							>
								Part History
							</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.service-history') === 'MinimalHistory') selected
									@else
									@if (old('service-history') == 'MinimalHistory') selected @endif
									@if (request()->service_history == 'MinimalHistory') selected @endif
									@endif
									value="MinimalHistory"
							>
								Minimal History
							</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.service-history') === 'None') selected
									@else
									@if (old('service-history') == 'None') selected @endif
									@if (request()->service_history == 'None') selected @endif
									@endif
									value="None"
							>
								None
							</option>
						</select>
					</label>
					<label for="mot">
						MOT
						<select name="mot" id="mot">
							<option disabled="" selected="">Please select</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.mot') === 'SixPlus') selected
									@else
									@if (old('mot') == 'SixPlus') selected @endif
									@if (request()->mot == 'SixPlus') selected @endif
									@endif
									value="SixPlus"
							>
								6 Months+
							</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.mot') === 'ThreetoSix') selected
									@else
									@if (old('mot') == 'ThreetoSix') selected @endif
									@if (request()->mot == 'ThreetoSix') selected @endif
									@endif
									value="ThreetoSix"
							>
								3-6 Months
							</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.mot') === 'OneToThree') selected
									@else
									@if (old('mot') == 'OneToThree') selected @endif
									@if (request()->mot == 'OneToThree') selected @endif
									@endif
									value="OneToThree"
							>
								1-3 Months
							</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.mot') === 'LessThanOne') selected
									@else
									@if (old('mot') == 'LessThanOne') selected @endif
									@if (request()->mot == 'LessThanOne') selected @endif
									@endif
									value="LessThanOne"
							>
								Less Than 1 Month
							</option>
							<option
									@if (Session::has('vehicle-details') and Session::get('vehicle-details.mot') === 'Expired') selected
									@else
									@if (old('mot') == 'Expired') selected @endif
									@if (request()->mot == 'Expired') selected @endif
									@endif
									value="Expired"
							>
								Expired
							</option>
						</select>
					</label>
					<label for="car-colour">
						Car colour
						<select name="car-colour" id="car-colour">
							<option value="" disabled></option>
							@foreach ($colors as $color)
								@if ($vehicle->meta->color == $color->id)
									<option value="{{ $color->id }}" selected>{{ $color->title }}</option>
								@else
									<option value="{{ $color->id }}">{{ $color->title }}</option>
								@endif
							@endforeach
							<option value="other">My colour isn't listed</option>
						</select>
					</label>
					<fieldset>
						<p class="label">Has your vehicle ever been subject to an insurance write-off?</p>
						<input type="radio" id="writeoffYes" class="toggle-writeoff" name="write-off" value="1" @if(old('write-off') == 1)) checked @endif>
						<label for="writeoffYes">Yes</label>
						<input type="radio" id="writeoffNo" class="toggle-writeoff" name="write-off" value="0" @if(old('write-off') == 0)) checked @endif>
						<label for="writeoffNo">No</label>
						<select name="write-off-category" id="write-off-category" @if(old('write-off') == 0) hidden @endif class="form__input">
							<option disabled>Please Select</option>
							<option value="none" @if (!old('write-off-category')) selected @endif>Select Write-Off Category</option>
							<option value="N" @if (old('write-off-category') == 'N') selected @endif>Cat N / Cat D (Can be repaired following non-structural damage)</option>
							<option value="S" @if (old('write-off-category') == 'S') selected @endif>Cat S / Cat C (Can be repaired following structural damage)</option>
						</select>
					</fieldset>
					<fieldset>
						<p class="label">Is your vehicle a non-runner?</p>
						<input type="radio" id="nonRunnerYes" name="non-runner" value="1" @if((int)old('non-runner') === 1) checked @endif>
						<label for="nonRunnerYes">Yes</label>
						<input type="radio" id="nonRunnerNo" name="non-runner" value="0" @if((int)old('non-runner') === 0) checked @endisset>
						<label for="nonRunnerNo">No</label>
						<div class="form__group form__group--defects"  @if((int) old('non-runner') === 0) hidden @endisset>
							<br>
							<p class="label">Please select all mechanical defects which best describe why your vehicle is a non-runner</p>

							@php
								$selectedDefects = [];
                                if (old('non-runner-reason')) {
                                    foreach (old('non-runner-reason') as $reasonId => $flag) {
                                        if ($flag == 'on') {
                                            $selectedDefects[] = $reasonId;
                                        }
                                    }
                                } elseif (request()->defects) {
                                    foreach ($vehicle->meta->non_runner_reason as $selectedDefect) {
                                        $selectedDefects[] = $selectedDefect->id;
                                    }
                                }
                                $columnNo = 1;
							@endphp
							<table style="width: 100%">
								@foreach ($defects as $defect)
									@if($columnNo == 1)
										<tr>
											@endif
											<td style="width: 50%;">
												<div class="form__checkbox">
													<label for="defects[{{$defect->id}}]">
														<input type="checkbox" style="-moz-appearance: checkbox; -webkit-appearance: checkbox; cursor:pointer; vertical-align: top; width:10%;" id="defects[{{$defect->id}}]" name="non-runner-reason[{{$defect->id}}]"
															   @isset($selectedDefects)
															   @if (in_array($defect->id, $selectedDefects))
															   checked
																@endif
																@endif
														>
														<span>{{$defect->title}}</span>
													</label>
												</div>
											</td>
											@if($columnNo == 2)
										</tr>
										@php
											$columnNo = 0;
										@endphp
										@endif
										@php
											$columnNo++;
										@endphp
										@endforeach
										@if($columnNo != 0)
										</tr>
									@endif
							</table>
						</div>
					</fieldset>
					<input type="hidden" name="owner_id" value="{{ request()->owner  ?? 0 }}">
					<button type="submit">Update Vehicle</button>
				</form>
				<aside>
					<article>
						<p class="small">
							<strong>Assumptions about your vehicle</strong>
						</p>
						<p class="small">
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since&#8230;
						</p>
					</article>
					<img src="/images/ux/kb_trusted_site.274ddd7b.png" alt="Trusted site" width="264">
					<article>
						<img src="/images/ux/kb_avatar_placeholder.c7013877.png" alt="Mr. H" width="72">
						<p>&#8220;Incredibly easy process&#8221;</p>
						<p class="small">Best price across all selling sites. I will definately use JamJar again and reccommended to all of my friends and family.</p>
						<p class="small">Mr H • Nottingham</p>
					</article>
				</aside>
			</div>
		</section>
		<section class="page-section mob-hide">
			<div class="page-section__content page-section__content--centered">
				<h3>
					As featured in
				</h3>
				<section class="page-section__content__media-logos">
					<div class="page-section__content__media-logos__carousel-tape">
						<img src="/images/ux/kb_media_logo_1.e77e5338.png" alt="The Guardian" width="154">
						<img src="/images/ux/kb_media_logo_2.d4f98b37.png" alt="BBC" width="169">
						<img src="/images/ux/kb_media_logo_3.12be4287.png" alt="Top Gear" width="224">
						<img src="/images/ux/kb_media_logo_4.c8c95f5f.png" alt="The Telegraph" width="297">
					</div>
				</section>
				<h3>
					Sell any car fast
				</h3>
				<section class="page-section__content__auto-logos">
					<div class="page-section__content__auto-logos__carousel-tape">
						<img src="/images/ux/kb_auto_logo_1.e7ab6737.png" alt="BMW" width="120">
						<img src="/images/ux/kb_auto_logo_2.c1b4defc.png" alt="Audi" width="120">
						<img src="/images/ux/kb_auto_logo_3.0cb5b59c.png" alt="Mercedes" width="120">
						<img src="/images/ux/kb_auto_logo_4.21c602f3.png" alt="Ford" width="120">
						<img src="/images/ux/kb_auto_logo_5.f20e53f2.png" alt="Volkswagen" width="120">
					</div>
				</section>
			</div>
		</section><!-- Media features -->
		<section class="page-section page-section--shaded mob-hide">
			<div class="page-section__content page-section__content--video-hero">
				<h3>
					The original car buying comparison service
				</h3>
				<div class="page-section__content__video-hero paused">
					<article class="video-hero__badge">
						<p><strong>Watch the latest TV ad to see how it works</strong></p>
					</article>
					<video width="556" height="313" poster="/images/ux/kb_video_placeholder.ce2fac56.png" onclick="this.paused ? this.play() : this.pause();" playsinline="">
						<source src="/images/ux/kb_ad_1.06f2fe74.mp4" type="video/mp4">
						<source src="/images/ux/kb_ad_1.8abb1b5f.ogv" type="video/ogg">
					</video>
				</div>
				<article>
					<p><strong>Hundreds of UK car buyers</strong></p>
					<p class="small">
						See offers from the hundreds of expert car buying businesses that work with Jamjar.
					</p>
				</article>
				<article>
					<p><strong>Fully transparent, always</strong></p>
					<p class="small">
						Get all the details you need right here to make the best decision for your vehicle.
					</p>
				</article>
				<article>
					<p><strong>Cars, vans and scrap</strong></p>
					<p class="small">
						We&#39;ll help you find the right deal, whether it&#39;s a car, a van or destined for the scrapyard.
					</p>
				</article>
				<article>
					<a href="#" title="More about Jamjar">More about Jamjar</a>
				</article>
			</div>
		</section><!-- Media features -->
	</main><!-- Main page content -->

	{{--<section class="jamjar__container--small">
        <div class="box">
            <h1 class="box__title box__title--mob-hide">Your Car</h1>
            <div class="box__content">
                <div class="box__logo" style="background-image: url({{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png);">
                     <img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}">
                </div>
                <h2 class="box__car-title">{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</h2>
                <ul>
                    <li>Derivative:</li>
                    <li>{{$vehicle->meta->derivative}}</li>
                    <li>Plate:</li>
                    <li>{{$vehicle->meta->plate_year}}</li>
                    <li>Registration:</li>
                    <li>{{$vehicle->numberplate}}</li>
                </ul>
            </div>
            <h1 class="box__title box__title--mob-hide">About Your Car</h1>
            <div class="box__content">
                <form class="form" method="POST" action="{{ route('storeVehicleMeta', $vehicle) }}" id="vehicle-form">
                    {{csrf_field()}}
                    <input type="hidden" name="previous-owners" value="{{ $vehicle->meta->number_of_owners }}">
                    <div class="form__group form__group--half @if ($errors->has('mileage')) form--errors @endif">
                        <label for="mileage" class="form__label">Mileage</label>
                        <input min="1000" data-type="separator-format" name="mileage" type="text" id="mileage" class="form__input" value="{{ old('mileage') ?? request()->mileage }}">
                    </div>

                    @if (!auth()->user() || empty($postcode))
                        <div class="form__group form__group--half @if ($errors->has('postcode')) form--errors @endif">
                            <label for="postcode" class="form__label">Collection Postcode</label>
                            <input name="postcode" type="text" id="postcode" class="form__input" value="{{ old('postcode') ?? request()->postcode }}">
                        </div>
                    @else
                        <input name="postcode" type="hidden" id="postcode" class="form__input" value="{{ auth()->user()->profile->postcode }}">
                    @endif

                    <div class="form__group form__group--half @if ($errors->has('service-history')) form--errors @endif">
                        <label for="service-history" class="form__label">Service History</label>
                        <select name="service-history" id="service-history" class="form__input">
                                <option selected disabled>Please Select</option>
                                <option
                                    @if (old('service-history') == 'FullFranchise') selected @endif
                                    @if (request()->service_history == 'FullFranchise') selected @endif
                                    value="FullFranchise"
                                >
                                    Full Franchise History
                                </option>
                                <option
                                    @if (old('service-history') == 'PartFranchise') selected @endif
                                    @if (request()->service_history == 'PartFranchise') selected @endif
                                    value="PartFranchise"
                                >
                                    Part Franchise History
                                </option>
                                <option
                                    @if (old('service-history') == 'PartHistory') selected @endif
                                    @if (request()->service_history == 'PartHistory') selected @endif
                                    value="PartHistory"
                                >
                                    Part History
                                </option>
                                <option
                                    @if (old('service-history') == 'MinimalHistory') selected @endif
                                    @if (request()->service_history == 'MinimalHistory') selected @endif
                                    value="MinimalHistory"
                                >
                                    Minimal History
                                </option>
                                <option
                                    @if (old('service-history') == 'None') selected @endif
                                    @if (request()->service_history == 'None') selected @endif
                                    value="None"
                                >
                                    None
                                </option>
                        </select>
                    </div>

                    <div class="form__group form__group--half @if ($errors->has('mot')) form--errors @endif">
                        <label for="mot" class="form__label">MOT</label>
                        <select name="mot" id="mot" class="form__input">
                            <option disabled selected>Please Select</option>
                            <option
                                @if (old('mot') == 'SixPlus') selected @endif
                                @if (request()->mot == 'SixPlus') selected @endif
                                value="SixPlus"
                            >
                                6 Months+
                            </option>
                            <option
                                @if (old('mot') == 'ThreetoSix') selected @endif
                                @if (request()->mot == 'ThreetoSix') selected @endif
                                value="ThreetoSix"
                            >
                                3-6 Months
                            </option>
                            <option
                                @if (old('mot') == 'OneToThree') selected @endif
                                @if (request()->mot == 'OneToThree') selected @endif
                                value="OneToThree"
                            >
                                1-3 Months
                            </option>
                            <option
                                @if (old('mot') == 'LessThanOne') selected @endif
                                @if (request()->mot == 'LessThanOne') selected @endif
                                value="LessThanOne"
                            >
                                Less Than 1 Month
                            </option>
                            <option
                                @if (old('mot') == 'Expired') selected @endif
                                @if (request()->mot == 'Expired') selected @endif
                                value="Expired"
                             >
                                Expired
                            </option>
                        </select>
                    </div>

                    <div class="form__group form__group--half @if ($errors->has('car-colour')) form--errors @endif">
                        <label for="car-colour" class="form__label">Car Colour</label>
                        <select name="car-colour" id="car-colour" class="form__input">
                            <option value="" disabled></option>
                            @foreach ($colors as $color)
                                @if ($vehicle->meta->color == $color->id)
                                <option value="{{ $color->id }}" selected>{{ $color->title }}</option>
                                @else
                                <option value="{{ $color->id }}">{{ $color->title }}</option>
                                @endif
                            @endforeach
                            <option value="other">My colour isn't listed</option>
                        </select>
                    </div>

                    <div class="form__group @if ($errors->has('write-off')) alert alert--danger @endif">
                        <div class="form__radio">
                            <label for="writeoff">Has your vehicle ever been subject to an insurance write-off?</label>
                            <div class="form__radioinput">
                                <input type="radio" name="write-off" id="writeoff" class="writeoff-no" value="0" @if(old('write-off') == 0)) checked @endif> <span data-id="no" class="js-toggle-writeoff">No</span>
                                <input type="radio" name="write-off" id="writeoff" class="writeoff-yes" value="1" @if(old('write-off') == 1)) checked @endif> <span data-id="yes" class="js-toggle-writeoff">Yes</span>
                            </div>
                        </div>
                    </div>

                    <div class="form__group form__group--writeoff @if(old('write-off') == 1) u-show @else u-hide @endif">
                        <div class="form__group form__group @if ($errors->has('write-off-category')) form--errors @endif">
                            <br />
                            <select name="write-off-category" id="write-off-category" class="form__input">
                                <option disabled>Please Select</option>
                                <option value="none" @if (!old('write-off-category')) selected @endif>Select Write-Off Category</option>
                                <option value="N" @if (old('write-off-category') == 'N') selected @endif>Cat N / Cat D (Can be repaired following non-structural damage)</option>
                                <option value="S" @if (old('write-off-category') == 'S') selected @endif>Cat S / Cat C (Can be repaired following structural damage)</option>
                            </select>
                            <div class="divider"></div>
                        </div>
                    </div>

                    <div class="form__group  @if ($errors->has('non-runner')) alert alert--danger @endif">
                        <div class="form__radio">
                            <label for="non-runner">Is your vehicle a non-runner?</label>
                            <div class="form__radioinput">
                                <input type="radio" name="non-runner" id="nonrunner" class="nonrunner-no" value="0" @if((int)old('non-runner') === 0) checked @endif> <span data-id="no" class="js-toggle-nonrunner">No</span>
                                <input type="radio" name="non-runner" id="nonrunner" class="nonrunner-yes" value="1" @if((int)old('non-runner') === 1) checked @endisset> <span data-id="yes" class="js-toggle-nonrunner">Yes</span>
                            </div>
                        </div>
                    </div>

                    <div class="form__group form__group--defects @if((int) old('nonrunner') === 1) u-show @else u-hide @endisset">
                        <p>Please select all mechanical defects which best describe why your vehicle is a non-runner</p>

                        @php
                            $selectedDefects = [];
                            if (old('defects')) {
                                foreach (old('defects') as $reasonId => $flag) {
                                    if ($flag == 'on') {
                                        $selectedDefects[] = $reasonId;
                                    }
                                }
                            } elseif (request()->defects) {
                                foreach ($vehicle->meta->non_runner_reason as $selectedDefect) {
                                    $selectedDefects[] = $selectedDefect->id;
                                }
                            }
                        @endphp
                        @foreach ($defects as $defect)
                            <div class="form__checkbox">
                                <label for="defects[{{$defect->id}}]">
                                    <input type="checkbox" id="defects[{{$defect->id}}]" name="non-runner-reason[{{$defect->id}}]"
                                    @isset($selectedDefects)
                                        @if (in_array($defect->id, $selectedDefects))
                                        checked
                                        @endif
                                    @endif
                                    >
                                    <span>{{$defect->title}}</span>
                                </label>
                            </div>
                        @endforeach
                    </div>

                    <input type="hidden" name="owner_id" value="{{ request()->owner  ?? 0 }}">

                    <div class="clearfix"></div>

                    <div style="text-align: right; margin-top: 30px;">
                        <input type="submit" value="See valuations" class="button button--green-bg">
                    </div>
                </form>
            </div>
        </div>
    </section>--}}

	<div class="loader" style="display: none;">
		<div class="loader__box">
			<div class="loader__content">
				<i class="fa fa-car"></i>
				<h3>Just a moment, we're collecting your offers</h3>
			</div>
		</div>
	</div>

	@push('scripts-after')
		<script>

			$('#vehicle-form').on('submit', function(e) {
				$('.loader').show();
				console.log('track: reach valuations');
				window.adalyserTracker("trackEvent", "lce3", {},true);
			});


			$(function(){
				$('[name=mot]').on('change', function() {
					var $select = $(this);
					if ($select.val() == 'Expired') {
						swal('No Valid MOT', 'Your vehicle\'s valuations could be affected by this. A valid MOT certificate may significantly increase your vehicle\'s valuations.', 'warning', {
							buttons: {'button': {text: 'Close', className: 'button--orange-bg'}},
						});
						$('.swal-footer').css('text-align', 'center');
					}
				});
			});

			$("#writeoffYes").on("click", function(event){
				$("#write-off-category").show();
			});

			$("#writeoffNo").on("click", function(event){
				$("#write-off-category").hide();
			});

			$("#nonRunnerYes").on("click", function(event){
				$(".form__group--defects").show();
			});

			$("#nonRunnerNo").on("click", function(event){
				$(".form__group--defects").hide();
			});

			$("#show-details-button").on("click", function(event) {
				$(".car-details").show();
				$(".progress-wrapper").addClass("margin-right--15");
				$("#show-details-button").hide();
				$("#hide-details-button").show();
			});

			$("#hide-details-button").on("click", function(event) {
				$(".car-details").hide();
				$(".progress-wrapper").removeClass("margin-right--15");
				$("#hide-details-button").hide();
				$("#show-details-button").show();
			});

		</script>
	@endpush

@endsection
