		<div class="mega">
			<div class="mega__container">
				<div class="mega__col">
					<p>Enter your registration for a FREE online comparison</p>

					<form class="mega__plate js-cta-plate-form" action="{{ route('/') }}" method="POST">
						{{ csrf_field() }}
						<input type="text" name="vrm" id="vrm" placeholder="Enter Reg"  maxlength="8" required>
						<button type="submit"><i class="fa fa-angle-right"></i></button>
					</form>
				</div>
				<div class="mega__col">
                    <ul>
                        <li>
                            <a href="https://www.jamjar.com/sell-my-car/">Sell My Car</a>
                        </li>
                        <li>
                            <a href="https://www.jamjar.com/scrap-my-car/">Scrap My Car</a>
                        </li>
                        <li>
                            <a href="https://www.jamjar.com/sell-my-van/">Sell My Van</a>
                        </li>
                        <li>
                            <a href="https://www.jamjar.com/how-it-works/">How it Works</a>
                        </li>
                        <li>
                            <a href="https://www.jamjar.com/faqs/">FAQs</a>
                        </li>
                        <li>
                            <a href="https://www.jamjar.com/about/">About Us</a>
                        </li>
                        <li>
                            <a href="https://www.jamjar.com/blog/">Blog</a>
                        </li>
                        <li>
                            <a href="https://comparison.jamjar.com/associates/apply">Become an Associate</a>
                        </li>
                        <li>
                            <a href="https://www.jamjar.com/contact-us/">Contact Us</a>
                        </li>
                    </ul>
				</div>
			</div>
		</div>
