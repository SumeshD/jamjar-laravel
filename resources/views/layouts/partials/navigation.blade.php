                <nav class="header__navigation">
                    <ul class="menu">
 						@if (Auth::guest())
                            <li class="menu__item">
                            	<a href="{{ route('login') }}" class="menu__link">
                            		<i class="fa fa-user-circle-o"></i> <span>Login</span>
                            	</a>
                            </li>
                        @else
                            <li class="menu__item">
                                @if (auth()->user()->isPartner())
								<a href="{{ route('partnerDashboard') }}" class="menu__link">
									<i class="fa fa-user-circle-o"></i> <span>{{ auth()->user()->company->name }}</span>
								</a>
                                @else
                                <a href="{{ route('account') }}" class="menu__link">
                                    <i class="fa fa-user-circle-o"></i> <span>{{ auth()->user()->name }}</span>
                                </a>
                                @endif
	                            <ul class="sub-menu">
                                    @if ((auth()->user()->isPartner()) || (auth()->user()->isAdmin()))
                                    <li class="sub-menu__item">
                                        <a href="{{ route('partnerDashboard') }}" class="sub-menu__link">
                                            Associate Area
                                        </a>
                                    </li>
                                    @else
                                    <li class="sub-menu__item">
                                        <a href="{{ route('dashboard') }}" class="sub-menu__link">
                                            Saved Valuations
                                        </a>
                                    </li>
                                    @endif
                                    <li class="sub-menu__item">
                                        <a href="{{ route('account') }}" class="sub-menu__link">
                                            My Account
                                        </a>
                                    </li>
	                            	<li class="sub-menu__item">
	                            		<a href="{{ route('logout') }}" class="sub-menu__link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
	                            			Log Out
	                            		</a>
	                            	</li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
								</ul>
                            </li>
                            @if (auth()->user()->isAdmin())
                            <li class="menu__item">
                                <a href="{{ route('adminDashboard') }}" class="menu__link">
                                    <i class="fa fa-cogs"></i> <span>Administration</span>
                                </a>
                            </li>
                            @endif
                        @endif
                    </ul>
                </nav>
