<header class="header jj-sticky-header">
	<div class="header__container">
		<!-- Menu Trigger -->
    	<div class="header__hamburger">
    		<a style="cursor:pointer" class="js-trigger-menu">
    			<i class="fa fa-bars"></i>
    		</a>
    	</div>
    	<!-- / Menu Trigger -->
    	
    	<!-- Logo -->
        <div class="header__logo">
            <a href="https://www.jamjar.com/">
                <img src="{{ secure_asset('images/Jamjarcom.png') }}" alt="jamjar.com">
            </a>
        </div>
        <!-- / Logo -->

		<!-- Navigation -->
		@include('layouts.partials.navigation')
		<!-- / Navigation -->
    </div>
</header>
<!-- Mega Menu -->
@include('layouts.partials.mega-menu')
<!-- / Mega Menu -->

@if (!str_contains(Request::url(), $_SERVER['HTTP_HOST'] . '/associate'))
<div class="spacer"></div>
@endif
<div class="blackout"></div>
