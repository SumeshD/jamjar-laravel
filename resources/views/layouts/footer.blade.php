@if (env('APP_ENV') !== 'testing')
@if (!str_contains(Request::url(), $_SERVER['HTTP_HOST'] . '/associate'))
  <footer class="footer">
      <div class="footer__container">
      	<div class="footer__logo">
            <a href="https://www.jamjar.com/"><img src="{{ secure_asset('images/logo.png') }}" alt="jamjar.com"></a>
            <div class="footer__social">
                <a href="https://www.facebook.com/Jamjarcompare/" rel="me nofollow"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                <a href="https://www.instagram.com/jamjarcompare/" rel="me nofollow"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="https://twitter.com/jamjarcompare" rel="me nofollow"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="https://www.youtube.com/channel/UC2x57dWX3lwN1PrgwBI_Zog" rel="me nofollow"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
            </div>
        </div>
      	<div class="footer__navigation">
      		<ul class="menu">

      			<li class="menu__item">
      				<a href="https://www.jamjar.com/blog/" class="menu__link">Blog</a>
      			</li>

                <li class="menu__item">
                    <a href="https://www.jamjar.com/contact-us/" class="menu__link">Contact Us</a>
                </li>

      			<li class="menu__item">
      				<a href="https://www.jamjar.com/terms-conditions/" class="menu__link">Terms &amp; Conditions</a>
      			</li>

                <li class="menu__item">
                    <a href="https://www.jamjar.com/privacy/" class="menu__link">Privacy Policy</a>
                </li>

                <li class="menu__item">
                    <a href="https://www.jamjar.com/cookie-policy" class="menu__link">Cookie Policy</a>
                </li>

      		</ul>
      	</div>
      	<div class="footer__copyright" style="text-align: left;">
          	<p>Copyright &copy; jamjar.com 1997 - {{ date('Y') }}</p>
            <p>Registered address: Jamjar.com, 74-76 Grantham Road, Nottingham, NG12 2HY, United Kingdom</p>
            <p>Jamjar.com is a trading name of Grapevine Europe Limited, registered in England and Wales, Companies House registered number 7046823</p>
    </div>
  </div>
</footer>
@else
  <footer class="footer footer__partner_matrix">
    <div class="footer__container">
      <div class="footer__logo">
        <img src="{{ secure_asset('images/logo.png') }}" alt="jamjar.com">
      </div>

      <div class="footer__copyright" style="text-align: left;">
        <p>Copyright &copy; jamjar.com 1997 - {{ date('Y') }}</p>
        <p>Registered address: Jamjar.com, 74-76 Grantham Road, Nottingham, NG12 2HY, United Kingdom</p>
        <p>Jamjar.com is a trading name of Grapevine Europe Limited, registered in England and Wales, Companies House registered number 7046823</p>
    </div>
  </div>
</footer>
@endif
@endif
@push('scripts-after')
<script type="text/javascript">
    (function(windowAlias, documentAlias, trackerName) {
        if (!windowAlias[trackerName]) {
            windowAlias.GlobalAdalyserNamespace = windowAlias.GlobalAdalyserNamespace
                    || [];
            windowAlias.GlobalAdalyserNamespace.push(trackerName);
            windowAlias[trackerName] = function() {
                (windowAlias[trackerName].q = windowAlias[trackerName].q || []).push(arguments)
            };
            windowAlias[trackerName].q = windowAlias[trackerName].q || [];
            var nel = documentAlias.createElement("script"),
                fel = documentAlias.getElementsByTagName("script")[0];
            nel.async = 1;
            nel.src = "//c5.adalyser.com/adalyser.js?cid=jamjar";
            fel.parentNode.insertBefore(nel, fel)
        }
    }(window, document, "adalyserTracker"));

    window.adalyserTracker("create", {
        campaignCookieTimeout: 15552000,
        conversionCookieTimeout: 604800,
        clientId: "jamjar",
        trafficSourceInternalReferrers: [
            "^(.*\\.)?jamjar.com$"
        ]
    });
    window.adalyserTracker("trackSession", "lce1", {});
</script>

<script type="text/javascript">

  if(window.location.href.indexOf("/your-car/") > -1) {
    console.log('track: about stage');
    window.adalyserTracker("trackEvent", "lce2", {},true);
  }

</script>
@endpush
