@extends('adminlte::page')

@section('content_header')
    <div class="pull-right">

    </div>
    <h1>Key Performance Indicator Data</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Key Performance Indicator Data</h3>
    </div>
    <div class="box-body">

        <div class="row">
            <form action="{{route('kpiEvaluation')}}" method="GET" class="form form-horizontal" role="form">
                <div class="form-group col-md-3">
                    <label for="from" class="col-sm-3 control-label">Date From</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" id="from" name="from" value="{{request()->get('from')}}">
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="to" class="col-sm-3 control-label">Date To</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" id="to" name="to" value="{{request()->get('to')}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </form>


        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>API Partner</th>
                        <th>Total Features in Valuations</th>
                        <th>Total Click-throughs</th>
                        <th>Total Completed Sales</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companies as $company)
                    <tr>
                        @if ($company->valuation)
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->total }}
                        {{-- <br>
                            @foreach ($companiesPos as $positions)
                                @if($positions->name == $company->name)
                                {{"Rank " . $positions->position . ": " . $positions->total}}<br>
                                @endif
                            @endforeach 
                        --}}
                        </td>
                        <td>{{ $company->clicks}}</td>
                        <td>{{ $company->purchased}}</td>
                        @endif
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
    </div>
</div>

@endsection
