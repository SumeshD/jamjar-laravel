@extends('adminlte::page')

@section('content_header')
    <div class="pull-right">
        <form class="form-inline" role="form">
            <div class="form-group">
                <label class="sr-only" for="vrm">VRM</label>
                <input type="text" value="{{ Request::input('numberPlate') }}" class="form-control" name="numberPlate" id="vrm" placeholder="Search for VRM's">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <h1>Look-Ups</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="completed-sales-filtering-header">
        <h3 class="box-title">Look-Ups</h3>
        {{ $vehicles->appends(request()->query())->links('vendor.pagination.small-default') }}
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Date Created</th>
                        <th>VRM</th>
                        <th>Partner</th>
                        <th>Vehicle</th>
                        <th>Vehicle Type</th>
                        <th>Drafts Count</th>
                        <th>Valuations Count</th>
                        <th>Highest Value</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vehicles as $vehicle)
                        <?php /** @var \JamJar\Vehicle $vehicle */ ?>
                        <tr>
                            <td>{{ $vehicle->created_at->format('Y/m/d H:i:s') }}</td>
                            <td>{{ $vehicle->numberplate }}</td>
                            <td>
                                @if (!$vehicle->owner)
                                    No vehicle owner
                                @elseif (!$vehicle->owner->user)
                                    No user
                                @elseif (!$vehicle->owner->user->company)
                                    No company
                                @else
                                    {{ $vehicle->owner->user->company->name }}
                                @endif
                            </td>
                            <td>{{ $vehicle->meta->formatted_name }}</td>
                            <td>{{ $vehicle->type }}</td>
                            <td>{{ count($vehicle->getDrafts()) }}</td>
                            <td>{{ count($vehicle->getValuations()) }}</td>
                            <td>£{{ number_format($vehicle->getCurrentValueInPence() / 100) }}</td>
                            <td>{{ $vehicle->getSaleStatus() }}</td>
                        </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        {{ $vehicles->appends(request()->query())->links('vendor.pagination.small-default') }}
    </div>
</div>

@endsection
