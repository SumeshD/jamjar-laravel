@extends('adminlte::page')

@section('content_header')
	<div class="pull-right">
		<a href="{{ route('adminUserAdd') }}" class="btn btn-primary">
			<i class="fa fa-plus"></i> Add User
		</a>
	</div>

    <h1>All Users</h1>
    <br>
    <form action="{{route('adminUsers')}}" method="GET" class="form-inline" role="form">
        <div class="form-group">
            <label class="sr-only" for="vrm">VRM</label>
            <input type="text" value="{{ Request::Input('search') }}" class="form-control" name="search" id="search" placeholder="email or name">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">All Users</h3>
        <div class="box-tools">
			{{ $users->appends(request()->query())->links('vendor.pagination.small-default') }}
		</div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Full Name</th>
                        <th>Email Address</th>
                        <th>Verified Email</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
	                @foreach ($users as $user)
                    <tr class="@if ($user->role->name == 'Partner') success @elseif ($user->role->name == 'Administrator') warning @endif">
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->verified == 0 ? 'No' : 'Yes' }}</td>
                        <td>{{ $user->role->name == 'Partner' ? 'Associate' : $user->role->name}}</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-warning btn-small" data-toggle="tooltip" title="Log in as {{$user->name}}" type="button" href="{{ route('adminUserLogin', $user) }}">
                                    <i class="fa fa-key"></i>
                                </a>
                                <a class="btn btn-primary btn-small" type="button" href="{{ route('adminUserShow', $user) }}">
                                    <i class="fa fa-eye"></i>
                                </a> 
                                <a class="btn btn-success btn-small" type="button" href="{{ route('adminUserEdit', $user) }}">
                                	<i class="fa fa-pencil"></i>
                                </a> 
                                @if ($user->id !== auth()->user()->id)
                                    <a class="btn btn-danger btn-small" type="button" onclick="event.preventDefault(); confirmAction(function() { document.getElementById('delete-user-{{$user->id}}').submit(); });">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <form action="{{ route('adminUserDelete', $user) }}" method="POST" id="delete-user-{{$user->id}}">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                    </form>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        {{ $users->appends(request()->query())->links('vendor.pagination.small-default') }}
    </div>
</div>
@endsection
