@extends('adminlte::page')

@section('content_header')
    <h1>
        {{ $user->name }} 
        @if ($user->isPartner())
            <a href="{{ route('adminPartnerEdit', $user) }}" class="label label-success pull-right" style="margin: 0 5px;">Associate</a>
        @endif 
        @if ($user->isAdmin())
            <span class="label label-danger pull-right" style="margin: 0 5px;">System Administrator</span>
        @endif        
        @if ($user->isStaff())
            <span class="label label-warning pull-right" style="margin: 0 5px;">System Staff</span>
        @endif
    </h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">User Information</h3>
            </div>
            <div class="box-body">
                <div class="list-group">
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Email Address</h4>
                        <strong class="list-group-item-text">{{ $user->email }}</strong>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Signed Up</h4>
                        <strong class="list-group-item-text">{{ $user->created_at->format('d/m/Y') }} ({{ $user->created_at->diffForHumans() }})</strong>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Last Updated</h4>
                        <strong class="list-group-item-text">{{ $user->updated_at->format('d/m/Y') }} ({{ $user->updated_at->diffForHumans() }})</strong>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Verified Email</h4>
                        <strong class="list-group-item-text">{{ $user->verified ? 'Yes' : 'No' }}</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($user->isPartner())
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Associate Information</h3>
            </div>
            <div class="box-body">
                @unless($user->company == null)
                <div class="list-group">
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Company Name</h4>
                        <strong class="list-group-item-text">{{ $user->company->name ?: 'Not Set' }}</strong>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Company Telephone</h4>
                        <strong class="list-group-item-text">{{ $user->company->telephone ?: 'Not Set' }}</strong>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Company Address</h4>
                        <strong class="list-group-item-text">
                        {{ $user->company->address_line_one }}<br>
                        {{ $user->company->address_line_two }}<br>
                        {{ $user->company->town }}<br>
                        {{ $user->company->city }}<br>
                        {{ $user->company->county }}<br>
                        {{ $user->company->postcode }}<br>
                        </strong>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Company VAT Number</h4>
                        <strong class="list-group-item-text">{{ $user->company->vat_number ?: 'Not Set'  }}</strong>
                    </div>
                </div>
                @else
                <p>Nothing found.</p>
                @endunless
            </div>
        </div>
    </div>
    @else
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Vehicle Valuations</h3>
            </div>
            <div class="box-body">
                
            </div>
            <div class="box-footer">
                @if (isset($vehicles) and count($vehicles) > 0)
                    <table class="table table-hover">
                        <tr>
                            <th>#id</th>
                            <th>created at</th>
                            <th>VRM</th>
                            <th>valuations</th>
                            <th></th>
                        </tr>
                        @foreach ($vehicles as $vehicle)
                            <tr>
                                <td>{{ $vehicle->id }}</td>
                                <td>{{ $vehicle->created_at->format('Y/m/d H:i:s') }}</td>
                                <td><a href="{{ route('adminValuations') }}?numberPlate={{ $vehicle->numberplate }}" data-toggle="tooltip" title="View all valuations for this VRM">{{ $vehicle->numberplate }}</a></td>
                                <td>{{ count($vehicle->valuations) }}</td>
                                <td>
                                    <input type="button" class="btn btn-sm btn-info" value="expand" onclick="$('#user-valuations-vehicle-{{ $vehicle->id }}').toggle();if($(this).val()=='expand'){$(this).val('collapse')}else{$(this).val('expand');};">
                                </td>
                            </tr>
                            <tr id="user-valuations-vehicle-{{ $vehicle->id }}" style="display: none">
                                <td colspan="5">
                                    <table class="table table-hover" id="user-valuations-vehicle-{{ $vehicle->id }}" style="background-color: navajowhite;">
                                        @foreach ($vehicle->valuations as $valuation)
                                            <tr>
                                                <td>{{ $valuation->created_at->format('Y/m/d') }} </td>
                                                <td>by {{ $valuation->company->name }}</td>
                                                <td>{{ $valuation->value }}</td>
                                                @if ($valuation->accepted_at or ($valuation->sale and $valuation->sale->status == 'complete'))
                                                    <td style="color: green;"><b>complete</b></td>
                                                @elseif ($valuation->sale and $valuation->sale->status == 'pending')
                                                    <td style="color: #7aa420;"><b>accepted</b></td>
                                                @elseif ($valuation->sale and $valuation->sale->status == 'rejected')
                                                    <td style="color: red;"><b>rejected</b></td>
                                                @else
                                                    <td style="color: black;">
                                                        @if ($valuation->accept_button_clicked_at)
                                                            <b style="cursor: help;" title="Customer clicked an accept button at {{ $valuation->accept_button_clicked_at->format('Y/m/d H:i') }}">clicked</b>
                                                        @else
                                                            <b>new</b>
                                                        @endif
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @else
                    <h3>No vehicles to show</h3>
                @endif
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
