@extends('adminlte::page')

@section('content_header')
    <div class="pull-right">
        <form action="{{route('adminValuationsSearch')}}" method="GET" class="form-inline" role="form">

            <div class="form-group">
                <label class="sr-only" for="vrm">VRM</label>
                <input type="text" class="form-control" name="vrm" id="vrm" placeholder="Search for VRM's">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <h1>API Partner Valuations</h1>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">API Partner Valuations</h3>
            <div class="box-tools">
                {{ $valuations->appends(request()->query())->links('vendor.pagination.small-default') }}
            </div>
        </div>
        <div class="box-body">

            <div class="row">
                <form action="{{route('adminApiValuations')}}" method="GET" class="form form-horizontal" role="form">
                    <div class="form-group col-md-3">
                        <label for="from" class="col-sm-3 control-label">Date From</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="from" name="from" value="{{request()->get('from')}}">
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="to" class="col-sm-3 control-label">Date To</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="to" name="to" value="{{request()->get('to')}}">
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="company_id" class="col-sm-4 control-label">Company</label>
                        <div class="col-sm-8">
                            <select name="company_id" id="company_id" class="form-control">
                                <option value="" disabled selected>-- Select One --</option>
                                <option value="999">-- Dummy --</option>
                                @foreach ($companies as $company)
                                    <option value="{{$company->id}}" @if (request()->get('company_id') == $company->id) selected @endif>{{$company->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </form>


            </div>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Date Valued</th>
                        <th>VRM</th>
                        <th>Partner</th>
                        <th>Vehicle</th>
                        <th>Vehicle Type</th>
                        <th>Position</th>
                        <th>Value Offered</th>
                        <th>Value Type</th>
                        <th>Full Vehicle Information</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($valuations as $valuation)
                        <tr>
                            @if (($valuation->vehicle) && ($valuation->value_int))
                                <td>{{ $valuation->created_at->format('d/m/Y H:i') }}</td>
                                <td><a href="{{ route('adminValuationsSearch', $valuation->vehicle->numberplate) }}" data-toggle="tooltip" title="View all valuations for this VRM">{{ $valuation->vehicle->numberplate }}</a></strong></td>
                                @if ($valuation->company)
                                    <td>{{ $valuation->company->name }}</td>
                                @else
                                    <td><em>No Company Information</em></td>
                                @endif
                                <td>{{ $valuation->vehicle->meta->formatted_name }}</td>
                                <td>{{ $valuation->vehicle->type }}</td>
                                <td>{{ $valuation->position }}</td>
                                <td>{{ $valuation->value }}</td>
                                <td>{{ $valuation->value_type }}</td>
                                <td><button data-toggle="modal" data-target="#vehicle-{{ $valuation->id }}" class="btn btn-xs btn-success">Information</button></td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">
            {{ $valuations->appends(request()->query())->links('vendor.pagination.small-default') }}
        </div>
    </div>

    @foreach ($valuations as $valuation)
        @if ($valuation->vehicle)
            <div id="vehicle-{{ $valuation->id }}" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                {{ $valuation->vehicle->meta->manufacturer }} {{ $valuation->vehicle->meta->model }}
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Vehicle Manufacturer</th>
                                        <th>Vehicle Model</th>
                                        <th>Vehicle Type</th>
                                        <th>Vehicle Numberplate</th>
                                        <th>Vehicle Collection Valuation</th>
                                        <th>Vehicle Dropoff Valuation</th>
                                        <th>Vehicle Owner</th>
                                        <th>Vehicle Colour</th>
                                        <th>Vehicle Transmission</th>
                                        <th>Vehicle Plate Year</th>
                                        <th>Vehicle Fuel Type</th>
                                        <th>Vehicle Mileage</th>
                                        <th>Vehicle Owners</th>
                                        <th>Vehicle Service History</th>
                                        <th>Vehicle MOT</th>
                                        <th>Vehicle Write Off</th>
                                        <th>Vehicle Non Runner</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{ $valuation->vehicle->meta->manufacturer }}</td>
                                        <td>{{ $valuation->vehicle->meta->model }}</td>
                                        <td>{{ $valuation->vehicle->type }}</td>
                                        <td>{{ $valuation->vehicle->numberplate }}</td>
                                        <td>{{ $valuation->collection_value }}</td>
                                        <td>{{ $valuation->dropoff_value }}</td>
                                        @if($valuation->vehicle->owner)
                                            @if($valuation->vehicle->owner->user_id)
                                                <td><a href="{{route('adminUserEdit', $valuation->vehicle->owner->user_id) }}">{{ $valuation->vehicle->owner->firstname }} {{ $valuation->vehicle->owner->lastname }}</a></td>
                                            @else
                                                <td>{{ $valuation->vehicle->owner->firstname }} {{ $valuation->vehicle->owner->lastname }} <small>(No account created)</small></td>
                                            @endif
                                        @else
                                            <td>No user information.</td>
                                        @endif
                                        <td>{{ optional($valuation->vehicle->meta->colour)->title }}</td>
                                        <td>{{ $valuation->vehicle->meta->transmission }}</td>
                                        <td>{{ $valuation->vehicle->meta->plate_year }}</td>
                                        <td>{{ $valuation->vehicle->meta->fuel_type }}</td>
                                        <td>{{ $valuation->vehicle->meta->mileage }}</td>
                                        <td>{{ $valuation->vehicle->meta->number_of_owners }}</td>
                                        <td>{{ $valuation->vehicle->meta->service_history }}</td>
                                        <td>{{ $valuation->vehicle->meta->mot }}</td>
                                        <td>{{ $valuation->vehicle->meta->write_off }}</td>
                                        <td>{{ $valuation->vehicle->meta->non_runner }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach

@endsection
