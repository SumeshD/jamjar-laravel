@extends('adminlte::page')

@section('content_header')
    <div class="pull-right">
        <form class="form-inline" role="form">
            <div class="form-group">
                <label class="sr-only" for="vrm">VRM</label>
                <input type="text" value="{{ Request::input('numberPlate') }}" class="form-control" name="numberPlate" id="vrm" placeholder="Search for VRM's">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <h1>{{ $pageName ?? 'All Valuations' }}</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="completed-sales-filtering-header">
        <h3 class="box-title">{{ $pageName ?? 'All Valuations' }}</h3>
        {{ $valuations->appends(request()->query())->links('vendor.pagination.small-default') }}
    </div>
    <div class="box-header with-border">

        <div>
            <form id="completed-sales-filtering" class="completed-sales-filtering">
                <div class="">
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2 border--right filter-by-date">
                        <div class="col-md-12 p--zero">
                            <b>Filter by Date</b>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 p-right-50 p-left--zero">
                            <span class="input-title">From</span> <br /> <input class="datepicker" type="text" name="dateFrom" style="width:100%;" value="{{ Request::input('dateFrom') }}">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 p-left-50 p-right--zero">
                            <span class="input-title">To</span> <br /> <input class="datepicker" type="text" name="dateTo" style="width:100%;" value="{{ Request::input('dateTo') }}">
                        </div>
                    </div>
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2 border--right">
                        <div class="col-md-12 p--zero">
                            <b>Filter by Partner type</b> <br />
                            <span class="input-title">  Select your partner type</span>
                        </div>
                        <select name="companyType">
                            <option value="">ALL</option>
                            <option {{ Request::input('companyType') == 'api' ? 'selected' : ''  }} value="api">API</option>
                            <option {{ Request::input('companyType') == 'matrix' ? 'selected' : ''  }} value="matrix">Matrix Partner</option>
                        </select>
                    </div>
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2 border--right">
                        <b>Filter by Partner</b> <br />
                        <span class="input-title">Select your partner</span>
                        <select name="companyName">
                            <option value="">ALL</option>
                            @foreach ($availablePartnersNames as $availablePartnerName)
                                <option {{ Request::input('companyName') == $availablePartnerName ? 'selected' : ''  }} value="{{$availablePartnerName}}">{{$availablePartnerName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2 border--right">
                        <b>Filter by Valuation </b><br />
                        <span class="input-title">Select valuation</span>
                        <select name="valuationValueRange">
                            <option value="">ALL</option>
                            @foreach ($availableValuesRanges as $rangeName => $availableValuesRange)
                                <option {{ Request::input('valuationValueRange') == $availableValuesRange ? 'selected' : ''  }} value="{{$availableValuesRange}}">{{$rangeName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2">
                        <b>Filter by Status</b> <br />
                        <span class="input-title">Select status</span>
                        <select name="status">
                            <option value="">ALL</option>
                            @foreach ($availableValuationStatuses as $availableValuationStatus)
                                <option {{ Request::input('status') == $availableValuationStatus ? 'selected' : ''  }} value="{{$availableValuationStatus}}">{{$availableValuationStatus}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-lg-2 p--zero">
                        <input type="submit" value="SUBMIT FILTERS" class="btn btn-primary" style="float: right;">
                    </div>

                    <input type="hidden" name="orderColumn" value="{{ Request::input('orderColumn') }}">
                    <input type="hidden" name="orderDirection" value="{{ Request::input('orderDirection') }}">
                </div>
            </form>
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>
                            Date Valued
                            @include ('admin.valuations.sorting-arrows', ['orderColumn' => 'createdAt'])
                        </th>
                        <th>VRM</th>
                        <th>Partner</th>
                        <th>Vehicle</th>
                        <th>
                            Vehicle Type
                            @include ('admin.valuations.sorting-arrows', ['orderColumn' => 'vehicleType'])
                        </th>
                        <th>Position</th>
                        <th>
                            Value Offered
                            @include ('admin.valuations.sorting-arrows', ['orderColumn' => 'valueOffered'])
                        </th>
                        <th>Value Type</th>
                        <th>Full Vehicle Information</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($valuations as $valuation)
                    <tr>
                        @if (($valuation->vehicle) && ($valuation->value_int))
                            <td>{{ $valuation->created_at->format('d/m/Y H:i') }}</td>
                            <td><strong><a href="{{ route('adminValuations') }}?numberPlate={{ $valuation->vehicle->numberplate }}" data-toggle="tooltip" title="View all valuations for this VRM">{{ $valuation->vehicle->numberplate }}</a></strong></td>
                            @if ($valuation->company)
                                <td>{{ $valuation->company->name }}</td>
                            @else
                                <td><em>No Company Information</em></td>
                            @endif
                            <td>{{ $valuation->vehicle->meta->formatted_name }}</td>
                            <td>{{ $valuation->vehicle->type }}</td>
                            <td>{{ $valuation->position }}</td>
                            <td>{{ $valuation->value }}</td>
                            <td>{{ $valuation->value_type }}</td>
                            <td><button data-toggle="modal" data-target="#vehicle-{{ $valuation->id }}" class="btn btn-xs btn-success">Information</button></td>
                            @if ($valuation->accepted_at or ($valuation->sale and $valuation->sale->status == 'complete'))
                                <td style="color: green;"><b>complete</b></td>
                            @elseif ($valuation->sale and $valuation->sale->status == 'pending')
                                <td style="color: #7aa420;"><b>accepted</b></td>
                            @elseif ($valuation->sale and $valuation->sale->status == 'rejected')
                                <td style="color: red;"><b>rejected</b></td>
                            @else
                                <td style="color: black;">
                                    @if ($valuation->accept_button_clicked_at)
                                        <b style="cursor: help;" title="Customer clicked an accept button at {{ $valuation->accept_button_clicked_at->format('Y/m/d H:i') }}">clicked</b>
                                    @else
                                        <b>new</b>
                                    @endif
                                </td>
                            @endif
                        @endif
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
            {{ $valuations->appends(request()->query())->links('vendor.pagination.small-default') }}
    </div>
</div>

@foreach ($valuations as $valuation)
    @if ($valuation->vehicle)
        @include('admin.vehicle.additional-info', ['valuation' => $valuation])
    @endif
@endforeach

@endsection
