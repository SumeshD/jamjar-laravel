@extends('adminlte::page') 

@section('content_header')
    <h1>Automatic Associate</h1>
@endsection

@section('content')
<div class="box box-primary">
    <form action="{{route('autoPartnerStore')}}" method="POST" role="form">
        {{csrf_field()}}
        <div class="box-header with-border">
            <h3 class="box-title">Automatic Associates</h3>
        </div>
        <div class="box-body">
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!!$error!!}
                </div>
            @endforeach
            <div class="container">
                

            <div class="row">
                <legend>User Information</legend>

                <div class="form-group col-md-6 @if ($errors->has('first_name')) has-error @endif">
                    <label for="first_name">First Name <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{old('first_name')}}" required>
                </div>

                <div class="form-group col-md-6 @if ($errors->has('last_name')) has-error @endif">
                    <label for="last_name">Last Name <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{old('last_name')}}" required>
                </div>

                <div class="form-group col-md-6 @if ($errors->has('email')) has-error @endif">
                    <label for="email">E-mail Address <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail Address" value="{{old('email')}}" required>
                </div>

                <div class="form-group col-md-6 @if ($errors->has('password')) has-error @endif">
                    <label for="password">Password <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="{{old('password')}}" required>
                </div>
            </div>

            <div class="row">
                <legend>Profile Information</legend>

                <div class="form-group col-md-6 @if ($errors->has('job_title')) has-error @endif">
                    <label for="job_title">Job Title</label>
                    <input type="text" class="form-control" id="job_title" name="job_title" placeholder="Job Title">
                </div>

                <div class="form-group col-md-6 @if ($errors->has('mobile_number')) has-error @endif">
                    <label for="mobile_number">Mobile Number <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="tel" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile Number" value="{{old('mobile_number')}}" required>
                </div>

                <div class="form-group col-md-6 @if ($errors->has('address_line_one')) has-error @endif">
                    <label for="address_line_one">Address Line One <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="address_line_one" name="address_line_one" placeholder="Address Line One" value="{{old('address_line_one')}}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="address_line_two">Address Line Two</label>
                    <input type="text" class="form-control" id="address_line_two" name="address_line_two" placeholder="Address Line Two">
                </div>

                <div class="form-group col-md-6">
                    <label for="town">Town</label>
                    <input type="text" class="form-control" id="town" name="town" placeholder="Town" value="{{old('town')}}">
                </div>

                <div class="form-group col-md-6 @if ($errors->has('city')) has-error @endif">
                    <label for="city">City <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{old('city')}}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="county">County</label>
                    <input type="text" class="form-control" id="county" name="county" placeholder="County">
                </div>

                <div class="form-group col-md-6 @if ($errors->has('postcode')) has-error @endif">
                    <label for="postcode">Postcode <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode" value="{{old('postcode')}}" required>
                </div>
            </div>
            
            <div class="row">
                <legend>Company Information</legend>
                
                <div class="form-group col-md-6 @if ($errors->has('company_name')) has-error @endif">
                    <label for="company_name">Company Name <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="{{old('company_name')}}" required>
                </div>

                <div class="form-group col-md-6 @if ($errors->has('telephone_number')) has-error @endif">
                    <label for="telephone_number">Telephone Number <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="tel" class="form-control" id="telephone_number" name="telephone_number" placeholder="Telephone (Land Line) Number" value="{{old('telephone_number')}}" required>
                </div>

                <div class="form-group col-md-6 @if ($errors->has('vat_number')) has-error @endif">
                    <label for="vat_number">Company VAT Number <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="vat_number" name="vat_number" placeholder="Company VAT Number" value="{{old('vat_number')}}" required>
                </div>


                <div class="form-group col-md-6 @if ($errors->has('valuation_validity')) has-error @endif">
                    <label for="valuation_validity">Valuation Validity <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="valuation_validity" name="valuation_validity" placeholder="Valuation Validity" value="{{old('valuation_validity')}}" required>
                </div>
            </div>

            <div class="row">
                <legend>Company Fees</legend>

                <div class="form-group col-md-6 @if ($errors->has('bank_transfer_fee')) has-error @endif"">
                    <label for="bank_transfer_fee">Bank Transfer Fee <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="bank_transfer_fee" name="bank_transfer_fee" placeholder="Bank Transfer Fee" value="{{old('bank_transfer_fee')}}" required>
                </div>

                <div class="form-group col-md-6 @if ($errors->has('admin_fee')) has-error @endif"">
                    <label for="admin_fee">Admin Fee <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="admin_fee" name="admin_fee" placeholder="Admin Fee" value="{{old('admin_fee')}}" required>
                </div>

                <div class="form-group col-md-6 @if ($errors->has('collection_fee')) has-error @endif"">
                    <label for="collection_fee">Collection Fee</label>
                    <input type="text" class="form-control" id="collection_fee" name="collection_fee" placeholder="Collection Fee" value="{{old('collection_fee' ?? 0)}}">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="allow_collection" name="allow_collection">
                            Allow Collection
                        </label>
                    </div>
                </div>

                <div class="form-group col-md-6 @if ($errors->has('dropoff_fee')) has-error @endif"">
                    <label for="dropoff_fee">Dropoff Fee</label>
                    <input type="text" class="form-control" id="dropoff_fee" name="dropoff_fee" placeholder="Dropoff Fee" value="{{old('dropoff_fee' ?? 0)}}">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="allow_dropoff" name="allow_dropoff">
                            Allow Dropoff
                        </label>
                    </div>
                </div>

            </div>

            <div class="row">
                <legend>Legal</legend>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="gdpr" id="gdpr" required> Please tick the box to confirm you, or the associate on whose behalf you are creating this account for, agree to our <a href="https://www.jamjar.com/associate-terms" class="u-text-orange" target="_blank">terms &amp; conditions</a> &amp; <a href="https://www.jamjar.com/associate-privacy" class="u-text-orange" target="_blank">privacy policy</a>.
                    </label>
                    <input type="hidden" name="marketing" id="marketing" value="1">
    {{--                 <label>
                        <input type="checkbox" name="auto_partner" value="1"> I confirm that this associate is an automatic associate. They will appear in all valuations and are not subject to the "Set Offers" system, but a secondary pricing structure.
                    </label> --}}
                </div>
            </div>
        </div>
{{--             <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div> --}}

        </div>
        <div class="box-footer">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save &amp; Continue to Pricing</button>
            </div>
        </div>
    </form>
</div>
@endsection
