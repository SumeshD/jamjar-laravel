@extends('adminlte::page') 

@section('content_header')
    <h1>Automatic Associate</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Automatic Associates</h3>
    </div>
    <div class="box-body">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">What is an Automatic Associate?</h3>
            </div>
            <div class="panel-body">
                <p>
                    An automatic associate is an associate appears in the listings automatically, regardless of vehicle attributes.
                </p>
                <p>
                    The automatic associate's valuation is based upon several rules:
                </p>
                <ul>
                    <li>If a vehicle has more than three but less than six genuine valuations:</li>
                    <ul>
                        <li>The lowest non-scrap value is used as a baseline</li>
                        <li>A percentage (defined in funding amounts) is taken away from the value and displayed.</li>
                    </ul>
                    <li>If a vehicle has more than six genuine valuations:</li>
                    <ul>
                        <li>The sixth non-scrap value is used as a baseline</li>
                        <li>A percentage (defined in funding amounts) is taken away from the value and displayed.</li>
                    </ul>
                    <li>If there is only one genuine valuation:</li>
                    <ul>
                        <li>The valuation is used as a baseline</li>
                        <li>A percentage (defined in funding amounts) is taken away from the value and displayed.</li>
                    </ul>
                </ul>
            </div>
        </div>
        <div class="center-block text-center">
            <a href="{{route('autoPartnerCreate')}}" class="btn btn-lg btn-primary">Create Automatic Associate</a>
        </div>
    </div>
</div>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">All Automatic Associates</h3>
        <div class="box-tools">
            {{ $partners->links('vendor.pagination.small-default') }}
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            @unless($partners->isEmpty())
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="30%">Name</th>
                        <th width="30%">Email</th>
                        <th width="25%">Company Name</th>
                        <th width="15%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($partners as $partner)
                    <tr>
                        <td width="30%">{{ $partner->name }}</td>
                        <td width="30%">{{ $partner->user->email }}</td>
                        <td width="25%">
                            {{$partner->user->company->name}}
                        </td>
                        <td width="15%">
                            <div class="btn-group">
                                <a href="{{route('autoPartnerEdit', $partner->user_id)}}" data-toggle="tooltip" title="Edit Details" class="btn btn-primary">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="{{route('autoPartnerPricing', $partner->user_id)}}" data-toggle="tooltip" title="Edit Funding" class="btn btn-success">
                                    <i class="fa fa-money"></i>
                                </a>
                                <a href="{{route('autoPartnerWebsite', $partner->user_id)}}" data-toggle="tooltip" title="Edit Website" class="btn btn-primary">
                                    <i class="fa fa-globe"></i>
                                </a>
                                <a class="btn btn-danger btn-small" type="button" data-toggle="tooltip" title="Delete Partner" onclick="event.preventDefault(); confirmAction(function() { document.getElementById('delete-partner-{{$partner->id}}').submit();});">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach                
                </tbody>
            </table>
            @else
                <div class="box__content">
                    <p>No auto associates... yet.</p>
                </div>
            @endunless
        </div>
    </div>
    <div class="box-footer">
        {{ $partners->links('vendor.pagination.small-default') }}
    </div>
</div>

@foreach ($partners as $partner)
<form action="{{ route('autoPartnerDelete', $partner->user_id) }}" method="POST" id="delete-partner-{{$partner->id}}">
    {{csrf_field()}}
    {{method_field('DELETE')}}
</form>
@endforeach
@endsection
