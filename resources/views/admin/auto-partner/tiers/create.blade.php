@extends('adminlte::page') 

@section('content_header')
    <h1>Automatic Associate Pricing for {{$partner->company->name}}</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Automatic Associate Pricing for {{$partner->company->name}}</h3>
    </div>
    <form action="{{route('autoPartnerPricingStore', $partner)}}" method="POST" role="form">
        {{csrf_field()}}
        <div class="box-body js-append-inputs">
            {{-- Input Group --}}
            <div class="row js-input-group">                
                <div class="form-group col-md-6">
                    <label for="valuation_to[]">Value of Car</label>
                    <input type="number" class="form-control" id="valuation_to" name="valuation_to[]" placeholder="Value of Car" required>
                    @if ($errors->has('valuation_to'))
                        <span class="help-block">
                            <strong>{{ $errors->first('valuation_to') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="percentage[]">Percentage Reduction</label>
                    <input type="text" class="form-control" id="percentage" name="percentage[]" placeholder="Reduce value by this amount to generate automatic associate price." max="100" required>
                </div>
            </div>
            {{-- / Input Group --}}
        </div>
        <div class="box-footer">
            <div class="pull-left">
                <button type="submit" class="btn btn-primary">Save Rules</button>
            </div>

            <div class="pull-right">
                <a href="#" class="btn btn-success js-clone-inputs">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
    </form>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Current Pricing Tiers for {{$partner->company->name}}</h3>
        <div class="box-tools">
            {{ $tiers->links('vendor.pagination.small-default') }}
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            @unless($tiers->isEmpty())
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="30%">Tier</th>
                        <th width="30%">Percentage Reduction</th>
                        <th width="30%">Highest Possible Value Given</th>
                        <th width="10%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tiers as $tier)
                    <tr>
                        <td width="30%">{{ $tier->name }}</td>
                        <td width="30%">{{ $tier->percentage }}%</td>
                        <td width="30%">
                            &pound;{{ number_format($tier->valuation_to - (( $tier->valuation_to / 100) * $tier->percentage)) }}
                        </td>
                        <td width="10%">
                            <div class="btn-group">
                                <a class="btn btn-danger btn-small" type="button" onclick="event.preventDefault(); confirmAction(function() { document.getElementById('delete-tier-{{$tier->id}}').submit();});">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <form action="{{ route('autoPartnerPricingDelete', [$partner, $tier]) }}" method="POST" id="delete-tier-{{$tier->id}}">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                    </form>
                    @endforeach                
                </tbody>
            </table>
            @else
                <div class="box__content">
                    <p>No pricing tiers... yet.</p>
                </div>
            @endunless
        </div>
    </div>
    <div class="box-footer">
        {{ $tiers->links('vendor.pagination.small-default') }}
    </div>
</div>

<div class="box">
    <div class="box-body">
        <a href="{{route('autoPartnerWebsite', $partner)}}" class="btn btn-primary btn-block">Continue to Company Branding</a>
        <p class="help-block">You can return to this screen at any time by clicking "Auto Associate Setup" from the left menu, then clicking the green money button.</p>
    </div>
</div>

@push('scripts-after')
    <script>
        $('.js-clone-inputs').on('click', function(e) {
            e.preventDefault();
            let toClone = $('.js-input-group:first-of-type');
            toClone.clone().appendTo('.js-append-inputs');
            var cloned = $('.js-input-group:last-of-type');
            cloned.find('input').val('');
        });
    </script>
@endpush

@endsection
