@extends('adminlte::page') 

@section('content_header')
    <h1>Automatic Associate</h1>
@endsection

@section('content')
<div class="box box-primary">
    <form action="{{route('autoPartnerUpdate', $partner)}}" method="POST" role="form">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="box-header with-border">
            <h3 class="box-title">Automatic Associates</h3>
        </div>
        <div class="box-body">
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!!$error!!}
                </div>
            @endforeach
            <div class="container">
                

            <div class="row">
                <legend>User Information</legend>

                <div class="form-group col-md-6">
                    <label for="first_name">First Name <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{$partner->first_name}}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="last_name">Last Name <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{$partner->last_name}}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="email">E-mail Address <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail Address" value="{{$partner->email}}" required>
                </div>

            </div>

            <div class="row">
                <legend>Profile Information</legend>

                <div class="form-group col-md-6">
                    <label for="job_title">Job Title</label>
                    <input type="text" class="form-control" id="job_title" name="job_title" value="{{$partner->job_title}}" placeholder="Job Title">
                </div>

                <div class="form-group col-md-6">
                    <label for="mobile_number">Mobile Number <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="tel" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile Number" value="{{$partner->profile->telephone_number}}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="address_line_one">Address Line One <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="address_line_one" name="address_line_one" placeholder="Address Line One" value="{{$partner->profile->address_line_one}}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="address_line_two">Address Line Two</label>
                    <input type="text" class="form-control" id="address_line_two" name="address_line_two" value="{{$partner->profile->address_line_two}}" placeholder="Address Line Two">
                </div>

                <div class="form-group col-md-6">
                    <label for="town">Town</label>
                    <input type="text" class="form-control" id="town" name="town" placeholder="Town" value="{{$partner->profile->town}}">
                </div>

                <div class="form-group col-md-6">
                    <label for="city">City <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{$partner->profile->city}}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="county">County</label>
                    <input type="text" class="form-control" id="county" name="county" placeholder="County" value="{{$partner->profile->county}}">
                </div>

                <div class="form-group col-md-6">
                    <label for="postcode">Postcode <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode" value="{{$partner->profile->postcode}}" required>
                </div>
            </div>
            
            <div class="row">
                <legend>Company Information</legend>
                
                <div class="form-group col-md-6">
                    <label for="company_name">Company Name <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="{{$partner->company->name}}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="telephone_number">Telephone Number <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="tel" class="form-control" id="telephone_number" name="telephone_number" placeholder="Telephone (Land Line) Number" value="{{$partner->company->telephone}}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="vat_number">Company VAT Number <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="vat_number" name="vat_number" placeholder="Company VAT Number" value="{{$partner->company->vat_number}}" required>
                </div>


                <div class="form-group col-md-6">
                    <label for="valuation_validity">Valuation Validity <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="valuation_validity" name="valuation_validity" placeholder="Valuation Validity" value="{{$partner->company->valuation_validity}}" required>
                </div>
            </div>

            <div class="row">
                <legend>Company Fees</legend>

                <div class="form-group col-md-6">
                    <label for="bank_transfer_fee">Bank Transfer Fee <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="bank_transfer_fee" name="bank_transfer_fee" placeholder="Bank Transfer Fee" value="{{$partner->company->bank_transfer_fee}}">
                </div>

                <div class="form-group col-md-6">
                    <label for="admin_fee">Admin Fee <span style="color:red;font-weight: bold;">*</span></label>
                    <input type="text" class="form-control" id="admin_fee" name="admin_fee" placeholder="Admin Fee" value="{{$partner->company->admin_fee}}">
                </div>

                <div class="form-group col-md-6">
                    <label for="collection_fee">Collection Fee</label>
                    <input type="text" class="form-control" id="collection_fee" name="collection_fee" placeholder="Collection Fee" value="{{$location->collection_fee}}">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="allow_collection" name="allow_collection" {{ ($location->allow_collection == 1) ? ' checked' : '' }}>
                            Allow Collection
                        </label>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label for="dropoff_fee">Dropoff Fee</label>
                    <input type="text" class="form-control" id="dropoff_fee" name="dropoff_fee" placeholder="Dropoff Fee" value="{{$location->dropoff_fee}}">
                        <div class="checkbox">
                        <label>
                            <input type="checkbox" id="allow_dropoff" name="allow_dropoff" {{ ($location->allow_dropoff == 1) ? ' checked' : '' }}>
                            Allow Dropoff
                        </label>
                    </div>
                </div>

            </div>

        </div>
{{--             <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div>

            <div class="form-group col-md-6">
                <label for="changeme">metoo</label>
                <input type="text" class="form-control" id="changeme" name="changeme" placeholder="metoo">
            </div> --}}

        </div>
        <div class="box-footer">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save &amp; Continue to Pricing</button>
            </div>
        </div>
    </form>
</div>
@endsection
