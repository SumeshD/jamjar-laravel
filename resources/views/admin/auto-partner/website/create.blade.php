@extends('adminlte::page') 

@section('content_header')
    <div class="pull-right">
        <a href="{{ route('partnerWebsiteIndex', $partner->website) }}" target="_blank" class="btn btn-success">
            <i class="fa fa-eye"></i> View Website
        </a>
    </div>
    <h1>Website Information and Branding Colours for <strong>{{$partner->company->name}}</strong></h1>
@endsection

@section('content')

@push('styles-after')
	<link href="{{ secure_asset('css/vendor.css') }}" rel="stylesheet">
@endpush
<div class="box box-primary">
    <form action="{{route('autoPartnerWebsiteStore', $partner)}}" method="POST" role="form" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="box-header with-border">
            <h3 class="box-title">Website Information and Branding Colours for <strong>{{$partner->company->name}}</strong></h3>
        </div>
        <div class="box-body">
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!!$error!!}
                </div>
            @endforeach
            <legend>Branding &amp; Imagery</legend>

            <div class="form-group">
                <label for="logo_image">Logo Image</label>
                <input type="file" class="form-control" name="logo_image" id="logo_image">
                <hr>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img src="{{secure_asset($partner->website->logo_image)}}" alt="Current Logo Image" class="img-responsive">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="hero_image">Hero Image</label>
                <input type="file" class="form-control" name="hero_image" id="hero_image">
                <hr>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img src="{{secure_asset($partner->website->hero_image)}}" alt="Current Logo Image" class="img-responsive">
                    </div>
                </div>
            </div>


            <legend>Choose Theme Colours</legend>
            <div class="form-group col-md-2">
                <div class="color-block">
                    <label for="brand_color">Main Brand Colour</label><p></p>
                    <input id="brand_color" name="brand_color" class="color-block__item jscolor form__input" value="{{$partner->website->brand_color}}">
                </div>
            </div>
            <div class="form-group col-md-2">
                <div class="color-block">
                    <label for="bg_color">Background Colour</label><p></p>
                    <input id="bg_color" name="bg_color" class="color-block__item jscolor form__input" value="{{$partner->website->bg_color}}">
                </div>
            </div>

            <div class="row"></div>

            <legend>Website Content</legend>

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#homepage" data-toggle="tab" aria-expanded="true">Home Page</a></li>
                    <li class=""><a href="#faqpage" data-toggle="tab" aria-expanded="false">FAQ Page</a></li>
                    <li class=""><a href="#contactpage" data-toggle="tab" aria-expanded="false">Contact Page</a></li>
                    <li class=""><a href="#openingtimes" data-toggle="tab" aria-expanded="false">Opening Times</a></li>
                    <li class=""><a href="#termsandconditionspage" data-toggle="tab" aria-expanded="false">Terms &amp; Conditions</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="homepage">
                        <div class="form-group">
                            <label for="pages[home]">Home Page Content</label>
                            <textarea name="pages[home]" id="pages[home]" class="form-control" rows="6" required="required" placeholder="Home Page Content">{{$partner->website->getPage('home')->content}}</textarea>
                        </div>
                    </div>
                    <div class="tab-pane" id="faqpage">
                        <div class="form-group">
                            <label for="pages[faq]">FAQ Page Content</label>
                            <textarea name="pages[faq]" id="pages[faq]" class="form-control" rows="6" required="required" placeholder="FAQ Page Content">{{$partner->website->getPage('faq')->content}}</textarea>
                        </div>
                    </div>
                    <div class="tab-pane" id="contactpage">
                        <div class="form-group">
                            <label for="pages[contact]">Contact Page Content</label>
                            <textarea name="pages[contact]" id="pages[contact]" class="form-control" rows="6" required="required" placeholder="Contact Page Content">{{$partner->website->getPage('contact')->content}}</textarea>
                        </div>
                    </div>
                    <div class="tab-pane" id="openingtimes">
                        <div class="container">
                            <div class="row">
                                <p><strong>Opening Times</strong></p>
                                @foreach(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] as $day)
                                    <div class="col-md-2">
                                        <strong>{{$day}}</strong>
                                    </div>
                                    <div class="col-md-5" style="padding: 0 1rem;">
                                        <div class="form-group">
                                            <input 
                                                type="text" 
                                                name="openingtimes[{{strtolower($day)}}][open]" 
                                                id="openingtimes[{{strtolower($day)}}][open]" 
                                                class="form__input" 
                                                placeholder="09:00" 
                                                value="{{ array_get($openingtimes, strtolower($day).'.open') }}"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-5" style="padding: 0 1rem;">
                                        <div class="form-group">
                                            <input 
                                                type="text" 
                                                name="openingtimes[{{strtolower($day)}}][close]" 
                                                id="openingtimes[{{strtolower($day)}}][close]" 
                                                class="form__input" 
                                                placeholder="17:00" 
                                                value="{{ array_get($openingtimes, strtolower($day).'.close') }}"
                                            >
                                        </div>
                                    </div>        
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="termsandconditionspage">
                        <label for="pages[terms_and_conditions]">Terms Page Content</label>
                        <textarea name="pages[terms_and_conditions]" id="pages[terms_and_conditions]" class="form-control" rows="6" required="required" placeholder="Terms Page Content">{{$partner->website->getPage('terms-and-conditions')->content}}</textarea>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-block">Complete Setup</button>
            </div>
        </div>
    </form>
</div>

@include('partials.trumbo-icons-svg')

@push('scripts-after')
<script src="{{ secure_asset('js/vendor.js') }}"></script>
<script>
	  // TrumboWYG options
	  $('textarea').trumbowyg({
	    autogrow: true,
	    removeformatPasted: true,
	    btns: [
	        ['formatting'],
	        ['strong', 'em', 'underline'],
	        ['foreColor'],
	        ['link'],
	        ['insertImage'],
	        ['justifyLeft', 'justifyCenter', 'justifyRight'],
	        ['unorderedList'],
	        ['removeformat'],
	    ]
	  });
	  // End trumbowyg options
</script>
@endpush

@endsection
