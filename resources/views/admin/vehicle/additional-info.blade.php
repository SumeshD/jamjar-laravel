@php ($vehicle = $valuation->vehicle)
@php ($seller = $valuation->vehicle->owner)
@php ($buyer = $valuation->company->user)
@php ($buyerCompany = $valuation->company)
@php ($sale = $valuation->sale)

<div id="vehicle-{{ $valuation->id }}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h1>Valuation Information</h1>
                </div>
                <div class="row">
                    <div class="col-sm-12"><h3>Seller Details</h3></div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Seller Name <br />
                            <b>{{ $seller->name }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Seller Telephone <br />
                            <b>{{ $seller->telephone }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Seller Email <br />
                            <b>{{ $seller->email }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Seller Postcode <br />
                            <b>{{ $seller->postcode }}</b>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><h3>Vehicle Details</h3></div>
                    <div class="col-sm-12"><h4>{{ $vehicle->meta->model }}</h4></div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Derivative <br />
                            <b>{{ $vehicle->meta->derivative }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Plate <br />
                            <b>{{ $vehicle->meta->plate_year }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Registration <br />
                            <b>{{ $vehicle->numberplate }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Mileage <br />
                            <b>{{ $vehicle->meta->mileage }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Previous Owners <br />
                            <b>{{trans('vehicles.owners_count_external.owners_'.($vehicle->meta->number_of_owners <= 5 ? $vehicle->meta->number_of_owners : 6)) }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Service History <br />
                            <b>{{ $vehicle->meta->service_history }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            MOT <br />
                            <b>{{ $vehicle->meta->mot }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Write-Off <br />
                            <b>{{ $vehicle->meta->write_off ? 'YES' : 'NO' }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        @if ($vehicle->meta->write_off)
                            <div class="box-header with-border">
                                Write-Off Category <br />
                                <b>{{ $vehicle->meta->write_off_category }}</b>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Non-Runner <br />
                            <b>{{ $vehicle->meta->non_runner ? 'YES' : 'NO' }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        @if ($vehicle->meta->non_runner and $vehicle->meta->non_runner_reason)
                            <div class="box-header with-border">
                                Non-Runner Defects <br />
                                <ul>
                                    @foreach ($vehicle->meta->non_runner_reason as $defect)
                                        <li><strong>{{$defect->title}}</strong></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><h3>Partner Details</h3></div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Company Name <br />
                            <b>{{ $buyerCompany->name }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Company Telephone <br />
                            <b>{{ $buyerCompany->telephone }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Contact Name <br />
                            <b>{{ $buyer->name }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Contact Email <br />
                            <b>{{ $buyer->email }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Contact Telephone <br />
                            <b>{{ $buyer->profile->mobile_number }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            VAT Number <br />
                            <b>{{ $buyerCompany->vat_number }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Company Address <br />
                            <div><b>{{ $buyerCompany->address_line_one }}</b></div>
                            <div><b>{{ $buyerCompany->address_line_two }}</b></div>
                            <div><b>{{ $buyerCompany->town }}</b></div>
                            <div><b>{{ $buyerCompany->city }}</b></div>
                            <div><b>{{ $buyerCompany->postcode }}</b></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><h3>Valuation Details</h3></div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Valuation Date <br />
                            <b>{{ $valuation->created_at->format('d/m/Y') }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Valuation Position <br />
                            <b>{{ $valuation->position }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Collection Valuation (£) <br />
                            <b>{{ $valuation->collection_value }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Delivery Valuation (£) <br />
                            <b>{{ $valuation->dropoff_value }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Lead Fee (£) <br />
                            <b>
                                @if ((float) $valuation->fee > 0)
                                    £{{ number_format($valuation->fee / 100, 2) }}
                                @else
                                    unknown
                                @endif
                            </b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            @if ($valuation->accepted_at or ($valuation->sale and $valuation->sale->status == 'complete'))
                                Completed At: <br />
                                <b style="color: green;">
                                    @if ($valuation->accepted_at)
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', $valuation->accepted_at)->format('d/m/Y') }}
                                    @else
                                        {{ $sale->updated_at->format('d/m/Y') }}
                                    @endif
                                </b>
                            @elseif ($valuation->sale and $valuation->sale->status == 'pending')
                                Accepted At: <br />
                                <b>{{ $sale->created_at->format('d/m/Y') }}</b>
                            @elseif ($valuation->sale and $valuation->sale->status == 'rejected')
                                Rejected At: <br />
                                <b>{{ $sale->updated_at->format('d/m/Y') }}</b>
                            @else
                                Created At: <br />
                                <b>{{ $valuation->created_at->format('d/m/Y') }}</b>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Admin Fee <br />
                            <b>
                                £{{ $valuation->admin_fee ?? '0.00' }}
                            </b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Payment Method <br />
                            <b>
                                {{ $valuation->payment_method }}
                            </b>
                        </div>
                    </div>

                    @php ($expires = $valuation->created_at->addDay($valuation->company->valuation_validity))

                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Quote Expiry <br />
                            <b>
                                @if (now()->gte($expires))
                                    <span style="color: dimgrey">EXPIRED</span>
                                @else
                                    @php ($daysToExpire = \Carbon\Carbon::now()->diff($expires)->days)
                                    {{ $daysToExpire }} {{ $daysToExpire == 1 ? 'day' : 'days' }}
                                @endif
                            </b>
                        </div>
                    </div>

                    @if ($valuation->accept_button_clicked_at)
                        <div class="col-sm-6">
                            <div class="box-header with-border">
                                Accept Button clicked at <br />
                                <b>
                                    {{ $valuation->accept_button_clicked_at->format('Y/m/d H:i:s') }}
                                </b>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>