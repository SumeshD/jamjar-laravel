@php ($vehicle = $valuation->vehicle)
@php ($seller = $valuation->vehicle->owner)
@php ($buyer = $valuation->company->user)
@php ($buyerCompany = $valuation->company)
@php ($sale = $valuation->sale)

<div id="vehicle-{{ $valuation->id }}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h1>Sale Information</h1>
                </div>
                <div class="row">
                    <div class="col-sm-12"><h3>Seller Details</h3></div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Seller Name <br />
                            <b>{{ $seller->name }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Seller Telephone <br />
                            <b>{{ $seller->telephone }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Seller Email <br />
                            <b>{{ $seller->email }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Seller Postcode <br />
                            <b>{{ $seller->postcode }}</b>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><h3>Vehicle Details</h3></div>
                    <div class="col-sm-12"><h4>{{ $vehicle->meta->model }}</h4></div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Derivative <br />
                            <b>{{ $vehicle->meta->derivative }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Plate <br />
                            <b>{{ $vehicle->meta->plate_year }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Registration <br />
                            <b>{{ $vehicle->numberplate }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Mileage <br />
                            <b>{{ $vehicle->meta->mileage }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Previous Owners <br />
                            <b>{{trans('vehicles.owners_count_external.owners_'.($vehicle->meta->number_of_owners <= 5 ? $vehicle->meta->number_of_owners : 6)) }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Service History <br />
                            <b>{{ $vehicle->meta->service_history }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            MOT <br />
                            <b>{{ $vehicle->meta->mot }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Write-Off <br />
                            <b>{{ $vehicle->meta->write_off ? 'YES' : 'NO' }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        @if ($vehicle->meta->write_off)
                            <div class="box-header with-border">
                                Write-Off Category <br />
                                <b>{{ $vehicle->meta->write_off_category }}</b>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Non-Runner <br />
                            <b>{{ $vehicle->meta->non_runner ? 'YES' : 'NO' }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        @if ($vehicle->meta->non_runner and $vehicle->meta->non_runner_reason)
                            <div class="box-header with-border">
                                Non-Runner Defects <br />
                                <ul>
                                    @foreach ($vehicle->meta->non_runner_reason as $defect)
                                        <li><strong>{{$defect->title}}</strong></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><h3>Buyer Details</h3></div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Company Name <br />
                            <b>{{ $buyerCompany->name }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Company Telephone <br />
                            <b>{{ $buyerCompany->telephone }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Contact Name <br />
                            <b>{{ $buyer->name }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Contact Email <br />
                            <b>{{ $buyer->email }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Contact Telephone <br />
                            <b>{{ $buyer->profile->mobile_number }}</b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            VAT Number <br />
                            <b>{{ $buyerCompany->vat_number }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Company Address <br />
                            <div><b>{{ $buyerCompany->address_line_one }}</b></div>
                            <div><b>{{ $buyerCompany->address_line_two }}</b></div>
                            <div><b>{{ $buyerCompany->town }}</b></div>
                            <div><b>{{ $buyerCompany->city }}</b></div>
                            <div><b>{{ $buyerCompany->postcode }}</b></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><h3>Valuation & Sale Details</h3></div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Valuation (£) <br />
                            <b>
                                @if($sale)
                                    @if ($sale->delivery_method == 'collection')
                                        {{ $valuation->collection_value }}
                                    @else
                                        {{ $valuation->dropoff_value }}
                                    @endif
                                @else
                                    {{ $valuation->collection_value }} or {{ $valuation->dropoff_value }}
                                @endif
                            </b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Valuation Date <br />
                            <b>{{ $valuation->created_at->format('d/m/Y') }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Final Sale Price (£) <br />
                            <b>
                                @if($sale)
                                    @if ($sale->status == 'complete')
                                        £{{ number_format($sale->final_price) }}
                                    @else
                                        @if ($sale->delivery_method == 'collection')
                                            {{ $valuation->collection_value }}
                                        @else
                                            {{ $valuation->dropoff_value }}
                                        @endif
                                    @endif
                                @else
                                    unknown
                                @endif
                            </b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Final Sale Date <br />
                            <b>{{ $sale->updated_at->format('d/m/Y') }}</b>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Buyer Fee Due (£) <br />
                            <b>
                                @if ((float) $valuation->fee > 0)
                                    £{{ number_format($valuation->fee / 100, 2) }}
                                @else
                                    unknown
                                @endif
                            </b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Collection / Drop Off <br />
                            <b>
                                {{ $valuation->collection_value }} / {{ $valuation->dropoff_value }}
                            </b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Payment Method <br />
                            <b>
                                {{ $valuation->payment_method }}
                            </b>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-header with-border">
                            Admin Fee <br />
                            <b>
                                £{{ $valuation->admin_fee ?? '0.00' }}
                            </b>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>