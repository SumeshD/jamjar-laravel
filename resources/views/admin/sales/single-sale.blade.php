@php ($vehicle = $valuation->vehicle)
@php ($seller = $valuation->vehicle->owner)
@php ($buyer = $valuation->company->user)
@php ($sale = $valuation->sale)

<tr>
    <td>
        {{ $sale->updated_at->format('d/m/Y') }}
    </td>
    <td>
        @if(strtolower($valuation->company->company_type) == 'api')
            API
        @else
            Matrix
        @endif
    </td>
    <td>
        @if(strtolower($valuation->company->company_type) == 'api')
            @if ($valuation->company)
                {{ $valuation->company->name }}
            @else
                <em>No Company Information</em>
            @endif
        @elseif ($buyer)
            {{ $buyer->name }}
        @else
            unknown
        @endif
    </td>
    <td>
        @if($seller)
            {{ $seller->name }}
        @else
            unknown
        @endif
    </td>
    <td>
        {{ $valuation->vehicle->numberplate }}
    </td>
    <td>
        @if($sale)
            @if ($sale->delivery_method == 'collection')
                {{ $valuation->collection_value }}
            @else
                {{ $valuation->dropoff_value }}
            @endif
        @else
            {{ $valuation->collection_value }} or {{ $valuation->dropoff_value }}
        @endif
    </td>
    <td>
        @if($sale)
            @if ($sale->status == 'complete')
                £{{ number_format($sale->final_price) }}
            @else
                @if ($sale->delivery_method == 'collection')
                    {{ $valuation->collection_value }}
                @else
                    {{ $valuation->dropoff_value }}
                @endif
            @endif
        @else
            unknown
        @endif
    </td>
    <td>
        @if ((float) $valuation->fee > 0)
            £{{ number_format($valuation->fee / 100, 2) }}
        @else
            unknown
        @endif
    </td>
    <td><button data-toggle="modal" data-target="#vehicle-{{ $valuation->id }}" class="btn btn-xs btn-success">More Info</button></td>
</tr>