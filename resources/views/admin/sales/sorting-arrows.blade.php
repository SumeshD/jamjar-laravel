@if (Request::input('orderColumn') == $orderColumn)
    <span data-order-column="{{ $orderColumn }}" data-order-direction="{{ Request::input('orderDirection') == 'DESC' ? 'ASC' : 'DESC' }}" class="completed-sales-ordering">
        {{ Request::input('orderDirection') == 'DESC' ? '▼' : '▲' }}
    </span>
@else
    <span data-order-column="{{ $orderColumn }}" data-order-direction="ASC" class="completed-sales-ordering">△</span>
@endif
