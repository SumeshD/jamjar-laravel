@extends('adminlte::page')

@section('content_header')
    <div class="pull-right">
        <form class="form-inline" role="form">
            <div class="form-group">
                <label class="sr-only" for="vrm">VRM</label>
                <input type="text" value="{{ Request::input('numberPlate') }}" class="form-control" name="numberPlate" id="vrm" placeholder="Search for VRM's">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <h1>{{ $pageName ?? 'Sales' }}</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="completed-sales-filtering-header">
        <h3 class="box-title">{{ $pageName ?? 'Sales' }}</h3>
        {{ $sales->appends(request()->query())->links('vendor.pagination.small-default') }}
    </div>
    <div class="box-header with-border">
        <div>
            <form id="completed-sales-filtering" class="completed-sales-filtering">
                <div class="">
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2 border--right filter-by-date">
                        <div class="col-md-12 p--zero">
                            <b>Filter by Date</b>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 p-right-50 p-left--zero">
                            <span class="input-title">From</span> <br /> <input class="datepicker" type="text" name="dateCompletedFrom" style="width:100%;" value="{{ Request::input('dateCompletedFrom') }}">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 p-left-50 p-right--zero">
                            <span class="input-title">To</span> <br /> <input class="datepicker" type="text" name="dateCompletedTo" style="width:100%;" value="{{ Request::input('dateCompletedTo') }}">
                        </div>
                    </div>
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2 border--right">
                      <div class="col-md-12 p--zero">
                        <b>Filter by Partner type</b> <br />
                        <span class="input-title">  Select your partner type</span>
                      </div>
                        <select name="companyType">
                            <option value="">ALL</option>
                            <option {{ Request::input('companyType') == 'api' ? 'selected' : ''  }} value="api">API</option>
                            <option {{ Request::input('companyType') == 'matrix' ? 'selected' : ''  }} value="matrix">Matrix Partner</option>
                        </select>
                    </div>
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2 border--right">
                        <b>Filter by Partner</b> <br />
                        <span class="input-title">Select your partner</span>
                        <select name="companyName">
                            <option value="">ALL</option>
                            @foreach ($availablePartnersNames as $availablePartnerName)
                                <option {{ Request::input('companyName') == $availablePartnerName ? 'selected' : ''  }} value="{{$availablePartnerName}}">{{$availablePartnerName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2 border--right">
                        <b>Filter by Valuation </b><br />
                        <span class="input-title">Select valuation</span>
                        <select name="valuationValueRange">
                            <option value="">ALL</option>
                            @foreach ($availableValuesRanges as $rangeName => $availableValuesRange)
                                <option {{ Request::input('valuationValueRange') == $availableValuesRange ? 'selected' : ''  }} value="{{$availableValuesRange}}">{{$rangeName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="filter-block col-xs-12 col-sm-4 col-lg-2">
                        <b>Filter by Final Price </b><br />
                        <span class="input-title">Select price</span>
                        <select name="finalPriceRange">
                            <option value="">ALL</option>
                            @foreach ($availableValuesRanges as $rangeName => $availableValuesRange)
                                <option {{ Request::input('finalPriceRange') == $availableValuesRange ? 'selected' : ''  }} value="{{$availableValuesRange}}">{{$rangeName}}</option>
                            @endforeach
                        </select>
                    </div>

                    {{--<div class="filter-block col-xs-12 col-sm-4 col-lg-2">--}}
                      {{--<b>Filter by VRM </b><br />--}}
                      {{--<span class="input-title">Select VRM</span>--}}
                      {{--<input type="text" name="numberPlate" value="{{ Request::input('numberPlate') }}">--}}
                    {{--</div>--}}

                    <div class="col-xs-12 col-sm-12 col-lg-2 p--zero">
                        <input type="submit" value="SUBMIT FILTERS" class="btn btn-primary" style="float: right;">
                    </div>

                    <input type="hidden" name="orderColumn" value="{{ Request::input('orderColumn') }}">
                    <input type="hidden" name="orderDirection" value="{{ Request::input('orderDirection') }}">
                </div>
            </form>
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>
                            Date Completed
                            @include ('admin.sales.sorting-arrows', ['orderColumn' => 'updatedAt'])
                        </th>
                        <th>
                            Partner Type
                            @include ('admin.sales.sorting-arrows', ['orderColumn' => 'companyType'])
                        </th>
                        <th>Buyer</th>
                        <th>Seller</th>
                        <th>Number Plate</th>
                        <th>
                            Valuation
                            @include ('admin.sales.sorting-arrows', ['orderColumn' => 'valuationValue'])
                        </th>
                        <th>
                            Final Price
                            @include ('admin.sales.sorting-arrows', ['orderColumn' => 'finalPrice'])
                        </th>
                        <th>
                            Buyer Fee
                            @include ('admin.sales.sorting-arrows', ['orderColumn' => 'buyerFee'])
                        </th>
                        <th>More Info</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sales as $sale)
                        @if (($sale->valuation->vehicle) && ($sale->valuation->value_int))
                            @include('admin.sales.single-sale', ['valuation' => $sale->valuation])
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        {{ $sales->appends(request()->query())->links('vendor.pagination.small-default') }}
    </div>
</div>


@foreach ($sales as $sale)
    @if ($sale->valuation->vehicle)
        @include('admin.sales.invoice-draft', ['valuation' => $sale->valuation])
    @endif
@endforeach
@endsection
