@extends('adminlte::page')

@section('content_heading')
    <h1>Update Tier: {{ $tier->name }}</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Update Tier: {{ $tier->name }}</h3>
    </div>
    <form action="{{ route('adminTiersUpdate', $tier) }}" method="POST" role="form">
        <div class="box-body">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="form-group col-md-6">
                <label for="valuation_to">Value of Car</label>
                <input type="number" class="form-control" id="valuation_to" name="valuation_to" value="{{$tier->valuation_to }}" placeholder="Value of Car" required>
                @if ($errors->has('valuation_to'))
                    <span class="help-block">
                        <strong>{{ $errors->first('valuation_to') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-6">
                <label for="fee">Fee &pound;</label>
                <input type="number" step=".01" class="form-control" id="fee" name="fee" value="{{$tier->fee }}" placeholder="Fee at upper" required>
            </div>
        </div>
        <div class="box-footer">
            <div class="form-group pull-right">
                <button type="submit" class="btn btn-primary">Update Product Tier</button>
            </div>
        </div>
    </form>
</div>
@endsection
