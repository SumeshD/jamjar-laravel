@extends('adminlte::page') 

@section('content_header')
    <div class="pull-right">
        <a href="{{ route('adminTiersCreate') }}" class="btn btn-primary">
            <i class="fa fa-plus"></i> Add Pricing Tier
        </a>
    </div>
    <h1>All Pricing Tiers</h1>
    <br>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">All Pricing Tiers</h3>
        <div class="box-tools">
            {{ $tiers->links('vendor.pagination.small-default') }}
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            @unless($tiers->isEmpty())
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="30%">Tier</th>
                        <th width="30%">Fee</th>
                        <th width="30%">Active</th>
                        <th width="10%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tiers as $tier)
                    <tr>
                        <td width="30%">{{ $tier->name }}</td>
                        <td width="30%">&pound;{{ $tier->fee }}</td>
                        <td width="30%">{{ $tier->active ? 'Yes' : 'No' }}</td>
                        <td width="10%">
                            <div class="btn-group">
                                @if ($tier->active)
                                <a class="btn btn-warning btn-small" type="button" href="{{ route('adminTiersPause', $tier) }}">
                                    <i class="fa fa-pause"></i>
                                </a> 
                                @else
                                <a class="btn btn-default btn-small" type="button" href="{{ route('adminTiersResume', $tier) }}">
                                    <i class="fa fa-play"></i>
                                </a> 
                                @endif
                                <a class="btn btn-primary btn-small" type="button" href="{{ route('adminTiersEdit', $tier) }}">
                                    <i class="fa fa-pencil"></i>
                                </a> 
                                <a class="btn btn-danger btn-small" type="button" onclick="event.preventDefault();document.getElementById('delete-tier-{{$tier->id}}').submit();">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <form action="{{ route('adminTiersDelete', $tier) }}" method="POST" id="delete-tier-{{$tier->id}}">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                    </form>
                    @endforeach                
                </tbody>
            </table>
            @else
                <div class="box__content">
                    <p>No pricing tiers... yet.</p>
                </div>
            @endunless
        </div>
    </div>
    <div class="box-footer">
        {{ $tiers->links('vendor.pagination.small-default') }}
    </div>
</div>
@endsection
