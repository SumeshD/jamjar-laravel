@extends('adminlte::page')

@section('content_header')
    <h1>Pricing Tiers</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Create New Pricing Tiers</h3>
    </div>
    <form action="{{route('adminTiersStore')}}" method="POST" role="form">
        {{csrf_field()}}
        <div class="box-body">
            {{-- Input Group --}}
            <div class="row js-input-group">
                {{-- <div class="form-group col-md-4">
                    <label for="valuation_from[]">From</label>
                    <input type="number" class="form-control" id="valuation_from" name="valuation_from[]" placeholder="Minimum Value" required>
                    @if ($errors->has('valuation_from'))
                        <span class="help-block">
                            <strong>{{ $errors->first('valuation_from') }}</strong>
                        </span>
                    @endif
                </div> --}}
                <div class="form-group col-md-6">
                    <label for="valuation_to[]">Value of Car</label>
                    <input type="number" step=".01" class="form-control" id="valuation_to" name="valuation_to[]" placeholder="Value of Car" required>
                    @if ($errors->has('valuation_to'))
                        <span class="help-block">
                            <strong>{{ $errors->first('valuation_to') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="fee[]">Fee &pound;</label>
                    <input type="number" step=".01" class="form-control" id="fee" name="fee[]" placeholder="Fee at upper" required>
                </div>
            </div>
            {{-- / Input Group --}}
        </div>
        <div class="box-footer">
            <div class="pull-left">
                <button type="submit" class="btn btn-primary">Save Rules</button>
            </div>

            <div class="pull-right">
                <a href="#" class="btn btn-success js-clone-inputs">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
    </form>
</div>

@push('scripts-after')
    <script>
        $('.js-clone-inputs').on('click', function(e) {
            e.preventDefault();
            let toClone = $('.js-input-group:first-of-type');
            toClone.clone().appendTo('.box-body');
            var cloned = $('.js-input-group:last-of-type');
            cloned.find('input').val('');
        });
    </script>
@endpush

@endsection
