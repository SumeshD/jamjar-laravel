@extends('adminlte::page')

@section('content_heading')
    <h1>Create New Funding Amount</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Create New Funding Amount</h3>
    </div>
    <form action="{{ route('adminFundingAmountStore') }}" method="POST" role="form">
        <div class="box-body">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="price">Price (GBP)</label>
                <input type="number" class="form-control" name="price" id="price" placeholder="Funding Amount. (eg. 40.00)" required>
            </div>

            <div class="checkbox">
                <label>
                    <input type="hidden" name="active" id="active" value="0">
                    <input type="checkbox" name="active" id="active" checked="checked" value="1"> Published
                </label>
            </div>

        </div>
        <div class="box-footer">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection
