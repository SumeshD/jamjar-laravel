@extends('adminlte::page')

@section('content_heading')
    <h1>Update Funding Amount: &pound;{{ $amount->price }}</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Update Funding Amount: &pound;{{ $amount->price }}</h3>
    </div>
    <form action="{{ route('adminFundingAmountUpdate', $amount) }}" method="POST" role="form">
        <div class="box-body">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="form-group">
                <label for="price">Price (GBP)</label>
                <input type="number" step=".01" class="form-control" name="price" id="price" placeholder="Product Price. (eg. 40.00)" value="{{ old('price') ?? $amount->price}}" required>
            </div>

            <div class="checkbox">
                <label>
                    <input type="hidden" name="active" id="active" value="0">
                    <input type="checkbox" name="active" id="active" @if ($amount->active == 1) checked="checked" value="1"@endif> Published
                </label>
            </div>

        </div>
        <div class="box-footer">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Update Product</button>
            </div>
        </div>
    </form>
</div>
@endsection
