@extends('adminlte::page') 

@section('content_header')
    <div class="pull-right">
        <a href="{{ route('adminFundingAmountAdd') }}" class="btn btn-primary">
            <i class="fa fa-plus"></i> Add Funding Amount
        </a>
    </div>
    <h1>All Funding Amounts</h1>
    <br>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">All Funding Amounts</h3>
        <div class="box-tools">
            {{ $amounts->links('vendor.pagination.small-default') }}
        </div>
    </div>
    <div class="box-body">
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Here you can add new Funding Amounts for Associates.</strong> These are the pre-determined amounts which matrix partners are allowed to add to their accounts.
        </div>
        <div class="table-responsive">
            @unless($amounts->isEmpty())
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="33.33%">Price</th>
                        <th width="33.33%">Active</th>
                        <th width="33.33%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($amounts as $amount)
                    <tr>
                        <td width="33.33%">&pound;{{ $amount->price }}</td>
                        <td width="33.33%">{{ $amount->active ? 'Yes' : 'No' }}</td>
                        <td width="33.33%">
                            <div class="btn-group">
                                @if ($amount->active)
                                <a class="btn btn-warning btn-small" type="button" href="{{ route('adminFundingAmountPause', $amount) }}">
                                    <i class="fa fa-pause"></i>
                                </a> 
                                @else
                                <a class="btn btn-default btn-small" type="button" href="{{ route('adminFundingAmountResume', $amount) }}">
                                    <i class="fa fa-play"></i>
                                </a> 
                                @endif
                                <a class="btn btn-primary btn-small" type="button" href="{{ route('adminFundingAmountEdit', $amount) }}">
                                    <i class="fa fa-pencil"></i>
                                </a> 
                                <a class="btn btn-danger btn-small" type="button" onclick="event.preventDefault();document.getElementById('delete-amount-{{$amount->id}}').submit();">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <form action="{{ route('adminFundingAmountDelete', $amount) }}" method="POST" id="delete-amount-{{$amount->id}}">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                    </form>
                    @endforeach                
                </tbody>
            </table>
            @else
                <div class="box__content">
                    <p>No amounts... yet.</p>
                </div>
            @endunless
        </div>
    </div>
    <div class="box-footer">
        {{ $amounts->links('vendor.pagination.small-default') }}
    </div>
</div>
@endsection
