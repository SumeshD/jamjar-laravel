@extends('adminlte::page')

@section('content_header')
    <h1>Pricing Tiers</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Pricing Tiers</h3>
    </div>
    <form action="" method="POST" role="form">
        <div class="box-body">
            {{-- Input Group --}}
            <div class="row js-input-group">
                <div class="form-group col-md-6">
                    <label for="value_from">Value</label>
                    <input type="number"  class="form-control" id="value_from" name="value_from">
                </div>
                <div class="form-group col-md-6">
                    <label for="upper_fee">Fee at Upper &pound;</label>
                    <input type="number" class="form-control" id="upper_fee" id="upper_fee">
                </div>
                <div class="form-group hide">
                    <label for="lower_fee">Fee at Lower &pound;</label>
                    <input type="hidden" class="form-control" id="lower_fee" id="lower_fee">
                </div>
            </div>
            {{-- / Input Group --}}
        </div>
        <div class="box-footer">
            <div class="pull-left">
                <button type="submit" class="btn btn-primary">Save Rules</button>
            </div>

            <div class="pull-right">
                <a href="#" class="btn btn-success js-clone-inputs">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
    </form>
</div>

@push('scripts-after')
    <script>
        $('.js-clone-inputs').on('click', function(e) {
            e.preventDefault();
            let toClone = $('.js-input-group:first-of-type');
            toClone.clone().appendTo('.box-body');
            var cloned = $('.js-input-group:last-of-type');
            cloned.find('input').val('');
        });
    </script>
@endpush

@endsection
