@extends('adminlte::page')

@section('content')
    <div class="box box-primary">
        <div class="completed-sales-filtering-header">
            <h3 class="box-title">Email Debugger</h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>View name</th>
                        <th>Description</th>
                        <th>Number of arguments</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($emails as $email)
                        <?php /* @var \JamJar\Mail\EmailDebuggerInterface $email */ ?>
                        <tr>
                            <td>{{ $email::getViewName() }}</td>
                            <td>{{ $email::getDescription() }}</td>
                            <td>{{ count($email::getArgumentsTypes()) }}</td>
                            <td><a href="{{ route('AdminEmailDebuggerSingleType', [$email::getType()]) }}" class="btn btn-xs btn-success">debug</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection