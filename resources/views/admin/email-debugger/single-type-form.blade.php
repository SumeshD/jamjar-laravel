@extends('adminlte::page')

@section('content')
    <div class="box box-primary">
        <div class="completed-sales-filtering-header">
            <h3 class="box-title">Email Debugger</h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">

                <div>
                    Show last:
                    <a class="btn btn-xs btn-success" href="?rows=20">20 rows</a>
                    <a class="btn btn-xs btn-success" href="?rows=100">100 rows</a>
                    <a class="btn btn-xs btn-success" href="?rows=500">500 rows</a>
                </div>
                <?php /** @var \JamJar\Mail\EmailDebuggerInterface $email */ ?>
                <h2>{{ $email::getType() }}</h2>
                <h4>{{ $email::getViewName() }}</h4>

                <form method="post" action="{{ route('AdminEmailDebuggerSingleTypeSubmit', [$email::getType()]) }}">
                    {{ csrf_field() }}
                    <?php $counter = 0; ?>
                    @foreach($email::getArgumentsTypes() as $argumentLabel => $argument)
                        <div>
                            {{ $counter + 1 }}. {{ $argumentLabel }} => {{ $argument }} ->
                            @if ($argument == 'array')
                                <div>
                                    <input type="hidden" name="alternative-arguments-field-class[{{ $counter }}]" value="{{ $argument }}">
                                    <input type="hidden" name="alternative-arguments-field-value[{{ $counter }}]" value="">
                                    <textarea name="arguments[{{ $counter }}]" placeholder="json format" style="width: 500px; height: 200px;"></textarea>
                                </div>
                            @elseif ($argument == 'bool')
                                <div>
                                    <input type="hidden" name="alternative-arguments-field-class[{{ $counter }}]" value="{{ $argument }}">
                                    <input type="hidden" name="alternative-arguments-field-value[{{ $counter }}]" value="">
                                    <input name="arguments[{{ $counter }}]" placeholder="1 or 0" value="1">
                                </div>
                            @elseif ($argument == 'int')
                                <div>
                                    <input type="hidden" name="alternative-arguments-field-class[{{ $counter }}]" value="{{ $argument }}">
                                    <input type="hidden" name="alternative-arguments-field-value[{{ $counter }}]" value="">
                                    <input name="arguments[{{ $counter }}]" placeholder="integer value" value="1234">
                                </div>
                            @elseif ($argument == 'string')
                                <div>
                                    <input type="hidden" name="alternative-arguments-field-class[{{ $counter }}]" value="{{ $argument }}">
                                    <input type="hidden" name="alternative-arguments-field-value[{{ $counter }}]" value="">
                                    <input name="arguments[{{ $counter }}]" placeholder="string value" value="24">
                                </div>
                            @elseif (substr($argument, -2) == '[]')
                                <?php $argument = substr($argument, 0, -2); ?>
                                <div>
                                    <input type="hidden" name="alternative-arguments-field-class[{{ $counter }}]" value="{{ $argument }}">
                                    <input type="hidden" name="alternative-arguments-field-value[{{ $counter }}]" value="">
                                    @for ($i = 0; $i < 3; $i++)
                                        <select name="arguments[{{ $counter }}][{{ $i }}]" style="width: 200px;">
                                            @foreach ($argument::orderBy('id', 'DESC')->limit(request('rows') ? (int) request('rows') : 20)->get() as $entity)
                                                <option title="{{ (string) $entity }}" value="{{$argument}};{{ $entity->id }}">id: {{ $entity->id }}, {{ substr((string) $entity, 0, 80) }}</option>
                                            @endforeach
                                        </select>
                                        <br />
                                        <br />
                                    @endfor
                                </div>
                            @else
                                <select name="arguments[]" style="width: 200px;">
                                    @foreach ($argument::orderBy('id', 'DESC')->limit(request('rows') ? (int) request('rows') : 20)->get() as $entity)
                                        <option title="{{ (string) $entity }}" value="{{$argument}};{{ $entity->id }}">id: {{ $entity->id }}, {{ substr((string) $entity, 0, 80) }}</option>
                                    @endforeach
                                </select>
                                <br />
                                OR
                                <br />
                                <input type="hidden" name="alternative-arguments-field-class[{{ $counter }}]" value="{{ $argument }}">
                                <input type="text" name="alternative-arguments-field-name[{{ $counter }}]" placeholder="field eg. name">
                                <input type="text" name="alternative-arguments-field-value[{{ $counter }}]" placeholder="value (like) eg. john snow">
                            @endif
                            <?php $counter++; ?>
                        </div>
                        <div style="border-bottom: 2px solid grey; padding-bottom: 20px; margin-bottom: 20px;"></div>
                    @endforeach
                    <br />
                    <input type="submit" value="debug">
                </form>
            </div>
        </div>
    </div>

@endsection