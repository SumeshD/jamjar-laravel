@component('admin.activity.types.activity')
    @slot('title')
        WARNING!
    @endslot
    <p><strong>Invalid log view-name: {{ $viewName }}, activity-id: {{ $activityId }}</strong></p>
@endcomponent
