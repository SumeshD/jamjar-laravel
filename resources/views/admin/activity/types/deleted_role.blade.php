@component('admin.activity.types.activity')
    @slot('title')
        Role Deleted!
        @include('admin.activity.partials.timestamp')
    @endslot

    {{ $event->subject }}
@endcomponent
