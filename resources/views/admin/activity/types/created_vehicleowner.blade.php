@component('admin.activity.types.activity')
    @slot('title')
        New Vehicle Owner Created!
        @include('admin.activity.partials.timestamp')
    @endslot

    <p><strong>{{ $event->subject->vehicle->numberplate }}</strong> just had an owner added to it.</p>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th>Postcode</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $event->subject->firstname }} {{ $event->subject->lastname }}</td>
                    <td>{{ $event->subject->email }}</td>
                    <td>{{ $event->subject->postcode }}</td>
                </tr>
            </tbody>
        </table>
    </div>
@endcomponent
