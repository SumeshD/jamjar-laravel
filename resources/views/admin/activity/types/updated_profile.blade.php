@component('admin.activity.types.activity')
    @slot('title')
        Updated Profile!
        @include('admin.activity.partials.timestamp')
    @endslot
    {{ $event->subject->name }} was updated.
    </div>
@endcomponent
