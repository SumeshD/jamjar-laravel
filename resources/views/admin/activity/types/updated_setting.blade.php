@component('admin.activity.types.activity')
    @slot('title')
        Setting Updated!
        @include('admin.activity.partials.timestamp')
    @endslot
    
    <p>The <strong>{{ trans('settings.' . $event->subject->key) }}</strong> setting was updated.</p>
{{-- 
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>New</th>
                    <th>Old</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                @foreach ($event->changes as $key => $change)
                    <td>{{$change['value'] }}</td>
                @endforeach
                </tr>
            </tbody>
        </table>
    </div> --}}
    
@endcomponent
