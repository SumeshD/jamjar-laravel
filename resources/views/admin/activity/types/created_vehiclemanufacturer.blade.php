@component('admin.activity.types.activity')
    @slot('title')
        New Vehicle Manufacturer Added!
        @include('admin.activity.partials.timestamp')
    @endslot
    
    <p><strong>{{ $event->subject->name }}</strong> was saved as manufacturer from the CAP API.</p>
@endcomponent
