@component('admin.activity.types.activity')
    @slot('title')
        Profile Deleted!
        @include('admin.activity.partials.timestamp')
    @endslot
    {{ $event->subject->name }} was deleted.

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>New</th>
                    <th>Old</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                @foreach ($event->changes as $key => $change)
                    <td>{{$change['value'] }}</td>
                @endforeach
                </tr>
            </tbody>
        </table>
    </div>
@endcomponent
