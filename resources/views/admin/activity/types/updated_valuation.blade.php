@component('admin.activity.types.activity')
    @slot('title')
        Updated Valuation!
        @include('admin.activity.partials.timestamp')
    @endslot

    <p><strong>{{ $event->subject->vehicle->numberplate }}</strong>'s value was updated.</p>
@endcomponent
