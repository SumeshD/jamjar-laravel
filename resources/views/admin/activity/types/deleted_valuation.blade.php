@component('admin.activity.types.activity')
    @slot('title')
        Valuation Deleted!
        @include('admin.activity.partials.timestamp')
    @endslot

    {{ $event->subject }}
@endcomponent
