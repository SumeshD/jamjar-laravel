@component('admin.activity.types.activity')
    @slot('title')
        New User Registration
        @include('admin.activity.partials.timestamp')
    @endslot

    <p><strong>{{ $event->subject->name }}</strong> registered.</p>
@endcomponent
