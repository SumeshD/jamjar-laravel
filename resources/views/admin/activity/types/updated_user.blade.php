@component('admin.activity.types.activity')
    @slot('title')
        User Updated!
        @include('admin.activity.partials.timestamp')
    @endslot
    @if ($event->subject)
    {{ $event->subject->name }} was updated.
    @endif
@endcomponent
