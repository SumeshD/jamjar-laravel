@component('admin.activity.types.activity')
    @slot('title')
        Updated Colour!
        @include('admin.activity.partials.timestamp')
    @endslot
    
    <p><strong>{{ $event->subject->title }}</strong> was {{ $event->description }} as a colour</p>

@endcomponent
