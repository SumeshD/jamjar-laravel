@component('admin.activity.types.activity')
    @slot('title')
        New Vehicle Derivative Added!
        @include('admin.activity.partials.timestamp')
    @endslot
    
    <p><strong>{{ $event->subject->name }}</strong> was saved as a derivative for the <strong>{{ $event->subject->model->manufacturer->name }} {{ $event->subject->model->name }}</strong> from the CAP API.</p>
@endcomponent
