@component('admin.activity.types.activity')
    @slot('title')
        New Role Added!
        @include('admin.activity.partials.timestamp')
    @endslot

    <p><strong>{{ $event->subject->name }}</strong> was {{$event->description}} as a role.</p>
@endcomponent
