@component('admin.activity.types.activity')
    @slot('title')
        New Vehicle Model Added!
        @include('admin.activity.partials.timestamp')
    @endslot
    
    <p><strong>{{ $event->subject->name }}</strong> was saved as a model for the <strong>{{ $event->subject->manufacturer->name }}</strong> from the CAP API.</p>
@endcomponent
