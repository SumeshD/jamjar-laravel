@component('admin.activity.types.activity')
    @slot('title')
        New Vehicle Information Added!
        @include('admin.activity.partials.timestamp')
    @endslot
    
    <p><strong>{{ $event->subject->vehicle->numberplate }}</strong> just had more information added to it.</p>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Mileage</th>
                    <th>Plate Year</th>
                    <th>Transmission</th>
                    <th>Service History</th>
                    <th>Fuel Type</th>
                    <th>Colour</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $event->subject->mileage }}</td>
                    <td>{{ $event->subject->plate_year }}</td>
                    <td>{{ $event->subject->transmission }}</td>
                    <td>{{ $event->subject->service_history }}</td>
                    <td>{{ $event->subject->fuel_type }}</td>
                    <td>{{ $event->subject->colour['title'] }}</td>
                </tr>
            </tbody>
        </table>
    </div>
@endcomponent
