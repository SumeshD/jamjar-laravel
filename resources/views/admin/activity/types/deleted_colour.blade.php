@component('admin.activity.types.activity')
    @slot('title')
        Colour Deleted!
        @include('admin.activity.partials.timestamp')
    @endslot

    {{ $event->subject }}
@endcomponent
