@component('admin.activity.types.activity')
    @slot('title')
        New Setting Added!
        @include('admin.activity.partials.timestamp')
    @endslot

    <p><strong>{{ trans('settings.' . $event->subject->key) }}</strong> was created</p>
@endcomponent
