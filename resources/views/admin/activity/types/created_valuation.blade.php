@component('admin.activity.types.activity')
    @slot('title')
        New Valuation Saved!
        @include('admin.activity.partials.timestamp')
    @endslot

    <p><strong>{{ $event->subject->vehicle->numberplate }}</strong> just received a new valuation.</p>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Mileage</th>
                    <th>Colour</th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Valuation</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $event->subject->vehicle->meta->mileage }}</td>
                    <td>{{ $event->subject->vehicle->meta->colour['title'] }}</td>
                    <td>{{ $event->subject->vehicle->meta->manufacturer }}</td>
                    <td>{{ $event->subject->vehicle->meta->model }}</td>
                    <td>{{ $event->subject->value }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    @endcomponent
