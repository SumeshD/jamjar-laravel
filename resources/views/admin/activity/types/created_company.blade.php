@component('admin.activity.types.activity')
    @slot('title')
        New Associated Company Added!
        @include('admin.activity.partials.timestamp')
    @endslot
    
    <p><strong>{{ $event->subject->name }}</strong> was {{ $event->description }}</p>
@endcomponent
