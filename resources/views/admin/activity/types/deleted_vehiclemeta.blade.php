@component('admin.activity.types.activity')
    @slot('title')
        Vehicle Meta Deleted!
        @include('admin.activity.partials.timestamp')
    @endslot

    {{ $event->subject }}
@endcomponent
