@component('admin.activity.types.activity')
    @slot('title')
        New Vehicle Saved!
        @include('admin.activity.partials.timestamp')
    @endslot
    @if ($event->subject)
    <p><strong>{{ $event->subject->numberplate }}</strong> was just added to Jamjar.com</p>
    @endif

    @if ($event->subject)
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>CAP ID</th>
                    @if ($event->subject->meta)
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Derivative</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $event->subject->type }}</td>
                    <td>{{ $event->subject->cap_id }}</td>
                    @if ($event->subject->meta)
                    <td>{{ $event->subject->meta->manufacturer }}</td>
                    <td>{{ $event->subject->meta->model }}</td>
                    <td>{{ $event->subject->meta->derivative }}</td>
                    @endif
                </tr>
            </tbody>
        </table>
    </div>
    @endif
@endcomponent
