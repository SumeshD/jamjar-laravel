@component('admin.activity.types.activity')
    @slot('title')
        New Profile Added!
        @include('admin.activity.partials.timestamp')
    @endslot

    <p><strong>{{ $event->subject->name }}'s</strong> profile was {{$event->description}}</p>
@endcomponent
