@component('admin.activity.types.activity')
    @slot('title')
        New Order Placed!
        @include('admin.activity.partials.timestamp')
    @endslot
    
    <p><strong>{{ $event->subject->user->name }}</strong> placed a new order for {{ $event->subject->product_name }}</p>
@endcomponent
