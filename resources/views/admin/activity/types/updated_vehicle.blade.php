@component('admin.activity.types.activity')
    @slot('title')
        Updated Vehicle! 
        @include('admin.activity.partials.timestamp')
    @endslot

    <p><strong>{{ $event->subject->numberplate }}</strong> was just updated.</p>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>CAP ID</th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Derivative</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $event->subject->type }}</td>
                    <td>{{ $event->subject->cap_id }}</td>
                    <td>{{ $event->subject->meta->manufacturer }}</td>
                    <td>{{ $event->subject->meta->model }}</td>
                    <td>{{ $event->subject->meta->derivative }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    @endcomponent
