@component('admin.activity.types.activity')
    @slot('title')
        User Deleted!
        @include('admin.activity.partials.timestamp')
    @endslot
@endcomponent
