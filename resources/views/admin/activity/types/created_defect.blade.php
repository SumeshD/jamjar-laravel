@component('admin.activity.types.activity')
    @slot('title')
        New Defect Added!
        @include('admin.activity.partials.timestamp')
    @endslot

    <p><strong>{{ $event->subject->title }}</strong> was {{ $event->description }} as a defect</p>
@endcomponent
