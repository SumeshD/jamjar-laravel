@component('admin.activity.types.activity')
    @slot('title')
        Vehicle Owner Deleted!
        @include('admin.activity.partials.timestamp')
    @endslot

    {{ $event->subject }}
@endcomponent
