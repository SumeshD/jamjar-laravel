@extends('adminlte::page') 

@section('content_header')
    <h1>Activity Log</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">All Activity</h3>
    </div>
    <div class="box-body">
        @unless ($activities->isEmpty())

            @foreach ($activityViews as $view)
                {!! $view !!}
            @endforeach
        @else
            <p>There is no activity to show.</p>
        @endunless
    </div>
    <div class="box-footer">
        {{ $activities->links() }}
    </div>
</div>

@endsection
