@extends('adminlte::page')

@section('content_header')
    <h1>Throttling whitelist ips</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="table-responsive">
            <h4>Add new whitelist id</h4>
            <form action="{{ route('adminThrottlingWhitelistIpAdd') }}" method="POST">
                {{csrf_field()}}
                <input name="ip" placeholder="IP">
                <input name="note" placeholder="Note">
                <input type="submit" value="Submit">
            </form>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ip</th>
                        <th>note</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
	                @foreach ($ips as $ip)
                        <tr>
                            <td>{{ $ip->ip }}</td>
                            <td>{{ $ip->note }}</td>
                            <td>
                                <a class="btn btn-danger btn-small" type="button" onclick="event.preventDefault(); confirmAction(function() { document.getElementById('delete-ip-{{$ip->id}}').submit(); });">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <form action="{{ route('adminThrottlingWhitelistIpDelete', $ip->id) }}" method="POST" id="delete-ip-{{$ip->id}}">
                            {{csrf_field()}}
                        </form>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
