@extends('adminlte::page')

@section('content_header')
    <div class="pull-right">

    </div>
    <h1>Valuation Reminders</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Key Performance Indicator Data</h3>
    </div>
    <div class="box-body">

        <div class="row">
            <form action="{{route('vehicleRemindersTracker')}}" method="GET" class="form form-horizontal" role="form">
                <div class="form-group col-md-3">
                    <label for="from" class="col-sm-3 control-label">Date From</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" id="from" name="from" value="{{request()->get('from')}}">
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="to" class="col-sm-3 control-label">Date To</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" id="to" name="to" value="{{request()->get('to')}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </form>


        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Sent At</th>
                        <th>Sending Schedule From</th>
                        <th>Sent To</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reminders as $reminder)
                    <tr>
                        <td>{{trans('vehicle_reminders.' . $reminder->type)}}</td>
                        <td>{{ $reminder->created_at }}</td>
                        <td>{{ $reminder->start_date_of_sending_reminders }}</td>
                        <td>{{ isset($reminder->vehicle->owner->user->email) ? $reminder->vehicle->owner->user->email : ''}}</td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        {{ $reminders->appends(request()->query())->links('vendor.pagination.small-default') }}
    </div>
</div>

@endsection
