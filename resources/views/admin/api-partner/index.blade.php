@extends('adminlte::page') 

@section('content_header')
    <h1>API Partners</h1>
    <br>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">API Partners</h3>
    </div>
    <div class="box-body">
    	@unless ($companies->isEmpty())
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>API Partner Name</th>
                        <th>API Partner Telephone</th>
                        <th>API Partner VAT Number</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
	                @foreach ($companies as $company)
                    <tr>
                        <td>{{ $company->id }}</td>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->telephone ?? 'Not Set' }}</td>
                        <td>{{ $company->vat_number ?? 'Not Set' }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('adminApiPartnerEdit', $company) }}" class="btn btn-success btn-small" data-toggle="tooltip" title="Modify API Partner" type="button">
                                    <i class="fa fa-pencil"></i>
                                </a> 
                                @if ($company->active == 1)
                                <a href="{{ route('adminApiPartnerToggle', $company) }}" class="btn btn-primary btn-small" data-toggle="tooltip" title="Pause API Partner" type="button">
                                    <i class="fa fa-pause"></i>
                                </a> 
                                @else
                                <a href="{{ route('adminApiPartnerEdit', $company) }}" class="btn btn-success btn-small" data-toggle="tooltip" title="Resume API Partner" type="button">
                                    <i class="fa fa-play"></i>
                                </a> 
                                @endif
                            </div>
                        </td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        @else
        	<p>There are no API partners to show.</p>
        @endunless
    </div>
    <div class="box-footer">
        {{ $companies->links() }} 
    </div>
</div>

@endsection
