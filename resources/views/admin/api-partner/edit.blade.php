@extends('adminlte::page')

@section('content_header')
    <h1>{{ $company->name }} <small>Edit API Associate</small></h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit API Associate</h3>
    </div>
    <form action="{{ route('adminApiPartnerEdit', $company) }}" method="POST" role="form" autocomplete="off">
        <div class="box-body">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="form-group">
                <label for="company_name">Company Name</label>
                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Smith's Cars" value="{{ $company->name }}">
            </div>

            <div class="form-group">
                <label for="address_line_one">Address Line One</label>
                <input type="text" class="form-control" name="address_line_one" id="address_line_one" rows="5" required value="{{ $company->address_line_one }}">
            </div>

            <div class="form-group">
                <label for="address_line_two">Address Line Two</label>
                <input type="text" class="form-control" name="address_line_two" id="address_line_two" rows="5" value="{{ $company->address_line_two }}">
            </div>

            <div class="form-group">
                <label for="town">Town</label>
                <input type="text" class="form-control" name="town" id="town" rows="5" required value="{{ $company->town }}">
            </div>

            <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control" name="city" id="city" rows="5" required value="{{ $company->city }}">
            </div>

            <div class="form-group">
                <label for="county">County</label>
                <input type="text" class="form-control" name="county" id="county" rows="5" value="{{ $company->county }}">
            </div>

            <div class="form-group">
                <label for="postcode">Postcode</label>
                <input type="text" class="form-control" name="postcode" id="postcode" rows="5" required value="{{ $company->postcode }}">
            </div>

            <div class="form-group">
                <label for="telephone_number">Telephone Number</label>
                <input type="text" class="form-control" name="telephone_number" id="telephone_number" placeholder="01773 534119" value="{{ $company->telephone }}">
            </div>

            <div class="form-group">
                <label for="vat_number">VAT Number</label>
                <input type="text" class="form-control" name="vat_number" id="vat_number" placeholder="098 7654 32" value="{{ $company->vat_number }}">
            </div>

            <div class="form-group">
                <label for="valuation_validity">Valuation Validity</label>
                <input type="text" class="form-control" name="valuation_validity" id="valuation_validity" placeholder="3 day" value="{{ $company->valuation_validity }}">
                <div class="help-block">Used if the associate does not supply this information through the API..</div>
            </div>

            <div class="form-group">
                <label for="bank_transfer_fee">Bank Transfer Fee</label>
                <input type="number" step=".01" class="form-control" name="bank_transfer_fee" id="bank_transfer_fee" placeholder="50.00" value="{{ $company->bank_transfer_fee }}">
                <div class="help-block">Used if the associate does not supply this information through the API..</div>
            </div>

            <div class="form-group">
                <label for="admin_fee">Admin Fee</label>
                <input type="number" step=".01" class="form-control" name="admin_fee" id="admin_fee" placeholder="50.00" value="{{ $company->admin_fee }}">
                <div class="help-block">Used if the associate does not supply this information through the API..</div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection
