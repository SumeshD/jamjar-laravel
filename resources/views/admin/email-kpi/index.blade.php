@extends('adminlte::page')

@section('content_header')
    <div class="pull-right">

    </div>
    <h1>Email KPIs</h1>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Email KPIs</h3>
        </div>
        <div class="box-body">

            <div class="row">
                <form action="{{route('emailKpi')}}" method="GET" class="form form-horizontal" role="form">
                    <div class="form-group col-md-3">
                        <label for="from" class="col-sm-3 control-label">Date From</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="from" name="from" value="{{request()->get('from')}}">
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="to" class="col-sm-3 control-label">Date To</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="to" name="to" value="{{request()->get('to')}}">
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="to" class="col-sm-3 control-label">Version</label>
                        <div class="col-sm-9">
                            <select name="version" id="version" class="form-control">
                                <option value="" selected>All Versions</option>
                                <option value="v1" @if (request()->get('version') == 'v1') selected @endif>v1</option>
                                <option value="v2" @if (request()->get('version') == 'v2') selected @endif>v2</option>
                            </select>

                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="to" class="col-sm-3 control-label">Type</label>
                        <div class="col-sm-9">

                            <select name="type" id="type" class="form-control">
                                <option value="" selected>All Types</option>
                            @foreach (\Illuminate\Support\Facades\Config::get('email_types') as $email_type)
                                    <option value="{{$email_type}}"  @if (request()->get('type') == $email_type) selected @endif>{{$email_type}}</option>
                            @endforeach

                            </select>

                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>


            </div>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Total emails</th>
                        <th>Opened emails</th>
                        <th>Opened rate(%)</th>
                        <th>Url clicked emails</th>
                        <th>Click through rate(%)</th>
                        <th>Number of clicks per link</th>
                    </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>{{$emailsCount}}</td>
                            <td>{{$emailsOpenedCount}}</td>
                            <td>{{$emailsOpenedRate}}</td>
                            <td>{{$emailsClickedCount}}</td>
                            <td>{{$emailsClickedRate}}</td>
                            <td>

                                <input type="button" class="btn btn-sm btn-info" value="expand" onclick="$('#clicks').toggle();if($(this).val()=='expand'){$(this).val('collapse')}else{$(this).val('expand');};">

                            </td>
                        </tr>
                        <tr id="clicks" style=" display: none">
                            <td colspan="5">
                                <table class="table table-hover" style="background-color: navajowhite;">
                                    <thead>
                                    <tr>
                                        <th>Url</th>
                                        <th>Clicks</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    @foreach ($clickedEmails as $click)
                                        @if($click->url!='')
                                            <tr>
                                                <td>{{ $click->url }}</td>
                                                <td>{{ $click->total_clicks }}</td>
                                            </tr>
                                        @endif

                                    @endforeach

                                    </tbody>
                                </table>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>


        </div>
        <div class="box-footer">
        </div>
    </div>

@endsection
