@extends('adminlte::page')

@section('content_header')
    <div class="pull-right">
        <a href="{{route('dashboardAlertCreate')}}" class="btn btn-primary">
            <i class="fa fa-plus"></i> Add New Alert
        </a>
    </div>
    <h1>Dashboard Alerts</h1>
    <br>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">All Dashboard Alerts</h3>
            <div class="box-tools">
                {{--{{ $amounts->links('vendor.pagination.small-default') }}--}}
            </div>
        </div>
        <div class="box-body">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Here you can add new Dashboard Alerts.</strong>
            </div>
            <div class="table-responsive">
                @unless($alerts->isEmpty())
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th width="25%">Subject</th>
                            <th width="25%">Text</th>
                            <th width="25%">Active</th>
                            <th width="25%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($alerts as $alert)
                            <tr>
                                <td width="25%">{{ $alert->subject }}</td>
                                <td width="25">{{ $alert->text }}</td>
                                <td width="25%">{{ $alert->status ? 'Yes' : 'No' }}</td>
                                <td width="25%">
                                    <div class="btn-group">
                                        @if ($alert->status)
                                            <a class="btn btn-warning btn-small" type="button" href="{{ route('dashboardAlertDeactivate', $alert) }}">
                                                <i class="fa fa-pause"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-default btn-small" type="button" href="{{ route('dashboardAlertActivate', $alert) }}">
                                                <i class="fa fa-play"></i>
                                            </a>
                                        @endif
                                        <a class="btn btn-danger btn-small" type="button" onclick="event.preventDefault();document.getElementById('delete-alert-{{$alert->id}}').submit();">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <form action="{{ route('dashboardAlertDelete', $alert) }}" method="POST" id="delete-alert-{{$alert->id}}">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                            </form>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="box__content">
                        <p>No alerts... yet.</p>
                    </div>
                @endunless
            </div>
        </div>
        <div class="box-footer">
            {{ $alerts->links('vendor.pagination.small-default') }}
        </div>
    </div>
@endsection
