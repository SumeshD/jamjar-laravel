@extends('adminlte::page')

@section('content_heading')
    <h1>Create New Funding Amount</h1>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create New Funding Amount</h3>
        </div>
        <form action="{{ route('dashboardAlertStore') }}" method="POST" role="form">
            <div class="box-body">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="subject">Subject</label>
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Alert subject" required>
                </div>
                <div class="form-group">
                    <label for="text">Text</label>
                    <textarea type="" class="form-control" name="text" id="subject" placeholder="Alert text" required></textarea>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="hidden" name="active" id="active" value="0">
                        <input type="checkbox" name="active" id="active" checked="checked" value="1"> Active
                    </label>
                </div>

            </div>
            <div class="box-footer">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
