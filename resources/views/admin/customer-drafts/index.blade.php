@extends('adminlte::page')

@section('content_header')
    <div class="pull-right">
        <form class="form-inline" role="form">
            <div class="form-group">
                <label class="sr-only" for="vrm">VRM</label>
                <input type="text" value="{{ Request::input('numberPlate') }}" class="form-control" name="numberPlate" id="vrm" placeholder="Search for VRM's">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <h1>Customer Drafts</h1>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="completed-sales-filtering-header">
            <h3 class="box-title">Customer Drafts</h3>
            {{ $vehicles->appends(request()->query())->links('vendor.pagination.small-default') }}
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Date Created</th>
                        <th>VRM</th>
                        <th>Vehicle</th>
                        <th>Vehicle Type</th>
                        <th>Drafts Count</th>
                        <th>Valuations Count</th>
                        <th>Highest Value</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($vehicles as $vehicle)
                        <?php /** @var \JamJar\Vehicle $vehicle */ ?>
                        <?php $draftsCount = count($vehicle->getDrafts()); ?>
                        <?php $valuationsCount = count($vehicle->getValuations()); ?>
                        <tr>
                            <td>{{ $vehicle->created_at->format('Y/m/d H:i:s') }}</td>
                            <td>{{ $vehicle->numberplate }}</td>
                            <td>{{ $vehicle->meta->formatted_name }}</td>
                            <td>{{ $vehicle->type }}</td>
                            <td>{{ $draftsCount }}</td>
                            <td>{{ $valuationsCount }}</td>
                            <td>£{{ number_format($vehicle->getCurrentValueInPence() / 100) }}</td>
                            <td>

                                    <input type="button" class="btn btn-sm btn-info" value="expand" onclick="$('#user-valuations-vehicle-{{ $vehicle->id }}').toggle();if($(this).val()=='expand'){$(this).val('collapse')}else{$(this).val('expand');};">

                            </td>
                        </tr>


                            <tr id="user-valuations-vehicle-{{ $vehicle->id }}" style=" display: none">
                                <td colspan="5">
                                    <table class="table table-hover" style="background-color: navajowhite;">
                                        <thead>
                                            <tr>
                                                <th>Partner</th>
                                                <th>Value</th>
                                                <th>Converted</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                        @foreach ($vehicle->getDrafts() as $draft)
                                            <tr>
                                                <td>{{ $draft->company->name }}</td>
                                                <td>{{ $draft->getFormattedPrice() }}</td>
                                                <td>{{ $draft->hasConverted() }}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </td>
                            </tr>



                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">
            {{ $vehicles->appends(request()->query())->links('vendor.pagination.small-default') }}
        </div>
    </div>

@endsection