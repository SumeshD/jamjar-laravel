@extends('adminlte::page') 

@section('content_header')
    <div class="pull-right">
        <a href="{{ route('adminPartnerAdd') }}" class="btn btn-primary">
            <i class="fa fa-plus"></i> Add Associate
        </a>
    </div>
    <h1>All Associates</h1>
    <br>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">All Associates</h3>
    </div>
    <div class="box-body">
    	@unless ($users->isEmpty())
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Associate Contact</th>
                        <th>Associate Name</th>
                        <th>Associate Telephone</th>
                        <th>Associate VAT Number</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
	                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        @if ($user->company)
                        <td>{{ $user->company->name ?? 'Not set'}}</td>
                        <td>{{ $user->company->telephone ?? 'Not Set' }}</td>
                        <td>{{ $user->company->vat_number ?? 'Not Set' }}</td>
                        @else
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        @endif
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-warning btn-small" data-toggle="tooltip" title="Log in as {{$user->name}}" type="button" href="{{ route('adminPartnerLogin', $user) }}">
                                    <i class="fa fa-key"></i>
                                </a> 
                                <a class="btn btn-info btn-small" type="button" href="{{ route('adminUserShow', $user) }}">
                                    <i class="fa fa-eye"></i>
                                </a> 
                                <a class="btn btn-success btn-small" type="button" href="{{ route('adminPartnerEdit', $user) }}">
                                    <i class="fa fa-pencil"></i>
                                </a> 
                                @if ($user->id !== auth()->user()->id)
                                <a class="btn btn-danger btn-small" type="button" onclick="event.preventDefault(); confirmAction(function() { document.getElementById('delete-profile-{{$user->id}}').submit(); });">
                                    <i class="fa fa-trash"></i>
                                </a>
                                @endif
                            </div>
                        </td>
                        <form action="{{ route('adminUserDelete', $user) }}" method="POST" id="delete-profile-{{$user->id}}">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                        </form>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        @else
        	<p>There are no associates to show.</p>
        @endunless
    </div>
    <div class="box-footer">
        {{ $users->links() }} 
    </div>
</div>

@endsection
