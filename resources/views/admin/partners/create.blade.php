@extends('adminlte::page')

@section('content_heading')
    <h1>Create New User</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Create New Associate</h3>
    </div>
    <form action="{{ route('adminPartnerAdd') }}" method="POST" role="form">
        <div class="box-body">
            {{ csrf_field() }}
            <legend>User Information</legend>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Username" required>
            </div>

            <div class="form-group">
                <label for="email">Email Address</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" required>
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
            </div>

            <legend>Company Information</legend>

            <div class="form-group">
                <label for="company_name">Company Name</label>
                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Smith's Cars" required>
            </div>

            <div class="form-group">
                <label for="address_line_one">Address Line One</label>
                <input type="text" class="form-control" name="address_line_one" id="address_line_one" required>
            </div>

            <div class="form-group">
                <label for="address_line_two">Address Line Two</label>
                <input type="text" class="form-control" name="address_line_two" id="address_line_two">
            </div>

            <div class="form-group">
                <label for="town">Town</label>
                <input type="text" class="form-control" name="town" id="town" >
            </div>

            <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control" name="city" id="city" required>
            </div>

            <div class="form-group">
                <label for="county">County</label>
                <input type="text" class="form-control" name="county" id="county">
            </div>

            <div class="form-group">
                <label for="postcode">Postcode</label>
                <input type="text" class="form-control" name="postcode" id="postcode" required>
            </div>

            <div class="form-group">
                <label for="telephone_number">Telephone Number</label>
                <input type="number" class="form-control" name="telephone_number" id="telephone_number" placeholder="01773 534119" required>
            </div>

            <div class="form-group">
                <label for="vat_number">VAT Number</label>
                <input type="text" class="form-control" name="vat_number" id="vat_number" placeholder="098 7654 32" required>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="verified" id="verified" checked> Verified Email Address
                    <small>
                        (If this is not checked, the partner will receive an email asking them to verify their email address)
                    </small>
                </label>
            </div>
        </div>
        <div class="box-footer">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection
