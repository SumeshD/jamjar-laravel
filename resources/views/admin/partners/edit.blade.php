@extends('adminlte::page')

@section('content_header')
    <h1>{{ $user->name }} <small>Edit Associate</small></h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Associate</h3>
    </div>
    <form action="{{ route('adminPartnerEdit', $user) }}" method="POST" role="form" autocomplete="off">
        <div class="box-body">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="form-group">
                <label for="username">Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Username" value="{{ $user->profile->name }}">
            </div>

            <div class="form-group">
                <label for="email">Email Address</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ $user->profile->email }}">
            </div>

            <div class="form-group">
                <label for="company_name">Company Name</label>
                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Smith's Cars" value="{{ $user->company->name }}">
            </div>

            <div class="form-group">
                <label for="address_line_one">Address Line One</label>
                <input type="text" class="form-control" name="address_line_one" id="address_line_one" rows="5" required value="{{ $user->company->address_line_one }}">
            </div>
            <div class="form-group">
                <label for="address_line_two">Address Line Two</label>
                <input type="text" class="form-control" name="address_line_two" id="address_line_two" rows="5" value="{{ $user->company->address_line_two }}">
            </div>
            <div class="form-group">
                <label for="town">Town</label>
                <input type="text" class="form-control" name="town" id="town" rows="5" required value="{{ $user->company->town }}">
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control" name="city" id="city" rows="5" required value="{{ $user->company->city }}">
            </div>
            <div class="form-group">
                <label for="county">County</label>
                <input type="text" class="form-control" name="county" id="county" rows="5" value="{{ $user->company->county }}">
            </div>
            <div class="form-group">
                <label for="postcode">Postcode</label>
                <input type="text" class="form-control" name="postcode" id="postcode" rows="5" required value="{{ $user->company->postcode }}">
            </div>

            <div class="form-group">
                <label for="telephone_number">Telephone Number</label>
                <input type="number" class="form-control" name="telephone_number" id="telephone_number" placeholder="01773 534119" value="{{ $user->company->telephone }}">
            </div>

            <div class="form-group">
                <label for="vat_number">VAT Number</label>
                <input type="text" class="form-control" name="vat_number" id="vat_number" placeholder="098 7654 32" value="{{ $user->company->vat_number }}">
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="verified" id="verified" @if ($user->verified == 1) checked="checked" value="1"@endif> Verified Email Address
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="is_scrap_dealer" id="is_scrap_dealer" @if ($user->profile->is_scrap_dealer == 1) checked="checked" @endif> This Associate is a Scrap Dealer
                </label>
                <p class="help-text">
                    <small>If this is checked, the price will be the price, there will be no condition slider displayed on the partner's website.</small>
                </p>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="use_flat_fee" id="use_flat_fee" @if ($user->profile->use_flat_fee == 1) checked="checked" value="1"@endif> Use Flat Fee System
                </label>
            </div>

            <div class="form-group">
                <label for="vat_number">Flat Fee Per Lead (in £)</label>
                <input type="text" class="form-control" name="flat_fee" id="flat_fee" value="{{ $user->profile->flat_fee ?? 0 }}">
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection
