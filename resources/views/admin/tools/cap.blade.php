@extends('adminlte::page')

@section('content_header')
    <h1>CAP Valuation Tool</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">CAP Valuation Tool</h3>
    </div>
    <div class="box-body">
        <form action="{{ route('getCapValuations') }}" method="POST" role="form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="vrm">VRM (Numberplate)</label>
                <input type="text" class="form-control" id="vrm" name="vrm" placeholder="Numberplate" required>
            </div>

            <div class="form-group">
                <label for="mileage">Mileage</label>
                <input type="number" class="form-control" id="mileage" name="mileage" placeholder="Mileage" required>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection
