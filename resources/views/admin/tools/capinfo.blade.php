@extends('adminlte::page') 

@section('content_header')
    <h1>CAP Valuation Results</h1>
@stop

@section('content')

<h1 style="text-transform:uppercase; font-weight: 900; color: green; text-align: center; font-size: 3rem;">{{$vehicle->meta->manufacturer}} {{$vehicle->meta->model}} {{$vehicle->meta->derivative}} is worth &pound; {{number_format($valuation['average'])}}</h1>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">CAP Valuation Results</h3>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-responsive table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Cap ID</th>
                        <th>Cap Code</th>
                        <th>Numberplate</th>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Derivative</th>
                        <th>Color</th>
                        <th>Transmission</th>
                        <th>Plate Year</th>
                        <th>Fuel Type</th>
                        <th>Previous Owners</th>
                        <th>VIN Number</th>
                        <th>Doors</th>
                        <th>Engine Capacity</th>
                        <th>Body Type</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $vehicle->cap_id ?? ''}}</td>
                        <td>{{ $vehicle->capcode ?? ''}}</td>
                        <td>{{ $vehicle->numberplate ?? ''}}</td>
                        <td>{{ $vehicle->meta->manufacturer ?? ''}}</td>
                        <td>{{ $vehicle->meta->model ?? ''}}</td>
                        <td>{{ $vehicle->meta->derivative ?? ''}}</td>
                        <td>{{ $vehicle->meta->colour->title ?? ''}}</td>
                        <td>{{ $vehicle->meta->transmission ?? ''}}</td>
                        <td>{{ $vehicle->meta->plate_year ?? ''}}</td>
                        <td>{{ $vehicle->meta->fuel_type ?? ''}}</td>
                        <td>{{trans('vehicles.owners_count_external.owners_'.($vehicle->meta->number_of_owners <= 5 ? $vehicle->meta->number_of_owners : 6)) }}</td>
                        <td>{{ $vehicle->meta->vin_number ?? ''}}</td>
                        <td>{{ $vehicle->meta->doors ?? ''}}</td>
                        <td>{{ $vehicle->meta->engine_capacity ?? ''}}</td>
                        <td>{{ $vehicle->meta->body_type ?? ''}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
