@extends('adminlte::page') 

@section('content_header')
    <h1>Derivatives</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Derivatives (From API)</h3>
        <div class="box-tools">
            {{ $derivatives->links('vendor.pagination.small-default') }}
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>CAP ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($derivatives as $derivative)
                    <tr>
                        <td>{{ $derivative->get('cap_derivative_id') }}</td>
                        <td>{{ $derivative->get('name') }}</td>
                        <td>    
                        </td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        {{ $derivatives->links('vendor.pagination.small-default') }}
    </div>
</div>
@endsection
