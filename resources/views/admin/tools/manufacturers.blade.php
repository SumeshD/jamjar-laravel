@extends('adminlte::page') 

@section('content_header')
    <h1>Manufacturers</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">All Manufacturers (From Database)</h3>
        <div class="box-tools">
            {{ $manufacturers->links('vendor.pagination.small-default') }}
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>CAP ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($manufacturers as $manufacturer)
                    <tr>
                        <td>{{ $manufacturer->id }}</td>
                        <td>{{ $manufacturer->cap_manufacturer_id }}</td>
                        <td>{{ $manufacturer->name }}</td>
                        <td>
                            <a href="{{route('getCapModels', $manufacturer->cap_manufacturer_id)}}" class="btn btn-primary">
                                <i class="fa fa-car"></i> View Models
                            </a>
                            <a href="{{route('saveCapData', $manufacturer->cap_manufacturer_id)}}" class="btn btn-success">
                                <i class="fa fa-save"></i> Save Models &amp; Derivatives to DB
                            </a>                    
                        </td>
                    </tr>
                    <form action="{{ route('adminUserDelete', $manufacturer) }}" method="POST" id="delete-manufacturer-{{$manufacturer->id}}">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                    </form>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        {{ $manufacturers->links('vendor.pagination.small-default') }}
    </div>
</div>
@endsection
