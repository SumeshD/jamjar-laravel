@extends('adminlte::page') 

@section('content_header')
    <h1>Models for {{$manufacturer->name}}</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">All Models for {{$manufacturer->name}} (From API)</h3>
        <div class="box-tools">
            {{ $models->links('vendor.pagination.small-default') }}
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>CAP ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($models as $model)
                    <tr>
                        <td>{{ $model->get('cap_model_id') }}</td>
                        <td>{{ $model->get('name') }}</td>
                        <td>
                            <a href="{{route('getCapDerivatives', $model->get('cap_model_id'))}}" class="btn btn-primary">
                                <i class="fa fa-car"></i> View Derivatives
                            </a>     

                            <a href="{{route('saveCapData', $model->get('cap_model_id'))}}" class="btn btn-success">
                                <i class="fa fa-save"></i> Save Model &amp; Derivatives to DB
                            </a>                    
                        </td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        {{ $models->links('vendor.pagination.small-default') }}
    </div>
</div>
@endsection
