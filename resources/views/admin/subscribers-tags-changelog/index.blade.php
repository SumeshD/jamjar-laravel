@extends('adminlte::page')

@section('content_header')
    <h1>Mailchimp Tags Changelog</h1>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Date Completed</th>
                        <th>Changes count</th>
                        <th>More Info</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($changelogs as $changelog)
                        <tr>
                            <td>{{ $changelog->id }}</td>
                            <td>{{ $changelog->created_at->format('Y/m/d H:i') }}</td>
                            <td>{{ count($changelog->getRecords()) }}</td>
                            <td><a class="btn btn-xs btn-success" href="{{ route('subscribersTagsChangelogDetails', [$changelog->id]) }}">See details</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        {{ $changelogs->appends(request()->query())->links('vendor.pagination.small-default') }}
    </div>
</div>

@endsection
