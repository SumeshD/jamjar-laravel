@extends('adminlte::page')

@section('content_header')
    <h1>Mailchimp Tags Changelog</h1>
    <h2>Changelog: {{ $changelog->id }}. - {{ $changelog->created_at->format('Y/m/d H:i') }}</h2>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Tags</th>
                        <th>Member id</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($changelog->getRecordsGroupedByEmail() as $record)
                        <tr>
                            <td>{{ $record->email }}</td>
                            <td>{{ $record->tags }}</td>
                            <td>{{ $record->member_id }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
