@extends('layouts.app')
@section('content')
    <section class="jamjar__container valuations">
        <div class="valuations__header">
            <div class="valuations__title">
                <h2>
                    <img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}">
                    <span>{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</span>

                    <span class="valuations__badge js-show-vehicle-details">Show Details <i class="fa fa-chevron-down"></i></span>
                </h2>
                @php
                    $cssClassName = '';
                    if ($vehicle->hasCompleteSale()) {
                        $cssClassName = 'complete-sales';
                    } elseif ($vehicle->hasPendingSale()) {
                        $cssClassName = 'pending-sales';
                    } elseif ($vehicle->hasActiveValuations()) {
                        $cssClassName = 'active-valuations';
                    }
                @endphp

                <div class="valuations__buttons mob-hide" style="float: none; width: 100%; margin-top: -60px;">
                    <a href="{{ route('vehicleEditForm', $vehicle->id) }}" class="edit-vehicle-button button button--grey-bg {{ $cssClassName }}" >
                        Edit Details
                    </a>
                </div>
            </div>
        </div>
        <div class="valuations__vehicle-details u-hide mob-hide">
            <ul>
                <li>
                    <span>Derivative:</span> <span>{{$vehicle->meta->derivative}}</span>
                </li>
                <li>
                    <span>Plate Year:</span> <span>{{$vehicle->meta->plate_year}}</span>
                </li>
                <li>
                    <span>Registration:</span> <span>{{$vehicle->numberplate}}</span>
                </li>
                <li>
                    <span>Mileage:</span> <span>{{$vehicle->meta->mileage}}</span>
                </li>
            </ul>
            <ul>
                <li>
                    <span>Service History:</span> <span>{{$vehicle->meta->service_history}}</span>
                </li>
                <li>
                    <span>MOT:</span> <span>{{$vehicle->meta->mot}}</span>
                </li>
                <li>
                    <span>Colour:</span> <span>{{$vehicle->meta->colour->title}}</span>
                </li>
                <li>
                    <span>Write Off:</span> <span>{{ trans('vehicles.WriteOff' . $vehicle->meta->write_off) }}</span>
                </li>
                <li>
                    <span>Non Runner:</span> <span>{{ trans('vehicles.NonRunner' . $vehicle->meta->non_runner) }}</span>
                </li>
            </ul>
        </div>

        @if (!empty((array) $valuations))
            <form id="save-valuation-form" action="{{ route('storeValuation', $vehicle) }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @endif
        <div class="box box--first">
            <div class="box__container empty-valuations-container" style="display:none">
                <h1 class="box__title">Saved Valuations</h1>
                <div class="box__content">
                    <p>Your vehicle is now visible to the UK's best online car buying companies, we will notify you when you receive a great offer!</p>
                </div>
            </div>
            <table style="width:100%;" class="valuation-table-header-only">
                <thead>
                <tr>
                    <th width="25%">Dealer</th>
                    <th width="35%">Details</th>
                    <th width="40%">Valuation</th>
                </tr>
                </thead>
                <tbody>
                    @if (count($valuations) == 0)
                        <tr class="valuation-holder" data-price="-1">
                            <td colspan="3">
                                <div class="loader__content offers-loading-box">
                                    <i class="fa fa-car"></i>
                                    Loading offers...
                                </div>
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
            @include('quote.loading-valuations')
            <div id="improve-wrapper" style="text-align: center;">
                @include('matrix.valuation-drafts.add-to-marketplace-area', [
                    'vehicle' => $vehicle,
                    'title' => 'Improve Your Offer',
                    'description' => "Adding more information including any images of your vehicle, will help gain improved valuations from our marketplace network of partners.",
                    'descriptionLine2' => "We will inform you of any improved valuations via text message and email",
                    'buttonText' => 'Improve My Offers',
                    'repeatBackground' => false,
                    'customerRemoveMarketplace' => $vehicle->is_visible_on_marketplace,
                    'route' => route('showAdditionalInformationForm', [$vehicle->id]),
                ])
            </div>
        </div>
    </section>

    <div class="loading" style="display:none;"></div>

    @push('scripts-after')
        <script>


            $(document).ready(function(){
                $('.remove').click(function(e){
                    e.preventDefault();
                    swal({
                        title: 'Remove vehicle from Marketplace?',
                        text: 'You are about to remove the vehicle from Jamjar Marketplace. Your vehicle will no longer be visible to the rest of our partner network',
                        type: 'warning',
                        buttons:true,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                    }).then((willDelete) => {
                        if (willDelete) {
                            window.location = $(this).attr("href");
                        }
                    })
                });

            });



            $('.js-show-vehicle-details').on('click', function(e) {
                e.preventDefault();
                $(this).toggleClass('active');
                if ($(this).hasClass('active')) {
                    $('.valuations__vehicle-details').slideDown();
                    $(this).html('Hide Details <i class="fa fa-chevron-up"></i>');
                    $(this).stop();
                } else {
                    $('.valuations__vehicle-details').slideUp();
                    $(this).html('Show Details <i class="fa fa-chevron-down"></i>');
                    $(this).stop();
                }
            });

            $('.edit-vehicle-button.pending-sales').on('click', function(e) {
                e.preventDefault();
                var $clickedElement = $(this);
                $('.loading').show();
                swal({
                    title: "Are you sure?",
                    text: 'Editing the details of this vehicle will cancel the offer you have already accepted.',
                    icon: "warning",
                    buttons: ["Cancel", "Edit"],
                    dangerMode: true,
                }).then((continueEdit) => {
                    if (continueEdit) {
                        window.location = $clickedElement.attr('href');
                    } else {
                        $('.loading').hide();
                    }
                });
                return false;
            });

            $('.button--orange-bg-front').on('click', function(e) {
                e.preventDefault();
                var $clickedElement = $(this);
                $('.loading').show();
                swal({
                    title: "Are you sure?",
                    text: 'By continuing you will cancel the offer you have already accepted.',
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                }).then((continueEdit) => {
                    if (continueEdit) {
                        $('#cancel-valuation-form-').submit();
                    } else {
                        $('.loading').hide();
                    }
                });
                return false;
            });




            $('.edit-vehicle-button.complete-sales').on('click', function(e) {
                e.preventDefault();
                var $clickedElement = $(this);
                $('.loading').show();
                swal({
                    title: "Sale Complete",
                    text: 'You are no longer able to edit this vehicle.',
                    icon: "warning",
                }).then(() => {
                    $('.loading').hide();
                });
                return false;
            });


            $('.accept-ctb-button').on('click', function(e) {
                e.preventDefault();
                var link = $(this).attr('href');
                $('.loading').show();
                var data = {
                    "valuationId": $(this).attr('data-valuation-id'),
                    "_token": $(this).attr('data-csrf-token'),
                };

                axios.post('{{ route('carTakeBackUpdateCustomerDataWebhook') }}', data)
                    .then(function(res) {
                        if(res.data.success){
                            window.location = link;
                        } else {
                            $('.loading').hide();
                        }
                    }).catch(function(err) {
                    $('.loading').show();
                    console.log("AXIOS ERROR: ", err);
                });
            });

            $('.accept-mw-button').on('click', function(e) {
                e.preventDefault();
                var link = $(this).attr('href');
                $('.loading').show();
                var data = {
                    "valuationId": $(this).attr('data-valuation-id'),
                    "_token": $(this).attr('data-csrf-token'),
                };

                axios.post('{{ route('MotorwiseCreateNewSale') }}', data)
                    .then(function(res) {
                        if(res.data.success){
                            window.location = link;
                        } else {
                            $('.loading').hide();
                            location.reload();
                        }
                    }).catch(function(err) {
                    $('.loading').show();
                    console.log("AXIOS ERROR: ", err);
                });
            });

            $('.accept-api-valuation').on('click', function(e) {
                e.preventDefault();
                var link = $(this).attr('href');
                $('.loading').show();
                var data = {
                    "valuationId": $(this).attr('data-valuation-id'),
                    "_token": $(this).attr('data-csrf-token'),
                };

                axios.post('{{ route('acceptValuationWebhook') }}', data)
                    .then(function(res) {
                        if(res.data.success){
                            window.location = link;
                        } else {
                            $('.loading').hide();
                        }
                    }).catch(function(err) {
                    $('.loading').show();
                    console.log("AXIOS ERROR: ", err);
                });
            });

        </script>
    @endpush
@endsection
