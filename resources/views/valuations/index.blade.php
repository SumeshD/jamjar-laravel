@extends('newlayouts.app')

@section('content')

	<main role="main">
		<section class="page-section">
			<div class="page-section__content page-section__content--valuation-saved page-section__content--centered">
				<h2>
					Your saved vehicles
				</h2>
				<p class="small">
					Select a valuation to confirm or refine.
				</p>
				<table>
					<thead>
					<tr>
						<th>Your vehicle</th>
						<th>Date</th>
						<th>Best valuation</th>
						<th>Purchaser</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					@php($border = false)
					@foreach($offers as $offer)
						@php ($valuation = $offer->highestValuation)
						@php ($vehicle = $offer->vehicle)
						@if($valuation)
							<tr class="additional-info">
								<td colspan="5" @if($border == false) class="no-border" @endif>
									@php($border = true)
								</td>
							</tr>
							<tr>
								<td>{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}<p class="small">{{ $vehicle->numberplate  }}</p></td>
								<td>
									<ul>
										<li>{{ $valuation ? $valuation->created_at->format('d.m.Y') : $vehicle->created_at->format('d.m.Y') }}</li>
									</ul>
								</td>
								<td>
									@if ($valuation)
										{{ $valuation->value }}
									@else
										no current offers
									@endif
								</td>
								<td>
									@if(isset($valuation->company))
										@if ($valuation->company->company_type == 'api')
											<img src="{{ secure_asset('images/api-partners/'.strtolower($valuation->company->name).'/logo.jpg') }}" alt="Dealer" width="168">
										@else
											<img src="{{ substr($valuation->company->website->logo_image, 0, 4) == 'http' ? '' : '/' }}{{ $valuation->company->website->logo_image }}" alt="Dealer" width="168">
										@endif
									@endif
								</td>
								<td>
									<a href="{{route('showSavedValuation', $valuation->uuid)}}" title="See all valuations">See all valuations</a>
								</td>
							</tr>
						@endif
					@endforeach
					</tbody>
				</table>
			</div>
		</section>
		<section class="page-section mob-hide">
			<div class="page-section__content page-section__content--centered">
				<h3>
					As featured in
				</h3>
				<section class="page-section__content__media-logos">
					<div class="page-section__content__media-logos__carousel-tape">
						<img src="/images/ux/kb_media_logo_1.e77e5338.png" alt="The Guardian" width="154">
						<img src="/images/ux/kb_media_logo_2.d4f98b37.png" alt="BBC" width="169">
						<img src="/images/ux/kb_media_logo_3.12be4287.png" alt="Top Gear" width="224">
						<img src="/images/ux/kb_media_logo_4.c8c95f5f.png" alt="The Telegraph" width="297">
					</div>
				</section>
				<h3>
					Sell any car fast
				</h3>
				<section class="page-section__content__auto-logos">
					<div class="page-section__content__auto-logos__carousel-tape">
						<img src="/images/ux/kb_auto_logo_1.e7ab6737.png" alt="BMW" width="120">
						<img src="/images/ux/kb_auto_logo_2.c1b4defc.png" alt="Audi" width="120">
						<img src="/images/ux/kb_auto_logo_3.0cb5b59c.png" alt="Mercedes" width="120">
						<img src="/images/ux/kb_auto_logo_4.21c602f3.png" alt="Ford" width="120">
						<img src="/images/ux/kb_auto_logo_5.f20e53f2.png" alt="Volkswagen" width="120">
					</div>
				</section>
			</div>
		</section><!-- Media features -->
		<section class="page-section page-section--shaded mob-hide">
			<div class="page-section__content page-section__content--video-hero">
				<h3>
					The original car buying comparison service
				</h3>
				<div class="page-section__content__video-hero paused">
					<article class="video-hero__badge">
						<p><strong>Watch the latest TV ad to see how it works</strong></p>
					</article>
					<video width="556" height="313" poster="/images/ux/kb_video_placeholder.ce2fac56.png" onclick="this.paused ? this.play() : this.pause();" playsinline="">
						<source src="/images/ux/kb_ad_1.06f2fe74.mp4" type="video/mp4">
						<source src="/images/ux/kb_ad_1.8abb1b5f.ogv" type="video/ogg">
					</video>
				</div>
				<article>
					<p><strong>Hundreds of UK car buyers</strong></p>
					<p class="small">
						See offers from the hundreds of expert car buying businesses that work with Jamjar.
					</p>
				</article>
				<article>
					<p><strong>Fully transparent, always</strong></p>
					<p class="small">
						Get all the details you need right here to make the best decision for your vehicle.
					</p>
				</article>
				<article>
					<p><strong>Cars, vans and scrap</strong></p>
					<p class="small">
						We&#39;ll help you find the right deal, whether it&#39;s a car, a van or destined for the scrapyard.
					</p>
				</article>
				<article>
					<a href="#" title="More about Jamjar">More about Jamjar</a>
				</article>
			</div>
		</section><!-- Media features -->
	</main><!-- Main page content -->
@endsection
@if(Session::has('showcase'))
	@push('scripts-after')
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
		<script type="text/javascript">
			// $(document).ready(function(){
			// 	Swal.fire({
			// 		html:
			// 				'<h2 style="margin-bottom: 20px;">Thanks for the additional information</h2>' +
			// 				'<p class="small">Our full dealer network have been sent your additional details  to help them make improved offers. <br>We will email or text you with these offers as and when they come in</p>' +
			// 				'<div style="padding-top: 20px;"><a id="close-swal" style="margin: 0 auto;" class="showcase-success" href="#" title="See your current best valuations">Review you current best offers</a></div>',
			// 		showCloseButton: false,
			// 		showCancelButton: false,
			// 		showConfirmButton: false,
			// 		width: "800px",
			// 		padding: "3%",
			// 		allowOutsideClick: false
			//
			// 	})
			//
			// 	$("#close-swal").on("click", function() {
			// 		Swal.close();
			// 	})
			// })
		</script>
		<style type="text/css">
			.swal2-html-container{
				color: #0a0a0a;
			}
		</style>
	@endpush
@endif
