@php
    $showcaseText = 'Edit showcase';
    if(count($vehicle->getImages()) == 0 && $vehicle->service_history_information == '' && $vehicle->additional_specification_information == '') {
        $showcaseText = 'Add to showcase';
    }
@endphp
@extends('newlayouts.app')
@section('content')
    <main role="main">
        <section class="page-section page-section--shaded">
            <div class="page-section__content page-section__content--valuation-progress">
                <table>
                    <tr>
                        <td>
                            <img src="{{ secure_asset('images/manufacturers/' . str_slug($vehicle->meta->manufacturer)) }}.png" alt="{{ $vehicle->meta->model }}" width="120">
                        </td>
                        <td class="padding-left--20">
                            <h3>{{ $vehicle->meta->manufacturer }} {{ $vehicle->meta->model }}</h3>
                            <p>{{$vehicle->numberplate}}</p>
                           <a href="{{route('vehicleEditForm', $vehicle->id)}}" title="Edit details"><small>Edit details</small></a> <span>•</span> <a href="#" hidden id="show-details-button" title="Show details"><small>Show</small></a><a href="#" id="hide-details-button" title="Hide details"><small>Hide</small></a>
                        </td>
                    </tr>
                </table>
                <table style="margin-left:50px; margin-top: 20px; font-size: 10pt" class="car-details">
                    <tr>
                        <td>Derivative:</td>
                        <td>{{$vehicle->meta->derivative}}</td>
                    </tr>
                    <tr>
                        <td>Plate Year:</td>
                        <td>{{$vehicle->meta->plate_year}}</td>
                    </tr>
                    <tr>
                        <td>Registration:</td>
                        <td>{{$vehicle->numberplate}}</td>
                    </tr>
                    <tr>
                        <td>Mileage:</td>
                        <td>{{$vehicle->meta->mileage}}</td>
                    </tr>

                </table>
                <table style="margin-left:50px; margin-top: 20px; font-size: 10pt" class="car-details">
                    <tr>
                        <td>Service History:</td>
                        <td>{{$vehicle->meta->service_history}}</td>
                    </tr>
                    <tr>
                        <td>MOT:</td>
                        <td>{{$vehicle->meta->mot}}</td>
                    </tr>
                    <tr>
                        <td>Colour:</td>
                        <td>{{$vehicle->meta->colour->title}}</td>
                    </tr>
                    <tr>
                        <td>Write Off:</td>
                        <td>{{ trans('vehicles.WriteOff' . $vehicle->meta->write_off) }}</td>
                    </tr>
                    <tr>
                        <td>Non Runner:</td>
                        <td>{{ trans('vehicles.NonRunner' . $vehicle->meta->non_runner) }}</td>
                    </tr>

                </table>
                <div class="progress-wrapper margin-right--15">
                    <div class="progress-wrapper__progress-point progress-wrapper__progress-point--passed">
                        <span>Vehicle</span>
                        1
                    </div>
                    <div class="progress-wrapper__progress-waypoint"></div>
                    <div class="progress-wrapper__progress-point progress-wrapper__progress-point--passed">
                        <span>Valuations</span>
                        2
                    </div>
                    <div class="progress-wrapper__progress-waypoint"></div>
                    <div class="progress-wrapper__progress-point progress-wrapper__progress-point--passed">
                        <span style="width:100px;">Vehicle location</span>
                        3
                    </div>
                    <div class="progress-wrapper__progress-waypoint"></div>
                    <div class="progress-wrapper__progress-point progress-wrapper__progress-point--active">
                        <span>Accept</span>
                        4
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section">
            <div class="page-section__content page-section__content--valuation-step5 page-section__content--centered">
                @if($valuationId)
                    <h2>
                        All done, you are ready to connect with your buying partner
                    </h2>
                    <p class="small">
                        You will be transferred to our buying partners website to complete the deal and JamJar.com will pass your information so that they can complete the deal with you.
                    </p>
                @else
                    <h2>
                        @if (isset($fromEmail))
                            Congratulations you've had a bespoke offer
                        @else
                            Your current best instant valuations
                        @endif
                    </h2>
                    <p class="small">
                    </p>
                @endif
                <table style="max-width: 1100px;">
                    <thead>
                    <tr>
                        <th style="text-align: center;">Dealer</th>
                        <th colspan="2" style="text-align: center">Details</th>
                        <th style="text-align: center">Valuation</th>
                        @if(empty($valuationId))
                            <th></th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $first = true
                    @endphp
                    @foreach ($valuations as $valuation)
                        <?php
                        /** @var \JamJar\Valuation $valuation */
                        ?>
                        @if($isSaved)
                            @php
                                $location = ($valuation->location ? $valuation->location : $valuation->company->location->first());
                            @endphp
                            @if ($valuation->getOriginal('dropoff_value') || $valuation->getOriginal('collection_value'))
                                <tr>
                                    <td>
                                        @if ($valuation->company->company_type == 'api')
                                            <img src="{{ secure_asset('images/api-partners/'.strtolower($valuation->company->name).'/logo.jpg') }}" alt="Dealer" width="168">
                                        @else
                                            <img src="{{ substr($valuation->company->website->logo_image, 0, 4) == 'http' ? '' : '/' }}{{ $valuation->company->website->logo_image }}" alt="Dealer" width="168">
                                        @endif
                                        @if($valuation->company->company_type != 'api' && $valuation->company->website->logo_image=='partner-websites/default/jamjar_partner.jpg')
                                            <p class="default-company">{{$valuation->company->name}}</p>
                                        @endif
                                    </td>
                                    <td style="font-weight: normal; font-size: 14px; text-align: center">
{{--                                        @if(!$valuation->scrap)--}}
                                            @if($location->allow_collection && $location->allow_dropoff)
                                                @if ((($location->collection_fee == null) || ($location->collection_fee == 0)) && (($location->dropoff_fee == null) || ($location->dropoff_fee == 0)))
                                                    Free collection included
                                                @elseif(($location->collection_fee != 0) && (($location->dropoff_fee == null) || ($location->dropoff_fee == 0)))
                                                    Collection available at &pound;{{$valuation->getCollectionFeeInPence() / 100}}<br> or, <br> Drop-off available. @if($valuation->distance){{$valuation->distance}} miles.@endif
                                                @elseif((($location->collection_fee == null) || ($location->collection_fee == 0)) && ($location->dropoff_fee != 0))
                                                    Free collection included
                                                @else
                                                    Drop-off available. @if($valuation->distance){{$valuation->distance}} miles.@endif
                                                @endif
                                            @elseif($location->allow_collection && !$location->allow_dropoff)
                                                @if(($location->collection_fee == null) || ($location->collection_fee == 0))
                                                    Free collection included.
                                                @else
                                                    Collection available at &pound;{{$valuation->getCollectionFeeInPence() / 100}}.
                                                @endif
                                            @elseif(!$location->allow_collection && $location->allow_dropoff)
                                                Drop-off available. @if($valuation->distance){{$valuation->distance}} miles.@endif
                                            @endif
{{--                                            @if ($valuation->shouldBeCollectedFromAnyLocation())--}}
{{--                                                Collection from your address--}}
{{--                                            @elseif ($valuation->shouldBeCollectedFromClientLocation())--}}
{{--                                                Collection from your address--}}
{{--                                            @elseif ($valuation->shouldBeDroppedAtDistance())--}}
{{--                                                Distance: {{$valuation->distance}} miles--}}
{{--                                            @elseif ($valuation->shouldBeDroppedAtLocation())--}}
{{--                                                Drop off in {{ ucfirst($location->city) }}--}}
{{--                                            @endif--}}
{{--                                        @else--}}
{{--                                            @if($valuation->collection_part)--}}
{{--                                                @if ($valuation->shouldBeCollectedFromAnyLocationScrapPartner())--}}
{{--                                                    Collection from Mainland UK--}}
{{--                                                @elseif ($valuation->shouldBeCollectedFromClientLocationScrapPartner())--}}
{{--                                                    Collection from your address--}}
{{--                                                @endif--}}
{{--                                            @else--}}
{{--                                                @if ($valuation->shouldBeDroppedAtDistance())--}}
{{--                                                    Distance: {{$valuation->distance}} miles--}}
{{--                                                @elseif ($valuation->shouldBeDroppedAtLocation())--}}
{{--                                                    Drop off in {{ ucfirst($location->city) }}--}}
{{--                                                @endif--}}
{{--                                            @endif--}}
{{--                                        @endif--}}
                                    </td>
                                    <td>
                                        <ul>
                                            @if ($valuation->getDraft() and $valuation->getDraft()->getValuationValidityInDays())
                                                <li>
                                                    {{$valuation->getDraft()->getValuationValidityInDays()}} days Quote Guarantee
                                                </li>
                                            @elseif ($valuation->company->valuation_validity == null)
                                                <li>
                                                    5 Day Quote Guarantee
                                                </li>
                                            @else
                                                <li>
                                                    {{$valuation->company->valuation_validity}} Quote Guarantee
                                                </li>
                                            @endif
                                            @if(!$valuation->scrap)
                                                @if($location->allow_collection)
                                                    @if (($location->collection_fee == null) || ($location->collection_fee == 0))
                                                        @if ($valuation->collection_value)
                                                            <li>
                                                                Free Collection
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endif
                                            @else
                                                @if($valuation->collection_part)
                                                    @if ($location->allow_collection)
                                                        @if (($location->collection_fee == null) || ($location->collection_fee == 0))
                                                            @if ($valuation->collection_value)
                                                                <li>
                                                                    Free Collection
                                                                </li>
                                                            @endif
                                                        @endif
                                                    @endif
                                                @endif
                                            @endif
                                            @if (!$valuation->company->company_type == 'api')
                                                @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))
                                                    <li>
                                                        Free Bank Transfer
                                                    </li>
                                                @else
                                                    <li class="u-text-green">
                                                        Offers Bank Transfer
                                                    </li>
                                                @endif
                                            @else
                                                @if ($valuation->payment_method == 'Bank Transfer')
                                                    @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))
                                                        <li>
                                                            Free Bank Transfer
                                                        </li>
                                                    @else
                                                        <li>
                                                            Offers Bank Transfer
                                                        </li>
                                                    @endif
                                                @else
                                                    @if (isset($valuation['payment_method']))
                                                        <li>
                                                            Payment via {{ ucwords($valuation->payment_method) }}
                                                        </li>
                                                    @endif
                                                @endif
                                            @endif

                                            @if ($valuation->company->company_type == 'api')
                                                @if (isset($valuation->admin_fee))
                                                @else
                                                    <li>
                                                        No Admin Fee
                                                    </li>
                                                @endif
                                            @else
                                                @if (($valuation->company->admin_fee == null) || ($valuation->company->admin_fee == 0))
                                                    <li>
                                                        No Admin Fee
                                                    </li>
                                                @endif
                                            @endif
                                        </ul>


                                        <ul>
                                            @if(!$valuation->scrap)
                                                @if ($valuation->getCollectionFeeInPence())
                                                    <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                        &pound;{{$valuation->getCollectionFeeInPence() / 100}} Collection Fee
                                                    </li>
                                                @elseif ($location->allow_collection)
                                                    @if (($location->collection_fee == null) || ($location->collection_fee == 0))
                                                    @else
                                                        @if ($valuation->getOriginal('collection_value'))
                                                            <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                                &pound;{{$location->collection_fee}} Collection Fee
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endif

                                                @if ($valuation->getDropOffFeeInPence())
                                                    <li>
                                                        &pound;{{$valuation->getDropOffFeeInPence() / 100}} Dropoff Fee
                                                    </li>
                                                @elseif ($location->allow_dropoff)
                                                    @if (($location->dropoff_fee != null) and ($location->dropoff_fee != 0))
                                                        @if ($valuation->getOriginal('dropoff_value'))
                                                            <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                                &pound;{{$location->dropoff_fee}} Dropoff Fee
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endif
                                            @endif
                                            @if (!$valuation->company->company_type == 'api')
                                                @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))
                                                @else
                                                    <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                        &pound;{{$valuation->company->bank_transfer_fee}} Bank Transfer Fee
                                                    </li>
                                                @endif
                                            @else
                                                @if ($valuation->payment_method == 'Bank Transfer')
                                                    @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))
                                                    @else
                                                        <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                            &pound;{{$valuation->company->bank_transfer_fee}} Bank Transfer Fee
                                                        </li>
                                                    @endif
                                                @endif
                                            @endif

                                            @if ($valuation->company->company_type == 'api')
                                                @if (isset($valuation->admin_fee))
                                                    <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                        &pound;{{$valuation->admin_fee}} Admin Fee
                                                    </li>
                                                @endif
                                            @else
                                                @if (($valuation->company->admin_fee == null) || ($valuation->company->admin_fee == 0))
                                                @else
                                                    <li style="background-image: url(/images/ux/kb_info_icon.984bc5f4.png);">
                                                        &pound;{{$valuation->company->admin_fee}} Admin Fee
                                                    </li>
                                                @endif
                                            @endif
                                        </ul>
                                    </td>
{{--                                    <td>--}}
{{--                                        <ul>--}}
{{--                                            @if(!$valuation->scrap)--}}
{{--                                                @if ($valuation->getCollectionFeeInPence())--}}
{{--                                                    <li>--}}
{{--                                                        &pound;{{$valuation->getCollectionFeeInPence() / 100}} Collection Fee--}}
{{--                                                    </li>--}}
{{--                                                @elseif ($location->allow_collection)--}}
{{--                                                    @if (($location->collection_fee == null) || ($location->collection_fee == 0))--}}
{{--                                                    @else--}}
{{--                                                        @if ($valuation->getOriginal('collection_value'))--}}
{{--                                                            <li>--}}
{{--                                                                &pound;{{$location->collection_fee}} Collection Fee--}}
{{--                                                            </li>--}}
{{--                                                        @endif--}}
{{--                                                    @endif--}}
{{--                                                @endif--}}

{{--                                                @if ($valuation->getDropOffFeeInPence())--}}
{{--                                                    <li>--}}
{{--                                                        &pound;{{$valuation->getDropOffFeeInPence() / 100}} Dropoff Fee--}}
{{--                                                    </li>--}}
{{--                                                @elseif ($location->allow_dropoff)--}}
{{--                                                    @if (($location->dropoff_fee != null) and ($location->dropoff_fee != 0))--}}
{{--                                                        @if ($valuation->getOriginal('dropoff_value'))--}}
{{--                                                            <li>--}}
{{--                                                                &pound;{{$location->dropoff_fee}} Dropoff Fee--}}
{{--                                                            </li>--}}
{{--                                                        @endif--}}
{{--                                                    @endif--}}
{{--                                                @endif--}}
{{--                                            @endif--}}
{{--                                            @if (!$valuation->company->company_type == 'api')--}}
{{--                                                @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))--}}
{{--                                                @else--}}
{{--                                                    <li>--}}
{{--                                                        &pound;{{$valuation->company->bank_transfer_fee}} Bank Transfer Fee--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            @else--}}
{{--                                                @if ($valuation->payment_method == 'Bank Transfer')--}}
{{--                                                    @if (($valuation->company->bank_transfer_fee == 0) || ($valuation->company->bank_transfer_fee == null))--}}
{{--                                                    @else--}}
{{--                                                        <li>--}}
{{--                                                            &pound;{{$valuation->company->bank_transfer_fee}} Bank Transfer Fee--}}
{{--                                                        </li>--}}
{{--                                                    @endif--}}
{{--                                                @endif--}}
{{--                                            @endif--}}

{{--                                            @if ($valuation->company->company_type == 'api')--}}
{{--                                                @if (isset($valuation->admin_fee))--}}
{{--                                                    <li>--}}
{{--                                                        &pound;{{$valuation->admin_fee}} Admin Fee--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            @else--}}
{{--                                                @if (($valuation->company->admin_fee == null) || ($valuation->company->admin_fee == 0))--}}
{{--                                                @else--}}
{{--                                                    <li>--}}
{{--                                                        &pound;{{$valuation->company->admin_fee}} Admin Fee--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            @endif--}}
{{--                                        </ul>--}}
{{--                                    </td>--}}
                                    <td class="clear-float">
                                        @php
                                            $collection_hide = false;
                                            $dropoff_hide = false;
                                        @endphp
                                        <?php /** @var \JamJar\Valuation $valuation */ ?>
                                        @if ($valuation->isCollectionAvailable() and $valuation->isDropOffAvailable() and !$valuation->scrap)
                                                <span>&pound;{{ number_format(($valuation->getPriceInPence() / 100)) }}</span>
                                        @elseif ($valuation->isCollectionAvailable() and $valuation->isDropOffAvailable() and $valuation->scrap)
                                            @if ($valuation->collection_part)
                                                    <span>{{$valuation->collection_new_price}}</span>
                                            @else
                                                    <span>{{$valuation->dropoff_new_price}}</span>
                                            @endif
                                        @else
                                            @if ($valuation->isCollectionAvailable())
                                                    <span>&pound;{{ number_format(($valuation->getPriceInPence() / 100)) }}</span>
                                            @else
                                                    <span>&pound;{{ number_format(($valuation->getPriceInPence() / 100)) }}</span>
                                            @endif
                                        @endif
                                    </td>
                                    @if(empty($valuationId))
                                        <td class="width--36 padding-left--36" style="text-align: center;">
                                            @php
                                                $expires = $valuation->created_at->addDay($valuation->company->valuation_validity);
                                            @endphp

                                            @if ($valuation->company->company_type == 'api')
                                                @if (now()->gte($expires))
                                                    <a class="button-link" href="#" style="cursor: not-allowed; padding-left: 20px; padding-right: 37px" >Expired</a>
                                                @elseif ($valuation->complete)
                                                    <a class="button-link" href="#" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                @elseif ($valuation->rejected)
                                                    <a class="button-link" href="#" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">Cancelled</a>
                                                @elseif ($valuation->pending)
                                                    <a class="button-link cancel-valuation" href="#" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">Cancel Offer</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">Add more information for buyer</a>
                                                    <form  action="{{ route('cancelValuation',[$uuid]) }}" method="post" id="cancel-valuation-form-{{$uuid}}">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" id="company_id"  name="company_id" value="{{$valuation->company->id}}">
                                                        <input type="hidden" id="uuid"  name="uuid" value="{{$valuation->uuid}}">
                                                        <input type="hidden" id="vehicle_id"  name="vehicle_id" value="{{$vehicle->id}}">
                                                        <input type="hidden" id="vid"  name="vid" value="{{$valuation->id}}">
                                                        <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                                    </form>
                                                @elseif ($valuation->pendingOverall)
                                                    <a class="button-link" href="#" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                @elseif($valuation['company']->name == 'Money4yourMotors')
                                                    @if (is_null($valuation->acceptance_uri) or !$valuation->acceptance_uri)
                                                        {{-- For now we need to accept valuations on their behalf --}}
                                                        <form id="accept-valuation-form-{{$valuation->company->id}}" style="text-align: center">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                                            <input type="hidden" id="valuation"  name="valuation" value="{{$valuation->collection_value }}">
                                                            <input type="hidden" id="name"  name="name" value="{{ $vehicle->latest_owner->name }}">
                                                            <input type="hidden" id="email"  name="email" value="{{ $vehicle->latest_owner->email }}">
                                                            <input type="hidden" id="phone"  name="phone" value="{{ $vehicle->latest_owner->telephone }}">
                                                            <input type="hidden" id="postcode"  name="postcode" value="{{ $vehicle->latest_owner->postcode }}">
                                                            <input type="hidden" id="ref" name="ref" value="{{ $valuation->temp_ref }}">
                                                            <input type="hidden" name="valuation-id" value="{{$valuation->id }}">
                                                            {{--                                    <input type="submit" class="button button--green-bg button--block" id="accept-valuation-button-{{$valuation->company->id}}" value="Accept">--}}
                                                            <a onclick="$(this).closest('form').submit();" id="accept-valuation-button-{{$valuation->company->id}}" class="button-link" href="#" style="padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                            <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                        </form>
                                                    @else
                                                        <a id="accept-valuation-button-api-{{$valuation->company->id}}" class="button-link" href="{{$valuation->acceptance_uri}}" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                        <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                    @endif
                                                @elseif ($valuation['company']->name == 'WeBuyCarsToday')
                                                    @if (is_null($valuation->acceptance_uri))
                                                        {{-- For now we need to accept valuations on their behalf --}}
                                                        <form id="accept-valuation-form-{{$valuation['company']->id}}" style="text-align: center">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                                            <input type="hidden" id="valuation"  name="valuation" value="{{$valuation->collection_value }}">
                                                            <input type="hidden" id="name"  name="name" value="{{ $vehicle->owner->name }}">
                                                            <input type="hidden" id="email"  name="email" value="{{ $vehicle->owner->email }}">
                                                            <input type="hidden" id="phone"  name="phone" value="{{ $vehicle->owner->telephone }}">
                                                            <input type="hidden" id="postcode"  name="postcode" value="{{ $vehicle->owner->postcode }}">
                                                            <input type="hidden" id="ref" name="ref" value="{{$valuation->temp_ref }}">
                                                            <input type="hidden" name="valuation-id" value="{{$valuation->id }}">
                                                            <a onclick="$(this).closest('form').submit();" id="accept-valuation-button-{{$valuation->company->id}}" class="button-link" href="#" style="padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                            <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                        </form>
                                                    @else
                                                        <a id="accept-valuation-button-api-{{$valuation->company->id}}" class="button-link" href="{{$valuation->acceptance_uri}}" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                        <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                    @endif
                                                @elseif ($valuation['company']->name == 'CarTakeBack')
                                                    <a data-csrf-token="{{ csrf_token() }}" data-valuation-id="{{ $valuation->id }}" href="{{$valuation->acceptance_uri}}" target="_blank" id="accept-valuation-button-api-{{$valuation['company']->id}}" class="button-link accept-ctb-button" style="padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                @elseif ($valuation['company']->name == 'Motorwise')
                                                    <a data-csrf-token="{{ csrf_token() }}" data-valuation-id="{{ $valuation->id }}" href="{{$valuation->acceptance_uri}}" target="_blank" id="accept-valuation-button-api-{{$valuation['company']->id}}" class="button-link accept-mw-button" style="padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                @else
                                                    {{-- This is the button for all other API partners --}}
                                                    <a data-csrf-token="{{ csrf_token() }}" data-valuation-id="{{ $valuation->id }}" href="{{$valuation->acceptance_uri}}" target="_blank" id="accept-valuation-button-api-{{$valuation['company']->id}}" class="button-link accept-api-valuation" style="padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                @endif
                                            @else

                                                @if (now()->gte($expires))
                                                    <a class="button-link" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">Expired</a>
                                                @elseif ($valuation->complete)
                                                    <a class="button-link" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                @elseif ($valuation->rejected)
                                                    <a class="button-link" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">Cancelled</a>
                                                @elseif ($valuation->pending)
                                                    <a class="button-link cancel-valuation" style="padding-left: 20px; padding-right: 37px" >Cancel Offer</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">Add more information for buyer</a>
                                                    <form  action="{{ route('cancelValuation',[$uuid]) }}" method="post" id="cancel-valuation-form-{{$uuid}}">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" id="company_id"  name="company_id" value="{{$valuation->company->id}}">
                                                        <input type="hidden" id="uuid"  name="uuid" value="{{$valuation->uuid}}">
                                                        <input type="hidden" id="vehicle_id"  name="vehicle_id" value="{{$vehicle->id}}">
                                                        <input type="hidden" id="vid"  name="vid" value="{{$valuation->id}}">
                                                        <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                                    </form>
                                                @elseif ($valuation->pendingOverall)
                                                    <a class="button-link" style="cursor:not-allowed; padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                @else
                                                    <a class="button-link" id="accept-valuation-button-{{$valuation->company->id}}" style="padding-left: 20px; padding-right: 37px">@if($first)Accept current best @else Accept @endif</a>
                                                    <a href="{{route("showShowcaseForm",$vehicle->id)}}">{{ $showcaseText }}</a>
                                                    <form target="_blank" action="{{ route('partnerWebsitePage', [$valuation->company->website,'your-offer']) }}" method="post" id="accept-valuation-form-{{$valuation->company->id}}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PATCH') }}
                                                        <input type="hidden" id="company_id"  name="company_id" value="{{$valuation->company->id}}">
                                                        <input type="hidden" id="uuid"  name="uuid" value="{{$valuation->uuid}}">
                                                        <input type="hidden" id="vid"  name="vid" value="{{$valuation->id}}">
                                                        <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                                        {{-- <input type="submit" class="button button--orange-bg button--block" id="accept-valuation-button-{{$valuation->company->id}}" value="Accept Offer"> --}}
                                                    </form>
                                                @endif
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                                @push('scripts-after')
                                    <script>
                                        @if ($valuation->company->company_type == 'api')
                                        @if ($valuation['company']->name == 'Money4yourMotors')
                                        $('#accept-valuation-form-{{$valuation['company']->id}}').on('submit', function(e) {
                                            e.preventDefault();
                                            $('.loading').show();
                                            var data = {
                                                "ColumnMappings": [
                                                    {
                                                        "Id": "TempReference",
                                                        "Value": $(this).find('#ref').val().toString()
                                                    },
                                                    {
                                                        "Id": "CustomerIPAddress",
                                                        "Value": "-"
                                                    },
                                                    {
                                                        "Id": "VehicleValuation",
                                                        "Value": $(this).find('#valuation').val().replace("£","").replace(",","")
                                                    },
                                                ],
                                                'valuationId': $(this).find('[name="valuation-id"]').val().toString()
                                            };

                                            axios.post('{{ route('money4YourMotorsAcceptValuation') }}', data)
                                                .then(function(res) {
                                                    if(res.data.SUCCESS){
                                                        window.location = res.data.URI;
                                                        // window.open(res.data.URI,'_blank');
                                                    } else {
                                                        $('.loading').hide();
                                                    }
                                                }).catch(function(err) {
                                                $('.loading').show();
                                                console.log("AXIOS ERROR: ", err);
                                            });

                                        });
                                        @elseif ($valuation['company']->name == 'WeBuyCarsToday')
                                        $('#accept-valuation-form-{{$valuation['company']->id}}').on('submit', function(e) {
                                            e.preventDefault();
                                            $('.loading').show();
                                            var data = {
                                                "fullname":$(this).find('#name').val().toString(),
                                                "registration_id": $(this).find('#ref').val().toString(),
                                                "postcode": $(this).find('#postcode').val().toString(),
                                                "contact_no": $(this).find('#phone').val().toString(),
                                                "email": $(this).find('#email').val().toString(),
                                                "vrm": $(this).find('#vrm').val().toString(),
                                                'valuationId': $(this).find('[name="valuation-id"]').val().toString()
                                            };

                                            axios.post('{{ route('weBuyCarsTodayAcceptValuation') }}', data)
                                                .then(function(res) {
                                                    if(res.data.SUCCESS){
                                                        window.location = res.data.URI;
                                                        // window.open(res.data.URI,'_blank');
                                                    } else {
                                                        $('.loading').hide();
                                                    }
                                                }).catch(function(err) {
                                                $('.loading').show();
                                                console.log("AXIOS ERROR: ", err);
                                            });

                                        });
                                        @else
                                        $('#accept-valuation-button-api-{{$valuation->company->id}}').on('click', function(e) {
                                            e.preventDefault();

                                            var $button = $(this);
                                            $button.html('loading...');
                                            $button.addClass('button-spinner-image');
                                            $button.unbind('click');

                                            var url = $(this).attr('href');
                                            Swal.fire({
                                                html:
                                                    '<h2 style="margin-bottom: 20px;">Thank you for using Jamjar Comparison</h2>' +
                                                    '<p class="small">We will now send you to your chosen buying partner. <br><br>Please let us know if we can help you again.</p>',
                                                showCloseButton: false,
                                                showCancelButton: false,
                                                showConfirmButton: false,
                                                width: "800px",
                                                padding: "3%",
                                                allowOutsideClick: false

                                            })
                                            setTimeout(function () {
                                                $('.loading').hide();
                                                window.open(url,'_blank').focus();
                                                window.location = '';
                                            }, 4000);
                                        });
                                        @endif
                                        @else
                                        $('#accept-valuation-button-{{$valuation->company->id}}').on('click', function(e) {
                                            e.preventDefault();
                                            var data = {
                                                'company_id': '{{$valuation->company->id}}',
                                                'vrm': '{{$vehicle->numberplate}}',
                                                'uuid': '{{$valuation->uuid}}',
                                                'vid': '{{$valuation->id}}',
                                            };

                                            var $button = $(this);
                                            $button.html('loading...');
                                            $button.addClass('button-spinner-image');
                                            $button.unbind('click');

                                            axios.patch('{{ route('partnerWebsitePage', [$valuation->company->website,'your-offer']) }}', data)
                                                .then(({data}) => {
                                                    setTimeout(() => {
                                                        $('.loading').hide();
                                                        window.location.href = '{{ route('partnerWebsitePage', [$valuation->company->website,'your-offer']) }}';
                                                    }, 1000);
                                                });

                                            return false;
                                        });
                                        @endif
                                    </script>
                                @endpush
                            @endif
                        @else
                        @endif

                    @php
                        $first = false;
                    @endphp
                    @endforeach
                    </tbody>
                </table>
                @if($valuationId)
                    @php
                        $expires = $valuation->created_at->addDay($valuation->company->valuation_validity);
                    @endphp

                    @if ($valuation->company->company_type == 'api')
                        @if (now()->gte($expires))
                            <a class="button-link" href="#" style="cursor: not-allowed" >Expired</a>
                        @elseif ($valuation->complete)
                            <a class="button-link" href="#" style="cursor:not-allowed;">@if($first)Accept current best @else Accept @endif</a>
                        @elseif ($valuation->rejected)
                            <a class="button-link" href="#" style="cursor:not-allowed;">Cancelled</a>
                        @elseif ($valuation->pending)
                            <a class="button-link cancel-valuation" href="#" style="cursor:not-allowed;">Cancel Offer</a>
                                <a href="{{route("showShowcaseForm",$vehicle->id)}}">Add more information for buyer</a>
                            <form  action="{{ route('cancelValuation',[$uuid]) }}" method="post" id="cancel-valuation-form-{{$uuid}}">
                                {{ csrf_field() }}
                                <input type="hidden" id="company_id"  name="company_id" value="{{$valuation->company->id}}">
                                <input type="hidden" id="uuid"  name="uuid" value="{{$valuation->uuid}}">
                                <input type="hidden" id="vehicle_id"  name="vehicle_id" value="{{$vehicle->id}}">
                                <input type="hidden" id="vid"  name="vid" value="{{$valuation->id}}">
                                <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                            </form>
                        @elseif ($valuation->pendingOverall)
                            <a class="button-link" href="#" style="cursor:not-allowed;">@if($first)Accept current best @else Accept @endif</a>
                        @elseif($valuation['company']->name == 'Money4yourMotors')
                            @if (is_null($valuation->acceptance_uri) or !$valuation->acceptance_uri)
                                {{-- For now we need to accept valuations on their behalf --}}
                                <form id="accept-valuation-form-{{$valuation->company->id}}" style="text-align: center">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                    <input type="hidden" id="valuation"  name="valuation" value="{{$valuation->collection_value }}">
                                    <input type="hidden" id="name"  name="name" value="{{ $vehicle->latest_owner->name }}">
                                    <input type="hidden" id="email"  name="email" value="{{ $vehicle->latest_owner->email }}">
                                    <input type="hidden" id="phone"  name="phone" value="{{ $vehicle->latest_owner->telephone }}">
                                    <input type="hidden" id="postcode"  name="postcode" value="{{ $vehicle->latest_owner->postcode }}">
                                    <input type="hidden" id="ref" name="ref" value="{{ $valuation->temp_ref }}">
                                    <input type="hidden" name="valuation-id" value="{{$valuation->id }}">
{{--                                    <input type="submit" class="button button--green-bg button--block" id="accept-valuation-button-{{$valuation->company->id}}" value="Accept">--}}
                                    <a onclick="$(this).closest('form').submit();" id="accept-valuation-button-{{$valuation->company->id}}" class="button-link" href="#" style="">@if($first)Accept current best @else Accept @endif</a>
                                </form>
                            @else
                                <a id="accept-valuation-button-api-{{$valuation->company->id}}" class="button-link" href="{{$valuation->acceptance_uri}}" style="cursor:not-allowed;">@if($first)Accept current best @else Accept @endif</a>
                            @endif
                        @elseif ($valuation['company']->name == 'WeBuyCarsToday')
                            @if (is_null($valuation->acceptance_uri))
                                {{-- For now we need to accept valuations on their behalf --}}
                                <form id="accept-valuation-form-{{$valuation['company']->id}}" style="text-align: center">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                    <input type="hidden" id="valuation"  name="valuation" value="{{$valuation->collection_value }}">
                                    <input type="hidden" id="name"  name="name" value="{{ $vehicle->owner->name }}">
                                    <input type="hidden" id="email"  name="email" value="{{ $vehicle->owner->email }}">
                                    <input type="hidden" id="phone"  name="phone" value="{{ $vehicle->owner->telephone }}">
                                    <input type="hidden" id="postcode"  name="postcode" value="{{ $vehicle->owner->postcode }}">
                                    <input type="hidden" id="ref" name="ref" value="{{$valuation->temp_ref }}">
                                    <input type="hidden" name="valuation-id" value="{{$valuation->id }}">
                                    <a onclick="$(this).closest('form').submit();" id="accept-valuation-button-{{$valuation->company->id}}" class="button-link" href="#">@if($first)Accept current best @else Accept @endif</a>
                                </form>
                            @else
                                <a id="accept-valuation-button-api-{{$valuation->company->id}}" class="button-link" href="{{$valuation->acceptance_uri}}" style="cursor:not-allowed;">@if($first)Accept current best @else Accept @endif</a>
                            @endif
                        @elseif ($valuation['company']->name == 'CarTakeBack')
                            <a data-csrf-token="{{ csrf_token() }}" data-valuation-id="{{ $valuation->id }}" href="{{$valuation->acceptance_uri}}" target="_blank" id="accept-valuation-button-api-{{$valuation['company']->id}}" class="button-link accept-ctb-button">@if($first)Accept current best @else Accept @endif</a>
                        @elseif ($valuation['company']->name == 'Motorwise')
                            <a data-csrf-token="{{ csrf_token() }}" data-valuation-id="{{ $valuation->id }}" href="{{$valuation->acceptance_uri}}" target="_blank" id="accept-valuation-button-api-{{$valuation['company']->id}}" class="button-link accept-mw-button">@if($first)Accept current best @else Accept @endif</a>
                        @else
                            {{-- This is the button for all other API partners --}}
                            <a data-csrf-token="{{ csrf_token() }}" data-valuation-id="{{ $valuation->id }}" href="{{$valuation->acceptance_uri}}" target="_blank" id="accept-valuation-button-api-{{$valuation['company']->id}}" class="button-link">@if($first)Accept current best @else Accept @endif</a>
                        @endif
                    @else

                        @if (now()->gte($expires))
                            <a class="button-link" style="cursor:not-allowed;">Expired</a>
                        @elseif ($valuation->complete)
                            <a class="button-link" style="cursor:not-allowed;">@if($first)Accept current best @else Accept @endif</a>
                        @elseif ($valuation->rejected)
                            <a class="button-link" style="cursor:not-allowed;">Cancelled</a>
                        @elseif ($valuation->pending)
                            <a class="button-link cancel-valuation" >Cancel Offer</a>
                                <a href="{{route("showShowcaseForm",$vehicle->id)}}">Add more information for buyer</a>
                            <form  action="{{ route('cancelValuation',[$uuid]) }}" method="post" id="cancel-valuation-form-{{$uuid}}">
                                {{ csrf_field() }}
                                <input type="hidden" id="company_id"  name="company_id" value="{{$valuation->company->id}}">
                                <input type="hidden" id="uuid"  name="uuid" value="{{$valuation->uuid}}">
                                <input type="hidden" id="vehicle_id"  name="vehicle_id" value="{{$vehicle->id}}">
                                <input type="hidden" id="vid"  name="vid" value="{{$valuation->id}}">
                                <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                            </form>
                        @elseif ($valuation->pendingOverall)
                            <a class="button-link" style="cursor:not-allowed;">@if($first)Accept current best @else Accept @endif</a>
                        @else
                            <a class="button-link" id="accept-valuation-button-{{$valuation->company->id}}">@if($first)Accept current best @else Accept @endif</a>
                            <form target="_blank" action="{{ route('partnerWebsitePage', [$valuation->company->website,'your-offer']) }}" method="post" id="accept-valuation-form-{{$valuation->company->id}}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <input type="hidden" id="company_id"  name="company_id" value="{{$valuation->company->id}}">
                                <input type="hidden" id="uuid"  name="uuid" value="{{$valuation->uuid}}">
                                <input type="hidden" id="vid"  name="vid" value="{{$valuation->id}}">
                                <input type="hidden" id="vrm"  name="vrm" value="{{$vehicle->numberplate}}">
                                {{-- <input type="submit" class="button button--orange-bg button--block" id="accept-valuation-button-{{$valuation->company->id}}" value="Accept Offer"> --}}
                            </form>
                        @endif
                    @endif

                    @push('scripts-after')
                        <script>
                            @if ($valuation->company->company_type == 'api')
                            @if ($valuation['company']->name == 'Money4yourMotors')
                            $('#accept-valuation-form-{{$valuation['company']->id}}').on('submit', function(e) {
                                e.preventDefault();
                                $('.loading').show();
                                var data = {
                                    "ColumnMappings": [
                                        {
                                            "Id": "TempReference",
                                            "Value": $(this).find('#ref').val().toString()
                                        },
                                        {
                                            "Id": "CustomerIPAddress",
                                            "Value": "-"
                                        },
                                        {
                                            "Id": "VehicleValuation",
                                            "Value": $(this).find('#valuation').val().replace("£","").replace(",","")
                                        },
                                    ],
                                    'valuationId': $(this).find('[name="valuation-id"]').val().toString()
                                };

                                axios.post('{{ route('money4YourMotorsAcceptValuation') }}', data)
                                    .then(function(res) {
                                        if(res.data.SUCCESS){
                                            window.location = res.data.URI;
                                            // window.open(res.data.URI,'_blank');
                                        } else {
                                            $('.loading').hide();
                                        }
                                    }).catch(function(err) {
                                    $('.loading').show();
                                    console.log("AXIOS ERROR: ", err);
                                });

                            });
                            @elseif ($valuation['company']->name == 'WeBuyCarsToday')
                            $('#accept-valuation-form-{{$valuation['company']->id}}').on('submit', function(e) {
                                e.preventDefault();
                                $('.loading').show();
                                var data = {
                                    "fullname":$(this).find('#name').val().toString(),
                                    "registration_id": $(this).find('#ref').val().toString(),
                                    "postcode": $(this).find('#postcode').val().toString(),
                                    "contact_no": $(this).find('#phone').val().toString(),
                                    "email": $(this).find('#email').val().toString(),
                                    "vrm": $(this).find('#vrm').val().toString(),
                                    'valuationId': $(this).find('[name="valuation-id"]').val().toString()
                                };

                                axios.post('{{ route('weBuyCarsTodayAcceptValuation') }}', data)
                                    .then(function(res) {
                                        if(res.data.SUCCESS){
                                            window.location = res.data.URI;
                                            // window.open(res.data.URI,'_blank');
                                        } else {
                                            $('.loading').hide();
                                        }
                                    }).catch(function(err) {
                                    $('.loading').show();
                                    console.log("AXIOS ERROR: ", err);
                                });

                            });
                            @else
                            $('#accept-valuation-button-api-{{$valuation->company->id}}').on('click', function(e) {
                                e.preventDefault();
                                var $button = $(this);
                                $button.html('loading...');
                                $button.addClass('button-spinner-image');
                                $button.unbind('click');

                                var url = $(this).attr('href');
                                Swal.fire({
                                    html:
                                        '<h2 style="margin-bottom: 20px;">Thank you for using Jamjar Comparison</h2>' +
                                        '<p class="small">We will now send you to your chosen buying partner. <br><br>Please let us know if we can help you again.</p>',
                                    showCloseButton: false,
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    width: "800px",
                                    padding: "3%",
                                    allowOutsideClick: false

                                })
                                setTimeout(function () {
                                    $('.loading').hide();
                                    window.open(url,'_blank').focus();
                                    window.location = '';
                                }, 4000);
                            });
                            @endif
                            @else
                            $('#accept-valuation-button-{{$valuation->company->id}}').on('click', function(e) {
                                e.preventDefault();
                                var data = {
                                    'company_id': '{{$valuation->company->id}}',
                                    'vrm': '{{$vehicle->numberplate}}',
                                    'uuid': '{{$valuation->uuid}}',
                                    'vid': '{{$valuation->id}}',
                                };

                                var $button = $(this);
                                $button.html('loading...');
                                $button.addClass('button-spinner-image');
                                $button.unbind('click');

                                axios.patch('{{ route('partnerWebsitePage', [$valuation->company->website,'your-offer']) }}', data)
                                    .then(({data}) => {
                                        setTimeout(() => {
                                            $('.loading').hide();
                                            window.location.href = '{{ route('partnerWebsitePage', [$valuation->company->website,'your-offer']) }}';
                                        }, 1000);
                                    });

                                return false;
                            });
                            @endif
                        </script>
                    @endpush
                @endif
            </div>
        </section>

        <section class="page-section mob-hide">
            <div class="page-section__content page-section__content--centered">
                <h3>
                    As featured in
                </h3>
                <section class="page-section__content__media-logos">
                    <div class="page-section__content__media-logos__carousel-tape">
                        <img src="/images/ux/kb_media_logo_1.e77e5338.png" alt="The Guardian" width="154">
                        <img src="/images/ux/kb_media_logo_2.d4f98b37.png" alt="BBC" width="169">
                        <img src="/images/ux/kb_media_logo_3.12be4287.png" alt="Top Gear" width="224">
                        <img src="/images/ux/kb_media_logo_4.c8c95f5f.png" alt="The Telegraph" width="297">
                    </div>
                </section>
                <h3>
                    Sell any car fast
                </h3>
                <section class="page-section__content__auto-logos">
                    <div class="page-section__content__auto-logos__carousel-tape">
                        <img src="/images/ux/kb_auto_logo_1.e7ab6737.png" alt="BMW" width="120">
                        <img src="/images/ux/kb_auto_logo_2.c1b4defc.png" alt="Audi" width="120">
                        <img src="/images/ux/kb_auto_logo_3.0cb5b59c.png" alt="Mercedes" width="120">
                        <img src="/images/ux/kb_auto_logo_4.21c602f3.png" alt="Ford" width="120">
                        <img src="/images/ux/kb_auto_logo_5.f20e53f2.png" alt="Volkswagen" width="120">
                    </div>
                </section>
            </div>
        </section><!-- Media features -->
        <section class="page-section page-section--shaded mob-hide">
            <div class="page-section__content page-section__content--video-hero">
                <h3>
                    The original car buying comparison service
                </h3>
                <div class="page-section__content__video-hero paused">
                    <article class="video-hero__badge">
                        <p><strong>Watch the latest TV ad to see how it works</strong></p>
                    </article>
                    <video width="556" height="313" poster="/images/ux/kb_video_placeholder.ce2fac56.png" onclick="this.paused ? this.play() : this.pause();" playsinline="">
                        <source src="/images/ux/kb_ad_1.06f2fe74.mp4" type="video/mp4">
                        <source src="/images/ux/kb_ad_1.8abb1b5f.ogv" type="video/ogg">
                    </video>
                </div>
                <article>
                    <p><strong>Hundreds of UK car buyers</strong></p>
                    <p class="small">
                        See offers from the hundreds of expert car buying businesses that work with Jamjar.
                    </p>
                </article>
                <article>
                    <p><strong>Fully transparent, always</strong></p>
                    <p class="small">
                        Get all the details you need right here to make the best decision for your vehicle.
                    </p>
                </article>
                <article>
                    <p><strong>Cars, vans and scrap</strong></p>
                    <p class="small">
                        We&#39;ll help you find the right deal, whether it&#39;s a car, a van or destined for the scrapyard.
                    </p>
                </article>
                <article>
                    <a href="#" title="More about Jamjar">More about Jamjar</a>
                </article>
            </div>
        </section><!-- Media features -->
    </main><!-- Main page content -->

    <div class="loading" style="display:none;"></div>
    @push('scripts-after')
        <style type="text/css">
            .swal2-html-container{
                color: #0a0a0a;
            }
        </style>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            $(document).ready(function(){
                @if(Session::has('showcase'))
                    Swal.fire({
                        html:
                            '<h2 style="margin-bottom: 20px;">Thanks for the additional information</h2>' +
                            '<p class="small">Our full dealer network have been sent your additional vehicle details to help them make an improved bespoke offer. <br><br>We will email or text you with these offers as and when they come in.</p>' +
                            '<div style="padding-top: 20px;"><a id="close-swal" style="margin: 0 auto;" class="showcase-success" href="#" title="See your current best valuations">Review you current best offers</a></div>'+
                            '<br><a href="{{route("showShowcaseForm",$vehicle->id)}}"><small>You can edit your showcase details anytime</small></a>',
                        showCloseButton: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        width: "800px",
                        padding: "3%",
                        allowOutsideClick: false

                    })

                    $("#close-swal").on("click", function() {
                        Swal.close();
                    })
                @endif

                @if(Session::has('no-details-showcase'))
                Swal.fire({
                    html:
                        '<h2>No added vehicle details</h2>' +
                        '<p class="small">We recommend you add images, highlight its full specification and service history to another 912 professional UK buyers currently interested in your vehicle.</p>' +
                        '<div style="padding-top: 20px;"><a id="add-details" style="width:70%; margin: 0 auto;" class="showcase-success" href=\"{{route("showShowcaseForm",$vehicle->id)}}\" title="See your current best valuations">Add vehicle details</a></div>' +
                        '<p class="small">or</p>' +
                        '<a id="close-swal" href="#" ><p class="small" style="color: #545454">Back to current offers</p></a>',
                    showCloseButton: true,
                    showCancelButton: false,
                    showConfirmButton: false,
                    width: "800px",
                    padding: "5%",
                    allowOutsideClick: false

                })

                $("#close-swal").on("click", function() {
                    Swal.close();
                })
                @endif


                $('.remove').click(function(e){
                    e.preventDefault();
                    swal({
                        title: 'Remove vehicle from Marketplace?',
                        text: 'You are about to remove the vehicle from Jamjar Marketplace. Your vehicle will no longer be visible to the rest of our partner network',
                        type: 'warning',
                        buttons:true,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                    }).then((willDelete) => {
                        if (willDelete) {
                            window.location = $(this).attr("href");
                        }
                    })
                });

                $('.cancel-valuation').on('click', function(e) {
                    e.preventDefault();
                    var $clickedElement = $(this);
                    $('.loading').show();

                    Swal.fire({
                        html:
                            '<div><h2>Are you sure?</h2>' +
                            '<p class="small">By continuing you will cancel the offer you have already accepted.</p></div>' +
                            '<div style="display: flex;\n' +
                            '    margin-top: 20px;\n' +
                            '    align-items: center;"><a class="showcase-success" id="confirm-cancel-offer" href="#" style="width:50%; margin: 0 10px; padding-left: 20px; padding-right: 37px" >Yes</a>' +
                            '<a class="showcase-success" id="close-swal" href="#" style="width: 50%; margin: 0 10px; padding-left: 20px; padding-right: 37px" >No</a></div>',
                        showCloseButton: true,
                        showCancelButton: false,
                        showConfirmButton: false,
                        width: "800px",
                        padding: "5%",
                        allowOutsideClick: false
                    });
                    $("#close-swal").on("click", function() {
                        Swal.close();
                    })
                    $("#confirm-cancel-offer").on("click", function() {
                        $('#cancel-valuation-form-{{$uuid}}').submit();
                    })
                        return false;
                });
            });



            $('.js-show-vehicle-details').on('click', function(e) {
                e.preventDefault();
                $(this).toggleClass('active');
                if ($(this).hasClass('active')) {
                    $('.valuations__vehicle-details').slideDown();
                    $(this).html('Hide <i class="fa fa-chevron-up"></i>');
                    $(this).stop();
                } else {
                    $('.valuations__vehicle-details').slideUp();
                    $(this).html('Show <i class="fa fa-chevron-down"></i>');
                    $(this).stop();
                }
            });

            $('.edit-vehicle-button.pending-sales').on('click', function(e) {
                e.preventDefault();
                var $clickedElement = $(this);
                $('.loading').show();
                swal({
                    title: "Are you sure?",
                    text: 'Editing the details of this vehicle will cancel the offer you have already accepted.',
                    icon: "warning",
                    buttons: ["Cancel", "Edit"],
                    dangerMode: true,
                }).then((continueEdit) => {
                    if (continueEdit) {
                        window.location = $clickedElement.attr('href');
                    } else {
                        $('.loading').hide();
                    }
                });
                return false;
            });




            $('.edit-vehicle-button.complete-sales').on('click', function(e) {
                e.preventDefault();
                var $clickedElement = $(this);
                $('.loading').show();
                swal({
                    title: "Sale Complete",
                    text: 'You are no longer able to edit this vehicle.',
                    icon: "warning",
                }).then(() => {
                    $('.loading').hide();
                });
                return false;
            });


            $('.accept-ctb-button').on('click', function(e) {
                e.preventDefault();
                var link = $(this).attr('href');
                $('.loading').show();
                var data = {
                    "valuationId": $(this).attr('data-valuation-id'),
                    "_token": $(this).attr('data-csrf-token'),
                };

                axios.post('{{ route('carTakeBackUpdateCustomerDataWebhook') }}', data)
                    .then(function(res) {
                        if(res.data.success){
                            //window.location = link;
                        } else {
                            $('.loading').hide();
                        }
                    }).catch(function(err) {
                    $('.loading').show();
                    console.log("AXIOS ERROR: ", err);
                });
            });

            $('.accept-mw-button').on('click', function(e) {
                e.preventDefault();
                var link = $(this).attr('href');
                $('.loading').show();
                var data = {
                    "valuationId": $(this).attr('data-valuation-id'),
                    "_token": $(this).attr('data-csrf-token'),
                };

                axios.post('{{ route('MotorwiseCreateNewSale') }}', data)
                    .then(function(res) {
                        if(res.data.success){
                            window.location = link;
                        } else {
                            $('.loading').hide();
                            location.reload();
                        }
                    }).catch(function(err) {
                    $('.loading').show();
                    console.log("AXIOS ERROR: ", err);
                });
            });

            $('.accept-api-valuation').on('click', function(e) {
                e.preventDefault();
                var link = $(this).attr('href');
                $('.loading').show();
                var data = {
                    "valuationId": $(this).attr('data-valuation-id'),
                    "_token": $(this).attr('data-csrf-token'),
                };

                axios.post('{{ route('acceptValuationWebhook') }}', data)
                    .then(function(res) {
                        if(res.data.success){
                            window.location = link;
                        } else {
                            $('.loading').hide();
                        }
                    }).catch(function(err) {
                    $('.loading').show();
                    console.log("AXIOS ERROR: ", err);
                });
            });

            $("#show-details-button").on("click", function(event) {
                $(".car-details").show();
                $(".progress-wrapper").addClass("margin-right--15");
                $("#show-details-button").hide();
                $("#hide-details-button").show();
            });

            $("#hide-details-button").on("click", function(event) {
                $(".car-details").hide();
                $(".progress-wrapper").removeClass("margin-right--15");
                $("#hide-details-button").hide();
                $("#show-details-button").show();
            });

        </script>
    @endpush
@endsection
