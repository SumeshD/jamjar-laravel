@extends('layouts.app')

@section('content')
    
    <div class="jamjar__container">

        <div class="box box--no-padding box--with-header">
            {{-- <h4 class="box__title">Vue Playground</h4> --}}
            <div class="box__content">
                <create-offer-form :years="{{$plateYears}}" :colors="{{$colors}}" :defects="{{$defects}}"></create-offer-form>
            </div>
        </div>

    </div>

@endsection
