@extends('layouts.app')

@section('content')
<header class="subheader">
    <div class="subheader__container">
        <h1 class="subheader__title">Delete Account</h1>
    </div>
</header>

<section class="dashboard dashboard--two-col">
    <div class="dashboard__container">
        <div class="dashboard__col">
            <div class="sidebar">
                <ul class="sidebar__list">
                    <li class="sidebar__item">
                        <a href="{{ route('dashboard') }}" class="sidebar__link">
                            Valuations
                        </a>
                    </li>
                    <li class="sidebar__item">
                        <a href="{{ route('account') }}" class="sidebar__link">
                            Account Details
                        </a>
                    </li>
                    <li class="sidebar__item">
                        <a href="{{ route('delete_account') }}" class="sidebar__link sidebar__link--is-active u-text-red u-text-bold">
                            Delete Account
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="dashboard__col">
            
            <div class="box box--with-header box--no-margin">
                <div class="box__header">Delete Account</div>
                <div class="box__container">
                    <form method="POST" action="{{ route('destroy_account', auth()->user()) }}" class="form" id="deletion-form">
                        <div class="box__content">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <p>Was there something we could have done better? Let us know by emailing <a href="mailto:info@jamjar.com">info@jamjar.com</a></p>
                            <p><br></p>
                            <p>If you wish to remove your account entirely from jamjar.com use the delete button below.</p>
                            <p><br></p>
                            <p>Please note, removing your account from Jamjar will only remove any personal details which we hold about you - any vehicles you have valued through Jamjar will remain as they are integral to our valuation engine.</p>
                            <p><br></p>
                            <p>If you are sure you want to delete your account press the red button below. <strong>There is no undo, all deletions are final, we cannot recover your data once your account has been deleted.</strong></p>
                            <p><br></p>
                            <button type="submit" class="js-big-red-button button button--big button--red-bg">Delete Account</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts-after')
<script>
    $('.js-big-red-button').on('click', function(event) {
        event.preventDefault();
        @if (auth()->user()->isAdmin())
            alert('Administrators cannot delete their accounts');
            return false;
        @endif
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to access your account. It will be removed from our systems.",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            swal({
                title: "You will now be logged out.",
                text: "Your account and all personally identifiable data has been removed. You can not log back in.", 
                icon: "info",
            });
            $('#deletion-form').submit();
          } else {
            swal("Your account has not been deleted!");
          }
        });
    });
</script>
@endpush
@endsection
