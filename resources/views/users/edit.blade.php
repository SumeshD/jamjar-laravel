@extends('layouts.app')

@section('content')
<header class="subheader">
	<div class="subheader__container">
		<h1 class="subheader__title">Account Details</h1>
		<div class="jamjar__buttons u-pull-right">
			<div class="jamjar__buttons u-pull-right">
				<a href="{{ route('/') }}" class="button button--grey-bg"><i class="fa fa-plus"></i>  New Valuation</a>
				<a href="{{ route('dashboard') }}" class="button button--grey-bg">Saved Valuations</a>
			</div>
		</div>
	</div>
</header>

<section class="dashboard dashboard--two-col">
	<div class="dashboard__container">
		<div class="dashboard__col">
			<div class="sidebar">
				<ul class="sidebar__list">
					<li class="sidebar__item">
						<a href="{{ route('dashboard') }}" class="sidebar__link">
							Valuations
						</a>
					</li>
                    <li class="sidebar__item">
                        <a href="{{ route('account') }}" class="sidebar__link sidebar__link--is-active">
                            Account Details
                        </a>
                    </li>
					<!--
                    <li class="sidebar__item">
                        <a href="" class="sidebar__link u-text-red u-text-bold">
                            Delete Account
                        </a>
                    </li>
                    -->
				</ul>
			</div>
		</div>
		<div class="dashboard__col">

			<div class="box box--with-header box--no-margin">
				<div class="box__header">Account Details</div>
				<div class="box__container">
					@if($user->isPartner())
						@include('users.form-associate')
					@else
						@include('users.form-user')
					@endif
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
