<form method="POST" action="{{ route('update_account') }}" class="form">
    <div class="box__content">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}

        <div class="form__group form__group--half {{ $errors->has('name') ? ' form--error' : '' }}">
            <label for="name" class="form__label">Your Name <span class="form__required">*</span></label>
            <input
                id="name"
                type="text"
                class="form__input"
                name="name"
                value="{{ old('name', auth()->user()->profile->name) }}"
                placeholder="Enter Your Name"
                required
            >
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>



        <div class="form__group form__group--half {{ $errors->has('email') ? ' form--error' : '' }}">
            <label for="email" class="form__label">E-Mail Address <span class="form__required">*</span></label>
            <input
                id="email"
                type="email"
                class="form__input"
                name="email"
                value="{{ old('email', auth()->user()->profile->email) }}"
                placeholder="Enter your E-Mail address"
                required
                autofocus
            >
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form__group form__group--half {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
            <label for="mobile_number" class="form__label">Your Mobile Number <span class="form__required">*</span></label>
            <input
                id="mobile_number"
                type="tel"
                class="form__input"
                name="mobile_number"
                value="{{ old('mobile_number') ?? $user->profile->mobile_number }}"
                placeholder="We only use this to send you text alerts."
                required
                autofocus
            >
            @if ($errors->has('mobile_number'))
                <span class="help-block">
                    <strong>{{ $errors->first('mobile_number') }}</strong>
                </span>
            @endif
        </div>

        <div class="form__group form__group--half  @if ($errors->has('address_line_one')) form--errors @endif">
            <label for="address_line_one" class="form__label">Address Line One <span class="form__required">*</span></label>
            <input type="text" class="form__input" id="address_line_one" name="address_line_one" value="{{ old('address_line_one', auth()->user()->profile->address_line_one) }}">
        </div>

        <div class="form__group form__group--half">
            <label for="address_line_two" class="form__label">Address Line Two</label>
            <input type="text" class="form__input" id="address_line_two" name="address_line_two" value="{{ old('address_line_two', auth()->user()->profile->address_line_two) }}">
        </div>

{{--        <div class="form__group form__group--half  @if ($errors->has('town')) form--errors @endif">--}}
{{--            <label for="town" class="form__label">Town</label>--}}
{{--            <input type="text" class="form__input" id="town" name="town" value="{{ old('town', auth()->user()->profile->town) }}">--}}
{{--        </div>--}}

        <div class="form__group form__group--half  @if ($errors->has('city')) form--errors @endif">
            <label for="city" class="form__label">Town or City <span class="form__required">*</span></label>
            <input type="text" class="form__input" id="city" name="city" value="{{ old('city', auth()->user()->profile->city) }}">
        </div>

        <div class="form__group form__group--half">
            <label for="county" class="form__label">County</label>
            <input type="text" class="form__input" id="county" name="county" value="{{ old('county', auth()->user()->profile->county) }}">
        </div>

        <div class="form__group form__group--half  @if ($errors->has('postcode')) form--errors @endif">
            <label for="postcode" class="form__label">Postcode <span class="form__required">*</span></label>
            <input type="text" class="form__input" id="postcode" name="postcode" required value="{{ old('postcode', auth()->user()->profile->postcode) }}">
        </div>

        <div class="form__group form__group--half">
            <label class="form__label"></label>
        </div>

        <div style="margin-top: 10px;" class="form__group form__group--half  @if ($errors->has('send_additional_emails')) form--errors @endif">
            <label for="send_additional_emails" class="form__label">
                <input type="checkbox" class="form__input" id="send_additional_emails" name="send_additional_emails" {{ old('send_additional_emails', auth()->user()->send_additional_emails) ? 'checked=checked' : '' }}>
                &nbsp; &nbsp; SEND ME EMAIL ALERTS
            </label>

        </div>

        <div class="form__submit form__submit--no-border">
            <input type="submit" class="form__input" value="Save Details">
        </div>
    </div>
</form>
