<?php

return [
    'SixPlus' => '6 Months+',
    'ThreetoSix' => '3-6 Months',
    'OneToThree' => '1-3 Months',
    'LessThanOne' => 'Less Than 1 Month',
    'Expired' => 'Expired',
    'FullFranchise' => 'Full Franchise History',
    'PartFranchise' => 'Part Franchise History',
    'PartHistory' => 'Part History',
    'MinimalHistory' => 'Minimal History',
    'None' => 'None',
    'AcceptWriteOffs1' => 'Yes',
    'AcceptWriteOffs0' => 'No',
    'AcceptWriteOffstrue' => 'Yes',
    'AcceptWriteOffsfalse' => 'No',
    'PreviousOwners1' => '1',
    'PreviousOwners2' => '2',
    'PreviousOwners3' => '3',
    'PreviousOwners4' => '4',
    'PreviousOwners5' => '5+',
    'PreviousOwners6' => '5+',
    'PreviousOwners7' => '5+',
    'PreviousOwners8' => '5+',
    'PreviousOwners9' => '5+',
    'PreviousOwners10' => '5+',
    'NonRunner1' => 'Yes',
    'NonRunner0' => 'No',
    'WriteOff1' => 'Yes',
    'WriteOff0' => 'No',
    'condition.great' => 'Great condition - No damage, showroom condition.',
    'condition.good' => 'Good condition - Just a couple of slight defects - i.e. one or two ding dents or scuffed alloy wheels, or a light scratch.',
    'condition.fair' => 'Fair condition - i.e 2 or 3 heavier scratches, couple of tyres below legal limit, small dents.',
    'condition.poor' => 'Poor condition - i.e. heavily dented, heavy scratches, illegal tyres, poor interior condition.',

    'owners_count' => [
        'owners_0' => 'I am the first owner',
        'owners_1' => 'I am the first owner',
        'owners_2' => '1 previous owner',
        'owners_3' => '2 previous owners',
        'owners_4' => '3 previous owners',
        'owners_5' => '4 previous owners',
        'owners_6' => 'More than 5 previous owners',
    ],

    'owners_count_external' => [
        'owners_0' => 'First owner',
        'owners_1' => 'First owner',
        'owners_2' => '1 previous owner',
        'owners_3' => '2 previous owners',
        'owners_4' => '3 previous owners',
        'owners_5' => '4 previous owners',
        'owners_6' => 'More than 4 previous owners',
    ],

    'owners_count_short' => [
        'owners_0' => '1st',
        'owners_1' => '1st',
        'owners_2' => '1 prev',
        'owners_3' => '2 prev',
        'owners_4' => '3 prev',
        'owners_5' => '4 prev',
        'owners_6' => '5+ prev',
    ],

    'service_history' => [
        'FullFranchise' => 'Full Franchise History',
        'PartFranchise' => 'Part Franchise History',
        'PartHistory' => 'Part History',
        'MinimalHistory' => 'Minimal History',
        'None' => 'None',
    ],

    'mot' => [
        'SixPlus' => '6 Months+',
        'ThreetoSix' => '3-6 Months',
        'OneToThree' => '1-3 Months',
        'LessThanOne' => 'Less Than 1 Month',
        'Expired' => 'Expired',
    ],

    'write_off_category' => [
        'none' => 'Select Write-Off Category',
        'A' => 'Cat A (Can\'t be repaired)',
        'B' => 'Cat B (Can\'t be repaired)',
        'C' => 'Cat C (Can be repaired, but it would cost more than the vehicle\'s worth)',
        'D' => 'Cat D (Can be repaired and would cost less than the vehicles worth, excl. additional costs)',
        'N' => 'Cat N (Can be repaired following non-structural damage)',
        'S' => 'Cat S (Can be repaired following structural damage)',
    ],

    'additional_information' => [
        'keys_count' => [
            '1' => '1',
            '2' => '2',
            '3' => '3+',
        ]
    ]


];
