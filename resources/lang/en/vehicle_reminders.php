<?php

return [
    'final_reminder' => '96h reminder',
    'continuous_reminder' => '24h reminder',
    'initial_reminder' => '1h reminder',
];
