
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VeeValidate from 'vee-validate';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('numberplate', require('./components/Numberplate.vue'));
Vue.component('manufacturer-search', require('./components/ManufacturerSearch.vue'));
Vue.component('matrix-offers', require('./components/MatrixOffers.vue'));
Vue.component('select-vehicles', require('./components/SelectVehicles.vue'));
Vue.component('offer-vehicles', require('./components/OfferVehicles.vue'));
Vue.component('enter-car', require('./components/EnterCar.vue'));
Vue.component('matrix-position', require('./components/MatrixPosition.vue'));
Vue.component('matrix-cost-calculator', require('./components/MatrixCostCalculator.vue'));
Vue.component('cost-calculator', require('./components/CostCalculator.vue'));
Vue.component('offer-edit', require('./components/OfferEdit.vue'));
Vue.component('offer-model', require('./components/OfferModel.vue'));
Vue.component('edit-vehicles', require('./components/EditVehicles.vue'));
Vue.component('suggested-values', require('./components/SuggestedValues.vue'));
Vue.component('automated-bids', require('./components/AutomatedBids.vue'));
Vue.component('vue-pagination', require('./components/Pagination.vue'));

Vue.component('create-offer-form', require('./components/CreateOfferForm.vue'));
Vue.component('show-hide-vehicles', require('./components/ShowHideVehicles.vue'));

Vue.use(VeeValidate);

const app = new Vue({
    el: '#app',
});
