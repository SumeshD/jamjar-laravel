$(function() {

    function setHeroHeight() {
        if ($(window).width() > 768) {
            var windowHeight = $(window).outerHeight(true);
            var headerHeight = $('.header').outerHeight(true);
            var heroHeight = windowHeight - headerHeight;
            $('.hero').css('height', heroHeight + 'px');
        }
    }

    // function setTwoNumberDecimal(event) {
    //   this.value = this.value;
    // }

    // setHeroHeight();

    $(window).resize(function(event) {
    // setHeroHeight();
    });

    function setHeaderHeight(){
        var headerHeight = $('.header').outerHeight(true);
        //set spacer same height as header
        $('.spacer').css('padding-bottom', headerHeight + 'px');
    }

    setHeaderHeight();


    //set focus on 'focus-watch' input divs

    $('input').on("focus", function() {

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $("body").addClass('keyboard');
      }

    }).on("blur", function() {
      $("body").removeClass('keyboard');
    });



    //hero__form count
    $( ".hero__form .numberplate__input" ).keydown(function() {

      var count = this.value.length;

      if(count >= 3){
        $(this).parent().find('button').addClass('numberplate__button__active');
      }else{
        $(this).parent().find('button').removeClass('numberplate__button__active');
      }

    });

    $('.hero__form .numberplate__input').on('input',function(e){
      var count = this.value.length;

      if(count >= 3){
        $(this).parent().find('button').addClass('numberplate__button__active');
      }else{
        $(this).parent().find('button').removeClass('numberplate__button__active');
      }

    });

    // Match Height
    $('.form__label').matchHeight();
    $('.uploadedfile__image, .uploadedfile__title, .uploadedfile__status, .uploadedfile__actions').matchHeight();
    $('.credits__col').matchHeight();
    if ($(window).width() > 768) {
        $('.matchheight').matchHeight();
    }
    // End Match Height

    // Onclick handlers
    $('.offer-box__item').on('click', function() {
        $(this).toggleClass('offer-box__item--active');
        $(this).find('.offer-box__children').toggleClass('offer-box__children--active');
    });

    // // Onclick handlers
    // $('#manual-address').on('click', function() {
    //     console.log('MANUAL CLICK');
    //     $('.manual-hide').toggleClass('active');
    // });

    if(typeof pca !== 'undefined') {

        pca.on("load", function(type, id, control) {
            control.listen("populate", function(address) {
                $('.line-one').text(address.Line1);
                $('.line-two').text(address.Line2);
                $('.postcode').text(address.PostalCode);
                $('.city').text(address.City);
                $('.county').text(address.Province);
            });

            control.listen("error", function(error) {
                if (error == 'Demo limit exceeded' || error == 'Key daily limit exceeded' || error == 'Web service not available on this key') {
                    $('#manual-address').hide();
                    $('.manual-hide').fadeIn();
                    $('label[for=address_line_one]').text('Address Line One');
                }
            });
            
        });

    }

    var originAddressLabel = $('label[for=address_line_one] .label-title').text();
    originAddressLabel = originAddressLabel ? originAddressLabel : 'Address';

    var originManualAddressLabel = $('#manual-address').text();
    originManualAddressLabel = originManualAddressLabel ? originManualAddressLabel : 'Enter address manually';

    $('#manual-address').on('click', function() {
        var $plainAddressBox = $('.full-address-box');
        var $hiddenFields = $('.manual-hide');
        var isManualMode = !$plainAddressBox.is(":visible");
        if (isManualMode) {
            $hiddenFields.hide();
            $plainAddressBox.fadeIn();
            $('label[for=address_line_one] .label-title').text(originAddressLabel);
            $('#manual-address').text(originManualAddressLabel);
        } else {
            if('' === $("#postcode").val()){
                if('' === $("#address_line_one").val()){
                    $("#postcode").val($("#address_line_one").attr('placeholder'));
                    $("#address_line_one").attr('placeholder',"");
                } else{
                    $("#postcode").val($("#address_line_one").val());
                }
                $("#address_line_one").val('');
            }
            $plainAddressBox.hide();
            $hiddenFields.fadeIn();
            $('label[for=address_line_one] .label-title').text('Address Line One');
            $('#manual-address').text('Hide details');
        }
    });

    $('.rules__image').on('click', function() {
        $(this).toggleClass('rules__image--active');
        var car = $(this).attr('data-id');
        var classToShow = '.rules__options--' + car;
    // $('.rules__options--' + car).toggleClass('rules__options--active');
    $(classToShow).toggle();
    });

    $('.js-trigger-menu').on('click', function(e) {
        e.preventDefault();
        $('.blackout').toggleClass('active');
        var headerHeight = $('.header').outerHeight(true);
        $('.mega').css('top', headerHeight + 'px');
        $('.mega').toggle();
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).html('<i class="fa fa-times"></i>');
        } else {
            $(this).html('<i class="fa fa-bars"></i>');
        }
    });

    $('[data-toggle]').on('click', function(e) {
        e.preventDefault();
        var classToShow = $(this).attr('data-toggle');

        $('.blackout').addClass('active');
        $('[data-modal=' + classToShow + ']').addClass('modal--active');
    });


    $('.modal__close').on('click', function(e) {
        e.preventDefault();

        $('.blackout').removeClass('active');
        $('[data-modal]').removeClass('modal--active');
    });

    $('.blackout').on('click', function(e) {
        $('.modal__close').trigger('click');
    });

    $('a[data-tab]').on('click', function(e) {
        e.preventDefault();
        let tab = $(this).attr('data-tab');
        let toggle = $('.js-tabcontent-' + tab);

        $('[class*="js-tabcontent"]').hide();
        toggle.show();
        $('.box__tab').removeClass('box__tab--active');
        $(this).parent().addClass('box__tab--active');
    });

    $('#nonrunner').on('change', function(e) {
        console.log('changed!');
    });

    $('.js-toggle-nonrunner').on('click', function(e) {
        let type = $(this).attr('data-id');
        $('.nonrunner-' + type).iCheck('check');
    });

    $('.js-toggle-nonrunner').on('click', function(e) {
        let type = $(this).attr('data-id');
        if (type === 'no') {
            // Uncheck any previously checked non runner attributes
            $('input[type="checkbox"]').iCheck('uncheck');
        }
    });

    $('.js-toggle-writeoff').on('click', function(e) {
        let type = $(this).attr('data-id');
        $('.writeoff-' + type).iCheck('check');
    })
    // end onclick handlers

    // input handlers
    $('#collection_fee').on('input', function() {
        $('#enable_collection').iCheck('check');
    });
    $('#delivery_fee').on('input', function() {
        $('#enable_delivery').iCheck('check');
    });
    // end input handlers

    // iCheck handlers
    $('input#writeoff').on('ifChecked', function(event) {
        var val = $(this).val();
        if (val == 1) {
            $('.form__group--writeoff').removeClass('.u-hide');
            $('.form__group--writeoff').show();
        } else {
            $('.form__group--writeoff').addClass('.u-hide');
            $('.form__group--writeoff').hide();
        }
        console.log(event.type + ' callback ' + val);
    });

    $('input#nonrunner').on('ifChecked', function(event) {
        var val = $(this).val();
        if (val == 1) {
            $('.form__group--defects').removeClass('.u-hide');
            $('.form__group--defects').show();
        } else {
            $('.form__group--defects').addClass('.u-hide');
            $('.form__group--defects').hide();
            // Uncheck any previously checked non runner attributes
            $('input[type="checkbox"]').iCheck('uncheck');
        }
        console.log(event.type + ' callback ' + val);
    });

    $('input').iCheck({
        checkboxClass: 'icheckbox_flat-orange',
        radioClass: 'iradio_flat-orange'
    });

    $('.onoff input').iCheck('destroy');
    $('.spirit-check input').iCheck('destroy');
    $('input.no-icheck').iCheck('destroy');
    // end iCheck handlers

    // TrumboWYG options
    $.trumbowyg.svgPath = '/vendor/trumbowyg/ui/icons.svg';
    $('.js-editor').trumbowyg({
        autogrow: true,
        removeformatPasted: true,
        btns: [
        ['formatting'],
        ['strong', 'em', 'underline'],
        ['foreColor'],
        ['link'],
        ['insertImage'],
        ['justifyLeft', 'justifyCenter', 'justifyRight'],
        ['unorderedList'],
        ['removeformat'],
        ]
    });
    // End trumbowyg options

    // Multi Select Options
    $('.multi-select').multipleSelect();
    // end multi select options

    $('.button--matrix-big').on('click', function(e) {
        e.preventDefault();
        $('.button--matrix-big').removeClass('is-active');
        $(this).addClass('is-active');
        let type = $(this).attr('id');
        $('#delivery-select').hide();
        let price = $(this).children('p.price.is-active').attr('data-price');
        $('#price').val(price);
        $('#deliveryMethod').val(type);
        if (type == 'dropoff') {
            $('#delivery-select').show();
        }
    });

    $("#appraisalSlider").ionRangeSlider({
        grid: true,
        from: 4,
        values: [
        "Poor", "Fair", "Good", "Great"
        ],
        force_edges: true,
        onChange: function(data) {
            var value = data.from_value;

            $('.js-appraisal').hide();
            $('#js-appraisal-' + value).show();

            $('.condition-name').text(value);

            $('.price').removeClass('is-active').hide();
            $('#drop' + value).addClass('is-active').show();
            $('#coll' + value).addClass('is-active').show();
            $('input[name="condition"]').val(value.toLowerCase());
            var collectionValue = $('#coll' + value).attr('data-price');
            var dropoffValue = $('#drop' + value).attr('data-price');

            if (typeof dropoffValue === 'undefined') {
                if (collectionValue <= 0) {
                    $('.not-accepted-condition-message').text('The dealer does not want to accept this car if it is ' + value + ' condition');
                    $('.not-accepted-condition-message').show();
                    $('[type=submit]').attr('disabled', 'disabled').hide();
                } else {
                    $('.footer-price').text('£'+collectionValue);
                    $('input#price').val(collectionValue);
                    $('.not-accepted-condition-message').hide();
                    $('[type=submit]').removeAttr('disabled').show();
                }
            } else {
                if (collectionValue <= 0) {
                    $('.not-accepted-condition-message').text('The dealer does not want to accept this car if it is ' + value + ' condition');
                    $('.not-accepted-condition-message').show();
                    $('[type=submit]').attr('disabled', 'disabled').hide();
                } else {
                    $('.footer-price').text('£'+dropoffValue);
                    $('input#price').val(dropoffValue);
                    $('.not-accepted-condition-message').hide();
                    $('[type=submit]').removeAttr('disabled').show();
                }
            }
        },
    });

    /**
    * Inline Edit Items on the Offers Edit Page
    */
    $(".offer__edit .fa-pencil").click(function() {
        $(this).parent().find('.offer__edit__hide_on_edit').hide();
        $(this).parent().find('.offer__edit__show_on_edit').show();
    });

    $(".offer__edit .fa-check").click(function() {

        var value = "";

    /**
    * Get the value
    * - Select
    * - Input
    * - Checkbox
    */
    if ($(this).parent().find('select').is(":visible")) {
        value = $(this).parent().find('select option:selected').text();
    } else if ($(this).parent().find('input').is(":visible")) {
        if ($(this).parent().find('input').is(':checkbox')) {
            $(this).parent().find('input').each(function() {
                if ($(this).is(":checked")) {
                    if ($(this).attr("id") != "selectAllDefects") {
                        value += $(this).attr('id') + ", ";
                    }
                }
            });
            if (value == "") {
                value = "All";
            }
        } else {
            value = $(this).parent().find('input').val();
        }
    }

    /**
    * Fill the value
    */
    $(this).parent().find('span.offer__edit_static_fill').html(value);

    /**
    * Toggle the elements
    */
    $(this).parent().find('.offer__edit__hide_on_edit').show();
    $(this).parent().find('.offer__edit__show_on_edit').hide();

    });

    $('.offer_edit__derivative_checkbox').change(function() {
        if ($(this).is(":checked")) {
            $(".offer_derivative_edit_" + $(this).attr('id')).removeClass('u-hide');
            $(".offer_derivative_edit_" + $(this).attr('id') + " input").removeAttr('disabled');
        } else {
            $(".offer_derivative_edit_" + $(this).attr('id')).addClass('u-hide');
            $(".offer_derivative_edit_" + $(this).attr('id') + " input").attr('disabled', 'disabled');
        }
    });

    $('.jj-suggested').on('click', function(e) {
        var id = $(this).attr('data-id');
        var val = $(this).attr('data-value');
        $('.offer_derivative_edit_' + id).find('input').val(val);
        $('.offer_derivative_edit_' + id).find('.offer__edit_static_fill').html(val + ' ');
        $(this).hide();
    });

    if ($('.billing_remaining_funds__amount').text().length > 5) {
        $('.billing_remaining_funds__amount').addClass('billing_remaining_funds__amount--small');
    }

    $('.statement__amount').each(function(index, el) {
        if ($(this).text().length > 9) {
            $(this).addClass('statement__amount--small');
        }
    });
    if(is_touch_device()) {
      $('.header__navigation > .menu > li.menu__item:first-child').click(function (e) {
          e.stopPropagation();
          console.log(e.target);
        if($(this).children('.sub-menu').length != 0 && !$(e.target).hasClass('sub-menu__link')) {
          e.preventDefault();
        }
      });
    //   if($('.valuation__ticklist li').length > 0)
    //     $('.valuation__ticklist li').matchHeight();
    }

    $('.valuation .show-more-info').click(function () {
      $(this).siblings('.valuation__ticklist').toggleClass('show');
    });

    $('.partner-add-to-marketplace-row .show-more-info').click(function () {
      $(this).closest('.valuation__ticklist').toggleClass('show');
    });


});

function deleteOffer(offerID){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this bid",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            document.getElementById('delete-offer-' + offerID).submit();
        } else {
            swal("Your offer is safe");
        }
    });
}

function is_touch_device() {
  var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
  var mq = function(query) {
    return window.matchMedia(query).matches;
  }

  if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
    return true;
  }

  // include the 'heartz' as a way to have a non matching MQ to help terminate the join
  // https://git.io/vznFH
  var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
  return mq(query);
}


$(function() {

    $('.radio-button').on('click', function() {
        var $this = $(this);

        $('.radio-button.active').removeClass('active').addClass('inactive');
        $this.removeClass('inactive').addClass('active');

        $('#marketing').val($this.attr('data-value'));
    });

    $('[name=email]').on('change', function(){
        var $element = $(this);

        $element.parent().parent().removeClass('form--errors');
    });

    $('[name=telephone_number]').on('change', function(){
        var $element = $(this);

        $element.parent().parent().removeClass('form--errors');
    });



    $('.radio-button-form').on('submit', function(event) {
        var $form = $(this);

        var processForm = true;

        var $emailField = $('[name=email]');
        if ($emailField && $emailField.parent().hasClass('validation-no')) {
            processForm = false;

            $emailField.parent().parent().addClass('form--errors');
        }

        var $phoneNumber = $('[name=telephone_number]');
        if ($phoneNumber && $phoneNumber.parent().hasClass('validation-no')) {
            processForm = false;

            $phoneNumber.parent().parent().addClass('form--errors');
        }

        if (!processForm) {
            event.preventDefault();
            $('.loader').hide();
        }
    });

    $('.loquate-data-submit-button').on('click', function(event) {
        var $cityInput = $('[name=city]');
        var $postcode = $('[name=postcode]');

        if ((!$cityInput.val() || !$postcode.val()) && (!$cityInput.is(':visible') || !$postcode.is(':visible'))) {
            $('#manual-address').click();
        }
    });

});

$(function() {
    $("input[data-type='separator-format']").each(function() {
        prepareSeparatorFormatInput($(this));
        changeSeparatorFormat($(this));
    });

    $("input[data-type='separator-format']").on({
        keyup: function() {
            changeSeparatorFormat($(this));
        },
        blur: function() {
            changeSeparatorFormat($(this), "blur");
        }
    });
});

function separatorFormatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function prepareSeparatorFormatInput(input){
    var $replacedField = $('<input type="hidden" name="'+ input.attr('name') +'" value="' + input.val() + '">');
    input.attr('original-name', input.attr('name'));
    input.removeAttr('name');

    input.after($replacedField);
}

var previousValuationPrice;

function changeSeparatorFormat(input) {
    var originalInputName = input.attr('original-name');
    var $replacedField = $('[name="' + originalInputName + '"]');

    if (!input.val()) {
        $replacedField.val('');
        return;
    }

    var input_val = input.val();

    if (input_val === "") { return; }
    var original_len = input_val.length;
    var caret_pos = input.prop("selectionStart");

    if (input_val.indexOf(".") >= 0) {
        var decimal_pos = input_val.indexOf(".");
        var left_side = input_val.substring(0, decimal_pos);
        var right_side = input_val.substring(decimal_pos);

        left_side = separatorFormatNumber(left_side);
        right_side = separatorFormatNumber(right_side);

        right_side = right_side.substring(0, 2);
        input_val = left_side + "." + right_side;

    } else {
        input_val = separatorFormatNumber(input_val);
        input_val = "" + input_val;
    }

    var original_val = input_val.replace('£', '').replace(/,/g, '');

    $replacedField.val(original_val);

    previousValuationPrice = input_val;
    input.val(input_val);
}

$(function() {
    var redirectClassPrefix = 'redirect:';
    $('body').on('click', '[class*="' + redirectClassPrefix + '"]', function() {
        var classList = $(this).attr('class').split(/\s+/);
        for (var i = 0; i < classList.length; i++) {
            var className = classList[i];
            if (className.indexOf(redirectClassPrefix) !== -1) {
                var url = className.substr(redirectClassPrefix.length);
                window.location.href = url;
            }
        }
    });

    $('body').on('click', '.redirect-to-associates-apply-form', function() {
        window.location.href = '/associates/apply';
    });
});
